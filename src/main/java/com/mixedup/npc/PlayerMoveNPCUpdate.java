package com.mixedup.npc;

import com.mixedup.Main;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.hashs.IDprov;
import com.mixedup.npc.hashs.*;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

public class PlayerMoveNPCUpdate implements Listener {

    public static float getLookAtYaw(final Vector motion) {
        final double dx = motion.getX();
        final double dz = motion.getZ();
        double yaw = 0;

        if (dx != 0) {
            if (dx < 0) {
                yaw = 1.5 * Math.PI;
            } else {
                yaw = 0.5 * Math.PI;
            }
            yaw -= Math.atan(dz / dx);
        } else if (dz < 0) {
            yaw = Math.PI;
        }
        return (float) (-yaw * 180 / Math.PI - 90);
    }

    public static float getLookAtPitch(final Vector motion) {
        final double dx = motion.getX();
        final double dz = motion.getZ();
        final double dy = motion.getY();

        final double cateto = Math.sqrt(dx * dx + dz * dz);
        final double hipotenuza = Math.sqrt(cateto * cateto + dy * dy);
        final double resultado = Math.toDegrees(dy / hipotenuza);

        if (resultado < 0) {
            return (float) -(resultado);
        } else if (resultado > 0) {
            final double rs = resultado;
            return (float) (rs - resultado - resultado);
        } else if (resultado == 0) {
            return (float) 0;
        }

        return 0;
    }

    @EventHandler
    public void onMove(final PlayerMoveEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (NpcUpdateUtil.get(UUID) != null && NpcUpdateUtil.get(UUID).getStatus() == true) {
            NpcUpdateUtil.get(UUID).setStatus(false);
            NPC.enableNPCS(player);
        }

        if (NpcVerify.get(UUID) == null) {
            new NpcVerify(UUID, true).insert();
            NPC.enableNPCS(player);
        } else if (NpcVerify.get(UUID).getStatus() == false) {
            NpcVerify.get(UUID).setStatus(true);
            NPC.enableNPCS(player);
        }

        if (RegionsNpcUtil.playerInArea(player.getLocation()) != null) {
            if (!RegionsNpcUtil.playerInArea(player.getLocation()).getUUID().equals("NULL")) {
                final String entityID = RegionsNpcUtil.playerInArea(player.getLocation()).getUUID();

                if (NpcCacheHR.get(NpcCacheLoc.get(entityID).getLocation()).getStatus() == true) {
                    final String location = (RegionsNpcUtil.playerInArea(player.getLocation()).getP1x() + 4) + ":" + RegionsNpcUtil.playerInArea(player.getLocation()).getPy() + ":" + (RegionsNpcUtil.playerInArea(player.getLocation()).getP1z() + 4);

                    if (LastNpcView.get(player.getName()) == null) {
                        new LastNpcView(player.getName(), location).insert();
                    } else {
                        LastNpcView.get(player.getName()).setLocation(location);
                    }

                    if (NpcCacheLastV.get(location).getLastViewer().equals("NULL")) {
                        NpcCacheLastV.get(location).setLastViewer(player.getName());

                        final Vector to = new Vector(player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ());
                        final Vector from = new Vector(RegionsNpcUtil.playerInArea(player.getLocation()).getP1x() + 4, RegionsNpcUtil.playerInArea(player.getLocation()).getPy(), RegionsNpcUtil.playerInArea(player.getLocation()).getP1z() + 4);
                        final Vector v = to.subtract(from);
                        final int x = -26;
                        final int y = 64;
                        final int z = -24;

                        for (final Player target : Bukkit.getOnlinePlayers()) {
                            final int id = IDprov.get(target.getName() + entityID).getIDNpcPlayer();
                            NPC.headRotation(Integer.valueOf(id), PlayerMoveNPCUpdate.getLookAtYaw(v) + 92, PlayerMoveNPCUpdate.getLookAtPitch(v));
                        }

                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                NpcCacheLastV.get(location).setLastViewer("NULL");
                            }
                        }, 1200L);
                    } else {
                        if (NpcCacheLastV.get(location).getLastViewer().equals(player.getName())) {
                            final Vector to = new Vector(player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ());
                            final Vector from = new Vector(RegionsNpcUtil.playerInArea(player.getLocation()).getP1x() + 4, RegionsNpcUtil.playerInArea(player.getLocation()).getPy(), RegionsNpcUtil.playerInArea(player.getLocation()).getP1z() + 4);
                            final Vector v = to.subtract(from);
                            final int x = -26;
                            final int y = 64;
                            final int z = -24;

                            for (final Player target : Bukkit.getOnlinePlayers()) {
                                final int id = IDprov.get(target.getName() + entityID).getIDNpcPlayer();
                                NPC.headRotation(Integer.valueOf(id), PlayerMoveNPCUpdate.getLookAtYaw(v) + 92, PlayerMoveNPCUpdate.getLookAtPitch(v));
                            }
                        }
                    }
                }
            }
        } else {
            if (LastNpcView.get(player.getName()) != null) {
                if (!LastNpcView.get(player.getName()).getLocation().equals("NULL")) {
                    if (NpcCacheLastV.get(LastNpcView.get(player.getName()).getLocation()) != null) {
                        NpcCacheLastV.get(LastNpcView.get(player.getName()).getLocation()).setLastViewer("NULL");
                        LastNpcView.get(player.getName()).setLocation("NULL");
                    }
                }
            }
        }
    }
}
