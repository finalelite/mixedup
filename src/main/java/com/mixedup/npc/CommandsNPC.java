package com.mixedup.npc;

import com.mixedup.Main;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import com.mixedup.hashs.IDNpc;
import com.mixedup.hashs.IDprov;
import com.mixedup.npc.hashs.RegionNpc;
import com.mixedup.npc.hashs.RegionsNpc;
import com.mixedup.utils.AlternateColor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

import java.util.Set;

public class CommandsNPC implements CommandExecutor {

    private static final String UUID = "";
    boolean has;

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUIDcache(player.getName());

        if (cmd.getName().equalsIgnoreCase("npc")) {
            if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                if (args.length == 0) {
                    player.sendMessage(ChatColor.RED + "Comandos referente á " + AlternateColor.alternate("&nnpc") + ChatColor.RED + ": " + "\n" + " " + "\n" + ChatColor.GRAY + "/npc criar (skinname) (texto)" + "\n" + ChatColor.GRAY + "/npc deletar" + "\n" + ChatColor.GRAY + "/npc trocartexto (texto)" + "\n" + ChatColor.GRAY + "/npc trocarskin (skinname)" + "\n" + ChatColor.GRAY + "/npc mover" + "\n" + ChatColor.GRAY + "/npc headrotation (on ou off)" + "\n" + ChatColor.GRAY + "/npc addfuncao (inv:xxx / say:xxx / tp:xxx)" + "\n" + ChatColor.GRAY + "/npc removerfuncao" + "\n" + " ");
                    return false;
                }
                if (args[0].equals("criar")) {
                    if (args.length >= 3) {

                        final StringBuilder build = new StringBuilder();
                        for (int i = 2; i < args.length; i++) {
                            build.append(" ");
                            build.append(args[i]);
                        }
                        final String msg = build.toString();

                        if (PlayerUUID.getWithMojang(args[1]) != null) {
                            final Location now = player.getLocation();
                            final Location newloc = new Location(now.getWorld(), now.getX(), now.getY(), now.getZ(), now.getYaw(), now.getPitch());

                            if (NPCApi.getExistNpcHere(newloc.getBlockY() + ":" + newloc.getBlockY() + ":" + newloc.getBlockZ()) == null) {
                                NPC.createNPC(newloc, PlayerUUID.getWithMojang(args[1]), msg.substring(1));
                                player.sendMessage(ChatColor.GREEN + " * NPC spawnado com sucesso.");
                            } else {
                                player.sendMessage(ChatColor.RED + "Ops, já existe um player nesta coordenada.");
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, o nick informado não existe.");
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/npc criar (skinname) (texto)");
                    }
                }

                if (args[0].equals("deletar")) {
                    if (args.length < 1 || args.length > 1) {
                        player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/npc deletar");
                        return false;
                    }
                    if (args.length == 1) {
                        final Location lastblock = player.getTargetBlock(null, 100).getLocation();
                        final Vector finalvector = lastblock.toVector();
                        final Vector firstvector = player.getEyeLocation().toVector();
                        final Vector substractedvector = finalvector.subtract(firstvector);

                        for (int i = 1; i <= NPC.getLastID(); i++) {
                            final BlockIterator BI = new BlockIterator(player.getLocation().getWorld(), firstvector, substractedvector, 0.0, ((Double) lastblock.distance(player.getLocation())).intValue());
                            while (BI.hasNext()) {
                                final org.bukkit.block.Block block = BI.next();

                                String loc1 = "";
                                String loc2 = "";
                                String loc3 = "";

                                if (block.getX() < 0 && block.getZ() < 0) {
                                    loc1 = (block.getX() + 1) + ":" + block.getY() + ":" + (block.getZ() + 1);
                                    loc2 = (block.getX() + 1) + ":" + (block.getY() - 1) + ":" + (block.getZ() + 1);
                                    loc3 = (block.getX() + 1) + ":" + (block.getY() - 2) + ":" + (block.getZ() + 1);
                                } else if (block.getX() > 0 && block.getZ() > 0) {
                                    loc1 = block.getX() + ":" + block.getY() + ":" + block.getZ();
                                    loc2 = block.getX() + ":" + (block.getY() - 1) + ":" + block.getZ();
                                    loc3 = block.getX() + ":" + (block.getY() - 2) + ":" + block.getZ();
                                } else if (block.getX() > 0 && block.getZ() < 0) {
                                    loc1 = block.getX() + ":" + block.getY() + ":" + (block.getZ() + 1);
                                    loc2 = block.getX() + ":" + (block.getY() - 1) + ":" + (block.getZ() + 1);
                                    loc3 = block.getX() + ":" + (block.getY() - 2) + ":" + (block.getZ() + 1);
                                } else if (block.getX() < 0 && block.getZ() > 0) {
                                    loc1 = (block.getX() + 1) + ":" + block.getY() + ":" + block.getZ();
                                    loc2 = (block.getX() + 1) + ":" + (block.getY() - 1) + ":" + block.getZ();
                                    loc3 = (block.getX() + 1) + ":" + (block.getY() - 2) + ":" + block.getZ();
                                }

                                final String npcloc = NPCApi.getLocationID(i);
                                final String[] locs = npcloc.split(":");
                                final double dx = Double.parseDouble(locs[0]);
                                final double dy = Double.parseDouble(locs[1]);
                                final double dz = Double.parseDouble(locs[2]);
                                final String loc = (int) dx + ":" + (int) dy + ":" + (int) dz;

                                if (loc1.equals(loc) || loc2.equals(loc) || loc3.equals(loc)) {
                                    for (final Player target : Bukkit.getOnlinePlayers()) {
                                        NPC.destroy(NPCApi.getNpcID(npcloc));
                                        IDprov.get(target.getName() + NPCApi.getNpcID(npcloc)).setUUID("NULL");

                                        if (NPCApi.getHeadRotateNPC(npcloc) == true) {
                                            final RegionNpc npc = RegionNpc.get(String.valueOf(NPCApi.getNpcID(npcloc)));
                                            RegionsNpc.getTerrains().remove(npc);
                                        }

                                        final int id = NPCApi.getNpcIDD(npcloc);
                                        NPCApi.removeNpc(NPCApi.getNpcID(npcloc));
                                        NPC.updateIDS(id);

                                        player.sendMessage(ChatColor.GREEN + " * NPC removido com sucesso.");

                                        i = NPC.getLastID() + 10;
                                        this.has = true;
                                        break;
                                    }
                                } else {
                                    this.has = false;
                                }
                            }
                        }

                        if (this.has == false) {
                            player.sendMessage(ChatColor.RED + "Para efetuar este comando você precisa estar olhando para algum NPC.");
                        }
                    }
                }

                if (args[0].equals("trocartexto")) {
                    if (args.length < 2) {
                        player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/npc trocartexto (texto)");
                        return false;
                    }
                    if (args.length >= 2) {

                        final StringBuilder build = new StringBuilder();
                        for (int i = 1; i < args.length; i++) {
                            build.append(" ");
                            build.append(args[i]);
                        }
                        final String msg = build.toString();

                        final Location lastblock = player.getTargetBlock(null, 100).getLocation();
                        final Vector finalvector = lastblock.toVector();
                        final Vector firstvector = player.getEyeLocation().toVector();
                        final Vector substractedvector = finalvector.subtract(firstvector);

                        for (int i = 1; i <= NPC.getLastID(); i++) {
                            final BlockIterator BI = new BlockIterator(player.getLocation().getWorld(), firstvector, substractedvector, 0.0, ((Double) lastblock.distance(player.getLocation())).intValue());
                            while (BI.hasNext()) {
                                final org.bukkit.block.Block block = BI.next();

                                String loc1 = "";
                                String loc2 = "";
                                String loc3 = "";

                                if (block.getX() < 0 && block.getZ() < 0) {
                                    loc1 = (block.getX() + 1) + ":" + block.getY() + ":" + (block.getZ() + 1);
                                    loc2 = (block.getX() + 1) + ":" + (block.getY() - 1) + ":" + (block.getZ() + 1);
                                    loc3 = (block.getX() + 1) + ":" + (block.getY() - 2) + ":" + (block.getZ() + 1);
                                } else if (block.getX() > 0 && block.getZ() > 0) {
                                    loc1 = block.getX() + ":" + block.getY() + ":" + block.getZ();
                                    loc2 = block.getX() + ":" + (block.getY() - 1) + ":" + block.getZ();
                                    loc3 = block.getX() + ":" + (block.getY() - 2) + ":" + block.getZ();
                                } else if (block.getX() > 0 && block.getZ() < 0) {
                                    loc1 = block.getX() + ":" + block.getY() + ":" + (block.getZ() + 1);
                                    loc2 = block.getX() + ":" + (block.getY() - 1) + ":" + (block.getZ() + 1);
                                    loc3 = block.getX() + ":" + (block.getY() - 2) + ":" + (block.getZ() + 1);
                                } else if (block.getX() < 0 && block.getZ() > 0) {
                                    loc1 = (block.getX() + 1) + ":" + block.getY() + ":" + block.getZ();
                                    loc2 = (block.getX() + 1) + ":" + (block.getY() - 1) + ":" + block.getZ();
                                    loc3 = (block.getX() + 1) + ":" + (block.getY() - 2) + ":" + block.getZ();
                                }

                                final String npcloc = NPCApi.getLocationID(i);
                                final String[] locs = npcloc.split(":");
                                final double dx = Double.parseDouble(locs[0]);
                                final double dy = Double.parseDouble(locs[1]);
                                final double dz = Double.parseDouble(locs[2]);
                                final String loc = (int) dx + ":" + (int) dy + ":" + (int) dz;

                                if (loc1.equals(loc) || loc2.equals(loc) || loc3.equals(loc)) {
                                    NPCApi.updateTextoNPC(npcloc, msg.substring(1));
                                    NPC.destroy(NPCApi.getNpcID(npcloc));

                                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                        @Override
                                        public void run() {
                                            final String[] visao = NPCApi.getNpcVisao(npcloc).split(":");
                                            final Location location = new Location(Bukkit.getWorld(NPCApi.getWorldNpcHere(npcloc)), Double.parseDouble(locs[0]), Double.parseDouble(locs[1]), Double.parseDouble(locs[2]), Float.parseFloat(visao[0]), Float.parseFloat(visao[1]));
                                            NPC.updateInfoNpc(NPCApi.getNpcID(npcloc));

                                            player.sendMessage(ChatColor.GREEN + " * O título do NPC foi modificado.");
                                        }
                                    }, 6L);

                                    i = NPC.getLastID() + 10;
                                    this.has = true;
                                    break;
                                }
                            }
                        }

                        if (this.has == false) {
                            player.sendMessage(ChatColor.RED + "Para efetuar este comando você precisa estar olhando para algum NPC.");
                        }
                    }
                }

                if (args[0].equals("trocarskin")) {
                    if (args.length < 2 || args.length > 2) {
                        player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/npc trocarskin (playerskin)");
                        return false;
                    }
                    if (args.length == 2) {
                        final Location lastblock = player.getTargetBlock(null, 100).getLocation();
                        final Vector finalvector = lastblock.toVector();
                        final Vector firstvector = player.getEyeLocation().toVector();
                        final Vector substractedvector = finalvector.subtract(firstvector);

                        for (int i = 1; i <= NPC.getLastID(); i++) {
                            final BlockIterator BI = new BlockIterator(player.getLocation().getWorld(), firstvector, substractedvector, 0.0, ((Double) lastblock.distance(player.getLocation())).intValue());
                            while (BI.hasNext()) {
                                final org.bukkit.block.Block block = BI.next();

                                String loc1 = "";
                                String loc2 = "";
                                String loc3 = "";

                                if (block.getX() < 0 && block.getZ() < 0) {
                                    loc1 = (block.getX() + 1) + ":" + block.getY() + ":" + (block.getZ() + 1);
                                    loc2 = (block.getX() + 1) + ":" + (block.getY() - 1) + ":" + (block.getZ() + 1);
                                    loc3 = (block.getX() + 1) + ":" + (block.getY() - 2) + ":" + (block.getZ() + 1);
                                } else if (block.getX() > 0 && block.getZ() > 0) {
                                    loc1 = block.getX() + ":" + block.getY() + ":" + block.getZ();
                                    loc2 = block.getX() + ":" + (block.getY() - 1) + ":" + block.getZ();
                                    loc3 = block.getX() + ":" + (block.getY() - 2) + ":" + block.getZ();
                                } else if (block.getX() > 0 && block.getZ() < 0) {
                                    loc1 = block.getX() + ":" + block.getY() + ":" + (block.getZ() + 1);
                                    loc2 = block.getX() + ":" + (block.getY() - 1) + ":" + (block.getZ() + 1);
                                    loc3 = block.getX() + ":" + (block.getY() - 2) + ":" + (block.getZ() + 1);
                                } else if (block.getX() < 0 && block.getZ() > 0) {
                                    loc1 = (block.getX() + 1) + ":" + block.getY() + ":" + block.getZ();
                                    loc2 = (block.getX() + 1) + ":" + (block.getY() - 1) + ":" + block.getZ();
                                    loc3 = (block.getX() + 1) + ":" + (block.getY() - 2) + ":" + block.getZ();
                                }

                                final String npcloc = NPCApi.getLocationID(i);
                                final String[] locs = npcloc.split(":");
                                final double dx = Double.parseDouble(locs[0]);
                                final double dy = Double.parseDouble(locs[1]);
                                final double dz = Double.parseDouble(locs[2]);
                                final String loc = (int) dx + ":" + (int) dy + ":" + (int) dz;

                                if (loc1.equals(loc) || loc2.equals(loc) || loc3.equals(loc)) {
                                    if (PlayerUUID.getUUIDcache(args[1]) == null) {
                                        player.sendMessage(ChatColor.RED + "Ops, este usuário não existe no banco de dados do minecraft.");
                                        return false;
                                    }
                                    NPCApi.updateSkinNPC(npcloc, PlayerUUID.getUUIDcache(args[1]));
                                    NPC.destroy(NPCApi.getNpcID(npcloc));

                                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                        @Override
                                        public void run() {
                                            final String[] visao = NPCApi.getNpcVisao(npcloc).split(":");
                                            final Location location = new Location(Bukkit.getWorld(NPCApi.getWorldNpcHere(npcloc)), Double.parseDouble(locs[0]), Double.parseDouble(locs[1]), Double.parseDouble(locs[2]), Float.parseFloat(visao[0]), Float.parseFloat(visao[1]));
                                            NPC.updateInfoNpc(NPCApi.getNpcID(npcloc));

                                            player.sendMessage(ChatColor.GREEN + " * A skin do NPC foi alterada com sucesso.");
                                        }
                                    }, 6L);

                                    i = NPC.getLastID() + 10;
                                    this.has = true;
                                    break;
                                } else {
                                    this.has = false;
                                }
                            }
                        }

                        if (this.has == false) {
                            player.sendMessage(ChatColor.RED + "Para efetuar este comando você precisa estar olhando para algum NPC.");
                        }
                    }
                }

                if (args[0].equals("headrotation")) {
                    if (args.length > 2 || args.length < 2) {
                        player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/npc headrotation (on ou off)");
                        return false;
                    }
                    if (args.length == 2) {
                        final Location lastblock = player.getTargetBlock(null, 100).getLocation();
                        final Vector finalvector = lastblock.toVector();
                        final Vector firstvector = player.getEyeLocation().toVector();
                        final Vector substractedvector = finalvector.subtract(firstvector);

                        for (int i = 1; i <= NPC.getLastID(); i++) {
                            final BlockIterator BI = new BlockIterator(player.getLocation().getWorld(), firstvector, substractedvector, 0.0, ((Double) lastblock.distance(player.getLocation())).intValue());
                            while (BI.hasNext()) {
                                final org.bukkit.block.Block block = BI.next();

                                String loc1 = "";
                                String loc2 = "";
                                String loc3 = "";

                                if (block.getX() < 0 && block.getZ() < 0) {
                                    loc1 = (block.getX() + 1) + ":" + block.getY() + ":" + (block.getZ() + 1);
                                    loc2 = (block.getX() + 1) + ":" + (block.getY() - 1) + ":" + (block.getZ() + 1);
                                    loc3 = (block.getX() + 1) + ":" + (block.getY() - 2) + ":" + (block.getZ() + 1);
                                } else if (block.getX() > 0 && block.getZ() > 0) {
                                    loc1 = block.getX() + ":" + block.getY() + ":" + block.getZ();
                                    loc2 = block.getX() + ":" + (block.getY() - 1) + ":" + block.getZ();
                                    loc3 = block.getX() + ":" + (block.getY() - 2) + ":" + block.getZ();
                                } else if (block.getX() > 0 && block.getZ() < 0) {
                                    loc1 = block.getX() + ":" + block.getY() + ":" + (block.getZ() + 1);
                                    loc2 = block.getX() + ":" + (block.getY() - 1) + ":" + (block.getZ() + 1);
                                    loc3 = block.getX() + ":" + (block.getY() - 2) + ":" + (block.getZ() + 1);
                                } else if (block.getX() < 0 && block.getZ() > 0) {
                                    loc1 = (block.getX() + 1) + ":" + block.getY() + ":" + block.getZ();
                                    loc2 = (block.getX() + 1) + ":" + (block.getY() - 1) + ":" + block.getZ();
                                    loc3 = (block.getX() + 1) + ":" + (block.getY() - 2) + ":" + block.getZ();
                                }

                                final String npcloc = NPCApi.getLocationID(i);
                                final String[] locs = npcloc.split(":");
                                final double dx = Double.parseDouble(locs[0]);
                                final double dy = Double.parseDouble(locs[1]);
                                final double dz = Double.parseDouble(locs[2]);
                                final String loc = (int) dx + ":" + (int) dy + ":" + (int) dz;

                                if (loc1.equals(loc) || loc2.equals(loc) || loc3.equals(loc)) {
                                    if (args[1].equals("on")) {
                                        if (NPCApi.getHeadRotateNPC(npcloc) == true) {
                                            player.sendMessage(ChatColor.RED + "Ops, este NPC já esta com a movimentação de cabeça habilitada!");
                                        } else {
                                            NPCApi.updateHeadRotateNPC(npcloc, true);
                                            player.sendMessage(ChatColor.GREEN + " * Movimento de cabeça habilitado.");
                                            final RegionNpc terrain = new RegionNpc(String.valueOf(NPCApi.getNpcID(npcloc)), dx - 4, 0, dz - 4, dx + 4, 256, dz + 4, dy, NPCApi.getWorldNpcHere(npcloc));
                                            RegionsNpc.getTerrains().add(terrain);
                                        }
                                    } else if (args[1].equals("off")) {
                                        if (NPCApi.getHeadRotateNPC(npcloc) == false) {
                                            player.sendMessage(ChatColor.RED + "Ops, este NPC já esta com a movimentação de cabeça desabilitada!");
                                        } else {
                                            NPCApi.updateHeadRotateNPC(npcloc, false);
                                            player.sendMessage(ChatColor.RED + " * Movimento de cabeça desabilitado.");
                                            RegionsNpc.getTerrain(String.valueOf(NPCApi.getNpcID(npcloc))).setUUID("NULL");
                                            NPC.destroy(NPCApi.getNpcID(npcloc));

                                            final RegionNpc npc = RegionNpc.get(String.valueOf(NPCApi.getNpcID(npcloc)));
                                            RegionsNpc.getTerrains().remove(npc);

                                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                @Override
                                                public void run() {
                                                    final String[] visao = NPCApi.getNpcVisao(npcloc).split(":");
                                                    final Location location = new Location(Bukkit.getWorld(NPCApi.getWorldNpcHere(npcloc)), dx, dy, dz, Float.parseFloat(visao[0]), Float.parseFloat(visao[1]));
                                                    NPC.updateInfoNpc(NPCApi.getNpcID(npcloc));
                                                }
                                            }, 6L);
                                        }
                                    }

                                    i = NPC.getLastID() + 10;
                                    this.has = true;
                                    break;
                                } else {
                                    this.has = false;
                                }
                            }
                        }

                        if (this.has == false) {
                            player.sendMessage(ChatColor.RED + "Para efetuar este comando você precisa estar olhando para algum NPC.");
                        }
                    }
                }

                if (args[0].equals("mover")) {
                    if (args.length > 1 || args.length < 1) {
                        player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/npc mover");
                        return false;
                    }
                    if (args.length == 1) {
                        if (IDNpc.get(player.getName()).getId() == 0) {
                            final Location lastblock = player.getTargetBlock(null, 100).getLocation();
                            final Vector finalvector = lastblock.toVector();
                            final Vector firstvector = player.getEyeLocation().toVector();
                            final Vector substractedvector = finalvector.subtract(firstvector);

                            for (int i = 1; i <= NPC.getLastID(); i++) {
                                final BlockIterator BI = new BlockIterator(player.getLocation().getWorld(), firstvector, substractedvector, 0.0, ((Double) lastblock.distance(player.getLocation())).intValue());
                                while (BI.hasNext()) {
                                    final org.bukkit.block.Block block = BI.next();

                                    String loc1 = "";
                                    String loc2 = "";
                                    String loc3 = "";

                                    if (block.getX() < 0 && block.getZ() < 0) {
                                        loc1 = (block.getX() + 1) + ":" + block.getY() + ":" + (block.getZ() + 1);
                                        loc2 = (block.getX() + 1) + ":" + (block.getY() - 1) + ":" + (block.getZ() + 1);
                                        loc3 = (block.getX() + 1) + ":" + (block.getY() - 2) + ":" + (block.getZ() + 1);
                                    } else if (block.getX() > 0 && block.getZ() > 0) {
                                        loc1 = block.getX() + ":" + block.getY() + ":" + block.getZ();
                                        loc2 = block.getX() + ":" + (block.getY() - 1) + ":" + block.getZ();
                                        loc3 = block.getX() + ":" + (block.getY() - 2) + ":" + block.getZ();
                                    } else if (block.getX() > 0 && block.getZ() < 0) {
                                        loc1 = block.getX() + ":" + block.getY() + ":" + (block.getZ() + 1);
                                        loc2 = block.getX() + ":" + (block.getY() - 1) + ":" + (block.getZ() + 1);
                                        loc3 = block.getX() + ":" + (block.getY() - 2) + ":" + (block.getZ() + 1);
                                    } else if (block.getX() < 0 && block.getZ() > 0) {
                                        loc1 = (block.getX() + 1) + ":" + block.getY() + ":" + block.getZ();
                                        loc2 = (block.getX() + 1) + ":" + (block.getY() - 1) + ":" + block.getZ();
                                        loc3 = (block.getX() + 1) + ":" + (block.getY() - 2) + ":" + block.getZ();
                                    }

                                    final String npcloc = NPCApi.getLocationID(i);
                                    final String[] locs = npcloc.split(":");
                                    final double dx = Double.parseDouble(locs[0]);
                                    final double dy = Double.parseDouble(locs[1]);
                                    final double dz = Double.parseDouble(locs[2]);
                                    final String loc = (int) dx + ":" + (int) dy + ":" + (int) dz;

                                    if (loc1.equals(loc) || loc2.equals(loc) || loc3.equals(loc)) {
                                        new IDNpc(player.getName(), NPCApi.getNpcID(npcloc)).insert();
                                        player.sendMessage(ChatColor.GREEN + "Npc salvo com sucesso, digite novamente o comando para definir a nova posição do NPC.");

                                        i = NPC.getLastID() + 10;
                                        this.has = true;
                                        break;
                                    } else {
                                        this.has = false;
                                    }
                                }
                            }

                            if (this.has == false) {
                                player.sendMessage(ChatColor.RED + "Para efetuar este comando você precisa estar olhando para algum NPC.");
                            }
                        } else {
                            if (NPCApi.getHeadRotateNPCWithEntityID(IDNpc.get(player.getName()).getId()) == true) {
                                final String npcloc = NPCApi.getLocationEntityID(IDNpc.get(player.getName()).getId());
                                NPC.destroy(NPCApi.getNpcID(npcloc));

                                final RegionNpc npc = RegionNpc.get(String.valueOf(NPCApi.getNpcID(npcloc)));
                                RegionsNpc.getTerrains().remove(npc);

                                final String location = player.getLocation().getX() + ":" + player.getLocation().getY() + ":" + player.getLocation().getZ();
                                NPCApi.updateNPCLocation(npcloc, location);
                                final String visao = player.getLocation().getYaw() + ":" + player.getLocation().getPitch();
                                NPCApi.updateNPCVisao(npcloc, visao);

                                final RegionNpc newNpc = new RegionNpc(String.valueOf(NPCApi.getNpcID(location)), player.getLocation().getX() - 4, 0, player.getLocation().getZ() - 4, player.getLocation().getX() + 4, 256, player.getLocation().getZ() + 4, player.getLocation().getY(), player.getWorld().getName());
                                RegionsNpc.getTerrains().add(newNpc);

                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                    @Override
                                    public void run() {
                                        NPC.updateInfoNpc(NPCApi.getNpcID(location));
                                        player.sendMessage(ChatColor.GREEN + " * NPC Alterado de posição com sucesso.");
                                    }
                                }, 4L);
                            } else {
                                final String npcloc = NPCApi.getLocationEntityID(IDNpc.get(player.getName()).getId());
                                NPC.destroy(NPCApi.getNpcID(npcloc));

                                final RegionNpc npc = RegionNpc.get(String.valueOf(NPCApi.getNpcID(npcloc)));
                                RegionsNpc.getTerrains().remove(npc);


                                final String location = player.getLocation().getX() + ":" + player.getLocation().getY() + ":" + player.getLocation().getZ();
                                NPCApi.updateNPCLocation(npcloc, location);
                                final String visao = player.getLocation().getYaw() + ":" + player.getLocation().getPitch();
                                NPCApi.updateNPCVisao(location, visao);

                                final RegionNpc newNpc = new RegionNpc(String.valueOf(NPCApi.getNpcID(location)), player.getLocation().getX() - 4, 0, player.getLocation().getZ() - 4, player.getLocation().getX() + 4, 256, player.getLocation().getZ() + 4, player.getLocation().getY(), player.getWorld().getName());
                                RegionsNpc.getTerrains().add(newNpc);

                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                    @Override
                                    public void run() {
                                        NPC.updateInfoNpc(NPCApi.getNpcID(location));
                                        player.sendMessage(ChatColor.GREEN + " * NPC Alterado de posição com sucesso.");
                                    }
                                }, 6L);
                            }
                        }
                    }
                }

                if (args[0].equals("addfuncao")) {
                    if (args.length > 2 || args.length < 2) {
                        player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/npc addfuncao (inv:xxx / say:xxx / tp:xxx)");
                        return false;
                    }
                    if (args.length == 2) {
                        final Location lastblock = player.getTargetBlock(null, 100).getLocation();
                        final Vector finalvector = lastblock.toVector();
                        final Vector firstvector = player.getEyeLocation().toVector();
                        final Vector substractedvector = finalvector.subtract(firstvector);

                        for (int i = 1; i <= NPC.getLastID(); i++) {
                            final BlockIterator BI = new BlockIterator(player.getLocation().getWorld(), firstvector, substractedvector, 0.0, ((Double) lastblock.distance(player.getLocation())).intValue());
                            while (BI.hasNext()) {
                                final org.bukkit.block.Block block = BI.next();

                                String loc1 = "";
                                String loc2 = "";
                                String loc3 = "";

                                if (block.getX() < 0 && block.getZ() < 0) {
                                    loc1 = (block.getX() + 1) + ":" + block.getY() + ":" + (block.getZ() + 1);
                                    loc2 = (block.getX() + 1) + ":" + (block.getY() - 1) + ":" + (block.getZ() + 1);
                                    loc3 = (block.getX() + 1) + ":" + (block.getY() - 2) + ":" + (block.getZ() + 1);
                                } else if (block.getX() > 0 && block.getZ() > 0) {
                                    loc1 = block.getX() + ":" + block.getY() + ":" + block.getZ();
                                    loc2 = block.getX() + ":" + (block.getY() - 1) + ":" + block.getZ();
                                    loc3 = block.getX() + ":" + (block.getY() - 2) + ":" + block.getZ();
                                } else if (block.getX() > 0 && block.getZ() < 0) {
                                    loc1 = block.getX() + ":" + block.getY() + ":" + (block.getZ() + 1);
                                    loc2 = block.getX() + ":" + (block.getY() - 1) + ":" + (block.getZ() + 1);
                                    loc3 = block.getX() + ":" + (block.getY() - 2) + ":" + (block.getZ() + 1);
                                } else if (block.getX() < 0 && block.getZ() > 0) {
                                    loc1 = (block.getX() + 1) + ":" + block.getY() + ":" + block.getZ();
                                    loc2 = (block.getX() + 1) + ":" + (block.getY() - 1) + ":" + block.getZ();
                                    loc3 = (block.getX() + 1) + ":" + (block.getY() - 2) + ":" + block.getZ();
                                }

                                final String npcloc = NPCApi.getLocationID(i);
                                final String[] locs = npcloc.split(":");
                                final double dx = Double.parseDouble(locs[0]);
                                final double dy = Double.parseDouble(locs[1]);
                                final double dz = Double.parseDouble(locs[2]);
                                final String loc = (int) dx + ":" + (int) dy + ":" + (int) dz;

                                if (loc1.equals(loc) || loc2.equals(loc) || loc3.equals(loc)) {
                                    if (args[1].contains("inv:") || args[1].contains("say:") || args[1].contains("tp:") || args[1].contains("villager:")) {
                                        if (NPCFunctions.getFunctionExist(args[1]) != null) {
                                            if (NPCFunctions.getFunctionsWithEntityID(NPCApi.getNpcID(npcloc)).equals("NULL")) {
                                                NPCFunctions.updateFunctionNPC(NPCApi.getNpcID(npcloc), args[1]);
                                                player.sendMessage(ChatColor.GREEN + " * Função de NPC " + AlternateColor.alternate("&a&n") + NPCApi.getNpcID(npcloc) + ChatColor.GREEN + " definida com sucesso.");

                                                i = NPC.getLastID() + 10;
                                                this.has = true;
                                                break;
                                            } else {
                                                player.sendMessage(ChatColor.RED + "Ops este player já contém uma função, para remove lo utilize /npc removerfuncao");
                                                return false;
                                            }
                                        } else {
                                            player.sendMessage(ChatColor.RED + "Ops, está função não existe. Peça á algum membro do desenvolvimento para que crie está função.");
                                            return false;
                                        }
                                    } else {
                                        player.sendMessage(ChatColor.RED + "Ops, algo está errado. Verifique o comando utilizado e tente novamente.");
                                        return false;
                                    }
                                } else {
                                    this.has = false;
                                }
                            }
                        }

                        if (this.has == false) {
                            player.sendMessage(ChatColor.RED + "Para efetuar este comando você precisa estar olhando para algum NPC.");
                        }
                    }
                }

                if (args[0].equals("removerfuncao")) {
                    if (args.length > 1 || args.length < 1) {
                        player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/npc removerfuncao");
                        return false;
                    }
                    if (args.length == 1) {
                        final Location lastblock = player.getTargetBlock(null, 100).getLocation();
                        final Vector finalvector = lastblock.toVector();
                        final Vector firstvector = player.getEyeLocation().toVector();
                        final Vector substractedvector = finalvector.subtract(firstvector);

                        for (int i = 1; i <= NPC.getLastID(); i++) {
                            final BlockIterator BI = new BlockIterator(player.getLocation().getWorld(), firstvector, substractedvector, 0.0, ((Double) lastblock.distance(player.getLocation())).intValue());
                            while (BI.hasNext()) {
                                final org.bukkit.block.Block block = BI.next();

                                String loc1 = "";
                                String loc2 = "";
                                String loc3 = "";

                                if (block.getX() < 0 && block.getZ() < 0) {
                                    loc1 = (block.getX() + 1) + ":" + block.getY() + ":" + (block.getZ() + 1);
                                    loc2 = (block.getX() + 1) + ":" + (block.getY() - 1) + ":" + (block.getZ() + 1);
                                    loc3 = (block.getX() + 1) + ":" + (block.getY() - 2) + ":" + (block.getZ() + 1);
                                } else if (block.getX() > 0 && block.getZ() > 0) {
                                    loc1 = block.getX() + ":" + block.getY() + ":" + block.getZ();
                                    loc2 = block.getX() + ":" + (block.getY() - 1) + ":" + block.getZ();
                                    loc3 = block.getX() + ":" + (block.getY() - 2) + ":" + block.getZ();
                                } else if (block.getX() > 0 && block.getZ() < 0) {
                                    loc1 = block.getX() + ":" + block.getY() + ":" + (block.getZ() + 1);
                                    loc2 = block.getX() + ":" + (block.getY() - 1) + ":" + (block.getZ() + 1);
                                    loc3 = block.getX() + ":" + (block.getY() - 2) + ":" + (block.getZ() + 1);
                                } else if (block.getX() < 0 && block.getZ() > 0) {
                                    loc1 = (block.getX() + 1) + ":" + block.getY() + ":" + block.getZ();
                                    loc2 = (block.getX() + 1) + ":" + (block.getY() - 1) + ":" + block.getZ();
                                    loc3 = (block.getX() + 1) + ":" + (block.getY() - 2) + ":" + block.getZ();
                                }

                                final String npcloc = NPCApi.getLocationID(i);
                                final String[] locs = npcloc.split(":");
                                final double dx = Double.parseDouble(locs[0]);
                                final double dy = Double.parseDouble(locs[1]);
                                final double dz = Double.parseDouble(locs[2]);
                                final String loc = (int) dx + ":" + (int) dy + ":" + (int) dz;

                                if (loc1.equals(loc) || loc2.equals(loc) || loc3.equals(loc)) {
                                    if (!NPCFunctions.getFunctionsWithEntityID(NPCApi.getNpcID(npcloc)).equals("NULL")) {
                                        NPCFunctions.updateFunctionNPC(NPCApi.getNpcID(npcloc), "NULL");
                                        player.sendMessage(ChatColor.GREEN + " * Função de NPC " + AlternateColor.alternate("&a&n") + NPCApi.getNpcID(npcloc) + ChatColor.GREEN + " removido com sucesso.");

                                        i = NPC.getLastID() + 10;
                                        this.has = true;
                                        break;
                                    } else {
                                        player.sendMessage(ChatColor.RED + "Ops, este player já não contém função alguma.");
                                        return false;
                                    }
                                } else {
                                    this.has = false;
                                }
                            }
                        }

                        if (this.has == false) {
                            player.sendMessage(ChatColor.RED + "Para efetuar este comando você precisa estar olhando para algum NPC.");
                        }
                    }
                }
            }
        }
        return false;
    }
}
