package com.mixedup.npc;

import com.mixedup.Main;
import com.mixedup.MySql;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.hashs.IDprov;
import com.mixedup.npc.hashs.*;
import com.mixedup.utils.AlternateColor;
import com.mixedup.utils.Skin;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import net.minecraft.server.v1_13_R2.*;
import net.minecraft.server.v1_13_R2.PacketPlayOutEntity.PacketPlayOutEntityLook;
import net.minecraft.server.v1_13_R2.PacketPlayOutPlayerInfo.EnumPlayerInfoAction;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_13_R2.CraftServer;
import org.bukkit.craftbukkit.v1_13_R2.CraftWorld;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class NPC {

    public static void enableNPCS(final Player player) {
        if (NPC.getLastID() != 0) {
            for (int i = 1; i <= NPC.getLastID(); i++) {
                final int entityID = NPCApi.getNpcIDD(i);
                NPC.setCacheInfos(entityID);
                NPC.onEnableNpcs(entityID, player);
            }
        }
    }

    public static void disableNpcs(final String UUID, final Player player) {
        for (int i = 1; i <= NPC.getLastID(); i++) {
            if (NpcsIdsHash.get(UUID) != null) {
                final String[] split = NpcsIdsHash.get(UUID).getId().split(":");
                for (int x = 1; x <= split.length; x++) {
                    NPC.destroyUpd(Integer.valueOf(split[x - 1]), player);
                    NpcsIdsHash.CACHE.get(UUID);
                }
            }
        }
        try {
            final PreparedStatement st1 = MySql.con.prepareStatement("SELECT * FROM Player_npcs WHERE UUID = ?");
            st1.setString(1, UUID);
            final ResultSet rs = st1.executeQuery();
            while (rs.next()) {
                final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Player_npcs WHERE UUID = ?");
                st.setString(1, UUID);
                st.executeUpdate();
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateIDS(final int idremoved) {
        if (NPC.getLastID() > idremoved) {
            for (int i = idremoved + 1; i <= NPC.getLastID(); i++) {
                NPCApi.updateID(i);
            }
        }
    }

    public static void setCacheInfos(final int entityID) {
        if (NpcCacheLoc.get(String.valueOf(entityID)) == null) {
            new NpcCacheLoc(String.valueOf(entityID), NPCApi.getLocationEntityID(entityID)).insert();
        }
        if (NpcCacheHR.get(NpcCacheLoc.get(String.valueOf(entityID)).getLocation()) == null) {
            new NpcCacheHR(NpcCacheLoc.get(String.valueOf(entityID)).getLocation(), NPCApi.getHeadRotateNPCWithEntityID(entityID)).insert();
        }
        if (NpcCacheLastV.get(NpcCacheLoc.get(String.valueOf(entityID)).getLocation()) == null) {
            new NpcCacheLastV(NpcCacheLoc.get(String.valueOf(entityID)).getLocation(), NPCApi.getLastViewerNPC(NpcCacheLoc.get(String.valueOf(entityID)).getLocation())).insert();
        }
    }

    public static int getLastID() {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT max(ID) FROM Npcs_data");
            final ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void updateInfoNpc(final int npcID) {
        final String[] loc = NPCApi.getLocationEntityID(npcID).split(":");
        final String[] visao = NPCApi.getVisaoEntityID(npcID).split(":");
        final Location location = new Location(Bukkit.getWorld(NPCApi.getWorldEntityID(npcID)), Double.parseDouble(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Float.parseFloat(visao[0]), Float.parseFloat(visao[1]));

        final MinecraftServer nmsServer = ((CraftServer) Bukkit.getServer()).getServer();
        final WorldServer nmsWorld = ((CraftWorld) location.getWorld()).getHandle();
        final GameProfile gameProfile = new GameProfile(UUID.randomUUID(), AlternateColor.alternate(""));
        NPC.changeSkin(gameProfile, NPCApi.getUUIDNpcWithNPCId(npcID));
        final EntityPlayer npc = new EntityPlayer(nmsServer, nmsWorld, gameProfile, new PlayerInteractManager(nmsWorld));
        npc.setLocation(location.getX(), location.getY(), location.getZ(), (location.getYaw() * 256.0F) / 360.0F, location.getPitch());
        npc.getDataWatcher().set(new DataWatcherObject<>(13, DataWatcherRegistry.a), (byte) 0xFF);
        //new NpcsIdsHash(PlayerUUID.getUUID(player.getName()), npc.getId()).insert();

        for (final Player target : Bukkit.getOnlinePlayers()) {
            final PlayerConnection connection = ((CraftPlayer) target).getHandle().playerConnection;
            connection.sendPacket(new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER, npc));
            connection.sendPacket(new PacketPlayOutNamedEntitySpawn(npc));
            if (NPCApi.getTextoNpcWithNPCId(npcID).equalsIgnoreCase("&bTridentes")) {
                connection.sendPacket(new PacketPlayOutEntityEquipment(npc.getId(), EnumItemSlot.OFFHAND, CraftItemStack.asNMSCopy(new ItemStack(Material.TRIDENT))));
            }
            if (NPCApi.getTextoNpcWithNPCId(npcID).equalsIgnoreCase("&bArcanjo")) {
                connection.sendPacket(new PacketPlayOutEntityEquipment(npc.getId(), EnumItemSlot.CHEST, CraftItemStack.asNMSCopy(new ItemStack(Material.ELYTRA))));
                connection.sendPacket(new PacketPlayOutEntityEquipment(npc.getId(), EnumItemSlot.MAINHAND, CraftItemStack.asNMSCopy(new ItemStack(Material.IRON_SWORD))));
                connection.sendPacket(new PacketPlayOutEntityEquipment(npc.getId(), EnumItemSlot.OFFHAND, CraftItemStack.asNMSCopy(new ItemStack(Material.IRON_SWORD))));
            }
            NPC.headRotation(npc.getId(), location.getYaw(), location.getPitch());

            IDprov.get(target.getName() + npcID).setUUID("NULL");
            new IDprov(target.getName() + npcID, npc.getId()).insert();
        }

        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
            @Override
            public void run() {
                for (final Player target : Bukkit.getOnlinePlayers()) {
                    final PlayerConnection connection = ((CraftPlayer) target).getHandle().playerConnection;
                    connection.sendPacket(new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER, npc));
                }
            }
        }, 5L);
    }

    public static void createNPC(final Location location, final String npcUUID, final String msg) {
        final MinecraftServer nmsServer = ((CraftServer) Bukkit.getServer()).getServer();
        final WorldServer nmsWorld = ((CraftWorld) location.getWorld()).getHandle();
        final GameProfile gameProfile = new GameProfile(UUID.randomUUID(), "");
        NPC.changeSkin(gameProfile, npcUUID);
        final EntityPlayer npc = new EntityPlayer(nmsServer, nmsWorld, gameProfile, new PlayerInteractManager(nmsWorld));
        npc.setLocation(location.getX(), location.getY(), location.getZ(), (location.getYaw() * 256.0F) / 360.0F, location.getPitch());
        npc.getDataWatcher().set(new DataWatcherObject<>(13, DataWatcherRegistry.a), (byte) 0xFF);
        //new NpcsIdsHash(PlayerUUID.getUUID(player.getName()), npc.getId()).insert();

        final String loc = location.getX() + ":" + location.getY() + ":" + location.getZ();
        final String visao = location.getYaw() + ":" + location.getPitch();
        final Skin skin = new Skin(npcUUID);
        NPCApi.setNpc(NPC.getLastID() + 1, loc, npc.getId(), msg, npcUUID, location.getWorld().getName(), visao, false, "NULL", skin.getSkinValue(), skin.getSkinSignatur());

        for (final Player target : Bukkit.getOnlinePlayers()) {
            final PlayerConnection connection = ((CraftPlayer) target).getHandle().playerConnection;
            connection.sendPacket(new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER, npc));
            connection.sendPacket(new PacketPlayOutNamedEntitySpawn(npc));
            if (msg.equalsIgnoreCase("&bTridentes")) {
                connection.sendPacket(new PacketPlayOutEntityEquipment(npc.getId(), EnumItemSlot.OFFHAND, CraftItemStack.asNMSCopy(new ItemStack(Material.TRIDENT))));
            }
            if (msg.equalsIgnoreCase("&bArcanjo")) {
                connection.sendPacket(new PacketPlayOutEntityEquipment(npc.getId(), EnumItemSlot.CHEST, CraftItemStack.asNMSCopy(new ItemStack(Material.ELYTRA))));
                connection.sendPacket(new PacketPlayOutEntityEquipment(npc.getId(), EnumItemSlot.MAINHAND, CraftItemStack.asNMSCopy(new ItemStack(Material.IRON_SWORD))));
                connection.sendPacket(new PacketPlayOutEntityEquipment(npc.getId(), EnumItemSlot.OFFHAND, CraftItemStack.asNMSCopy(new ItemStack(Material.IRON_SWORD))));
            }
            NPC.headRotation(npc.getId(), location.getYaw(), location.getPitch());

            new IDprov(target.getName() + npc.getId(), npc.getId()).insert();
            NPCApi.setNpcIDProv(target.getName() + npc.getId(), npc.getId(), PlayerUUID.getUUID(target.getName()));
            new RegisterPlayerName(npc.getId(), target.getName()).insert();
        }

        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
            @Override
            public void run() {
                for (final Player target : Bukkit.getOnlinePlayers()) {
                    final PlayerConnection connection = ((CraftPlayer) target).getHandle().playerConnection;
                    connection.sendPacket(new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER, npc));
                }
            }
        }, 5L);
    }

    public static void onEnableNpcs(final int npcID, final Player player) {
        if (!NPCApi.getWorldEntityID(npcID).equalsIgnoreCase(player.getWorld().getName())) return;
        final String[] loc = NPCApi.getLocationEntityID(npcID).split(":");
        final String[] visao = NPCApi.getVisaoEntityID(npcID).split(":");
        final Location location = new Location(Bukkit.getWorld(NPCApi.getWorldEntityID(npcID)), Double.parseDouble(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Float.parseFloat(visao[0]), Float.parseFloat(visao[1]));

        final MinecraftServer nmsServer = ((CraftServer) Bukkit.getServer()).getServer();
        final WorldServer nmsWorld = ((CraftWorld) location.getWorld()).getHandle();
        final GameProfile gameProfile = new GameProfile(UUID.randomUUID(), AlternateColor.alternate(""));
        NPC.skinLoad(gameProfile, NPCApi.getValueSkin(NPCApi.getLocationEntityID(npcID)), NPCApi.getSignatureSkin(NPCApi.getLocationEntityID(npcID)));
        final EntityPlayer npc = new EntityPlayer(nmsServer, nmsWorld, gameProfile, new PlayerInteractManager(nmsWorld));
        npc.setLocation(location.getX(), location.getY(), location.getZ(), (location.getYaw() * 256.0F) / 360.0F, location.getPitch());
        npc.getDataWatcher().set(new DataWatcherObject<>(13, DataWatcherRegistry.a), (byte) 0xFF);
        if (NpcsIdsHash.get(PlayerUUID.getUUID(player.getName())) == null) {
            new NpcsIdsHash(PlayerUUID.getUUID(player.getName()), String.valueOf(npc.getId())).insert();
        } else {
            final String msg = NpcsIdsHash.get(PlayerUUID.getUUID(player.getName())).getId() + ":" + npc.getId();
            NpcsIdsHash.get(PlayerUUID.getUUID(player.getName())).setId(msg);
        }

        final PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
        connection.sendPacket(new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER, npc));
        connection.sendPacket(new PacketPlayOutNamedEntitySpawn(npc));
        if (NPCApi.getTextoNpcWithNPCId(npcID).equalsIgnoreCase("&bTridentes")) {
            connection.sendPacket(new PacketPlayOutEntityEquipment(npc.getId(), EnumItemSlot.OFFHAND, CraftItemStack.asNMSCopy(new ItemStack(Material.TRIDENT))));
        }
        if (NPCApi.getTextoNpcWithNPCId(npcID).equalsIgnoreCase("&bArcanjo")) {
            connection.sendPacket(new PacketPlayOutEntityEquipment(npc.getId(), EnumItemSlot.CHEST, CraftItemStack.asNMSCopy(new ItemStack(Material.ELYTRA))));
            connection.sendPacket(new PacketPlayOutEntityEquipment(npc.getId(), EnumItemSlot.MAINHAND, CraftItemStack.asNMSCopy(new ItemStack(Material.IRON_SWORD))));
            connection.sendPacket(new PacketPlayOutEntityEquipment(npc.getId(), EnumItemSlot.OFFHAND, CraftItemStack.asNMSCopy(new ItemStack(Material.IRON_SWORD))));
        }

        NPC.headRotation(npc.getId(), location.getYaw(), location.getPitch());

        new IDprov(player.getName() + npcID, npc.getId()).insert();
        NPCApi.setNpcIDProv(player.getName() + npc.getId(), npcID, PlayerUUID.getUUID(player.getName()));
        new RegisterPlayerName(npc.getId(), player.getName()).insert();

        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
            @Override
            public void run() {
                final PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
                connection.sendPacket(new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER, npc));
            }
        }, 5L);

        if (RegionNpc.get(String.valueOf(npcID)) == null) {
            final RegionNpc newNpc = new RegionNpc(String.valueOf(npcID), Double.parseDouble(loc[0]) - 4, 0, Double.parseDouble(loc[2]) - 4, Double.parseDouble(loc[0]) + 4, 256, Double.parseDouble(loc[2]) + 4, Double.parseDouble(loc[1]), location.getWorld().getName());
            RegionsNpc.getTerrains().add(newNpc);
        }
    }

    private static void changeSkin(final GameProfile profile, final String npcName) {
        final Skin skin = new Skin(npcName);
        profile.getProperties().put("textures", new Property("textures", skin.getSkinValue(), skin.getSkinSignatur()));
    }

    private static void skinLoad(final GameProfile profile, final String value, final String signature) {
        profile.getProperties().put("textures", new Property("textures", value, signature));
    }

    public static void teleport(final int entityID, final Location location) {
        final PacketPlayOutEntityTeleport packet = new PacketPlayOutEntityTeleport();
        NPC.setValue(packet, "a", entityID);
        NPC.setValue(packet, "b", NPC.getFixLocation(location.getX()));
        NPC.setValue(packet, "c", NPC.getFixLocation(location.getY()));
        NPC.setValue(packet, "d", NPC.getFixLocation(location.getZ()));
        NPC.setValue(packet, "e", NPC.getFixRotation(location.getYaw()));
        NPC.setValue(packet, "f", NPC.getFixRotation(location.getPitch()));

        NPC.sendPacket(packet);
        NPC.headRotation(entityID, location.getYaw(), location.getPitch());
    }


    public static void headRotation(final int entityID, final float yaw, final float pitch) {
        for (final Player target : Bukkit.getOnlinePlayers()) {
            if (IDprov.get(target.getName() + entityID) != null) {
                final int npcId = IDprov.get(target.getName() + entityID).getIDNpcPlayer();

                final PacketPlayOutEntityLook packet = new PacketPlayOutEntityLook(npcId, NPC.getFixRotation(yaw), NPC.getFixRotation(pitch), true);
                final PacketPlayOutEntityHeadRotation packetHead = new PacketPlayOutEntityHeadRotation();
                NPC.setValue(packetHead, "a", Integer.valueOf(npcId));
                NPC.setValue(packetHead, "b", Byte.valueOf(NPC.getFixRotation(yaw)));

                NPC.sendPacket(packet);
                NPC.sendPacket(packetHead);
            } else {
                final PacketPlayOutEntityLook packet = new PacketPlayOutEntityLook(entityID, NPC.getFixRotation(yaw), NPC.getFixRotation(pitch), true);
                final PacketPlayOutEntityHeadRotation packetHead = new PacketPlayOutEntityHeadRotation();
                NPC.setValue(packetHead, "a", Integer.valueOf(entityID));
                NPC.setValue(packetHead, "b", Byte.valueOf(NPC.getFixRotation(yaw)));

                NPC.sendPacket(packet);
                NPC.sendPacket(packetHead);
            }
        }
    }

    public static void destroy(final int entityID) {
        for (final Player target : Bukkit.getOnlinePlayers()) {
            if (IDprov.get(target.getName() + entityID) != null) {
                final int npcId = IDprov.get(target.getName() + entityID).getIDNpcPlayer();
                IDprov.get(target.getName() + entityID).setUUID("NULL");
                final PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(npcId);
                NPC.sendPacket(packet);
            }
        }
    }

    public static void destroyUpd(final int entityID, final Player player) {
        final PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(entityID);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
    }

    private static int getFixLocation(final double pos) {
        return MathHelper.floor(pos * 32.0D);
    }

    private static byte getFixRotation(final float yawpitch) {
        return (byte) ((int) (yawpitch * 256.0F / 360.0F));
    }

    private static void setValue(final Object obj, final String name, final Object value) {
        try {
            final Field field = obj.getClass().getDeclaredField(name);
            field.setAccessible(true);
            field.set(obj, value);
        } catch (final Exception e) {
        }
    }

    public static Object getValue(final Object obj, final String name) {
        try {
            final Field field = obj.getClass().getDeclaredField(name);
            field.setAccessible(true);
            return field.get(obj);
        } catch (final Exception e) {
        }
        return null;
    }

    private static void sendPacket(final Packet<?> packet, final Player player) {
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
    }

    private static void sendPacket(final Packet<?> packet) {
        for (final Player player : Bukkit.getOnlinePlayers()) {
            NPC.sendPacket(packet, player);
        }
    }
}
