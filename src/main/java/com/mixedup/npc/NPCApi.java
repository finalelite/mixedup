package com.mixedup.npc;

import com.mixedup.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class NPCApi {

    public static void setNpc(final int ID, final String Location, final int npcID, final String text, final String nick, final String world, final String visao, final boolean movehead, final String lastviewer, final String value, final String signature) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Npcs_data(ID, Location, EntityID, Texto, UUID, World, Visao, MoveHead, LastViewer, FunctionName, Value, Signature) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            st.setInt(1, ID);
            st.setString(2, Location);
            st.setInt(3, npcID);
            st.setString(4, text);
            st.setString(5, nick);
            st.setString(6, world);
            st.setString(7, visao);
            st.setBoolean(8, movehead);
            st.setString(9, lastviewer);
            st.setString(10, "NULL");
            st.setString(11, value);
            st.setString(12, signature);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeNpc(final int npcID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Npcs_data WHERE EntityID = ?");
            st.setInt(1, npcID);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getValueSkin(final String location) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE Location = ?");
            st.setString(1, location);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Value");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getSignatureSkin(final String location) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE Location = ?");
            st.setString(1, location);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Signature");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getLastViewerNPC(final String location) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE Location = ?");
            st.setString(1, location);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("LastViewer");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getIamLastViewerNPC(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE LastViewer = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("LastViewer");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getTextNpc(final String location) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE Location = ?");
            st.setString(1, location);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Texto");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getNickNpc(final String location) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE Location = ?");
            st.setString(1, location);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Nick");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int getNpcID(final String location) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE Location = ?");
            st.setString(1, location);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("EntityID");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static int getNpcIDD(final String location) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE Location = ?");
            st.setString(1, location);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("ID");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static int getNpcIDWithEntityID(final int EntityID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE EntityID = ?");
            st.setInt(1, EntityID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("ID");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getNpcVisao(final String location) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE Location = ?");
            st.setString(1, location);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Visao");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getNpcVisaoID(final int ID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE ID = ?");
            st.setInt(1, ID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Visao");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getExistNpcHere(final String location) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE Location = ?");
            st.setString(1, location);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Location");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getNpcsLocations() {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data");
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Location");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getWorldNpcHere(final String location) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE Location = ?");
            st.setString(1, location);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("World");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getWorldNpcID(final int ID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE ID = ?");
            st.setInt(1, ID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("World");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getTextNpcID(final int ID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE ID = ?");
            st.setInt(1, ID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Texto");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getUUIDNpcID(final int ID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE ID = ?");
            st.setInt(1, ID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("UUID");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getUUIDNpcWithNPCId(final int NpcID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE EntityID = ?");
            st.setInt(1, NpcID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("UUID");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getUUIDNpc(final String location) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE Location = ?");
            st.setString(1, location);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("UUID");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int getNpcIDD(final int ID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE ID = ?");
            st.setInt(1, ID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("EntityID");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getLocationID(final int ID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE ID = ?");
            st.setInt(1, ID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Location");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getLocationEntityID(final int EntityID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE EntityID = ?");
            st.setInt(1, EntityID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Location");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getVisaoEntityID(final int EntityID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE EntityID = ?");
            st.setInt(1, EntityID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Visao");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getWorldEntityID(final int EntityID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE EntityID = ?");
            st.setInt(1, EntityID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("World");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getTextoNpcWithNPCId(final int NpcID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE EntityID = ?");
            st.setInt(1, NpcID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Texto");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void updateLastViewerNPC(final String location, final String lastviewer) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Npcs_data SET LastViewer = ? WHERE Location = ?");
            st.setString(2, location);
            st.setString(1, lastviewer);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateID(final int ID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Npcs_data SET ID = ? WHERE ID = ?");
            st.setInt(2, ID);
            st.setInt(1, ID - 1);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeLastViewerNPC(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Npcs_data SET LastViewer = ? WHERE LastViewer = ?");
            st.setString(2, UUID);
            st.setString(1, "NULL");
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateTextoNPC(final String location, final String text) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Npcs_data SET Texto = ? WHERE Location = ?");
            st.setString(2, location);
            st.setString(1, text);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateSkinNPC(final String location, final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Npcs_data SET UUID = ? WHERE Location = ?");
            st.setString(2, location);
            st.setString(1, UUID);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateNPCId(final String location, final int newid) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Npcs_data SET EntityID = ? WHERE Location = ?");
            st.setString(2, location);
            st.setInt(1, newid);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean getHeadRotateNPC(final String location) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE Location = ?");
            st.setString(1, location);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("MoveHead");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean getHeadRotateNPCWithEntityID(final int EntityID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE EntityID = ?");
            st.setInt(1, EntityID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("MoveHead");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void updateHeadRotateNPC(final String location, final boolean movehead) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Npcs_data SET MoveHead = ? WHERE Location = ?");
            st.setString(2, location);
            st.setBoolean(1, movehead);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateNPCLocation(final String location, final String newloc) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Npcs_data SET Location = ? WHERE Location = ?");
            st.setString(2, location);
            st.setString(1, newloc);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateNPCVisao(final String location, final String visao) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Npcs_data SET Visao = ? WHERE Location = ?");
            st.setString(2, location);
            st.setString(1, visao);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static int getNpcIDProv(final String player) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Player_npcs WHERE Player = ?");
            st.setString(1, player);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("NpcID");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getNpcIDfromUUID(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Player_npcs WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Player");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void setNpcIDProv(final String player, final int npcID, final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Player_npcs (Player, NpcID, UUID) VALUES (?, ?, ?)");
            st.setString(1, player);
            st.setInt(2, npcID);
            st.setString(3, UUID);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeNpcCache(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Player_npcs WHERE UUID = ?");
            st.setString(1, UUID);
            st.executeUpdate();
        } catch (final SQLException e) {
            //-
        }
    }
}
