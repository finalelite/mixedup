package com.mixedup.npc.hashs;

import java.util.HashMap;

public class RegionNpc {

    private static final HashMap<String, RegionNpc> CACHE = new HashMap<String, RegionNpc>();
    private String UUID;
    private double p1x;
    private double p1y;
    private double p1z;
    private double p2x;
    private double p2y;
    private double p2z;
    private double py;
    private String world;

    public RegionNpc(final String UUID, final double p1x, final double p1y, final double p1z, final double p2x, final double p2y, final double p2z, final double py, final String world) {
        this.UUID = UUID;
        this.p1x = p1x;
        this.p1y = p1y;
        this.p1z = p1z;
        this.p2x = p2x;
        this.p2y = p2y;
        this.p2z = p2z;
        this.py = py;
        this.world = world;
    }

    public static RegionNpc get(final String UUID) {
        return RegionNpc.CACHE.get(UUID);
    }

    public RegionNpc insert() {
        RegionNpc.CACHE.put(UUID, this);

        return this;
    }

    public String getUUID() {
        return this.UUID;
    }

    public void setUUID(final String UUID) {
        this.UUID = UUID;
    }

    public String getWorld() {
        return this.world;
    }

    public void setWorld(final String world) {
        this.world = world;
    }

    public double getP1x() {
        return this.p1x;
    }

    public void setP1x(final double p1x) {
        this.p1x = p1x;
    }

    public double getPy() {
        return this.py;
    }

    public void setPy(final double py) {
        this.py = py;
    }

    public double getP1y() {
        return this.p1y;
    }

    public void setP1y(final double p1y) {
        this.p1y = p1y;
    }

    public double getP1z() {
        return this.p1z;
    }

    public void setP1z(final double p1z) {
        this.p1z = p1z;
    }

    public double getP2x() {
        return this.p2x;
    }

    public void setP2x(final double p2x) {
        this.p2x = p2x;
    }

    public double getP2y() {
        return this.p2y;
    }

    public void setP2y(final double p2y) {
        this.p2y = p2y;
    }

    public double getP2z() {
        return this.p2z;
    }

    public void setP2z(final double p2z) {
        this.p2z = p2z;
    }
}
