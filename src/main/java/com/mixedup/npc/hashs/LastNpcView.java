package com.mixedup.npc.hashs;

import java.util.HashMap;

public class LastNpcView {

    private static final HashMap<String, LastNpcView> CACHE = new HashMap<String, LastNpcView>();
    private String player;
    private String Location;

    public LastNpcView(final String player, final String Location) {
        this.player = player;
        this.Location = Location;
    }

    public static LastNpcView get(final String player) {
        return LastNpcView.CACHE.get(String.valueOf(player));
    }

    public static LastNpcView getIamLocation(final String Location) {
        return LastNpcView.CACHE.get(String.valueOf(Location));
    }

    public LastNpcView insert() {
        LastNpcView.CACHE.put(String.valueOf(player), this);

        return this;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(final String Location) {
        this.Location = Location;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(final String player) {
        this.player = player;
    }
}
