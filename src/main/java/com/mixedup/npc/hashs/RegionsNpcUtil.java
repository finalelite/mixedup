package com.mixedup.npc.hashs;

import org.bukkit.Location;

import java.util.List;

public class RegionsNpcUtil {

    public static RegionNpc playerInArea(final Location loc) {

        final List<RegionNpc> regionList = RegionsNpc.getTerrains();
        for (final RegionNpc rg : regionList) {
            if (!rg.getUUID().equals("NULL")) {

                final double p1x = rg.getP1x();
                final double p1y = rg.getP1y();
                final double p1z = rg.getP1z();
                final double p2x = rg.getP2x();
                final double p2y = rg.getP2y();
                final double p2z = rg.getP2z();

                final double minX = p1x < p2x ? p1x : p2x;
                final double minY = p1y < p2y ? p1y : p2y;
                final double minZ = p1z < p2z ? p1z : p2z;

                final double maxX = p1x > p2x ? p1x : p2x;
                final double maxY = p1y > p2y ? p1y : p2y;
                final double maxZ = p1z > p2z ? p1z : p2z;

                if ((loc.getBlockX() >= minX) && (loc.getBlockX() <= maxX) && (loc.getBlockY() >= minY)
                        && (loc.getBlockY() <= maxY) && (loc.getBlockZ() >= minZ) && (loc.getBlockZ() <= maxZ)) {

                    return rg;

                }
            }
        }
        return null;
    }

}
