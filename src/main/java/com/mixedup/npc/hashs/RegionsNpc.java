package com.mixedup.npc.hashs;

import java.util.ArrayList;
import java.util.List;

public class RegionsNpc {

    private static List<RegionNpc> terrain = new ArrayList<>();

    public static RegionNpc getTerrain(final String UUID) {
        for (final RegionNpc terrain : RegionsNpc.getTerrains()) {
            if (terrain.getUUID().equalsIgnoreCase(UUID)) {
                return terrain;
            }
        }
        return null;
    }

    public static List<RegionNpc> getTerrains() {
        return RegionsNpc.terrain;
    }

    public static void setTerrains(final List<RegionNpc> terrain) {
        RegionsNpc.terrain = terrain;
    }
}
