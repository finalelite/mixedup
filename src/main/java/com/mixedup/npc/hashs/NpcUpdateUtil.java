package com.mixedup.npc.hashs;

import java.util.HashMap;

public class NpcUpdateUtil {

    private static final HashMap<String, NpcUpdateUtil> CACHE = new HashMap<String, NpcUpdateUtil>();
    private final String UUID;
    private boolean status;

    public NpcUpdateUtil(final String UUID, final boolean status) {
        this.UUID = UUID;
        this.status = status;
    }

    public static NpcUpdateUtil get(final String UUID) {
        return NpcUpdateUtil.CACHE.get(String.valueOf(UUID));
    }

    public NpcUpdateUtil insert() {
        NpcUpdateUtil.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(final boolean status) {
        this.status = status;
    }

    public String getUUID() {
        return UUID;
    }
}
