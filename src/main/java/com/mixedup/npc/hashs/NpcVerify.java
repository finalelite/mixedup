package com.mixedup.npc.hashs;

import java.util.HashMap;

public class NpcVerify {

    private static final HashMap<String, NpcVerify> CACHE = new HashMap<String, NpcVerify>();
    private final String UUID;
    private boolean status;

    public NpcVerify(final String UUID, final boolean status) {
        this.UUID = UUID;
        this.status = status;
    }

    public static NpcVerify get(final String UUID) {
        return NpcVerify.CACHE.get(String.valueOf(UUID));
    }

    public NpcVerify insert() {
        NpcVerify.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(final boolean status) {
        this.status = status;
    }

    public String getUUID() {
        return UUID;
    }
}
