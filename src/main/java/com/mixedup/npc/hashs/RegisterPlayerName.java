package com.mixedup.npc.hashs;

import java.util.HashMap;

public class RegisterPlayerName {

    private static final HashMap<Integer, RegisterPlayerName> CACHE = new HashMap<Integer, RegisterPlayerName>();
    private int ID;
    private final String UUID;

    public RegisterPlayerName(final int ID, final String UUID) {
        this.ID = ID;
        this.UUID = UUID;
    }

    public static RegisterPlayerName get(final int ID) {
        return RegisterPlayerName.CACHE.get(Integer.valueOf(ID));
    }

    public RegisterPlayerName insert() {
        RegisterPlayerName.CACHE.put(Integer.valueOf(ID), this);

        return this;
    }

    public int getId() {
        return ID;
    }

    public void setId(final int ID) {
        this.ID = ID;
    }

    public String getUUID() {
        return UUID;
    }
}
