package com.mixedup.npc.hashs;

import java.util.HashMap;

public class NpcCacheLoc {

    private static final HashMap<String, NpcCacheLoc> CACHE = new HashMap<String, NpcCacheLoc>();
    private String ID;
    private String Location;

    public NpcCacheLoc(final String ID, final String Location) {
        this.ID = ID;
        this.Location = Location;
    }

    public static NpcCacheLoc get(final String ID) {
        return NpcCacheLoc.CACHE.get(String.valueOf(ID));
    }

    public NpcCacheLoc insert() {
        NpcCacheLoc.CACHE.put(String.valueOf(ID), this);

        return this;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(final String Location) {
        this.Location = Location;
    }

    public String getID() {
        return ID;
    }

    public void setID(final String ID) {
        this.ID = ID;
    }
}
