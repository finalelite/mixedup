package com.mixedup.npc.hashs;

import java.util.HashMap;

public class NpcCacheHR {

    private static final HashMap<String, NpcCacheHR> CACHE = new HashMap<String, NpcCacheHR>();
    private String Location;
    private boolean status;

    public NpcCacheHR(final String Location, final boolean status) {
        this.Location = Location;
        this.status = status;
    }

    public static NpcCacheHR get(final String Location) {
        return NpcCacheHR.CACHE.get(String.valueOf(Location));
    }

    public NpcCacheHR insert() {
        NpcCacheHR.CACHE.put(String.valueOf(Location), this);

        return this;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(final boolean status) {
        this.status = status;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(final String Location) {
        this.Location = Location;
    }
}
