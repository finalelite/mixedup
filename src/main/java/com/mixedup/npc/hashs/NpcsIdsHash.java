package com.mixedup.npc.hashs;

import java.util.HashMap;

public class NpcsIdsHash {

    public static HashMap<String, NpcsIdsHash> CACHE = new HashMap<String, NpcsIdsHash>();
    private String UUID;
    private String ID;

    public NpcsIdsHash(final String UUID, final String ID) {
        this.UUID = UUID;
        this.ID = ID;
    }

    public static NpcsIdsHash get(final String UUID) {
        return NpcsIdsHash.CACHE.get(String.valueOf(UUID));
    }

    public NpcsIdsHash insert() {
        NpcsIdsHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public String getId() {
        return ID;
    }

    public void setId(final String ID) {
        this.ID = ID;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(final String UUID) {
        this.UUID = UUID;
    }

}
