package com.mixedup.npc.hashs;

import java.util.HashMap;

public class NpcCacheLastV {

    private static final HashMap<String, NpcCacheLastV> CACHE = new HashMap<String, NpcCacheLastV>();
    private String Location;
    private String lastViewer;

    public NpcCacheLastV(final String Location, final String lastViewer) {
        this.Location = Location;
        this.lastViewer = lastViewer;
    }

    public static NpcCacheLastV get(final String Location) {
        return NpcCacheLastV.CACHE.get(String.valueOf(Location));
    }

    public NpcCacheLastV insert() {
        NpcCacheLastV.CACHE.put(String.valueOf(Location), this);

        return this;
    }

    public String getLastViewer() {
        return lastViewer;
    }

    public void setLastViewer(final String lastViewer) {
        this.lastViewer = lastViewer;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(final String Location) {
        this.Location = Location;
    }
}
