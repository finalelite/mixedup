package com.mixedup.npc;

import com.mixedup.MySql;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.npc.hashs.NpcUpdateUtil;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class NpcUpdates implements Listener {

    private static int getLastNpcID() {
        int last = 0;
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data ORDER BY ID");
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if (last < rs.getInt("ID") && rs.getInt("ID") != 0) {
                    last = rs.getInt("ID");
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return last;
    }

    @EventHandler
    public void onLeft(final PlayerQuitEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        NPC.disableNpcs(UUID, player);
    }

    // private static void updateNpc(String UUID, boolean recreate, Player player) {
    //        if (recreate == false) {
    //            for (int i = 1; i <= getLastNpcID(); i++) {
    //                NPCApi.removeNpcCache(UUID);
    //            }
    //        } else {
    //            for (int i = 1; i <= getLastNpcID(); i++) {
    //                try {
    //                    if (NPCApi.getNpcIDfromUUID(UUID) != null) {
    //                        int npcID = Integer.valueOf(NPCApi.getNpcIDfromUUID(UUID).replaceAll(player.getName(), ""));
    //                        NPC.destroyUpd(npcID);
    //                        NPCApi.removeNpcCache(UUID);
    //                    }
    //                } catch (NumberFormatException e) {
    //                    //-
    //                }
    //            }
    //        }
    //    }

    @EventHandler
    public void onTeleport(final PlayerTeleportEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (NpcUpdateUtil.get(UUID) == null) {
            NPC.disableNpcs(UUID, player);
            new NpcUpdateUtil(UUID, true).insert();
        } else {
            NPC.disableNpcs(UUID, player);
            NpcUpdateUtil.get(UUID).setStatus(true);
        }
    }
}
