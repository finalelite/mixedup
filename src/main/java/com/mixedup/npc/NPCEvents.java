package com.mixedup.npc;

import com.mixedup.Main;
import com.mixedup.npc.hashs.RegisterPlayerName;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import net.minecraft.server.v1_13_R2.Packet;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.util.List;

public class NPCEvents {

    private static Player player;
    private static Channel channel;
    private static boolean check;

    public NPCEvents(final Player player) {
        NPCEvents.player = player;
    }

    public void inject() {
        final CraftPlayer cPlayer = (CraftPlayer) player;
        NPCEvents.channel = cPlayer.getHandle().playerConnection.networkManager.channel;
        NPCEvents.channel.pipeline().addAfter("decoder", "PacketInjector", new MessageToMessageDecoder<Packet<?>>() {
            @Override
            protected void decode(final ChannelHandlerContext arg0, final Packet<?> packet, final List<Object> arg2) throws Exception {
                arg2.add(packet);
                NPCEvents.this.interact(packet);
            }
        });
    }

    public void uninject() {
        if (NPCEvents.channel.pipeline().get("PacketInjector") != null) {
            NPCEvents.channel.pipeline().remove("PacketInjector");
        }
    }

    public void setValue(final Object obj, final String name, final Object value) {
        try {
            final Field field = obj.getClass().getDeclaredField(name);
            field.setAccessible(true);
            field.set(obj, value);
        } catch (final Exception e) {
        }
    }

    public Object getValue(final Object obj, final String name) {
        try {
            final Field field = obj.getClass().getDeclaredField(name);
            field.setAccessible(true);
            return field.get(obj);
        } catch (final Exception e) {
        }
        return null;
    }

    public void interact(final Packet<?> packet) {
        if (packet.getClass().getSimpleName().equalsIgnoreCase("PacketPlayInUseEntity")) {
            try {
                final int ID = (Integer) this.getValue(packet, "a");

                final Player target = Bukkit.getPlayerExact(RegisterPlayerName.get(ID).getUUID());
                final int EntityID = NPCApi.getNpcIDProv(target.getName() + ID);

                if (NPCFunctions.getFunctionsWithEntityID(EntityID) != null) {
                    if (NPCFunctions.getFunctionsWithEntityID(EntityID).contains("inv:")) {
                        if (NPCEvents.check == false) {
                            //Inventory inv = (Inventory) NPCFunctions.getExistMethod(NPCFunctions.getFunctionsWithEntityID(EntityID), target);
                            //target.openInventory(inv);
                            NPCEvents.check = true;

                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    NPCEvents.check = false;
                                }
                            }, 20L);
                        }
                    }

                    if (NPCFunctions.getFunctionsWithEntityID(EntityID).contains("villager:encantar")) {
                        if (NPCEvents.check == false) {
                            //NPCFunctions.method(NPCFunctions.getFunctionsWithEntityID(EntityID), target);
                            NPCEvents.check = true;

                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    NPCEvents.check = false;
                                }
                            }, 20L);
                        }
                    }

                    if (NPCFunctions.getFunctionsWithEntityID(EntityID).contains("say:")) {
                        if (NPCEvents.check == false) {
                            //NPCFunctions.getExistMethod(NPCFunctions.getFunctionsWithEntityID(EntityID), target);
                            NPCEvents.check = true;

                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    NPCEvents.check = false;
                                }
                            }, 20L);
                        }
                    }

                    if (NPCFunctions.getFunctionsWithEntityID(EntityID).contains("tp:")) {
                        if (NPCEvents.check == false) {
                            //NPCFunctions.getExistMethod(NPCFunctions.getFunctionsWithEntityID(EntityID), target);
                            NPCEvents.check = true;

                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    NPCEvents.check = false;
                                }
                            }, 20L);
                        }
                    }
                }
            } catch (final Exception e) {

            }
        }
    }
}
