package com.mixedup.npc;

import com.mixedup.MySql;
import com.mixedup.apis.GuildaAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.managers.InventoryBanqueiro;
import com.mixedup.managers.InventoryElytra;
import com.mixedup.rankUp.InventoryRankUp;
import com.mixedup.utils.AlternateColor;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import org.bukkit.*;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Merchant;
import org.bukkit.inventory.MerchantRecipe;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NPCFunctions implements Listener {

    public static void invFerreiro(final Player player) {
        final Merchant merchant = Bukkit.createMerchant("Ancião de trocas");

        final List<MerchantRecipe> merchantRecipes = new ArrayList<MerchantRecipe>();

        final ItemStack buyItem1 = new ItemStack(Material.BOOK);
        final ItemMeta meta4 = buyItem1.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Livro de encantamentos");
        final List<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.DARK_GRAY + "Encantamentos: " + ChatColor.GRAY + "Básicos");
        lore4.add(" ");
        meta4.setLore(lore4);
        buyItem1.setItemMeta(meta4);

        final ItemStack buyItem2 = new ItemStack(Material.BOOK);
        final ItemMeta meta5 = buyItem2.getItemMeta();
        meta5.setDisplayName(ChatColor.YELLOW + "Livro de encantamentos");
        final List<String> lore5 = new ArrayList<>();
        lore5.add(" ");
        lore5.add(ChatColor.DARK_GRAY + "Encantamentos: " + ChatColor.GRAY + "Médios");
        lore5.add(" ");
        meta5.setLore(lore5);
        buyItem2.setItemMeta(meta5);

        final ItemStack buyItem3 = new ItemStack(Material.BOOK);
        final ItemMeta meta6 = buyItem3.getItemMeta();
        meta6.setDisplayName(ChatColor.YELLOW + "Livro de encantamentos");
        final List<String> lore6 = new ArrayList<>();
        lore6.add(" ");
        lore6.add(ChatColor.DARK_GRAY + "Encantamentos: " + ChatColor.GRAY + "Avançados");
        lore6.add(" ");
        meta6.setLore(lore6);
        buyItem3.setItemMeta(meta6);

        final MerchantRecipe recipe = new MerchantRecipe(buyItem1, 10000);
        final MerchantRecipe recipe1 = new MerchantRecipe(buyItem2, 10000);
        final MerchantRecipe recipe2 = new MerchantRecipe(buyItem3, 10000);
        recipe.setExperienceReward(false);
        recipe1.setExperienceReward(false);
        recipe2.setExperienceReward(false);

        final ItemStack papel = new ItemStack(Material.PAPER);
        final ItemMeta meta = papel.getItemMeta();
        meta.setDisplayName(ChatColor.YELLOW + "Papiro com escritos");
        final List<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.YELLOW + "Escritos: " + ChatColor.DARK_GRAY + "Comum");
        lore.add(" ");
        meta.setLore(lore);
        papel.setItemMeta(meta);
        recipe.addIngredient(papel);

        final ItemStack papel1 = new ItemStack(Material.PAPER);
        final ItemMeta meta1 = papel1.getItemMeta();
        meta1.setDisplayName(ChatColor.YELLOW + "Papiro com escritos");
        final List<String> lore1 = new ArrayList<>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Escritos: " + ChatColor.DARK_GRAY + "Raro");
        lore1.add(" ");
        meta1.setLore(lore1);
        papel1.setItemMeta(meta1);
        recipe1.addIngredient(papel1);

        final ItemStack papel2 = new ItemStack(Material.PAPER);
        final ItemMeta meta2 = papel2.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "Papiro com escritos");
        final List<String> lore2 = new ArrayList<>();
        lore2.add(" ");
        lore2.add(ChatColor.YELLOW + "Escritos: " + ChatColor.DARK_GRAY + "Super raro");
        lore2.add(" ");
        meta2.setLore(lore2);
        papel2.setItemMeta(meta2);
        recipe2.addIngredient(papel2);

        merchantRecipes.add(recipe);
        merchantRecipes.add(recipe1);
        merchantRecipes.add(recipe2);

        merchant.setRecipes(merchantRecipes);

        player.openMerchant(merchant, true);
    }

    private static void npcMsgsInicio(final Player player) {

    }

    //public static Object getExistMethod(String method, Player player) {
    //
    //        for (int i = 1; i <= getLastFunctionName(); i++) {
    //            if (getFunctionsWithID(i).equals(method)) {
    //                if (method.equals("inv:guildaselect")) {
    //                    if (GuildaAPI.getGuilda(PlayerUUID.getUUID(player.getName())) == null) {
    //                        return invGuildaSelect();
    //                    } else {
    //                        if (GuildaAPI.getGuildaRank(PlayerUUID.getUUID(player.getName())) < 5) {
    //                            return InventoryRankUp.getInventory(PlayerUUID.getUUID(player.getName()));
    //                        } else {
    //                            player.sendMessage(ChatColor.RED + " * Ops, você já antigiu o nível máximo da guilda.");
    //                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
    //                        }
    //                    }
    //                }
    //                if (method.equals("inv:lojablocos")) {
    //                    return InventoryLoja.buildLojaBlocos1();
    //                }
    //                if (method.equals("inv:lojadecorativos")) {
    //                    return InventoryLoja.buildLojaDecorativos1();
    //                }
    //                if (method.equals("inv:lojaarmas")) {
    //                    return InventoryLoja.buildLojaTnts();
    //                }
    //                if (method.equals("inv:lojamateriais")) {
    //                    return InventoryLoja.buildLojaMateriais1();
    //                }
    //                if (method.equals("inv:lojacomidas")) {
    //                    return InventoryLoja.buildLojaComidas();
    //                }
    //                if (method.equals("inv:lojaredstone")) {
    //                    return InventoryLoja.buildLojaRedstone();
    //                }
    //                if (method.equals("inv:lojaalquimia")) {
    //                    return InventoryLoja.buildLojaAlquimia();
    //                }
    //                if (method.equals("inv:lojavarios")) {
    //                    return InventoryLoja.buildLojaVarios();
    //                }
    //                if (method.equals("inv:lojacoloridos")) {
    //                    return InventoryLoja.buildLojaColoridos();
    //                }
    //                if (method.equals("inv:lojamobspawners")) {
    //                    return InventoryLoja.buildLojaSpawners();
    //                }
    //                if (method.equals("inv:lojacaixas")) {
    //                    return InventoryLoja.buildLojaCaixas();
    //                }
    //                if (method.equals("inv:lojapaineis")) {
    //                    return InventoryLoja.buildLojaPainelSolar();
    //                }
    //                if (method.equals("inv:elytra")) {
    //                    return InventoryElytra.firstInv();
    //                }
    //                if (method.equals("inv:banqueiro")) {
    //                    return InventoryBanqueiro.getInventory(PlayerUUID.getUUID(player.getName()));
    //                }
    //                if (method.equals("say:npcinicio")) {
    //                    npcMsgsInicio(player);
    //                }
    //                if (method.equals("inv:tridente")) {
    //                    return InventoryTrident.inv();
    //                }
    //                if (method.equals("inv:lojavip")) {
    //                    return com.mixedup.managers.InventoryLoja.inv();
    //                }
    //
    //                i = getLastFunctionName() + 10;
    //                break;
    //            }
    //        }
    //        return null;
    //    }

    private static Inventory invGuildaSelect() {
        final Inventory inv = Bukkit.createInventory(null, 27, "Selecione sua Guilda: ");

        final ItemStack i = new ItemStack(Material.LEGACY_BANNER);
        final BannerMeta m = (BannerMeta) i.getItemMeta();
        m.setDisplayName(ChatColor.YELLOW + "Guilda sanguinaria.");
        final ArrayList<String> lore = new ArrayList<String>();
        lore.add(" ");
        lore.add(ChatColor.GRAY + "Você gosta de algo mais frenético, e pouco trabalho?");
        lore.add(ChatColor.GRAY + "Então esta é sua melhor opção, apenas guerreiros");
        lore.add(ChatColor.GRAY + "e os mais fortes resistem a esta guilda, mas não");
        lore.add(ChatColor.GRAY + "vá pensando que tudo será moleza, para servir a");
        lore.add(ChatColor.GRAY + "esta Guilda você precisa de muita força e paciência.");
        lore.add(" ");
        lore.add(ChatColor.YELLOW + "Estilo: " + ChatColor.GRAY + "Factions, Rank Up, RPG, Hardcore e Futurista.");
        lore.add(" ");
        m.setLore(lore);
        m.setBaseColor(DyeColor.GRAY);
        final List<Pattern> patterns = new ArrayList<Pattern>();
        patterns.add(new Pattern(DyeColor.RED, PatternType.STRIPE_BOTTOM));
        patterns.add(new Pattern(DyeColor.GRAY, PatternType.DIAGONAL_RIGHT_MIRROR));
        patterns.add(new Pattern(DyeColor.RED, PatternType.STRIPE_TOP));
        patterns.add(new Pattern(DyeColor.GRAY, PatternType.RHOMBUS_MIDDLE));
        patterns.add(new Pattern(DyeColor.RED, PatternType.STRIPE_DOWNRIGHT));
        patterns.add(new Pattern(DyeColor.GRAY, PatternType.BORDER));
        m.setPatterns(patterns);
        m.addItemFlags(org.bukkit.inventory.ItemFlag.HIDE_ATTRIBUTES);
        m.addItemFlags(org.bukkit.inventory.ItemFlag.HIDE_DESTROYS);
        m.addItemFlags(org.bukkit.inventory.ItemFlag.HIDE_ENCHANTS);
        m.addItemFlags(org.bukkit.inventory.ItemFlag.HIDE_PLACED_ON);
        m.addItemFlags(org.bukkit.inventory.ItemFlag.HIDE_POTION_EFFECTS);
        m.addItemFlags(org.bukkit.inventory.ItemFlag.HIDE_UNBREAKABLE);
        i.setItemMeta(m);

        final ItemStack i2 = new ItemStack(Material.LEGACY_BANNER);
        final BannerMeta m2 = (BannerMeta) i2.getItemMeta();
        m2.setDisplayName(ChatColor.YELLOW + "Guilda anciã.");
        final ArrayList<String> lore2 = new ArrayList<String>();
        lore2.add(" ");
        lore2.add(ChatColor.GRAY + "Você é o tipo de pessoa que basta utilizar sua");
        lore2.add(ChatColor.GRAY + "inteligência para conquistar seus objetivos?");
        lore2.add(ChatColor.GRAY + "Então esta Guilda é para pessoas como você,");
        lore2.add(ChatColor.GRAY + "muitos acham que força é tudo, mas as vezes");
        lore2.add(ChatColor.GRAY + "usar metade de um neuronio é melhor do que");
        lore2.add(ChatColor.GRAY + "gastar suas forças.");
        lore2.add(" ");
        lore2.add(ChatColor.YELLOW + "Estilo: " + ChatColor.GRAY + "Factions, Rank Up, Skyblock e RPG.");
        lore2.add(" ");
        m2.setLore(lore2);
        m2.setBaseColor(DyeColor.GRAY);
        final List<Pattern> patterns2 = new ArrayList<Pattern>();
        patterns2.add(new Pattern(DyeColor.BLUE, PatternType.STRIPE_LEFT));
        patterns2.add(new Pattern(DyeColor.BLUE, PatternType.STRIPE_RIGHT));
        patterns2.add(new Pattern(DyeColor.BLUE, PatternType.STRIPE_TOP));
        patterns2.add(new Pattern(DyeColor.BLUE, PatternType.STRIPE_MIDDLE));
        patterns2.add(new Pattern(DyeColor.GRAY, PatternType.BORDER));
        m2.setPatterns(patterns2);
        m2.addItemFlags(org.bukkit.inventory.ItemFlag.HIDE_ATTRIBUTES);
        m2.addItemFlags(org.bukkit.inventory.ItemFlag.HIDE_DESTROYS);
        m2.addItemFlags(org.bukkit.inventory.ItemFlag.HIDE_ENCHANTS);
        m2.addItemFlags(org.bukkit.inventory.ItemFlag.HIDE_PLACED_ON);
        m2.addItemFlags(org.bukkit.inventory.ItemFlag.HIDE_POTION_EFFECTS);
        m2.addItemFlags(org.bukkit.inventory.ItemFlag.HIDE_UNBREAKABLE);
        i2.setItemMeta(m2);

        final ItemStack i3 = new ItemStack(Material.LEGACY_BANNER);
        final BannerMeta m3 = (BannerMeta) i3.getItemMeta();
        m3.setDisplayName(ChatColor.YELLOW + "Guilda nobre.");
        final ArrayList<String> lore3 = new ArrayList<String>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Você gosta de mexer com terra, criar as coisas do");
        lore3.add(ChatColor.GRAY + "0, botar a mão na massa e preparar as coisas por");
        lore3.add(ChatColor.GRAY + "conta própria sem necessitar de ajuda de ninguém?");
        lore3.add(ChatColor.GRAY + "Pessoas como você são necessárias nesta Guilda,");
        lore3.add(ChatColor.GRAY + "pois aqui não a tempo para preguiça, esforço é o");
        lore3.add(ChatColor.GRAY + "nosso dilema. Pegue um machado e começe a trabalhar.");
        lore3.add(" ");
        lore3.add(ChatColor.YELLOW + "Estilo: " + ChatColor.GRAY + "Factions, Rank Up, survival e RPG.");
        lore3.add(" ");
        m3.setLore(lore3);
        m3.setBaseColor(DyeColor.GRAY);
        final List<Pattern> patterns3 = new ArrayList<Pattern>();
        patterns3.add(new Pattern(DyeColor.GREEN, PatternType.STRIPE_LEFT));
        patterns3.add(new Pattern(DyeColor.GREEN, PatternType.STRIPE_RIGHT));
        patterns3.add(new Pattern(DyeColor.GREEN, PatternType.STRIPE_DOWNRIGHT));
        patterns3.add(new Pattern(DyeColor.GRAY, PatternType.BORDER));
        m3.setPatterns(patterns3);
        m3.addItemFlags(org.bukkit.inventory.ItemFlag.HIDE_ATTRIBUTES);
        m3.addItemFlags(org.bukkit.inventory.ItemFlag.HIDE_DESTROYS);
        m3.addItemFlags(org.bukkit.inventory.ItemFlag.HIDE_ENCHANTS);
        m3.addItemFlags(org.bukkit.inventory.ItemFlag.HIDE_PLACED_ON);
        m3.addItemFlags(org.bukkit.inventory.ItemFlag.HIDE_POTION_EFFECTS);
        m3.addItemFlags(org.bukkit.inventory.ItemFlag.HIDE_UNBREAKABLE);
        i3.setItemMeta(m3);

        inv.setItem(11, i);
        inv.setItem(13, i2);
        inv.setItem(15, i3);

        return inv;
    }

    public static int getLastFunctionName() {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT max(ID) FROM Npc_functions");
            final ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getFunctionsWithID(final int id) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npc_functions WHERE ID = ?");
            st.setInt(1, id);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("FunctionName");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getFunctionExist(final String function) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npc_functions WHERE FunctionName = ?");
            st.setString(1, function);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("FunctionName");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getFunctionsWithEntityID(final int EntityID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE EntityID = ?");
            st.setInt(1, EntityID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("FunctionName");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void updateFunctionNPC(final int EntityID, final String function) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Npcs_data SET FunctionName = ? WHERE EntityID = ?");
            st.setInt(2, EntityID);
            st.setString(1, function);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    @EventHandler
    public void onClickNpc(final NPCRightClickEvent event) {
        if (event.getNPC().getId() == 16 || event.getNPC().getId() == 24) {
            invFerreiro(event.getClicker());
        }
        if (event.getNPC().getFullName().equalsIgnoreCase(ChatColor.RED + "Ferreiro")) {
            event.getClicker().openInventory(InventoryElytra.firstInv());
        }
        if (event.getNPC().getId() == 17) {
            event.getClicker().openInventory(InventoryBanqueiro.getInventory(PlayerUUID.getUUID(event.getClicker().getName())));
        }
        if (event.getNPC().getFullName().equalsIgnoreCase(AlternateColor.alternate2("&bLoja")) || event.getNPC().getId() == 23) {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "lojaabrir " + event.getClicker().getName());
        }
        if (event.getNPC().getFullName().equalsIgnoreCase(ChatColor.GREEN + "Sábio")) {
            if (GuildaAPI.getGuilda(PlayerUUID.getUUID(event.getClicker().getName())) == null) {
                event.getClicker().openInventory(NPCFunctions.invGuildaSelect());
            } else {
                if (GuildaAPI.getGuildaRank(PlayerUUID.getUUID(event.getClicker().getName())) < 5) {
                    event.getClicker().openInventory(InventoryRankUp.getInventory(PlayerUUID.getUUID(event.getClicker().getName())));
                } else {
                    event.getClicker().sendMessage(ChatColor.RED + " * Ops, você já antigiu o nível máximo da guilda.");
                    event.getClicker().playSound(event.getClicker().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
    }
}
