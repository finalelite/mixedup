package com.mixedup.vipAutentication;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum VIPRole {
    TITAN,
    DUQUE,
    LORD,
    CONDE;

    public static VIPRole fromVIPName(final String name) {
        try {
            return valueOf(name.toUpperCase());
        } catch (final IllegalArgumentException e) {
            return null;
        }
    }

    public static VIPRole fromId(final byte id) {
        return VIPRole.values()[id];
    }

}
