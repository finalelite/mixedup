package com.mixedup.vipAutentication;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandAtivar implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;

        if (cmd.getName().equalsIgnoreCase("ativar")) {
            Authentication.authNoTime(player.getUniqueId().toString());
        }
        return false;
    }
}
