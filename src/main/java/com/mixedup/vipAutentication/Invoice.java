package com.mixedup.vipAutentication;


import lombok.Getter;

@Getter
public class Invoice {

    private final VIPRole vip;
    private final InvoiceType type;

    public Invoice(final int price) {
        switch (price) {
            // TITAN
            case 15:
                this.vip = VIPRole.TITAN;
                this.type = InvoiceType.ONE_MONTH;
                break;
            case 16:
                this.vip = VIPRole.TITAN;
                this.type = InvoiceType.TWO_MONTHS;
                break;
            case 17:
                this.vip = VIPRole.TITAN;
                this.type = InvoiceType.ETERNAL;
                break;
            // Duque
            case 12:
                this.vip = VIPRole.DUQUE;
                this.type = InvoiceType.ONE_MONTH;
                break;
            case 13:
                this.vip = VIPRole.DUQUE;
                this.type = InvoiceType.TWO_MONTHS;
                break;
            case 14:
                this.vip = VIPRole.DUQUE;
                this.type = InvoiceType.ETERNAL;
                break;
            // Lord
            case 6:
                this.vip = VIPRole.LORD;
                this.type = InvoiceType.ONE_MONTH;
                break;
            case 9:
                this.vip = VIPRole.LORD;
                this.type = InvoiceType.TWO_MONTHS;
                break;
            case 11:
                this.vip = VIPRole.LORD;
                this.type = InvoiceType.ETERNAL;
                break;
            // Conde
            case 7:
                this.vip = VIPRole.CONDE;
                this.type = InvoiceType.ONE_MONTH;
                break;
            case 8:
                this.vip = VIPRole.CONDE;
                this.type = InvoiceType.TWO_MONTHS;
                break;
            case 10:
                this.vip = VIPRole.CONDE;
                this.type = InvoiceType.ETERNAL;
                break;

            default:
                this.vip = null;
                this.type = null;
        }
    }

}
