package com.mixedup.vipAutentication;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter @Setter @AllArgsConstructor
public class VipData {

    private Date end;
    private boolean eternal;

}
