package com.mixedup.vipAutentication;

import com.mixedup.MySqlHub;
import lombok.val;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AuthAPI {

    public static void createIndo(final String UUID, final String vip, final String server, final boolean eterno, final Date date) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("INSERT INTO Vips_data(UUID, Vip, Server, Eterno, Ends) VALUES (?, ?, ?, ?, ?)");
            st.setString(1, UUID);
            st.setString(2, vip);
            st.setString(3, server);
            st.setBoolean(4, eterno);
            st.setDate(5, date);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeVip(final String UUID) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("DELETE FROM Vips_data WHERE UUID = ?");
            st.setString(1, UUID);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean getEterno(final String UUID) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Vips_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Eterno");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static Date getDateEnds(final String UUID) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Vips_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getDate("Ends");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getTagVip(final String UUID) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Vips_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Vip");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getExistInfo(final String UUID) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Vips_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("UUID");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean getVip(final String nick) {
        return AuthAPI.getPaid(AuthAPI.getUserId(nick));
    }

    private static int getUserId(final String nick) {
        try {
            final PreparedStatement st = MySqlWeb.con.prepareStatement("SELECT * FROM users WHERE username = ?");
            st.setString(1, nick);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("id");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void removeVipWeb(final String nick) {
        AuthAPI.removePaid(AuthAPI.getUserId(nick));
    }

    private static void removePaid(final int id) {
        try {
            final PreparedStatement st = MySqlWeb.con.prepareStatement("DELETE FROM invoices WHERE user_id = ?");
            st.setInt(1, id);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    private static boolean getPaid(final int id) {
        boolean back = false;
        try {
            final PreparedStatement st = MySqlWeb.con.prepareStatement("SELECT * FROM invoices WHERE user_id = ?");
            st.setInt(1, id);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if (back == false && rs.getBoolean("paid") == true) {
                    back = true;
                    break;
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return back;
    }

    private static int getPriceId(final int id) {
        try {
            final PreparedStatement st = MySqlWeb.con.prepareStatement("SELECT * FROM invoices WHERE user_id = ?");
            st.setInt(1, id);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if (rs.getBoolean("paid") == true) {
                    return rs.getInt("price_id");
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private static Float getTotal(final int id) {
        try {
            final PreparedStatement st = MySqlWeb.con.prepareStatement("SELECT * FROM invoices WHERE user_id = ?");
            st.setInt(1, id);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if (rs.getBoolean("paid") == true) {
                    return rs.getFloat("total");
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0F;
    }

    private static int getProductId(final Float total) {
        try {
            final PreparedStatement st = MySqlWeb.con.prepareStatement("SELECT * FROM prices WHERE total = ?");
            st.setFloat(1, total);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("product_id");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private static String getProductTime(final Float total) {
        try {
            final PreparedStatement st = MySqlWeb.con.prepareStatement("SELECT * FROM prices WHERE total = ?");
            st.setFloat(1, total);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("name");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String getProductType(final int productId) {
        try {
            final PreparedStatement st = MySqlWeb.con.prepareStatement("SELECT * FROM products WHERE id = ?");
            st.setInt(1, productId);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("name");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //GETUSERID(24) > TOTAL(20) > PRODUCTID(14) > PRODUCTS(CONDE)
    public static String getProduct(final String nick) {
        final val priceId = AuthAPI.getPriceId(AuthAPI.getUserId(nick));

        final val invoice = new Invoice(priceId);
        return br.com.finalelite.pauloo27.api.message.MessageUtils.capitalize(invoice.getVip().name().toLowerCase()) + " " + invoice.getType().toPtBR();
    }
}
