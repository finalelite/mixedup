package com.mixedup.vipAutentication;

import br.com.finalelite.pauloo27.api.PauloAPI;
import com.mixedup.Main;
import com.mixedup.apis.AccountAPI;
import com.mixedup.apis.TagAPI;
import com.mixedup.economy.CoinsAPI;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.sql.Date;
import java.util.UUID;

public class Authentication {

    public static void auth(final String uuid) {
        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
            @Override
            public void run() {
                if (TagAPI.getTag(uuid).equalsIgnoreCase("membro")) {
                    if (AuthAPI.getVip(AccountAPI.getNick(uuid)) == true) {
                        if (AuthAPI.getExistInfo(uuid) == null) {
                            if (AuthAPI.getProduct(AccountAPI.getNick(uuid)).contains("Titan")) {
                                if (AuthAPI.getProduct(AccountAPI.getNick(uuid)).contains("Eterno")) {
                                    AuthAPI.createIndo(uuid, "Vip+++", "MixedUP", true, null);
                                } else if (AuthAPI.getProduct(AccountAPI.getNick(uuid)).contains("1")) {
                                    final java.util.Date date = new java.util.Date();
                                    date.setMonth(date.getMonth() + 1);
                                    AuthAPI.createIndo(uuid, "Vip+++", "MixedUP", false, new Date(date.getTime()));
                                } else if (AuthAPI.getProduct(AccountAPI.getNick(uuid)).contains("2")) {
                                    final java.util.Date date = new java.util.Date();
                                    date.setMonth(date.getMonth() + 2);
                                    AuthAPI.createIndo(uuid, "Vip+++", "MixedUP", false, new Date(date.getTime()));
                                }
                            } else if (AuthAPI.getProduct(AccountAPI.getNick(uuid)).contains("Duque")) {
                                if (AuthAPI.getProduct(AccountAPI.getNick(uuid)).contains("Eterno")) {
                                    AuthAPI.createIndo(uuid, "Vip++", "MixedUP", true, null);
                                } else if (AuthAPI.getProduct(AccountAPI.getNick(uuid)).contains("1")) {
                                    final java.util.Date date = new java.util.Date();
                                    date.setMonth(date.getMonth() + 1);
                                    AuthAPI.createIndo(uuid, "Vip++", "MixedUP", false, new Date(date.getTime()));
                                } else if (AuthAPI.getProduct(AccountAPI.getNick(uuid)).contains("2")) {
                                    final java.util.Date date = new java.util.Date();
                                    date.setMonth(date.getMonth() + 2);
                                    AuthAPI.createIndo(uuid, "Vip++", "MixedUP", false, new Date(date.getTime()));
                                }
                            } else if (AuthAPI.getProduct(AccountAPI.getNick(uuid)).contains("Lord")) {
                                if (AuthAPI.getProduct(AccountAPI.getNick(uuid)).contains("Eterno")) {
                                    AuthAPI.createIndo(uuid, "Vip+", "MixedUP", true, null);
                                } else if (AuthAPI.getProduct(AccountAPI.getNick(uuid)).contains("1")) {
                                    final java.util.Date date = new java.util.Date();
                                    date.setMonth(date.getMonth() + 1);
                                    AuthAPI.createIndo(uuid, "Vip+", "MixedUP", false, new Date(date.getTime()));
                                } else if (AuthAPI.getProduct(AccountAPI.getNick(uuid)).contains("2")) {
                                    final java.util.Date date = new java.util.Date();
                                    date.setMonth(date.getMonth() + 2);
                                    AuthAPI.createIndo(uuid, "Vip+", "MixedUP", false, new Date(date.getTime()));
                                }
                            } else if (AuthAPI.getProduct(AccountAPI.getNick(uuid)).contains("Conde")) {
                                if (AuthAPI.getProduct(AccountAPI.getNick(uuid)).contains("Eterno")) {
                                    AuthAPI.createIndo(uuid, "Vip", "MixedUP", true, null);
                                } else if (AuthAPI.getProduct(AccountAPI.getNick(uuid)).contains("1")) {
                                    final java.util.Date date = new java.util.Date();
                                    date.setMonth(date.getMonth() + 1);
                                    AuthAPI.createIndo(uuid, "Vip", "MixedUP", false, new Date(date.getTime()));
                                } else if (AuthAPI.getProduct(AccountAPI.getNick(uuid)).contains("2")) {
                                    final java.util.Date date = new java.util.Date();
                                    date.setMonth(date.getMonth() + 2);
                                    AuthAPI.createIndo(uuid, "Vip", "MixedUP", false, new Date(date.getTime()));
                                }
                            }

                            TagAPI.updateTag(uuid, AuthAPI.getTagVip(uuid));

                            PauloAPI.getInstance().getAccountsCache().removeFromCache(uuid);
                            final val player = Bukkit.getPlayer(UUID.fromString(uuid));
                            if (Main.scoreboardManager.hasSquadScoreboard(player)) {
                                Main.scoreboardManager.getSquadScoreboard().updateEntry(player, "group");
                            } else {
                                Main.scoreboardManager.getDefaultScoreboard().updateEntry(player, "group");
                            }
                            Main.scoreboardManager.getTablistManager().setPlayerGroup(player, Main.scoreboardManager.getTablistManager().getGroupIdByName(PauloAPI.getInstance().getAccountInfo(player).getRole().getDisplayName()));
                            Main.scoreboardManager.updateTabList(player);

                            if (TagAPI.getTag(uuid).equalsIgnoreCase("Vip")) {
                                CoinsAPI.updateCoins(uuid, CoinsAPI.getCoins(uuid) + 10000);
                                Main.scoreboardManager.getSquadScoreboard().updateEntry(player, "coins");
                            } else if (TagAPI.getTag(uuid).equalsIgnoreCase("Vip+")) {
                                CoinsAPI.updateCoins(uuid, CoinsAPI.getCoins(uuid) + 15000);
                                Main.scoreboardManager.getSquadScoreboard().updateEntry(player, "coins");
                            } else if (TagAPI.getTag(uuid).equalsIgnoreCase("Vip++")) {
                                CoinsAPI.updateCoins(uuid, CoinsAPI.getCoins(uuid) + 20000);
                                Main.scoreboardManager.getSquadScoreboard().updateEntry(player, "coins");
                            } else if (TagAPI.getTag(uuid).equalsIgnoreCase("Vip+++")) {
                                CoinsAPI.updateCoins(uuid, CoinsAPI.getCoins(uuid) + 25000);
                                Main.scoreboardManager.getSquadScoreboard().updateEntry(player, "coins");
                            }

                            for (final Player target : Bukkit.getOnlinePlayers()) {
                                target.playSound(target.getLocation(), Sound.ENTITY_WITHER_DEATH, 1.0f, 1.0f);
                                if (TagAPI.getTag(uuid).equalsIgnoreCase("Vip")) {
                                    target.sendTitle(ChatColor.YELLOW + AccountAPI.getNick(uuid), ChatColor.GRAY + "Tornou-se " + ChatColor.YELLOW + "[Conde]", 20, 60, 20);
                                } else if (TagAPI.getTag(uuid).equalsIgnoreCase("Vip+")) {
                                    target.sendTitle(ChatColor.AQUA + AccountAPI.getNick(uuid), ChatColor.GRAY + "Tornou-se " + ChatColor.AQUA + "[Lord]", 20, 60, 20);
                                } else if (TagAPI.getTag(uuid).equalsIgnoreCase("Vip++")) {
                                    target.sendTitle(ChatColor.DARK_RED + AccountAPI.getNick(uuid), ChatColor.GRAY + "Tornou-se " + ChatColor.DARK_RED + "[Duque]", 20, 60, 20);
                                } else if (TagAPI.getTag(uuid).equalsIgnoreCase("Vip+++")) {
                                    target.sendTitle(ChatColor.DARK_PURPLE + AccountAPI.getNick(uuid), ChatColor.GRAY + "Tornou-se " + ChatColor.DARK_PURPLE + "[Titan]", 20, 60, 20);
                                    target.getWorld().spawnEntity(target.getLocation(), EntityType.FIREWORK);
                                }
                            }
                        }
                    }
                } else {
                    //-
                }
            }
        }, 400L);
    }

    public static void authNoTime(final String UUID) {
        if (TagAPI.getTag(UUID).equalsIgnoreCase("membro")) {
            if (AuthAPI.getVip(AccountAPI.getNick(UUID)) == true) {
                if (AuthAPI.getExistInfo(UUID) == null) {
                    if (AuthAPI.getProduct(AccountAPI.getNick(UUID)).contains("Titan")) {
                        if (AuthAPI.getProduct(AccountAPI.getNick(UUID)).contains("Eterno")) {
                            AuthAPI.createIndo(UUID, "Vip+++", "MixedUP", true, null);
                        } else if (AuthAPI.getProduct(AccountAPI.getNick(UUID)).contains("1")) {
                            final java.util.Date date = new java.util.Date();
                            date.setMonth(date.getMonth() + 1);
                            AuthAPI.createIndo(UUID, "Vip+++", "MixedUP", false, new Date(date.getTime()));
                        } else if (AuthAPI.getProduct(AccountAPI.getNick(UUID)).contains("2")) {
                            final java.util.Date date = new java.util.Date();
                            date.setMonth(date.getMonth() + 2);
                            AuthAPI.createIndo(UUID, "Vip+++", "MixedUP", false, new Date(date.getTime()));
                        }
                    } else if (AuthAPI.getProduct(AccountAPI.getNick(UUID)).contains("Duque")) {
                        if (AuthAPI.getProduct(AccountAPI.getNick(UUID)).contains("Eterno")) {
                            AuthAPI.createIndo(UUID, "Vip++", "MixedUP", true, null);
                        } else if (AuthAPI.getProduct(AccountAPI.getNick(UUID)).contains("1")) {
                            final java.util.Date date = new java.util.Date();
                            date.setMonth(date.getMonth() + 1);
                            AuthAPI.createIndo(UUID, "Vip++", "MixedUP", false, new Date(date.getTime()));
                        } else if (AuthAPI.getProduct(AccountAPI.getNick(UUID)).contains("2")) {
                            final java.util.Date date = new java.util.Date();
                            date.setMonth(date.getMonth() + 2);
                            AuthAPI.createIndo(UUID, "Vip++", "MixedUP", false, new Date(date.getTime()));
                        }
                    } else if (AuthAPI.getProduct(AccountAPI.getNick(UUID)).contains("Lord")) {
                        if (AuthAPI.getProduct(AccountAPI.getNick(UUID)).contains("Eterno")) {
                            AuthAPI.createIndo(UUID, "Vip+", "MixedUP", true, null);
                        } else if (AuthAPI.getProduct(AccountAPI.getNick(UUID)).contains("1")) {
                            final java.util.Date date = new java.util.Date();
                            date.setMonth(date.getMonth() + 1);
                            AuthAPI.createIndo(UUID, "Vip+", "MixedUP", false, new Date(date.getTime()));
                        } else if (AuthAPI.getProduct(AccountAPI.getNick(UUID)).contains("2")) {
                            final java.util.Date date = new java.util.Date();
                            date.setMonth(date.getMonth() + 2);
                            AuthAPI.createIndo(UUID, "Vip+", "MixedUP", false, new Date(date.getTime()));
                        }
                    } else if (AuthAPI.getProduct(AccountAPI.getNick(UUID)).contains("Conde")) {
                        if (AuthAPI.getProduct(AccountAPI.getNick(UUID)).contains("Eterno")) {
                            AuthAPI.createIndo(UUID, "Vip", "MixedUP", true, null);
                        } else if (AuthAPI.getProduct(AccountAPI.getNick(UUID)).contains("1")) {
                            final java.util.Date date = new java.util.Date();
                            date.setMonth(date.getMonth() + 1);
                            AuthAPI.createIndo(UUID, "Vip", "MixedUP", false, new Date(date.getTime()));
                        } else if (AuthAPI.getProduct(AccountAPI.getNick(UUID)).contains("2")) {
                            final java.util.Date date = new java.util.Date();
                            date.setMonth(date.getMonth() + 2);
                            AuthAPI.createIndo(UUID, "Vip", "MixedUP", false, new Date(date.getTime()));
                        }
                    }

                    TagAPI.updateTag(UUID, AuthAPI.getTagVip(UUID));
                    for (final Player target : Bukkit.getOnlinePlayers()) {
                        if (Main.scoreboardManager.hasSquadScoreboard(target)) {
                            Main.scoreboardManager.getSquadScoreboard().updateEntry(target, "group");
                        } else {
                            Main.scoreboardManager.getDefaultScoreboard().updateEntry(target, "group");
                        }
                        target.playSound(target.getLocation(), Sound.ENTITY_WITHER_DEATH, 1.0f, 1.0f);
                        if (TagAPI.getTag(UUID).equalsIgnoreCase("Vip")) {
                            target.sendTitle(ChatColor.YELLOW + AccountAPI.getNick(UUID), ChatColor.GRAY + "Tornou-se " + ChatColor.YELLOW + "[Conde]", 20, 60, 20);
                        } else if (TagAPI.getTag(UUID).equalsIgnoreCase("Vip+")) {
                            target.sendTitle(ChatColor.AQUA + AccountAPI.getNick(UUID), ChatColor.GRAY + "Tornou-se " + ChatColor.AQUA + "[Lord]", 20, 60, 20);
                        } else if (TagAPI.getTag(UUID).equalsIgnoreCase("Vip++")) {
                            target.sendTitle(ChatColor.DARK_RED + AccountAPI.getNick(UUID), ChatColor.GRAY + "Tornou-se " + ChatColor.DARK_RED + "[Duque]", 20, 60, 20);
                        } else if (TagAPI.getTag(UUID).equalsIgnoreCase("Vip+++")) {
                            target.sendTitle(ChatColor.DARK_PURPLE + AccountAPI.getNick(UUID), ChatColor.GRAY + "Tornou-se " + ChatColor.DARK_PURPLE + "[Titan]", 20, 60, 20);
                            target.getWorld().spawnEntity(target.getLocation(), EntityType.FIREWORK);
                        }
                    }
                }
            }
        } else {
            //-
        }
    }

    public static void removeVip(final String UUID, final Player player) {
        if (AuthAPI.getExistInfo(UUID) != null) {
            final Date date = AuthAPI.getDateEnds(UUID);
            if (date == null) return;
            final int value = date.compareTo(new java.util.Date());

            if (value <= 0) {
                AuthAPI.removeVip(UUID);
                AuthAPI.removeVipWeb(AccountAPI.getNick(UUID));
                TagAPI.updateTag(UUID, "Membro");

                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (TagAPI.getTag(UUID).equalsIgnoreCase("Membro")) {
                            player.sendMessage(ChatColor.RED + "\n * Vosso VIP acaba de expirar D=\n  " + ChatColor.GRAY + "Adquira um novo VIP em: " + ChatColor.AQUA + "www.finalelite.com.br\n ");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    }
                }, 20L);
            }
        }
    }
}
