package com.mixedup.vipAutentication;


public enum InvoiceType {
    ONE_MONTH,
    TWO_MONTHS,
    ETERNAL;

    public static InvoiceType fromId(final byte id) {
        return InvoiceType.values()[id];
    }

    public String toPtBR() {
        if (this == InvoiceType.ONE_MONTH)
            return "1";
        if (this == InvoiceType.TWO_MONTHS)
            return "2";
        if (this == InvoiceType.ETERNAL)
            return "Eterno";
        else
            return null;
    }

}
