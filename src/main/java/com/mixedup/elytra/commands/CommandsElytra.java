package com.mixedup.elytra.commands;

import com.mixedup.Main;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import com.mixedup.elytra.DungeonAPI;
import com.mixedup.elytra.PortalAPI;
import com.mixedup.elytra.hashs.CreatingPortalHash;
import com.mixedup.elytra.hashs.IsSelectedHash;
import com.mixedup.elytra.hashs.LocationSelectedHash;
import com.mixedup.elytra.hashs.SelectPortalFromDeleteHash;
import com.mixedup.elytra.regions.DungeonsHash;
import com.mixedup.elytra.regions.DungeonsUtils;
import com.mixedup.elytra.regions.PortalsHash;
import com.mixedup.elytra.regions.PortalsUtils;
import com.mixedup.utils.AlternateColor;
import com.mixedup.utils.Hologram;
import com.mixedup.utils.RemoveItemInv;
import com.mixedup.utils.WorldEditUtil;
import com.sk89q.worldedit.WorldEditException;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.io.IOException;

public class CommandsElytra implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        // -/criar (portal/dungeon) (nível)**Dungeon
        if (cmd.getName().equalsIgnoreCase("criar")) {
            if (TagAPI.getTag(UUID).equalsIgnoreCase("Master") || TagAPI.getTag(UUID).equalsIgnoreCase("Supervisor")) {
                if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("portal")) {
                        if (IsSelectedHash.get(UUID) != null && IsSelectedHash.get(UUID).getStatus() == true) {
                            final ItemStack marcador = new ItemStack(Material.STICK);
                            final ItemMeta metaMarc = marcador.getItemMeta();
                            metaMarc.setDisplayName(ChatColor.YELLOW + "Marcador");
                            marcador.setItemMeta(metaMarc);
                            RemoveItemInv.removeItems(player.getInventory(), marcador, 1);

                            IsSelectedHash.get(UUID).setStatus(false);
                            CreatingPortalHash.get(UUID).setStatus(false);

                            final String portal = LocationSelectedHash.get(UUID).getPos1() + "::" + LocationSelectedHash.get(UUID).getPos2();
                            final String saida = player.getWorld().getName() + ":" + player.getLocation().getX() + ":" + player.getLocation().getY() + ":" + player.getLocation().getZ() + ":" + player.getLocation().getYaw() + ":" + player.getLocation().getPitch();
                            PortalAPI.createPortal(portal, saida);
                            final PortalsHash portals = new PortalsHash(saida, LocationSelectedHash.get(UUID).getPos1(), LocationSelectedHash.get(UUID).getPos2()).insert();
                            PortalsUtils.getRegions().add(portals);

                            LocationSelectedHash.get(UUID).setPos1("NULL");
                            LocationSelectedHash.get(UUID).setPos2("NULL");

                            player.sendMessage(ChatColor.GREEN + " * Portal criado com sucesso!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            return false;
                        }
                        if (CreatingPortalHash.get(UUID) == null) {
                            new CreatingPortalHash(UUID, true).insert();

                            final ItemStack marcador = new ItemStack(Material.STICK);
                            final ItemMeta metaMarc = marcador.getItemMeta();
                            metaMarc.setDisplayName(ChatColor.YELLOW + "Marcador");
                            marcador.setItemMeta(metaMarc);
                            player.getInventory().addItem(marcador);
                            player.sendMessage(ChatColor.GREEN + " * Selecione o portal e utilize novamente /criar portal");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else if (CreatingPortalHash.get(UUID).getStatus() == false) {
                            CreatingPortalHash.get(UUID).setStatus(true);

                            final ItemStack marcador = new ItemStack(Material.STICK);
                            final ItemMeta metaMarc = marcador.getItemMeta();
                            metaMarc.setDisplayName(ChatColor.YELLOW + "Marcador");
                            marcador.setItemMeta(metaMarc);
                            player.getInventory().addItem(marcador);
                            player.sendMessage(ChatColor.GREEN + " * Selecione o portal e utilize novamente /criar portal");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes á &ncriar&c:\n \n&7 - /criar dungeon (nível)\n&7 - /criar portal\n \n"));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else if (args.length == 2) {
                    if (args[0].equalsIgnoreCase("dungeon")) {
                        try {
                            final int nivel = Integer.valueOf(args[1]);
                            if (nivel > 0 && nivel <= 5) {
                                if (DungeonsUtils.playerInArea(player.getLocation()) == null) {
                                    final String loc = player.getWorld().getName() + ":" + player.getLocation().getBlockX() + ":" + player.getLocation().getBlockY() + ":" + player.getLocation().getBlockZ();
                                    final String loc1 = player.getWorld().getName() + ":" + (player.getLocation().getBlockX() + 20) + ":0:" + (player.getLocation().getBlockZ() + 20);
                                    final String loc2 = player.getWorld().getName() + ":" + (player.getLocation().getBlockX() - 20) + ":256:" + (player.getLocation().getBlockZ() - 20);
                                    DungeonAPI.createDungeon(loc, nivel);

                                    final String[] text = {ChatColor.RED + "Desativado"};
                                    final ArmorStand hologram = Hologram.createHologramReturnStand(new Location(player.getWorld(), player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ()).add(0.5, 1.65, 0.5), text);

                                    final DungeonsHash dungeon = new DungeonsHash(loc, loc1, loc2, hologram, null, nivel, null, false).insert();
                                    DungeonsUtils.getRegions().add(dungeon);

                                    if (nivel == 1 || nivel == 2 || nivel == 3) {
                                        try {
                                            WorldEditUtil.paste(WorldEditUtil.load(Main.plugin.getDataFolder() + File.separator + "WorldEdit/schematics/dungeon-1"), true, player.getLocation());
                                        } catch (final WorldEditException e) {
                                            e.printStackTrace();
                                        } catch (final IOException e) {
                                            e.printStackTrace();
                                        }
                                    } else if (nivel == 4 || nivel == 5) {
                                        try {
                                            WorldEditUtil.paste(WorldEditUtil.load(Main.plugin.getDataFolder() + File.separator + "WorldEdit/schematics/dungeon-2"), true, player.getLocation());
                                        } catch (final WorldEditException e) {
                                            e.printStackTrace();
                                        } catch (final IOException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    player.sendMessage(ChatColor.GREEN + " * Dungeon definida com sucesso!");
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                } else {
                                    player.sendMessage(ChatColor.RED + " * Ops, existe alguma dungeon muito próxima desta área!");
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, o nível a ser definido deve ser de 1 à 5.");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } catch (final NumberFormatException e) {
                            player.sendMessage(ChatColor.RED + " * Ops, informe algum valor para nível existente.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes á &ncriar&c:\n \n&7 - /criar dungeon (nível)\n&7 - /criar portal\n \n"));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes á &ncriar&c:\n \n&7 - /criar dungeon (nível)\n&7 - /criar portal\n \n"));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }

        // -/remover (portal/dungeon)
        if (cmd.getName().equalsIgnoreCase("remover")) {
            if (TagAPI.getTag(UUID).equalsIgnoreCase("Master") || TagAPI.getTag(UUID).equalsIgnoreCase("Supervisor")) {
                if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("portal")) {
                        if (SelectPortalFromDeleteHash.get(UUID) == null) {
                            new SelectPortalFromDeleteHash(UUID, true).insert();
                        } else {
                            if (SelectPortalFromDeleteHash.get(UUID).getStatus() == false) {
                                SelectPortalFromDeleteHash.get(UUID).setStatus(true);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Atravesse o portal desejado, para que seja deletado!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                return false;
                            }
                        }

                        player.sendMessage(ChatColor.GREEN + " * Atravesse o portal desejado, para que seja deletado!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else if (args[0].equalsIgnoreCase("dungeon")) {

                    } else {
                        player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes á &nremover&c:\n \n&7 - /remover dungeon\n&7 - /remover portal\n \n"));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes á &nremover&c:\n \n&7 - /remover dungeon\n&7 - /remover portal\n \n"));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
