package com.mixedup.elytra;

import java.util.HashMap;

public class DungeonCache {

    public static HashMap<String, DungeonCache> CACHE = new HashMap<>();
    //(Location, Level, Activated, TimeUnlock
    private String location;
    private int level;
    private boolean activated;
    private long timeLock;

    public DungeonCache(final String location, final int level, final boolean activated, final long timeLock) {
        this.location = location;
        this.level = level;
        this.activated = activated;
        this.timeLock = timeLock;
    }

    public static DungeonCache get(final String location) {
        return DungeonCache.CACHE.get(location);
    }

    public DungeonCache insert() {
        DungeonCache.CACHE.put(location, this);
        return this;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(final String location) {
        this.location = location;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(final int level) {
        this.level = level;
    }

    public boolean getActivated() {
        return activated;
    }

    public void setActivated(final boolean activated) {
        this.activated = activated;
    }

    public long getTimeLock() {
        return timeLock;
    }

    public void setTimeLock(final long timeLock) {
        this.timeLock = timeLock;
    }
}
