package com.mixedup.elytra.listeners;

import com.mixedup.Main;
import com.mixedup.apis.GuildaAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.elytra.PortalAPI;
import com.mixedup.elytra.hashs.IsTeleportedHash;
import com.mixedup.elytra.hashs.SelectPortalFromDeleteHash;
import com.mixedup.elytra.regions.PortalsHash;
import com.mixedup.elytra.regions.PortalsUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerEntryPortals implements Listener {

    @EventHandler
    public void onEntryPortal(final PlayerMoveEvent event) {
        final Player player = event.getPlayer();

        if (PortalsUtils.playerInArea(player.getLocation()) != null) {
            final String UUID = PlayerUUID.getUUID(player.getName());

            if (SelectPortalFromDeleteHash.get(UUID) != null && SelectPortalFromDeleteHash.get(UUID).getStatus() == true) {
                SelectPortalFromDeleteHash.get(UUID).setStatus(false);
                final String local = PortalsUtils.playerInArea(player.getLocation()).getLocal();
                PortalAPI.removePortal(local);
                PortalsHash.get(local).setPos1("NULL");
                PortalsHash.get(local).setPos2("NULL");

                player.sendMessage(ChatColor.GREEN + " * Portal removido com sucesso!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                return;
            }

            if (IsTeleportedHash.get(UUID) != null && IsTeleportedHash.get(UUID).getStatus() == false && GuildaAPI.getGuilda(UUID) != null) {
                final String[] split = PortalsUtils.playerInArea(player.getLocation()).getLocal().split(":");
                final Location location = new Location(Bukkit.getWorld(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]), Double.parseDouble(split[3]), Float.parseFloat(split[4]), Float.parseFloat(split[5]));

                player.teleport(location);
                player.sendMessage(ChatColor.GREEN + " * Teletransportando..");
                player.playSound(player.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 1.0f, 1.0f);

                IsTeleportedHash.get(UUID).setStatus(true);

                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        IsTeleportedHash.get(UUID).setStatus(false);
                    }
                }, 20L);
            } else if (IsTeleportedHash.get(UUID) == null && GuildaAPI.getGuilda(UUID) != null) {
                final String[] split = PortalsUtils.playerInArea(player.getLocation()).getLocal().split(":");
                final Location location = new Location(Bukkit.getWorld(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]), Double.parseDouble(split[3]), Float.parseFloat(split[4]), Float.parseFloat(split[5]));

                player.teleport(location);
                player.sendMessage(ChatColor.GREEN + " * Teletransportando..");
                player.playSound(player.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 1.0f, 1.0f);

                new IsTeleportedHash(UUID, true).insert();

                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        IsTeleportedHash.get(UUID).setStatus(false);
                    }
                }, 20L);
            }
        }
    }
}
