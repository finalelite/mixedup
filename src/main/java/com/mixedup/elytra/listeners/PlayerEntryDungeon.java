package com.mixedup.elytra.listeners;

import com.mixedup.Main;
import com.mixedup.apis.HorarioAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.elytra.DungeonAPI;
import com.mixedup.elytra.hashs.PlayerInRegionHash;
import com.mixedup.elytra.hashs.RotateHash;
import com.mixedup.elytra.hashs.RotateReEntryHash;
import com.mixedup.elytra.hashs.RotateSecondsHash;
import com.mixedup.elytra.regions.DungeonsHash;
import com.mixedup.elytra.regions.DungeonsUtils;
import com.mixedup.utils.Hologram;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;
import java.util.Map.Entry;

public class PlayerEntryDungeon implements Listener {

    private static void randomizer(final Player player, final int level) {
        //11-13-15
        //1
        final int rotate = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
            final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Sorteador: ");
            final int t = 0;

            @Override
            public void run() {
                this.inventory.setItem(11, getItemRandom(level));
                this.inventory.setItem(13, getItemRandom(level));
                this.inventory.setItem(15, getItemRandom(level));
                if (this.t == 0) {
                    player.openInventory(this.inventory);
                }
                if (player.getOpenInventory() != null && player.getOpenInventory().getTopInventory().getName().equalsIgnoreCase("Sorteador: ")) {
                    player.openInventory(this.inventory);
                }
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }, 0L, 10L);

        //2
        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
            @Override
            public void run() {
                Bukkit.getScheduler().cancelTask(rotate);
            }
        }, 155L);

        final ItemStack itemRandom = random(level);

        //3
        final int rotate2 = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
            final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Sorteador: ");
            int next;

            @Override
            public void run() {
                this.inventory.setItem(11, itemRandom);
                this.inventory.setItem(13, itemRandom);
                this.inventory.setItem(15, itemRandom);
                if (this.next <= 9) {
                    this.inventory.setItem(this.next, randomPainel());
                } else if (this.next >= 10) {
                    this.inventory.setItem(this.next + 8, randomPainel());
                }
                this.next++;

                if (player.getOpenInventory() != null && player.getOpenInventory().getTopInventory().getName().equalsIgnoreCase("Sorteador: ")) {
                    player.openInventory(this.inventory);
                }
            }
        }, 155L, 3L);

        //4
        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
            @Override
            public void run() {
                Bukkit.getScheduler().cancelTask(rotate2);
                player.getOpenInventory().close();
                player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);

                final String[] split = DungeonsUtils.playerInArea(player.getLocation()).getLocal().split(":");
                final Location location = new Location(Bukkit.getWorld(split[0]), Integer.valueOf(split[1]), Integer.valueOf(split[2]), Integer.valueOf(split[3]));
                final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(itemRandom);
                for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                    player.getWorld().dropItemNaturally(location, entry.getValue());
                }
                player.playSound(player.getLocation(), Sound.BLOCK_CHEST_CLOSE, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.BLOCK_CHEST_LOCKED, 1.0f, 1.0f);
            }
        }, 210L);
    }

    private static ItemStack random(final int level) {
        final Random random = new Random();

        if (level == 1) {
            //-
            final ItemStack helmet = new ItemStack(Material.IRON_HELMET);
            helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
            helmet.addEnchantment(Enchantment.DURABILITY, 1);

            final ItemStack ironSword = new ItemStack(Material.IRON_SWORD);

            final ItemStack frag = new ItemStack(Material.FEATHER);
            final ItemMeta metaFrag = frag.getItemMeta();
            metaFrag.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
            final ArrayList<String> lore = new ArrayList<>();
            lore.add(" ");
            lore.add(ChatColor.GRAY + "Fragmento para asa, tipo: 1");
            lore.add(" ");
            metaFrag.setLore(lore);
            frag.setItemMeta(metaFrag);

            final ItemStack goldenC = new ItemStack(Material.GOLDEN_CARROT, 32);

            final ItemStack rep = new ItemStack(Material.COBWEB);
            final ItemMeta metaRep = rep.getItemMeta();
            metaRep.setDisplayName(ChatColor.RED + "Tente novamente..");
            rep.setItemMeta(metaRep);

            final ItemStack pick = new ItemStack(Material.IRON_PICKAXE);

            final ItemStack chestPlate = new ItemStack(Material.IRON_CHESTPLATE);
            chestPlate.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
            chestPlate.addEnchantment(Enchantment.DURABILITY, 1);

            final ItemStack iron = new ItemStack(Material.IRON_INGOT, 32);
            final ItemStack xp = new ItemStack(Material.EXPERIENCE_BOTTLE, 8);
            //-

            final int r = random.nextInt(10);
            if (r == 0) {
                return rep;
            } else if (r == 1) {
                return helmet;
            } else if (r == 2) {
                return ironSword;
            } else if (r == 3) {
                return frag;
            } else if (r == 4) {
                return goldenC;
            } else if (r == 5) {
                return rep;
            } else if (r == 6) {
                return pick;
            } else if (r == 7) {
                return chestPlate;
            } else if (r == 8) {
                return iron;
            } else if (r == 9) {
                return xp;
            } else if (r == 10) {
                return rep;
            }
        } else if (level == 2) {
            final ItemStack rep = new ItemStack(Material.COBWEB);
            final ItemMeta metaRep = rep.getItemMeta();
            metaRep.setDisplayName(ChatColor.RED + "Tente novamente..");
            rep.setItemMeta(metaRep);

            final ItemStack pickD = new ItemStack(Material.DIAMOND_PICKAXE);

            final ItemStack ironB = new ItemStack(Material.IRON_BOOTS);
            ironB.addEnchantment(Enchantment.DURABILITY, 1);
            ironB.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);

            final ItemStack frag = new ItemStack(Material.FEATHER, 5);
            final ItemMeta metaFrag = frag.getItemMeta();
            metaFrag.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
            final ArrayList<String> lore = new ArrayList<>();
            lore.add(" ");
            lore.add(ChatColor.GRAY + "Fragmento para asa, tipo: 1");
            lore.add(" ");
            metaFrag.setLore(lore);
            frag.setItemMeta(metaFrag);
            //-

            final ItemStack fragN = new ItemStack(Material.PHANTOM_MEMBRANE);
            final ItemMeta metaFragN = fragN.getItemMeta();
            metaFragN.setDisplayName(ChatColor.GREEN + "Fragmento de nível");
            final ArrayList<String> lore1 = new ArrayList<>();
            lore1.add(" ");
            lore1.add(ChatColor.GRAY + "Fragmento de nível p/ asa, nível: 1");
            lore1.add(" ");
            metaFragN.setLore(lore1);
            fragN.setItemMeta(metaFragN);

            final ItemStack swordD = new ItemStack(Material.DIAMOND_SWORD);

            final ItemStack frag1 = new ItemStack(Material.FEATHER);
            final ItemMeta metaFrag1 = frag1.getItemMeta();
            metaFrag1.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
            final ArrayList<String> lore2 = new ArrayList<>();
            lore2.add(" ");
            lore2.add(ChatColor.GRAY + "Fragmento para asa, tipo: 2");
            lore2.add(" ");
            metaFrag1.setLore(lore2);
            frag1.setItemMeta(metaFrag1);

            final ItemStack ironL = new ItemStack(Material.IRON_LEGGINGS);
            ironL.addEnchantment(Enchantment.DURABILITY, 1);
            ironL.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);

            final ItemStack goldenA = new ItemStack(Material.GOLDEN_APPLE);

            final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 3);
            final ItemMeta metaChave = chave.getItemMeta();
            metaChave.setDisplayName(ChatColor.GREEN + "Chave");
            final List<String> lore3 = new ArrayList<>();
            lore3.add(" ");
            lore3.add(ChatColor.DARK_GRAY + "Utilizado para abrir caixas");
            lore3.add(ChatColor.YELLOW + "50% sucesso");
            lore3.add(" ");
            metaChave.setLore(lore3);
            chave.setItemMeta(metaChave);

            final ItemStack iron = new ItemStack(Material.IRON_INGOT, 32);

            final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
            final ItemMeta meta2 = moeda.getItemMeta();
            meta2.setDisplayName(ChatColor.YELLOW + "$2.000,00 moedas");
            moeda.setItemMeta(meta2);

            final int r = random.nextInt(12);
            if (r == 0) {
                return rep;
            } else if (r == 1) {
                return ironB;
            } else if (r == 2) {
                return frag;
            } else if (r == 3) {
                return rep;
            } else if (r == 4) {
                return fragN;
            } else if (r == 5) {
                return swordD;
            } else if (r == 6) {
                return frag1;
            } else if (r == 7) {
                return ironL;
            } else if (r == 8) {
                return goldenA;
            } else if (r == 9) {
                return chave;
            } else if (r == 10) {
                return iron;
            } else if (r == 11) {
                return moeda;
            } else if (r == 12) {
                return pickD;
            }
        } else if (level == 3) {
            final ItemStack rep = new ItemStack(Material.COBWEB);
            final ItemMeta metaRep = rep.getItemMeta();
            metaRep.setDisplayName(ChatColor.RED + "Tente novamente..");
            rep.setItemMeta(metaRep);
            //-

            final ItemStack helmetD = new ItemStack(Material.DIAMOND_HELMET);
            helmetD.addEnchantment(Enchantment.DURABILITY, 2);
            helmetD.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);

            final ItemStack caixa = new ItemStack(Material.CHEST);
            final ItemMeta metaCaixa = caixa.getItemMeta();
            metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
            final List<String> lore = new ArrayList<String>();
            lore.add(" ");
            lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.DARK_GRAY + "Básica");
            lore.add(" ");
            metaCaixa.setLore(lore);
            caixa.setItemMeta(metaCaixa);

            final ItemStack tntComum = new ItemStack(Material.TNT, 32);
            final ItemMeta meta5 = tntComum.getItemMeta();
            meta5.setDisplayName(ChatColor.RED + "Explosivo comum");
            final List<String> lore4 = new ArrayList<>();
            lore4.add(" ");
            lore4.add(ChatColor.GRAY + "Dano: 1/5");
            lore4.add(" ");
            meta5.setLore(lore4);
            tntComum.setItemMeta(meta5);

            final ItemStack fragN = new ItemStack(Material.PHANTOM_MEMBRANE);
            final ItemMeta metaFragN = fragN.getItemMeta();
            metaFragN.setDisplayName(ChatColor.GREEN + "Fragmento de nível");
            final ArrayList<String> lore1 = new ArrayList<>();
            lore1.add(" ");
            lore1.add(ChatColor.GRAY + "Fragmento de nível p/ asa, nível: 2");
            lore1.add(" ");
            metaFragN.setLore(lore1);
            fragN.setItemMeta(metaFragN);

            final ItemStack iron = new ItemStack(Material.IRON_INGOT, 64);

            final ItemStack frag1 = new ItemStack(Material.FEATHER, 5);
            final ItemMeta metaFrag1 = frag1.getItemMeta();
            metaFrag1.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
            final ArrayList<String> lore2 = new ArrayList<>();
            lore2.add(" ");
            lore2.add(ChatColor.GRAY + "Fragmento para asa, tipo: 2");
            lore2.add(" ");
            metaFrag1.setLore(lore2);
            frag1.setItemMeta(metaFrag1);

            final ItemStack gold = new ItemStack(Material.GOLD_INGOT, 16);

            final ItemStack goldenA = new ItemStack(Material.GOLDEN_APPLE, 16);

            final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 6);
            final ItemMeta metaChave = chave.getItemMeta();
            metaChave.setDisplayName(ChatColor.GREEN + "Chave");
            final List<String> lore3 = new ArrayList<>();
            lore3.add(" ");
            lore3.add(ChatColor.DARK_GRAY + "Utilizado para abrir caixas");
            lore3.add(ChatColor.YELLOW + "50% sucesso");
            lore3.add(" ");
            metaChave.setLore(lore3);
            chave.setItemMeta(metaChave);

            final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
            final ItemMeta meta2 = moeda.getItemMeta();
            meta2.setDisplayName(ChatColor.YELLOW + "$5.000,00 moedas");
            moeda.setItemMeta(meta2);

            final int r = random.nextInt(11);
            if (r == 0) {
                return rep;
            } else if (r == 1) {
                return rep;
            } else if (r == 2) {
                return helmetD;
            } else if (r == 3) {
                return caixa;
            } else if (r == 4) {
                return tntComum;
            } else if (r == 5) {
                return fragN;
            } else if (r == 6) {
                return iron;
            } else if (r == 7) {
                return frag1;
            } else if (r == 8) {
                return gold;
            } else if (r == 9) {
                return goldenA;
            } else if (r == 10) {
                return chave;
            } else if (r == 11) {
                return chave;
            }
        } else if (level == 4) {
            final ItemStack rep = new ItemStack(Material.COBWEB);
            final ItemMeta metaRep = rep.getItemMeta();
            metaRep.setDisplayName(ChatColor.RED + "Tente novamente..");
            rep.setItemMeta(metaRep);

            final ItemStack tntComum = new ItemStack(Material.TNT, 32);
            final ItemMeta meta5 = tntComum.getItemMeta();
            meta5.setDisplayName(ChatColor.RED + "Explosivo comum");
            final List<String> lore4 = new ArrayList<>();
            lore4.add(" ");
            lore4.add(ChatColor.GRAY + "Dano: 1/5");
            lore4.add(" ");
            meta5.setLore(lore4);
            tntComum.setItemMeta(meta5);

            final ItemStack appleG = new ItemStack(Material.ENCHANTED_GOLDEN_APPLE, 16);

            final ItemStack frag1 = new ItemStack(Material.FEATHER, 8);
            final ItemMeta metaFrag1 = frag1.getItemMeta();
            metaFrag1.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
            final ArrayList<String> lore2 = new ArrayList<>();
            lore2.add(" ");
            lore2.add(ChatColor.GRAY + "Fragmento para asa, tipo: 2");
            lore2.add(" ");
            metaFrag1.setLore(lore2);
            frag1.setItemMeta(metaFrag1);

            final ItemStack gold = new ItemStack(Material.GOLD_INGOT, 32);

            final ItemStack fragN = new ItemStack(Material.PHANTOM_MEMBRANE);
            final ItemMeta metaFragN = fragN.getItemMeta();
            metaFragN.setDisplayName(ChatColor.GREEN + "Fragmento de nível");
            final ArrayList<String> lore1 = new ArrayList<>();
            lore1.add(" ");
            lore1.add(ChatColor.GRAY + "Fragmento de nível p/ asa, nível: 3");
            lore1.add(" ");
            metaFragN.setLore(lore1);
            fragN.setItemMeta(metaFragN);

            final ItemStack caixa = new ItemStack(Material.CHEST);
            final ItemMeta metaCaixa = caixa.getItemMeta();
            metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
            final List<String> lore = new ArrayList<String>();
            lore.add(" ");
            lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.DARK_GRAY + "Básica");
            lore.add(" ");
            metaCaixa.setLore(lore);
            caixa.setItemMeta(metaCaixa);

            //FRAGMENTO ASA 2 - FRAG1
            //-

            final ItemStack diamond = new ItemStack(Material.DIAMOND, 5);
            final ItemStack enderpearl = new ItemStack(Material.ENDER_PEARL, 8);

            final ItemStack leggingD = new ItemStack(Material.DIAMOND_LEGGINGS);
            leggingD.addEnchantment(Enchantment.DURABILITY, 2);
            leggingD.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);

            final ItemStack pickD = new ItemStack(Material.DIAMOND_PICKAXE);
            pickD.addEnchantment(Enchantment.DURABILITY, 2);
            pickD.addEnchantment(Enchantment.DIG_SPEED, 5);

            final ItemStack xp = new ItemStack(Material.EXPERIENCE_BOTTLE, 16);

            final int r = random.nextInt(12);
            if (r == 0) {
                return rep;
            } else if (r == 1) {
                return tntComum;
            } else if (r == 2) {
                return frag1;
            } else if (r == 3) {
                return gold;
            } else if (r == 4) {
                return fragN;
            } else if (r == 5) {
                return caixa;
            } else if (r == 6) {
                return frag1;
            } else if (r == 7) {
                return rep;
            } else if (r == 8) {
                return diamond;
            } else if (r == 9) {
                return enderpearl;
            } else if (r == 10) {
                return leggingD;
            } else if (r == 11) {
                return pickD;
            } else if (r == 12) {
                return xp;
            }
        } else if (level == 5) {
            final ItemStack caixa = new ItemStack(Material.ENDER_CHEST);
            final ItemMeta metaCaixa = caixa.getItemMeta();
            metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
            final List<String> lore = new ArrayList<String>();
            lore.add(" ");
            lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.DARK_GRAY + "Avançada");
            lore.add(" ");
            metaCaixa.setLore(lore);
            caixa.setItemMeta(metaCaixa);

            final ItemStack axeA = new ItemStack(Material.DIAMOND_AXE);
            axeA.addEnchantment(Enchantment.DURABILITY, 3);
            axeA.addEnchantment(Enchantment.DAMAGE_UNDEAD, 4);

            final ItemStack rep = new ItemStack(Material.COBWEB);
            final ItemMeta metaRep = rep.getItemMeta();
            metaRep.setDisplayName(ChatColor.RED + "Tente novamente..");
            rep.setItemMeta(metaRep);

            final ItemStack pickD = new ItemStack(Material.DIAMOND_PICKAXE);

            final ItemStack fragN = new ItemStack(Material.PHANTOM_MEMBRANE, 3);
            final ItemMeta metaFragN = fragN.getItemMeta();
            metaFragN.setDisplayName(ChatColor.GREEN + "Fragmento de nível");
            final ArrayList<String> lore1 = new ArrayList<>();
            lore1.add(" ");
            lore1.add(ChatColor.GRAY + "Fragmento de nível p/ asa, nível: 3");
            lore1.add(" ");
            metaFragN.setLore(lore1);
            fragN.setItemMeta(metaFragN);

            final ItemStack gold = new ItemStack(Material.GOLD_INGOT, 32);

            final ItemStack frag1 = new ItemStack(Material.FEATHER, 10);
            final ItemMeta metaFrag1 = frag1.getItemMeta();
            metaFrag1.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
            final ArrayList<String> lore2 = new ArrayList<>();
            lore2.add(" ");
            lore2.add(ChatColor.GRAY + "Fragmento para asa, tipo: 2");
            lore2.add(" ");
            metaFrag1.setLore(lore2);
            frag1.setItemMeta(metaFrag1);

            final ItemStack appleG = new ItemStack(Material.ENCHANTED_GOLDEN_APPLE, 16);
            final ItemStack diamondB = new ItemStack(Material.DIAMOND_BLOCK);

            final ItemStack bootsD = new ItemStack(Material.DIAMOND_BOOTS);
            bootsD.addEnchantment(Enchantment.DURABILITY, 3);
            bootsD.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);

            final ItemStack xp = new ItemStack(Material.EXPERIENCE_BOTTLE, 32);
            final ItemStack enderpearl = new ItemStack(Material.ENDER_PEARL, 16);

            //-

            final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
            final ItemMeta meta2 = moeda.getItemMeta();
            meta2.setDisplayName(ChatColor.YELLOW + "$10.000,00 moedas");
            moeda.setItemMeta(meta2);

            final int r = random.nextInt(13);
            if (r == 0) {
                return rep;
            } else if (r == 1) {
                return caixa;
            } else if (r == 2) {
                return axeA;
            } else if (r == 3) {
                return rep;
            } else if (r == 4) {
                return pickD;
            } else if (r == 5) {
                return fragN;
            } else if (r == 6) {
                return gold;
            } else if (r == 7) {
                return frag1;
            } else if (r == 8) {
                return appleG;
            } else if (r == 9) {
                return diamondB;
            } else if (r == 10) {
                return bootsD;
            } else if (r == 11) {
                return xp;
            } else if (r == 12) {
                return enderpearl;
            } else if (r == 13) {
                return moeda;
            }
        }

        return null;
    }

    private static ItemStack randomPainel() {
        final Random random = new Random();
        final int r = random.nextInt(15);

        if (r == 0) {
            final ItemStack glass = new ItemStack(Material.WHITE_STAINED_GLASS_PANE);
            return glass;
        } else if (r == 2) {
            final ItemStack glass = new ItemStack(Material.ORANGE_STAINED_GLASS_PANE);
            return glass;
        } else if (r == 3) {
            final ItemStack glass = new ItemStack(Material.MAGENTA_STAINED_GLASS_PANE);
            return glass;
        } else if (r == 4) {
            final ItemStack glass = new ItemStack(Material.LIGHT_BLUE_STAINED_GLASS_PANE);
            return glass;
        } else if (r == 5) {
            final ItemStack glass = new ItemStack(Material.YELLOW_STAINED_GLASS_PANE);
            return glass;
        } else if (r == 6) {
            final ItemStack glass = new ItemStack(Material.LIME_STAINED_GLASS_PANE);
            return glass;
        } else if (r == 7) {
            final ItemStack glass = new ItemStack(Material.PINK_STAINED_GLASS_PANE);
            return glass;
        } else if (r == 8) {
            final ItemStack glass = new ItemStack(Material.LIGHT_GRAY_STAINED_GLASS_PANE);
            return glass;
        } else if (r == 9) {
            final ItemStack glass = new ItemStack(Material.LIGHT_GRAY_STAINED_GLASS_PANE);
            return glass;
        } else if (r == 10) {
            final ItemStack glass = new ItemStack(Material.CYAN_STAINED_GLASS_PANE);
            return glass;
        } else if (r == 11) {
            final ItemStack glass = new ItemStack(Material.PURPLE_STAINED_GLASS_PANE);
            return glass;
        } else if (r == 12) {
            final ItemStack glass = new ItemStack(Material.BLUE_STAINED_GLASS_PANE);
            return glass;
        } else if (r == 13) {
            final ItemStack glass = new ItemStack(Material.BROWN_STAINED_GLASS_PANE);
            return glass;
        } else if (r == 14) {
            final ItemStack glass = new ItemStack(Material.GREEN_STAINED_GLASS_PANE);
            return glass;
        } else if (r == 15) {
            final ItemStack glass = new ItemStack(Material.RED_STAINED_GLASS_PANE);
            return glass;
        } else if (r == 1) {
            final ItemStack glass = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
            return glass;
        }
        return null;
    }

    private static ItemStack getItemRandom(final int level) {
        if (level == 1) {
            final Random random = new Random();
            final int r = random.nextInt(8);

            if (r == 0 || r == 1) {
                final ItemStack helmet = new ItemStack(Material.IRON_HELMET);
                helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
                helmet.addEnchantment(Enchantment.DURABILITY, 1);

                return helmet;
            } else if (r == 2) {
                final ItemStack ironSword = new ItemStack(Material.IRON_SWORD);

                return ironSword;
            } else if (r == 3) {
                final ItemStack frag = new ItemStack(Material.FEATHER);
                final ItemMeta metaFrag = frag.getItemMeta();
                metaFrag.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
                final ArrayList<String> lore = new ArrayList<>();
                lore.add(" ");
                lore.add(ChatColor.GRAY + "Fragmento para asa, tipo: 1");
                lore.add(" ");
                metaFrag.setLore(lore);
                frag.setItemMeta(metaFrag);

                return frag;
            } else if (r == 4) {
                final ItemStack goldenC = new ItemStack(Material.GOLDEN_CARROT, 32);

                return goldenC;
            } else if (r == 5) {
                final ItemStack pick = new ItemStack(Material.IRON_PICKAXE);

                return pick;
            } else if (r == 6) {
                final ItemStack chestPlate = new ItemStack(Material.IRON_CHESTPLATE);
                chestPlate.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
                chestPlate.addEnchantment(Enchantment.DURABILITY, 1);

                return chestPlate;
            } else if (r == 7) {
                final ItemStack iron = new ItemStack(Material.IRON_INGOT, 32);

                return iron;
            } else if (r == 8) {
                final ItemStack xp = new ItemStack(Material.EXPERIENCE_BOTTLE, 8);

                return xp;
            }
        } else if (level == 2) {
            final Random random = new Random();
            final int r = random.nextInt(11);

            if (r == 0 || r == 1) {
                final ItemStack pickD = new ItemStack(Material.DIAMOND_PICKAXE);

                return pickD;
            } else if (r == 2) {
                final ItemStack ironB = new ItemStack(Material.IRON_BOOTS);
                ironB.addEnchantment(Enchantment.DURABILITY, 1);
                ironB.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);

                return ironB;
            } else if (r == 3) {
                final ItemStack frag = new ItemStack(Material.FEATHER, 5);
                final ItemMeta metaFrag = frag.getItemMeta();
                metaFrag.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
                final ArrayList<String> lore = new ArrayList<>();
                lore.add(" ");
                lore.add(ChatColor.GRAY + "Fragmento para asa, tipo: 1");
                lore.add(" ");
                metaFrag.setLore(lore);
                frag.setItemMeta(metaFrag);

                return frag;
            } else if (r == 4) {
                final ItemStack fragN = new ItemStack(Material.PHANTOM_MEMBRANE);
                final ItemMeta metaFragN = fragN.getItemMeta();
                metaFragN.setDisplayName(ChatColor.GREEN + "Fragmento de nível");
                final ArrayList<String> lore1 = new ArrayList<>();
                lore1.add(" ");
                lore1.add(ChatColor.GRAY + "Fragmento de nível p/ asa, nível: 1");
                lore1.add(" ");
                metaFragN.setLore(lore1);
                fragN.setItemMeta(metaFragN);

                return fragN;
            } else if (r == 5) {
                final ItemStack swordD = new ItemStack(Material.DIAMOND_SWORD);

                return swordD;
            } else if (r == 6) {
                final ItemStack frag1 = new ItemStack(Material.FEATHER);
                final ItemMeta metaFrag1 = frag1.getItemMeta();
                metaFrag1.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
                final ArrayList<String> lore2 = new ArrayList<>();
                lore2.add(" ");
                lore2.add(ChatColor.GRAY + "Fragmento para asa, tipo: 2");
                lore2.add(" ");
                metaFrag1.setLore(lore2);
                frag1.setItemMeta(metaFrag1);

                return frag1;
            } else if (r == 7) {
                final ItemStack ironL = new ItemStack(Material.IRON_LEGGINGS);
                ironL.addEnchantment(Enchantment.DURABILITY, 1);
                ironL.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);

                return ironL;
            } else if (r == 8) {
                final ItemStack goldenA = new ItemStack(Material.GOLDEN_APPLE);

                return goldenA;
            } else if (r == 9) {
                final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 3);
                final ItemMeta metaChave = chave.getItemMeta();
                metaChave.setDisplayName(ChatColor.GREEN + "Chave");
                final List<String> lore3 = new ArrayList<>();
                lore3.add(" ");
                lore3.add(ChatColor.DARK_GRAY + "Utilizado para abrir caixas");
                lore3.add(ChatColor.YELLOW + "50% sucesso");
                lore3.add(" ");
                metaChave.setLore(lore3);
                chave.setItemMeta(metaChave);

                return chave;
            } else if (r == 10) {
                final ItemStack iron = new ItemStack(Material.IRON_INGOT, 32);

                return iron;
            } else if (r == 11) {
                final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
                final ItemMeta meta2 = moeda.getItemMeta();
                meta2.setDisplayName(ChatColor.YELLOW + "$2.000,00 moedas");
                moeda.setItemMeta(meta2);

                return moeda;
            }
        } else if (level == 3) {
            final Random random = new Random();
            final int r = random.nextInt(11);

            if (r == 0 || r == 1) {
                final ItemStack helmetD = new ItemStack(Material.DIAMOND_HELMET);
                helmetD.addEnchantment(Enchantment.DURABILITY, 2);
                helmetD.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);

                return helmetD;
            } else if (r == 2) {
                final ItemStack caixa = new ItemStack(Material.CHEST);
                final ItemMeta metaCaixa = caixa.getItemMeta();
                metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
                final List<String> lore = new ArrayList<String>();
                lore.add(" ");
                lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.DARK_GRAY + "Básica");
                lore.add(" ");
                metaCaixa.setLore(lore);
                caixa.setItemMeta(metaCaixa);

                return caixa;
            } else if (r == 3) {
                final ItemStack tntComum = new ItemStack(Material.TNT, 32);
                final ItemMeta meta5 = tntComum.getItemMeta();
                meta5.setDisplayName(ChatColor.RED + "Explosivo comum");
                final List<String> lore4 = new ArrayList<>();
                lore4.add(" ");
                lore4.add(ChatColor.GRAY + "Dano: 1/5");
                lore4.add(" ");
                meta5.setLore(lore4);
                tntComum.setItemMeta(meta5);

                return tntComum;
            } else if (r == 4) {
                final ItemStack fragN = new ItemStack(Material.PHANTOM_MEMBRANE);
                final ItemMeta metaFragN = fragN.getItemMeta();
                metaFragN.setDisplayName(ChatColor.GREEN + "Fragmento de nível");
                final ArrayList<String> lore1 = new ArrayList<>();
                lore1.add(" ");
                lore1.add(ChatColor.GRAY + "Fragmento de nível p/ asa, nível: 2");
                lore1.add(" ");
                metaFragN.setLore(lore1);
                fragN.setItemMeta(metaFragN);

                return fragN;
            } else if (r == 5) {
                final ItemStack iron = new ItemStack(Material.IRON_INGOT, 64);

                return iron;
            } else if (r == 6) {
                final ItemStack frag1 = new ItemStack(Material.FEATHER, 5);
                final ItemMeta metaFrag1 = frag1.getItemMeta();
                metaFrag1.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
                final ArrayList<String> lore2 = new ArrayList<>();
                lore2.add(" ");
                lore2.add(ChatColor.GRAY + "Fragmento para asa, tipo: 2");
                lore2.add(" ");
                metaFrag1.setLore(lore2);
                frag1.setItemMeta(metaFrag1);

                return frag1;
            } else if (r == 7) {
                final ItemStack gold = new ItemStack(Material.GOLD_INGOT, 16);

                return gold;
            } else if (r == 8) {
                final ItemStack goldenA = new ItemStack(Material.GOLDEN_APPLE, 16);

                return goldenA;
            } else if (r == 9) {
                final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 6);
                final ItemMeta metaChave = chave.getItemMeta();
                metaChave.setDisplayName(ChatColor.GREEN + "Chave");
                final List<String> lore3 = new ArrayList<>();
                lore3.add(" ");
                lore3.add(ChatColor.DARK_GRAY + "Utilizado para abrir caixas");
                lore3.add(ChatColor.YELLOW + "50% sucesso");
                lore3.add(" ");
                metaChave.setLore(lore3);
                chave.setItemMeta(metaChave);

                return chave;
            } else if (r == 10) {
                final ItemStack iron = new ItemStack(Material.IRON_INGOT, 32);

                return iron;
            } else if (r == 11) {
                final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
                final ItemMeta meta2 = moeda.getItemMeta();
                meta2.setDisplayName(ChatColor.YELLOW + "$5.000,00 moedas");
                moeda.setItemMeta(meta2);

                return moeda;
            }
        } else if (level == 4) {
            final Random random = new Random();
            final int r = random.nextInt(8);

            if (r == 0 || r == 1) {
                final ItemStack tntComum = new ItemStack(Material.TNT, 32);
                final ItemMeta meta5 = tntComum.getItemMeta();
                meta5.setDisplayName(ChatColor.RED + "Explosivo comum");
                final List<String> lore4 = new ArrayList<>();
                lore4.add(" ");
                lore4.add(ChatColor.GRAY + "Dano: 1/5");
                lore4.add(" ");
                meta5.setLore(lore4);
                tntComum.setItemMeta(meta5);

                return tntComum;
            } else if (r == 2) {
                final ItemStack appleG = new ItemStack(Material.ENCHANTED_GOLDEN_APPLE, 16);

                return appleG;
            } else if (r == 3) {
                final ItemStack frag1 = new ItemStack(Material.FEATHER, 8);
                final ItemMeta metaFrag1 = frag1.getItemMeta();
                metaFrag1.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
                final ArrayList<String> lore2 = new ArrayList<>();
                lore2.add(" ");
                lore2.add(ChatColor.GRAY + "Fragmento para asa, tipo: 2");
                lore2.add(" ");
                metaFrag1.setLore(lore2);
                frag1.setItemMeta(metaFrag1);

                return frag1;
            } else if (r == 4) {
                final ItemStack gold = new ItemStack(Material.GOLD_INGOT, 32);

                return gold;
            } else if (r == 5) {
                final ItemStack fragN = new ItemStack(Material.PHANTOM_MEMBRANE);
                final ItemMeta metaFragN = fragN.getItemMeta();
                metaFragN.setDisplayName(ChatColor.GREEN + "Fragmento de nível");
                final ArrayList<String> lore1 = new ArrayList<>();
                lore1.add(" ");
                lore1.add(ChatColor.GRAY + "Fragmento de nível p/ asa, nível: 3");
                lore1.add(" ");
                metaFragN.setLore(lore1);
                fragN.setItemMeta(metaFragN);

                return fragN;
            } else if (r == 6) {
                final ItemStack caixa = new ItemStack(Material.CHEST);
                final ItemMeta metaCaixa = caixa.getItemMeta();
                metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
                final List<String> lore = new ArrayList<String>();
                lore.add(" ");
                lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.DARK_GRAY + "Básica");
                lore.add(" ");
                metaCaixa.setLore(lore);
                caixa.setItemMeta(metaCaixa);

                return caixa;
            } else if (r == 7) {
                final ItemStack frag1 = new ItemStack(Material.FEATHER, 5);
                final ItemMeta metaFrag1 = frag1.getItemMeta();
                metaFrag1.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
                final ArrayList<String> lore2 = new ArrayList<>();
                lore2.add(" ");
                lore2.add(ChatColor.GRAY + "Fragmento para asa, tipo: 2");
                lore2.add(" ");
                metaFrag1.setLore(lore2);
                frag1.setItemMeta(metaFrag1);

                return frag1;
            } else if (r == 8) {
                final ItemStack diamond = new ItemStack(Material.DIAMOND, 5);

                return diamond;
            } else if (r == 9) {
                final ItemStack enderpearl = new ItemStack(Material.ENDER_PEARL, 8);

                return enderpearl;
            } else if (r == 10) {
                final ItemStack leggingD = new ItemStack(Material.DIAMOND_LEGGINGS);
                leggingD.addEnchantment(Enchantment.DURABILITY, 2);
                leggingD.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);

                return leggingD;
            } else if (r == 11) {
                final ItemStack pickD = new ItemStack(Material.DIAMOND_PICKAXE);
                pickD.addEnchantment(Enchantment.DURABILITY, 2);
                pickD.addEnchantment(Enchantment.DIG_SPEED, 5);

                return pickD;
            } else if (r == 12) {
                final ItemStack xp = new ItemStack(Material.EXPERIENCE_BOTTLE, 16);

                return xp;
            }
        } else if (level == 5) {
            final Random random = new Random();
            final int r = random.nextInt(8);

            if (r == 0 || r == 1) {
                final ItemStack caixa = new ItemStack(Material.ENDER_CHEST);
                final ItemMeta metaCaixa = caixa.getItemMeta();
                metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
                final List<String> lore = new ArrayList<String>();
                lore.add(" ");
                lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.DARK_GRAY + "Avançada");
                lore.add(" ");
                metaCaixa.setLore(lore);
                caixa.setItemMeta(metaCaixa);

                return caixa;
            } else if (r == 2) {
                final ItemStack axeA = new ItemStack(Material.DIAMOND_AXE);
                axeA.addEnchantment(Enchantment.DURABILITY, 3);
                axeA.addEnchantment(Enchantment.DAMAGE_UNDEAD, 4);

                return axeA;
            } else if (r == 3) {
                final ItemStack pickD = new ItemStack(Material.DIAMOND_PICKAXE);

                return pickD;
            } else if (r == 4) {
                final ItemStack fragN = new ItemStack(Material.PHANTOM_MEMBRANE, 3);
                final ItemMeta metaFragN = fragN.getItemMeta();
                metaFragN.setDisplayName(ChatColor.GREEN + "Fragmento de nível");
                final ArrayList<String> lore1 = new ArrayList<>();
                lore1.add(" ");
                lore1.add(ChatColor.GRAY + "Fragmento de nível p/ asa, nível: 3");
                lore1.add(" ");
                metaFragN.setLore(lore1);
                fragN.setItemMeta(metaFragN);

                return fragN;
            } else if (r == 5) {
                final ItemStack gold = new ItemStack(Material.GOLD_INGOT, 32);

                return gold;
            } else if (r == 6) {
                final ItemStack frag1 = new ItemStack(Material.FEATHER, 10);
                final ItemMeta metaFrag1 = frag1.getItemMeta();
                metaFrag1.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
                final ArrayList<String> lore2 = new ArrayList<>();
                lore2.add(" ");
                lore2.add(ChatColor.GRAY + "Fragmento para asa, tipo: 2");
                lore2.add(" ");
                metaFrag1.setLore(lore2);
                frag1.setItemMeta(metaFrag1);

                return frag1;
            } else if (r == 7) {
                final ItemStack appleG = new ItemStack(Material.ENCHANTED_GOLDEN_APPLE, 16);

                return appleG;
            } else if (r == 8) {
                final ItemStack diamondB = new ItemStack(Material.DIAMOND_BLOCK);

                return diamondB;
            } else if (r == 9) {
                final ItemStack bootsD = new ItemStack(Material.DIAMOND_BOOTS);
                bootsD.addEnchantment(Enchantment.DURABILITY, 3);
                bootsD.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);

                return bootsD;
            } else if (r == 10) {
                final ItemStack xp = new ItemStack(Material.EXPERIENCE_BOTTLE, 32);

                return xp;
            } else if (r == 11) {
                final ItemStack enderpearl = new ItemStack(Material.ENDER_PEARL, 16);

                return enderpearl;
            } else if (r == 12) {
                final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
                final ItemMeta meta2 = moeda.getItemMeta();
                meta2.setDisplayName(ChatColor.YELLOW + "$10.000,00 moedas");
                moeda.setItemMeta(meta2);

                return moeda;
            }
        }
        return null;
    }

    public static int getStandsInArea(final Location location) {
        int quantia = 0;

        for (final Entity entity : location.getNearbyEntities(50, 50, 50)) {
            if (entity.getType().equals(EntityType.ARMOR_STAND)) {
                quantia++;
            }
        }

        return quantia;
    }

    public static boolean containsPlayer(final Location locGhast) {
        boolean have = false;
        for (final Entity entity : locGhast.getNearbyEntities(50, 50, 50)) {
            if (entity instanceof Player) {
                have = true;
            }
        }

        return have;
    }

    public static void cancelRotating(final int rotate) {
        Bukkit.getScheduler().cancelTask(rotate);
    }

    private static void updateMobsSpawning(final Location location, final int level) {
        if (location.getNearbyEntities(40, 40, 40).size() < 25) {
            for (int i = 1; i <= 5; i++) {
                final Entity entity = location.getWorld().spawnEntity(location, EntityType.VEX);
                entity.setCustomName(ChatColor.YELLOW + "Nível-" + level);
                entity.setCustomNameVisible(false);
            }
        }
    }

    @EventHandler
    public void onInteract(final PlayerInteractEvent event) {
        final Player player = event.getPlayer();

        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (event.getClickedBlock().getType().equals(Material.CHEST)) {
                if (DungeonsUtils.playerInArea(event.getClickedBlock().getLocation()) != null) {
                    if (DungeonAPI.getTimeUnlock(DungeonsUtils.playerInArea(event.getClickedBlock().getLocation()).getLocal()) > (new Date().getTime() / 1000)) {
                        if (DungeonsUtils.playerInArea(event.getClickedBlock().getLocation()).isGetted() == false) {
                            event.setCancelled(true);
                            DungeonsUtils.playerInArea(event.getClickedBlock().getLocation()).setIsGetted(true);
                            randomizer(player, DungeonsUtils.playerInArea(event.getClickedBlock().getLocation()).getLevel());
                        } else {
                            player.playSound(player.getLocation(), Sound.BLOCK_CHEST_LOCKED, 1.0f, 1.0f);
                            event.setCancelled(true);
                        }
                    } else {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onDup(final InventoryClickEvent event) {
        if (event.getClickedInventory() != null && event.getClickedInventory().getName().equalsIgnoreCase("Sorteador: ")) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onEntryDungeon(final PlayerMoveEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (DungeonsUtils.playerInArea(player.getLocation()) != null) {
            if (PlayerInRegionHash.get(UUID) == null) {
                new PlayerInRegionHash(UUID, true).insert();
                player.sendTitle(ChatColor.YELLOW + "Dungeon", ChatColor.GRAY + "Nível " + DungeonsUtils.playerInArea(player.getLocation()).getLevel(), 20, 60, 20);
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            } else if (PlayerInRegionHash.get(UUID).getStatus() == false) {
                PlayerInRegionHash.get(UUID).setStatus(true);
                player.sendTitle(ChatColor.YELLOW + "Dungeon", ChatColor.GRAY + "Nível " + DungeonsUtils.playerInArea(player.getLocation()).getLevel(), 20, 60, 20);
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
            if (DungeonAPI.getIsActivated(DungeonsUtils.playerInArea(player.getLocation()).getLocal()) == false && DungeonAPI.getTimeUnlock(DungeonsUtils.playerInArea(player.getLocation()).getLocal()) < (new Date().getTime() / 1000)) {
                DungeonAPI.updateActivated(DungeonsUtils.playerInArea(player.getLocation()).getLocal(), true);

                //5
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (DungeonsUtils.playerInArea(player.getLocation()).getStand() != null) {
                            DungeonsUtils.playerInArea(player.getLocation()).getStand().remove();
                        }

                        final String[] text = {ChatColor.GREEN + "Ativado"};
                        if (DungeonsUtils.playerInArea(player.getLocation()).getStand() != null) {
                            final String[] split = DungeonsUtils.playerInArea(player.getLocation()).getLocal().split(":");
                            final Location location = new Location(Bukkit.getWorld(split[0]), Integer.valueOf(split[1]), Integer.valueOf(split[2]), Integer.valueOf(split[3]));

                            for (final Entity entity : location.getNearbyEntities(50, 50, 50)) {
                                if (entity.getType().equals(EntityType.ARMOR_STAND)) {
                                    entity.remove();
                                }
                            }

                            final ArmorStand armorStand = Hologram.createHologramReturnStand(location.add(0.5, 1.65, 0.5), text);
                            DungeonsUtils.playerInArea(player.getLocation()).getStand().remove();
                            DungeonsUtils.playerInArea(player.getLocation()).setStand(armorStand);
                        } else {
                            final String[] split = DungeonsUtils.playerInArea(player.getLocation()).getLocal().split(":");
                            final Location location = new Location(Bukkit.getWorld(split[0]), Integer.valueOf(split[1]), Integer.valueOf(split[2]), Integer.valueOf(split[3]));

                            for (final Entity entity : location.getNearbyEntities(50, 50, 50)) {
                                if (entity.getType().equals(EntityType.ARMOR_STAND)) {
                                    entity.remove();
                                }
                            }

                            final ArmorStand armorStand = Hologram.createHologramReturnStand(location.add(0.5, 1.65, 0.5), text);
                            DungeonsUtils.playerInArea(player.getLocation()).setStand(armorStand);
                        }
                    }
                }, 20L);

                final String[] split = DungeonsUtils.playerInArea(player.getLocation()).getLocal().split(":");
                final Location location = new Location(Bukkit.getWorld(split[0]), Integer.valueOf(split[1]), Integer.valueOf(split[2]), Integer.valueOf(split[3]));
                final Entity entity = location.getWorld().spawnEntity(location.add(0, 3, 0), EntityType.GHAST);

                //String[] lifeText = {ChatColor.YELLOW + String.valueOf(DungeonsUtils.playerInArea(player.getLocation()).getLevel() * 20)};
                //ArmorStand lifeGhast = Hologram.createHologramReturnStand(location, lifeText);
                //lifeGhast.setCustomNameVisible(true);
                //entity.setPassenger(lifeGhast);

                entity.setCustomName(ChatColor.YELLOW + "Nível-" + DungeonsUtils.playerInArea(location).getLevel() + "::" + DungeonsUtils.playerInArea(player.getLocation()).getLocal());
                //entity.setCustomNameVisible(false);
                DungeonsUtils.playerInArea(player.getLocation()).setEntity(entity);

                final Location locProv = location.clone();
                final String local = DungeonsUtils.playerInArea(player.getLocation()).getLocal();
                final Location loc = location.clone().add(0, 1.5, 0);
                final Location locGhast = location.clone().add(0, 6, 0);
                final Location locStandCount = new Location(locProv.getWorld(), locProv.getBlockX() + 0.5, locProv.getBlockY() - 1.80, locProv.getBlockZ() + 0.5);
                final DungeonsHash hash = DungeonsUtils.playerInArea(player.getLocation());

                FixTaskLag.addTask(local, locStandCount, locGhast);

                //6
               // final int rotat = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                //                    int seconds = 9;
                //                    int y;
                //
                //                    @Override
                //                    public void run() {
                //                        if (containsPlayer(locGhast) == false) {
                //                            DungeonAPI.updateActivated(local, false);
                //                            if (DungeonsHash.get(local).getStand() != null) {
                //                                DungeonsHash.get(local).getStand().remove();
                //                            }
                //                            if (DungeonsHash.get(local).getStandCount() != null) {
                //                                DungeonsHash.get(local).getStandCount().remove();
                //                            }
                //                            if (DungeonsHash.get(local).getEntity() != null) {
                //                                DungeonsHash.get(local).getEntity().remove();
                //                                DungeonsHash.get(local).setEntity(null);
                //                            }
                //                            return;
                //                        }
                //
                //                        if (DungeonAPI.getIsActivated(local) == false) {
                //                            if (DungeonsHash.get(local).getStandCount() != null) {
                //                                DungeonsHash.get(local).getStandCount().remove();
                //                            }
                //                            DungeonsHash.get(local).setStandCount(null);
                //                            cancelRotating(RotateSecondsHash.get(local).getId());
                //                            return;
                //                        }
                //                        final String[] texts = {ChatColor.GRAY + String.valueOf(this.seconds)};
                //
                //                        if (this.y == 0) {
                //                            this.y = locStandCount.getBlockY();
                //                        }
                //
                //                        locStandCount.setY(this.y);
                //                        final ArmorStand hologramCount = Hologram.createHologramReturnStand(locStandCount, texts);
                //                        if (DungeonsHash.get(local).getStandCount() != null) {
                //                            DungeonsHash.get(local).getStandCount().remove();
                //                        }
                //                        DungeonsHash.get(local).setStandCount(hologramCount);
                //                        this.seconds--;
                //                        if (this.seconds < 0) {
                //                            this.seconds = 10;
                //                        }
                //                    }
                //                }, 20L, 20L);
                //
                //if (RotateSecondsHash.get(UUID) == null) {
                //                    new RotateSecondsHash(DungeonsUtils.playerInArea(locProv).getLocal(), rotat).insert();
                //                } else {
                //                    RotateSecondsHash.get(DungeonsUtils.playerInArea(locProv).getLocal()).setId(rotat);
                //                }

                //7
                final int rotate = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (DungeonAPI.getIsActivated(hash.getLocal()) == true) {
                            updateMobsSpawning(loc, hash.getLevel());
                        } else {
                            entity.remove();
                            cancelRotating(RotateHash.get(hash.getLocal()).getId());
                            return;
                        }
                        if (entity.getLocation().distance(location) > 50) {
                            entity.teleport(locGhast);
                        }
                    }
                }, 0L, 200L);

                if (RotateHash.get(DungeonsUtils.playerInArea(location).getLocal()) == null) {
                    new RotateHash(DungeonsUtils.playerInArea(location).getLocal(), rotate).insert();
                } else {
                    RotateHash.get(DungeonsUtils.playerInArea(location).getLocal()).setId(rotate);
                }
            } else if (DungeonAPI.getIsActivated(DungeonsUtils.playerInArea(player.getLocation()).getLocal()) == false && DungeonAPI.getTimeUnlock(DungeonsUtils.playerInArea(player.getLocation()).getLocal()) > new Date().getTime()) {
                final String[] split = DungeonsUtils.playerInArea(player.getLocation()).getLocal().split(":");
                final Location location = new Location(Bukkit.getWorld(split[0]), Integer.valueOf(split[1]), Integer.valueOf(split[2]), Integer.valueOf(split[3]));

                if (getStandsInArea(location) > 1) {
                    if (PlayerInRegionHash.get(UUID).getStatus() == false) {
                        for (final Entity entity : location.getNearbyEntities(50, 50, 50)) {
                            if (entity.getType().equals(EntityType.ARMOR_STAND)) {
                                entity.remove();
                            }
                        }

                        //8
                        final int rotate = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                DungeonsHash.get(split[1]).getStand().setCustomName(ChatColor.YELLOW + HorarioAPI.getTime(DungeonAPI.getTimeUnlock(DungeonsUtils.playerInArea(player.getLocation()).getLocal())));
                            }
                        }, 0L, 20L);

                        if (RotateReEntryHash.get(UUID) == null) {
                            new RotateReEntryHash(UUID, rotate);
                        } else {
                            RotateReEntryHash.get(UUID).setId(rotate);
                        }
                        //-
                    }
                }
            }
        } else {
            if (RotateReEntryHash.get(UUID) != null && RotateReEntryHash.get(UUID).getId() != 0) {
                Bukkit.getScheduler().cancelTask(RotateReEntryHash.get(UUID).getId());
                RotateReEntryHash.get(UUID).setId(0);
            }
            if (PlayerInRegionHash.get(UUID) != null) {
                if (PlayerInRegionHash.get(UUID).getStatus() == true) {
                    PlayerInRegionHash.get(UUID).setStatus(false);
                }
            }
        }
    }
}
