package com.mixedup.elytra.listeners;

import com.mixedup.Main;
import com.mixedup.elytra.DungeonAPI;
import com.mixedup.elytra.regions.DungeonsHash;
import com.mixedup.utils.Hologram;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;

import java.util.ArrayList;
import java.util.HashMap;

public class FixTaskLag {

    private static ArrayList<String> list = new ArrayList<>();

    public static void addTask(String local, Location locStandCount, Location locGhast) {
        new FixTaskLag(local, locStandCount, locGhast, 9).insert();
        list.add(local);
    }

    public static void taskDungeons() {
        final Thread thread = new Thread(() -> {
            while (true) {
                try {
                    Thread.currentThread().sleep(1000 * 1);
                } catch (final Exception e) {
                    e.printStackTrace();
                }

                ArrayList<String> listRemove = new ArrayList<>();
                for (String local : list) {
                    int seconds = get(local).getSeconds();

                    //if (PlayerEntryDungeon.containsPlayer(locGhast) == false) {
                    if (PlayerEntryDungeon.containsPlayer(get(local).getLocGhast()) == false) {
                        DungeonAPI.updateActivated(local, false);
                        if (DungeonsHash.get(local).getStand() != null) {
                            DungeonsHash.get(local).getStand().remove();
                        }
                        if (DungeonsHash.get(local).getStandCount() != null) {
                            DungeonsHash.get(local).getStandCount().remove();
                        }
                        if (DungeonsHash.get(local).getEntity() != null) {
                            DungeonsHash.get(local).getEntity().remove();
                            DungeonsHash.get(local).setEntity(null);
                        }
                        listRemove.add(local);
                        cache.remove(local);
                        continue;
                    }

                    if (DungeonAPI.getIsActivated(local) == false) {
                        if (DungeonsHash.get(local).getStandCount() != null) {
                            DungeonsHash.get(local).getStandCount().remove();
                        }
                        DungeonsHash.get(local).setStandCount(null);
                        listRemove.add(local);
                        cache.remove(local);
                        //PlayerEntryDungeon.cancelRotating(RotateSecondsHash.get(local).getId());
                        continue;
                    }
                    final String texts = ChatColor.GRAY + String.valueOf(seconds);

                    Bukkit.getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
                        @Override
                        public void run() {
                            final ArmorStand hologramCount = Hologram.createHologramReturnStandNoNegative(get(local).getLocStandCount(), texts);
                            DungeonsHash.get(local).setStandCount(hologramCount);
                        }
                    }, 1L);
                    if (DungeonsHash.get(local).getStandCount() != null) {
                        DungeonsHash.get(local).getStandCount().remove();
                    }
                    seconds--;
                    get(local).setSeconds(seconds);
                    if (seconds < 0) {
                        get(local).setSeconds(10);
                    }
                }

                if (listRemove.size() > 0) {
                    for (String r : listRemove) {
                        list.remove(r);
                    }
                    listRemove.clear();
                }
            }
        });
        thread.setName("dungeon-thread");
        thread.start();
    }

    private String local;
    private Location locStandCount;
    private Location locGhast;
    private int seconds;

    public static HashMap<String, FixTaskLag> cache = new HashMap<>();

    public FixTaskLag(String local, Location locStandCount, Location locGhast, int seconds) {
        this.local = local;
        this.locStandCount = locStandCount;
        this.locGhast = locGhast;
        this.seconds = seconds;
    }

    public static FixTaskLag get(String local) {
        return cache.get(local);
    }

    public FixTaskLag insert() {
        return cache.put(this.local, this);
    }

    public int getSeconds() {
        return this.seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    public Location getLocGhast() {
        return this.locGhast;
    }

    public void setLocGhast(Location locGhast) {
        this.locGhast = locGhast;
    }

    public Location getLocStandCount() {
        return this.locStandCount;
    }

    public void setLocStandCount(Location locStandCount) {
        this.locStandCount = locStandCount;
    }
}
