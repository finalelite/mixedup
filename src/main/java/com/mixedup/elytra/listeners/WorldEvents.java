package com.mixedup.elytra.listeners;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Trident;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityExplodeEvent;

import java.util.ArrayList;

public class WorldEvents implements Listener {

    @EventHandler
    public void onEntityExplode(final EntityExplodeEvent event) {
        if (event.getLocation().getWorld().getName().equalsIgnoreCase("aether")) {
            for (final Block block : new ArrayList<>(event.blockList())) {
                event.blockList().remove(block);
            }
        }
    }

    @EventHandler
    public void onBreak(final BlockBreakEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getBlock().getWorld().getName().equalsIgnoreCase("aether")) {
            if (!TagAPI.getTag(UUID).equalsIgnoreCase("Master") && !TagAPI.getTag(UUID).equalsIgnoreCase("Supervisor")) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPlace(final BlockPlaceEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getBlock().getWorld().getName().equalsIgnoreCase("aether")) {
            if (!TagAPI.getTag(UUID).equalsIgnoreCase("Master") && !TagAPI.getTag(UUID).equalsIgnoreCase("Supervisor")) {
                event.setCancelled(true);
            }
        }
    }


    @EventHandler
    public void onDamage(final EntityDamageByEntityEvent event) {
        if (event.getDamager().getWorld().getName().equalsIgnoreCase("aether")) {
            if (event.getCause().equals(DamageCause.PROJECTILE)) {
                if (event.getDamager() instanceof Arrow && event.getEntity() instanceof Player) {
                    if (event.getEntity().getWorld().getTime() > 13000) {
                        event.setCancelled(false);
                    } else {
                        event.setCancelled(true);
                    }
                } else if (event.getDamager() instanceof Trident && event.getEntity() instanceof Player) {
                    final Player player = (Player) ((Trident) event.getDamager()).getShooter();
                    if (event.getEntity().getWorld().getTime() > 13000) {
                        event.setCancelled(false);
                    } else {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, o pvp não esta ativado no momento!");
                    }
                }
            } else {
                if (event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
                    final Player player = (Player) event.getDamager();
                    if (event.getEntity().getWorld().getTime() > 13000) {
                        event.setCancelled(false);
                    } else {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, o pvp não esta ativado no momento!");
                    }
                }
            }
        }
    }
}
