package com.mixedup.elytra.listeners;

import com.destroystokyo.paper.event.player.PlayerArmorChangeEvent;
import com.destroystokyo.paper.event.player.PlayerArmorChangeEvent.SlotType;
import com.mixedup.Main;
import com.mixedup.apis.GuildaAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import com.mixedup.elytra.hashs.LastBoostHash;
import com.mixedup.hashs.LastHitHash;
import com.mixedup.mcmmo.ProbabilityApi;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.Date;

public class ElytraEvents implements Listener {

    @EventHandler
    public void onClickInv(InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getSlot() == 38) {
            if (event.getCursor() != null && event.getCursor().getType().equals(Material.ELYTRA)) {
                if (TagAPI.getTag(UUID).equalsIgnoreCase("Master") || TagAPI.getTag(UUID).equalsIgnoreCase("Supervisor") || TagAPI.getTag(UUID).equalsIgnoreCase("Admin") || TagAPI.getTag(UUID).equalsIgnoreCase("Moderador") || TagAPI.getTag(UUID).equalsIgnoreCase("Suporte")
                        || TagAPI.getTag(UUID).equalsIgnoreCase("Youtuber") || TagAPI.getTag(UUID).equalsIgnoreCase("Vip+++")) {
                    //-
                } else {
                    if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("Ancia") && event.getCursor().getItemMeta().getDisplayName().contains("Ancia")) {
                        //-
                    } else if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("Sanguinaria") && event.getCursor().getItemMeta().getDisplayName().contains("Sanguinaria")) {
                        //-
                    } else if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("Nobre") && event.getCursor().getItemMeta().getDisplayName().contains("Nobre")) {
                        //-
                    } else {
                        if (event.getClickedInventory() != null && event.getClickedInventory().getItem(event.getSlot()) != null) {
                            player.getInventory().addItem(event.getClickedInventory().getItem(event.getSlot()));
                            event.getClickedInventory().getItem(event.getSlot()).setType(Material.AIR);
                            event.getClickedInventory().getItem(event.getSlot()).setAmount(0);
                        }
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, você não pode utilizar elytra de outra Guilda!");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        return;
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onEquip(final PlayerArmorChangeEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getSlotType().equals(SlotType.CHEST)) {
            if (event.getNewItem().getType().equals(Material.ELYTRA)) {
                if (TagAPI.getTag(UUID).equalsIgnoreCase("Master") || TagAPI.getTag(UUID).equalsIgnoreCase("Supervisor") || TagAPI.getTag(UUID).equalsIgnoreCase("Admin") || TagAPI.getTag(UUID).equalsIgnoreCase("Moderador") || TagAPI.getTag(UUID).equalsIgnoreCase("Suporte")
                        || TagAPI.getTag(UUID).equalsIgnoreCase("Youtuber") || TagAPI.getTag(UUID).equalsIgnoreCase("Vip+++")) {
                    //-
                } else {
                    if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("Ancia") && event.getNewItem().getItemMeta().getDisplayName().contains("Ancia")) {
                        //-
                    } else if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("Sanguinaria") && event.getNewItem().getItemMeta().getDisplayName().contains("Sanguinaria")) {
                        //-
                    } else if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("Nobre") && event.getNewItem().getItemMeta().getDisplayName().contains("Nobre")) {
                        //-
                    } else {
                        event.getPlayer().getInventory().addItem(event.getNewItem());
                        if (event.getOldItem() != null) {
                            //event.getPlayer().getInventory().setChestplate(event.getOldItem());
                            event.getPlayer().getInventory().setChestplate(new ItemStack(Material.AIR));
                            event.getPlayer().getOpenInventory().getCursor().setType(Material.AIR);
                            event.getPlayer().getOpenInventory().getCursor().setAmount(0);
                        } else {
                            event.getPlayer().getInventory().setChestplate(new ItemStack(Material.AIR));
                        }
                        player.sendMessage(ChatColor.RED + " * Ops, você não pode utilizar elytra de outra Guilda!");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        return;
                    }
                }
            }
        }
    }

    @EventHandler
    public void onMove(final PlayerMoveEvent event) {
        final Player player = event.getPlayer();

        if (player.isGliding() && event.getPlayer().getInventory().getChestplate() != null && event.getPlayer().getInventory().getChestplate().getType().equals(Material.ELYTRA)) {
            final String UUID = PlayerUUID.getUUID(player.getName());
            if (LastHitHash.get(UUID) != null) {
                final Date date = LastHitHash.get(UUID).getDate();
                final int value = date.compareTo(new java.util.Date());

                if (value > 0) {
                    final ItemStack elytra = player.getInventory().getChestplate().clone();
                    player.getInventory().setChestplate(new ItemStack(Material.AIR));
                    player.sendMessage(ChatColor.RED + " * Ops, você está em combate, não é permitido isto!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.getInventory().setChestplate(elytra);
                        }
                    }, 5L);
                    return;
                }
            }
        }

        if (player.isGliding() && event.getPlayer().getInventory().getChestplate() != null && event.getPlayer().getInventory().getChestplate().getItemMeta().getDisplayName().contains("[Nivel 3]")) {
            final Vector unitVector = new Vector(0.0D, player.getLocation().getDirection().getY(), 0.0D);
            player.setVelocity(player.getVelocity().add(unitVector.multiply(0.1D)));

            if (event.getPlayer().getInventory().getChestplate().getItemMeta().getDisplayName().contains("Sanguinaria")) {
                player.getWorld().spawnParticle(Particle.FLAME, player.getLocation(), 1, 0, 0, 0);
            } else if (event.getPlayer().getInventory().getChestplate().getItemMeta().getDisplayName().contains("Ancia")) {
                player.getWorld().spawnParticle(Particle.CLOUD, player.getLocation(), 1, 0, 0, 0);
            } else if (event.getPlayer().getInventory().getChestplate().getItemMeta().getDisplayName().contains("Nobre")) {
                player.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, player.getLocation(), 1, 0, 0, 0);
            }
        }
    }

    @EventHandler
    public void onPvP(final EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
            if (((Player) event.getDamager()).getInventory().getChestplate() != null && ((Player) event.getDamager()).getInventory().getChestplate().getType().equals(Material.ELYTRA)) {
                if (((Player) event.getDamager()).getInventory().getChestplate().getItemMeta().getDisplayName().contains("[Nivel 2]")) {
                    if (ProbabilityApi.probab(5000) == true) {
                        if (((Player) event.getDamager()).getInventory().getChestplate().getItemMeta().getDisplayName().contains("Sanguinaria")) {
                            event.getEntity().setFireTicks(80);
                        } else if (((Player) event.getDamager()).getInventory().getChestplate().getItemMeta().getDisplayName().contains("Ancia")) {
                            final PotionEffect potionEffect = new PotionEffect(PotionEffectType.SLOW, 80, 1);
                            ((Player) event.getEntity()).addPotionEffect(potionEffect);
                        } else if (((Player) event.getDamager()).getInventory().getChestplate().getItemMeta().getDisplayName().contains("Nobre")) {
                            final PotionEffect potionEffect = new PotionEffect(PotionEffectType.POISON, 80, 1);
                            ((Player) event.getEntity()).addPotionEffect(potionEffect);
                        }
                    }
                } else if (((Player) event.getDamager()).getInventory().getChestplate().getItemMeta().getDisplayName().contains("[Nivel 3]")) {
                    if (ProbabilityApi.probab(10000) == true) {
                        if (((Player) event.getDamager()).getInventory().getChestplate().getItemMeta().getDisplayName().contains("Sanguinaria")) {
                            event.getEntity().setFireTicks(80);
                        } else if (((Player) event.getDamager()).getInventory().getChestplate().getItemMeta().getDisplayName().contains("Ancia")) {
                            final PotionEffect potionEffect = new PotionEffect(PotionEffectType.SLOW, 80, 1);
                            ((Player) event.getEntity()).addPotionEffect(potionEffect);
                        } else if (((Player) event.getDamager()).getInventory().getChestplate().getItemMeta().getDisplayName().contains("Nobre")) {
                            final PotionEffect potionEffect = new PotionEffect(PotionEffectType.POISON, 80, 1);
                            ((Player) event.getEntity()).addPotionEffect(potionEffect);
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onToggleSneak(final PlayerToggleSneakEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (!player.isOnGround() && player.getInventory().getChestplate() != null && player.getInventory().getChestplate().getType().equals(Material.ELYTRA) && event.getPlayer().getInventory().getChestplate().getItemMeta().getDisplayName().contains("[Nivel 3]")) {
            if (LastHitHash.get(UUID) != null) {
                final Date date = LastHitHash.get(UUID).getDate();
                final int value = date.compareTo(new java.util.Date());

                if (value > 0) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + " * Ops, você está em combate, não é permitido isto!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }
            }

            if (LastBoostHash.get(UUID) == null) {
                new LastBoostHash(UUID, 0).insert();
            }

            if (LastBoostHash.get(UUID).getId() == 0) {
                final Location loc = player.getLocation();
                final Vector dir = loc.getDirection().add(new Vector(0.0D, 0.1D, 0.0D));

                player.setVelocity(player.getVelocity().add(dir));
                loc.getWorld().playEffect(loc, Effect.DRAGON_BREATH, 0, 0);
                player.playSound(loc, Sound.ENTITY_ENDER_DRAGON_FLAP, 0.1F, 2.0F);

                LastBoostHash.get(UUID).setId(30);
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        LastBoostHash.get(UUID).setId(0);
                    }
                }, 600L);
            } else {
                player.sendActionBar(ChatColor.RED + "Boost sobrecarregado!");
            }
        }
    }
}
