package com.mixedup.elytra.listeners;

import com.mixedup.Main;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.elytra.hashs.CreatingPortalHash;
import com.mixedup.elytra.hashs.IsSelectedHash;
import com.mixedup.elytra.hashs.LocationSelectedHash;
import com.mixedup.elytra.hashs.PreventionRightClickHash;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class SelectPortalEvent implements Listener {

    @EventHandler
    public void onInteract(final PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getPlayer().getItemInHand() != null && !event.getPlayer().getItemInHand().getType().equals(Material.AIR) && event.getPlayer().getItemInHand().getItemMeta().getDisplayName() != null && event.getPlayer().getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "Marcador") && event.getPlayer().getItemInHand().getType().equals(Material.STICK) && CreatingPortalHash.get(UUID) != null && CreatingPortalHash.get(UUID).getStatus() == true) {
            event.setCancelled(true);
            if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {

                if (PreventionRightClickHash.get(UUID) == null) {
                    new PreventionRightClickHash(UUID, true).insert();

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            PreventionRightClickHash.get(UUID).setStatus(false);
                        }
                    }, 10L);
                } else {
                    if (PreventionRightClickHash.get(UUID).getStatus() == false) {
                        PreventionRightClickHash.get(UUID).setStatus(true);

                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                PreventionRightClickHash.get(UUID).setStatus(false);
                            }
                        }, 10L);
                    } else {
                        return;
                    }
                }

                final String loc = event.getClickedBlock().getWorld().getName() + ":" + event.getClickedBlock().getX() + ":" + event.getClickedBlock().getY() + ":" + event.getClickedBlock().getZ();
                if (LocationSelectedHash.get(UUID) == null) {
                    new LocationSelectedHash(UUID, "NULL", loc).insert();
                } else {
                    LocationSelectedHash.get(UUID).setPos2(loc);
                }

                if (!LocationSelectedHash.get(UUID).getPos1().equalsIgnoreCase("NULL")) {
                    player.sendMessage(ChatColor.YELLOW + " Posição 2: " + ChatColor.GRAY + event.getClickedBlock().getX() + "X " + event.getClickedBlock().getY() + "Y " + event.getClickedBlock().getZ() + "Z\n" +
                            "  Utilize /criar portal para finalizar. Se posicione no ponto de saida do portal!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    if (IsSelectedHash.get(UUID) == null) {
                        new IsSelectedHash(UUID, true).insert();
                    } else {
                        IsSelectedHash.get(UUID).setStatus(true);
                    }
                } else {
                    player.sendMessage(ChatColor.YELLOW + " Posição 2: " + ChatColor.GRAY + event.getClickedBlock().getX() + "X " + event.getClickedBlock().getY() + "Y " + event.getClickedBlock().getZ() + "Z");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else if (event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
                final String loc = event.getClickedBlock().getWorld().getName() + ":" + event.getClickedBlock().getX() + ":" + event.getClickedBlock().getY() + ":" + event.getClickedBlock().getZ();
                if (LocationSelectedHash.get(UUID) == null) {
                    new LocationSelectedHash(UUID, loc, "NULL").insert();
                } else {
                    LocationSelectedHash.get(UUID).setPos1(loc);
                }

                if (!LocationSelectedHash.get(UUID).getPos2().equalsIgnoreCase("NULL")) {
                    player.sendMessage(ChatColor.YELLOW + " Posição 1: " + ChatColor.GRAY + event.getClickedBlock().getX() + "X " + event.getClickedBlock().getY() + "Y " + event.getClickedBlock().getZ() + "Z\n" +
                            "  Utilize /criar portal para finalizar. Se posicione no ponto de saida do portal!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    if (IsSelectedHash.get(UUID) == null) {
                        new IsSelectedHash(UUID, true).insert();
                    } else {
                        IsSelectedHash.get(UUID).setStatus(true);
                    }
                } else {
                    player.sendMessage(ChatColor.YELLOW + " Posição 1: " + ChatColor.GRAY + event.getClickedBlock().getX() + "X " + event.getClickedBlock().getY() + "Y " + event.getClickedBlock().getZ() + "Z");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
    }
}
