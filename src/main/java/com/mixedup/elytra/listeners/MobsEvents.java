package com.mixedup.elytra.listeners;

import com.mixedup.Main;
import com.mixedup.apis.HorarioAPI;
import com.mixedup.elytra.DungeonAPI;
import com.mixedup.elytra.hashs.GhastDamageHash;
import com.mixedup.elytra.hashs.RotateKillGhast;
import com.mixedup.elytra.regions.DungeonsHash;
import com.mixedup.mcmmo.ProbabilityApi;
import org.bukkit.*;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

public class MobsEvents implements Listener {

    private static ArmorStand getArmorStand(final Location location) {
        int quantia = 0;

        for (final Entity entity : location.getNearbyEntities(50, 50, 50)) {
            if (entity.getType().equals(EntityType.ARMOR_STAND)) {
                quantia++;
            }
        }

        if (quantia == 1) {
            for (final Entity entity : location.getNearbyEntities(50, 50, 50)) {
                if (entity.getType().equals(EntityType.ARMOR_STAND)) {
                    return (ArmorStand) entity;
                }
            }
        }

        return null;
    }

    @EventHandler
    public void entityExplode(final EntityExplodeEvent event) {
        if (event.getEntity().getType().equals(EntityType.FIREBALL)) {
            if (event.getEntity().getWorld().getName().equalsIgnoreCase("aether")) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onSpawnEgg(final PlayerInteractEvent event) {
        if (event.getPlayer().getWorld().getName().equalsIgnoreCase("aether")) {
            if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
                if (event.getPlayer().getItemInHand() != null && !event.getPlayer().getItemInHand().getType().equals(Material.AIR) && event.getPlayer().getItemInHand().equals(Material.CREEPER_SPAWN_EGG) || event.getPlayer().getItemInHand().equals(Material.GHAST_SPAWN_EGG)) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onDamageGhast(final EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player || event.getDamager().getType().equals(EntityType.ARROW) || event.getDamager().getType().equals(EntityType.TIPPED_ARROW) || event.getDamager().getType().equals(EntityType.SPECTRAL_ARROW) || event.getDamager().getType().equals(EntityType.FISHING_HOOK)
                || event.getDamager().getType().equals(EntityType.PRIMED_TNT) || event.getDamager().getType().equals(EntityType.TRIDENT) || event.getDamager().getType().equals(EntityType.FIREBALL) || event.getDamager().getType().equals(EntityType.SMALL_FIREBALL) || event.getDamager().getType().equals(EntityType.DRAGON_FIREBALL)) {
            if (event.getEntity().getType().equals(EntityType.GHAST)) {
                if (event.getEntity().getWorld().getName().equalsIgnoreCase("aether")) {
                    if (event.getEntity().getCustomName() != null) {
                        final String[] split = event.getEntity().getCustomName().split("::");

                        if (split[0].equalsIgnoreCase(ChatColor.YELLOW + "nível-1")) {
                            event.setDamage(0);
                            if (GhastDamageHash.get(event.getEntity().getCustomName()) == null) {
                                new GhastDamageHash(event.getEntity().getCustomName(), 20).insert();
                            } else {
                                GhastDamageHash.get(event.getEntity().getCustomName()).setId(GhastDamageHash.get(event.getEntity().getCustomName()).getId() - 1);
                                if (GhastDamageHash.get(event.getEntity().getCustomName()).getId() <= 0) {
                                    event.setDamage(100);
                                    GhastDamageHash.get(event.getEntity().getCustomName()).setId(20);
                                }
                            }
                        } else if (split[0].equalsIgnoreCase(ChatColor.YELLOW + "nível-2")) {
                            event.setDamage(0);
                            if (GhastDamageHash.get(event.getEntity().getCustomName()) == null) {
                                new GhastDamageHash(event.getEntity().getCustomName(), 40).insert();
                            } else {
                                GhastDamageHash.get(event.getEntity().getCustomName()).setId(GhastDamageHash.get(event.getEntity().getCustomName()).getId() - 1);
                                if (GhastDamageHash.get(event.getEntity().getCustomName()).getId() <= 0) {
                                    event.setDamage(100);
                                    GhastDamageHash.get(event.getEntity().getCustomName()).setId(40);
                                }
                            }
                        } else if (split[0].equalsIgnoreCase(ChatColor.YELLOW + "nível-3")) {
                            event.setDamage(0);
                            if (GhastDamageHash.get(event.getEntity().getCustomName()) == null) {
                                new GhastDamageHash(event.getEntity().getCustomName(), 60).insert();
                            } else {
                                GhastDamageHash.get(event.getEntity().getCustomName()).setId(GhastDamageHash.get(event.getEntity().getCustomName()).getId() - 1);
                                if (GhastDamageHash.get(event.getEntity().getCustomName()).getId() <= 0) {
                                    event.setDamage(100);
                                    GhastDamageHash.get(event.getEntity().getCustomName()).setId(60);
                                }
                            }
                        } else if (split[0].equalsIgnoreCase(ChatColor.YELLOW + "nível-4")) {
                            event.setDamage(0);
                            if (GhastDamageHash.get(event.getEntity().getCustomName()) == null) {
                                new GhastDamageHash(event.getEntity().getCustomName(), 80).insert();
                            } else {
                                GhastDamageHash.get(event.getEntity().getCustomName()).setId(GhastDamageHash.get(event.getEntity().getCustomName()).getId() - 1);
                                if (GhastDamageHash.get(event.getEntity().getCustomName()).getId() <= 0) {
                                    event.setDamage(100);
                                    GhastDamageHash.get(event.getEntity().getCustomName()).setId(80);
                                }
                            }
                        } else if (split[0].equalsIgnoreCase(ChatColor.YELLOW + "nível-5")) {
                            event.setDamage(0);
                            if (GhastDamageHash.get(event.getEntity().getCustomName()) == null) {
                                new GhastDamageHash(event.getEntity().getCustomName(), 100).insert();
                            } else {
                                GhastDamageHash.get(event.getEntity().getCustomName()).setId(GhastDamageHash.get(event.getEntity().getCustomName()).getId() - 1);
                                if (GhastDamageHash.get(event.getEntity().getCustomName()).getId() <= 0) {
                                    event.setDamage(100);
                                    GhastDamageHash.get(event.getEntity().getCustomName()).setId(100);
                                }
                            }
                        }
                    }
                }
            }
        } else if (event.getDamager().getType().equals(EntityType.GHAST)) {
            if (event.getEntity() instanceof Player) {
                if (event.getEntity().getWorld().getName().equalsIgnoreCase("aether")) {
                    if (event.getEntity().getCustomName() != null) {
                        final String[] split = event.getEntity().getCustomName().split("::");

                        if (split[0].equalsIgnoreCase("nivel-1")) {
                            event.setDamage(event.getDamage());
                        } else if (split[0].equalsIgnoreCase(ChatColor.YELLOW + "nível-2")) {
                            event.setDamage(event.getDamage() + 2);
                        } else if (split[0].equalsIgnoreCase(ChatColor.YELLOW + "nível-3")) {
                            event.setDamage(event.getDamage() + 4);
                        } else if (split[0].equalsIgnoreCase(ChatColor.YELLOW + "nível-4")) {
                            event.setDamage(event.getDamage() + 6);
                        } else if (split[0].equalsIgnoreCase(ChatColor.YELLOW + "nível-5")) {
                            event.setDamage(event.getDamage() + 8);
                        }
                    }
                }
            }
        } else if (event.getDamager().getType().equals(EntityType.VEX)) {
            if (event.getEntity() instanceof Player) {
                if (event.getEntity().getWorld().getName().equalsIgnoreCase("aether")) {
                    if (event.getEntity().getCustomName() != null) {
                        final String[] split = event.getEntity().getCustomName().split(":");
                        final Date date = new Date();
                        final String horario = date.getHours() + ":" + date.getMinutes();

                        if (split[0].equalsIgnoreCase("nivel-1")) {
                            event.setDamage(event.getDamage() + 1);
                        } else if (split[0].equalsIgnoreCase("nivel-2")) {
                            event.setDamage(event.getDamage() + 2);
                        } else if (split[0].equalsIgnoreCase("nivel-3")) {
                            event.setDamage(event.getDamage() + 3);
                        } else if (split[0].equalsIgnoreCase("nivel-4")) {
                            event.setDamage(event.getDamage() + 4);
                        } else if (split[0].equalsIgnoreCase("nivel-5")) {
                            event.setDamage(event.getDamage() + 5);
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onDeathMob(final EntityDeathEvent event) {
        if (event.getEntity().getWorld().getName().equalsIgnoreCase("aether")) {
            if (event.getEntityType().equals(EntityType.GHAST)) {
                if (event.getEntity().getCustomName() != null) {
                    final String[] split = event.getEntity().getCustomName().split("::");
                    final String loc = split[1];
                    final String[] splitLoc = split[1].split(":");
                    final Location location = new Location(Bukkit.getWorld(splitLoc[0]), Integer.valueOf(splitLoc[1]), Integer.valueOf(splitLoc[2]), Integer.valueOf(splitLoc[3]));
                    DungeonAPI.updateActivated(split[1], false);
                    //TEMPO
                    final Date date = new Date();
                    date.setMinutes(date.getMinutes() + 10);
                    DungeonAPI.updateTimeUnlock(split[1], date.getTime() / 1000);
                    DungeonsHash.get(loc).setIsGetted(false);

                    final int rotate = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                        int y;

                        @Override
                        public void run() {
                            if (DungeonAPI.getTimeUnlock(loc) < (new Date().getTime() / 1000)) {
                                Bukkit.getScheduler().cancelTask(RotateKillGhast.get(loc).getId());
                                return;
                            }
                            if (DungeonAPI.getIsActivated(loc) == false && PlayerEntryDungeon.containsPlayer(location) == false) {
                                Bukkit.getScheduler().cancelTask(RotateKillGhast.get(loc).getId());
                                return;
                            }

                            final String[] text = {ChatColor.YELLOW + HorarioAPI.getTime(DungeonAPI.getTimeUnlock(loc) - (new Date().getTime() / 1000))};

                            if (this.y == 0) {
                                if (DungeonsHash.get(loc) != null) {
                                    this.y = DungeonsHash.get(loc).getStand().getLocation().getBlockY();
                                }
                            }

                            final ArmorStand armorStand = MobsEvents.getArmorStand(location);
                            DungeonsHash.get(split[1]).setStand(armorStand);
                            if (armorStand == null) return;
                            final Location location = armorStand.getLocation().clone();
                            location.setY(this.y);
                            DungeonsHash.get(split[1]).getStand().setCustomName(ChatColor.YELLOW + HorarioAPI.getTime(DungeonAPI.getTimeUnlock(loc) - (new Date().getTime() / 1000)));
                        }
                    }, 0L, 20L);

                    if (RotateKillGhast.get(loc) == null) {
                        new RotateKillGhast(loc, rotate).insert();
                    } else {
                        RotateKillGhast.get(loc).setId(rotate);
                    }

                    if (split[0].equalsIgnoreCase(ChatColor.YELLOW + "nível-1")) {
                        final ItemStack frag = new ItemStack(Material.PHANTOM_MEMBRANE);
                        final ItemMeta metaFrag = frag.getItemMeta();
                        metaFrag.setDisplayName(ChatColor.GREEN + "Fragmento de nível");
                        final ArrayList<String> lore = new ArrayList<>();
                        lore.add(" ");
                        lore.add(ChatColor.GRAY + "Fragmento de nível p/ asa, nível: 1");
                        lore.add(" ");
                        metaFrag.setLore(lore);
                        frag.setItemMeta(metaFrag);

                        event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), frag);
                        event.setDeathSound(Sound.ENTITY_ENDER_DRAGON_DEATH);
                        event.setDroppedExp(10);
                        event.getEntity().remove();
                    } else if (split[0].equalsIgnoreCase(ChatColor.YELLOW + "nível-2")) {
                        final ItemStack frag = new ItemStack(Material.PHANTOM_MEMBRANE);
                        final ItemMeta metaFrag = frag.getItemMeta();
                        metaFrag.setDisplayName(ChatColor.GREEN + "Fragmento de nível");
                        final ArrayList<String> lore = new ArrayList<>();
                        lore.add(" ");
                        lore.add(ChatColor.GRAY + "Fragmento de nível p/ asa, nível: 2");
                        lore.add(" ");
                        metaFrag.setLore(lore);
                        frag.setItemMeta(metaFrag);

                        event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), frag);
                        event.setDeathSound(Sound.ENTITY_ENDER_DRAGON_DEATH);
                        event.setDroppedExp(20);
                        event.getEntity().remove();
                    } else if (split[0].equalsIgnoreCase(ChatColor.YELLOW + "nível-3")) {
                        final ItemStack frag = new ItemStack(Material.PHANTOM_MEMBRANE);
                        final ItemMeta metaFrag = frag.getItemMeta();
                        metaFrag.setDisplayName(ChatColor.GREEN + "Fragmento de nível");
                        final ArrayList<String> lore = new ArrayList<>();
                        lore.add(" ");
                        lore.add(ChatColor.GRAY + "Fragmento de nível p/ asa, nível: 3");
                        lore.add(" ");
                        metaFrag.setLore(lore);
                        frag.setItemMeta(metaFrag);

                        event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), frag);
                        event.setDeathSound(Sound.ENTITY_ENDER_DRAGON_DEATH);
                        event.setDroppedExp(30);
                        event.getEntity().remove();
                    } else if (split[0].equalsIgnoreCase(ChatColor.YELLOW + "nível-4")) {
                        final ItemStack frag = new ItemStack(Material.PHANTOM_MEMBRANE);
                        final ItemMeta metaFrag = frag.getItemMeta();
                        metaFrag.setDisplayName(ChatColor.GREEN + "Fragmento de nível");
                        final ArrayList<String> lore = new ArrayList<>();
                        lore.add(" ");
                        lore.add(ChatColor.GRAY + "Fragmento de nível p/ asa, nível: 3");
                        lore.add(" ");
                        metaFrag.setLore(lore);
                        frag.setItemMeta(metaFrag);

                        final ItemStack moeda = new ItemStack(Material.CARROT_ON_A_STICK);
                        final ItemMeta meta = moeda.getItemMeta();
                        meta.setDisplayName(ChatColor.YELLOW + "Saco de moedas");
                        final ArrayList<String> lore1 = new ArrayList<>();
                        lore1.add(" ");
                        final Random random = new Random();
                        lore1.add(ChatColor.DARK_GRAY + "Moeda(s): " + ChatColor.GRAY + random.nextInt(30));
                        lore1.add(" ");
                        meta.setLore(lore1);
                        moeda.setItemMeta(meta);

                        event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), moeda);
                        event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), frag);
                        event.setDeathSound(Sound.ENTITY_ENDER_DRAGON_DEATH);
                        event.setDroppedExp(40);
                        event.getEntity().remove();
                    } else if (split[0].equalsIgnoreCase(ChatColor.YELLOW + "nível-5")) {
                        final ItemStack frag = new ItemStack(Material.PHANTOM_MEMBRANE);
                        final ItemMeta metaFrag = frag.getItemMeta();
                        metaFrag.setDisplayName(ChatColor.GREEN + "Fragmento de nível");
                        final ArrayList<String> lore = new ArrayList<>();
                        lore.add(" ");
                        lore.add(ChatColor.GRAY + "Fragmento de nível p/ asa, nível: 3");
                        lore.add(" ");
                        metaFrag.setLore(lore);
                        frag.setItemMeta(metaFrag);

                        final ItemStack moeda = new ItemStack(Material.CARROT_ON_A_STICK);
                        final ItemMeta meta = moeda.getItemMeta();
                        meta.setDisplayName(ChatColor.YELLOW + "Saco de moedas");
                        final ArrayList<String> lore1 = new ArrayList<>();
                        lore1.add(" ");
                        final Random random = new Random();
                        lore1.add(ChatColor.DARK_GRAY + "Moeda(s): " + ChatColor.GRAY + random.nextInt(60));
                        lore1.add(" ");
                        meta.setLore(lore1);
                        moeda.setItemMeta(meta);

                        event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), moeda);
                        event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), frag);
                        event.setDeathSound(Sound.ENTITY_ENDER_DRAGON_DEATH);
                        event.setDroppedExp(50);
                        event.getEntity().remove();
                    }
                }
            }

            if (event.getEntityType().equals(EntityType.VEX)) {
                if (event.getEntity().getCustomName() != null) {
                    final String[] split = event.getEntity().getCustomName().split(":");

                    final ItemStack frag = new ItemStack(Material.FEATHER);
                    final ItemMeta metaFrag = frag.getItemMeta();
                    metaFrag.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
                    final ArrayList<String> lore = new ArrayList<>();
                    lore.add(" ");
                    lore.add(ChatColor.GRAY + "Fragmento para asa, tipo: 1");
                    lore.add(" ");
                    metaFrag.setLore(lore);
                    frag.setItemMeta(metaFrag);

                    if (split[0].equalsIgnoreCase(ChatColor.YELLOW + "nível-1")) {
                        if (ProbabilityApi.probab(3000) == true) {
                            final ItemStack moeda = new ItemStack(Material.CARROT_ON_A_STICK);
                            final ItemMeta meta = moeda.getItemMeta();
                            meta.setDisplayName(ChatColor.YELLOW + "Saco de moedas");
                            final ArrayList<String> lore1 = new ArrayList<>();
                            lore1.add(" ");
                            final Random random = new Random();
                            lore1.add(ChatColor.DARK_GRAY + "Moeda(s): " + ChatColor.GRAY + random.nextInt(1));
                            lore1.add(" ");
                            meta.setLore(lore1);
                            moeda.setItemMeta(meta);
                            event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), moeda);
                        }
                        event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), frag);
                        final int xp = event.getDroppedExp() / 5;
                        event.setDroppedExp(event.getDroppedExp() + (xp * 1));
                        event.setDeathSound(Sound.ENTITY_VEX_DEATH);
                        event.setDeathSound(Sound.ENTITY_VEX_CHARGE);
                    } else if (split[0].equalsIgnoreCase(ChatColor.YELLOW + "nível-2")) {
                        if (ProbabilityApi.probab(3000) == true) {
                            final ItemStack moeda = new ItemStack(Material.CARROT_ON_A_STICK);
                            final ItemMeta meta = moeda.getItemMeta();
                            meta.setDisplayName(ChatColor.YELLOW + "Saco de moedas");
                            final ArrayList<String> lore1 = new ArrayList<>();
                            lore1.add(" ");
                            final Random random = new Random();
                            lore1.add(ChatColor.DARK_GRAY + "Moeda(s): " + ChatColor.GRAY + random.nextInt(2));
                            lore1.add(" ");
                            meta.setLore(lore1);
                            moeda.setItemMeta(meta);
                            event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), moeda);
                        }
                        event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), frag);
                        final int xp = event.getDroppedExp() / 5;
                        event.setDroppedExp(event.getDroppedExp() + (xp * 2));
                        event.setDeathSound(Sound.ENTITY_VEX_DEATH);
                        event.setDeathSound(Sound.ENTITY_VEX_CHARGE);
                    } else if (split[0].equalsIgnoreCase(ChatColor.YELLOW + "nível-3")) {
                        if (ProbabilityApi.probab(4000) == true) {
                            final ItemStack moeda = new ItemStack(Material.CARROT_ON_A_STICK);
                            final ItemMeta meta = moeda.getItemMeta();
                            meta.setDisplayName(ChatColor.YELLOW + "Saco de moedas");
                            final ArrayList<String> lore1 = new ArrayList<>();
                            lore1.add(" ");
                            final Random random = new Random();
                            lore1.add(ChatColor.DARK_GRAY + "Moeda(s): " + ChatColor.GRAY + random.nextInt(2));
                            lore1.add(" ");
                            meta.setLore(lore1);
                            moeda.setItemMeta(meta);
                            event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), moeda);
                        }
                        event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), frag);
                        final int xp = event.getDroppedExp() / 5;
                        event.setDroppedExp(event.getDroppedExp() + (xp * 3));
                        event.setDeathSound(Sound.ENTITY_VEX_DEATH);
                        event.setDeathSound(Sound.ENTITY_VEX_CHARGE);
                    } else if (split[0].equalsIgnoreCase(ChatColor.YELLOW + "nível-4")) {
                        if (ProbabilityApi.probab(4000) == true) {
                            final ItemStack moeda = new ItemStack(Material.CARROT_ON_A_STICK);
                            final ItemMeta meta = moeda.getItemMeta();
                            meta.setDisplayName(ChatColor.YELLOW + "Saco de moedas");
                            final ArrayList<String> lore1 = new ArrayList<>();
                            lore1.add(" ");
                            final Random random = new Random();
                            lore1.add(ChatColor.DARK_GRAY + "Moeda(s): " + ChatColor.GRAY + random.nextInt(3));
                            lore1.add(" ");
                            meta.setLore(lore1);
                            moeda.setItemMeta(meta);
                            event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), moeda);
                        }
                        event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), frag);
                        final int xp = event.getDroppedExp() / 5;
                        event.setDroppedExp(event.getDroppedExp() + (xp * 4));
                        event.setDeathSound(Sound.ENTITY_VEX_DEATH);
                        event.setDeathSound(Sound.ENTITY_VEX_CHARGE);
                    } else if (split[0].equalsIgnoreCase("nível-5")) {
                        if (ProbabilityApi.probab(5000) == true) {
                            final ItemStack moeda = new ItemStack(Material.CARROT_ON_A_STICK);
                            final ItemMeta meta = moeda.getItemMeta();
                            meta.setDisplayName(ChatColor.YELLOW + "Saco de moedas");
                            final ArrayList<String> lore1 = new ArrayList<>();
                            lore1.add(" ");
                            final Random random = new Random();
                            lore1.add(ChatColor.DARK_GRAY + "Moeda(s): " + ChatColor.GRAY + random.nextInt(3));
                            lore1.add(" ");
                            meta.setLore(lore1);
                            moeda.setItemMeta(meta);
                            event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), moeda);
                        }
                        event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), frag);
                        final int xp = event.getDroppedExp() / 5;
                        event.setDroppedExp(event.getDroppedExp() + (xp * 5));
                        event.setDeathSound(Sound.ENTITY_VEX_DEATH);
                        event.setDeathSound(Sound.ENTITY_VEX_CHARGE);
                    }
                }
            }

            if (event.getEntityType().equals(EntityType.PHANTOM)) {
                event.setCancelled(true);

                final ItemStack frag = new ItemStack(Material.FEATHER);
                final ItemMeta metaFrag = frag.getItemMeta();
                metaFrag.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
                final ArrayList<String> lore = new ArrayList<>();
                lore.add(" ");
                lore.add(ChatColor.GRAY + "Fragmento para asa, tipo: 2");
                lore.add(" ");
                metaFrag.setLore(lore);
                frag.setItemMeta(metaFrag);

                final int probability = 50000;
                if (ProbabilityApi.probab(probability) == true) {
                    event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), frag);
                }

                event.getEntity().remove();
            }

            if (event.getEntityType().equals(EntityType.PARROT)) {
                event.setCancelled(true);

                final ItemStack frag = new ItemStack(Material.FEATHER, 3);
                final ItemMeta metaFrag = frag.getItemMeta();
                metaFrag.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
                final ArrayList<String> lore = new ArrayList<>();
                lore.add(" ");
                lore.add(ChatColor.GRAY + "Fragmento para asa, tipo: 1");
                lore.add(" ");
                metaFrag.setLore(lore);
                frag.setItemMeta(metaFrag);

                final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
                final ItemMeta meta2 = moeda.getItemMeta();
                meta2.setDisplayName(ChatColor.YELLOW + "$2.000,00 moedas");
                moeda.setItemMeta(meta2);

                event.getEntity().getWorld().playSound(event.getEntity().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0F, 1.0F);
                event.getEntity().getWorld().playSound(event.getEntity().getLocation(), Sound.ENTITY_PARROT_IMITATE_ZOMBIE, 1.0F, 1.0F);
                event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), frag);
                event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), moeda);
                event.getEntity().remove();
            }
        }
    }

    @EventHandler
    public void onSpawn(final EntitySpawnEvent event) {
        if (event.getLocation().getWorld().getName().equalsIgnoreCase("aether")) {
            if (event.getEntityType().equals(EntityType.VEX) || event.getEntityType().equals(EntityType.GHAST) || event.getEntityType().equals(EntityType.ARMOR_STAND)) {
                event.setCancelled(false);
                return;
            }

            if (event.getEntityType().equals(EntityType.PIG) || event.getEntityType().equals(EntityType.CHICKEN) || event.getEntityType().equals(EntityType.COW) || event.getEntityType().equals(EntityType.SHEEP) || event.getEntityType().equals(EntityType.ZOMBIE) || event.getEntityType().equals(EntityType.SKELETON)
                    || event.getEntityType().equals(EntityType.SPIDER) || event.getEntityType().equals(EntityType.CREEPER) || event.getEntityType().equals(EntityType.ENDERMAN) || event.getEntityType().equals(EntityType.RABBIT) || event.getEntityType().equals(EntityType.WOLF)) {
                final int probability = 75000;
                if (ProbabilityApi.probab(probability) == true) {
                    event.getLocation().getWorld().spawnEntity(event.getLocation(), EntityType.PHANTOM);
                    event.setCancelled(true);
                }

                final int probability2 = 1000;
                if (ProbabilityApi.probab(probability2) == true) {
                    event.getLocation().getWorld().spawnEntity(event.getLocation(), EntityType.PARROT);
                    event.setCancelled(true);
                }
            }
        }
    }
}
