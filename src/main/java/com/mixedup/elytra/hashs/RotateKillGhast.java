package com.mixedup.elytra.hashs;

import java.util.HashMap;

public class RotateKillGhast {

    private static final HashMap<String, RotateKillGhast> CACHE = new HashMap<String, RotateKillGhast>();
    private final String UUID;
    private int ID;

    public RotateKillGhast(final String UUID, final int ID) {
        this.UUID = UUID;
        this.ID = ID;
    }

    public static RotateKillGhast get(final String UUID) {
        return RotateKillGhast.CACHE.get(String.valueOf(UUID));
    }

    public RotateKillGhast insert() {
        RotateKillGhast.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public int getId() {
        return ID;
    }

    public void setId(final int ID) {
        this.ID = ID;
    }

    public String getUUID() {
        return UUID;
    }
}
