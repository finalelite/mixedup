package com.mixedup.elytra.hashs;

import java.util.HashMap;

public class IsTeleportedHash {

    private static final HashMap<String, IsTeleportedHash> CACHE = new HashMap<String, IsTeleportedHash>();
    private final String UUID;
    private boolean Status;

    public IsTeleportedHash(final String UUID, final boolean Status) {
        this.UUID = UUID;
        this.Status = Status;
    }

    public static IsTeleportedHash get(final String UUID) {
        return IsTeleportedHash.CACHE.get(String.valueOf(UUID));
    }

    public IsTeleportedHash insert() {
        IsTeleportedHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(final boolean Status) {
        this.Status = Status;
    }

    public String getUUID() {
        return UUID;
    }
}
