package com.mixedup.elytra.hashs;

import java.util.HashMap;

public class PreventionRightClickHash {

    private static final HashMap<String, PreventionRightClickHash> CACHE = new HashMap<String, PreventionRightClickHash>();
    private final String UUID;
    private boolean Status;

    public PreventionRightClickHash(final String UUID, final boolean Status) {
        this.UUID = UUID;
        this.Status = Status;
    }

    public static PreventionRightClickHash get(final String UUID) {
        return PreventionRightClickHash.CACHE.get(String.valueOf(UUID));
    }

    public PreventionRightClickHash insert() {
        PreventionRightClickHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(final boolean Status) {
        this.Status = Status;
    }

    public String getUUID() {
        return UUID;
    }
}
