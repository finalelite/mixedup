package com.mixedup.elytra.hashs;

import java.util.HashMap;

public class SelectPortalFromDeleteHash {

    private static final HashMap<String, SelectPortalFromDeleteHash> CACHE = new HashMap<String, SelectPortalFromDeleteHash>();
    private final String UUID;
    private boolean Status;

    public SelectPortalFromDeleteHash(final String UUID, final boolean Status) {
        this.UUID = UUID;
        this.Status = Status;
    }

    public static SelectPortalFromDeleteHash get(final String UUID) {
        return SelectPortalFromDeleteHash.CACHE.get(String.valueOf(UUID));
    }

    public SelectPortalFromDeleteHash insert() {
        SelectPortalFromDeleteHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(final boolean Status) {
        this.Status = Status;
    }

    public String getUUID() {
        return UUID;
    }
}
