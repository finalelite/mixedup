package com.mixedup.elytra.hashs;

import java.util.HashMap;

public class IsSelectedHash {

    private static final HashMap<String, IsSelectedHash> CACHE = new HashMap<String, IsSelectedHash>();
    private final String UUID;
    private boolean Status;

    public IsSelectedHash(final String UUID, final boolean Status) {
        this.UUID = UUID;
        this.Status = Status;
    }

    public static IsSelectedHash get(final String UUID) {
        return IsSelectedHash.CACHE.get(String.valueOf(UUID));
    }

    public IsSelectedHash insert() {
        IsSelectedHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(final boolean Status) {
        this.Status = Status;
    }

    public String getUUID() {
        return UUID;
    }
}
