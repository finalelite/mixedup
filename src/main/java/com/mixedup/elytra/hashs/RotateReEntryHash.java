package com.mixedup.elytra.hashs;

import java.util.HashMap;

public class RotateReEntryHash {

    private static final HashMap<String, RotateReEntryHash> CACHE = new HashMap<String, RotateReEntryHash>();
    private final String UUID;
    private int ID;

    public RotateReEntryHash(final String UUID, final int ID) {
        this.UUID = UUID;
        this.ID = ID;
    }

    public static RotateReEntryHash get(final String UUID) {
        return RotateReEntryHash.CACHE.get(String.valueOf(UUID));
    }

    public RotateReEntryHash insert() {
        RotateReEntryHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public int getId() {
        return ID;
    }

    public void setId(final int ID) {
        this.ID = ID;
    }

    public String getUUID() {
        return UUID;
    }
}
