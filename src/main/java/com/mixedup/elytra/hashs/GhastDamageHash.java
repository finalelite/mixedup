package com.mixedup.elytra.hashs;

import java.util.HashMap;

public class GhastDamageHash {

    private static final HashMap<String, GhastDamageHash> CACHE = new HashMap<String, GhastDamageHash>();
    private final String UUID;
    private int ID;

    public GhastDamageHash(final String UUID, final int ID) {
        this.UUID = UUID;
        this.ID = ID;
    }

    public static GhastDamageHash get(final String UUID) {
        return GhastDamageHash.CACHE.get(UUID);
    }

    public GhastDamageHash insert() {
        GhastDamageHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public int getId() {
        return ID;
    }

    public void setId(final int ID) {
        this.ID = ID;
    }

    public String getUUID() {
        return UUID;
    }
}
