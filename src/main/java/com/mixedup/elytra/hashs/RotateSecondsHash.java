package com.mixedup.elytra.hashs;

import java.util.HashMap;

public class RotateSecondsHash {

    private static final HashMap<String, RotateSecondsHash> CACHE = new HashMap<String, RotateSecondsHash>();
    private final String UUID;
    private int ID;

    public RotateSecondsHash(final String UUID, final int ID) {
        this.UUID = UUID;
        this.ID = ID;
    }

    public static RotateSecondsHash get(final String UUID) {
        return RotateSecondsHash.CACHE.get(String.valueOf(UUID));
    }

    public RotateSecondsHash insert() {
        RotateSecondsHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public int getId() {
        return ID;
    }

    public void setId(final int ID) {
        this.ID = ID;
    }

    public String getUUID() {
        return UUID;
    }
}
