package com.mixedup.elytra.hashs;

import java.util.HashMap;

public class LocationSelectedHash {

    private static final HashMap<String, LocationSelectedHash> CACHE = new HashMap<String, LocationSelectedHash>();
    private final String UUID;
    private String pos1;
    private String pos2;

    public LocationSelectedHash(final String UUID, final String pos1, final String pos2) {
        this.UUID = UUID;
        this.pos1 = pos1;
        this.pos2 = pos2;
    }

    public static LocationSelectedHash get(final String UUID) {
        return LocationSelectedHash.CACHE.get(String.valueOf(UUID));
    }

    public LocationSelectedHash insert() {
        LocationSelectedHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public String getPos1() {
        return pos1;
    }

    public void setPos1(final String pos1) {
        this.pos1 = pos1;
    }

    public String getPos2() {
        return pos2;
    }

    public void setPos2(final String pos2) {
        this.pos2 = pos2;
    }

    public String getUUID() {
        return UUID;
    }
}
