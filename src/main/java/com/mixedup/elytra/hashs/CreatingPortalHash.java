package com.mixedup.elytra.hashs;

import java.util.HashMap;

public class CreatingPortalHash {

    private static final HashMap<String, CreatingPortalHash> CACHE = new HashMap<String, CreatingPortalHash>();
    private final String UUID;
    private boolean Status;

    public CreatingPortalHash(final String UUID, final boolean Status) {
        this.UUID = UUID;
        this.Status = Status;
    }

    public static CreatingPortalHash get(final String UUID) {
        return CreatingPortalHash.CACHE.get(String.valueOf(UUID));
    }

    public CreatingPortalHash insert() {
        CreatingPortalHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(final boolean Status) {
        this.Status = Status;
    }

    public String getUUID() {
        return UUID;
    }
}
