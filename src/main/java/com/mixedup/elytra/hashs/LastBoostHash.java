package com.mixedup.elytra.hashs;

import java.util.HashMap;

public class LastBoostHash {

    private static final HashMap<String, LastBoostHash> CACHE = new HashMap<String, LastBoostHash>();
    private final String UUID;
    private int ID;

    public LastBoostHash(final String UUID, final int ID) {
        this.UUID = UUID;
        this.ID = ID;
    }

    public static LastBoostHash get(final String UUID) {
        return LastBoostHash.CACHE.get(String.valueOf(UUID));
    }

    public LastBoostHash insert() {
        LastBoostHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public int getId() {
        return ID;
    }

    public void setId(final int ID) {
        this.ID = ID;
    }

    public String getUUID() {
        return UUID;
    }

}
