package com.mixedup.elytra.hashs;

import java.util.HashMap;

public class RotateHash {

    private static final HashMap<String, RotateHash> CACHE = new HashMap<String, RotateHash>();
    private final String UUID;
    private int ID;

    public RotateHash(final String UUID, final int ID) {
        this.UUID = UUID;
        this.ID = ID;
    }

    public static RotateHash get(final String UUID) {
        return RotateHash.CACHE.get(String.valueOf(UUID));
    }

    public RotateHash insert() {
        RotateHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public int getId() {
        return ID;
    }

    public void setId(final int ID) {
        this.ID = ID;
    }

    public String getUUID() {
        return UUID;
    }
}
