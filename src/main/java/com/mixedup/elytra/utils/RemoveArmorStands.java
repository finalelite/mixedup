package com.mixedup.elytra.utils;

import com.mixedup.elytra.DungeonAPI;
import com.mixedup.elytra.regions.DungeonsHash;

public class RemoveArmorStands {

    public static void removeAllArmorStand() {
        for (final DungeonsHash dungeons : DungeonsHash.CACHE.values()) {
            if (DungeonAPI.getIsActivated(dungeons.getLocal()) == true) {
                DungeonAPI.updateActivated(dungeons.getLocal(), false);
            }
            if (dungeons.getEntity() != null) {
                dungeons.getEntity().remove();
            }
            if (dungeons.getStand() != null) {
                dungeons.getStand().remove();
            }
            if (dungeons.getStandCount() != null) {
                dungeons.getStandCount().remove();
            }
        }
    }
}
