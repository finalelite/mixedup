package com.mixedup.elytra.regions;

import com.mixedup.Main;
import com.mixedup.MySql;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DungeonsUtils {

    private static List<DungeonsHash> regions = new ArrayList<>();

    public static DungeonsHash getRegion(final String local) {
        for (final DungeonsHash region : DungeonsUtils.getRegions()) {
            if (region.getLocal() == local) {
                return region;
            }
        }
        return null;
    }

    public static List<DungeonsHash> getRegions() {
        return DungeonsUtils.regions;
    }

    public static void setRegions(final List<DungeonsHash> regions) {
        DungeonsUtils.regions = regions;
    }

    public static DungeonsHash playerInArea(final Location loc) {
        final List<DungeonsHash> regionList = getRegions();
        for (final DungeonsHash rg : regionList) {

            if (!rg.getPos1().equalsIgnoreCase("NULL") && !rg.getPos2().equalsIgnoreCase("NULL")) {
                final String[] pos1 = rg.getPos1().split(":");
                final String[] pos2 = rg.getPos2().split(":");

                if (loc.getWorld().getName().equalsIgnoreCase(pos1[0])) {
                    final int p1x = Integer.valueOf(pos1[1]);
                    final int p1y = Integer.valueOf(pos1[2]);
                    final int p1z = Integer.valueOf(pos1[3]);
                    final int p2x = Integer.valueOf(pos2[1]);
                    final int p2y = Integer.valueOf(pos2[2]);
                    final int p2z = Integer.valueOf(pos2[3]);

                    final int minX = p1x < p2x ? p1x : p2x;
                    final int minY = p1y < p2y ? p1y : p2y;
                    final int minZ = p1z < p2z ? p1z : p2z;

                    final int maxX = p1x > p2x ? p1x : p2x;
                    final int maxY = p1y > p2y ? p1y : p2y;
                    final int maxZ = p1z > p2z ? p1z : p2z;

                    if ((loc.getBlockX() >= minX) && (loc.getBlockX() <= maxX) && (loc.getBlockY() >= minY)
                            && (loc.getBlockY() <= maxY) && (loc.getBlockZ() >= minZ) && (loc.getBlockZ() <= maxZ)) {

                        return rg;

                    }
                }
            }
        }
        return null;
    }

    public static DungeonsHash playerInArea50(final Location loc) {
        final List<DungeonsHash> regionList = getRegions();
        for (final DungeonsHash rg : regionList) {

            if (!rg.getPos1().equalsIgnoreCase("NULL") && !rg.getPos2().equalsIgnoreCase("NULL")) {
                final String[] pos1 = rg.getPos1().split(":");
                final String[] pos2 = rg.getPos2().split(":");

                if (loc.getWorld().getName().equalsIgnoreCase(pos1[0])) {
                    final int p1x = Integer.valueOf(pos1[1]) + 30;
                    final int p1y = Integer.valueOf(pos1[2]);
                    final int p1z = Integer.valueOf(pos1[3]) + 30;
                    final int p2x = Integer.valueOf(pos2[1]) - 30;
                    final int p2y = Integer.valueOf(pos2[2]);
                    final int p2z = Integer.valueOf(pos2[3]) - 30;

                    final int minX = p1x < p2x ? p1x : p2x;
                    final int minY = p1y < p2y ? p1y : p2y;
                    final int minZ = p1z < p2z ? p1z : p2z;

                    final int maxX = p1x > p2x ? p1x : p2x;
                    final int maxY = p1y > p2y ? p1y : p2y;
                    final int maxZ = p1z > p2z ? p1z : p2z;

                    if ((loc.getBlockX() >= minX) && (loc.getBlockX() <= maxX) && (loc.getBlockY() >= minY)
                            && (loc.getBlockY() <= maxY) && (loc.getBlockZ() >= minZ) && (loc.getBlockZ() <= maxZ)) {

                        return rg;

                    }
                }
            }
        }
        return null;
    }


    public static void enableRegions() {
        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Dungeons_data");
                    final ResultSet rs = st.executeQuery();
                    while (rs.next()) {
                        final String[] split = rs.getString("Location").split(":");
                        final String loc1 = split[0] + ":" + (Integer.valueOf(split[1]) + 20) + ":0:" + (Integer.valueOf(split[3]) + 20);
                        final String loc2 = split[0] + ":" + (Integer.valueOf(split[1]) - 20) + ":256:" + (Integer.valueOf(split[3]) - 20);

                        final DungeonsHash dungeon = new DungeonsHash(rs.getString("Location"), loc1, loc2, null, null, rs.getInt("Level"), null, true).insert();
                        getRegions().add(dungeon);
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }, 200L);
    }
}
