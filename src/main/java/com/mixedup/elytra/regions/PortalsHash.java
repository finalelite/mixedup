package com.mixedup.elytra.regions;

import java.util.HashMap;

public class PortalsHash {

    private static final HashMap<String, PortalsHash> CACHE = new HashMap<String, PortalsHash>();
    private String local;
    private String pos1;
    private String pos2;

    public PortalsHash(final String local, final String pos1, final String pos2) {
        this.local = local;
        this.pos1 = pos1;
        this.pos2 = pos2;
    }

    public static PortalsHash get(final String local) {
        return PortalsHash.CACHE.get(local);
    }

    public PortalsHash insert() {
        PortalsHash.CACHE.put(local, this);

        return this;
    }

    public String getLocal() {
        return this.local;
    }

    public void setLocal(final String local) {
        this.local = local;
    }

    public String getPos1() {
        return this.pos1;
    }

    public void setPos1(final String pos1) {
        this.pos1 = pos1;
    }

    public String getPos2() {
        return this.pos2;
    }

    public void setPos2(final String pos2) {
        this.pos2 = pos2;
    }
}
