package com.mixedup.elytra.regions;

import com.mixedup.MySql;
import org.bukkit.Location;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PortalsUtils {

    private static List<PortalsHash> regions = new ArrayList<>();

    public static PortalsHash getRegion(final String local) {
        for (final PortalsHash region : PortalsUtils.getRegions()) {
            if (region.getLocal() == local) {
                return region;
            }
        }
        return null;
    }

    public static List<PortalsHash> getRegions() {
        return PortalsUtils.regions;
    }

    public static void setRegions(final List<PortalsHash> regions) {
        PortalsUtils.regions = regions;
    }

    public static PortalsHash playerInArea(final Location loc) {
        final List<PortalsHash> regionList = getRegions();
        for (final PortalsHash rg : regionList) {

            if (!rg.getPos1().equalsIgnoreCase("NULL") && !rg.getPos2().equalsIgnoreCase("NULL")) {
                final String[] pos1 = rg.getPos1().split(":");
                final String[] pos2 = rg.getPos2().split(":");

                if (loc.getWorld().getName().equalsIgnoreCase(pos1[0])) {
                    final int p1x = Integer.valueOf(pos1[1]);
                    final int p1y = Integer.valueOf(pos1[2]);
                    final int p1z = Integer.valueOf(pos1[3]);
                    final int p2x = Integer.valueOf(pos2[1]);
                    final int p2y = Integer.valueOf(pos2[2]);
                    final int p2z = Integer.valueOf(pos2[3]);

                    final int minX = p1x < p2x ? p1x : p2x;
                    final int minY = p1y < p2y ? p1y : p2y;
                    final int minZ = p1z < p2z ? p1z : p2z;

                    final int maxX = p1x > p2x ? p1x : p2x;
                    final int maxY = p1y > p2y ? p1y : p2y;
                    final int maxZ = p1z > p2z ? p1z : p2z;

                    if ((loc.getBlockX() >= minX) && (loc.getBlockX() <= maxX) && (loc.getBlockY() >= minY)
                            && (loc.getBlockY() <= maxY) && (loc.getBlockZ() >= minZ) && (loc.getBlockZ() <= maxZ)) {

                        return rg;

                    }
                }
            }
        }
        return null;
    }


    public static void enableRegions() {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Portals_data");
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                final String[] split = rs.getString("PortalLoc").split("::");
                final PortalsHash portal = new PortalsHash(rs.getString("SaidaLoc"), split[0], split[1]).insert();
                getRegions().add(portal);
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }
}
