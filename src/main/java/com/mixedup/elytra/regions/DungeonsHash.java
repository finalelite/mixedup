package com.mixedup.elytra.regions;

import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;

import java.util.HashMap;

public class DungeonsHash {

    public static HashMap<String, DungeonsHash> CACHE = new HashMap<String, DungeonsHash>();
    private String local;
    private String pos1;
    private String pos2;
    private ArmorStand stand;
    private ArmorStand standCount;
    private int level;
    private Entity entity;
    private boolean getted;

    public DungeonsHash(final String local, final String pos1, final String pos2, final ArmorStand stand, final ArmorStand standCount, final int level, final Entity entity, final boolean getted) {
        this.local = local;
        this.pos1 = pos1;
        this.pos2 = pos2;
        this.stand = stand;
        this.standCount = standCount;
        this.level = level;
        this.entity = entity;
        this.getted = getted;
    }

    public static DungeonsHash get(final String local) {
        return DungeonsHash.CACHE.get(local);
    }

    public DungeonsHash insert() {
        DungeonsHash.CACHE.put(local, this);

        return this;
    }

    public boolean isGetted() {
        return this.getted;
    }

    public void setIsGetted(final boolean getted) {
        this.getted = getted;
    }

    public ArmorStand getStandCount() {
        return this.standCount;
    }

    public void setStandCount(final ArmorStand standCount) {
        this.standCount = standCount;
    }

    public ArmorStand getStand() {
        return this.stand;
    }

    public void setStand(final ArmorStand stand) {
        this.stand = stand;
    }

    public Entity getEntity() {
        return this.entity;
    }

    public void setEntity(final Entity entity) {
        this.entity = entity;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(final int level) {
        this.level = level;
    }

    public String getLocal() {
        return this.local;
    }

    public void setLocal(final String local) {
        this.local = local;
    }

    public String getPos1() {
        return this.pos1;
    }

    public void setPos1(final String pos1) {
        this.pos1 = pos1;
    }

    public String getPos2() {
        return this.pos2;
    }

    public void setPos2(final String pos2) {
        this.pos2 = pos2;
    }
}
