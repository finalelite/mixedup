package com.mixedup.elytra;

import com.mixedup.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DungeonAPI {

    public static void createDungeon(final String location, final int nivel) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Dungeons_data(Location, Level, Activated, TimeUnlock) VALUES (?, ?, ?, ?)");
            st.setString(1, location);
            st.setInt(2, nivel);
            st.setBoolean(3, false);
            st.setLong(4, 0);
            st.executeUpdate();

            new DungeonCache(location, nivel, false, 0).insert();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateTimeUnlock(final String location, final long timeUnlock) {
        DungeonCache.get(location).setTimeLock(timeUnlock);
        //try {
        //            PreparedStatement st = MySql.con.prepareStatement("UPDATE Dungeons_data SET TimeUnlock = ? WHERE Location = ?");
        //            st.setString(2, location);
        //            st.setLong(1, timeUnlock);
        //            st.executeUpdate();
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
    }

    public static void updateActivated(final String location, final boolean activated) {
        DungeonCache.get(location).setActivated(activated);
        //try {
        //            PreparedStatement st = MySql.con.prepareStatement("UPDATE Dungeons_data SET Activated = ? WHERE Location = ?");
        //            st.setString(2, location);
        //            st.setBoolean(1, activated);
        //            st.executeUpdate();
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
    }

    public static boolean getIsActivated(final String location) {
        if (DungeonCache.get(location) != null) {
            return DungeonCache.get(location).getActivated();
        } else {
            return false;
        }
        //try {
        //            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Dungeons_data WHERE Location = ?");
        //            st.setString(1, location);
        //            ResultSet rs = st.executeQuery();
        //            while (rs.next()) {
        //                return rs.getBoolean("Activated");
        //            }
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
        //        return false;
    }

    public static Long getTimeUnlock(final String location) {
        if (DungeonCache.get(location) != null) {
            return DungeonCache.get(location).getTimeLock();
        } else {
            return (long) 0;
        }
        //  try {
        //            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Dungeons_data WHERE Location = ?");
        //            st.setString(1, location);
        //            ResultSet rs = st.executeQuery();
        //            while (rs.next()) {
        //                return rs.getLong("TimeUnlock");
        //            }
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
        //        return (long)0;
    }

    public static int getDungeonLevel(final String location) {
        if (DungeonCache.get(location) != null) {
            return DungeonCache.get(location).getLevel();
        } else {
            return 0;
        }
        // try {
        //            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Dungeons_data WHERE Location = ?");
        //            st.setString(1, location);
        //            ResultSet rs = st.executeQuery();
        //            while (rs.next()) {
        //                return rs.getInt("Level");
        //            }
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
        //        return 0;
    }

    public static void removeDungeon(final String location) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Dungeons_data WHERE Location = ?");
            st.setString(1, location);
            st.executeUpdate();

            DungeonCache.CACHE.remove(location);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void loadDungeons() {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Dungeons_data");
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                final String location = rs.getString("Location");
                final PreparedStatement st2 = MySql.con.prepareStatement("SELECT * FROM Dungeons_data WHERE Location = ?");
                st2.setString(1, location);
                final ResultSet rs2 = st2.executeQuery();
                while (rs2.next()) {
                    final int level = rs2.getInt("Level");

                    new DungeonCache(location, level, false, 0).insert();
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }
}
