package com.mixedup.elytra;

import com.mixedup.MySql;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PortalAPI {

    public static void createPortal(final String portal, final String saida) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Portals_data(PortalLoc, SaidaLoc) VALUES (?, ?)");
            st.setString(1, portal);
            st.setString(2, saida);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removePortal(final String saida) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Portals_data WHERE SaidaLoc = ?");
            st.setString(1, saida);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }
}
