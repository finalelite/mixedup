package com.mixedup.eventos.DetritoEvent;

import com.mixedup.Main;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import com.mixedup.eventos.DetritoEvent.hashs.DetritoHash;
import com.mixedup.factions.FacAPI;
import com.mixedup.worldProtection.hashs.ProtectionUtil;
import org.bukkit.*;
import org.bukkit.block.Chest;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CommandDetrito implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final org.bukkit.command.Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("detrito")) {
            if (TagAPI.getTag(UUID).equalsIgnoreCase("Master") || TagAPI.getTag(UUID).equalsIgnoreCase("Supervisor")) {
                if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("spawnar")) {
                        final Random random = new Random();
                        int x = random.nextInt(39000);
                        int z = random.nextInt(39000);
                        if (x > 19500) {
                            x = +(x - 19500);
                        }
                        if (z > 19500) {
                            z = -(z - 19500);
                        }
                        final Location r = new Location(Bukkit.getWorld("Trappist-1b"), x, 70, z);
                        final String chunk = r.getChunk().getX() + ":" + r.getChunk().getZ();

                        while (FacAPI.getChunkOwn(chunk) != null || ProtectionUtil.playerInArea(r) != null) {
                            x = random.nextInt(39000);
                            z = random.nextInt(39000);
                        }

                        r.setY(r.getWorld().getHighestBlockYAt(r));

                        final Location first = r;
                        final Location location = r.clone().add(0, 30, 0);
                        player.sendMessage(ChatColor.GREEN + " \n * Detrito spawnado\n \n  Mundo: " + ChatColor.GRAY + "Trappist-1b\n  " + ChatColor.GREEN + "Posição: " + ChatColor.GRAY + first.getBlockX() + "x " + first.getBlockY() + "y " + first.getBlockZ() + "z \n ");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                        final Location loc = first.clone().add(0, -1, 0);
                        new DetritoHash(loc.getBlockX() + ":" + loc.getBlockY() + ":" + loc.getBlockZ(), false, 0).insert();

                        final ItemStack obsidian = new ItemStack(Material.OBSIDIAN);
                        final ItemStack chest = new ItemStack(Material.CHEST);
                        final ItemStack magma = new ItemStack(Material.MAGMA_BLOCK);
                        location.getWorld().spawnFallingBlock(location.clone(), obsidian.getType(), obsidian.getData().getData());
                        final FallingBlock f1 = location.getWorld().spawnFallingBlock(location.clone().add(-1, 0, 0), obsidian.getType(), obsidian.getData().getData());
                        final FallingBlock f2 = location.getWorld().spawnFallingBlock(location.clone().add(0, 0, -1), obsidian.getType(), obsidian.getData().getData());
                        final FallingBlock f3 = location.getWorld().spawnFallingBlock(location.clone().add(-1, 0, -1), obsidian.getType(), obsidian.getData().getData());
                        final FallingBlock f4 = location.getWorld().spawnFallingBlock(location.clone().add(1, 0, 1), obsidian.getType(), obsidian.getData().getData());
                        final FallingBlock f5 = location.getWorld().spawnFallingBlock(location.clone().add(-1, 0, 1), obsidian.getType(), obsidian.getData().getData());
                        final FallingBlock f6 = location.getWorld().spawnFallingBlock(location.clone().add(1, 0, -1), obsidian.getType(), obsidian.getData().getData());
                        final FallingBlock f7 = location.getWorld().spawnFallingBlock(location.clone().add(1, 0, 0), obsidian.getType(), obsidian.getData().getData());
                        final FallingBlock f8 = location.getWorld().spawnFallingBlock(location.clone().add(0, 0, 1), obsidian.getType(), obsidian.getData().getData());
                        //-
                        final FallingBlock f9 = location.getWorld().spawnFallingBlock(location.clone().add(-1, 1, 0), obsidian.getType(), obsidian.getData().getData());
                        final FallingBlock f10 = location.getWorld().spawnFallingBlock(location.clone().add(0, 1, -1), obsidian.getType(), obsidian.getData().getData());
                        final FallingBlock f11 = location.getWorld().spawnFallingBlock(location.clone().add(-1, 1, -1), obsidian.getType(), obsidian.getData().getData());
                        final FallingBlock f12 = location.getWorld().spawnFallingBlock(location.clone().add(1, 1, 1), obsidian.getType(), obsidian.getData().getData());
                        final FallingBlock f13 = location.getWorld().spawnFallingBlock(location.clone().add(-1, 1, 1), obsidian.getType(), obsidian.getData().getData());
                        final FallingBlock f14 = location.getWorld().spawnFallingBlock(location.clone().add(1, 1, -1), obsidian.getType(), obsidian.getData().getData());
                        final FallingBlock f15 = location.getWorld().spawnFallingBlock(location.clone().add(1, 1, 0), obsidian.getType(), obsidian.getData().getData());
                        final FallingBlock f16 = location.getWorld().spawnFallingBlock(location.clone().add(0, 1, 1), obsidian.getType(), obsidian.getData().getData());
                        //-
                        final FallingBlock f17 = location.getWorld().spawnFallingBlock(location.clone().add(0, 2, 0), obsidian.getType(), obsidian.getData().getData());
                        final FallingBlock f18 = location.getWorld().spawnFallingBlock(location.clone().add(-1, 2, 0), obsidian.getType(), obsidian.getData().getData());
                        final FallingBlock f19 = location.getWorld().spawnFallingBlock(location.clone().add(0, 2, -1), obsidian.getType(), obsidian.getData().getData());
                        final FallingBlock f20 = location.getWorld().spawnFallingBlock(location.clone().add(-1, 2, -1), obsidian.getType(), obsidian.getData().getData());
                        final FallingBlock f21 = location.getWorld().spawnFallingBlock(location.clone().add(1, 2, 1), obsidian.getType(), obsidian.getData().getData());
                        final FallingBlock f22 = location.getWorld().spawnFallingBlock(location.clone().add(-1, 2, 1), obsidian.getType(), obsidian.getData().getData());
                        final FallingBlock f23 = location.getWorld().spawnFallingBlock(location.clone().add(1, 2, -1), obsidian.getType(), obsidian.getData().getData());
                        final FallingBlock f24 = location.getWorld().spawnFallingBlock(location.clone().add(1, 2, 0), obsidian.getType(), obsidian.getData().getData());
                        final FallingBlock f25 = location.getWorld().spawnFallingBlock(location.clone().add(0, 2, 1), obsidian.getType(), obsidian.getData().getData());

                        final List<FallingBlock> list = new ArrayList<>();
                        list.add(f1);
                        list.add(f2);
                        list.add(f3);
                        list.add(f4);
                        list.add(f5);
                        list.add(f6);
                        list.add(f7);
                        list.add(f8);
                        list.add(f9);
                        list.add(f10);
                        list.add(f11);
                        list.add(f12);
                        list.add(f13);
                        list.add(f14);
                        list.add(f15);
                        list.add(f16);
                        list.add(f17);
                        list.add(f18);
                        list.add(f19);
                        list.add(f20);
                        list.add(f21);
                        list.add(f22);
                        list.add(f23);
                        list.add(f24);
                        list.add(f25);

                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                location.setY(location.getY() - 27);
                                for (int i = 1; i <= 10; i++) {
                                    final Random random = new Random();

                                    final int r = random.nextInt(4);
                                    if (r == 0 || r == 1) {
                                        location.getWorld().spawnFallingBlock(location.clone().add(random.nextInt(10), random.nextInt(3), random.nextInt(10)), magma.getType(), magma.getData().getData());
                                    } else if (r == 2) {
                                        location.getWorld().spawnFallingBlock(location.clone().add(-(-random.nextInt(10)), random.nextInt(3), -(-random.nextInt(10))), magma.getType(), magma.getData().getData());
                                    } else if (r == 3) {
                                        location.getWorld().spawnFallingBlock(location.clone().add(random.nextInt(10), random.nextInt(3), -(-random.nextInt(10))), magma.getType(), magma.getData().getData());
                                    } else if (r == 2) {
                                        location.getWorld().spawnFallingBlock(location.clone().add(-(-random.nextInt(10)), random.nextInt(3), random.nextInt(10)), magma.getType(), magma.getData().getData());
                                    }
                                }
                            }
                        }, 37L);

                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                final Entity entity = location.getWorld().spawnEntity(location.clone().add(0, 1, 0), EntityType.PRIMED_TNT);
                                entity.setCustomName(ChatColor.RED + "detrito_event");
                                ((TNTPrimed) entity).setFuseTicks(0);

                                for (final Player target : Bukkit.getOnlinePlayers()) {
                                    target.sendTitle(ChatColor.RED + "FUUUUHH.. BUMM!", ChatColor.GRAY + "Um detrito acaba de cair!", 20, 60, 20);
                                    target.playSound(target.getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 1.0f, 1.0f);
                                }
                            }
                        }, 40L);

                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                first.clone().getBlock().setType(obsidian.getType());
                                first.clone().add(-1, -1, 0).getBlock().setType(obsidian.getType());
                                first.clone().add(0, -1, -1).getBlock().setType(obsidian.getType());
                                first.clone().add(-1, -1, -1).getBlock().setType(obsidian.getType());
                                first.clone().add(1, -1, 1).getBlock().setType(obsidian.getType());
                                first.clone().add(-1, -1, 1).getBlock().setType(obsidian.getType());
                                first.clone().add(1, -1, -1).getBlock().setType(obsidian.getType());
                                first.clone().add(1, -1, 0).getBlock().setType(obsidian.getType());
                                first.clone().add(0, -1, 1).getBlock().setType(obsidian.getType());
                                //-
                                first.clone().add(0, 0, 0).getBlock().setType(chest.getType());
                                first.clone().add(-1, 0, 0).getBlock().setType(obsidian.getType());
                                first.clone().add(0, 0, -1).getBlock().setType(obsidian.getType());
                                first.clone().add(-1, 0, -1).getBlock().setType(obsidian.getType());
                                first.clone().add(1, 0, 1).getBlock().setType(obsidian.getType());
                                first.clone().add(-1, 0, 1).getBlock().setType(obsidian.getType());
                                first.clone().add(1, 0, -1).getBlock().setType(obsidian.getType());
                                first.clone().add(1, 0, 0).getBlock().setType(obsidian.getType());
                                first.clone().add(0, 0, 1).getBlock().setType(obsidian.getType());
                                //-
                                first.clone().add(0, 1, 0).getBlock().setType(obsidian.getType());
                                first.clone().add(-1, 1, 0).getBlock().setType(obsidian.getType());
                                first.clone().add(0, 1, -1).getBlock().setType(obsidian.getType());
                                first.clone().add(-1, 1, -1).getBlock().setType(obsidian.getType());
                                first.clone().add(1, 1, 1).getBlock().setType(obsidian.getType());
                                first.clone().add(-1, 1, 1).getBlock().setType(obsidian.getType());
                                first.clone().add(1, 1, -1).getBlock().setType(obsidian.getType());
                                first.clone().add(1, 1, 0).getBlock().setType(obsidian.getType());
                                first.clone().add(0, 1, 1).getBlock().setType(obsidian.getType());

                                for (final FallingBlock fallingBlock : list) {
                                    fallingBlock.remove();
                                }
                            }
                        }, 43L);

                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                for (final Player target : Bukkit.getOnlinePlayers()) {
                                    target.sendMessage(ChatColor.YELLOW + " \n * Um detrito acaba de cair!\n \n  Mundo: " + ChatColor.GRAY + "Trappist-1b\n  " + ChatColor.YELLOW + "Dica (Posição): " + ChatColor.GRAY + first.getBlockX() + "x ?y ?z \n ");
                                    target.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                            }
                        }, 60L);

                        final int rotate = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                            int time;

                            @Override
                            public void run() {
                                this.time += 2;
                                if (this.time < 10) {
                                    for (final Player target : Bukkit.getOnlinePlayers()) {
                                        target.sendMessage(ChatColor.YELLOW + " \n * Busca ao detrito.\n \n  Mundo: " + ChatColor.GRAY + "Trappist-1b\n  " + ChatColor.YELLOW + "Dica (Posição): " + ChatColor.GRAY + first.getBlockX() + "x ?y ?z \n ");
                                        target.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    }
                                } else if (this.time >= 10) {
                                    for (final Player target : Bukkit.getOnlinePlayers()) {
                                        target.sendMessage(ChatColor.YELLOW + " \n * Busca ao detrito!\n \n  Mundo: " + ChatColor.GRAY + "Trappist-1b\n  " + ChatColor.YELLOW + "Dica (Posição): " + ChatColor.GRAY + first.getBlockX() + "x " + first.getBlockY() + "y ?z \n ");
                                        target.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    }
                                } else if (this.time >= 20) {
                                    for (final Player target : Bukkit.getOnlinePlayers()) {
                                        target.sendMessage(ChatColor.YELLOW + " \n * Busca ao detrito!\n \n  Mundo: " + ChatColor.GRAY + "Trappist-1b\n  " + ChatColor.YELLOW + "Dica (Posição): " + ChatColor.GRAY + first.getBlockX() + "x " + first.getBlockY() + "y " + first.getBlockZ() + "z \n ");
                                        target.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    }
                                }
                            }
                        }, 2400L, 2400L);

                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                final Chest c = (Chest) first.clone().getBlock().getState();
                                c.setCustomName("closed");
                                c.setLock(String.valueOf(rotate));
                                c.update();
                            }
                        }, 60L);

                        if (DetritoHash.get(loc.getBlockX() + ":" + loc.getBlockY() + ":" + loc.getBlockZ()) != null) {
                            DetritoHash.get(loc.getBlockX() + ":" + loc.getBlockY() + ":" + loc.getBlockZ()).setRotate(rotate);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/detrito spawnar");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/detrito spawnar");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
