package com.mixedup.eventos.DetritoEvent.hashs;

import java.util.HashMap;

public class DetritoHash {

    public static HashMap<String, DetritoHash> CACHE = new HashMap<>();
    private final String block;
    private boolean status;
    private int rotate;

    public DetritoHash(final String block, final boolean status, final int rotate) {
        this.block = block;
        this.status = status;
        this.rotate = rotate;
    }

    public static DetritoHash get(final String block) {
        return DetritoHash.CACHE.get(block);
    }

    public DetritoHash insert() {
        DetritoHash.CACHE.put(block, this);
        return this;
    }

    public int getRotate() {
        return rotate;
    }

    public void setRotate(final int rotate) {
        this.rotate = rotate;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(final boolean status) {
        this.status = status;
    }
}
