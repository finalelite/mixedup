package com.mixedup.eventos.DetritoEvent.Listeners;

import com.mixedup.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class DetritoOpenEvent implements Listener {

    @EventHandler
    public void onLeftInv(final InventoryCloseEvent event) {
        final Player player = (Player) event.getPlayer();

        if (event.getInventory().getName().equalsIgnoreCase("Detrito: ")) {
            for (final ItemStack itens : event.getInventory().getContents()) {
                final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(itens);
                for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                    player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                }
            }
        }
    }

    @EventHandler
    public void onInteract(final PlayerInteractEvent event) {
        final Player player = event.getPlayer();

        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && event.getClickedBlock() != null && event.getClickedBlock().getType().equals(Material.CHEST)) {
            if (event.getClickedBlock().getRelative(BlockFace.UP) == null || event.getClickedBlock().getRelative(BlockFace.UP).getType().equals(Material.AIR)) {
                final Chest chest = (Chest) event.getClickedBlock().getState();
                if (chest.getCustomName() != null) {
                    if (chest.getCustomName().equalsIgnoreCase("closed")) {
                        event.setCancelled(true);
                        chest.setCustomName("Chest");
                        Bukkit.getScheduler().cancelTask(Integer.valueOf(chest.getLock()));

                        for (final Player target : Bukkit.getOnlinePlayers()) {
                            target.playSound(target.getLocation(), Sound.BLOCK_CHEST_OPEN, 1.0f, 1.0f);
                            target.sendTitle(ChatColor.YELLOW + player.getName(), ChatColor.GRAY + "Acaba de encontrar o detrito!", 20, 40, 20);
                            target.playSound(target.getLocation(), Sound.ENTITY_VILLAGER_AMBIENT, 1.0f, 1.0f);
                        }

                        chest.setLock(null);
                        chest.update();

                        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Detrito: ");
                        inventory.setItem(13, new ItemStack(Material.NETHER_STAR));

                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                player.openInventory(inventory);
                            }
                        }, 5L);
                    }
                }
            }
        }
    }
}
