package com.mixedup.eventos.DetritoEvent.Listeners;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.FallingBlock;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.util.Vector;

public class DetritoExplodeEvent implements Listener {

    @EventHandler
    public void onEntityExplode(final ExplosionPrimeEvent event) {
        if (event.getEntity().getType() == EntityType.PRIMED_TNT) {
            if (event.getEntity().getCustomName() != null && event.getEntity().getCustomName().equalsIgnoreCase(ChatColor.RED + "detrito_event")) {
                event.setRadius(5);
                event.setFire(true);
            }
        }
    }

    @EventHandler
    public void onExplode(final EntityExplodeEvent event) {
        if (event.getEntity().getCustomName() != null && event.getEntity().getCustomName().equalsIgnoreCase(ChatColor.RED + "detrito_event")) {
            for (final Block block : event.blockList()) {
                final int Max = 1;
                final int Min = -1;
                final double x = Math.random() * (Max - Min) + Min;
                final double y = Math.random() * (Max - Min) + Min;
                final double z = Math.random() * (Max - Min) + Min;

                final FallingBlock fallingBlock = block.getWorld().spawnFallingBlock(block.getLocation(), block.getType(), block.getData());
                fallingBlock.setVelocity(new Vector(x, y, z));
                block.setType(Material.AIR);
            }
        }
    }
}
