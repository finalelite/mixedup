package com.mixedup.inventoryListeners;

import com.mixedup.Main;
import com.mixedup.apis.PlayerUUID;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class InventoryElytra implements Listener {

    @EventHandler
    public void onLeftInv(final InventoryCloseEvent event) {
        final Player player = (Player) event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getInventory().getName().equalsIgnoreCase("Criar asa: ")) {
            for (int i = 1; i <= 20; i++) {
                if (i <= 5) {
                    if (event.getInventory().getItem(i + 9) != null) {
                        final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(event.getInventory().getItem(i + 9));
                        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                        }
                    }
                } else if (i > 5 && i <= 10) {
                    if (event.getInventory().getItem(i + 13) != null) {
                        final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(event.getInventory().getItem(i + 13));
                        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                        }
                    }
                } else if (i > 10 && i <= 15) {
                    if (event.getInventory().getItem(i + 17) != null) {
                        final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(event.getInventory().getItem(i + 17));
                        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                        }
                    }
                } else if (i > 15 && i <= 20) {
                    if (event.getInventory().getItem(i + 21) != null) {
                        final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(event.getInventory().getItem(i + 21));
                        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                        }
                    }
                }
            }
        }

        if (event.getInventory().getName().equalsIgnoreCase("Upar asa: ")) {
            for (int i = 1; i <= 20; i++) {
                if (i <= 5) {
                    if (event.getInventory().getItem(i + 9) != null) {
                        final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(event.getInventory().getItem(i + 9));
                        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                        }
                    }
                } else if (i > 5 && i <= 10) {
                    if (event.getInventory().getItem(i + 13) != null) {
                        final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(event.getInventory().getItem(i + 13));
                        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                        }
                    }
                } else if (i > 10 && i <= 15) {
                    if (event.getInventory().getItem(i + 17) != null) {
                        final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(event.getInventory().getItem(i + 17));
                        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                        }
                    }
                } else if (i > 15 && i <= 20) {
                    if (event.getInventory().getItem(i + 21) != null) {
                        final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(event.getInventory().getItem(i + 21));
                        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onInteract(final InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getClickedInventory() != null && event.getInventory().getName().equalsIgnoreCase("Upar asa: ")) {
            if (event.getAction().equals(InventoryAction.MOVE_TO_OTHER_INVENTORY)) {
                if (event.getSlot() == 10 || event.getSlot() == 11 || event.getSlot() == 12 || event.getSlot() == 13 || event.getSlot() == 14 || event.getSlot() == 19 || event.getSlot() == 20 || event.getSlot() == 21 || event.getSlot() == 22 || event.getSlot() == 23
                        || event.getSlot() == 28 || event.getSlot() == 29 || event.getSlot() == 30 || event.getSlot() == 31 || event.getSlot() == 32 || event.getSlot() == 37 || event.getSlot() == 38 || event.getSlot() == 39 || event.getSlot() == 40 || event.getSlot() == 41) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            if (event.getCurrentItem().getType() != null) {
                                if (event.getCurrentItem().getType().equals(Material.ELYTRA) && event.getCurrentItem().getItemMeta().getDisplayName().contains("Asa ") && event.getCurrentItem().getItemMeta().getDisplayName().contains(" [Nivel ")) {
                                    //if (com.mixedup.managers.InventoryElytra.containsElytraBoolean(event.getClickedInventory()) == false) {
                                    event.getClickedInventory().getItem(48).getItemMeta().setLore(com.mixedup.managers.InventoryElytra.updatePaperUpgradeLv(event.getClickedInventory(), UUID));
                                    //} else {
                                    //    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                    //    event.setCancelled(true);
                                    //}
                                } else {
                                    event.getInventory().getItem(48).getItemMeta().setLore(com.mixedup.managers.InventoryElytra.updatePaperUpgradeLv(event.getWhoClicked().getOpenInventory().getTopInventory(), UUID));
                                }
                                player.updateInventory();
                            }
                        }
                    }, 10L);
                    event.setCancelled(false);
                    return;
                } else {
                    event.setCancelled(true);
                }
            }
        }

        if (event.getClickedInventory() != null && event.getClickedInventory().getName().equalsIgnoreCase("Upar asa: ")) {
            if (event.getSlot() == 10 || event.getSlot() == 11 || event.getSlot() == 12 || event.getSlot() == 13 || event.getSlot() == 14 || event.getSlot() == 19 || event.getSlot() == 20 || event.getSlot() == 21 || event.getSlot() == 22 || event.getSlot() == 23
                    || event.getSlot() == 28 || event.getSlot() == 29 || event.getSlot() == 30 || event.getSlot() == 31 || event.getSlot() == 32 || event.getSlot() == 37 || event.getSlot() == 38 || event.getSlot() == 39 || event.getSlot() == 40 || event.getSlot() == 41) {

                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (event.getCurrentItem().getType().equals(Material.ELYTRA) && event.getCurrentItem().getItemMeta().getDisplayName().contains("Asa ") && event.getCurrentItem().getItemMeta().getDisplayName().contains(" [Nivel ")) {
                            //if (com.mixedup.managers.InventoryElytra.containsElytraBoolean(event.getClickedInventory()) == false) {
                            event.getClickedInventory().getItem(48).getItemMeta().setLore(com.mixedup.managers.InventoryElytra.updatePaperUpgradeLv(event.getClickedInventory(), UUID));
                            //} else {
                            //    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            //    event.setCancelled(true);
                            //}
                        } else {
                            event.getInventory().getItem(48).getItemMeta().setLore(com.mixedup.managers.InventoryElytra.updatePaperUpgradeLv(event.getWhoClicked().getOpenInventory().getTopInventory(), UUID));
                        }
                        player.updateInventory();
                    }
                }, 10L);
                event.setCancelled(false);
                return;
            }

            event.setCancelled(true);

            if (event.getSlot() == 43) {
                final String[] split = event.getClickedInventory().getItem(16).getItemMeta().getDisplayName().split("Nivel ");
                try {
                    final Integer nivel = Integer.valueOf(split[1].substring(0, split[1].length() - 1));

                    final ItemStack frag = new ItemStack(Material.FEATHER);
                    final ItemMeta metaFrag = frag.getItemMeta();
                    metaFrag.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
                    final ArrayList<String> lore1 = new ArrayList<>();
                    lore1.add(" ");
                    lore1.add(ChatColor.GRAY + "Fragmento para asa, tipo: 1");
                    lore1.add(" ");
                    metaFrag.setLore(lore1);
                    frag.setItemMeta(metaFrag);

                    final ItemStack frag2 = new ItemStack(Material.FEATHER);
                    final ItemMeta metaFrag2 = frag2.getItemMeta();
                    metaFrag2.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
                    final ArrayList<String> lore2 = new ArrayList<>();
                    lore2.add(" ");
                    lore2.add(ChatColor.GRAY + "Fragmento para asa, tipo: 2");
                    lore2.add(" ");
                    metaFrag2.setLore(lore2);
                    frag2.setItemMeta(metaFrag2);

                    final ItemStack fragN = new ItemStack(Material.PHANTOM_MEMBRANE);
                    final ItemMeta metaFragN = fragN.getItemMeta();
                    metaFragN.setDisplayName(ChatColor.GREEN + "Fragmento de nível");
                    final ArrayList<String> lore3 = new ArrayList<>();
                    lore3.add(" ");
                    lore3.add(ChatColor.GRAY + "Fragmento de nível p/ asa, nível: " + nivel);
                    lore3.add(" ");
                    metaFragN.setLore(lore3);
                    fragN.setItemMeta(metaFragN);

                    int fragQ = 0;
                    if (nivel == 2) {
                        fragQ = 30;
                    } else if (nivel == 3) {
                        fragQ = 60;
                    }

                    int frag1Q = 0;
                    if (nivel == 2) {
                        frag1Q = 200;
                    } else if (nivel == 3) {
                        frag1Q = 300;
                    }

                    int fragm1 = frag1Q;
                    int dev1 = 0;
                    int fragm2 = 0;
                    int dev2 = 0;
                    int fragmN = fragQ;
                    int dev3 = 0;
                    for (int i = 1; i <= event.getClickedInventory().getSize(); i++) {
                        if (event.getClickedInventory().getItem(i - 1) != null && event.getClickedInventory().getItem(i - 1).getItemMeta().getDisplayName().contains(ChatColor.GREEN + "Fragmento de")) {
                            if (event.getClickedInventory().getItem(i - 1).getItemMeta().getLore().get(1).equalsIgnoreCase(frag.getItemMeta().getLore().get(1))) {
                                fragm1 -= event.getClickedInventory().getItem(i - 1).getAmount();
                                dev1 += event.getClickedInventory().getItem(i - 1).getAmount();
                            } else if (event.getClickedInventory().getItem(i - 1).getItemMeta().getLore().get(1).equalsIgnoreCase(frag2.getItemMeta().getLore().get(1))) {
                                fragm2 += event.getClickedInventory().getItem(i - 1).getAmount();
                                dev2 += event.getClickedInventory().getItem(i - 1).getAmount();
                            } else if (event.getClickedInventory().getItem(i - 1).getItemMeta().getLore().get(1).equalsIgnoreCase(fragN.getItemMeta().getLore().get(1))) {
                                fragmN -= event.getClickedInventory().getItem(i - 1).getAmount();
                                dev3 += event.getClickedInventory().getItem(i - 1).getAmount();
                            }
                        }
                    }

                    if (fragm1 <= 0) {
                        if (fragmN > 0) {
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.RED + " * Ops, faltam " + fragmN + " fragmentos de nível.");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            return;
                        } else {
                            frag.setAmount(dev1 - frag1Q);
                            if (frag.getAmount() <= 0) {
                                if (dev2 > 0) {
                                    final int total = dev2 * 2;
                                    int faltam = dev1 - frag1Q;
                                    final int retorno = total + (--faltam);
                                    frag2.setAmount(retorno / 2);
                                }
                            } else {
                                frag2.setAmount(dev2);
                            }
                            fragN.setAmount(dev3 - fragQ);
                            final HashMap<Integer, ItemStack> nope3 = player.getOpenInventory().getBottomInventory().addItem(event.getClickedInventory().getItem(16));
                            for (final Entry<Integer, ItemStack> entry : nope3.entrySet()) {
                                player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                            }
                            final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(frag);
                            for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                            }
                            final HashMap<Integer, ItemStack> nope1 = player.getOpenInventory().getBottomInventory().addItem(frag2);
                            for (final Entry<Integer, ItemStack> entry : nope1.entrySet()) {
                                player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                            }
                            final HashMap<Integer, ItemStack> nope2 = player.getOpenInventory().getBottomInventory().addItem(fragN);
                            for (final Entry<Integer, ItemStack> entry : nope2.entrySet()) {
                                player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                            }
                            player.sendMessage(ChatColor.GREEN + " * Asa concedida com sucesso!");
                            player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                            event.getClickedInventory().clear();
                            player.getOpenInventory().close();
                        }
                    } else {
                        final int soma = dev1 - fragQ - (dev2 * 2);
                        if (soma <= 0) {
                            if (fragmN > 0) {
                                event.setCancelled(true);
                                player.sendMessage(ChatColor.RED + " * Ops, faltam " + fragmN + " fragmentos de nível.");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                return;
                            }
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.RED + " * Ops, faltam " + (frag1Q - dev1) + " fragmentos de asa.");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            return;
                        } else {
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.RED + " * Ops, faltam " + (frag1Q - dev1) + " fragmentos de asa.");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            return;
                        }
                    }
                } catch (final NumberFormatException e) {
                    player.sendMessage(ChatColor.RED + " * Ops, está asa se encontra no ultimo nível.");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }

            if (event.getSlot() == 34) {
                player.getOpenInventory().close();
                player.sendMessage(ChatColor.RED + " * Ação cancelada.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------

        if (event.getClickedInventory() != null && event.getInventory().getName().equalsIgnoreCase("Criar asa: ")) {
            if (event.getAction().equals(InventoryAction.MOVE_TO_OTHER_INVENTORY)) {
                if (event.getSlot() == 10 || event.getSlot() == 11 || event.getSlot() == 12 || event.getSlot() == 13 || event.getSlot() == 14 || event.getSlot() == 19 || event.getSlot() == 20 || event.getSlot() == 21 || event.getSlot() == 22 || event.getSlot() == 23
                        || event.getSlot() == 28 || event.getSlot() == 29 || event.getSlot() == 30 || event.getSlot() == 31 || event.getSlot() == 32 || event.getSlot() == 37 || event.getSlot() == 38 || event.getSlot() == 39 || event.getSlot() == 40 || event.getSlot() == 41) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            event.getInventory().getItem(48).getItemMeta().setLore(com.mixedup.managers.InventoryElytra.updatePaper(event.getWhoClicked().getOpenInventory().getTopInventory()));
                            player.updateInventory();
                        }
                    }, 10L);
                    event.setCancelled(false);
                    return;
                } else {
                    event.setCancelled(true);
                }
            }
        }

        if (event.getClickedInventory() != null && event.getClickedInventory().getName().equalsIgnoreCase("Criar asa: ")) {
            if (event.getSlot() == 10 || event.getSlot() == 11 || event.getSlot() == 12 || event.getSlot() == 13 || event.getSlot() == 14 || event.getSlot() == 19 || event.getSlot() == 20 || event.getSlot() == 21 || event.getSlot() == 22 || event.getSlot() == 23
                    || event.getSlot() == 28 || event.getSlot() == 29 || event.getSlot() == 30 || event.getSlot() == 31 || event.getSlot() == 32 || event.getSlot() == 37 || event.getSlot() == 38 || event.getSlot() == 39 || event.getSlot() == 40 || event.getSlot() == 41) {

                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        event.getClickedInventory().getItem(48).getItemMeta().setLore(com.mixedup.managers.InventoryElytra.updatePaper(event.getClickedInventory()));
                        player.updateInventory();
                    }
                }, 10L);
                event.setCancelled(false);
                return;
            }

            event.setCancelled(true);

            if (event.getSlot() == 43) {
                final ItemStack frag = new ItemStack(Material.FEATHER);
                final ItemMeta metaFrag = frag.getItemMeta();
                metaFrag.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
                final ArrayList<String> lore1 = new ArrayList<>();
                lore1.add(" ");
                lore1.add(ChatColor.GRAY + "Fragmento para asa, tipo: 1");
                lore1.add(" ");
                metaFrag.setLore(lore1);
                frag.setItemMeta(metaFrag);

                final ItemStack frag2 = new ItemStack(Material.FEATHER);
                final ItemMeta metaFrag2 = frag2.getItemMeta();
                metaFrag2.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
                final ArrayList<String> lore2 = new ArrayList<>();
                lore2.add(" ");
                lore2.add(ChatColor.GRAY + "Fragmento para asa, tipo: 2");
                lore2.add(" ");
                metaFrag2.setLore(lore2);
                frag2.setItemMeta(metaFrag2);

                final ItemStack fragN = new ItemStack(Material.PHANTOM_MEMBRANE);
                final ItemMeta metaFragN = fragN.getItemMeta();
                metaFragN.setDisplayName(ChatColor.GREEN + "Fragmento de nível");
                final ArrayList<String> lore3 = new ArrayList<>();
                lore3.add(" ");
                lore3.add(ChatColor.GRAY + "Fragmento de nível p/ asa, nível: 1");
                lore3.add(" ");
                metaFragN.setLore(lore3);
                fragN.setItemMeta(metaFragN);

                final int fragQ = 15;
                int fragm1 = 100;
                int dev1 = 0;
                int fragm2 = 0;
                int dev2 = 0;
                int fragmN = fragQ;
                int dev3 = 0;
                for (int i = 1; i <= event.getClickedInventory().getSize(); i++) {
                    if (event.getClickedInventory().getItem(i - 1) != null && event.getClickedInventory().getItem(i - 1).getItemMeta().getDisplayName().contains(ChatColor.GREEN + "Fragmento de")) {
                        if (event.getClickedInventory().getItem(i - 1).getItemMeta().getLore().get(1).equalsIgnoreCase(frag.getItemMeta().getLore().get(1))) {
                            fragm1 -= event.getClickedInventory().getItem(i - 1).getAmount();
                            dev1 += event.getClickedInventory().getItem(i - 1).getAmount();
                        } else if (event.getClickedInventory().getItem(i - 1).getItemMeta().getLore().get(1).equalsIgnoreCase(frag2.getItemMeta().getLore().get(1))) {
                            fragm2 += event.getClickedInventory().getItem(i - 1).getAmount();
                            dev2 += event.getClickedInventory().getItem(i - 1).getAmount();
                        } else if (event.getClickedInventory().getItem(i - 1).getItemMeta().getLore().get(1).equalsIgnoreCase(fragN.getItemMeta().getLore().get(1))) {
                            fragmN -= event.getClickedInventory().getItem(i - 1).getAmount();
                            dev3 += event.getClickedInventory().getItem(i - 1).getAmount();
                        }
                    }
                }

                final int frag1Q = 100;

                //FRAGMENTO DE ASA JÁ CONTÉM
                if (fragm1 <= 0) {
                    if (fragmN > 0) {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, faltam " + fragmN + " fragmentos de nível.");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        return;
                    } else {
                        frag.setAmount(dev1 - 100);
                        if (frag.getAmount() <= 0) {
                            if (dev2 > 0) {
                                final int total = dev2 * 2;
                                int faltam = dev1 - frag1Q;
                                final int retorno = total + (--faltam);
                                frag2.setAmount(retorno / 2);
                            }
                        } else {
                            frag2.setAmount(dev2);
                        }
                        fragN.setAmount(dev3 - fragQ);
                        final HashMap<Integer, ItemStack> nope3 = player.getOpenInventory().getBottomInventory().addItem(event.getClickedInventory().getItem(16));
                        for (final Entry<Integer, ItemStack> entry : nope3.entrySet()) {
                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                        }
                        final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(frag);
                        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                        }
                        final HashMap<Integer, ItemStack> nope1 = player.getOpenInventory().getBottomInventory().addItem(frag2);
                        for (final Entry<Integer, ItemStack> entry : nope1.entrySet()) {
                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                        }
                        final HashMap<Integer, ItemStack> nope2 = player.getOpenInventory().getBottomInventory().addItem(fragN);
                        for (final Entry<Integer, ItemStack> entry : nope2.entrySet()) {
                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                        }
                        player.sendMessage(ChatColor.GREEN + " * Asa concedida com sucesso!");
                        player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                        event.getClickedInventory().clear();
                        player.getOpenInventory().close();
                    }
                } else {
                    final int soma = dev1 - fragQ - (dev2 * 2);
                    if (soma <= 0) {
                        if (fragmN > 0) {
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.RED + " * Ops, faltam " + fragmN + " fragmentos de nível.");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            return;
                        }
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, faltam " + (100 - dev1) + " fragmentos de asa.");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        return;
                    } else {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, faltam " + (100 - dev1) + " fragmentos de asa.");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        return;
                    }
                }
            }

            if (event.getSlot() == 34) {
                player.getOpenInventory().close();
                player.sendMessage(ChatColor.RED + " * Ação cancelada.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }

        if (event.getInventory().getName().equalsIgnoreCase("Ferreiro de asas: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 12) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryElytra.buyElytra(UUID));
                    }
                }, 5L);
            }

            if (event.getSlot() == 14) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryElytra.upgradeElytra(UUID));
                    }
                }, 5L);
            }
        }
    }
}
