package com.mixedup.inventoryListeners;

import com.mixedup.Main;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.economy.CoinsAPI;
import com.mixedup.hashs.AskedServeHash;
import com.mixedup.rpg.MobdropMoedas;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class InventoryBanqueiro implements Listener {

    private static final DecimalFormat formatter = new DecimalFormat("#,###.00");

    @EventHandler
    public void onSay(final AsyncPlayerChatEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (AskedServeHash.get(UUID) != null && AskedServeHash.get(UUID).getAsked() == true) {
            try {
                final int valor = Integer.valueOf(event.getMessage());

                if (valor < 0) {
                    player.sendMessage(ChatColor.RED + " * Ops, valor incorreto!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }

                if (CoinsAPI.getBlackCoins(UUID) < valor) {
                    player.sendMessage(ChatColor.RED + " * Ops, você não contém dinheiro para isto!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    AskedServeHash.get(UUID).setAsked(false);
                    return;
                }

                CoinsAPI.updateBlackCoins(UUID, CoinsAPI.getBlackCoins(UUID) - valor);
//                val sb = Main.scoreboardManager;
//                sb.getDefaultScoreboard().updateEntry(player, "coins");
                event.setCancelled(true);
                event.setMessage(null);
                player.sendMessage(ChatColor.GREEN + " * Foram sacados $" + InventoryBanqueiro.formatter.format(valor) + " moedas negras!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_VEX_CHARGE, 1.0f, 1.0f);

                for (int i = 0; i <= player.getInventory().getSize(); i++) {
                    if (player.getInventory().getItem(i) != null) {
                        if (player.getInventory().getItem(i).getType().equals(Material.CARROT_ON_A_STICK)) {
                            if (player.getInventory().getItem(i).getItemMeta().getLore().get(1).contains("Moeda(s): ")) {
                                try {
                                    final int quantia = Integer.valueOf(player.getInventory().getItem(i).getItemMeta().getLore().get(1).substring(14));
                                    final ArrayList<String> lore = new ArrayList<>();
                                    lore.add(" ");
                                    lore.add(ChatColor.DARK_GRAY + "Moeda(s): " + ChatColor.GRAY + (quantia + valor));
                                    lore.add(" ");
                                    final ItemMeta meta = player.getInventory().getItem(i).getItemMeta();
                                    meta.setLore(lore);
                                    player.getInventory().getItem(i).setItemMeta(meta);
                                    MobdropMoedas.removeItem(player.getInventory(), i);
                                    break;
                                } catch (final NumberFormatException e) {
                                    //-
                                }
                            }
                        }
                    }

                    if (player.getInventory().getSize() == i) {
                        final ItemStack moeda = new ItemStack(Material.CARROT_ON_A_STICK);
                        final ItemMeta meta = moeda.getItemMeta();
                        meta.setDisplayName(ChatColor.YELLOW + "Saco de moedas");
                        final ArrayList<String> lore = new ArrayList<>();
                        lore.add(" ");
                        lore.add(ChatColor.DARK_GRAY + "Moeda(s): " + ChatColor.GRAY + valor);
                        lore.add(" ");
                        meta.setLore(lore);
                        moeda.setItemMeta(meta);

                        final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(moeda);
                        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                        }
                    }
                }
                AskedServeHash.get(UUID).setAsked(false);
            } catch (final NumberFormatException e) {
                player.sendMessage(ChatColor.RED + " * Ops, valor informado contém algum erro.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
    }

    @EventHandler
    public void onLeftInv(final InventoryCloseEvent event) {
        if (event.getInventory().getName().equalsIgnoreCase("Banqueiro: ")) {
            if (event.getInventory().getItem(12) != null) {
                final HashMap<Integer, ItemStack> nope = event.getPlayer().getOpenInventory().getBottomInventory().addItem(event.getInventory().getItem(12));
                for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                    event.getPlayer().getWorld().dropItemNaturally(event.getPlayer().getLocation(), entry.getValue());
                }
            }
        }
    }

    @EventHandler
    public void onClick(final InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getClickedInventory() != null && event.getClickedInventory().getName().equalsIgnoreCase("Banqueiro: ")) {

            if (event.getSlot() == 12 && event.getCurrentItem().getType().equals(Material.CARROT_ON_A_STICK)) {
                event.setCancelled(false);
                return;
            }

            if (event.getSlot() == 12 && event.getCursor() != null && event.getCursor().getType().equals(Material.CARROT_ON_A_STICK)) {
                event.setCancelled(false);
                return;
            }

            event.setCancelled(true);

            if (event.getSlot() == 16) {
                // if (event.getClickedInventory().getItem(12) != null) {
                //                    HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(new ItemStack[]{event.getClickedInventory().getItem(12)});
                //                    for (Map.Entry<Integer, ItemStack> entry : nope.entrySet()) {
                //                        player.getWorld().dropItemNaturally(player.getLocation(), (ItemStack) entry.getValue());
                //                    }
                //                }
                player.getOpenInventory().close();
                player.sendMessage(ChatColor.RED + " * Volte para negociarmos mais tarde!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_AMBIENT, 1.0f, 1.0f);
            }

            if (event.getSlot() == 15) {
                if (event.getClickedInventory().getItem(12) != null) {
                    if (!event.getClickedInventory().getItem(12).hasItemMeta() && !event.getClickedInventory().getItem(12).getItemMeta().hasLore() && event.getClickedInventory().getItem(12).getItemMeta().getLore().size() < 2)
                        return;
                    final String[] split = event.getClickedInventory().getItem(12).getItemMeta().getLore().get(1).split(": §7");
                    try {
                        event.getClickedInventory().setItem(12, new ItemStack(Material.AIR));
                        final int moedas = Integer.valueOf(split[1]);

                        player.getOpenInventory().close();
                        CoinsAPI.updateBlackCoins(UUID, CoinsAPI.getBlackCoins(UUID) + moedas);
                        final val sb = Main.scoreboardManager;
//                        sb.getDefaultScoreboard().updateEntry(player, "blackcoins");

                        player.sendMessage(ChatColor.GREEN + "\n Banqueiro: \n" + ChatColor.GRAY + " * Foram depositados em sua conta " + InventoryBanqueiro.formatter.format(moedas) + " moedas negras.\n ");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                    } catch (final NumberFormatException e) {
                        //-
                    }
                }
            }

            if (event.getSlot() == 14) {
                if (AskedServeHash.get(UUID) == null) {
                    new AskedServeHash(UUID, true).insert();

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            if (AskedServeHash.get(UUID).getAsked() == true) {
                                AskedServeHash.get(UUID).setAsked(false);
                                player.sendMessage(ChatColor.RED + " * Ops, você demorou para informar um valor para saque.");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        }
                    }, 400L);
                } else {
                    if (AskedServeHash.get(UUID).getAsked() == false) {
                        AskedServeHash.get(UUID).setAsked(true);

                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                if (AskedServeHash.get(UUID).getAsked() == true) {
                                    AskedServeHash.get(UUID).setAsked(false);
                                    player.sendMessage(ChatColor.RED + " * Ops, você demorou para informar um valor para saque.");
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                            }
                        }, 400L);
                    }
                }

                player.getOpenInventory().close();
                player.sendMessage(ChatColor.GREEN + "\n * Informe um valor para sacar!\n ");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
    }
}
