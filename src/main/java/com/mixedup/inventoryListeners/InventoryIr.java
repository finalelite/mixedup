package com.mixedup.inventoryListeners;

import com.mixedup.apis.ConfigAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.RegionsSpawnAPI;
import com.mixedup.apis.TagAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryIr implements Listener {

    @EventHandler
    public void onInteract(final InventoryClickEvent event) {

        Player player = (Player) event.getWhoClicked();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getInventory().getName().equalsIgnoreCase("Ir: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 13) {
                final String[] loc = RegionsSpawnAPI.getLocation("Gliese-667-Cc").split(":");
                final Location location = new Location(Bukkit.getWorld(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), Float.parseFloat(loc[4]), Float.parseFloat(loc[5]));
                player.sendMessage(ChatColor.GREEN + "\n * Teletransportando para Kepler-438b\n ");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.teleport(location);
            }

            if (event.getSlot() == 14) {
                final String[] loc = RegionsSpawnAPI.getLocation("Trappist-1b").split(":");
                final Location location = new Location(Bukkit.getWorld(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), Float.parseFloat(loc[4]), Float.parseFloat(loc[5]));
                player.sendMessage(ChatColor.GREEN + "\n * Teletransportando para Trappist-1b\n ");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.teleport(location);
            }

            if (event.getSlot() == 15) {
                final String[] loc = RegionsSpawnAPI.getLocation("nether").split(":");
                final Location location = new Location(Bukkit.getWorld(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), Float.parseFloat(loc[4]), Float.parseFloat(loc[5]));
                player.sendMessage(ChatColor.GREEN + "\n * Teletransportando para Nether\n ");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.teleport(location);
            }

            if (event.getSlot() == 28) {
                final String[] loc = RegionsSpawnAPI.getLocation("arena").split(":");
                final Location location = new Location(Bukkit.getWorld(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), Float.parseFloat(loc[4]), Float.parseFloat(loc[5]));
                player.sendMessage(ChatColor.GREEN + "\n * Teletransportando para Arena\n ");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.teleport(location);
            }

            if (event.getSlot() == 32) {
                final String[] loc = RegionsSpawnAPI.getLocation("banqueiro").split(":");
                final Location location = new Location(Bukkit.getWorld(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), Float.parseFloat(loc[4]), Float.parseFloat(loc[5]));
                player.sendMessage(ChatColor.GREEN + "\n * Teletransportando para Banqueiro\n ");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.teleport(location);
            }

            if (event.getSlot() == 16) {
                //player.sendMessage(ChatColor.RED + " * Temporariamente desabilitado.");
                //player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                final String[] loc = RegionsSpawnAPI.getLocation("aether").split(":");
                final Location location = new Location(Bukkit.getWorld(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), Float.parseFloat(loc[4]), Float.parseFloat(loc[5]));
                player.sendMessage(ChatColor.GREEN + "\n * Teletransportando para mundo das Elytras\n ");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.teleport(location);
            }

            if (event.getSlot() == 29) {
                final String[] loc = RegionsSpawnAPI.getLocation("guildaselect").split(":");
                final Location location = new Location(Bukkit.getWorld(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), Float.parseFloat(loc[4]), Float.parseFloat(loc[5]));
                player.sendMessage(ChatColor.GREEN + "\n * Teletransportando para mundo das guildas\n  ");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.teleport(location);
            }

            if (event.getSlot() == 34) {
                final String[] loc = RegionsSpawnAPI.getLocation("loja").split(":");
                final Location location = new Location(Bukkit.getWorld(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), Float.parseFloat(loc[4]), Float.parseFloat(loc[5]));
                player.sendMessage(ChatColor.GREEN + "\n * Teletransportando para Loja\n  ");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.teleport(location);
            }

            if (event.getSlot() == 10) {
                final String[] loc = RegionsSpawnAPI.getLocation("spawn").split(":");
                final Location location = new Location(Bukkit.getWorld(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), Float.parseFloat(loc[4]), Float.parseFloat(loc[5]));
                player.sendMessage(ChatColor.GREEN + "\n * Teletransportando para Spawn\n  ");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.teleport(location);
            }

            if (event.getSlot() == 12) {
                final String[] loc = RegionsSpawnAPI.getLocation("skygrid").split(":");
                final Location location = new Location(Bukkit.getWorld(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), Float.parseFloat(loc[4]), Float.parseFloat(loc[5]));
                player.sendMessage(ChatColor.GREEN + "\n * Teletransportando para Skygrid\n  ");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.teleport(location);
            }

            if (event.getSlot() == 33) {
                if (!TagAPI.getTag(UUID).equalsIgnoreCase("Membro")) {
                    final String[] loc = RegionsSpawnAPI.getLocation("area-vip").split(":");
                    final Location location = new Location(Bukkit.getWorld(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), Float.parseFloat(loc[4]), Float.parseFloat(loc[5]));
                    player.sendMessage(ChatColor.GREEN + "\n * Teletransportando para Área vip\n  ");
                    player.setAllowFlight(true);
                    ConfigAPI.updateFly(UUID, true);
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.teleport(location);
                } else {
                    //-
                }
            }

            if (event.getSlot() == 30) {

            }
        }
    }
}
