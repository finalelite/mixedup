package com.mixedup.inventoryListeners;

import com.mixedup.apis.LojaPosAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.economy.CoinsAPI;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryLojaWarp implements Listener {

    @EventHandler
    public void onInteractInventory(final InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getInventory().getName().equalsIgnoreCase("Deletar warp mercado: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 12) {
                LojaPosAPI.removeLojaPos(UUID);

                player.sendMessage(ChatColor.GREEN + " * Warp mercado deletada!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.getOpenInventory().close();
            }

            if (event.getSlot() == 14) {
                player.sendMessage(ChatColor.RED + "Evento cancelado!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.getOpenInventory().close();
            }
        }

        if (event.getInventory().getName().equalsIgnoreCase("Confirmar compra warp mercado: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 30) {
                final int coins = CoinsAPI.getCoins(UUID);

                if (coins >= 10000) {
                    CoinsAPI.updateCoins(UUID, coins - 10000);
                    final String location = player.getWorld().getName() + ":" + player.getLocation().getX() + ":" + player.getLocation().getY() + ":" + player.getLocation().getZ() + ":" + player.getLocation().getYaw() + ":" + player.getLocation().getPitch();
                    LojaPosAPI.createLojaPos(UUID, location);

                    player.sendMessage(ChatColor.GREEN + " * Warp para sua mercado adquirida com sucesso!\n" + ChatColor.GRAY + "  /mercado " + player.getName());
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.getOpenInventory().close();
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, você não contém dinheiro para efetuar está ação!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.getOpenInventory().close();
                }
            }

            if (event.getSlot() == 32) {
                player.sendMessage(ChatColor.RED + "Evento cancelado!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.getOpenInventory().close();
            }
        }
    }
}
