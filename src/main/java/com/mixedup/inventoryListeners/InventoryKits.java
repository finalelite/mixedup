package com.mixedup.inventoryListeners;

import com.mixedup.Main;
import com.mixedup.apis.HorarioAPI;
import com.mixedup.apis.KitsVipAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import com.mixedup.utils.AlternateColor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryKits implements Listener {

    @EventHandler
    public void onInteractInventory(final InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getInventory().getName().equalsIgnoreCase("Kits VIP (Conde): ")) {
            event.setCancelled(true);

            if (event.getSlot() == 10) {
                if (event.isRightClick()) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitMensalConde());
                        }
                    }, 5l);
                }
                if (event.isLeftClick()) {
                    if (TagAPI.getTag(UUID).equals("VIP") || TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                        if (TagAPI.getTag(UUID).equals("VIP")) {
                            if (KitsVipAPI.getCountdown(UUID + "MENSAL") <= 0) {
                                KitsVipAPI.updateCountdown(UUID + "MENSAL", 2678400);

                                player.sendMessage(ChatColor.GREEN + "\n \n * Kit mensal pego com sucesso!\n \n");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                player.getOpenInventory().close();

                                com.mixedup.managers.InventoryKits.giveKitMensalConde(player);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Seu kit será liberado em " + AlternateColor.alternate("&c&n" + HorarioAPI.getTime(KitsVipAPI.getCountdown(UUID + "MENSAL"))) + ChatColor.RED + ".");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                            com.mixedup.managers.InventoryKits.giveKitMensalConde(player);
                            player.sendMessage(ChatColor.GREEN + "\n \n * Kit mensal (Conde) pego com sucesso!\n \n");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                            player.getOpenInventory().close();
                        }
                    } else {
                        player.sendMessage(ChatColor.DARK_GRAY + "\n \n * Adquira " + ChatColor.AQUA + "VIP" + ChatColor.DARK_GRAY + " e obtenha vantagem como estas em: \n  www.finalelite.com.br\n \n");
                    }
                }
            }

            if (event.getSlot() == 12) {
                if (event.isRightClick()) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitSemanalConde());
                        }
                    }, 5l);
                }
                if (event.isLeftClick()) {
                    if (TagAPI.getTag(UUID).equals("VIP") || TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                        if (TagAPI.getTag(UUID).equals("VIP")) {
                            if (KitsVipAPI.getCountdown(UUID + "SEMANAL") <= 0) {
                                KitsVipAPI.updateCountdown(UUID + "SEMANAL", 604800);

                                player.sendMessage(ChatColor.GREEN + "\n \n * Kit semanal pego com sucesso!\n \n");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                player.getOpenInventory().close();

                                com.mixedup.managers.InventoryKits.giveKitSemanalConde(player);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Seu kit será liberado em " + AlternateColor.alternate("&c&n" + HorarioAPI.getTime(KitsVipAPI.getCountdown(UUID + "SEMANAL"))) + ChatColor.RED + ".");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                            com.mixedup.managers.InventoryKits.giveKitSemanalConde(player);
                            player.sendMessage(ChatColor.GREEN + "\n \n * Kit semanal (Conde) pego com sucesso!\n \n");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                            player.getOpenInventory().close();
                        }
                    } else {
                        player.sendMessage(ChatColor.DARK_GRAY + "\n \n * Adquira " + ChatColor.AQUA + "VIP" + ChatColor.DARK_GRAY + " e obtenha vantagem como estas em: \n  www.finalelite.com.br\n \n");
                    }
                }
            }

            if (event.getSlot() == 16) {
                if (event.isRightClick()) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitDiarioConde());
                        }
                    }, 5l);
                }
                if (event.isLeftClick()) {
                    if (TagAPI.getTag(UUID).equals("VIP") || TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                        if (TagAPI.getTag(UUID).equals("VIP")) {
                            if (KitsVipAPI.getCountdown(UUID + "DIARIO") <= 0) {
                                KitsVipAPI.updateCountdown(UUID + "DIARIO", 86400);

                                player.sendMessage(ChatColor.GREEN + "\n \n * Kit diário pego com sucesso!\n \n");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                player.getOpenInventory().close();

                                com.mixedup.managers.InventoryKits.giveKitDiarioConde(player);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Seu kit será liberado em " + AlternateColor.alternate("&c&n" + HorarioAPI.getTime(KitsVipAPI.getCountdown(UUID + "DIARIO"))) + ChatColor.RED + ".");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                            com.mixedup.managers.InventoryKits.giveKitDiarioConde(player);
                            player.sendMessage(ChatColor.GREEN + "\n \n * Kit diário (Conde) pego com sucesso!\n \n");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                            player.getOpenInventory().close();
                        }
                    } else {
                        player.sendMessage(ChatColor.DARK_GRAY + "\n \n * Adquira " + ChatColor.AQUA + "VIP" + ChatColor.DARK_GRAY + " e obtenha vantagem como estas em: \n  www.finalelite.com.br\n \n");
                    }
                }
            }

            if (event.getSlot() == 14) {
                if (event.isRightClick()) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mixedup.managers.InventoryKits.inventoryKit6HorasConde());
                        }
                    }, 5l);
                }
                if (event.isLeftClick()) {
                    if (TagAPI.getTag(UUID).equals("VIP") || TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                        if (TagAPI.getTag(UUID).equals("VIP")) {
                            if (KitsVipAPI.getCountdown(UUID + "6HORASVIP") <= 0) {
                                KitsVipAPI.updateCountdown(UUID + "6HORASVIP", 21600);

                                player.sendMessage(ChatColor.GREEN + "\n \n * Kit 6 horas jogadas pego com sucesso!\n \n");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                player.getOpenInventory().close();

                                com.mixedup.managers.InventoryKits.giveKit6HorasConde(player);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Seu kit será liberado em +" + AlternateColor.alternate("&c&n" + HorarioAPI.getTime(KitsVipAPI.getCountdown(UUID + "6HORASVIP") - 21600)) + ChatColor.RED + ".");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                            com.mixedup.managers.InventoryKits.giveKit6HorasConde(player);
                            player.sendMessage(ChatColor.GREEN + "\n \n * Kit 6 horas jogadas (Conde) pego com sucesso!\n \n");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                            player.getOpenInventory().close();
                        }
                    } else {
                        player.sendMessage(ChatColor.DARK_GRAY + "\n \n * Adquira " + ChatColor.AQUA + "VIP" + ChatColor.DARK_GRAY + " e obtenha vantagem como estas em: \n  www.finalelite.com.br\n \n");
                    }
                }
            }

            if (event.getSlot() == 18) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsVips());
                    }
                }, 5l);
            }
        }

        if (event.getInventory().getName().equalsIgnoreCase("Kits VIP (Lord): ")) {
            event.setCancelled(true);

            if (event.getSlot() == 10) {
                if (event.isRightClick()) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitMensalLord());
                        }
                    }, 5l);
                }
                if (event.isLeftClick()) {
                    if (TagAPI.getTag(UUID).equals("VIP+") || TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                        if (TagAPI.getTag(UUID).equals("VIP+")) {
                            if (KitsVipAPI.getCountdown(UUID + "MENSAL") <= 0) {
                                KitsVipAPI.updateCountdown(UUID + "MENSAL", 2678400);

                                player.sendMessage(ChatColor.GREEN + "\n \n * Kit mensal pego com sucesso!\n \n");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                player.getOpenInventory().close();

                                com.mixedup.managers.InventoryKits.giveKitMensalLord(player);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Seu kit será liberado em " + AlternateColor.alternate("&c&n" + HorarioAPI.getTime(KitsVipAPI.getCountdown(UUID + "MENSAL"))) + ChatColor.RED + ".");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                            com.mixedup.managers.InventoryKits.giveKitMensalLord(player);
                            player.sendMessage(ChatColor.GREEN + "\n \n * Kit mensal (Lord) pego com sucesso!\n \n");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                            player.getOpenInventory().close();
                        }
                    } else {
                        player.sendMessage(ChatColor.DARK_GRAY + "\n \n * Adquira " + ChatColor.AQUA + "VIP" + ChatColor.DARK_GRAY + " e obtenha vantagem como estas em: \n  www.finalelite.com.br\n \n");
                    }
                }
            }

            if (event.getSlot() == 12) {
                if (event.isRightClick()) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitSemanalLord());
                        }
                    }, 5l);
                }
                if (event.isLeftClick()) {
                    if (TagAPI.getTag(UUID).equals("VIP+") || TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                        if (TagAPI.getTag(UUID).equals("VIP+")) {
                            if (KitsVipAPI.getCountdown(UUID + "SEMANAL") <= 0) {
                                KitsVipAPI.updateCountdown(UUID + "SEMANAL", 604800);

                                player.sendMessage(ChatColor.GREEN + "\n \n * Kit semanal pego com sucesso!\n \n");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                player.getOpenInventory().close();

                                com.mixedup.managers.InventoryKits.giveKitSemanalLord(player);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Seu kit será liberado em " + AlternateColor.alternate("&c&n" + HorarioAPI.getTime(KitsVipAPI.getCountdown(UUID + "SEMANAL"))) + ChatColor.RED + ".");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                            com.mixedup.managers.InventoryKits.giveKitSemanalLord(player);
                            player.sendMessage(ChatColor.GREEN + "\n \n * Kit semanal (Lord) pego com sucesso!\n \n");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                            player.getOpenInventory().close();
                        }
                    } else {
                        player.sendMessage(ChatColor.DARK_GRAY + "\n \n * Adquira " + ChatColor.AQUA + "VIP" + ChatColor.DARK_GRAY + " e obtenha vantagem como estas em: \n  www.finalelite.com.br\n \n");
                    }
                }
            }

            if (event.getSlot() == 16) {
                if (event.isRightClick()) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitDiarioLord());
                        }
                    }, 5l);
                }
                if (event.isLeftClick()) {
                    if (TagAPI.getTag(UUID).equals("VIP+") || TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                        if (TagAPI.getTag(UUID).equals("VIP+")) {
                            if (KitsVipAPI.getCountdown(UUID + "DIARIO") <= 0) {
                                KitsVipAPI.updateCountdown(UUID + "DIARIO", 86400);

                                player.sendMessage(ChatColor.GREEN + "\n \n * Kit diário pego com sucesso!\n \n");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                player.getOpenInventory().close();

                                com.mixedup.managers.InventoryKits.giveKitDiarioLord(player);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Seu kit será liberado em " + AlternateColor.alternate("&c&n" + HorarioAPI.getTime(KitsVipAPI.getCountdown(UUID + "DIARIO"))) + ChatColor.RED + ".");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                            com.mixedup.managers.InventoryKits.giveKitDiarioLord(player);
                            player.sendMessage(ChatColor.GREEN + "\n \n * Kit diário (Lord) pego com sucesso!\n \n");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                            player.getOpenInventory().close();
                        }
                    } else {
                        player.sendMessage(ChatColor.DARK_GRAY + "\n \n * Adquira " + ChatColor.AQUA + "VIP" + ChatColor.DARK_GRAY + " e obtenha vantagem como estas em: \n  www.finalelite.com.br\n \n");
                    }
                }
            }

            if (event.getSlot() == 14) {
                if (event.isRightClick()) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mixedup.managers.InventoryKits.inventoryKit6HorasLord());
                        }
                    }, 5l);
                }
                if (event.isLeftClick()) {
                    if (TagAPI.getTag(UUID).equals("VIP+") || TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                        if (TagAPI.getTag(UUID).equals("VIP+")) {
                            if (KitsVipAPI.getCountdown(UUID + "6HORASVIP") <= 0) {
                                KitsVipAPI.updateCountdown(UUID + "6HORASVIP", 21600);

                                player.sendMessage(ChatColor.GREEN + "\n \n * Kit 6 horas jogadas pego com sucesso!\n \n");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                player.getOpenInventory().close();

                                com.mixedup.managers.InventoryKits.giveKit6HorasLord(player);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Seu kit será liberado em +" + AlternateColor.alternate("&c&n" + HorarioAPI.getTime(KitsVipAPI.getCountdown(UUID + "6HORASVIP") - 21600)) + ChatColor.RED + ".");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                            com.mixedup.managers.InventoryKits.giveKit6HorasLord(player);
                            player.sendMessage(ChatColor.GREEN + "\n \n * Kit 6 horas jogadas (Lord) pego com sucesso!\n \n");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                            player.getOpenInventory().close();
                        }
                    } else {
                        player.sendMessage(ChatColor.DARK_GRAY + "\n \n * Adquira " + ChatColor.AQUA + "VIP" + ChatColor.DARK_GRAY + " e obtenha vantagem como estas em: \n  www.finalelite.com.br\n \n");
                    }
                }
            }

            if (event.getSlot() == 18) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsVips());
                    }
                }, 5l);
            }
        }

        if (event.getInventory().getName().equalsIgnoreCase("Kits VIP (Titan): ")) {
            event.setCancelled(true);

            if (event.getSlot() == 10) {
                if (event.isRightClick()) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitMensalTitan());
                        }
                    }, 5l);
                }
                if (event.isLeftClick()) {
                    if (TagAPI.getTag(UUID).equals("VIP++") || TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                        if (TagAPI.getTag(UUID).equals("VIP++")) {
                            if (KitsVipAPI.getCountdown(UUID + "MENSAL") <= 0) {
                                KitsVipAPI.updateCountdown(UUID + "MENSAL", 2678400);

                                player.sendMessage(ChatColor.GREEN + "\n \n * Kit mensal pego com sucesso!\n \n");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                player.getOpenInventory().close();

                                com.mixedup.managers.InventoryKits.giveKitMensalTitan(player);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Seu kit será liberado em " + AlternateColor.alternate("&c&n" + HorarioAPI.getTime(KitsVipAPI.getCountdown(UUID + "MENSAL"))) + ChatColor.RED + ".");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                            com.mixedup.managers.InventoryKits.giveKitMensalTitan(player);
                            player.sendMessage(ChatColor.GREEN + "\n \n * Kit mensal (Titan) pego com sucesso!\n \n");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                            player.getOpenInventory().close();
                        }
                    } else {
                        player.sendMessage(ChatColor.DARK_GRAY + "\n \n * Adquira " + ChatColor.AQUA + "VIP" + ChatColor.DARK_GRAY + " e obtenha vantagem como estas em: \n  www.finalelite.com.br\n \n");
                    }
                }
            }

            if (event.getSlot() == 12) {
                if (event.isRightClick()) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitSemanalTitan());
                        }
                    }, 5l);
                }
                if (event.isLeftClick()) {
                    if (TagAPI.getTag(UUID).equals("VIP++") || TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                        if (TagAPI.getTag(UUID).equals("VIP++")) {
                            if (KitsVipAPI.getCountdown(UUID + "SEMANAL") <= 0) {
                                KitsVipAPI.updateCountdown(UUID + "SEMANAL", 604800);

                                player.sendMessage(ChatColor.GREEN + "\n \n * Kit semanal pego com sucesso!\n \n");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                player.getOpenInventory().close();

                                com.mixedup.managers.InventoryKits.giveKitSemanalTitan(player);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Seu kit será liberado em " + AlternateColor.alternate("&c&n" + HorarioAPI.getTime(KitsVipAPI.getCountdown(UUID + "SEMANAL"))) + ChatColor.RED + ".");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                            com.mixedup.managers.InventoryKits.giveKitSemanalTitan(player);
                            player.sendMessage(ChatColor.GREEN + "\n \n * Kit semanal (Titan) pego com sucesso!\n \n");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                            player.getOpenInventory().close();
                        }
                    } else {
                        player.sendMessage(ChatColor.DARK_GRAY + "\n \n * Adquira " + ChatColor.AQUA + "VIP" + ChatColor.DARK_GRAY + " e obtenha vantagem como estas em: \n  www.finalelite.com.br\n \n");
                    }
                }
            }

            if (event.getSlot() == 16) {
                if (event.isRightClick()) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitDiarioTitan());
                        }
                    }, 5l);
                }
                if (event.isLeftClick()) {
                    if (TagAPI.getTag(UUID).equals("VIP++") || TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                        if (TagAPI.getTag(UUID).equals("VIP++")) {
                            if (KitsVipAPI.getCountdown(UUID + "DIARIO") <= 0) {
                                KitsVipAPI.updateCountdown(UUID + "DIARIO", 86400);

                                player.sendMessage(ChatColor.GREEN + "\n \n * Kit diário pego com sucesso!\n \n");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                player.getOpenInventory().close();

                                com.mixedup.managers.InventoryKits.giveKitDiarioTitan(player);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Seu kit será liberado em " + AlternateColor.alternate("&c&n" + HorarioAPI.getTime(KitsVipAPI.getCountdown(UUID + "DIARIO"))) + ChatColor.RED + ".");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                            com.mixedup.managers.InventoryKits.giveKitDiarioTitan(player);
                            player.sendMessage(ChatColor.GREEN + "\n \n * Kit diário (Titan) pego com sucesso!\n \n");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                            player.getOpenInventory().close();
                        }
                    } else {
                        player.sendMessage(ChatColor.DARK_GRAY + "\n \n * Adquira " + ChatColor.AQUA + "VIP" + ChatColor.DARK_GRAY + " e obtenha vantagem como estas em: \n  www.finalelite.com.br\n \n");
                    }
                }
            }

            if (event.getSlot() == 14) {
                if (event.isRightClick()) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mixedup.managers.InventoryKits.inventoryKit6HorasTitan());
                        }
                    }, 5l);
                }
                if (event.isLeftClick()) {
                    if (TagAPI.getTag(UUID).equals("VIP++") || TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                        if (TagAPI.getTag(UUID).equals("VIP++")) {
                            if (KitsVipAPI.getCountdown(UUID + "6HORASVIP") <= 0) {
                                KitsVipAPI.updateCountdown(UUID + "6HORASVIP", 21600);

                                player.sendMessage(ChatColor.GREEN + "\n \n * Kit 6 horas jogadas pego com sucesso!\n \n");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                player.getOpenInventory().close();

                                com.mixedup.managers.InventoryKits.giveKit6HorasTitan(player);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Seu kit será liberado em +" + AlternateColor.alternate("&c&n" + HorarioAPI.getTime(KitsVipAPI.getCountdown(UUID + "6HORASVIP") - 21600)) + ChatColor.RED + ".");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                            com.mixedup.managers.InventoryKits.giveKit6HorasTitan(player);
                            player.sendMessage(ChatColor.GREEN + "\n \n * Kit 6 horas jogadas (Titan) pego com sucesso!\n \n");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                            player.getOpenInventory().close();
                        }
                    } else {
                        player.sendMessage(ChatColor.DARK_GRAY + "\n \n * Adquira " + ChatColor.AQUA + "VIP" + ChatColor.DARK_GRAY + " e obtenha vantagem como estas em: \n  www.finalelite.com.br\n \n");
                    }
                }
            }

            if (event.getSlot() == 18) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsVips());
                    }
                }, 5l);
            }
        }

        if (event.getInventory().getName().equalsIgnoreCase("Kits VIP (Duque): ")) {
            event.setCancelled(true);

            if (event.getSlot() == 10) {
                if (event.isRightClick()) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitMensalDuque());
                        }
                    }, 5l);
                }
                if (event.isLeftClick()) {
                    if (TagAPI.getTag(UUID).equals("VIP+++") || TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                        if (TagAPI.getTag(UUID).equals("VIP+++")) {
                            if (KitsVipAPI.getCountdown(UUID + "MENSAL") <= 0) {
                                KitsVipAPI.updateCountdown(UUID + "MENSAL", 2678400);

                                player.sendMessage(ChatColor.GREEN + "\n \n * Kit mensal pego com sucesso!\n \n");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                player.getOpenInventory().close();

                                com.mixedup.managers.InventoryKits.giveKitMensalDuque(player);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Seu kit será liberado em " + AlternateColor.alternate("&c&n" + HorarioAPI.getTime(KitsVipAPI.getCountdown(UUID + "MENSAL"))) + ChatColor.RED + ".");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                            com.mixedup.managers.InventoryKits.giveKitMensalDuque(player);
                            player.sendMessage(ChatColor.GREEN + "\n \n * Kit mensal (Duque) pego com sucesso!\n \n");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                            player.getOpenInventory().close();
                        }
                    } else {
                        player.sendMessage(ChatColor.DARK_GRAY + "\n \n * Adquira " + ChatColor.AQUA + "VIP" + ChatColor.DARK_GRAY + " e obtenha vantagem como estas em: \n  www.finalelite.com.br\n \n");
                    }
                }
            }

            if (event.getSlot() == 12) {
                if (event.isRightClick()) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitSemanalDuque());
                        }
                    }, 5l);
                }
                if (event.isLeftClick()) {
                    if (TagAPI.getTag(UUID).equals("VIP+++") || TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                        if (TagAPI.getTag(UUID).equals("VIP+++")) {
                            if (KitsVipAPI.getCountdown(UUID + "SEMANAL") <= 0) {
                                KitsVipAPI.updateCountdown(UUID + "SEMANAL", 604800);

                                player.sendMessage(ChatColor.GREEN + "\n \n * Kit semanal pego com sucesso!\n \n");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                player.getOpenInventory().close();

                                com.mixedup.managers.InventoryKits.giveKitSemanalDuque(player);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Seu kit será liberado em " + AlternateColor.alternate("&c&n" + HorarioAPI.getTime(KitsVipAPI.getCountdown(UUID + "SEMANAL"))) + ChatColor.RED + ".");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                            com.mixedup.managers.InventoryKits.giveKitSemanalTitan(player);
                            player.sendMessage(ChatColor.GREEN + "\n \n * Kit semanal (Duque) pego com sucesso!\n \n");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                            player.getOpenInventory().close();
                        }
                    } else {
                        player.sendMessage(ChatColor.DARK_GRAY + "\n \n * Adquira " + ChatColor.AQUA + "VIP" + ChatColor.DARK_GRAY + " e obtenha vantagem como estas em: \n  www.finalelite.com.br\n \n");
                    }
                }
            }

            if (event.getSlot() == 16) {
                if (event.isRightClick()) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitDiarioDuque());
                        }
                    }, 5l);
                }
                if (event.isLeftClick()) {
                    if (TagAPI.getTag(UUID).equals("VIP+++") || TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                        if (TagAPI.getTag(UUID).equals("VIP+++")) {
                            if (KitsVipAPI.getCountdown(UUID + "DIARIO") <= 0) {
                                KitsVipAPI.updateCountdown(UUID + "DIARIO", 86400);

                                player.sendMessage(ChatColor.GREEN + "\n \n * Kit diário pego com sucesso!\n \n");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                player.getOpenInventory().close();

                                com.mixedup.managers.InventoryKits.giveKitDiarioDuque(player);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Seu kit será liberado em " + AlternateColor.alternate("&c&n" + HorarioAPI.getTime(KitsVipAPI.getCountdown(UUID + "DIARIO"))) + ChatColor.RED + ".");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                            com.mixedup.managers.InventoryKits.giveKitDiarioDuque(player);
                            player.sendMessage(ChatColor.GREEN + "\n \n * Kit diário (Duque) pego com sucesso!\n \n");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                            player.getOpenInventory().close();
                        }
                    } else {
                        player.sendMessage(ChatColor.DARK_GRAY + "\n \n * Adquira " + ChatColor.AQUA + "VIP" + ChatColor.DARK_GRAY + " e obtenha vantagem como estas em: \n  www.finalelite.com.br\n \n");
                    }
                }
            }

            if (event.getSlot() == 14) {
                if (event.isRightClick()) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mixedup.managers.InventoryKits.inventoryKit6HorasDuque());
                        }
                    }, 5l);
                }
                if (event.isLeftClick()) {
                    if (TagAPI.getTag(UUID).equals("VIP+++") || TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                        if (TagAPI.getTag(UUID).equals("VIP+++")) {
                            if (KitsVipAPI.getCountdown(UUID + "6HORASVIP") <= 0) {
                                KitsVipAPI.updateCountdown(UUID + "6HORASVIP", 21600);

                                player.sendMessage(ChatColor.GREEN + "\n \n * Kit 6 horas jogadas pego com sucesso!\n \n");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                player.getOpenInventory().close();

                                com.mixedup.managers.InventoryKits.giveKit6HorasDuque(player);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Seu kit será liberado em +" + AlternateColor.alternate("&c&n" + HorarioAPI.getTime(KitsVipAPI.getCountdown(UUID + "6HORASVIP") - 21600)) + ChatColor.RED + ".");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                            com.mixedup.managers.InventoryKits.giveKit6HorasDuque(player);
                            player.sendMessage(ChatColor.GREEN + "\n \n * Kit 6 horas jogadas (Duque) pego com sucesso!\n \n");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                            player.getOpenInventory().close();
                        }
                    } else {
                        player.sendMessage(ChatColor.DARK_GRAY + "\n \n * Adquira " + ChatColor.AQUA + "VIP" + ChatColor.DARK_GRAY + " e obtenha vantagem como estas em: \n  www.finalelite.com.br\n \n");
                    }
                }
            }

            if (event.getSlot() == 18) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsVips());
                    }
                }, 5l);
            }
        }
        //-----------

        //VOLTAR INVENTARIO ANTERIOR KIT SEMANAL DUQUE
        if (event.getInventory().getName().equalsIgnoreCase("Kit 6 horas jogadas (Duque): ")) {
            event.setCancelled(true);

            if (event.getSlot() == 18) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsVipDuque());
                    }
                }, 5l);
            }
        }
        //--

        //VOLTAR INVENTARIO ANTERIOR KIT SEMANAL TITAN
        if (event.getInventory().getName().equalsIgnoreCase("Kit 6 horas jogadas (Titan): ")) {
            event.setCancelled(true);

            if (event.getSlot() == 18) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsVipTitan());
                    }
                }, 5l);
            }
        }
        //--

        //VOLTAR INVENTARIO ANTERIOR KIT SEMANAL LORD
        if (event.getInventory().getName().equalsIgnoreCase("Kit 6 horas jogadas (Lord): ")) {
            event.setCancelled(true);

            if (event.getSlot() == 18) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsVipLord());
                    }
                }, 5l);
            }
        }
        //--

        //VOLTAR INVENTARIO ANTERIOR KIT 6 HORAS CONDE
        if (event.getInventory().getName().equalsIgnoreCase("Kit 6 horas jogadas (Conde): ")) {
            event.setCancelled(true);

            if (event.getSlot() == 18) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsVipConde());
                    }
                }, 5l);
            }
        }
        //--

        //VOLTAR INVENTARIO ANTERIOR KIT DIARIO DUQUE
        if (event.getInventory().getName().equalsIgnoreCase("Kit diário (Duque): ")) {
            event.setCancelled(true);

            if (event.getSlot() == 27) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsVipDuque());
                    }
                }, 5l);
            }
        }
        //--

        //VOLTAR INVENTARIO ANTERIOR KIT DIARIO TITAN
        if (event.getInventory().getName().equalsIgnoreCase("Kit diário (Titan): ")) {
            event.setCancelled(true);

            if (event.getSlot() == 27) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsVipTitan());
                    }
                }, 5l);
            }
        }
        //--

        //VOLTAR INVENTARIO ANTERIOR KIT DIARIO LORD
        if (event.getInventory().getName().equalsIgnoreCase("Kit diário (Lord): ")) {
            event.setCancelled(true);

            if (event.getSlot() == 27) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsVipLord());
                    }
                }, 5l);
            }
        }
        //--

        //VOLTAR INVENTARIO ANTERIOR KIT DIARIO CONDE
        if (event.getInventory().getName().equalsIgnoreCase("Kit diário (Conde): ")) {
            event.setCancelled(true);

            if (event.getSlot() == 27) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsVipConde());
                    }
                }, 5l);
            }
        }
        //--

        //VOLTAR INVENTARIO ANTERIOR KIT SEMANAL DUQUE
        if (event.getInventory().getName().equalsIgnoreCase("Kit semanal (Duque): ")) {
            event.setCancelled(true);

            if (event.getSlot() == 27) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsVipDuque());
                    }
                }, 5l);
            }
        }
        //--

        //VOLTAR INVENTARIO ANTERIOR KIT SEMANAL TITAN
        if (event.getInventory().getName().equalsIgnoreCase("Kit semanal (Titan): ")) {
            event.setCancelled(true);

            if (event.getSlot() == 27) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsVipTitan());
                    }
                }, 5l);
            }
        }
        //--

        //VOLTAR INVENTARIO ANTERIOR KIT SEMANAL LORD
        if (event.getInventory().getName().equalsIgnoreCase("Kit semanal (Lord): ")) {
            event.setCancelled(true);

            if (event.getSlot() == 27) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsVipLord());
                    }
                }, 5l);
            }
        }
        //--

        //VOLTAR INVENTARIO ANTERIOR KIT SEMANAL CONDE
        if (event.getInventory().getName().equalsIgnoreCase("Kit semanal (Conde): ")) {
            event.setCancelled(true);

            if (event.getSlot() == 27) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsVipConde());
                    }
                }, 5l);
            }
        }
        //--

        //VOLTAR INVENTARIO ANTERIOR KIT MENSAL TITAN
        if (event.getInventory().getName().equalsIgnoreCase("Kit mensal (Duque): ")) {
            event.setCancelled(true);

            if (event.getSlot() == 45) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (event.getClickedInventory().getItem(53).getType().equals(Material.ARROW)) {
                            player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsVipDuque());
                        } else if (event.getClickedInventory().getItem(53).getType().equals(Material.AIR)) {
                            player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitMensalDuque());
                        }
                    }
                }, 5l);
            }

            if (event.getSlot() == 18) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (!event.getCurrentItem().getType().equals(Material.AIR)) {
                            player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitMensalDuque());
                        }
                    }
                }, 5l);
            }

            if (event.getSlot() == 53) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (!event.getCurrentItem().getType().equals(Material.AIR)) {
                            player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitMensalDuque2());
                        }
                    }
                }, 5l);
            }
        }
        //--

        //VOLTAR INVENTARIO ANTERIOR KIT MENSAL TITAN
        if (event.getInventory().getName().equalsIgnoreCase("Kit mensal (Titan): ")) {
            event.setCancelled(true);

            if (event.getSlot() == 45) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsVipTitan());
                    }
                }, 5l);
            }
        }
        //--

        //VOLTAR INVENTARIO ANTERIOR KIT MENSAL LORD
        if (event.getInventory().getName().equalsIgnoreCase("Kit mensal (Lord): ")) {
            event.setCancelled(true);

            if (event.getSlot() == 45) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsVipLord());
                    }
                }, 5l);
            }
        }
        //--

        //VOLTAR INVENTARIO ANTERIOR KIT MENSAL CONDE
        if (event.getInventory().getName().equalsIgnoreCase("Kit mensal (Conde): ")) {
            event.setCancelled(true);

            if (event.getSlot() == 45) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsVipConde());
                    }
                }, 5l);
            }
        }
        //--

        if (event.getInventory().getName().equalsIgnoreCase("Kits básicos: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 12) {
                if (event.isRightClick()) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitBasico());
                        }
                    }, 5l);
                }
                if (event.isLeftClick()) {
                    if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                        com.mixedup.managers.InventoryKits.giveKitBasico(player);
                        player.sendMessage(ChatColor.GREEN + "\n \n * Kit básico pego com sucesso!\n \n");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                        player.getOpenInventory().close();
                        return;
                    }
                    if (KitsVipAPI.getCountdown(UUID + "BASICO") <= 0) {
                        KitsVipAPI.updateCountdown(UUID + "BASICO", 600);

                        player.sendMessage(ChatColor.GREEN + "\n \n * Kit básico pego com sucesso!\n \n");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                        player.getOpenInventory().close();

                        com.mixedup.managers.InventoryKits.giveKitBasico(player);
                    } else {
                        player.sendMessage(ChatColor.RED + " * Este kit será liberado em " + AlternateColor.alternate("&c&n" + HorarioAPI.getTime(KitsVipAPI.getCountdown(UUID))) + ChatColor.RED + ".");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                }
            }

            if (event.getSlot() == 14) {
                if (event.isRightClick()) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitBasico6Horas());
                        }
                    }, 5l);
                }
                if (event.isLeftClick()) {
                    if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                        com.mixedup.managers.InventoryKits.giveKit6HorasTitan(player);
                        player.sendMessage(ChatColor.GREEN + "\n \n * Kit 6 horas jogadas pego com sucesso!\n \n");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                        player.getOpenInventory().close();
                        return;
                    }
                    if (KitsVipAPI.getCountdown(UUID + "6HORAS") <= 0) {
                        KitsVipAPI.updateCountdown(UUID + "6HORAS", 21600);

                        player.sendMessage(ChatColor.GREEN + "\n \n * Kit 6 horas jogadas pego com sucesso!\n \n");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                        player.getOpenInventory().close();

                        com.mixedup.managers.InventoryKits.giveKit6HorasTitan(player);
                    } else {
                        player.sendMessage(ChatColor.RED + " * Seu kit será liberado em +" + AlternateColor.alternate("&c&n" + HorarioAPI.getTime(KitsVipAPI.getCountdown(UUID + "6HORAS") - 21600)) + ChatColor.RED + ".");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                }
            }

            if (event.getSlot() == 18) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.firstInvKits());
                    }
                }, 5l);
            }
        }

        //VOLTAR INVENTARIO ANTERIOR KIT BASICO 6 HORAS
        if (event.getInventory().

                getName().

                equalsIgnoreCase("Kit básico, 6 horas jogadas: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 18) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsBasics());
                    }
                }, 5l);
            }
        }
        //--

        //VOLTAR INVENTARIO ANTERIOR KIT BASICO
        if (event.getInventory().

                getName().

                equalsIgnoreCase("Kit básico: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 27) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsBasics());
                    }
                }, 5l);
            }

            if (event.getSlot() == 18) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.firstInvKits());
                    }
                }, 5L);
            }
        }
        //--

        if (event.getInventory().getName().equalsIgnoreCase("Kits VIPS: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 10) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsVipConde());
                    }
                }, 5L);
            }

            if (event.getSlot() == 12) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsVipLord());
                    }
                }, 5L);
            }

            if (event.getSlot() == 14) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsVipTitan());
                    }
                }, 5L);
            }

            if (event.getSlot() == 16) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsVipDuque());
                    }
                }, 5L);
            }

            if (event.getSlot() == 18) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.firstInvKits());
                    }
                }, 5L);
            }
        }

        if (event.getInventory().

                getName().

                equalsIgnoreCase("Seleção classe: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 12) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsBasics());
                    }
                }, 5L);
            }

            if (event.getSlot() == 14) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(com.mixedup.managers.InventoryKits.inventoryKitsVips());
                    }
                }, 5L);
            }
        }
    }
}