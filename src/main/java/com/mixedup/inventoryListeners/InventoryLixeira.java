package com.mixedup.inventoryListeners;

import com.mixedup.apis.PlayerUUID;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class InventoryLixeira implements Listener {

    public static int getQuantityItens(final Inventory inventory) {
        int quantity = 0;
        for (int i = 0; i <= inventory.getSize(); i++) {
            if (i == 54) {
                break;
            }
            if (inventory.getItem(i) != null && !inventory.getItem(i).getType().equals(Material.AIR)) {
                quantity += inventory.getItem(i).getAmount();
            }
        }

        return quantity;
    }

    @EventHandler
    public void onInteractInventory(final InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getInventory().getName().equalsIgnoreCase("Lixeira: ")) {
            if (event.getSlot() == 53) {
                event.setCancelled(true);

                int quantity = InventoryLixeira.getQuantityItens(event.getClickedInventory());

                int quantia = 0;
                while (quantity > 0) {
                    if (quantity >= 678) {
                        quantia++;
                        quantity -= 678;
                    } else if (quantity < 678) {
                        break;
                    }
                }
                final ItemStack frasco = new ItemStack(Material.EXPERIENCE_BOTTLE, quantia);

                if (quantia > 0) {
                    final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(frasco);
                    for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                        player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                    }
                }

                player.sendMessage(ChatColor.GREEN + " * Itens deletados!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.getOpenInventory().close();
            }
        }
    }
}
