package com.mixedup.inventoryListeners;

import com.mixedup.Main;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.hashs.GuildaSelectProv;
import net.minecraft.server.v1_13_R2.IChatBaseComponent;
import net.minecraft.server.v1_13_R2.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_13_R2.PacketPlayOutChat;
import net.minecraft.server.v1_13_R2.PlayerConnection;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryGuildaSelect implements Listener {
    @EventHandler
    public void onInteract(final InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();

        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getInventory().getName().equalsIgnoreCase("Selecione sua Guilda: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 11) {
                final PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
                for (int i = 0; i < 100; i++) {
                    player.sendMessage(" ");
                }
                final PacketPlayOutChat msg1 = new PacketPlayOutChat(ChatSerializer.a("{\"text\":\"\n \n      * Você deseja entrar para a Guilda sanguinária?\",\"color\":\"yellow\"}"));
                final PacketPlayOutChat msg2 = new PacketPlayOutChat(ChatSerializer.a("{\"text\":\"           Esta é uma escolha única, não poderá mudar\",\"color\":\"yellow\"}"));
                final PacketPlayOutChat msg3 = new PacketPlayOutChat(ChatSerializer.a("{\"text\":\"                           lá futuramente.\",\"color\":\"yellow\"}"));
                final PacketPlayOutChat msg4 = new PacketPlayOutChat(ChatSerializer.a("{\"text\":\"                             [Confirmar]\n \n \",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/guildasanguinariaconfirmar\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"CLIQUE PARA CONFIRMAR.\",\"color\":\"green\"}]}}}"));
                connection.sendPacket(msg1);
                connection.sendPacket(msg2);
                connection.sendPacket(msg3);
                connection.sendPacket(msg4);

                if (GuildaSelectProv.get(UUID) == null) {
                    new GuildaSelectProv(UUID, true).insert();
                } else {
                    GuildaSelectProv.get(UUID).setStatus(true);
                }

                player.getOpenInventory().close();

                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        GuildaSelectProv.get(UUID).setStatus(false);
                    }
                }, 300L);
            }

            if (event.getSlot() == 13) {
                final PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
                for (int i = 0; i < 100; i++) {
                    player.sendMessage(" ");
                }
                final PacketPlayOutChat msg1 = new PacketPlayOutChat(ChatSerializer.a("{\"text\":\"\n \n            * Você deseja entrar para a Guilda anciã?\",\"color\":\"yellow\"}"));
                final PacketPlayOutChat msg2 = new PacketPlayOutChat(ChatSerializer.a("{\"text\":\"           Esta é uma escolha única, não poderá mudar\",\"color\":\"yellow\"}"));
                final PacketPlayOutChat msg3 = new PacketPlayOutChat(ChatSerializer.a("{\"text\":\"                           lá futuramente.\",\"color\":\"yellow\"}"));
                final PacketPlayOutChat msg4 = new PacketPlayOutChat(ChatSerializer.a("{\"text\":\"                             [Confirmar]\n \n \",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/guildaanciaconfirmar\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"CLIQUE PARA CONFIRMAR.\",\"color\":\"green\"}]}}}"));
                connection.sendPacket(msg1);
                connection.sendPacket(msg2);
                connection.sendPacket(msg3);
                connection.sendPacket(msg4);

                if (GuildaSelectProv.get(UUID) == null) {
                    new GuildaSelectProv(UUID, true).insert();
                } else {
                    GuildaSelectProv.get(UUID).setStatus(true);
                }

                player.getOpenInventory().close();

                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        GuildaSelectProv.get(UUID).setStatus(false);
                    }
                }, 300L);
            }

            if (event.getSlot() == 15) {
                final PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
                for (int i = 0; i < 100; i++) {
                    player.sendMessage(" ");
                }
                final PacketPlayOutChat msg1 = new PacketPlayOutChat(ChatSerializer.a("{\"text\":\"\n \n           * Você deseja entrar para a Guilda nobre?\",\"color\":\"yellow\"}"));
                final PacketPlayOutChat msg2 = new PacketPlayOutChat(ChatSerializer.a("{\"text\":\"           Esta é uma escolha única, não poderá mudar\",\"color\":\"yellow\"}"));
                final PacketPlayOutChat msg3 = new PacketPlayOutChat(ChatSerializer.a("{\"text\":\"                           lá futuramente.\",\"color\":\"yellow\"}"));
                final PacketPlayOutChat msg4 = new PacketPlayOutChat(ChatSerializer.a("{\"text\":\"                             [Confirmar]\n \n \",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/guildanobreconfirmar\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"CLIQUE PARA CONFIRMAR.\",\"color\":\"green\"}]}}}"));
                connection.sendPacket(msg1);
                connection.sendPacket(msg2);
                connection.sendPacket(msg3);
                connection.sendPacket(msg4);

                player.getOpenInventory().close();

                if (GuildaSelectProv.get(UUID) == null) {
                    new GuildaSelectProv(UUID, true).insert();
                } else {
                    GuildaSelectProv.get(UUID).setStatus(true);
                }

                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        GuildaSelectProv.get(UUID).setStatus(false);
                    }
                }, 300L);
            }
        }
    }
}
