package com.mixedup.inventoryListeners;

import br.com.finalelite.pauloo27.api.message.MessageUtils;
import com.mixedup.Main;
import com.mixedup.apis.ConfigAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import com.mixedup.managers.InventoryConfig;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class InventoryConfigs implements Listener {

    @EventHandler
    public void onInteract(final InventoryClickEvent event) {

        Player player = (Player) event.getWhoClicked();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getInventory().getName().equalsIgnoreCase("Configurações: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 10) {
                if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                    ConfigAPI.updateChatLocal(UUID, false);
                    player.sendMessage(ChatColor.GREEN + " * Chat local desabilitado.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfig.buildInvConfig(UUID));
                        }
                    }, 5L);
                } else {
                    ConfigAPI.updateChatLocal(UUID, true);
                    player.sendMessage(ChatColor.GREEN + " * Chat local habilitado.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfig.buildInvConfig(UUID));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 12) {
                if (ConfigAPI.getChatGlobalStatus(UUID) == true) {
                    ConfigAPI.updateChatGlobal(UUID, false);
                    player.sendMessage(ChatColor.GREEN + " * Chat global desabilitado.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfig.buildInvConfig(UUID));
                        }
                    }, 5L);
                } else {
                    ConfigAPI.updateChatGlobal(UUID, true);
                    player.sendMessage(ChatColor.GREEN + " * Chat global habilitado.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfig.buildInvConfig(UUID));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 14) {
                if (ConfigAPI.getTellStatus(UUID) == true) {
                    ConfigAPI.updateTell(UUID, false);
                    player.sendMessage(ChatColor.GREEN + " * Mensagens privadas desabilitado.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfig.buildInvConfig(UUID));
                        }
                    }, 5L);
                } else {
                    ConfigAPI.updateTell(UUID, true);
                    player.sendMessage(ChatColor.GREEN + " * Mensagens privadas habilitado.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfig.buildInvConfig(UUID));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 16) {

                if (TagAPI.getTag(UUID).equals("Membro")) {

                    player.sendMessage(ChatColor.GRAY + " * Adquira vantagens como estas adquirindo " + ChatColor.GREEN + "VIP" + ChatColor.GRAY + " em nosso site." + "\n" + ChatColor.YELLOW + " * www.finalelite.com.br");
                    return;

                }

                if (ConfigAPI.getFlyStatus(UUID) == true) {
                    ConfigAPI.updateFly(UUID, false);
                    player.sendMessage(ChatColor.GREEN + " * Fly desabilitado.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);
                    player.setAllowFlight(false);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfig.buildInvConfig(UUID));
                        }
                    }, 5L);
                } else {
                    if (player.getLocation().getWorld().getName().equalsIgnoreCase("spawn")) {
                        ConfigAPI.updateFly(UUID, true);
                        player.sendMessage(ChatColor.GREEN + " * Fly habilitado.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);
                        player.setAllowFlight(true);

                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                player.openInventory(InventoryConfig.buildInvConfig(UUID));
                            }
                        }, 5L);
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, voar é apenas permito dentro do spawn/area-vip!");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
            }

            if (event.getSlot() == 29) {
                if (ConfigAPI.getTpaStatus(UUID) == true) {
                    ConfigAPI.updateTpa(UUID, false);
                    player.sendMessage(ChatColor.GREEN + " * Tpa desabilitado.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfig.buildInvConfig(UUID));
                        }
                    }, 5L);
                } else {
                    ConfigAPI.updateTpa(UUID, true);
                    player.sendMessage(ChatColor.GREEN + " * Tpa habilitado.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfig.buildInvConfig(UUID));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 31) {
                if (ConfigAPI.getDepositosStatus(UUID) == true) {
                    ConfigAPI.updateDepositos(UUID, false);
                    player.sendMessage(ChatColor.GREEN + " * Depósitos desabilitado.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfig.buildInvConfig(UUID));
                        }
                    }, 5L);
                } else {
                    ConfigAPI.updateDepositos(UUID, true);
                    player.sendMessage(ChatColor.GREEN + " * Depósitos habilitado.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfig.buildInvConfig(UUID));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 33) {
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);
                final val result = Main.scoreboardManager.toggleScoreboardToPlayer(player);
                MessageUtils.sendColouredMessage(player, "&aScoreboard " + (result ? "ativada" : "desativada") + ".");
                final ItemStack scoreboardOff = new ItemStack(Material.PAINTING);
                final ItemMeta metaScoreboardOff = scoreboardOff.getItemMeta();
                metaScoreboardOff.setDisplayName(ChatColor.RED + " * Scoreboard");
                final ArrayList<String> loreSb = new ArrayList<String>();
                loreSb.add(" ");
                loreSb.add(ChatColor.DARK_GRAY + " * Desabilitado!");
                loreSb.add(" ");
                metaScoreboardOff.setLore(loreSb);
                scoreboardOff.setItemMeta(metaScoreboardOff);

                final ItemStack scoreabordOn = new ItemStack(Material.ITEM_FRAME);
                final ItemMeta metaScoreabordOn = scoreabordOn.getItemMeta();
                metaScoreabordOn.setDisplayName(ChatColor.GREEN + " * Scoreboard");
                final ArrayList<String> loreSbOn = new ArrayList<String>();
                loreSbOn.add(" ");
                loreSbOn.add(ChatColor.DARK_GRAY + " * Habilitado!");
                loreSbOn.add(" ");
                metaScoreabordOn.setLore(loreSbOn);
                scoreabordOn.setItemMeta(metaScoreabordOn);

                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventoryConfig.buildInvConfig(UUID));
                    }
                }, 5L);
            }
        }
    }
}
