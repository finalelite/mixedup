package com.mixedup.inventoryListeners;

import com.mixedup.Main;
import com.mixedup.apis.GuildaAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.economy.InventoryLoja;
import com.mixedup.managers.InventoryTrident;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class inventoryLoja implements Listener {

    public static boolean playerInArea(final Location loc) {
        final String[] pos1 = "spawn:140:50:106".split(":");
        final String[] pos2 = "spawn:78:31:44".split(":");

        if (!loc.getWorld().getName().equalsIgnoreCase(pos1[0])) return false;

        final int p1x = Integer.valueOf(pos1[1]);
        final int p1y = Integer.valueOf(pos1[2]);
        final int p1z = Integer.valueOf(pos1[3]);
        final int p2x = Integer.valueOf(pos2[1]);
        final int p2y = Integer.valueOf(pos2[2]);
        final int p2z = Integer.valueOf(pos2[3]);

        final int minX = p1x < p2x ? p1x : p2x;
        final int minY = p1y < p2y ? p1y : p2y;
        final int minZ = p1z < p2z ? p1z : p2z;

        final int maxX = p1x > p2x ? p1x : p2x;
        final int maxY = p1y > p2y ? p1y : p2y;
        final int maxZ = p1z > p2z ? p1z : p2z;

        return (loc.getBlockX() >= minX) && (loc.getBlockX() <= maxX) && (loc.getBlockY() >= minY)
                && (loc.getBlockY() <= maxY) && (loc.getBlockZ() >= minZ) && (loc.getBlockZ() <= maxZ);
    }

    @EventHandler
    public void onInteractInventory(final InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getInventory().getName().equalsIgnoreCase("Loja virtual: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 10) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (inventoryLoja.playerInArea(player.getLocation()) == true) {
                            player.openInventory(InventoryLoja.buildLojaBlocos1("vip"));
                        } else {
                            player.openInventory(InventoryLoja.buildLojaBlocos1(GuildaAPI.getGuilda(UUID)));
                        }
                    }
                }, 5L);
            }

            if (event.getSlot() == 11) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (inventoryLoja.playerInArea(player.getLocation()) == true) {
                            player.openInventory(InventoryLoja.buildLojaDecorativos1("vip"));
                        } else {
                            player.openInventory(InventoryLoja.buildLojaDecorativos1(GuildaAPI.getGuilda(UUID)));
                        }
                    }
                }, 5L);
            }

            if (event.getSlot() == 12) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (inventoryLoja.playerInArea(player.getLocation()) == true) {
                            player.openInventory(InventoryLoja.buildLojaTnts("vip"));
                        } else {
                            player.openInventory(InventoryLoja.buildLojaTnts(GuildaAPI.getGuilda(UUID)));
                        }
                    }
                }, 5L);
            }

            if (event.getSlot() == 13) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (inventoryLoja.playerInArea(player.getLocation()) == true) {
                            player.openInventory(InventoryLoja.buildLojaMateriais1("vip"));
                        } else {
                            player.openInventory(InventoryLoja.buildLojaMateriais1(GuildaAPI.getGuilda(UUID)));
                        }
                    }
                }, 5L);
            }

            if (event.getSlot() == 14) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (inventoryLoja.playerInArea(player.getLocation()) == true) {
                            player.openInventory(InventoryLoja.buildLojaComidas("vip"));
                        } else {
                            player.openInventory(InventoryLoja.buildLojaComidas(GuildaAPI.getGuilda(UUID)));
                        }
                    }
                }, 5L);
            }

            if (event.getSlot() == 15) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (inventoryLoja.playerInArea(player.getLocation()) == true) {
                            player.openInventory(InventoryLoja.buildLojaRedstone("vip"));
                        } else {
                            player.openInventory(InventoryLoja.buildLojaRedstone(GuildaAPI.getGuilda(UUID)));
                        }
                    }
                }, 5L);
            }

            if (event.getSlot() == 16) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (inventoryLoja.playerInArea(player.getLocation()) == true) {
                            player.openInventory(InventoryLoja.buildLojaAlquimia("vip"));
                        } else {
                            player.openInventory(InventoryLoja.buildLojaAlquimia(GuildaAPI.getGuilda(UUID)));
                        }
                    }
                }, 5L);
            }

            if (event.getSlot() == 19) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (inventoryLoja.playerInArea(player.getLocation()) == true) {
                            player.openInventory(InventoryLoja.buildLojaVarios("vip"));
                        } else {
                            player.openInventory(InventoryLoja.buildLojaVarios(GuildaAPI.getGuilda(UUID)));
                        }
                    }
                }, 5L);
            }

            if (event.getSlot() == 20) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventoryLoja.buildLojaColoridos());
                    }
                }, 5L);
            }

            if (event.getSlot() == 21) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (inventoryLoja.playerInArea(player.getLocation()) == true) {
                            player.openInventory(InventoryTrident.inv("vip"));
                        } else {
                            player.openInventory(InventoryTrident.inv(GuildaAPI.getGuilda(UUID)));
                        }
                    }
                }, 5L);
            }

            if (event.getSlot() == 22) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (inventoryLoja.playerInArea(player.getLocation()) == true) {
                            player.openInventory(InventoryLoja.buildLojaSpawners("vip"));
                        } else {
                            player.openInventory(InventoryLoja.buildLojaSpawners(GuildaAPI.getGuilda(UUID)));
                        }
                    }
                }, 5L);
            }

            if (event.getSlot() == 23) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (inventoryLoja.playerInArea(player.getLocation()) == true) {
                            player.openInventory(InventoryLoja.buildLojaCaixas("vip"));
                        } else {
                            player.openInventory(InventoryLoja.buildLojaCaixas(GuildaAPI.getGuilda(UUID)));
                        }
                    }
                }, 5L);
            }

            if (event.getSlot() == 24) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (inventoryLoja.playerInArea(player.getLocation()) == true) {
                            player.openInventory(InventoryLoja.buildLojaPainelSolar("vip"));
                        } else {
                            player.openInventory(InventoryLoja.buildLojaPainelSolar(GuildaAPI.getGuilda(UUID)));
                        }
                    }
                }, 5L);
            }

            if (event.getSlot() == 31) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.getOpenInventory().close();
                    }
                }, 5L);
            }
        }
    }
}
