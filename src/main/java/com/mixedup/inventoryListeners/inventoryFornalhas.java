package com.mixedup.inventoryListeners;

import com.mixedup.Main;
import com.mixedup.apis.EnergiaAPI;
import com.mixedup.apis.FurnaceAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import com.mixedup.economy.CoinsAPI;
import com.mixedup.managers.InventoryFornalhas;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftInventoryFurnace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;

import java.util.Iterator;

public class inventoryFornalhas implements Listener {

    @EventHandler
    public void onLeft(final InventoryCloseEvent event) {
        if (event.getInventory().getName().equalsIgnoreCase("Fornalha privada")) {
            if (event.getInventory().getItem(0) != null && !event.getInventory().getItem(0).getType().equals(Material.AIR)) {
                event.getPlayer().getInventory().addItem(event.getInventory().getItem(0));
            }
            if (event.getInventory().getItem(2) != null && !event.getInventory().getItem(2).getType().equals(Material.AIR)) {
                event.getPlayer().getInventory().addItem(event.getInventory().getItem(2));
            }
        }
    }

    @EventHandler
    public void onInteract(final InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getClickedInventory() != null && event.getInventory().getName().equalsIgnoreCase("Fornalha privada") && event.getClickedInventory().getType().equals(InventoryType.FURNACE)) {
            if (event.getSlot() == 1) {
                event.setCancelled(true);
            }

            if (event.getSlot() == 0) {
                final CraftInventoryFurnace furnace = (CraftInventoryFurnace) event.getClickedInventory();
                final Iterator<Recipe> recipes = Bukkit.recipeIterator();

                final double energia = EnergiaAPI.getEnergia(UUID);
                if (!event.getCursor().getType().equals(Material.AIR)) {
                    if (energia < 0.009) {
                        player.sendMessage(ChatColor.RED + " * Ops, você não contém energia para isto!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        return;
                    }
                }

                while (recipes.hasNext()) {
                    final Recipe next = recipes.next();
                    if (!(next instanceof FurnaceRecipe)) continue;
                    final FurnaceRecipe recipe = (FurnaceRecipe) next;

                    if (recipe.getInput().getType().equals(event.getCursor().getType())) {
                        final ItemStack item = recipe.getResult();
                        item.setAmount(event.getCursor().getAmount());

                        event.getCursor().setType(Material.AIR);
                        event.getCursor().setAmount(0);

                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                if (furnace.getResult() == null) {
                                    EnergiaAPI.updateEnergia(UUID, energia - 0.009);
                                    Main.scoreboardManager.getDefaultScoreboard().updateEntry(player, "power");
                                    player.openInventory(InventoryFornalhas.furnace(item));
                                    player.playSound(player.getLocation(), Sound.BLOCK_FURNACE_FIRE_CRACKLE, 1.0f, 1.0f);
                                }
                            }
                        }, 5L);
                    }
                }
            }
        }

        if (event.getClickedInventory() != null && event.getInventory().getName().equalsIgnoreCase("Fornalha virtual: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 13) {
                if (FurnaceAPI.has(UUID) == false) {
                    String tag = TagAPI.getTag(UUID);
                    if (!tag.equalsIgnoreCase("Vip") && !tag.equalsIgnoreCase("Vip+") && !tag.equalsIgnoreCase("Vip++") && !tag.equalsIgnoreCase("Vip+++")) {
                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                player.openInventory(InventoryFornalhas.inventoryConfirm());
                            }
                        }, 5L);
                    } else {
                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                player.openInventory(InventoryFornalhas.furnace(null));
                            }
                        }, 5L);
                    }
                } else {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryFornalhas.furnace(null));
                        }
                    }, 5L);
                }
            }
        }

        if (event.getClickedInventory() != null && event.getInventory().getName().equalsIgnoreCase("Confirmar compra fornalha: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 12) {
                final int coins = CoinsAPI.getCoins(UUID);

                if (coins < 30000) {
                    player.getOpenInventory().close();
                    player.sendMessage(ChatColor.RED + " * Ops, você não contém dinheiro para isto!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                } else {
                    CoinsAPI.updateCoins(UUID, coins - 30000);
                    FurnaceAPI.createFurnace(UUID);
                    player.sendMessage(ChatColor.GREEN + " * Fornalha virtual adquirida!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryFornalhas.inventory(UUID));
                        }
                    }, 5l);
                }
            }

            if (event.getSlot() == 14) {
                player.getOpenInventory().close();
                player.sendMessage(ChatColor.RED + " * Evento cancelado!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
    }
}
