package com.mixedup.factions;

public enum FacRole {
    RECRUIT,
    MEMBER,
    CAPTAIN,
    LEADER
}
