package com.mixedup.factions.listeners;

import com.mixedup.Main;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.factions.hashs.PlayerInRadiation;
import com.mixedup.factions.hashs.RadiationRgs;
import org.bukkit.Bukkit;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerMoveOnRadiation implements Listener {

    @EventHandler
    public void onMove(final PlayerMoveEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (player.getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
            if (RadiationRgs.playerInArea(player.getLocation()) != null) {
                if (PlayerInRadiation.get(UUID) == null) {
                    final int rotate = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.damage(1);
                            player.spawnParticle(Particle.CLOUD, player.getLocation(), 10);
                        }
                    }, 0L, 60L);

                    new PlayerInRadiation(UUID, true, rotate).insert();
                } else {
                    if (PlayerInRadiation.get(UUID).getZone() == false) {
                        final int rotate = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                player.damage(1);
                                player.spawnParticle(Particle.CLOUD, player.getLocation(), 10);
                            }
                        }, 0L, 60L);

                        PlayerInRadiation.get(UUID).setZone(true);
                        PlayerInRadiation.get(UUID).setRegionID(rotate);
                    }
                }
            } else {
                if (PlayerInRadiation.get(UUID) != null && PlayerInRadiation.get(UUID).getZone() == true) {
                    Bukkit.getScheduler().cancelTask(PlayerInRadiation.get(UUID).getRegionID());
                    PlayerInRadiation.get(UUID).setZone(false);
                    PlayerInRadiation.get(UUID).setRegionID(0);
                }
            }
        } else {
            if (PlayerInRadiation.get(UUID) != null && PlayerInRadiation.get(UUID).getZone() == true) {
                Bukkit.getScheduler().cancelTask(PlayerInRadiation.get(UUID).getRegionID());
                PlayerInRadiation.get(UUID).setZone(false);
                PlayerInRadiation.get(UUID).setRegionID(0);
            }
        }
    }
}
