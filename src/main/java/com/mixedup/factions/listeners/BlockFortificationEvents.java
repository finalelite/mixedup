package com.mixedup.factions.listeners;

import com.mixedup.Main;
import com.mixedup.MySql;
import com.mixedup.apis.AccountAPI;
import com.mixedup.commands.CommandTeste;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.FortificationAPI;
import com.mixedup.factions.hashs.*;
import com.mixedup.utils.RemoveItemInv;
import lombok.val;
import org.bukkit.*;
import org.bukkit.block.*;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.Map.Entry;

public class BlockFortificationEvents implements Listener {

    private static void a(final Block block) {
        if (block.getRelative(BlockFace.NORTH).getType().equals(Material.TNT)) {
            if (ActiveUtil.get(block.getRelative(BlockFace.NORTH).getX() + ":" + block.getRelative(BlockFace.NORTH).getY() + ":" + block.getRelative(BlockFace.NORTH).getZ()) == null) {
                new ActiveUtil(block.getRelative(BlockFace.NORTH).getX() + ":" + block.getRelative(BlockFace.NORTH).getY() + ":" + block.getRelative(BlockFace.NORTH).getZ(), true).insert();
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        ActiveUtil.get(block.getRelative(BlockFace.NORTH).getX() + ":" + block.getRelative(BlockFace.NORTH).getY() + ":" + block.getRelative(BlockFace.NORTH).getZ()).setActivated(false);
                    }
                }, 102L);
            } else {
                if (ActiveUtil.get(block.getRelative(BlockFace.NORTH).getX() + ":" + block.getRelative(BlockFace.NORTH).getY() + ":" + block.getRelative(BlockFace.NORTH).getZ()).getActivated() == true) {
                    return;
                } else {
                    ActiveUtil.get(block.getRelative(BlockFace.NORTH).getX() + ":" + block.getRelative(BlockFace.NORTH).getY() + ":" + block.getRelative(BlockFace.NORTH).getZ()).setActivated(true);
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            ActiveUtil.get(block.getRelative(BlockFace.NORTH).getX() + ":" + block.getRelative(BlockFace.NORTH).getY() + ":" + block.getRelative(BlockFace.NORTH).getZ()).setActivated(false);
                        }
                    }, 102L);
                }
            }
            String owner = null;
            if (block.hasMetadata("tnt-owner")) {
                owner = block.getMetadata("tnt-owner").get(0).asString();
            }
            block.getRelative(BlockFace.NORTH).setType(Material.AIR);
            final Random random = new Random();
            final String r = String.valueOf(random.nextInt(10000000));

            final String loc = block.getRelative(BlockFace.NORTH).getX() + ":" + block.getRelative(BlockFace.NORTH).getY() + ":" + block.getRelative(BlockFace.NORTH).getZ();
            if (FortificationAPI.getTNTLevel(loc) != 0) {
                if (TntLevelUtil.get(r) == null) {
                    new TntLevelUtil(r, FortificationAPI.getTNTLevel(loc)).insert();
                } else {
                    TntLevelUtil.get(r).setLevel(FortificationAPI.getTNTLevel(loc));
                }
                FortificationAPI.removeTNTInfo(loc);
            } else {
                return;
            }

            final Entity tnt = block.getRelative(BlockFace.NORTH).getWorld().spawn(block.getRelative(BlockFace.NORTH).getLocation().add(0, 1, 0), TNTPrimed.class);
            ((TNTPrimed) tnt).setFuseTicks(90);
            tnt.setCustomName(r);
            if (owner != null) {
                tnt.setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), owner));
            }
            tnt.setCustomNameVisible(false);
        } else if (block.getRelative(BlockFace.SOUTH).getType().equals(Material.TNT)) {
            if (ActiveUtil.get(block.getRelative(BlockFace.SOUTH).getX() + ":" + block.getRelative(BlockFace.SOUTH).getY() + ":" + block.getRelative(BlockFace.SOUTH).getZ()) == null) {
                new ActiveUtil(block.getRelative(BlockFace.SOUTH).getX() + ":" + block.getRelative(BlockFace.SOUTH).getY() + ":" + block.getRelative(BlockFace.SOUTH).getZ(), true).insert();
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        ActiveUtil.get(block.getRelative(BlockFace.SOUTH).getX() + ":" + block.getRelative(BlockFace.SOUTH).getY() + ":" + block.getRelative(BlockFace.SOUTH).getZ()).setActivated(false);
                    }
                }, 102L);
            } else {
                if (ActiveUtil.get(block.getRelative(BlockFace.SOUTH).getX() + ":" + block.getRelative(BlockFace.SOUTH).getY() + ":" + block.getRelative(BlockFace.SOUTH).getZ()).getActivated() == true) {
                    return;
                } else {
                    ActiveUtil.get(block.getRelative(BlockFace.SOUTH).getX() + ":" + block.getRelative(BlockFace.SOUTH).getY() + ":" + block.getRelative(BlockFace.SOUTH).getZ()).setActivated(true);
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            ActiveUtil.get(block.getRelative(BlockFace.SOUTH).getX() + ":" + block.getRelative(BlockFace.SOUTH).getY() + ":" + block.getRelative(BlockFace.SOUTH).getZ()).setActivated(false);
                        }
                    }, 102L);
                }
            }
            String owner = null;
            if (block.hasMetadata("tnt-owner")) {
                owner = block.getMetadata("tnt-owner").get(0).asString();
            }
            block.getRelative(BlockFace.SOUTH).setType(Material.AIR);
            final Random random = new Random();
            final String r = String.valueOf(random.nextInt(10000000));

            final String loc = block.getRelative(BlockFace.SOUTH).getX() + ":" + block.getRelative(BlockFace.SOUTH).getY() + ":" + block.getRelative(BlockFace.SOUTH).getZ();
            if (FortificationAPI.getTNTLevel(loc) != 0) {
                if (TntLevelUtil.get(r) == null) {
                    new TntLevelUtil(r, FortificationAPI.getTNTLevel(loc)).insert();
                } else {
                    TntLevelUtil.get(r).setLevel(FortificationAPI.getTNTLevel(loc));
                }
                FortificationAPI.removeTNTInfo(loc);
            } else {
                return;
            }

            final Entity tnt = block.getRelative(BlockFace.SOUTH).getWorld().spawn(block.getRelative(BlockFace.SOUTH).getLocation().add(0, 1, 0), TNTPrimed.class);
            ((TNTPrimed) tnt).setFuseTicks(90);
            tnt.setCustomName(r);
            if (owner != null) {
                tnt.setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), owner));
            }
            tnt.setCustomNameVisible(false);
        } else if (block.getRelative(BlockFace.WEST).getType().equals(Material.TNT)) {
            if (ActiveUtil.get(block.getRelative(BlockFace.WEST).getX() + ":" + block.getRelative(BlockFace.WEST).getY() + ":" + block.getRelative(BlockFace.WEST).getZ()) == null) {
                new ActiveUtil(block.getRelative(BlockFace.WEST).getX() + ":" + block.getRelative(BlockFace.WEST).getY() + ":" + block.getRelative(BlockFace.WEST).getZ(), true).insert();
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        ActiveUtil.get(block.getRelative(BlockFace.WEST).getX() + ":" + block.getRelative(BlockFace.WEST).getY() + ":" + block.getRelative(BlockFace.WEST).getZ()).setActivated(false);
                    }
                }, 102L);
            } else {
                if (ActiveUtil.get(block.getRelative(BlockFace.WEST).getX() + ":" + block.getRelative(BlockFace.WEST).getY() + ":" + block.getRelative(BlockFace.WEST).getZ()).getActivated() == true) {
                    return;
                } else {
                    ActiveUtil.get(block.getRelative(BlockFace.WEST).getX() + ":" + block.getRelative(BlockFace.WEST).getY() + ":" + block.getRelative(BlockFace.WEST).getZ()).setActivated(true);
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            ActiveUtil.get(block.getRelative(BlockFace.WEST).getX() + ":" + block.getRelative(BlockFace.WEST).getY() + ":" + block.getRelative(BlockFace.WEST).getZ()).setActivated(false);
                        }
                    }, 102L);
                }
            }
            String owner = null;
            if (block.hasMetadata("tnt-owner")) {
                owner = block.getMetadata("tnt-owner").get(0).asString();
            }
            block.getRelative(BlockFace.WEST).setType(Material.AIR);
            final Random random = new Random();
            final String r = String.valueOf(random.nextInt(10000000));

            final String loc = block.getRelative(BlockFace.WEST).getX() + ":" + block.getRelative(BlockFace.WEST).getY() + ":" + block.getRelative(BlockFace.WEST).getZ();
            if (FortificationAPI.getTNTLevel(loc) != 0) {
                if (TntLevelUtil.get(r) == null) {
                    new TntLevelUtil(r, FortificationAPI.getTNTLevel(loc)).insert();
                } else {
                    TntLevelUtil.get(r).setLevel(FortificationAPI.getTNTLevel(loc));
                }
                FortificationAPI.removeTNTInfo(loc);
            } else {
                return;
            }

            final Entity tnt = block.getRelative(BlockFace.WEST).getWorld().spawn(block.getRelative(BlockFace.WEST).getLocation().add(0, 1, 0), TNTPrimed.class);
            ((TNTPrimed) tnt).setFuseTicks(90);
            tnt.setCustomName(r);
            if (owner != null) {
                tnt.setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), owner));
            }
            tnt.setCustomNameVisible(false);
        } else if (block.getRelative(BlockFace.EAST).getType().equals(Material.TNT)) {
            if (ActiveUtil.get(block.getRelative(BlockFace.EAST).getX() + ":" + block.getRelative(BlockFace.EAST).getY() + ":" + block.getRelative(BlockFace.EAST).getZ()) == null) {
                new ActiveUtil(block.getRelative(BlockFace.EAST).getX() + ":" + block.getRelative(BlockFace.EAST).getY() + ":" + block.getRelative(BlockFace.EAST).getZ(), true).insert();
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        ActiveUtil.get(block.getRelative(BlockFace.EAST).getX() + ":" + block.getRelative(BlockFace.EAST).getY() + ":" + block.getRelative(BlockFace.EAST).getZ()).setActivated(false);
                    }
                }, 102L);
            } else {
                if (ActiveUtil.get(block.getRelative(BlockFace.EAST).getX() + ":" + block.getRelative(BlockFace.EAST).getY() + ":" + block.getRelative(BlockFace.EAST).getZ()).getActivated() == true) {
                    return;
                } else {
                    ActiveUtil.get(block.getRelative(BlockFace.EAST).getX() + ":" + block.getRelative(BlockFace.EAST).getY() + ":" + block.getRelative(BlockFace.EAST).getZ()).setActivated(true);
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            ActiveUtil.get(block.getRelative(BlockFace.EAST).getX() + ":" + block.getRelative(BlockFace.EAST).getY() + ":" + block.getRelative(BlockFace.EAST).getZ()).setActivated(false);
                        }
                    }, 102L);
                }
            }
            String owner = null;
            if (block.hasMetadata("tnt-owner")) {
                owner = block.getMetadata("tnt-owner").get(0).asString();
            }
            block.getRelative(BlockFace.EAST).setType(Material.AIR);
            final Random random = new Random();
            final String r = String.valueOf(random.nextInt(10000000));

            final String loc = block.getRelative(BlockFace.EAST).getX() + ":" + block.getRelative(BlockFace.EAST).getY() + ":" + block.getRelative(BlockFace.EAST).getZ();
            if (FortificationAPI.getTNTLevel(loc) != 0) {
                if (TntLevelUtil.get(r) == null) {
                    new TntLevelUtil(r, FortificationAPI.getTNTLevel(loc)).insert();
                } else {
                    TntLevelUtil.get(r).setLevel(FortificationAPI.getTNTLevel(loc));
                }
                FortificationAPI.removeTNTInfo(loc);
            } else {
                return;
            }

            final Entity tnt = block.getRelative(BlockFace.EAST).getWorld().spawn(block.getRelative(BlockFace.EAST).getLocation().add(0, 1, 0), TNTPrimed.class);
            ((TNTPrimed) tnt).setFuseTicks(90);
            tnt.setCustomName(r);
            if (owner != null) {
                tnt.setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), owner));
            }
            tnt.setCustomNameVisible(false);
        } else if (block.getRelative(BlockFace.UP).getType().equals(Material.TNT)) {
            if (ActiveUtil.get(block.getRelative(BlockFace.UP).getX() + ":" + block.getRelative(BlockFace.UP).getY() + ":" + block.getRelative(BlockFace.UP).getZ()) == null) {
                new ActiveUtil(block.getRelative(BlockFace.UP).getX() + ":" + block.getRelative(BlockFace.UP).getY() + ":" + block.getRelative(BlockFace.UP).getZ(), true).insert();
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        ActiveUtil.get(block.getRelative(BlockFace.UP).getX() + ":" + block.getRelative(BlockFace.UP).getY() + ":" + block.getRelative(BlockFace.UP).getZ()).setActivated(false);
                    }
                }, 102L);
            } else {
                if (ActiveUtil.get(block.getRelative(BlockFace.UP).getX() + ":" + block.getRelative(BlockFace.UP).getY() + ":" + block.getRelative(BlockFace.UP).getZ()).getActivated() == true) {
                    return;
                } else {
                    ActiveUtil.get(block.getRelative(BlockFace.UP).getX() + ":" + block.getRelative(BlockFace.UP).getY() + ":" + block.getRelative(BlockFace.UP).getZ()).setActivated(true);
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            ActiveUtil.get(block.getRelative(BlockFace.UP).getX() + ":" + block.getRelative(BlockFace.UP).getY() + ":" + block.getRelative(BlockFace.UP).getZ()).setActivated(false);
                        }
                    }, 102L);
                }
            }
            String owner = null;
            if (block.hasMetadata("tnt-owner")) {
                owner = block.getMetadata("tnt-owner").get(0).asString();
            }
            block.getRelative(BlockFace.UP).setType(Material.AIR);
            final Random random = new Random();
            final String r = String.valueOf(random.nextInt(10000000));

            final String loc = block.getRelative(BlockFace.UP).getX() + ":" + block.getRelative(BlockFace.UP).getY() + ":" + block.getRelative(BlockFace.UP).getZ();
            if (FortificationAPI.getTNTLevel(loc) != 0) {
                if (TntLevelUtil.get(r) == null) {
                    new TntLevelUtil(r, FortificationAPI.getTNTLevel(loc)).insert();
                } else {
                    TntLevelUtil.get(r).setLevel(FortificationAPI.getTNTLevel(loc));
                }
                FortificationAPI.removeTNTInfo(loc);
            } else {
                return;
            }

            final Entity tnt = block.getRelative(BlockFace.UP).getWorld().spawn(block.getRelative(BlockFace.UP).getLocation().add(0, 1, 0), TNTPrimed.class);
            ((TNTPrimed) tnt).setFuseTicks(90);
            tnt.setCustomName(r);
            if (owner != null) {
                tnt.setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), owner));
            }
            tnt.setCustomNameVisible(false);
        } else if (block.getRelative(BlockFace.DOWN).getType().equals(Material.TNT)) {
            if (ActiveUtil.get(block.getRelative(BlockFace.DOWN).getX() + ":" + block.getRelative(BlockFace.DOWN).getY() + ":" + block.getRelative(BlockFace.DOWN).getZ()) == null) {
                new ActiveUtil(block.getRelative(BlockFace.DOWN).getX() + ":" + block.getRelative(BlockFace.DOWN).getY() + ":" + block.getRelative(BlockFace.DOWN).getZ(), true).insert();
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        ActiveUtil.get(block.getRelative(BlockFace.DOWN).getX() + ":" + block.getRelative(BlockFace.DOWN).getY() + ":" + block.getRelative(BlockFace.DOWN).getZ()).setActivated(false);
                    }
                }, 102L);
            } else {
                if (ActiveUtil.get(block.getRelative(BlockFace.DOWN).getX() + ":" + block.getRelative(BlockFace.DOWN).getY() + ":" + block.getRelative(BlockFace.DOWN).getZ()).getActivated() == true) {
                    return;
                } else {
                    ActiveUtil.get(block.getRelative(BlockFace.DOWN).getX() + ":" + block.getRelative(BlockFace.DOWN).getY() + ":" + block.getRelative(BlockFace.DOWN).getZ()).setActivated(true);
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            ActiveUtil.get(block.getRelative(BlockFace.DOWN).getX() + ":" + block.getRelative(BlockFace.DOWN).getY() + ":" + block.getRelative(BlockFace.DOWN).getZ()).setActivated(false);
                        }
                    }, 102L);
                }
            }
            String owner = null;
            if (block.hasMetadata("tnt-owner")) {
                owner = block.getMetadata("tnt-owner").get(0).asString();
            }
            block.getRelative(BlockFace.DOWN).setType(Material.AIR);
            final Random random = new Random();
            final String r = String.valueOf(random.nextInt(10000000));

            final String loc = block.getRelative(BlockFace.DOWN).getX() + ":" + block.getRelative(BlockFace.DOWN).getY() + ":" + block.getRelative(BlockFace.DOWN).getZ();
            if (FortificationAPI.getTNTLevel(loc) != 0) {
                if (TntLevelUtil.get(r) == null) {
                    new TntLevelUtil(r, FortificationAPI.getTNTLevel(loc)).insert();
                } else {
                    TntLevelUtil.get(r).setLevel(FortificationAPI.getTNTLevel(loc));
                }
                FortificationAPI.removeTNTInfo(loc);
            } else {
                return;
            }

            final Entity tnt = block.getRelative(BlockFace.DOWN).getWorld().spawn(block.getRelative(BlockFace.DOWN).getLocation().add(0, 1, 0), TNTPrimed.class);
            ((TNTPrimed) tnt).setFuseTicks(90);
            tnt.setCustomName(r);
            if (owner != null) {
                tnt.setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), owner));
            }
            tnt.setCustomNameVisible(false);
        }
    }

    public static void updateBossLife(final String teamOne, final String teamTwo, final double nexusLife) {
        final double a = nexusLife / 100;
        if (LifeNexusCache.get(teamOne) == null) {
            final String t1 = FacAPI.getRecrutas(teamOne) + ":" + FacAPI.getMembros(teamOne) + ":" + FacAPI.getCapitoes(teamOne) + ":" + FacAPI.getLideres(teamOne);
            for (final Player player : Bukkit.getOnlinePlayers()) {
                if (t1.contains(player.getUniqueId().toString())) {
                    if (nexusLife >= 63) {
                        if (LifeNexusCache.get(teamOne) == null) {
                            final BossBar b = Bukkit.createBossBar(ChatColor.GREEN + "NEXUS", BarColor.GREEN, BarStyle.SEGMENTED_20);
                            b.addPlayer(player);
                            b.setVisible(true);
                            b.setProgress(a);

                            new LifeNexusCache(teamOne, b).insert();
                        } else {
                            LifeNexusCache.get(teamOne).getBossBar().addPlayer(player);
                        }
                    } else if (nexusLife < 63 && nexusLife >= 33) {
                        if (LifeNexusCache.get(teamOne) == null) {
                            final BossBar b = Bukkit.createBossBar(ChatColor.YELLOW + "NEXUS", BarColor.YELLOW, BarStyle.SEGMENTED_20);
                            b.addPlayer(player);
                            b.setVisible(true);
                            b.setProgress(a);

                            new LifeNexusCache(teamOne, b).insert();
                        } else {
                            LifeNexusCache.get(teamOne).getBossBar().addPlayer(player);
                        }
                    } else if (nexusLife < 33 && nexusLife >= 1) {
                        if (LifeNexusCache.get(teamOne) == null) {
                            final BossBar b = Bukkit.createBossBar(ChatColor.RED + "NEXUS", BarColor.RED, BarStyle.SEGMENTED_20);
                            b.addPlayer(player);
                            b.setVisible(true);
                            b.setProgress(a);

                            new LifeNexusCache(teamOne, b).insert();
                        } else {
                            LifeNexusCache.get(teamOne).getBossBar().addPlayer(player);
                        }
                    } else if (nexusLife <= 0) {
                        if (LifeNexusCache.get(teamOne) != null) {
                            LifeNexusCache.get(teamOne).getBossBar().removeAll();
                        }
                    }
                }
            }
        } else {
            final String t1 = FacAPI.getRecrutas(teamOne) + ":" + FacAPI.getMembros(teamOne) + ":" + FacAPI.getCapitoes(teamOne) + ":" + FacAPI.getLideres(teamOne);
            for (final Player player : Bukkit.getOnlinePlayers()) {
                if (t1.contains(player.getUniqueId().toString())) {
                    if (nexusLife >= 63) {
                        LifeNexusCache.get(teamOne).getBossBar().setTitle(ChatColor.GREEN + "NEXUS");
                        LifeNexusCache.get(teamOne).getBossBar().setColor(BarColor.GREEN);
                        LifeNexusCache.get(teamOne).getBossBar().setProgress(a);
                    } else if (nexusLife < 63 && nexusLife >= 33) {
                        LifeNexusCache.get(teamOne).getBossBar().setTitle(ChatColor.YELLOW + "NEXUS");
                        LifeNexusCache.get(teamOne).getBossBar().setColor(BarColor.YELLOW);
                        LifeNexusCache.get(teamOne).getBossBar().setProgress(a);
                    } else if (nexusLife < 33 && nexusLife >= 1) {
                        LifeNexusCache.get(teamOne).getBossBar().setTitle(ChatColor.RED + "NEXUS");
                        LifeNexusCache.get(teamOne).getBossBar().setColor(BarColor.RED);
                        LifeNexusCache.get(teamOne).getBossBar().setProgress(a);
                    } else if (nexusLife <= 0) {
                        LifeNexusCache.get(teamOne).getBossBar().removeAll();
                    }
                }
            }
        }
        //-
        if (LifeNexusCache.get(teamTwo) == null) {
            final String t1 = FacAPI.getRecrutas(teamTwo) + ":" + FacAPI.getMembros(teamTwo) + ":" + FacAPI.getCapitoes(teamTwo) + ":" + FacAPI.getLideres(teamTwo);
            for (final Player player : Bukkit.getOnlinePlayers()) {
                if (t1.contains(player.getUniqueId().toString())) {
                    if (nexusLife >= 63) {
                        if (LifeNexusCache.get(teamTwo) == null) {
                            final BossBar b = Bukkit.createBossBar(ChatColor.GREEN + "NEXUS", BarColor.GREEN, BarStyle.SEGMENTED_20);
                            b.addPlayer(player);
                            b.setVisible(true);
                            b.setProgress(a);

                            new LifeNexusCache(teamTwo, b).insert();
                        } else {
                            LifeNexusCache.get(teamTwo).getBossBar().addPlayer(player);
                        }
                    } else if (nexusLife < 63 && nexusLife >= 33) {
                        if (LifeNexusCache.get(teamTwo) == null) {
                            final BossBar b = Bukkit.createBossBar(ChatColor.YELLOW + "NEXUS", BarColor.YELLOW, BarStyle.SEGMENTED_20);
                            b.addPlayer(player);
                            b.setVisible(true);
                            b.setProgress(a);

                            new LifeNexusCache(teamTwo, b).insert();
                        } else {
                            LifeNexusCache.get(teamTwo).getBossBar().addPlayer(player);
                        }
                    } else if (nexusLife < 33 && nexusLife >= 1) {
                        if (LifeNexusCache.get(teamTwo) == null) {
                            final BossBar b = Bukkit.createBossBar(ChatColor.RED + "NEXUS", BarColor.RED, BarStyle.SEGMENTED_20);
                            b.addPlayer(player);
                            b.setVisible(true);
                            b.setProgress(a);

                            new LifeNexusCache(teamTwo, b).insert();
                        } else {
                            LifeNexusCache.get(teamTwo).getBossBar().addPlayer(player);
                        }
                    } else if (nexusLife <= 0) {
                        if (LifeNexusCache.get(teamTwo) != null) {
                            LifeNexusCache.get(teamTwo).getBossBar().removeAll();
                        }
                    }
                }
            }
        } else {
            final String t1 = FacAPI.getRecrutas(teamTwo) + ":" + FacAPI.getMembros(teamTwo) + ":" + FacAPI.getCapitoes(teamTwo) + ":" + FacAPI.getLideres(teamTwo);
            for (final Player player : Bukkit.getOnlinePlayers()) {
                if (t1.contains(player.getUniqueId().toString())) {
                    if (nexusLife >= 63) {
                        LifeNexusCache.get(teamTwo).getBossBar().setTitle(ChatColor.GREEN + "NEXUS");
                        LifeNexusCache.get(teamTwo).getBossBar().setColor(BarColor.GREEN);
                        LifeNexusCache.get(teamTwo).getBossBar().setProgress(a);
                    } else if (nexusLife < 63 && nexusLife >= 33) {
                        LifeNexusCache.get(teamTwo).getBossBar().setTitle(ChatColor.YELLOW + "NEXUS");
                        LifeNexusCache.get(teamTwo).getBossBar().setColor(BarColor.YELLOW);
                        LifeNexusCache.get(teamTwo).getBossBar().setProgress(a);
                    } else if (nexusLife < 33 && nexusLife >= 1) {
                        LifeNexusCache.get(teamTwo).getBossBar().setTitle(ChatColor.RED + "NEXUS");
                        LifeNexusCache.get(teamTwo).getBossBar().setColor(BarColor.RED);
                        LifeNexusCache.get(teamTwo).getBossBar().setProgress(a);
                    } else if (nexusLife <= 0) {
                        LifeNexusCache.get(teamTwo).getBossBar().removeAll();
                    }
                }
            }
        }
    }

    public static List<Location> generateSphere(final Location centerBlock, final int radius, final boolean hollow) {
        if (centerBlock == null) {
            return new ArrayList<>();
        }
        final List<Location> circleBlocks = new ArrayList<Location>();

        final int bx = centerBlock.getBlockX();
        final int by = centerBlock.getBlockY();
        final int bz = centerBlock.getBlockZ();

        for (int x = bx - radius; x <= bx + radius; x++) {
            for (int y = by - radius; y <= by + radius; y++) {
                for (int z = bz - radius; z <= bz + radius; z++) {
                    final double distance = ((bx - x) * (bx - x) + ((bz - z) * (bz - z)) + ((by - y) * (by - y)));

                    if (distance < radius * radius && !(hollow && distance < ((radius - 1) * (radius - 1)))) {
                        final Location l = new Location(centerBlock.getWorld(), x, y, z);

                        circleBlocks.add(l);

                    }

                }
            }
        }
        return circleBlocks;
    }

    public static void updateProtection() {
        final Thread thread = new Thread(() -> {
            while (true) {
                try {
                    Thread.currentThread().sleep(1000 * 300);
                } catch (final Exception e) {
                    e.printStackTrace();
                }

                try {
                    final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Fortification_data");
                    final ResultSet rs = st.executeQuery();
                    while (rs.next()) {
                        final String[] split = rs.getString("Location").split(":");
                        final Location loc = new Location(Bukkit.getWorld("Trappist-1b"), Integer.valueOf(split[0]), Integer.valueOf(split[1]), Integer.valueOf(split[2]));

                        if (loc.getBlock().getType().equals(Material.END_STONE)) {
                            final PreparedStatement st2 = MySql.con.prepareStatement("SELECT * FROM Fortification_data WHERE Location = ?");
                            st2.setString(1, rs.getString("Location"));
                            final ResultSet rs2 = st2.executeQuery();
                            while (rs2.next()) {
                                if (rs2.getInt("Life") < 4) {
                                    final PreparedStatement st3 = MySql.con.prepareStatement("UPDATE Fortification_data SET Life = Life + 1 WHERE Location = ?");
                                    st3.setString(1, rs.getString("Location"));
                                    st3.executeUpdate();
                                }
                            }
                        } else if (loc.getBlock().getType().equals(Material.SMOOTH_STONE)) {
                            final PreparedStatement st2 = MySql.con.prepareStatement("SELECT * FROM Fortification_data WHERE Location = ?");
                            st2.setString(1, rs.getString("Location"));
                            final ResultSet rs2 = st2.executeQuery();
                            while (rs2.next()) {
                                if (rs2.getInt("Life") < 10) {
                                    final PreparedStatement st3 = MySql.con.prepareStatement("UPDATE Fortification_data SET Life = Life + 1 WHERE Location = ?");
                                    st3.setString(1, rs.getString("Location"));
                                    st3.executeUpdate();
                                }
                            }
                        }
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();

        //Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
        //            @Override
        //            public void run() {
        //                try {
        //                    PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Fortification_data");
        //                    ResultSet rs = st.executeQuery();
        //                    while (rs.next()) {
        //                        String[] split = rs.getString("Location").split(":");
        //                        Location loc = new Location(Bukkit.getWorld("Trappist-1b"), Integer.valueOf(split[0]), Integer.valueOf(split[1]), Integer.valueOf(split[2]));
        //
        //                        if (loc.getBlock().getType().equals(Material.END_STONE)) {
        //                            PreparedStatement st2 = MySql.con.prepareStatement("SELECT * FROM Fortification_data WHERE Location = ?");
        //                            st2.setString(1, rs.getString("Location"));
        //                            ResultSet rs2 = st2.executeQuery();
        //                            while (rs2.next()) {
        //                                if (rs2.getInt("Life") < 4) {
        //                                    PreparedStatement st3 = MySql.con.prepareStatement("UPDATE Fortification_data SET Life = Life + 1 WHERE Location = ?");
        //                                    st3.setString(1, rs.getString("Location"));
        //                                    st3.executeUpdate();
        //                                }
        //                            }
        //                        } else if (loc.getBlock().getType().equals(Material.SMOOTH_STONE)) {
        //                            PreparedStatement st2 = MySql.con.prepareStatement("SELECT * FROM Fortification_data WHERE Location = ?");
        //                            st2.setString(1, rs.getString("Location"));
        //                            ResultSet rs2 = st2.executeQuery();
        //                            while (rs2.next()) {
        //                                if (rs2.getInt("Life") < 10) {
        //                                    PreparedStatement st3 = MySql.con.prepareStatement("UPDATE Fortification_data SET Life = Life + 1 WHERE Location = ?");
        //                                    st3.setString(1, rs.getString("Location"));
        //                                    st3.executeUpdate();
        //                                }
        //                            }
        //                        }
        //                    }
        //                } catch (SQLException e) {
        //                    e.printStackTrace();
        //                }
        //            }
        //        }, 0L, 6000L);
    }

    private static void attack(final Location loc, final boolean notific, final String facname, final String facname2, final boolean destroy) {
        final String chunk = loc.getChunk().getX() + ":" + loc.getChunk().getZ();
        if (FacAPI.getChunkOwn(chunk) != null) {
            if (FacAPI.getSobAttack(FacAPI.getChunkOwn(chunk)) == false) {
                FacAPI.updateSobAttack(FacAPI.getChunkOwn(chunk), true);

                if (TimeAttachHash.get(FacAPI.getChunkOwn(chunk)) == null) {
                    new TimeAttachHash(FacAPI.getChunkOwn(chunk), 12000L).insert();

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            FacAPI.updateSobAttack(FacAPI.getChunkOwn(chunk), false);
                            TimeAttachHash.get(FacAPI.getChunkOwn(chunk)).setTime(0L);

                            if (LifeNexusCache.get(facname) != null) {
                                LifeNexusCache.get(facname).getBossBar().removeAll();
                                LifeNexusCache.cache.remove(facname);
                            }
                            if (LifeNexusCache.get(facname2) != null) {
                                LifeNexusCache.get(facname2).getBossBar().removeAll();
                                LifeNexusCache.cache.remove(facname2);
                            }
                        }
                    }, TimeAttachHash.get(FacAPI.getChunkOwn(chunk)).getTime());
                } else {
                    if (TimeAttachHash.get(FacAPI.getChunkOwn(chunk)).getTime() != 0L) {
                        TimeAttachHash.get(FacAPI.getChunkOwn(chunk)).setTime(12000L);
                    } else {
                        TimeAttachHash.get(FacAPI.getChunkOwn(chunk)).setTime(12000L);

                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                FacAPI.updateSobAttack(FacAPI.getChunkOwn(chunk), false);
                                TimeAttachHash.get(FacAPI.getChunkOwn(chunk)).setTime(0L);

                                if (LifeNexusCache.get(facname) != null) {
                                    LifeNexusCache.get(facname).getBossBar().removeAll();
                                    LifeNexusCache.cache.remove(facname);
                                }
                                if (LifeNexusCache.get(facname2) != null) {
                                    LifeNexusCache.get(facname2).getBossBar().removeAll();
                                    LifeNexusCache.cache.remove(facname2);
                                }
                            }
                        }, TimeAttachHash.get(FacAPI.getChunkOwn(chunk)).getTime());
                    }
                }
            }

            if (destroy == true) {
                final String nome = facname;
                String lista = null;
                if (!FacAPI.getRecrutas(nome).equals("NULL")) {
                    lista = FacAPI.getRecrutas(nome);
                }
                if (!FacAPI.getMembros(nome).equals("NULL")) {
                    if (lista == null) {
                        lista = FacAPI.getMembros(nome);
                    } else {
                        lista = lista + ":" + FacAPI.getMembros(nome);
                    }
                }
                if (!FacAPI.getCapitoes(nome).equals("NULL")) {
                    if (lista == null) {
                        lista = FacAPI.getCapitoes(nome);
                    } else {
                        lista = lista + ":" + FacAPI.getCapitoes(nome);
                    }
                }
                if (!FacAPI.getLideres(nome).equals("NULL")) {
                    if (lista == null) {
                        lista = FacAPI.getLideres(nome);
                    } else {
                        lista = lista + ":" + FacAPI.getLideres(nome);
                    }
                }

                if (lista.contains(":")) {
                    final String[] list = lista.split(":");

                    for (int i = 1; i <= list.length; i++) {
                        final Player target = Bukkit.getPlayerExact(AccountAPI.getNick(list[i - 1]));

                        if (target != null && target.isOnline()) {
                            target.sendTitle(ChatColor.RED + "NEXUS", ChatColor.RED + "DESTRUIDO!", 20, 60, 20);
                            target.playSound(target.getLocation(), Sound.BLOCK_CONDUIT_DEACTIVATE, 1.0f, 1.0f);
                            final val sb = Main.scoreboardManager;
                            if (sb.hasSquadScoreboard(target)) {
                                sb.updateSquadScoreboardTitle(target);
                            }
                        }
                    }
                } else {
                    final Player target = Bukkit.getPlayerExact(AccountAPI.getNick(lista));

                    if (target != null && target.isOnline()) {
                        target.sendTitle(ChatColor.RED + "NEXUS", ChatColor.RED + "DESTRUIDO!", 20, 60, 20);
                        target.playSound(target.getLocation(), Sound.BLOCK_CONDUIT_DEACTIVATE, 1.0f, 1.0f);
                        final val sb = Main.scoreboardManager;
                        if (sb.hasSquadScoreboard(target)) {
                            sb.updateSquadScoreboardTitle(target);
                        }
                    }
                }
                return;
            }

            if (notific == true) {
                final String nome = facname;
                String lista = null;
                if (!FacAPI.getRecrutas(nome).equals("NULL")) {
                    lista = FacAPI.getRecrutas(nome);
                }
                if (!FacAPI.getMembros(nome).equals("NULL")) {
                    if (lista == null) {
                        lista = FacAPI.getMembros(nome);
                    } else {
                        lista = lista + ":" + FacAPI.getMembros(nome);
                    }
                }
                if (!FacAPI.getCapitoes(nome).equals("NULL")) {
                    if (lista == null) {
                        lista = FacAPI.getCapitoes(nome);
                    } else {
                        lista = lista + ":" + FacAPI.getCapitoes(nome);
                    }
                }
                if (!FacAPI.getLideres(nome).equals("NULL")) {
                    if (lista == null) {
                        lista = FacAPI.getLideres(nome);
                    } else {
                        lista = lista + ":" + FacAPI.getLideres(nome);
                    }
                }

                if (lista.contains(":")) {
                    final String[] list = lista.split(":");

                    for (int i = 1; i <= list.length; i++) {
                        final Player target = Bukkit.getPlayerExact(AccountAPI.getNick(list[i - 1]));

                        if (target != null && target.isOnline()) {
                            target.sendTitle(ChatColor.RED + "Sob ataque", ChatColor.RED + "(!)", 20, 60, 20);
                            target.playSound(target.getLocation(), Sound.BLOCK_CONDUIT_ATTACK_TARGET, 1.0f, 1.0f);
                            final val sb = Main.scoreboardManager;
                            if (sb.hasSquadScoreboard(target)) {
                                sb.updateSquadScoreboardTitle(target);
                            }
                        }
                    }
                } else {
                    final Player target = Bukkit.getPlayerExact(AccountAPI.getNick(lista));

                    if (target != null && target.isOnline()) {
                        target.sendTitle(ChatColor.RED + "Sob ataque", ChatColor.RED + "(!)", 20, 60, 20);
                        target.playSound(target.getLocation(), Sound.BLOCK_CONDUIT_ATTACK_TARGET, 1.0f, 1.0f);
                        final val sb = Main.scoreboardManager;
                        if (sb.hasSquadScoreboard(target)) {
                            sb.updateSquadScoreboardTitle(target);
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void waterMoveDown(final BlockFromToEvent event) {
        if (event.getBlock().getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
            final Block block = event.getBlock();
            if (block.isLiquid()) {
                if (event.getToBlock().getLocation().getBlockY() < block.getLocation().getBlockY()) {
                    if (block.getRelative(BlockFace.UP).isLiquid() && block.getType().equals(block.getRelative(BlockFace.UP).getType())) {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onBreakFortification(final BlockBreakEvent event) {
        final String loc = event.getBlock().getX() + ":" + event.getBlock().getY() + ":" + event.getBlock().getZ();
        if (event.getBlock().getType().equals(Material.END_STONE) || event.getBlock().getType().equals(Material.SMOOTH_STONE)) {
            if (FortificationAPI.getLife(loc, event.getBlock().getType()) == 0) return;
            if (event.isCancelled()) return;
            if (event.getBlock().getType().equals(Material.END_STONE)) {
                if (FortificationAPI.getLife(loc, event.getBlock().getType()) > 0) {
                    event.setCancelled(true);
                    FortificationAPI.removeFortification(loc);
                    event.getBlock().setType(Material.AIR);

                    final ItemStack end = new ItemStack(Material.END_STONE);
                    final ItemMeta meta6 = end.getItemMeta();
                    meta6.setDisplayName(ChatColor.GREEN + "Bloco de fortificação");
                    final List<String> lore5 = new ArrayList<>();
                    lore5.add(" ");
                    lore5.add(ChatColor.GRAY + "Defesa: " + ChatColor.RED + "4/10");
                    lore5.add(" ");
                    meta6.setLore(lore5);
                    end.setItemMeta(meta6);

                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), end);
                }
            } else if (event.getBlock().getType().equals(Material.SMOOTH_STONE)) {
                if (FortificationAPI.getLife(loc, event.getBlock().getType()) > 0) {
                    event.setCancelled(true);
                    FortificationAPI.removeFortification(loc);
                    event.getBlock().setType(Material.AIR);

                    final ItemStack bedrock = new ItemStack(Material.SMOOTH_STONE);
                    final ItemMeta meta7 = bedrock.getItemMeta();
                    meta7.setDisplayName(ChatColor.GREEN + "Bloco de fortificação");
                    final List<String> lore7 = new ArrayList<>();
                    lore7.add(" ");
                    lore7.add(ChatColor.GRAY + "Defesa: " + ChatColor.GREEN + "10/10");
                    lore7.add(" ");
                    meta7.setLore(lore7);
                    bedrock.setItemMeta(meta7);

                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), bedrock);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void igniteTnt(final BlockRedstoneEvent event) {
        if (event.getBlock().getRelative(BlockFace.EAST).isBlockPowered() || event.getBlock().getRelative(BlockFace.EAST).isBlockIndirectlyPowered()) {
            final Block block = event.getBlock().getRelative(BlockFace.EAST);
            a(block);
        }
        if (event.getBlock().getRelative(BlockFace.NORTH).isBlockPowered() || event.getBlock().getRelative(BlockFace.NORTH).isBlockIndirectlyPowered()) {
            final Block block = event.getBlock().getRelative(BlockFace.NORTH);
            a(block);
        }
        if (event.getBlock().getRelative(BlockFace.SOUTH).isBlockPowered() || event.getBlock().getRelative(BlockFace.SOUTH).isBlockIndirectlyPowered()) {
            final Block block = event.getBlock().getRelative(BlockFace.SOUTH);
            a(block);
        }
        if (event.getBlock().getRelative(BlockFace.WEST).isBlockPowered() || event.getBlock().getRelative(BlockFace.WEST).isBlockIndirectlyPowered()) {
            final Block block = event.getBlock().getRelative(BlockFace.WEST);
            a(block);
        }
        if (event.getBlock().getRelative(BlockFace.UP).isBlockPowered() || event.getBlock().getRelative(BlockFace.UP).isBlockIndirectlyPowered()) {
            final Block block = event.getBlock().getRelative(BlockFace.UP);
            a(block);
        }
        if (event.getBlock().getRelative(BlockFace.DOWN).isBlockPowered() || event.getBlock().getRelative(BlockFace.DOWN).isBlockIndirectlyPowered()) {
            final Block block = event.getBlock().getRelative(BlockFace.DOWN);
            a(block);
        }

        if (event.getBlock().getRelative(BlockFace.NORTH).getType().equals(Material.TNT)) {
            if (ActiveUtil.get(event.getBlock().getRelative(BlockFace.NORTH).getX() + ":" + event.getBlock().getRelative(BlockFace.NORTH).getY() + ":" + event.getBlock().getRelative(BlockFace.NORTH).getZ()) == null) {
                new ActiveUtil(event.getBlock().getRelative(BlockFace.NORTH).getX() + ":" + event.getBlock().getRelative(BlockFace.NORTH).getY() + ":" + event.getBlock().getRelative(BlockFace.NORTH).getZ(), true).insert();
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        ActiveUtil.get(event.getBlock().getRelative(BlockFace.NORTH).getX() + ":" + event.getBlock().getRelative(BlockFace.NORTH).getY() + ":" + event.getBlock().getRelative(BlockFace.NORTH).getZ()).setActivated(false);
                    }
                }, 102L);
            } else {
                if (ActiveUtil.get(event.getBlock().getRelative(BlockFace.NORTH).getX() + ":" + event.getBlock().getRelative(BlockFace.NORTH).getY() + ":" + event.getBlock().getRelative(BlockFace.NORTH).getZ()).getActivated() == true) {
                    return;
                } else {
                    ActiveUtil.get(event.getBlock().getRelative(BlockFace.NORTH).getX() + ":" + event.getBlock().getRelative(BlockFace.NORTH).getY() + ":" + event.getBlock().getRelative(BlockFace.NORTH).getZ()).setActivated(true);
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            ActiveUtil.get(event.getBlock().getRelative(BlockFace.NORTH).getX() + ":" + event.getBlock().getRelative(BlockFace.NORTH).getY() + ":" + event.getBlock().getRelative(BlockFace.NORTH).getZ()).setActivated(false);
                        }
                    }, 102L);
                }
            }
            String owner = null;
            if (event.getBlock().hasMetadata("tnt-owner")) {
                owner = event.getBlock().getMetadata("tnt-owner").get(0).asString();
            }
            event.getBlock().getRelative(BlockFace.NORTH).setType(Material.AIR);
            final Random random = new Random();
            final String r = String.valueOf(random.nextInt(10000000));

            final String loc = event.getBlock().getRelative(BlockFace.NORTH).getX() + ":" + event.getBlock().getRelative(BlockFace.NORTH).getY() + ":" + event.getBlock().getRelative(BlockFace.NORTH).getZ();
            if (FortificationAPI.getTNTLevel(loc) != 0) {
                if (TntLevelUtil.get(r) == null) {
                    new TntLevelUtil(r, FortificationAPI.getTNTLevel(loc)).insert();
                } else {
                    TntLevelUtil.get(r).setLevel(FortificationAPI.getTNTLevel(loc));
                }
                FortificationAPI.removeTNTInfo(loc);
            } else {
                return;
            }

            Location tntSpawn = event.getBlock().getRelative(BlockFace.NORTH).getLocation().add(0, 1, 0);
                    
            if (event.getBlock().getData() == 8) {
                tntSpawn.setY(tntSpawn.getY() - 1.0D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }
            if (event.getBlock().getData() == 9) {
                tntSpawn.setY(tntSpawn.getY() + 1.0D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }
            if (event.getBlock().getData() == 10) {
                tntSpawn.setZ(tntSpawn.getZ() - 0.5D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
            }
            if (event.getBlock().getData() == 11) {
                tntSpawn.setZ(tntSpawn.getZ() + 1.5D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
            }
            if (event.getBlock().getData() == 12) {
                tntSpawn.setX(tntSpawn.getX() - 0.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }
            if (event.getBlock().getData() == 13) {
                tntSpawn.setX(tntSpawn.getX() + 1.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }
            
            final Entity tnt = event.getBlock().getRelative(BlockFace.NORTH).getWorld().spawn(tntSpawn, TNTPrimed.class);
            ((TNTPrimed) tnt).setFuseTicks(90);
            tnt.setCustomName(r);
            if (owner != null) {
                tnt.setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), owner));
            }
            tnt.setCustomNameVisible(false);
        } else if (event.getBlock().getRelative(BlockFace.SOUTH).getType().equals(Material.TNT)) {
            if (ActiveUtil.get(event.getBlock().getRelative(BlockFace.SOUTH).getX() + ":" + event.getBlock().getRelative(BlockFace.SOUTH).getY() + ":" + event.getBlock().getRelative(BlockFace.SOUTH).getZ()) == null) {
                new ActiveUtil(event.getBlock().getRelative(BlockFace.SOUTH).getX() + ":" + event.getBlock().getRelative(BlockFace.SOUTH).getY() + ":" + event.getBlock().getRelative(BlockFace.SOUTH).getZ(), true).insert();
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        ActiveUtil.get(event.getBlock().getRelative(BlockFace.SOUTH).getX() + ":" + event.getBlock().getRelative(BlockFace.SOUTH).getY() + ":" + event.getBlock().getRelative(BlockFace.SOUTH).getZ()).setActivated(false);
                    }
                }, 102L);
            } else {
                if (ActiveUtil.get(event.getBlock().getRelative(BlockFace.SOUTH).getX() + ":" + event.getBlock().getRelative(BlockFace.SOUTH).getY() + ":" + event.getBlock().getRelative(BlockFace.SOUTH).getZ()).getActivated() == true) {
                    return;
                } else {
                    ActiveUtil.get(event.getBlock().getRelative(BlockFace.SOUTH).getX() + ":" + event.getBlock().getRelative(BlockFace.SOUTH).getY() + ":" + event.getBlock().getRelative(BlockFace.SOUTH).getZ()).setActivated(true);
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            ActiveUtil.get(event.getBlock().getRelative(BlockFace.SOUTH).getX() + ":" + event.getBlock().getRelative(BlockFace.SOUTH).getY() + ":" + event.getBlock().getRelative(BlockFace.SOUTH).getZ()).setActivated(false);
                        }
                    }, 102L);
                }
            }
            String owner = null;
            if (event.getBlock().hasMetadata("tnt-owner")) {
                owner = event.getBlock().getMetadata("tnt-owner").get(0).asString();
            }
            event.getBlock().getRelative(BlockFace.SOUTH).setType(Material.AIR);
            final Random random = new Random();
            final String r = String.valueOf(random.nextInt(10000000));

            final String loc = event.getBlock().getRelative(BlockFace.SOUTH).getX() + ":" + event.getBlock().getRelative(BlockFace.SOUTH).getY() + ":" + event.getBlock().getRelative(BlockFace.SOUTH).getZ();
            if (FortificationAPI.getTNTLevel(loc) != 0) {
                if (TntLevelUtil.get(r) == null) {
                    new TntLevelUtil(r, FortificationAPI.getTNTLevel(loc)).insert();
                } else {
                    TntLevelUtil.get(r).setLevel(FortificationAPI.getTNTLevel(loc));
                }
                FortificationAPI.removeTNTInfo(loc);
            } else {
                return;
            }

            Location tntSpawn = event.getBlock().getRelative(BlockFace.SOUTH).getLocation().add(0, 1, 0);

            if (event.getBlock().getData() == 8) {
                tntSpawn.setY(tntSpawn.getY() - 1.0D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }
            if (event.getBlock().getData() == 9) {
                tntSpawn.setY(tntSpawn.getY() + 1.0D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }
            if (event.getBlock().getData() == 10) {
                tntSpawn.setZ(tntSpawn.getZ() - 0.5D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
            }
            if (event.getBlock().getData() == 11) {
                tntSpawn.setZ(tntSpawn.getZ() + 1.5D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
            }
            if (event.getBlock().getData() == 12) {
                tntSpawn.setX(tntSpawn.getX() - 0.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }
            if (event.getBlock().getData() == 13) {
                tntSpawn.setX(tntSpawn.getX() + 1.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }

            final Entity tnt = event.getBlock().getRelative(BlockFace.SOUTH).getWorld().spawn(tntSpawn, TNTPrimed.class);
            ((TNTPrimed) tnt).setFuseTicks(90);
            tnt.setCustomName(r);
            if (owner != null) {
                tnt.setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), owner));
            }
            tnt.setCustomNameVisible(false);
        } else if (event.getBlock().getRelative(BlockFace.WEST).getType().equals(Material.TNT)) {
            if (ActiveUtil.get(event.getBlock().getRelative(BlockFace.WEST).getX() + ":" + event.getBlock().getRelative(BlockFace.WEST).getY() + ":" + event.getBlock().getRelative(BlockFace.WEST).getZ()) == null) {
                new ActiveUtil(event.getBlock().getRelative(BlockFace.WEST).getX() + ":" + event.getBlock().getRelative(BlockFace.WEST).getY() + ":" + event.getBlock().getRelative(BlockFace.WEST).getZ(), true).insert();
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        ActiveUtil.get(event.getBlock().getRelative(BlockFace.WEST).getX() + ":" + event.getBlock().getRelative(BlockFace.WEST).getY() + ":" + event.getBlock().getRelative(BlockFace.WEST).getZ()).setActivated(false);
                    }
                }, 102L);
            } else {
                if (ActiveUtil.get(event.getBlock().getRelative(BlockFace.WEST).getX() + ":" + event.getBlock().getRelative(BlockFace.WEST).getY() + ":" + event.getBlock().getRelative(BlockFace.WEST).getZ()).getActivated() == true) {
                    return;
                } else {
                    ActiveUtil.get(event.getBlock().getRelative(BlockFace.WEST).getX() + ":" + event.getBlock().getRelative(BlockFace.WEST).getY() + ":" + event.getBlock().getRelative(BlockFace.WEST).getZ()).setActivated(true);
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            ActiveUtil.get(event.getBlock().getRelative(BlockFace.WEST).getX() + ":" + event.getBlock().getRelative(BlockFace.WEST).getY() + ":" + event.getBlock().getRelative(BlockFace.WEST).getZ()).setActivated(false);
                        }
                    }, 102L);
                }
            }
            String owner = null;
            if (event.getBlock().hasMetadata("tnt-owner")) {
                owner = event.getBlock().getMetadata("tnt-owner").get(0).asString();
            }
            event.getBlock().getRelative(BlockFace.WEST).setType(Material.AIR);
            final Random random = new Random();
            final String r = String.valueOf(random.nextInt(10000000));

            final String loc = event.getBlock().getRelative(BlockFace.WEST).getX() + ":" + event.getBlock().getRelative(BlockFace.WEST).getY() + ":" + event.getBlock().getRelative(BlockFace.WEST).getZ();
            if (FortificationAPI.getTNTLevel(loc) != 0) {
                if (TntLevelUtil.get(r) == null) {
                    new TntLevelUtil(r, FortificationAPI.getTNTLevel(loc)).insert();
                } else {
                    TntLevelUtil.get(r).setLevel(FortificationAPI.getTNTLevel(loc));
                }
                FortificationAPI.removeTNTInfo(loc);
            } else {
                return;
            }

            Location tntSpawn = event.getBlock().getRelative(BlockFace.WEST).getLocation().add(0, 1, 0);

            if (event.getBlock().getData() == 8) {
                tntSpawn.setY(tntSpawn.getY() - 1.0D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }
            if (event.getBlock().getData() == 9) {
                tntSpawn.setY(tntSpawn.getY() + 1.0D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }
            if (event.getBlock().getData() == 10) {
                tntSpawn.setZ(tntSpawn.getZ() - 0.5D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
            }
            if (event.getBlock().getData() == 11) {
                tntSpawn.setZ(tntSpawn.getZ() + 1.5D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
            }
            if (event.getBlock().getData() == 12) {
                tntSpawn.setX(tntSpawn.getX() - 0.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }
            if (event.getBlock().getData() == 13) {
                tntSpawn.setX(tntSpawn.getX() + 1.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }

            final Entity tnt = event.getBlock().getRelative(BlockFace.WEST).getWorld().spawn(tntSpawn, TNTPrimed.class);
            ((TNTPrimed) tnt).setFuseTicks(90);
            tnt.setCustomName(r);
            if (owner != null) {
                tnt.setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), owner));
            }
            tnt.setCustomNameVisible(false);
        } else if (event.getBlock().getRelative(BlockFace.EAST).getType().equals(Material.TNT)) {
            if (ActiveUtil.get(event.getBlock().getRelative(BlockFace.EAST).getX() + ":" + event.getBlock().getRelative(BlockFace.EAST).getY() + ":" + event.getBlock().getRelative(BlockFace.EAST).getZ()) == null) {
                new ActiveUtil(event.getBlock().getRelative(BlockFace.EAST).getX() + ":" + event.getBlock().getRelative(BlockFace.EAST).getY() + ":" + event.getBlock().getRelative(BlockFace.EAST).getZ(), true).insert();
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        ActiveUtil.get(event.getBlock().getRelative(BlockFace.EAST).getX() + ":" + event.getBlock().getRelative(BlockFace.EAST).getY() + ":" + event.getBlock().getRelative(BlockFace.EAST).getZ()).setActivated(false);
                    }
                }, 102L);
            } else {
                if (ActiveUtil.get(event.getBlock().getRelative(BlockFace.EAST).getX() + ":" + event.getBlock().getRelative(BlockFace.EAST).getY() + ":" + event.getBlock().getRelative(BlockFace.EAST).getZ()).getActivated() == true) {
                    return;
                } else {
                    ActiveUtil.get(event.getBlock().getRelative(BlockFace.EAST).getX() + ":" + event.getBlock().getRelative(BlockFace.EAST).getY() + ":" + event.getBlock().getRelative(BlockFace.EAST).getZ()).setActivated(true);
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            ActiveUtil.get(event.getBlock().getRelative(BlockFace.EAST).getX() + ":" + event.getBlock().getRelative(BlockFace.EAST).getY() + ":" + event.getBlock().getRelative(BlockFace.EAST).getZ()).setActivated(false);
                        }
                    }, 102L);
                }
            }
            String owner = null;
            if (event.getBlock().hasMetadata("tnt-owner")) {
                owner = event.getBlock().getMetadata("tnt-owner").get(0).asString();
            }
            event.getBlock().getRelative(BlockFace.EAST).setType(Material.AIR);
            final Random random = new Random();
            final String r = String.valueOf(random.nextInt(10000000));

            final String loc = event.getBlock().getRelative(BlockFace.EAST).getX() + ":" + event.getBlock().getRelative(BlockFace.EAST).getY() + ":" + event.getBlock().getRelative(BlockFace.EAST).getZ();
            if (FortificationAPI.getTNTLevel(loc) != 0) {
                if (TntLevelUtil.get(r) == null) {
                    new TntLevelUtil(r, FortificationAPI.getTNTLevel(loc)).insert();
                } else {
                    TntLevelUtil.get(r).setLevel(FortificationAPI.getTNTLevel(loc));
                }
                FortificationAPI.removeTNTInfo(loc);
            } else {
                return;
            }

            Location tntSpawn = event.getBlock().getRelative(BlockFace.EAST).getLocation().add(0, 1, 0);

            if (event.getBlock().getData() == 8) {
                tntSpawn.setY(tntSpawn.getY() - 1.0D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }
            if (event.getBlock().getData() == 9) {
                tntSpawn.setY(tntSpawn.getY() + 1.0D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }
            if (event.getBlock().getData() == 10) {
                tntSpawn.setZ(tntSpawn.getZ() - 0.5D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
            }
            if (event.getBlock().getData() == 11) {
                tntSpawn.setZ(tntSpawn.getZ() + 1.5D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
            }
            if (event.getBlock().getData() == 12) {
                tntSpawn.setX(tntSpawn.getX() - 0.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }
            if (event.getBlock().getData() == 13) {
                tntSpawn.setX(tntSpawn.getX() + 1.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }

            final Entity tnt = event.getBlock().getRelative(BlockFace.EAST).getWorld().spawn(tntSpawn, TNTPrimed.class);
            ((TNTPrimed) tnt).setFuseTicks(90);
            tnt.setCustomName(r);
            if (owner != null) {
                tnt.setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), owner));
            }
            tnt.setCustomNameVisible(false);
        } else if (event.getBlock().getRelative(BlockFace.UP).getType().equals(Material.TNT)) {
            if (ActiveUtil.get(event.getBlock().getRelative(BlockFace.UP).getX() + ":" + event.getBlock().getRelative(BlockFace.UP).getY() + ":" + event.getBlock().getRelative(BlockFace.UP).getZ()) == null) {
                new ActiveUtil(event.getBlock().getRelative(BlockFace.UP).getX() + ":" + event.getBlock().getRelative(BlockFace.UP).getY() + ":" + event.getBlock().getRelative(BlockFace.UP).getZ(), true).insert();
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        ActiveUtil.get(event.getBlock().getRelative(BlockFace.UP).getX() + ":" + event.getBlock().getRelative(BlockFace.UP).getY() + ":" + event.getBlock().getRelative(BlockFace.UP).getZ()).setActivated(false);
                    }
                }, 102L);
            } else {
                if (ActiveUtil.get(event.getBlock().getRelative(BlockFace.UP).getX() + ":" + event.getBlock().getRelative(BlockFace.UP).getY() + ":" + event.getBlock().getRelative(BlockFace.UP).getZ()).getActivated() == true) {
                    return;
                } else {
                    ActiveUtil.get(event.getBlock().getRelative(BlockFace.UP).getX() + ":" + event.getBlock().getRelative(BlockFace.UP).getY() + ":" + event.getBlock().getRelative(BlockFace.UP).getZ()).setActivated(true);
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            ActiveUtil.get(event.getBlock().getRelative(BlockFace.UP).getX() + ":" + event.getBlock().getRelative(BlockFace.UP).getY() + ":" + event.getBlock().getRelative(BlockFace.UP).getZ()).setActivated(false);
                        }
                    }, 102L);
                }
            }
            String owner = null;
            if (event.getBlock().hasMetadata("tnt-owner")) {
                owner = event.getBlock().getMetadata("tnt-owner").get(0).asString();
            }
            event.getBlock().getRelative(BlockFace.UP).setType(Material.AIR);
            final Random random = new Random();
            final String r = String.valueOf(random.nextInt(10000000));

            final String loc = event.getBlock().getRelative(BlockFace.UP).getX() + ":" + event.getBlock().getRelative(BlockFace.UP).getY() + ":" + event.getBlock().getRelative(BlockFace.UP).getZ();
            if (FortificationAPI.getTNTLevel(loc) != 0) {
                if (TntLevelUtil.get(r) == null) {
                    new TntLevelUtil(r, FortificationAPI.getTNTLevel(loc)).insert();
                } else {
                    TntLevelUtil.get(r).setLevel(FortificationAPI.getTNTLevel(loc));
                }
                FortificationAPI.removeTNTInfo(loc);
            } else {
                return;
            }

            Location tntSpawn = event.getBlock().getRelative(BlockFace.UP).getLocation().add(0, 1, 0);

            if (event.getBlock().getData() == 8) {
                tntSpawn.setY(tntSpawn.getY() - 1.0D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }
            if (event.getBlock().getData() == 9) {
                tntSpawn.setY(tntSpawn.getY() + 1.0D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }
            if (event.getBlock().getData() == 10) {
                tntSpawn.setZ(tntSpawn.getZ() - 0.5D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
            }
            if (event.getBlock().getData() == 11) {
                tntSpawn.setZ(tntSpawn.getZ() + 1.5D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
            }
            if (event.getBlock().getData() == 12) {
                tntSpawn.setX(tntSpawn.getX() - 0.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }
            if (event.getBlock().getData() == 13) {
                tntSpawn.setX(tntSpawn.getX() + 1.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }

            final Entity tnt = event.getBlock().getRelative(BlockFace.UP).getWorld().spawn(tntSpawn, TNTPrimed.class);
            ((TNTPrimed) tnt).setFuseTicks(90);
            tnt.setCustomName(r);
            if (owner != null) {
                tnt.setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), owner));
            }
            tnt.setCustomNameVisible(false);
        } else if (event.getBlock().getRelative(BlockFace.DOWN).getType().equals(Material.TNT)) {
            if (ActiveUtil.get(event.getBlock().getRelative(BlockFace.DOWN).getX() + ":" + event.getBlock().getRelative(BlockFace.DOWN).getY() + ":" + event.getBlock().getRelative(BlockFace.DOWN).getZ()) == null) {
                new ActiveUtil(event.getBlock().getRelative(BlockFace.DOWN).getX() + ":" + event.getBlock().getRelative(BlockFace.DOWN).getY() + ":" + event.getBlock().getRelative(BlockFace.DOWN).getZ(), true).insert();
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        ActiveUtil.get(event.getBlock().getRelative(BlockFace.DOWN).getX() + ":" + event.getBlock().getRelative(BlockFace.DOWN).getY() + ":" + event.getBlock().getRelative(BlockFace.DOWN).getZ()).setActivated(false);
                    }
                }, 102L);
            } else {
                if (ActiveUtil.get(event.getBlock().getRelative(BlockFace.DOWN).getX() + ":" + event.getBlock().getRelative(BlockFace.DOWN).getY() + ":" + event.getBlock().getRelative(BlockFace.DOWN).getZ()).getActivated() == true) {
                    return;
                } else {
                    ActiveUtil.get(event.getBlock().getRelative(BlockFace.DOWN).getX() + ":" + event.getBlock().getRelative(BlockFace.DOWN).getY() + ":" + event.getBlock().getRelative(BlockFace.DOWN).getZ()).setActivated(true);
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            ActiveUtil.get(event.getBlock().getRelative(BlockFace.DOWN).getX() + ":" + event.getBlock().getRelative(BlockFace.DOWN).getY() + ":" + event.getBlock().getRelative(BlockFace.DOWN).getZ()).setActivated(false);
                        }
                    }, 102L);
                }
            }
            String owner = null;
            if (event.getBlock().hasMetadata("tnt-owner")) {
                owner = event.getBlock().getMetadata("tnt-owner").get(0).asString();
            }
            event.getBlock().getRelative(BlockFace.DOWN).setType(Material.AIR);
            final Random random = new Random();
            final String r = String.valueOf(random.nextInt(10000000));

            final String loc = event.getBlock().getRelative(BlockFace.DOWN).getX() + ":" + event.getBlock().getRelative(BlockFace.DOWN).getY() + ":" + event.getBlock().getRelative(BlockFace.DOWN).getZ();
            if (FortificationAPI.getTNTLevel(loc) != 0) {
                if (TntLevelUtil.get(r) == null) {
                    new TntLevelUtil(r, FortificationAPI.getTNTLevel(loc)).insert();
                } else {
                    TntLevelUtil.get(r).setLevel(FortificationAPI.getTNTLevel(loc));
                }
                FortificationAPI.removeTNTInfo(loc);
            } else {
                return;
            }

            Location tntSpawn = event.getBlock().getRelative(BlockFace.DOWN).getLocation().add(0, 1, 0);

            if (event.getBlock().getData() == 8) {
                tntSpawn.setY(tntSpawn.getY() - 1.0D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }
            if (event.getBlock().getData() == 9) {
                tntSpawn.setY(tntSpawn.getY() + 1.0D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }
            if (event.getBlock().getData() == 10) {
                tntSpawn.setZ(tntSpawn.getZ() - 0.5D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
            }
            if (event.getBlock().getData() == 11) {
                tntSpawn.setZ(tntSpawn.getZ() + 1.5D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
            }
            if (event.getBlock().getData() == 12) {
                tntSpawn.setX(tntSpawn.getX() - 0.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }
            if (event.getBlock().getData() == 13) {
                tntSpawn.setX(tntSpawn.getX() + 1.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }

            final Entity tnt = event.getBlock().getRelative(BlockFace.DOWN).getWorld().spawn(tntSpawn, TNTPrimed.class);
            ((TNTPrimed) tnt).setFuseTicks(90);
            tnt.setCustomName(r);
            if (owner != null) {
                tnt.setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), owner));
            }
            tnt.setCustomNameVisible(false);
        }
    }

    @EventHandler
    public void onEntityExplode(final ExplosionPrimeEvent event) {
        if (event.getEntity().getType() == EntityType.PRIMED_TNT) {
            if (event.getEntity().getCustomName() != null) {
                if (event.getEntity().getCustomName().equalsIgnoreCase("GHAST")) {
                    event.setRadius(2);
                    event.setFire(true);
                    return;
                }
                if (TntLevelUtil.get(event.getEntity().getCustomName()) != null && TntLevelUtil.get(event.getEntity().getCustomName()).getLevel() == 1) {
                    event.setRadius(2.5f);
                } else if (TntLevelUtil.get(event.getEntity().getCustomName()) != null && TntLevelUtil.get(event.getEntity().getCustomName()).getLevel() == 3) {
                    event.setRadius(3.5f);
                    event.setFire(true);
                } else if (TntLevelUtil.get(event.getEntity().getCustomName()) != null && TntLevelUtil.get(event.getEntity().getCustomName()).getLevel() == 5) {
                    event.setRadius(5);
                    event.setFire(true);
                }
            }
        }
    }

    @EventHandler
    public void activeTnTFlint(final PlayerInteractEvent event) {
        final Player player = event.getPlayer();

        if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
            if (event.getPlayer().getItemInHand().getType() == Material.STICK) {
                if (event.getClickedBlock().getType() == Material.END_STONE || event.getClickedBlock().getType() == Material.SMOOTH_STONE) {
                    final String loc = event.getClickedBlock().getX() + ":" + event.getClickedBlock().getY() + ":" + event.getClickedBlock().getZ();
                    if (FortificationAPI.getExist(loc) != null) {
                        if (event.getClickedBlock().getType() == Material.END_STONE) {
                            player.sendMessage(ChatColor.GRAY + "\n * Este bloco tem a durabilidade de " + FortificationAPI.getLife(loc, event.getClickedBlock().getType()) + "/4\n ");
                            return;
                        }
                        if (event.getClickedBlock().getType() == Material.SMOOTH_STONE) {
                            player.sendMessage(ChatColor.GRAY + "\n * Este bloco tem a durabilidade de " + FortificationAPI.getLife(loc, event.getClickedBlock().getType()) + "/10\n ");
                            return;
                        }
                    }
                }
            }
        }

        if (event.getAction() == Action.RIGHT_CLICK_BLOCK &&
                event.getPlayer().getItemInHand().getType() == Material.FLINT_AND_STEEL &&
                event.getClickedBlock().getType() == Material.TNT) {
            event.setCancelled(true);
            String owner = null;
            if (event.getClickedBlock().hasMetadata("tnt-owner")) {
                owner = event.getClickedBlock().getMetadata("tnt-owner").get(0).asString();
            }
            event.getClickedBlock().setType(Material.AIR);
            final Random random = new Random();
            final String r = String.valueOf(random.nextInt(10000000));

            final String loc = event.getClickedBlock().getX() + ":" + event.getClickedBlock().getY() + ":" + event.getClickedBlock().getZ();
            if (FortificationAPI.getTNTLevel(loc) != 0) {
                if (TntLevelUtil.get(r) == null) {
                    new TntLevelUtil(r, FortificationAPI.getTNTLevel(loc)).insert();
                } else {
                    TntLevelUtil.get(r).setLevel(FortificationAPI.getTNTLevel(loc));
                }
                FortificationAPI.removeTNTInfo(loc);
            } else {
                return;
            }

            Location tntSpawn = event.getClickedBlock().getLocation();

            if (event.getClickedBlock().getData() == 8) {
                tntSpawn.setY(tntSpawn.getY() - 1.0D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }
            if (event.getClickedBlock().getData() == 9) {
                tntSpawn.setY(tntSpawn.getY() + 1.0D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }
            if (event.getClickedBlock().getData() == 10) {
                tntSpawn.setZ(tntSpawn.getZ() - 0.5D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
            }
            if (event.getClickedBlock().getData() == 11) {
                tntSpawn.setZ(tntSpawn.getZ() + 1.5D);
                tntSpawn.setX(tntSpawn.getX() + 0.5D);
            }
            if (event.getClickedBlock().getData() == 12) {
                tntSpawn.setX(tntSpawn.getX() - 0.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }
            if (event.getClickedBlock().getData() == 13) {
                tntSpawn.setX(tntSpawn.getX() + 1.5D);
                tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
            }

            final Entity tnt = event.getClickedBlock().getWorld().spawn(tntSpawn, TNTPrimed.class);
            ((TNTPrimed) tnt).setFuseTicks(90);
            tnt.setCustomName(r);
            if (owner != null) {
                tnt.setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), owner));
            }
            tnt.setCustomNameVisible(false);
        }
    }

    @EventHandler
    public void activeTntDispenser(final BlockDispenseEvent event) {
        if (event.getBlock().getType() == Material.DISPENSER &&
                event.getItem().hasItemMeta() && event.getItem().getType() == Material.TNT &&
                event.getItem().getItemMeta().getDisplayName().contains(ChatColor.RED + "Explosivo de repulsão")) {
            event.setCancelled(true);
            final Dispenser dispenser = (Dispenser) event.getBlock().getState();
            dispenser.getInventory().removeItem(event.getItem());
            final Block block = event.getBlock().getRelative(event.getBlock().getFace(event.getBlock()));
            final Location loc = block.getLocation();
            if (event.getBlock().getData() == 8) {
                loc.setY(loc.getY() - 1.0D);
                loc.setX(loc.getX() + 0.5D);
                loc.setZ(loc.getZ() + 0.5D);
            }
            if (event.getBlock().getData() == 9) {
                loc.setY(loc.getY() + 1.0D);
                loc.setX(loc.getX() + 0.5D);
                loc.setZ(loc.getZ() + 0.5D);
            }
            if (event.getBlock().getData() == 10) {
                loc.setZ(loc.getZ() - 0.5D);
                loc.setX(loc.getX() + 0.5D);
            }
            if (event.getBlock().getData() == 11) {
                loc.setZ(loc.getZ() + 1.5D);
                loc.setX(loc.getX() + 0.5D);
            }
            if (event.getBlock().getData() == 12) {
                loc.setX(loc.getX() - 0.5D);
                loc.setZ(loc.getZ() + 0.5D);
            }
            if (event.getBlock().getData() == 13) {
                loc.setX(loc.getX() + 1.5D);
                loc.setZ(loc.getZ() + 0.5D);
            }

            final Random random = new Random();
            final String r = String.valueOf(random.nextInt(10000000));

            if (TntLevelUtil.get(r) == null) {
                new TntLevelUtil(r, 1000).insert();
            } else {
                TntLevelUtil.get(r).setLevel(1000);
            }

            final Entity tnt = event.getBlock().getWorld().spawn(loc, TNTPrimed.class);
            ((TNTPrimed) tnt).setFuseTicks(90);
            tnt.setCustomName(r);
            tnt.setCustomNameVisible(false);
        } else if (event.getBlock().getType() == Material.DISPENSER &&
                event.getItem().hasItemMeta() && event.getItem().getType() == Material.TNT &&
                event.getItem().getItemMeta().getDisplayName().contains(ChatColor.RED + "Explosivo") &&
                event.getItem().getItemMeta().getLore().get(1).contains(ChatColor.GRAY + "Dano: ")) {
            event.setCancelled(true);
            final Dispenser dispenser = (Dispenser) event.getBlock().getState();
            dispenser.getInventory().removeItem(event.getItem());
            final Block block = event.getBlock().getRelative(event.getBlock().getFace(event.getBlock()));
            final Location loc = block.getLocation();
            if (event.getBlock().getData() == 8) {
                loc.setY(loc.getY() - 1.0D);
                loc.setX(loc.getX() + 0.5D);
                loc.setZ(loc.getZ() + 0.5D);
            }
            if (event.getBlock().getData() == 9) {
                loc.setY(loc.getY() + 1.0D);
                loc.setX(loc.getX() + 0.5D);
                loc.setZ(loc.getZ() + 0.5D);
            }
            if (event.getBlock().getData() == 10) {
                loc.setZ(loc.getZ() - 0.5D);
                loc.setX(loc.getX() + 0.5D);
            }
            if (event.getBlock().getData() == 11) {
                loc.setZ(loc.getZ() + 1.5D);
                loc.setX(loc.getX() + 0.5D);
            }
            if (event.getBlock().getData() == 12) {
                loc.setX(loc.getX() - 0.5D);
                loc.setZ(loc.getZ() + 0.5D);
            }
            if (event.getBlock().getData() == 13) {
                loc.setX(loc.getX() + 1.5D);
                loc.setZ(loc.getZ() + 0.5D);
            }

            final Random random = new Random();
            final String r = String.valueOf(random.nextInt(10000000));

            if (TntLevelUtil.get(r) == null) {
                new TntLevelUtil(r, Integer.valueOf(event.getItem().getItemMeta().getLore().get(1).substring(8, event.getItem().getItemMeta().getLore().get(1).length() - 2))).insert();
            } else {
                TntLevelUtil.get(r).setLevel(Integer.valueOf(event.getItem().getItemMeta().getLore().get(1).substring(8, event.getItem().getItemMeta().getLore().get(1).length() - 2)));
            }

            final Entity tnt = event.getBlock().getWorld().spawn(loc, TNTPrimed.class);
            ((TNTPrimed) tnt).setFuseTicks(90);
            tnt.setCustomName(r);
            if (dispenser.getCustomName() != null) {
                tnt.setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), dispenser.getCustomName()));
            }
            tnt.setCustomNameVisible(false);
        }
    }

    @EventHandler
    public void activeTntProjectileHit(final ProjectileHitEvent event) {
        final List<EntityType> entityTypes;
        entityTypes = new ArrayList<>();
        entityTypes.add(EntityType.ARROW);
        entityTypes.add(EntityType.FIREBALL);
        entityTypes.add(EntityType.SMALL_FIREBALL);

        final Projectile projectile = event.getEntity();
        if (entityTypes.contains(projectile.getType())) {
            if (projectile.getFireTicks() == 0) {
                return;
            }

            if (event.getHitBlock() != null && event.getHitBlock().getType().equals(Material.TNT)) {
                final String owner = event.getHitBlock().getMetadata("tnt-owner").get(0).asString();
                event.getHitBlock().setType(Material.AIR);
                final Random random = new Random();
                final String r = String.valueOf(random.nextInt(10000000));

                final String loc = event.getHitBlock().getX() + ":" + event.getHitBlock().getY() + ":" + event.getHitBlock().getZ();
                if (FortificationAPI.getTNTLevel(loc) != 0) {
                    if (TntLevelUtil.get(r) == null) {
                        new TntLevelUtil(r, FortificationAPI.getTNTLevel(loc)).insert();
                    } else {
                        TntLevelUtil.get(r).setLevel(FortificationAPI.getTNTLevel(loc));
                    }
                    FortificationAPI.removeTNTInfo(loc);
                } else {
                    return;
                }

                final Location tntSpawn = event.getHitBlock().getLocation().add(0, 1, 0);
                if (event.getHitBlock().getData() == 8) {
                    tntSpawn.setY(tntSpawn.getY() - 1.0D);
                    tntSpawn.setX(tntSpawn.getX() + 0.5D);
                    tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
                }
                if (event.getHitBlock().getData() == 9) {
                    tntSpawn.setY(tntSpawn.getY() + 1.0D);
                    tntSpawn.setX(tntSpawn.getX() + 0.5D);
                    tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
                }
                if (event.getHitBlock().getData() == 10) {
                    tntSpawn.setZ(tntSpawn.getZ() - 0.5D);
                    tntSpawn.setX(tntSpawn.getX() + 0.5D);
                }
                if (event.getHitBlock().getData() == 11) {
                    tntSpawn.setZ(tntSpawn.getZ() + 1.5D);
                    tntSpawn.setX(tntSpawn.getX() + 0.5D);
                }
                if (event.getHitBlock().getData() == 12) {
                    tntSpawn.setX(tntSpawn.getX() - 0.5D);
                    tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
                }
                if (event.getHitBlock().getData() == 13) {
                    tntSpawn.setX(tntSpawn.getX() + 1.5D);
                    tntSpawn.setZ(tntSpawn.getZ() + 0.5D);
                }

                final Entity tnt = event.getHitBlock().getWorld().spawn(tntSpawn, TNTPrimed.class);
                ((TNTPrimed) tnt).setFuseTicks(90);
                tnt.setCustomName(r);
                tnt.setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), owner));
                tnt.setCustomNameVisible(false);
            }
        }
    }

    @EventHandler
    public void entitySpawn(final PlayerInteractEvent event) {
        if (event.getPlayer().getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
            if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && event.getPlayer().getItemInHand() != null) {
                if (event.getPlayer().getItemInHand().getType().equals(Material.CREEPER_SPAWN_EGG)) {
                    event.setCancelled(true);
                    final Entity entity = event.getClickedBlock().getWorld().spawnEntity(event.getClickedBlock().getLocation().add(-0.5, 1, -0.5), EntityType.CREEPER);
                    entity.setGlowing(true);
                    RemoveItemInv.removeItemsFromType(event.getPlayer().getInventory(), event.getPlayer().getItemInHand(), 1);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            final Entity tnt = event.getClickedBlock().getWorld().spawn(event.getClickedBlock().getLocation().add(-0.5, 1, -0.5), TNTPrimed.class);
                            tnt.setCustomName("CREEPER");
                            tnt.setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), FacAPI.getFacNome(event.getPlayer().getUniqueId().toString())));
                            tnt.setCustomNameVisible(false);
                            ((TNTPrimed) tnt).setFuseTicks(0);
                            entity.remove();
                        }
                    }, 40L);
                } else if (event.getPlayer().getItemInHand().getType().equals(Material.GHAST_SPAWN_EGG)) {
                    event.setCancelled(true);

                    final Entity armorS = event.getClickedBlock().getWorld().spawnEntity(event.getClickedBlock().getLocation().add(-0.5, 4, -0.5), EntityType.ARMOR_STAND);
                    final ArmorStand armorStand = (ArmorStand) armorS;
                    armorStand.setVisible(false);

                    final Entity entity = event.getClickedBlock().getWorld().spawnEntity(event.getClickedBlock().getLocation().add(-0.5, 4, -0.5), EntityType.GHAST);

                    armorS.setPassenger(entity);

                    entity.setGlowing(true);
                    RemoveItemInv.removeItemsFromType(event.getPlayer().getInventory(), event.getPlayer().getItemInHand(), 1);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            final Entity tnt = event.getClickedBlock().getWorld().spawn(event.getClickedBlock().getLocation().add(-0.5, 4, -0.5), TNTPrimed.class);
                            tnt.setCustomName("GHAST");
                            tnt.setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), FacAPI.getFac(event.getPlayer().getUniqueId().toString())));
                            tnt.setCustomNameVisible(false);
                            ((TNTPrimed) tnt).setFuseTicks(0);
                            entity.remove();
                            armorS.remove();
                        }
                    }, 60L);
                }
            }
        }
    }

    @EventHandler
    public void blockExplode(final EntityExplodeEvent event) {
        if (event.getLocation().getBlock().isLiquid() || event.getLocation().getBlock().getType().equals(Material.WATER)) {
            event.setCancelled(true);
            final int runnable = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                @Override
                public void run() {
                    event.getLocation().getBlock().setType(Material.AIR);
                    if (event.getLocation().getBlock().getRelative(BlockFace.NORTH).getType().equals(Material.WATER)) {
                        event.getLocation().getBlock().getRelative(BlockFace.NORTH).setType(Material.AIR);
                    }
                    if (event.getLocation().getBlock().getRelative(BlockFace.SOUTH).getType().equals(Material.WATER)) {
                        event.getLocation().getBlock().getRelative(BlockFace.SOUTH).setType(Material.AIR);
                    }
                    if (event.getLocation().getBlock().getRelative(BlockFace.WEST).getType().equals(Material.WATER)) {
                        event.getLocation().getBlock().getRelative(BlockFace.WEST).setType(Material.AIR);
                    }
                    if (event.getLocation().getBlock().getRelative(BlockFace.EAST).getType().equals(Material.WATER)) {
                        event.getLocation().getBlock().getRelative(BlockFace.EAST).setType(Material.AIR);
                    }
                    if (event.getLocation().getBlock().getRelative(BlockFace.DOWN).getType().equals(Material.WATER)) {
                        event.getLocation().getBlock().getRelative(BlockFace.DOWN).setType(Material.AIR);
                    }
                    if (event.getLocation().getBlock().getRelative(BlockFace.UP).getType().equals(Material.WATER)) {
                        event.getLocation().getBlock().getRelative(BlockFace.UP).setType(Material.AIR);
                    }
                }
            }, 0L, 2L);

            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                @Override
                public void run() {
                    final Entity tnt = event.getEntity().getWorld().spawn(event.getEntity().getLocation(), TNTPrimed.class);
                    ((TNTPrimed) tnt).setFuseTicks(1);
                    tnt.setCustomName(event.getEntity().getCustomName());
                    if (event.getEntity().hasMetadata("tnt-owner")) {
                        tnt.setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), event.getEntity().getMetadata("tnt-owner").get(0).asString()));
                    }
                    tnt.setCustomNameVisible(false);
                }
            }, 3L);

            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                @Override
                public void run() {
                    Bukkit.getScheduler().cancelTask(runnable);
                }
            }, 10L);
        }

        if (event.getEntity().getLocation().getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
            //EXPLOSÃO NO NEXUS
            if (NexusUtil.playerInArea(event.getEntity().getLocation()) != null) {
                if (event.getEntity().getCustomName() != null) {
                    final String chunk = event.getLocation().getChunk().getX() + ":" + event.getLocation().getChunk().getZ();

                    if (event.getEntity().getCustomName().equalsIgnoreCase("GHAST")) {
                        FacAPI.updateLifeNexus(FacAPI.getChunkOwn(chunk), FacAPI.getNexusLife(FacAPI.getChunkOwn(chunk)) - 3);

                        if (event.getEntity().hasMetadata("tnt-owner")) {
                            updateBossLife(event.getEntity().getMetadata("tnt-owner").get(0).asString(), FacAPI.getChunkOwn(chunk), FacAPI.getNexusLife(FacAPI.getChunkOwn(chunk)));
                        }
                        if (FacAPI.getNexusLife(FacAPI.getChunkOwn(chunk)) > 0) {
                            event.setCancelled(true);

                            if (FacAPI.getSobAttack(FacAPI.getChunkOwn(chunk)) == true) {
                                attack(event.getEntity().getLocation(), false, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), false);
                            } else {
                                attack(event.getEntity().getLocation(), true, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), false);
                            }

                            return;
                        } else {
                            final List<Block> a = new ArrayList<>();
                            for (final Block b : event.blockList()) {
                                if (!b.getType().equals(Material.GRAY_STAINED_GLASS) && !b.getType().equals(Material.IRON_DOOR)) {
                                    a.add(b);
                                    continue;
                                } else {
                                    b.setType(Material.AIR);
                                }
                            }
                            for (final Block c : a) {
                                event.blockList().remove(c);
                            }
                        }

                        final String chunkOwn = FacAPI.getChunkOwn(chunk);
                        if (InvadedHash.get(chunkOwn) == null || InvadedHash.get(chunkOwn).getStatus() == false) {
                            if (InvadedHash.get(chunkOwn) == null) {
                                new InvadedHash(chunkOwn, true).insert();
                            } else {
                                InvadedHash.get(chunkOwn).setStatus(true);
                            }

                            if (FacAPI.getSobAttack(FacAPI.getChunkOwn(chunk)) == true) {
                                attack(event.getEntity().getLocation(), false, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), true);
                            } else {
                                attack(event.getEntity().getLocation(), true, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), true);
                            }
                        }
                    } else if (event.getEntity().getCustomName().equalsIgnoreCase("CREEPER")) {
                        FacAPI.updateLifeNexus(FacAPI.getChunkOwn(chunk), FacAPI.getNexusLife(FacAPI.getChunkOwn(chunk)) - 1);

                        if (event.getEntity().hasMetadata("tnt-owner")) {
                            updateBossLife(event.getEntity().getMetadata("tnt-owner").get(0).asString(), FacAPI.getChunkOwn(chunk), FacAPI.getNexusLife(FacAPI.getChunkOwn(chunk)));
                        }
                        if (FacAPI.getNexusLife(FacAPI.getChunkOwn(chunk)) > 0) {
                            event.setCancelled(true);

                            if (FacAPI.getSobAttack(FacAPI.getChunkOwn(chunk)) == true) {
                                attack(event.getEntity().getLocation(), false, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), false);
                            } else {
                                attack(event.getEntity().getLocation(), true, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), false);
                            }

                            return;
                        } else {
                            final List<Block> a = new ArrayList<>();
                            for (final Block b : event.blockList()) {
                                if (!b.getType().equals(Material.GRAY_STAINED_GLASS) && !b.getType().equals(Material.IRON_DOOR)) {
                                    a.add(b);
                                    continue;
                                } else {
                                    b.setType(Material.AIR);
                                }
                            }
                            for (final Block c : a) {
                                event.blockList().remove(c);
                            }
                        }

                        final String chunkOwn = FacAPI.getChunkOwn(chunk);
                        if (InvadedHash.get(chunkOwn) == null || InvadedHash.get(chunkOwn).getStatus() == false) {
                            if (InvadedHash.get(chunkOwn) == null) {
                                new InvadedHash(chunkOwn, true).insert();
                            } else {
                                InvadedHash.get(chunkOwn).setStatus(true);
                            }

                            if (FacAPI.getSobAttack(FacAPI.getChunkOwn(chunk)) == true) {
                                attack(event.getEntity().getLocation(), false, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), true);
                            } else {
                                attack(event.getEntity().getLocation(), true, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), true);
                            }
                        }
                    } else {
                        FacAPI.updateLifeNexus(FacAPI.getChunkOwn(chunk), FacAPI.getNexusLife(FacAPI.getChunkOwn(chunk)) - TntLevelUtil.get(event.getEntity().getCustomName()).getLevel());

                        if (event.getEntity().hasMetadata("tnt-owner")) {
                            updateBossLife(event.getEntity().getMetadata("tnt-owner").get(0).asString(), FacAPI.getChunkOwn(chunk), FacAPI.getNexusLife(FacAPI.getChunkOwn(chunk)));
                        }
                        if (FacAPI.getNexusLife(FacAPI.getChunkOwn(chunk)) > 0) {
                            event.setCancelled(true);

                            if (FacAPI.getSobAttack(FacAPI.getChunkOwn(chunk)) == true) {
                                attack(event.getEntity().getLocation(), false, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), false);
                            } else {
                                attack(event.getEntity().getLocation(), true, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), false);
                            }

                            return;
                        } else {
                            final List<Block> a = new ArrayList<>();
                            for (final Block b : event.blockList()) {
                                if (!b.getType().equals(Material.GRAY_STAINED_GLASS) && !b.getType().equals(Material.IRON_DOOR)) {
                                    a.add(b);
                                    continue;
                                } else {
                                    b.setType(Material.AIR);
                                }
                            }
                            for (final Block c : a) {
                                event.blockList().remove(c);
                            }
                        }

                        final String chunkOwn = FacAPI.getChunkOwn(chunk);
                        if (InvadedHash.get(chunkOwn) == null || InvadedHash.get(chunkOwn).getStatus() == false) {
                            if (InvadedHash.get(chunkOwn) == null) {
                                new InvadedHash(chunkOwn, true).insert();
                            } else {
                                InvadedHash.get(chunkOwn).setStatus(true);
                            }

                            if (FacAPI.getSobAttack(FacAPI.getChunkOwn(chunk)) == true) {
                                attack(event.getEntity().getLocation(), false, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), true);
                            } else {
                                attack(event.getEntity().getLocation(), true, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), true);
                            }
                        }
                    }
                }
                return;
            }
            //----------

            if (TntLevelUtil.get(event.getEntity().getCustomName()) != null && TntLevelUtil.get(event.getEntity().getCustomName()).getLevel() == 1) {
                final List<Location> ob = generateSphere(event.getLocation(), 2, false);
                for (final Location lcs : ob) {
                    final Block block = lcs.getBlock();
                    if (block.getType().equals(Material.OBSIDIAN)) {
                        block.breakNaturally();
                    }
                }
            } else if (TntLevelUtil.get(event.getEntity().getCustomName()) != null && TntLevelUtil.get(event.getEntity().getCustomName()).getLevel() == 3) {
                final List<Location> ob = generateSphere(event.getLocation(), 2, false);
                for (final Location lcs : ob) {
                    final Block block = lcs.getBlock();
                    if (block.getType().equals(Material.OBSIDIAN)) {
                        block.breakNaturally();
                    }
                }
            } else if (TntLevelUtil.get(event.getEntity().getCustomName()) != null && TntLevelUtil.get(event.getEntity().getCustomName()).getLevel() == 5) {
                final List<Location> ob = generateSphere(event.getLocation(), 3, false);
                for (final Location lcs : ob) {
                    final Block block = lcs.getBlock();
                    if (block.getType().equals(Material.OBSIDIAN)) {
                        block.breakNaturally();
                    }
                }
            }

            final List<Block> notRemove = new ArrayList<>();
            final List<Block> blocks = event.blockList();
            for (int i = 1; i <= blocks.size(); i++) {
                if (NexusUtil.playerInArea(blocks.get(i - 1).getLocation()) != null) {
                    notRemove.add(blocks.get(i - 1));
                }
            }

            for (int i = 1; i <= notRemove.size(); i++) {
                blocks.remove(notRemove.get(i - 1));
            }

            for (int i = 1; i <= blocks.size(); i++) {
                if (event.getEntity().getCustomName() != null && TntLevelUtil.get(event.getEntity().getCustomName()) != null && TntLevelUtil.get(event.getEntity().getCustomName()).getLevel() > 100) {
                    final String loc = blocks.get(i - 1).getLocation().getBlockX() + ":" + blocks.get(i - 1).getLocation().getBlockY() + ":" + blocks.get(i - 1).getLocation().getBlockZ();
                    event.setCancelled(true);
                    FortificationAPI.removeTNTInfo(loc);
                }

                if (blocks.get(i - 1).getType().equals(Material.SPAWNER)) {
                    final BlockState blockState = blocks.get(i - 1).getState();
                    final CreatureSpawner spawner = (CreatureSpawner) blockState;

                    if (!blocks.get(i - 1).hasMetadata("placed")) {
                        event.setCancelled(true);
                        blocks.get(i - 1).setType(Material.AIR);
                    } else {
                        if (spawner.getSpawnedType().equals(EntityType.COW)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Vaca");
                            item.setItemMeta(metaItem);
                            blocks.get(i - 1).getWorld().dropItemNaturally(blocks.get(i - 1).getLocation(), item);
                            blocks.get(i - 1).setType(Material.AIR);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.CHICKEN)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Galinha");
                            item.setItemMeta(metaItem);
                            blocks.get(i - 1).getWorld().dropItemNaturally(blocks.get(i - 1).getLocation(), item);
                            blocks.get(i - 1).setType(Material.AIR);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.PIG)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Porco");
                            item.setItemMeta(metaItem);
                            blocks.get(i - 1).getWorld().dropItemNaturally(blocks.get(i - 1).getLocation(), item);
                            blocks.get(i - 1).setType(Material.AIR);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.SHEEP)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Ovelha");
                            item.setItemMeta(metaItem);
                            blocks.get(i - 1).getWorld().dropItemNaturally(blocks.get(i - 1).getLocation(), item);
                            blocks.get(i - 1).setType(Material.AIR);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.RABBIT)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Coelho");
                            item.setItemMeta(metaItem);
                            blocks.get(i - 1).getWorld().dropItemNaturally(blocks.get(i - 1).getLocation(), item);
                            blocks.get(i - 1).setType(Material.AIR);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.ZOMBIE)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Zumbi");
                            item.setItemMeta(metaItem);
                            blocks.get(i - 1).getWorld().dropItemNaturally(blocks.get(i - 1).getLocation(), item);
                            blocks.get(i - 1).setType(Material.AIR);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.SKELETON)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Esqueleto");
                            item.setItemMeta(metaItem);
                            blocks.get(i - 1).getWorld().dropItemNaturally(blocks.get(i - 1).getLocation(), item);
                            blocks.get(i - 1).setType(Material.AIR);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.SPIDER)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Aranha");
                            item.setItemMeta(metaItem);
                            blocks.get(i - 1).getWorld().dropItemNaturally(blocks.get(i - 1).getLocation(), item);
                            blocks.get(i - 1).setType(Material.AIR);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.CREEPER)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Creeper");
                            item.setItemMeta(metaItem);
                            blocks.get(i - 1).getWorld().dropItemNaturally(blocks.get(i - 1).getLocation(), item);
                            blocks.get(i - 1).setType(Material.AIR);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.ENDERMAN)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Enderman");
                            item.setItemMeta(metaItem);
                            blocks.get(i - 1).getWorld().dropItemNaturally(blocks.get(i - 1).getLocation(), item);
                            blocks.get(i - 1).setType(Material.AIR);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.IRON_GOLEM)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de golem de ferro");
                            item.setItemMeta(metaItem);
                            blocks.get(i - 1).getWorld().dropItemNaturally(blocks.get(i - 1).getLocation(), item);
                            blocks.get(i - 1).setType(Material.AIR);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.SLIME)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Slime");
                            item.setItemMeta(metaItem);
                            blocks.get(i - 1).getWorld().dropItemNaturally(blocks.get(i - 1).getLocation(), item);
                            blocks.get(i - 1).setType(Material.AIR);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.BLAZE)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Blaze");
                            item.setItemMeta(metaItem);
                            blocks.get(i - 1).getWorld().dropItemNaturally(blocks.get(i - 1).getLocation(), item);
                            blocks.get(i - 1).setType(Material.AIR);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.PIG_ZOMBIE)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Homem porco zumbi");
                            item.setItemMeta(metaItem);
                            blocks.get(i - 1).getWorld().dropItemNaturally(blocks.get(i - 1).getLocation(), item);
                            blocks.get(i - 1).setType(Material.AIR);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.WITHER_SKELETON)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Esqueleto wither");
                            item.setItemMeta(metaItem);
                            blocks.get(i - 1).getWorld().dropItemNaturally(blocks.get(i - 1).getLocation(), item);
                            blocks.get(i - 1).setType(Material.AIR);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.WITCH)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Bruxa");
                            item.setItemMeta(metaItem);
                            blocks.get(i - 1).getWorld().dropItemNaturally(blocks.get(i - 1).getLocation(), item);
                            blocks.get(i - 1).setType(Material.AIR);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.MAGMA_CUBE)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Cubo magma");
                            item.setItemMeta(metaItem);
                            blocks.get(i - 1).getWorld().dropItemNaturally(blocks.get(i - 1).getLocation(), item);
                            blocks.get(i - 1).setType(Material.AIR);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.ZOMBIE_VILLAGER)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Aldeão zumbi");
                            item.setItemMeta(metaItem);
                            blocks.get(i - 1).getWorld().dropItemNaturally(blocks.get(i - 1).getLocation(), item);
                            blocks.get(i - 1).setType(Material.AIR);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.GHAST)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Ghast");
                            item.setItemMeta(metaItem);
                            blocks.get(i - 1).getWorld().dropItemNaturally(blocks.get(i - 1).getLocation(), item);
                            blocks.get(i - 1).setType(Material.AIR);
                        }
                    }
                }

                if (blocks.get(i - 1).getType().equals(Material.END_STONE) || blocks.get(i - 1).getType().equals(Material.SMOOTH_STONE)) {
                    final String explode = event.getEntity().getLocation().getBlockX() + ":" + event.getEntity().getLocation().getBlockY() + ":" + event.getEntity().getLocation().getBlockZ();
                    final String loc = blocks.get(i - 1).getLocation().getBlockX() + ":" + blocks.get(i - 1).getLocation().getBlockY() + ":" + blocks.get(i - 1).getLocation().getBlockZ();

                    if (event.getEntity().getCustomName() != null) {
                        if (event.getEntity().getCustomName().equalsIgnoreCase("CREEPER")) {
                            if (FortificationAPI.getExist(loc) != null) {
                                FortificationAPI.updateLife(loc, 1, blocks.get(i - 1).getType());
                                if (FortificationAPI.getLife(loc, blocks.get(i - 1).getType()) > 0) {
                                    event.setCancelled(true);
                                } else {
                                    FortificationAPI.removeFortification(loc);
                                }
                            }

                            final String chunk = event.getEntity().getChunk().getX() + ":" + event.getEntity().getChunk().getZ();
                            if (FacAPI.getChunkOwn(chunk) != null) {
                                if (FacAPI.getSobAttack(FacAPI.getChunkOwn(chunk)) == true) {
                                    attack(event.getEntity().getLocation(), false, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), false);
                                } else {
                                    attack(event.getEntity().getLocation(), true, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), false);
                                }
                            }
                            return;
                        }
                        if (event.getEntity().getCustomName().equalsIgnoreCase("GHAST")) {
                            if (FortificationAPI.getExist(loc) != null) {
                                FortificationAPI.updateLife(loc, 3, blocks.get(i - 1).getType());
                                if (FortificationAPI.getLife(loc, blocks.get(i - 1).getType()) > 0) {
                                    event.setCancelled(true);
                                } else {
                                    FortificationAPI.removeFortification(loc);
                                }
                            }

                            final String chunk = event.getEntity().getChunk().getX() + ":" + event.getEntity().getChunk().getZ();
                            if (FacAPI.getChunkOwn(chunk) != null) {
                                if (FacAPI.getSobAttack(FacAPI.getChunkOwn(chunk)) == true) {
                                    attack(event.getEntity().getLocation(), false, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), false);
                                } else {
                                    attack(event.getEntity().getLocation(), true, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), false);
                                }
                            }
                            return;
                        }
                        if (TntLevelUtil.get(event.getEntity().getCustomName()) != null) {
                            if (TntLevelUtil.get(event.getEntity().getCustomName()).getLevel() == 1) {
                                if (FortificationAPI.getExist(loc) != null) {
                                    FortificationAPI.updateLife(loc, 1, blocks.get(i - 1).getType());
                                    if (FortificationAPI.getLife(loc, blocks.get(i - 1).getType()) > 0) {
                                        event.setCancelled(true);
                                    } else {
                                        FortificationAPI.removeFortification(loc);
                                    }
                                }
                                FortificationAPI.removeTNTInfo(explode);

                                final String chunk = event.getEntity().getChunk().getX() + ":" + event.getEntity().getChunk().getZ();
                                if (FacAPI.getChunkOwn(chunk) != null) {
                                    if (FacAPI.getSobAttack(FacAPI.getChunkOwn(chunk)) == true) {
                                        attack(event.getEntity().getLocation(), false, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), false);
                                    } else {
                                        attack(event.getEntity().getLocation(), true, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), false);
                                    }
                                }
                            } else if (TntLevelUtil.get(event.getEntity().getCustomName()).getLevel() == 3) {
                                if (FortificationAPI.getExist(loc) != null) {
                                    FortificationAPI.updateLife(loc, 3, blocks.get(i - 1).getType());
                                    if (FortificationAPI.getLife(loc, blocks.get(i - 1).getType()) > 0) {
                                        event.setCancelled(true);
                                    } else {
                                        FortificationAPI.removeFortification(loc);
                                    }
                                    FortificationAPI.removeTNTInfo(explode);

                                    final Random random = new Random();
                                    final int r = random.nextInt(999999999);
                                    final String pos1 = (event.getLocation().getBlockX() - 5) + ":" + (event.getLocation().getBlockY() - 5) + ":" + (event.getLocation().getBlockZ() - 5);
                                    final String pos2 = (event.getLocation().getBlockX() + 5) + ":" + (event.getLocation().getBlockY() + 5) + ":" + (event.getLocation().getBlockZ() + 5);
                                    final RadiationHash rg = new RadiationHash(String.valueOf(r), pos1, pos2);
                                    RadiationRgs.getRegions().add(rg);

                                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                        @Override
                                        public void run() {
                                            RadiationHash.CACHE.remove(String.valueOf(r));
                                        }
                                    }, 12000L);

                                    final String chunk = event.getEntity().getChunk().getX() + ":" + event.getEntity().getChunk().getZ();
                                    if (FacAPI.getChunkOwn(chunk) != null) {
                                        if (FacAPI.getSobAttack(FacAPI.getChunkOwn(chunk)) == true) {
                                            attack(event.getEntity().getLocation(), false, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), false);
                                        } else {
                                            attack(event.getEntity().getLocation(), true, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), false);
                                        }
                                    }
                                }
                            } else if (TntLevelUtil.get(event.getEntity().getCustomName()).getLevel() == 5) {
                                if (FortificationAPI.getExist(loc) != null) {
                                    FortificationAPI.updateLife(loc, 5, blocks.get(i - 1).getType());
                                    if (FortificationAPI.getLife(loc, blocks.get(i - 1).getType()) > 0) {
                                        event.setCancelled(true);
                                    } else {
                                        FortificationAPI.removeFortification(loc);
                                    }
                                    FortificationAPI.removeTNTInfo(explode);

                                    final Random random = new Random();
                                    final int r = random.nextInt(999999999);
                                    final String pos1 = (event.getLocation().getBlockX() - 10) + ":" + (event.getLocation().getBlockY() - 10) + ":" + (event.getLocation().getBlockZ() - 10);
                                    final String pos2 = (event.getLocation().getBlockX() + 10) + ":" + (event.getLocation().getBlockY() + 10) + ":" + (event.getLocation().getBlockZ() + 10);
                                    final RadiationHash rg = new RadiationHash(String.valueOf(r), pos1, pos2);
                                    RadiationRgs.getRegions().add(rg);

                                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                        @Override
                                        public void run() {
                                            RadiationHash.CACHE.remove(String.valueOf(r));
                                        }
                                    }, 24000L);

                                    final String chunk = event.getEntity().getChunk().getX() + ":" + event.getEntity().getChunk().getZ();
                                    if (FacAPI.getChunkOwn(chunk) != null) {
                                        if (FacAPI.getSobAttack(FacAPI.getChunkOwn(chunk)) == true) {
                                            attack(event.getEntity().getLocation(), false, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), false);
                                        } else {
                                            attack(event.getEntity().getLocation(), true, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), false);
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    final String loc = blocks.get(i - 1).getLocation().getBlockX() + ":" + blocks.get(i - 1).getLocation().getBlockY() + ":" + blocks.get(i - 1).getLocation().getBlockZ();
                    if (TntLevelUtil.get(event.getEntity().getCustomName()) != null && TntLevelUtil.get(event.getEntity().getCustomName()).getLevel() == 1) {
                        final String chunk = event.getEntity().getChunk().getX() + ":" + event.getEntity().getChunk().getZ();
                        if (FacAPI.getChunkOwn(chunk) != null) {
                            if (FacAPI.getSobAttack(FacAPI.getChunkOwn(chunk)) == true) {
                                attack(event.getEntity().getLocation(), false, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), false);
                            } else {
                                attack(event.getEntity().getLocation(), true, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), false);
                            }
                        }
                    } else if (TntLevelUtil.get(event.getEntity().getCustomName()) != null && TntLevelUtil.get(event.getEntity().getCustomName()).getLevel() == 3) {
                        final Random random = new Random();
                        final int r = random.nextInt(999999999);
                        final String pos1 = (event.getLocation().getBlockX() - 5) + ":" + (event.getLocation().getBlockY() - 5) + ":" + (event.getLocation().getBlockZ() - 5);
                        final String pos2 = (event.getLocation().getBlockX() + 5) + ":" + (event.getLocation().getBlockY() + 5) + ":" + (event.getLocation().getBlockZ() + 5);
                        final RadiationHash rg = new RadiationHash(String.valueOf(r), pos1, pos2);
                        RadiationRgs.getRegions().add(rg);

                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                RadiationHash.CACHE.remove(String.valueOf(r));
                            }
                        }, 12000L);

                        final String chunk = event.getEntity().getChunk().getX() + ":" + event.getEntity().getChunk().getZ();
                        if (FacAPI.getChunkOwn(chunk) != null) {
                            if (FacAPI.getSobAttack(FacAPI.getChunkOwn(chunk)) == true) {
                                attack(event.getEntity().getLocation(), false, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), false);
                            } else {
                                attack(event.getEntity().getLocation(), true, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), false);
                            }
                        }
                    } else if (TntLevelUtil.get(event.getEntity().getCustomName()) != null && TntLevelUtil.get(event.getEntity().getCustomName()).getLevel() == 5) {
                        final Random random = new Random();
                        final int r = random.nextInt(999999999);
                        final String pos1 = (event.getLocation().getBlockX() - 10) + ":" + (event.getLocation().getBlockY() - 10) + ":" + (event.getLocation().getBlockZ() - 10);
                        final String pos2 = (event.getLocation().getBlockX() + 10) + ":" + (event.getLocation().getBlockY() + 10) + ":" + (event.getLocation().getBlockZ() + 10);
                        final RadiationHash rg = new RadiationHash(String.valueOf(r), pos1, pos2);
                        RadiationRgs.getRegions().add(rg);

                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                RadiationHash.CACHE.remove(String.valueOf(r));
                            }
                        }, 24000L);

                        final String chunk = event.getEntity().getChunk().getX() + ":" + event.getEntity().getChunk().getZ();
                        if (FacAPI.getChunkOwn(chunk) != null) {
                            if (FacAPI.getSobAttack(FacAPI.getChunkOwn(chunk)) == true) {
                                attack(event.getEntity().getLocation(), false, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), false);
                            } else {
                                attack(event.getEntity().getLocation(), true, FacAPI.getChunkOwn(chunk), event.getEntity().getMetadata("tnt-owner").get(0).asString(), false);
                            }
                        }
                    } else {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onBreak(final BlockBreakEvent event) {
        final Player player = event.getPlayer();

        if (event.getBlock() != null && event.getBlock().getType().equals(Material.TNT)) {
            final String loc = event.getBlock().getX() + ":" + event.getBlock().getY() + ":" + event.getBlock().getZ();
            if (FortificationAPI.getTNTLevel(loc) != 0) {
                if (FortificationAPI.getTNTLevel(loc) == 1) {
                    final ItemStack tntComum = new ItemStack(Material.TNT);
                    final ItemMeta meta5 = tntComum.getItemMeta();
                    meta5.setDisplayName(ChatColor.RED + "Explosivo comum");
                    final List<String> lore4 = new ArrayList<>();
                    lore4.add(" ");
                    lore4.add(ChatColor.GRAY + "Dano: 1/5");
                    lore4.add(" ");
                    meta5.setLore(lore4);
                    tntComum.setItemMeta(meta5);
                    final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(tntComum);
                    for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                        player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                    }
                    event.setDropItems(false);
                }

                if (FortificationAPI.getTNTLevel(loc) == 3) {
                    final ItemStack tntMedia = new ItemStack(Material.TNT);
                    final ItemMeta meta1 = tntMedia.getItemMeta();
                    meta1.setDisplayName(ChatColor.RED + "Explosivo nuclear");
                    final List<String> lore1 = new ArrayList<>();
                    lore1.add(" ");
                    lore1.add(ChatColor.GRAY + "Dano: 3/5");
                    lore1.add(" ");
                    meta1.setLore(lore1);
                    tntMedia.setItemMeta(meta1);
                    final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(tntMedia);
                    for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                        player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                    }
                    event.setDropItems(false);
                }

                if (FortificationAPI.getTNTLevel(loc) == 5) {
                    final ItemStack tntH = new ItemStack(Material.TNT);
                    final ItemMeta meta2 = tntH.getItemMeta();
                    meta2.setDisplayName(ChatColor.RED + "Explosivo de hidrogênio");
                    final List<String> lore2 = new ArrayList<>();
                    lore2.add(" ");
                    lore2.add(ChatColor.GRAY + "Dano: 5/5");
                    lore2.add(" ");
                    meta2.setLore(lore2);
                    tntH.setItemMeta(meta2);
                    final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(CommandTeste.addGlow(tntH));
                    for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                        player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                    }
                    event.setDropItems(false);
                }

                if (FortificationAPI.getTNTLevel(loc) > 100) {
                    final ItemStack tntRepulsao = new ItemStack(Material.TNT);
                    final ItemMeta meta3 = tntRepulsao.getItemMeta();
                    meta3.setDisplayName(ChatColor.RED + "Explosivo de repulsão");
                    tntRepulsao.setItemMeta(meta3);
                    final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(CommandTeste.addGlow(tntRepulsao));
                    for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                        player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                    }
                    event.setDropItems(false);
                }
                FortificationAPI.removeTNTInfo(loc);
            }
        }
    }

    @EventHandler
    public void onPlace(final BlockPlaceEvent event) {
        final Player player = event.getPlayer();

        if (event.getBlock().getType() == Material.REDSTONE_BLOCK || event.getBlock().getType() == Material.REDSTONE_TORCH) {
            if (event.getBlockPlaced().getRelative(BlockFace.EAST).getType().equals(Material.TNT)) {
                final String owner = event.getBlockPlaced().getRelative(BlockFace.EAST).getMetadata("tnt-owner").get(0).asString();
                event.getBlockPlaced().getRelative(BlockFace.EAST).setType(Material.AIR);

                final Random random = new Random();
                final String r = String.valueOf(random.nextInt(10000000));

                final String loc = event.getBlock().getRelative(BlockFace.EAST).getX() + ":" + event.getBlock().getRelative(BlockFace.EAST).getY() + ":" + event.getBlock().getRelative(BlockFace.EAST).getZ();
                if (FortificationAPI.getTNTLevel(loc) != 0) {
                    if (TntLevelUtil.get(r) == null) {
                        new TntLevelUtil(r, FortificationAPI.getTNTLevel(loc)).insert();
                    } else {
                        TntLevelUtil.get(r).setLevel(FortificationAPI.getTNTLevel(loc));
                    }
                    FortificationAPI.removeTNTInfo(loc);
                } else {
                    return;
                }

                final Entity tnt = event.getBlock().getRelative(BlockFace.EAST).getWorld().spawn(event.getBlock().getRelative(BlockFace.EAST).getLocation().add(0, 1, 0), TNTPrimed.class);
                ((TNTPrimed) tnt).setFuseTicks(90);
                tnt.setCustomName(r);
                tnt.setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), owner));
                tnt.setCustomNameVisible(false);
            } else if (event.getBlockPlaced().getRelative(BlockFace.WEST).getType().equals(Material.TNT)) {
                final String owner = event.getBlockPlaced().getRelative(BlockFace.WEST).getMetadata("tnt-owner").get(0).asString();
                event.getBlockPlaced().getRelative(BlockFace.WEST).setType(Material.AIR);

                final Random random = new Random();
                final String r = String.valueOf(random.nextInt(10000000));

                final String loc = event.getBlock().getRelative(BlockFace.WEST).getX() + ":" + event.getBlock().getRelative(BlockFace.WEST).getY() + ":" + event.getBlock().getRelative(BlockFace.WEST).getZ();
                if (FortificationAPI.getTNTLevel(loc) != 0) {
                    if (TntLevelUtil.get(r) == null) {
                        new TntLevelUtil(r, FortificationAPI.getTNTLevel(loc)).insert();
                    } else {
                        TntLevelUtil.get(r).setLevel(FortificationAPI.getTNTLevel(loc));
                    }
                    FortificationAPI.removeTNTInfo(loc);
                } else {
                    return;
                }

                final Entity tnt = event.getBlock().getRelative(BlockFace.WEST).getWorld().spawn(event.getBlock().getRelative(BlockFace.WEST).getLocation().add(0, 1, 0), TNTPrimed.class);
                ((TNTPrimed) tnt).setFuseTicks(90);
                tnt.setCustomName(r);
                tnt.setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), owner));
                tnt.setCustomNameVisible(false);
            } else if (event.getBlockPlaced().getRelative(BlockFace.SOUTH).getType().equals(Material.TNT)) {
                final String owner = event.getBlockPlaced().getRelative(BlockFace.SOUTH).getMetadata("tnt-owner").get(0).asString();
                event.getBlockPlaced().getRelative(BlockFace.SOUTH).setType(Material.AIR);

                final Random random = new Random();
                final String r = String.valueOf(random.nextInt(10000000));

                final String loc = event.getBlock().getRelative(BlockFace.SOUTH).getX() + ":" + event.getBlock().getRelative(BlockFace.SOUTH).getY() + ":" + event.getBlock().getRelative(BlockFace.SOUTH).getZ();
                if (FortificationAPI.getTNTLevel(loc) != 0) {
                    if (TntLevelUtil.get(r) == null) {
                        new TntLevelUtil(r, FortificationAPI.getTNTLevel(loc)).insert();
                    } else {
                        TntLevelUtil.get(r).setLevel(FortificationAPI.getTNTLevel(loc));
                    }
                    FortificationAPI.removeTNTInfo(loc);
                } else {
                    return;
                }

                final Entity tnt = event.getBlock().getRelative(BlockFace.SOUTH).getWorld().spawn(event.getBlock().getRelative(BlockFace.SOUTH).getLocation().add(0, 1, 0), TNTPrimed.class);
                ((TNTPrimed) tnt).setFuseTicks(90);
                tnt.setCustomName(r);
                tnt.setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), owner));
                tnt.setCustomNameVisible(false);
            } else if (event.getBlockPlaced().getRelative(BlockFace.NORTH).getType().equals(Material.TNT)) {
                final String owner = event.getBlockPlaced().getRelative(BlockFace.NORTH).getMetadata("tnt-owner").get(0).asString();
                event.getBlockPlaced().getRelative(BlockFace.NORTH).setType(Material.AIR);

                final Random random = new Random();
                final String r = String.valueOf(random.nextInt(10000000));

                final String loc = event.getBlock().getRelative(BlockFace.NORTH).getX() + ":" + event.getBlock().getRelative(BlockFace.NORTH).getY() + ":" + event.getBlock().getRelative(BlockFace.NORTH).getZ();
                if (FortificationAPI.getTNTLevel(loc) != 0) {
                    if (TntLevelUtil.get(r) == null) {
                        new TntLevelUtil(r, FortificationAPI.getTNTLevel(loc)).insert();
                    } else {
                        TntLevelUtil.get(r).setLevel(FortificationAPI.getTNTLevel(loc));
                    }
                    FortificationAPI.removeTNTInfo(loc);
                } else {
                    return;
                }

                final Entity tnt = event.getBlock().getRelative(BlockFace.NORTH).getWorld().spawn(event.getBlock().getRelative(BlockFace.NORTH).getLocation().add(0, 1, 0), TNTPrimed.class);
                ((TNTPrimed) tnt).setFuseTicks(90);
                tnt.setCustomName(r);
                tnt.setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), owner));
                tnt.setCustomNameVisible(false);
            }
        }

        if (event.getBlockPlaced().getType().equals(Material.TNT)) {
            final String loc = event.getBlockPlaced().getX() + ":" + event.getBlockPlaced().getY() + ":" + event.getBlockPlaced().getZ();
            if (event.getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.RED + "Explosivo de repulsão")) {
                FortificationAPI.createTNTInfo(loc, 1000);
                event.getBlockPlaced().setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), FacAPI.getFacNome(player.getUniqueId().toString())));
            } else {
                if (event.getItemInHand().getItemMeta().getLore().get(1).equalsIgnoreCase(ChatColor.GRAY + "Dano: 1/5")) {
                    FortificationAPI.createTNTInfo(loc, 1);
                    event.getBlockPlaced().setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), FacAPI.getFacNome(player.getUniqueId().toString())));
                }
                if (event.getItemInHand().getItemMeta().getLore().get(1).equalsIgnoreCase(ChatColor.GRAY + "Dano: 3/5")) {
                    FortificationAPI.createTNTInfo(loc, 3);
                    event.getBlockPlaced().setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), FacAPI.getFacNome(player.getUniqueId().toString())));
                }
                if (event.getItemInHand().getItemMeta().getLore().get(1).equalsIgnoreCase(ChatColor.GRAY + "Dano: 5/5")) {
                    FortificationAPI.createTNTInfo(loc, 5);
                    event.getBlockPlaced().setMetadata("tnt-owner", new FixedMetadataValue(Main.getInstance(), FacAPI.getFacNome(player.getUniqueId().toString())));
                }
            }
        }

        if (event.getBlockPlaced().getType().equals(Material.DISPENSER)) {
            final Dispenser dispenser = (Dispenser) event.getBlockPlaced().getState();
            dispenser.setCustomName(FacAPI.getFacNome(player.getUniqueId().toString()));
            dispenser.update();
        }

        if (event.getBlockPlaced().getType().equals(Material.SMOOTH_STONE) || event.getBlockPlaced().getType().equals(Material.END_STONE)) {
            if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.GREEN + "Bloco de fortificação")) {
                if (event.getBlockPlaced().getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
                    try {
                        final String loc = event.getBlockPlaced().getLocation().getBlockX() + ":" + event.getBlockPlaced().getLocation().getBlockY() + ":" + event.getBlockPlaced().getLocation().getBlockZ();
                        if (event.getBlockPlaced().getType().equals(Material.SMOOTH_STONE)) {
                            FortificationAPI.createBlock(loc, 1, 2);
                        } else if (event.getBlockPlaced().getType().equals(Material.END_STONE)) {
                            FortificationAPI.createBlock(loc, 1, 1);
                        }
                    } catch (final NumberFormatException e) {
                        //-
                    }
                } else {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onJoin(final PlayerJoinEvent event) {
        final Player player = event.getPlayer();
        final String UUID = player.getUniqueId().toString();

        if (FacAPI.getFacNome(UUID) != null && LifeNexusCache.get(FacAPI.getFacNome(UUID)) != null) {
            LifeNexusCache.get(FacAPI.getFacNome(UUID)).getBossBar().addPlayer(player);
        }
    }

    @EventHandler
    public void onQuit(final PlayerQuitEvent event) {
        final Player player = event.getPlayer();
        final String UUID = player.getUniqueId().toString();

        if (FacAPI.getFacNome(UUID) != null && LifeNexusCache.get(FacAPI.getFacNome(UUID)) != null) {
            LifeNexusCache.get(FacAPI.getFacNome(UUID)).getBossBar().removePlayer(player);
        }
    }

}
