package com.mixedup.factions.listeners;

import com.mixedup.Main;
import com.mixedup.apis.AccountAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.factions.CommandsFac;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.hashs.NexusHash;
import com.mixedup.utils.CentralizeMsg;
import com.mixedup.utils.WorldEditUtil;
import com.sk89q.worldedit.WorldEditException;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryExclusao implements Listener {

    @EventHandler
    public void onInteract(final InventoryClickEvent event) {

        Player player = (Player) event.getWhoClicked();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getInventory().getName().equalsIgnoreCase("Confirmar exclusão esquadrão: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 12) {
                String lista = null;
                final String nome = FacAPI.getNome(UUID);
                if (!FacAPI.getRecrutas(nome).equals("NULL")) {
                    lista = FacAPI.getRecrutas(nome);
                }
                if (!FacAPI.getMembros(nome).equals("NULL")) {
                    if (lista == null) {
                        lista = FacAPI.getMembros(nome);
                    } else {
                        lista = lista + ":" + FacAPI.getMembros(nome);
                    }
                }
                if (!FacAPI.getCapitoes(nome).equals("NULL")) {
                    if (lista == null) {
                        lista = FacAPI.getRecrutas(nome);
                    } else {
                        lista = lista + ":" + FacAPI.getCapitoes(nome);
                    }
                }
                if (!FacAPI.getLideres(nome).equals("NULL")) {
                    if (lista == null) {
                        lista = FacAPI.getLideres(nome);
                    } else {
                        lista = lista + ":" + FacAPI.getLideres(nome);
                    }
                }

                if (!FacAPI.getNexus(FacAPI.getFacNome(UUID)).equalsIgnoreCase("NULL")) {
                    if (FacAPI.getTerras(FacAPI.getFacNome(UUID)) > 0) {
                        FacAPI.updateTerras(FacAPI.getFacNome(UUID), 0);

                        final String[] split = FacAPI.getNexus(FacAPI.getFacNome(UUID)).split("::");
                        final String[] pos1 = split[0].split(":");
                        final Location loc1 = new Location(Bukkit.getWorld("Trappist-1b"), Integer.valueOf(pos1[0]), Integer.valueOf(pos1[1]), Integer.valueOf(pos1[2]));
                        final String[] pos2 = split[1].split(":");
                        final Location loc2 = new Location(Bukkit.getWorld("Trappist-1b"), Integer.valueOf(pos2[0]), Integer.valueOf(pos2[1]), Integer.valueOf(pos2[2]));

                        try {
                            WorldEditUtil.clear(WorldEditUtil.copy(WorldEditUtil.getRegion(loc1, loc2)));
                            FacAPI.updateNexus(FacAPI.getFacNome(UUID), "NULL");
                            CommandsFac.removeAllChunks(FacAPI.getFacNome(UUID));

                            NexusHash.get(FacAPI.getFacNome(UUID)).setPos1("NULL");
                            NexusHash.get(FacAPI.getFacNome(UUID)).setPos2("NULL");
                            NexusHash.get(FacAPI.getFacNome(UUID)).setSquad("NULL");
                        } catch (final WorldEditException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    if (FacAPI.getTerras(FacAPI.getFacNome(UUID)) > 0) {
                        FacAPI.updateTerras(FacAPI.getFacNome(UUID), 0);
                        CommandsFac.removeAllChunks(FacAPI.getFacNome(UUID));
                    }
                }

                final String facName = FacAPI.getNome(UUID);
                if (lista.contains(":")) {
                    final String[] list = lista.split(":");

                    for (int i = 1; i <= list.length; i++) {
                        FacAPI.deletePlayerInfo(list[i - 1]);
                        FacAPI.removerPlayerPerms(list[i - 1]);
                        final Player target = Bukkit.getPlayerExact(AccountAPI.getNick(list[i - 1]));

                        if (target != null) {
                            if (Main.scoreboardManager.hasSquadScoreboard(target)) {
                                Main.scoreboardManager.getSquadScoreboard().updateEntry(target, "squad_name");
                                Main.scoreboardManager.getSquadScoreboard().updateEntry(target, "squad_online");
                                Main.scoreboardManager.getSquadScoreboard().updateEntry(target, "squad_lands");
                                Main.scoreboardManager.getSquadScoreboard().updateEntry(target, "squad_power");
                            } else {
                                Main.scoreboardManager.getDefaultScoreboard().updateEntry(target, "squad");
                            }
                        }

                        if (player != null && target != null && !player.getName().equals(target.getName())) {
                            if (target != null && target.isOnline()) {
                                final String line1 = CentralizeMsg.sendCenteredMessage(ChatColor.YELLOW + "Seu esquadrão acaba de ser deletado");
                                final String line2 = CentralizeMsg.sendCenteredMessage(ChatColor.DARK_GRAY + "Por: " + ChatColor.GRAY + player.getName());
                                target.sendMessage("\n" + line1 + "\n" + line2 + "\n ");
                                target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else {
                            final String line1 = CentralizeMsg.sendCenteredMessage(ChatColor.YELLOW + "Esquadrão deletado com sucesso.");
                            player.sendMessage("\n" + line1 + "\n ");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    }
                    FacAPI.deleteFac(FacAPI.getNome(UUID));
                    player.getOpenInventory().close();
                } else {
                    FacAPI.deletePlayerInfo(lista);
                    FacAPI.removerPlayerPerms(lista);
                    FacAPI.deleteFac(facName);
                    final String line1 = CentralizeMsg.sendCenteredMessage(ChatColor.YELLOW + "Esquadrão deletado com sucesso.");
                    player.sendMessage("\n" + line1 + "\n ");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.getOpenInventory().close();
                }
            }
            if (event.getSlot() == 14) {
                player.getOpenInventory().close();
                player.sendMessage(ChatColor.RED + " * Evento cancelado.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
    }
}
