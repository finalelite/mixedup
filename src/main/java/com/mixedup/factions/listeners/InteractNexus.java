package com.mixedup.factions.listeners;

import com.mixedup.Main;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.hashs.NexusUtil;
import com.mixedup.factions.hashs.TypeNexusUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class InteractNexus implements Listener {

    private static Inventory invNexus(final String UUID, final String chunk) {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Nexus (Gerador de plutônio): ");

        final ItemStack plutonio = new ItemStack(Material.EMERALD);
        final ItemMeta metaPlu = plutonio.getItemMeta();
        metaPlu.setDisplayName(ChatColor.GREEN + "Plutônio");
        final ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.GRAY + String.valueOf(FacAPI.getQuantiaPlutonio(FacAPI.getChunkOwn(chunk))) + " minério(s)");
        lore.add(" ");
        metaPlu.setLore(lore);
        plutonio.setItemMeta(metaPlu);
        inventory.setItem(11, plutonio);

        if (TypeNexusUtil.get(UUID) == null) {
            new TypeNexusUtil(UUID, "minerio").insert();
        }

        final ItemStack troca = new ItemStack(Material.MUSIC_DISC_FAR);
        final ItemMeta metaTroca = troca.getItemMeta();
        metaTroca.setDisplayName(ChatColor.YELLOW + "Conversão");
        metaTroca.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        metaTroca.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        metaTroca.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        metaTroca.addItemFlags(ItemFlag.HIDE_DESTROYS);
        metaTroca.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        metaTroca.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        final ArrayList<String> lore1 = new ArrayList<>();
        lore1.add(" ");
        if (TypeNexusUtil.get(UUID).gettype().equals("minerio")) {
            lore1.add(ChatColor.GRAY + "Tipo: minério");
        } else {
            lore1.add(ChatColor.GRAY + "Tipo: energia");
        }
        lore1.add(" ");
        metaTroca.setLore(lore1);
        troca.setItemMeta(metaTroca);
        inventory.setItem(14, troca);

        final ItemStack confirm = new ItemStack(Material.MUSIC_DISC_CHIRP);
        final ItemMeta metaConf = confirm.getItemMeta();
        metaConf.setDisplayName(ChatColor.YELLOW + "Confirmar");
        metaConf.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        metaConf.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        metaConf.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        metaConf.addItemFlags(ItemFlag.HIDE_DESTROYS);
        metaConf.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        metaConf.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        final ArrayList<String> lore2 = new ArrayList<>();
        lore2.add(" ");
        if (TypeNexusUtil.get(UUID).gettype().equals("minerio")) {
            lore2.add(ChatColor.GRAY + "Ao confirmar, " + FacAPI.getQuantiaPlutonio(FacAPI.getChunkOwn(chunk)) + " plutônios serão removidos e cedidos.");
        } else {
            lore2.add(ChatColor.GRAY + "Ao confirmar, " + FacAPI.getQuantiaPlutonio(FacAPI.getChunkOwn(chunk)) + " plutônios serão convertidos de minério à energia");
            lore2.add(ChatColor.GRAY + "e dividido entre todo o esquadrão.");
        }
        lore2.add(" ");
        metaConf.setLore(lore2);
        confirm.setItemMeta(metaConf);
        inventory.setItem(15, confirm);

        return inventory;
    }

    @EventHandler
    public void entityInteractNexus(final EntityInteractEvent event) {
        if (event.getEntity().getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
            if (NexusUtil.playerInArea(event.getEntity().getLocation()) != null) {
                if (event.getBlock() != null) {
                    final Material mat = event.getBlock().getType();
                    if (mat == Material.ACACIA_PRESSURE_PLATE || mat == Material.BIRCH_PRESSURE_PLATE || mat == Material.DARK_OAK_PRESSURE_PLATE || mat == Material.HEAVY_WEIGHTED_PRESSURE_PLATE || mat == Material.JUNGLE_PRESSURE_PLATE || mat == Material.LIGHT_WEIGHTED_PRESSURE_PLATE
                            || mat == Material.OAK_PRESSURE_PLATE || mat == Material.SPRUCE_PRESSURE_PLATE || mat == Material.STONE_PRESSURE_PLATE) {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onInteractNexus(final PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && event.getClickedBlock() != null && event.getClickedBlock().getType().equals(Material.EMERALD_BLOCK)) {
            if (NexusUtil.playerInArea(event.getClickedBlock().getLocation()) != null) {
                if (!event.getClickedBlock().getRelative(BlockFace.EAST).getType().equals(Material.EMERALD_BLOCK) && !event.getClickedBlock().getRelative(BlockFace.WEST).getType().equals(Material.EMERALD_BLOCK) &&
                        !event.getClickedBlock().getRelative(BlockFace.SOUTH).getType().equals(Material.EMERALD_BLOCK) && !event.getClickedBlock().getRelative(BlockFace.NORTH).getType().equals(Material.EMERALD_BLOCK)) {
                    player.openInventory(InteractNexus.invNexus(UUID, event.getClickedBlock().getChunk().getX() + ":" + event.getClickedBlock().getChunk().getZ()));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }

        if (event.getAction().equals(Action.PHYSICAL)) {
            final Material mat = event.getClickedBlock().getType();
            if (mat == Material.ACACIA_PRESSURE_PLATE || mat == Material.BIRCH_PRESSURE_PLATE || mat == Material.DARK_OAK_PRESSURE_PLATE || mat == Material.HEAVY_WEIGHTED_PRESSURE_PLATE || mat == Material.JUNGLE_PRESSURE_PLATE || mat == Material.LIGHT_WEIGHTED_PRESSURE_PLATE
                    || mat == Material.OAK_PRESSURE_PLATE || mat == Material.SPRUCE_PRESSURE_PLATE || mat == Material.STONE_PRESSURE_PLATE) {
                if (event.getPlayer().getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
                    final String chunk = event.getPlayer().getChunk().getX() + ":" + event.getPlayer().getChunk().getZ();

                    if (FacAPI.getChunkOwn(chunk) != null) {
                        if (FacAPI.getFacNome(UUID) != null) {
                            if (!FacAPI.getFacNome(UUID).equalsIgnoreCase(FacAPI.getChunkOwn(chunk))) {
                                event.setCancelled(true);
                                event.setUseInteractedBlock(Result.DENY);
                            }
                        } else {
                            event.setCancelled(true);
                            event.setUseInteractedBlock(Result.DENY);
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onInteractEntityNexus(final EntityInteractEvent event) {
        final Material mat = event.getBlock().getType();
        if (event.getEntity().getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
            if (mat == Material.ACACIA_PRESSURE_PLATE || mat == Material.BIRCH_PRESSURE_PLATE || mat == Material.DARK_OAK_PRESSURE_PLATE || mat == Material.HEAVY_WEIGHTED_PRESSURE_PLATE || mat == Material.JUNGLE_PRESSURE_PLATE || mat == Material.LIGHT_WEIGHTED_PRESSURE_PLATE
                    || mat == Material.OAK_PRESSURE_PLATE || mat == Material.SPRUCE_PRESSURE_PLATE || mat == Material.STONE_PRESSURE_PLATE) {
                final String chunk = event.getEntity().getChunk().getX() + ":" + event.getEntity().getChunk().getZ();

                if (FacAPI.getChunkOwn(chunk) != null) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onClick(final InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getInventory().getName().equalsIgnoreCase("Nexus (Gerador de plutônio): ")) {
            event.setCancelled(true);

            if (event.getSlot() == 15) {
                //-
            }

            if (event.getSlot() == 14) {
                if (TypeNexusUtil.get(UUID).gettype().equalsIgnoreCase("minerio")) {
                    TypeNexusUtil.get(UUID).settype("energia");
                } else {
                    TypeNexusUtil.get(UUID).settype("minerio");
                }

                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        final String chunk = player.getChunk().getX() + ":" + player.getChunk().getZ();
                        player.openInventory(InteractNexus.invNexus(UUID, chunk));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                }, 5L);
            }
        }
    }

    @EventHandler
    public void onBreak(final BlockBreakEvent event) {
        final Player player = event.getPlayer();

        if (NexusUtil.playerInArea(event.getBlock().getLocation()) != null && event.getBlock().getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
            player.sendMessage(ChatColor.RED + "\n (!) Zona do nexus, você não pode mexer nesta área.\n ");
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onBreak(final BlockPlaceEvent event) {
        final Player player = event.getPlayer();

        if (NexusUtil.playerInArea(event.getBlock().getLocation()) != null && event.getBlock().getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
            player.sendMessage(ChatColor.RED + "\n (!) Zona do nexus, você não pode mexer nesta área.\n ");
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            event.setCancelled(true);
        }
    }
}
