package com.mixedup.factions.listeners;

import com.mixedup.apis.PoderAPI;
import com.mixedup.utils.CriarItem;
import com.mixedup.utils.RemoveItemInv;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class PoderEvent implements Listener {

    @EventHandler
    public void onCraft(final PrepareItemCraftEvent event) {
        if (event.getInventory() != null) {
            final ItemStack[] itens = event.getInventory().getMatrix();
            for (final ItemStack item : itens) {
                if (item != null && item.getType() != null && item.getType().equals(Material.NETHER_STAR)) {
                    if (item.getItemMeta().getDisplayName().contains(ChatColor.GREEN + "Poder")) {
                        event.getInventory().setResult(new ItemStack(Material.AIR));
                    }
                }
            }
        }
    }

    @EventHandler
    public void onInteract(final PlayerInteractEvent event) {
        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) || event.getAction().equals(Action.RIGHT_CLICK_AIR)) {
            if (event.getPlayer().getItemInHand() != null && !event.getPlayer().getItemInHand().getType().equals(Material.AIR)) {
                if (event.getPlayer().getItemInHand().getType().equals(Material.NETHER_STAR)) {
                    if (event.getPlayer().getItemInHand().getItemMeta().getDisplayName() != null && event.getPlayer().getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GREEN + "Poder máximo")) {
                        final Player player = event.getPlayer();
                        final String UUID = player.getUniqueId().toString();

                        if (PoderAPI.getPoder(UUID) == 15) {
                            player.sendMessage(ChatColor.RED + " * Ops, você já se encontra com poder máximo!");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            return;
                        }

                        final ItemStack poder = CriarItem.add(Material.NETHER_STAR, ChatColor.GREEN + "Poder máximo", 1);

                        PoderAPI.updatePoder(UUID, 15);
                        player.sendMessage(ChatColor.GREEN + " * Poder setado ao nível máximo!");
                        player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                        RemoveItemInv.removeItems(player.getInventory(), poder, 1);
                        player.getWorld().spawnEntity(player.getLocation(), EntityType.FIREWORK);
                    }
                }
            }
        }
    }
}
