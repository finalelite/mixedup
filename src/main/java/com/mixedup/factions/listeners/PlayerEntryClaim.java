package com.mixedup.factions.listeners;

import com.mixedup.Main;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.hashs.LastChunkMoved;
import com.mixedup.factions.hashs.PlayerInZone;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerEntryClaim implements Listener {

    @EventHandler
    public void onEntry(final PlayerMoveEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (player.getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
            final String chunk = player.getLocation().getChunk().getX() + ":" + player.getLocation().getChunk().getZ();

            if (LastChunkMoved.get(UUID) == null) {
                new LastChunkMoved(UUID, chunk).insert();
            } else {
                if (!LastChunkMoved.get(UUID).getLastChunk().equalsIgnoreCase(chunk)) {
                    LastChunkMoved.get(UUID).setLastChunk(chunk);
                    Main.scoreboardManager.updateSquadScoreboardTitle(player);

                    if (FacAPI.getChunkOwn(chunk) != null) {
                        if (PlayerInZone.get(UUID) == null) {
                            new PlayerInZone(UUID, FacAPI.getChunkOwn(chunk)).insert();
                        } else {
                            if (FacAPI.getChunkOwn(chunk) != null && PlayerInZone.get(UUID).getZone().equalsIgnoreCase(FacAPI.getChunkOwn(chunk))) {
                                return;
                            } else {
                                PlayerInZone.get(UUID).setZone(FacAPI.getChunkOwn(chunk));
                            }
                        }
                    } else {
                        if (PlayerInZone.get(UUID) == null) {
                            new PlayerInZone(UUID, "NULL").insert();
                        } else {
                            if (!PlayerInZone.get(UUID).getZone().equals("NULL")) {
                                player.sendTitle(ChatColor.GREEN + "ZONA LIVRE", "", 20, 40, 20);
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                            PlayerInZone.get(UUID).setZone("NULL");
                        }
                    }

                    if (FacAPI.getChunkOwn(chunk) != null) {
                        if (FacAPI.getChunkOwn(chunk).equalsIgnoreCase(FacAPI.getFacNome(UUID))) {
                            player.sendTitle(ChatColor.GREEN + "ZONA SEGURA", ChatColor.GRAY + "[" + FacAPI.getTagWithNome(FacAPI.getChunkOwn(chunk)) + "] " + FacAPI.getChunkOwn(chunk), 20, 40, 20);
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else {
                            if (FacAPI.getFacNome(UUID) == null) return;
                            if (FacAPI.getAmigos(FacAPI.getFacNome(UUID)) != null && !FacAPI.getAmigos(FacAPI.getFacNome(UUID)).equals("NULL")) {
                                if (FacAPI.getAmigos(FacAPI.getFacNome(UUID)).contains(":")) {
                                    final String[] split = FacAPI.getAmigos(FacAPI.getFacNome(UUID)).split(":");

                                    for (int i = 1; i <= split.length; i++) {
                                        if (split[i - 1].equalsIgnoreCase(FacAPI.getChunkOwn(chunk))) {
                                            player.sendTitle(ChatColor.BLUE + "ZONA ALIADA", ChatColor.GRAY + "[" + FacAPI.getTagWithNome(FacAPI.getChunkOwn(chunk)) + "] " + FacAPI.getChunkOwn(chunk), 20, 40, 20);
                                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        }
                                    }
                                } else {
                                    if (FacAPI.getAmigos(FacAPI.getFacNome(UUID)).equalsIgnoreCase(FacAPI.getChunkOwn(chunk))) {
                                        player.sendTitle(ChatColor.BLUE + "ZONA ALIADA", ChatColor.GRAY + "[" + FacAPI.getTagWithNome(FacAPI.getChunkOwn(chunk)) + "] " + FacAPI.getChunkOwn(chunk), 20, 40, 20);
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    }
                                }
                            } else {
                                if (FacAPI.getRivais(FacAPI.getFacNome(UUID)) != null && !FacAPI.getRivais(FacAPI.getFacNome(UUID)).equals("NULL")) {
                                    if (FacAPI.getRivais(FacAPI.getFacNome(UUID)).contains(":")) {
                                        final String[] split = FacAPI.getRivais(FacAPI.getFacNome(UUID)).split(":");

                                        for (int i = 1; i <= split.length; i++) {
                                            if (split[i - 1].equalsIgnoreCase(FacAPI.getChunkOwn(chunk))) {
                                                player.sendTitle(ChatColor.RED + "ZONA DE RISCO", ChatColor.GRAY + "[" + FacAPI.getTagWithNome(FacAPI.getChunkOwn(chunk)) + "] " + FacAPI.getChunkOwn(chunk), 20, 40, 20);
                                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                            }
                                        }
                                    } else {
                                        if (FacAPI.getRivais(FacAPI.getFacNome(UUID)).equalsIgnoreCase(FacAPI.getChunkOwn(chunk))) {
                                            player.sendTitle(ChatColor.RED + "ZONA DE RISCO", ChatColor.GRAY + "[" + FacAPI.getTagWithNome(FacAPI.getChunkOwn(chunk)) + "] " + FacAPI.getChunkOwn(chunk), 20, 40, 20);
                                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        }
                                    }
                                } else {
                                    player.sendTitle(ChatColor.YELLOW + "ZONA PROTEGIDA", ChatColor.GRAY + "[" + FacAPI.getTagWithNome(FacAPI.getChunkOwn(chunk)) + "] " + FacAPI.getChunkOwn(chunk), 20, 40, 20);
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
