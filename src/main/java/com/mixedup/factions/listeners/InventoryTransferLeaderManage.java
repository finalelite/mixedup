package com.mixedup.factions.listeners;

import com.mixedup.apis.AccountAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.hashs.TransferLeaderProv;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryTransferLeaderManage implements Listener {

    @EventHandler
    public void onInteract(final InventoryClickEvent event) {

        Player player = (Player) event.getWhoClicked();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getInventory().getName().equalsIgnoreCase("Transferir liderança esquadrão: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 12) {
                final String facNome = FacAPI.getNome(UUID);

                final String hierarquia = FacAPI.getHierarquia(TransferLeaderProv.get(UUID).getUUIDtarget());
                if (hierarquia.equalsIgnoreCase("recruta")) {
                    if (FacAPI.getRecrutas(facNome).contains(":")) {
                        final String[] split = FacAPI.getRecrutas(facNome).split(":");
                        String newtxt = null;
                        for (int i = 1; i <= split.length; i++) {
                            if (!split[i - 1].equalsIgnoreCase(UUID)) {
                                if (newtxt == null) {
                                    newtxt = split[i - 1];
                                } else {
                                    newtxt = newtxt + ":" + split[i - 1];
                                }
                            }
                        }
                        FacAPI.updateRecrutas(facNome, newtxt);
                    } else {
                        FacAPI.updateRecrutas(facNome, "NULL");
                    }
                } else if (hierarquia.equalsIgnoreCase("membro")) {
                    if (FacAPI.getMembros(facNome).contains(":")) {
                        final String[] split = FacAPI.getMembros(facNome).split(":");
                        String newtxt = null;
                        for (int i = 1; i <= split.length; i++) {
                            if (!split[i - 1].equalsIgnoreCase(UUID)) {
                                if (newtxt == null) {
                                    newtxt = split[i - 1];
                                } else {
                                    newtxt = newtxt + ":" + split[i - 1];
                                }
                            }
                        }
                        FacAPI.updateMembros(facNome, newtxt);
                    } else {
                        FacAPI.updateMembros(facNome, "NULL");
                    }
                } else if (hierarquia.equalsIgnoreCase("capitão")) {
                    if (FacAPI.getCapitoes(facNome).contains(":")) {
                        final String[] split = FacAPI.getCapitoes(facNome).split(":");
                        String newtxt = null;
                        for (int i = 1; i <= split.length; i++) {
                            if (!split[i - 1].equalsIgnoreCase(UUID)) {
                                if (newtxt == null) {
                                    newtxt = split[i - 1];
                                } else {
                                    newtxt = newtxt + ":" + split[i - 1];
                                }
                            }
                        }
                        FacAPI.updateCapitoes(facNome, newtxt);
                    } else {
                        FacAPI.updateCapitoes(facNome, "NULL");
                    }
                }

                FacAPI.updateLideres(facNome, TransferLeaderProv.get(UUID).getUUIDtarget());
                FacAPI.updateCriador(facNome, TransferLeaderProv.get(UUID).getUUIDtarget());
                FacAPI.updateInfo(UUID, "Recruta");
                FacAPI.updateInfo(TransferLeaderProv.get(UUID).getUUIDtarget(), "Líder");
                if (!FacAPI.getRecrutas(facNome).equals("NULL")) {
                    FacAPI.updateRecrutas(facNome, FacAPI.getRecrutas(facNome) + ":" + UUID);
                } else {
                    FacAPI.updateRecrutas(facNome, UUID);
                }
                player.sendMessage(ChatColor.YELLOW + " * Liderança transferida para " + AccountAPI.getNick(TransferLeaderProv.get(UUID).getUUIDtarget()) + " com sucesso.\n" + ChatColor.DARK_GRAY + " * Você foi movido a recruta do esquadrão.");
                player.getOpenInventory().close();

                final Player target = Bukkit.getPlayerExact(AccountAPI.getNick(TransferLeaderProv.get(UUID).getUUIDtarget()));

                if (target != null && target.isOnline()) {
                    target.sendMessage(ChatColor.YELLOW + " * Você foi promovido a líder do esquadrão!");
                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
            if (event.getSlot() == 14) {
                player.getOpenInventory().close();
                player.sendMessage(ChatColor.RED + " * Evento cancelado.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
    }
}
