package com.mixedup.factions.listeners;

import com.mixedup.Main;
import com.mixedup.apis.AccountAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.hashs.PlayerPermHash;
import com.mixedup.factions.managers.InventorySquad;
import com.mixedup.utils.AlternateColor;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class InventoryInteractSquad implements Listener {

    @EventHandler
    public void onInteract(final InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getClickedInventory() != null && event.getClickedInventory().getName().contains("Aliados: ")) {
            if (event.getSlot() == 18) {
                //1
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventorySquad.getInventory(FacAPI.getFacNome(UUID)));
                    }
                }, 5L);
            }
        }

        if (event.getClickedInventory() != null && event.getClickedInventory().getName().contains("Permissões (Aliado: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 13) {
                if (FacAPI.getAliadoPermChests(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + PlayerPermHash.get(UUID).getMembro().substring(2)) == true) {
                    FacAPI.updateAliadoPermChests(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + PlayerPermHash.get(UUID).getMembro().substring(2), false);
                    player.sendMessage(ChatColor.GREEN + " * Permissão alterada com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    //2
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventorySquad.permsAliado(UUID));
                        }
                    }, 5L);
                } else {
                    FacAPI.updateAliadoPermChests(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + PlayerPermHash.get(UUID).getMembro().substring(2), true);
                    player.sendMessage(ChatColor.GREEN + " * Permissão alterada com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    //3
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventorySquad.permsAliado(UUID));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 14) {
                if (FacAPI.getAliadoPermAcesso(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + PlayerPermHash.get(UUID).getMembro().substring(2)) == true) {
                    FacAPI.updateAliadoPermAcesso(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + PlayerPermHash.get(UUID).getMembro().substring(2), false);
                    player.sendMessage(ChatColor.GREEN + " * Permissão alterada com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    //4
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventorySquad.permsAliado(UUID));
                        }
                    }, 5L);
                } else {
                    FacAPI.updateAliadoPermAcesso(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + PlayerPermHash.get(UUID).getMembro().substring(2), true);
                    player.sendMessage(ChatColor.GREEN + " * Permissão alterada com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    //5
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventorySquad.permsAliado(UUID));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 12) {
                if (FacAPI.getAliadoPermBuild(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + PlayerPermHash.get(UUID).getMembro().substring(2)) == true) {
                    FacAPI.updateAliadoPermBuild(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + PlayerPermHash.get(UUID).getMembro().substring(2), false);
                    player.sendMessage(ChatColor.GREEN + " * Permissão alterada com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    //6
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventorySquad.permsAliado(UUID));
                        }
                    }, 5L);
                } else {
                    FacAPI.updateAliadoPermBuild(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + PlayerPermHash.get(UUID).getMembro().substring(2), true);
                    player.sendMessage(ChatColor.GREEN + " * Permissão alterada com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    //7
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventorySquad.permsAliado(UUID));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 22) {
                //8
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventorySquad.invPermAliados(UUID));
                    }
                }, 5L);
            }
        }

        if (event.getClickedInventory() != null && event.getClickedInventory().getName().contains("Permissões (Aliados): ")) {
            event.setCancelled(true);

            if (event.getSlot() == 18) {
                //9
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventorySquad.invPerm());
                    }
                }, 5L);
            }

            if (event.getClickedInventory().getItem(event.getSlot()) != null && !event.getClickedInventory().getItem(event.getSlot()).getType().equals(Material.AIR) && !event.getClickedInventory().getItem(event.getSlot()).getType().equals(Material.ARROW)) {
                if (PlayerPermHash.get(UUID) == null) {
                    new PlayerPermHash(UUID, event.getClickedInventory().getItem(event.getSlot()).getItemMeta().getDisplayName()).insert();
                } else {
                    PlayerPermHash.get(UUID).setMembro(event.getClickedInventory().getItem(event.getSlot()).getItemMeta().getDisplayName());
                }

                //10
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventorySquad.permsAliado(UUID));
                    }
                }, 5L);
            }
        }

        if (event.getClickedInventory() != null && event.getClickedInventory().getName().contains("Permissões (Membro: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 16) {
                if (FacAPI.getPermSpawners(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro())) == true) {
                    FacAPI.updatePermSpawners(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro()), false);
                    player.sendMessage(ChatColor.GREEN + " * Permissão alterada com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    //11
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventorySquad.perms(UUID));
                        }
                    }, 5L);
                } else {
                    FacAPI.updatePermSpawners(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro()), true);
                    player.sendMessage(ChatColor.GREEN + " * Permissão alterada com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    //12
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventorySquad.perms(UUID));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 15) {
                if (FacAPI.getPermExpulsar(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro())) == true) {
                    FacAPI.updatePermExpulsar(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro()), false);
                    player.sendMessage(ChatColor.GREEN + " * Permissão alterada com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    //13
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventorySquad.perms(UUID));
                        }
                    }, 5L);
                } else {
                    FacAPI.updatePermExpulsar(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro()), true);
                    player.sendMessage(ChatColor.GREEN + " * Permissão alterada com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    //14
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventorySquad.perms(UUID));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 14) {
                if (FacAPI.getPermRecrutar(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro())) == true) {
                    FacAPI.updatePermRecrutar(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro()), false);
                    player.sendMessage(ChatColor.GREEN + " * Permissão alterada com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    //15
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventorySquad.perms(UUID));
                        }
                    }, 5L);
                } else {
                    FacAPI.updatePermRecrutar(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro()), true);
                    player.sendMessage(ChatColor.GREEN + " * Permissão alterada com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    //16
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventorySquad.perms(UUID));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 13) {
                if (FacAPI.getPermAbandonar(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro())) == true) {
                    FacAPI.updatePermAbandonar(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro()), false);
                    player.sendMessage(ChatColor.GREEN + " * Permissão alterada com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    //17
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventorySquad.perms(UUID));
                        }
                    }, 5L);
                } else {
                    FacAPI.updatePermAbandonar(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro()), true);
                    player.sendMessage(ChatColor.GREEN + " * Permissão alterada com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    //18
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventorySquad.perms(UUID));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 12) {
                if (FacAPI.getPermDominar(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro())) == true) {
                    FacAPI.updatePermDominar(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro()), false);
                    player.sendMessage(ChatColor.GREEN + " * Permissão alterada com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    //19
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventorySquad.perms(UUID));
                        }
                    }, 5L);
                } else {
                    FacAPI.updatePermDominar(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro()), true);
                    player.sendMessage(ChatColor.GREEN + " * Permissão alterada com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    //20
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventorySquad.perms(UUID));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 11) {
                if (FacAPI.getPermChests(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro())) == true) {
                    FacAPI.updatePermChests(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro()), false);
                    player.sendMessage(ChatColor.GREEN + " * Permissão alterada com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    //21
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventorySquad.perms(UUID));
                        }
                    }, 5L);
                } else {
                    FacAPI.updatePermChests(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro()), true);
                    player.sendMessage(ChatColor.GREEN + " * Permissão alterada com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    //22
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventorySquad.perms(UUID));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 10) {
                if (FacAPI.getPermBuild(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro())) == true) {
                    FacAPI.updatePermBuild(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro()), false);
                    player.sendMessage(ChatColor.GREEN + " * Permissão alterada com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    //23
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventorySquad.perms(UUID));
                        }
                    }, 5L);
                } else {
                    FacAPI.updatePermBuild(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro()), true);
                    player.sendMessage(ChatColor.GREEN + " * Permissão alterada com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    //24
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventorySquad.perms(UUID));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 22) {
                //25
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventorySquad.invPermMembros(UUID));
                    }
                }, 5L);
            }
        }

        if (event.getClickedInventory() != null && event.getClickedInventory().getName().equalsIgnoreCase("Permissões (Membros): ")) {
            event.setCancelled(true);

            if (event.getSlot() == 40) {
                //26
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventorySquad.invPerm());
                    }
                }, 5L);
            }

            if (event.getClickedInventory().getItem(event.getSlot()) != null && !event.getClickedInventory().getItem(event.getSlot()).getType().equals(Material.AIR) && event.getClickedInventory().getItem(event.getSlot()).getItemMeta().getDisplayName().contains("]")) {
                final String[] split = event.getClickedInventory().getItem(event.getSlot()).getItemMeta().getDisplayName().split("]");

                if (split[1].contains(player.getName())) {
                    player.sendMessage(ChatColor.RED + " * Ops, você não pode fazer isto D=");
                    return;
                }

                if (PlayerPermHash.get(UUID) == null) {
                    new PlayerPermHash(UUID, split[1]).insert();
                } else {
                    PlayerPermHash.get(UUID).setMembro(split[1]);
                }

                //27
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventorySquad.perms(UUID));
                    }
                }, 5L);
            }
        }

        if (event.getClickedInventory() != null && event.getClickedInventory().getName().contains("Permissões: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 14) {
                //28
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (FacAPI.getAliados(FacAPI.getFacNome(UUID)) != null && !FacAPI.getAliados(FacAPI.getFacNome(UUID)).equalsIgnoreCase("NULL")) {
                            player.openInventory(InventorySquad.invPermAliados(UUID));
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, você não contém aliados!");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    }
                }, 5L);
            }

            if (event.getSlot() == 31) {
                //29
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventorySquad.getInventory(FacAPI.getFacNome(UUID)));
                    }
                }, 5L);
            }

            if (event.getSlot() == 12) {
                //30
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventorySquad.invPermMembros(UUID));
                    }
                }, 5L);
            }
        }

        if (event.getClickedInventory() != null && event.getClickedInventory().getName().contains("Esquadrão: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 42) {
                final ItemStack confirmar = new ItemStack(Material.LIME_DYE);
                final ItemMeta meta = confirmar.getItemMeta();
                meta.setDisplayName(ChatColor.GREEN + "CONFIRMAR");
                confirmar.setItemMeta(meta);

                final ItemStack negar = new ItemStack(Material.ROSE_RED);
                final ItemMeta meta1 = negar.getItemMeta();
                meta1.setDisplayName(ChatColor.RED + "NEGAR");
                negar.setItemMeta(meta1);

                final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Confirmar exclusão esquadrão: ");
                inventory.setItem(12, confirmar);
                inventory.setItem(14, negar);

                //30
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(inventory);
                    }
                }, 5L);
            }

            if (event.getSlot() == 38) {
                if (FacAPI.getHierarquia(UUID).equalsIgnoreCase("Líder")) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventorySquad.invPerm());
                        }
                    }, 5L);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, o você não contém permissão para alterar as permissões.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                return;
            }

            if (event.getSlot() == 23) {
                event.setCancelled(true);
            }

            if (event.getSlot() == 22) {
                final Inventory inventory = Bukkit.createInventory(null, 5 * 9, "Inimigos: ");

                if (FacAPI.getInimigos(FacAPI.getFacNome(UUID)) != null && !FacAPI.getInimigos(FacAPI.getFacNome(UUID)).equalsIgnoreCase("NULL")) {
                    if (FacAPI.getInimigos(FacAPI.getFacNome(UUID)).contains(":")) {
                        final String[] split = FacAPI.getInimigos(FacAPI.getFacNome(UUID)).split(":");

                        for (int i = 1; i <= split.length; i++) {
                            if (i <= 7) {
                                final ItemStack inimigo1 = new ItemStack(Material.PLAYER_HEAD);
                                final SkullMeta metainimigo1 = (SkullMeta) inimigo1.getItemMeta();
                                //metainimigo1.setOwner(AccountAPI.getNick(FacAPI.getLideres(FacAPI.getNomeWithTag(split[i - 1]))));
                                metainimigo1.setDisplayName(ChatColor.GREEN + split[i - 1]);
                                inimigo1.setItemMeta(metainimigo1);
                                inventory.setItem(9 + i, inimigo1);
                            } else if (i > 7 && i <= 14) {
                                final ItemStack inimigo1 = new ItemStack(Material.PLAYER_HEAD);
                                final SkullMeta metainimigo1 = (SkullMeta) inimigo1.getItemMeta();
                                //metainimigo1.setOwner(AccountAPI.getNick(FacAPI.getLideres(FacAPI.getNomeWithTag(split[i - 1]))));
                                metainimigo1.setDisplayName(ChatColor.GREEN + split[i - 1]);
                                inimigo1.setItemMeta(metainimigo1);
                                inventory.setItem(11 + i, inimigo1);
                            } else if (i > 14 && i <= 20) {
                                final ItemStack inimigo1 = new ItemStack(Material.PLAYER_HEAD);
                                final SkullMeta metainimigo1 = (SkullMeta) inimigo1.getItemMeta();
                                //metainimigo1.setOwner(AccountAPI.getNick(FacAPI.getLideres(FacAPI.getNomeWithTag(split[i - 1]))));
                                metainimigo1.setDisplayName(ChatColor.GREEN + split[i - 1]);
                                inimigo1.setItemMeta(metainimigo1);
                                inventory.setItem(13 + i, inimigo1);
                            }
                        }
                    } else {
                        final ItemStack inimigo1 = new ItemStack(Material.PLAYER_HEAD);
                        final SkullMeta metainimigo1 = (SkullMeta) inimigo1.getItemMeta();
                        //metainimigo1.setOwner(AccountAPI.getNick(FacAPI.getLideres(FacAPI.getNomeWithTag(FacAPI.getInimigos(FacAPI.getFacNome(UUID))))));
                        metainimigo1.setDisplayName(ChatColor.GREEN + FacAPI.getInimigos(FacAPI.getFacNome(UUID)));
                        inimigo1.setItemMeta(metainimigo1);
                        inventory.setItem(22, inimigo1);
                    }

                    final ItemStack voltar = new ItemStack(Material.ARROW);
                    final ItemMeta metaVoltar = voltar.getItemMeta();
                    metaVoltar.setDisplayName(ChatColor.RED + "Voltar");
                    voltar.setItemMeta(metaVoltar);
                    inventory.setItem(36, voltar);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(inventory);
                        }
                    }, 5L);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, o esquadrão não contém inimigos.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                return;
            }

            if (event.getSlot() == 21) {
                final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Aliados: ");

                if (FacAPI.getAliados(FacAPI.getFacNome(UUID)) != null && !FacAPI.getAliados(FacAPI.getFacNome(UUID)).equalsIgnoreCase("NULL")) {
                    if (FacAPI.getAliados(FacAPI.getFacNome(UUID)).contains(":")) {
                        final String[] split = FacAPI.getAliados(FacAPI.getFacNome(UUID)).split(":");

                        if (split.length == 2) {
                            final ItemStack aliado1 = new ItemStack(Material.PLAYER_HEAD);
                            final SkullMeta metaAliado1 = (SkullMeta) aliado1.getItemMeta();
                            if (AccountAPI.getNick(FacAPI.getLideres(FacAPI.getNomeWithTag(split[0]))) != null) {
                                //metaAliado1.setOwner(AccountAPI.getNick(FacAPI.getLideres(FacAPI.getNomeWithTag(split[0]))));
                            }
                            metaAliado1.setDisplayName(ChatColor.GREEN + split[0]);
                            aliado1.setItemMeta(metaAliado1);
                            inventory.setItem(12, aliado1);

                            final ItemStack aliado2 = new ItemStack(Material.PLAYER_HEAD);
                            final SkullMeta metaAliado2 = (SkullMeta) aliado2.getItemMeta();
                            if (AccountAPI.getNick(FacAPI.getLideres(FacAPI.getNomeWithTag(split[1]))) != null) {
                                //metaAliado2.setOwner(AccountAPI.getNick(FacAPI.getLideres(FacAPI.getNomeWithTag(split[1]))));
                            }
                            metaAliado2.setDisplayName(ChatColor.GREEN + split[1]);
                            aliado2.setItemMeta(metaAliado2);
                            inventory.setItem(14, aliado2);
                        } else if (split.length == 3) {
                            final ItemStack aliado1 = new ItemStack(Material.PLAYER_HEAD);
                            final SkullMeta metaAliado1 = (SkullMeta) aliado1.getItemMeta();
                            if (AccountAPI.getNick(FacAPI.getLideres(FacAPI.getNomeWithTag(split[0]))) != null) {
                                //metaAliado1.setOwner(AccountAPI.getNick(FacAPI.getLideres(FacAPI.getNomeWithTag(split[0]))));
                            }
                            metaAliado1.setDisplayName(ChatColor.GREEN + split[0]);
                            aliado1.setItemMeta(metaAliado1);
                            inventory.setItem(12, aliado1);

                            final ItemStack aliado2 = new ItemStack(Material.PLAYER_HEAD);
                            final SkullMeta metaAliado2 = (SkullMeta) aliado2.getItemMeta();
                            if (AccountAPI.getNick(FacAPI.getLideres(FacAPI.getNomeWithTag(split[1]))) != null) {
                                //metaAliado2.setOwner(AccountAPI.getNick(FacAPI.getLideres(FacAPI.getNomeWithTag(split[1]))));
                            }
                            metaAliado2.setDisplayName(ChatColor.GREEN + split[1]);
                            aliado2.setItemMeta(metaAliado2);
                            inventory.setItem(13, aliado2);

                            final ItemStack aliado3 = new ItemStack(Material.PLAYER_HEAD);
                            final SkullMeta metaAliado3 = (SkullMeta) aliado3.getItemMeta();
                            if (AccountAPI.getNick(FacAPI.getLideres(FacAPI.getNomeWithTag(split[2]))) != null) {
                                //metaAliado3.setOwner(AccountAPI.getNick(FacAPI.getLideres(FacAPI.getNomeWithTag(split[2]))));
                            }
                            metaAliado3.setDisplayName(ChatColor.GREEN + split[2]);
                            aliado3.setItemMeta(metaAliado3);
                            inventory.setItem(14, aliado3);
                        }
                    } else {
                        final ItemStack aliado1 = new ItemStack(Material.PLAYER_HEAD);
                        final SkullMeta metaAliado1 = (SkullMeta) aliado1.getItemMeta();

                        if (AccountAPI.getNick(FacAPI.getLideres(FacAPI.getNomeWithTag(FacAPI.getAliados(FacAPI.getFacNome(UUID))))) != null) {
                            //metaAliado1.setOwner(AccountAPI.getNick(FacAPI.getLideres(FacAPI.getNomeWithTag(FacAPI.getAliados(FacAPI.getFacNome(UUID))))));
                        }
                        metaAliado1.setDisplayName(ChatColor.GREEN + FacAPI.getAliados(FacAPI.getFacNome(UUID)));
                        aliado1.setItemMeta(metaAliado1);
                        inventory.setItem(13, aliado1);
                    }

                    final ItemStack voltar = new ItemStack(Material.ARROW);
                    final ItemMeta metaVoltar = voltar.getItemMeta();
                    metaVoltar.setDisplayName(ChatColor.RED + "Voltar");
                    voltar.setItemMeta(metaVoltar);
                    inventory.setItem(18, voltar);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(inventory);
                        }
                    }, 5L);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, o esquadrão não contém aliados.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                return;
            }

            if (event.getSlot() == 14) {
                if (FacAPI.getSpawn(FacAPI.getFacNome(UUID)) != null) {
                    final String[] split = FacAPI.getSpawn(FacAPI.getFacNome(UUID)).split(":");
                    final Location location = new Location(Bukkit.getWorld("Trappist-1b"), Double.parseDouble(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]), Float.parseFloat(split[3]), Float.parseFloat(split[4]));

                    player.getOpenInventory().close();
                    player.sendMessage(ChatColor.GREEN + " * Teletransportando para base.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.teleport(location);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, o esquadrão ainda não contém uma base definida.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
                return;
            }

            if (event.getSlot() == 13) {
                final String nome = FacAPI.getFacNome(UUID);
                String lista = null;
                if (!FacAPI.getRecrutas(nome).equalsIgnoreCase("NULL")) {
                    lista = FacAPI.getRecrutas(nome);
                }
                if (!FacAPI.getMembros(nome).equalsIgnoreCase("NULL")) {
                    if (lista == null) {
                        lista = FacAPI.getMembros(nome);
                    } else {
                        lista = lista + ":" + FacAPI.getMembros(nome);
                    }
                }
                if (!FacAPI.getCapitoes(nome).equalsIgnoreCase("NULL")) {
                    if (lista == null) {
                        lista = FacAPI.getCapitoes(nome);
                    } else {
                        lista = lista + ":" + FacAPI.getCapitoes(nome);
                    }
                }
                if (!FacAPI.getLideres(nome).equalsIgnoreCase("NULL")) {
                    if (lista == null) {
                        lista = FacAPI.getLideres(nome);
                    } else {
                        lista = lista + ":" + FacAPI.getLideres(nome);
                    }
                }

                final Inventory inventory = Bukkit.createInventory(null, 5 * 9, "Membros esquadrão [" + FacAPI.getFacNome(UUID) + "]" + ": ");

                if (lista.contains(":")) {
                    final String[] list = lista.split(":");

                    int slot = 10;
                    for (int i = 1; i <= 15; i++) {
                        if (i <= list.length) {
                            final ItemStack item = new ItemStack(Material.PLAYER_HEAD);
                            final SkullMeta meta = (SkullMeta) item.getItemMeta();
                            //meta.setOwner(AccountAPI.getNick(list[i - 1]));
                            meta.setDisplayName(ChatColor.YELLOW + "[" + FacAPI.getHierarquia(list[i - 1]) + "] " + AccountAPI.getNick(list[i - 1]));
                            item.setItemMeta(meta);

                            if (slot >= 10 && slot <= 16) {
                                inventory.setItem(slot, item);
                                if (slot == 16) {
                                    slot = 19;
                                } else {
                                    slot++;
                                }
                            } else if (slot >= 19 && slot <= 25) {
                                inventory.setItem(slot, item);
                                if (slot == 25) {
                                    slot = 31;
                                } else {
                                    slot++;
                                }
                            } else if (slot == 31) {
                                inventory.setItem(31, item);
                            }
                        } else {
                            final ItemStack item = new ItemStack(Material.PLAYER_HEAD);
                            final SkullMeta meta = (SkullMeta) item.getItemMeta();
                            meta.setDisplayName(" ");
                            item.setItemMeta(meta);

                            if (slot >= 10 && slot <= 16) {
                                inventory.setItem(slot, item);
                                if (slot == 16) {
                                    slot = 19;
                                } else {
                                    slot++;
                                }
                            } else if (slot >= 19 && slot <= 25) {
                                inventory.setItem(slot, item);
                                if (slot == 25) {
                                    slot = 31;
                                } else {
                                    slot++;
                                }
                            } else if (slot == 31) {
                                inventory.setItem(31, item);
                            }
                        }
                    }

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(inventory);
                        }
                    }, 5L);
                } else {
                    int slot = 10;
                    for (int i = 1; i <= 15; i++) {
                        if (lista != null && i == 1) {
                            final ItemStack item = new ItemStack(Material.PLAYER_HEAD);
                            final SkullMeta meta = (SkullMeta) item.getItemMeta();
                            //meta.setOwner(AccountAPI.getNick(lista));
                            meta.setDisplayName(ChatColor.WHITE + "[" + FacAPI.getHierarquia(lista) + "]" + AccountAPI.getNick(lista));
                            item.setItemMeta(meta);

                            if (slot >= 10 && slot <= 16) {
                                inventory.setItem(slot, item);
                                if (slot == 16) {
                                    slot = 19;
                                } else {
                                    slot++;
                                }
                            } else if (slot >= 19 && slot <= 25) {
                                inventory.setItem(slot, item);
                                if (slot == 25) {
                                    slot = 31;
                                } else {
                                    slot++;
                                }
                            } else if (slot == 31) {
                                inventory.setItem(31, item);
                            }
                        } else {
                            final ItemStack item = new ItemStack(Material.PLAYER_HEAD);
                            final SkullMeta meta = (SkullMeta) item.getItemMeta();
                            meta.setDisplayName(" ");
                            item.setItemMeta(meta);

                            if (slot >= 10 && slot <= 16) {
                                inventory.setItem(slot, item);
                                if (slot == 16) {
                                    slot = 19;
                                } else {
                                    slot++;
                                }
                            } else if (slot >= 19 && slot <= 25) {
                                inventory.setItem(slot, item);
                                if (slot == 25) {
                                    slot = 31;
                                } else {
                                    slot++;
                                }
                            } else if (slot == 31) {
                                inventory.setItem(31, item);
                            }
                        }
                    }
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(inventory);
                        }
                    }, 5L);
                }
                return;
            }

            if (event.getSlot() == 12) {
                player.getOpenInventory().close();
                player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes á &n/e&c:\n \n&7 - /e ajuda\n&7 - /e top\n&7 - /e info (tag)\n&7 - /e listar\n&7 - /e perfil (jogador)" +
                        "\n&7 - /e membros (tag)\n&7 - /e permissoes\n&7 - /e chat\n&7 - /e base\n&7 - /e criar (tag) (nome)\n&7 - /e aceitar alianca (tag)" +
                        "\n&7 - /e convites\n&7 - /e sair\n&7 - /e aceitar convite (tag)\n&7 - /e convidar (jogador)\n&7 - /e expulsar (jogador)\n&7 - /e desfazer\n&7 - /e sethome (home)" +
                        "\n&7 - /e dominar\n&7 - /e abandonar (todos/um)\n&7 - /e titulo (jogador)\n&7 - /e mapa\n&7 - /e transferir (jogador)\n&7 - /e relacao (tag)" +
                        "\n&7 - /e verterras\n&7 - /e promover (jogador)\n&7 - /e rebaixar (jogador)\n&7 - /. (mensagem)\n&7 - /a (mensagem)\n&7 - /e geracao\n&7 - /e gerar nexus\n&7 - /e mover nexus" +
                        "\n&7 - /e remover nexus\n&7 - /e reativar nexus\n \n"));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                return;
            }
        }
    }
}
