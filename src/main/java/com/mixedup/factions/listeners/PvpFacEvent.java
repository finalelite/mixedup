package com.mixedup.factions.listeners;

import com.mixedup.factions.FacAPI;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class PvpFacEvent implements Listener {

    @EventHandler(priority = EventPriority.HIGH)
    public void onDamage(final EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
            final Player damager = (Player) event.getDamager();
            final Player damaged = (Player) event.getEntity();

            final String fac1 = FacAPI.getFacNome(damager.getUniqueId().toString());
            final String fac2 = FacAPI.getFacNome(damaged.getUniqueId().toString());
            if (fac1 != null && fac2 != null) {
                if (fac1.equalsIgnoreCase(fac2)) {
                    event.setCancelled(true);
                } else if (FacAPI.getAliados(fac1) != null && FacAPI.getAliados(fac1).contains(fac2)) {
                    event.setCancelled(true);
                } else if (FacAPI.getAmigos(fac1) != null && FacAPI.getAmigos(fac1).contains(fac2)) {
                    event.setCancelled(true);
                }
            }
        }
    }
}
