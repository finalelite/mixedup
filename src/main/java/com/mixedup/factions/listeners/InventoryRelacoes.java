package com.mixedup.factions.listeners;

import com.mixedup.apis.AccountAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.hashs.InvitationsHash;
import com.mixedup.factions.hashs.SquadName;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.util.ArrayList;
import java.util.Arrays;

public class InventoryRelacoes implements Listener {

    private static void sendMessageInimigo(final Player player, final String fac1, final String fac2) {
        final String nome = fac1;
        String lista = null;
        if (!FacAPI.getRecrutas(nome).equals("NULL")) {
            lista = FacAPI.getRecrutas(nome);
        }
        if (!FacAPI.getMembros(nome).equals("NULL")) {
            if (lista == null) {
                lista = FacAPI.getMembros(nome);
            } else {
                lista = lista + ":" + FacAPI.getMembros(nome);
            }
        }
        if (!FacAPI.getCapitoes(nome).equals("NULL")) {
            if (lista == null) {
                lista = FacAPI.getCapitoes(nome);
            } else {
                lista = lista + ":" + FacAPI.getCapitoes(nome);
            }
        }
        if (!FacAPI.getLideres(nome).equals("NULL")) {
            if (lista == null) {
                lista = FacAPI.getLideres(nome);
            } else {
                lista = lista + ":" + FacAPI.getLideres(nome);
            }
        }

        if (lista.contains(":")) {
            final String[] list = lista.split(":");
            for (int i = 1; i <= list.length; i++) {
                final String targetUUID = list[i - 1];
                for (final Player target : Bukkit.getOnlinePlayers()) {
                    if (target.getName().equalsIgnoreCase(AccountAPI.getNick(targetUUID))) {
                        target.sendMessage(ChatColor.RED + "\n (!) Vosso esquadrão acaba de rivalizar-se a [" + FacAPI.getTagWithNome(fac2) + "]\n ");
                        target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        target.playSound(target.getLocation(), Sound.ENTITY_VEX_CHARGE, 1.0f, 1.0f);
                    }
                }
            }
        } else {
            player.sendMessage(ChatColor.RED + "\n (!) Vosso esquadrão acaba de rivalizar-se á [" + FacAPI.getTagWithNome(fac2) + "]\n ");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            player.playSound(player.getLocation(), Sound.ENTITY_VEX_CHARGE, 1.0f, 1.0f);
        }

        final String nome2 = fac2;
        String lista2 = null;
        if (!FacAPI.getRecrutas(nome2).equals("NULL")) {
            lista2 = FacAPI.getRecrutas(nome2);
        }
        if (!FacAPI.getMembros(nome2).equals("NULL")) {
            if (lista2 == null) {
                lista2 = FacAPI.getMembros(nome2);
            } else {
                lista2 = lista2 + ":" + FacAPI.getMembros(nome2);
            }
        }
        if (!FacAPI.getCapitoes(nome2).equals("NULL")) {
            if (lista2 == null) {
                lista2 = FacAPI.getCapitoes(nome2);
            } else {
                lista2 = lista2 + ":" + FacAPI.getCapitoes(nome2);
            }
        }
        if (!FacAPI.getLideres(nome2).equals("NULL")) {
            if (lista2 == null) {
                lista2 = FacAPI.getLideres(nome2);
            } else {
                lista2 = lista2 + ":" + FacAPI.getLideres(nome2);
            }
        }

        if (lista2.contains(":")) {
            final String[] list2 = lista2.split(":");
            for (int i = 1; i <= list2.length; i++) {
                final String targetUUID = list2[i - 1];
                for (final Player target : Bukkit.getOnlinePlayers()) {
                    if (target.getName().equalsIgnoreCase(AccountAPI.getNick(targetUUID))) {
                        target.sendMessage(ChatColor.RED + "\n (!) [" + FacAPI.getTagWithNome(nome) + "] acaba de declarar-se rival de vosso esquadrão.\n ");
                        target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        target.playSound(target.getLocation(), Sound.ENTITY_VEX_CHARGE, 1.0f, 1.0f);
                    }
                }
            }
        } else {
            final String targetUUID = lista2;
            for (final Player target : Bukkit.getOnlinePlayers()) {
                if (target.getName().equalsIgnoreCase(AccountAPI.getNick(targetUUID))) {
                    target.sendMessage(ChatColor.RED + "\n (!) [" + FacAPI.getTagWithNome(nome) + "] acaba de declarar-se rival de vosso esquadrão.\n ");
                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    target.playSound(target.getLocation(), Sound.ENTITY_VEX_CHARGE, 1.0f, 1.0f);
                }
            }
        }
    }

    @EventHandler
    public void onInteract(final InventoryClickEvent event) {

        Player player = (Player) event.getWhoClicked();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getClickedInventory() != null && event.getClickedInventory().getName() != null && event.getClickedInventory().getName().equalsIgnoreCase("Aliados: ")) {
            event.setCancelled(true);
        }

        if (event.getClickedInventory() != null && event.getClickedInventory().getName() != null && event.getClickedInventory().getName().equalsIgnoreCase("Inimigos: ")) {
            event.setCancelled(true);
        }

        if (event.getClickedInventory() != null && event.getClickedInventory().getName() != null && event.getClickedInventory().getName().equalsIgnoreCase("Amigos: ")) {
            event.setCancelled(true);
        }

        if (event.getInventory().getName().equalsIgnoreCase("Relação: ")) {
            event.setCancelled(true);

            //INIMIGO >20(inimigos)
            if (event.getSlot() == 33) {
                final String name = FacAPI.getFacNome(UUID);
                if (FacAPI.getInimigos(name) == null) FacAPI.createRelacoes(name);

                if (!FacAPI.getInimigos(name).equalsIgnoreCase("NULL")) {
                    if (FacAPI.getInimigos(name).contains(SquadName.get(UUID).getSquad())) {
                        player.sendMessage(ChatColor.RED + " * Ops, este esquadrão já é um inimigo!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        return;
                    }
                    int inim = 1;
                    if (FacAPI.getInimigos(name).contains(":")) {
                        inim = FacAPI.getInimigos(name).split(":").length;
                    }
                    if (inim >= 20) {
                        player.sendMessage(ChatColor.RED + " * Ops, você atingiu o limite de inimigos!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        return;
                    }
                }

                final String inimigos = FacAPI.getInimigos(name);
                if (inimigos.equalsIgnoreCase("NULL")) {
                    FacAPI.updateInimigos(name, SquadName.get(UUID).getSquad());
                } else {
                    FacAPI.updateInimigos(name, inimigos + ":" + SquadName.get(UUID).getSquad());
                }

                final String rivais = FacAPI.getRivais(FacAPI.getNomeWithTag(SquadName.get(UUID).getSquad()));
                if (!rivais.contains(SquadName.get(UUID).getSquad())) {
                    if (rivais.equalsIgnoreCase("NULL")) {
                        FacAPI.updateRivais(FacAPI.getNomeWithTag(SquadName.get(UUID).getSquad()), FacAPI.getTagWithNome(name));
                    } else {
                        FacAPI.updateRivais(FacAPI.getNomeWithTag(SquadName.get(UUID).getSquad()), rivais + ":" + FacAPI.getTagWithNome(name));
                    }
                }

                final String aliados2 = FacAPI.getAliados(FacAPI.getNomeWithTag(SquadName.get(UUID).getSquad()));
                if (aliados2.contains(FacAPI.getTagWithNome(name))) {
                    if (aliados2.contains(":")) {
                        final String[] all = aliados2.split(":");
                        String newa = "NULL";
                        for (int i = 1; i <= all.length; i++) {
                            if (!all[i - 1].equalsIgnoreCase(FacAPI.getTagWithNome(name))) {
                                if (newa == "NULL") {
                                    newa = all[i - 1];
                                } else {
                                    newa = newa + ":" + all[i - 1];
                                }
                            }
                        }
                        FacAPI.updateAliados(FacAPI.getNomeWithTag(SquadName.get(UUID).getSquad()), newa);
                    } else {
                        FacAPI.updateAliados(FacAPI.getNomeWithTag(SquadName.get(UUID).getSquad()), "NULL");
                    }
                }

                final String aliados = FacAPI.getAliados(name);
                if (aliados.contains(SquadName.get(UUID).getSquad())) {
                    if (aliados.contains(":")) {
                        final String[] all = aliados.split(":");
                        String newa = "NULL";
                        for (int i = 1; i <= all.length; i++) {
                            if (!all[i - 1].equalsIgnoreCase(SquadName.get(UUID).getSquad())) {
                                if (newa == "NULL") {
                                    newa = all[i - 1];
                                } else {
                                    newa = newa + ":" + all[i - 1];
                                }
                            }
                        }
                        FacAPI.updateAliados(name, newa);
                    } else {
                        FacAPI.updateAliados(name, "NULL");
                    }
                }

                final String amigos2 = FacAPI.getAmigos(FacAPI.getNomeWithTag(SquadName.get(UUID).getSquad()));
                if (amigos2.contains(FacAPI.getTagWithNome(name))) {
                    if (amigos2.contains(":")) {
                        final String[] all = amigos2.split(":");
                        String newa = "NULL";
                        for (int i = 1; i <= all.length; i++) {
                            if (!all[i - 1].equalsIgnoreCase(FacAPI.getTagWithNome(name))) {
                                if (newa == "NULL") {
                                    newa = all[i - 1];
                                } else {
                                    newa = newa + ":" + all[i - 1];
                                }
                            }
                        }
                        FacAPI.updateAmigos(name, newa);
                    } else {
                        FacAPI.updateAmigos(name, "NULL");
                    }
                }

                final String amigos = FacAPI.getAmigos(name);
                if (amigos.contains(SquadName.get(UUID).getSquad())) {
                    if (amigos.contains(":")) {
                        final String[] all = amigos.split(":");
                        String newa = "NULL";
                        for (int i = 1; i <= all.length; i++) {
                            if (!all[i - 1].equalsIgnoreCase(SquadName.get(UUID).getSquad())) {
                                if (newa == "NULL") {
                                    newa = all[i - 1];
                                } else {
                                    newa = newa + ":" + all[i - 1];
                                }
                            }
                        }
                        FacAPI.updateAmigos(name, newa);
                    } else {
                        FacAPI.updateAmigos(name, "NULL");
                    }
                }

                InventoryRelacoes.sendMessageInimigo(player, FacAPI.getFacNome(UUID), FacAPI.getNomeWithTag(SquadName.get(UUID).getSquad()));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.getOpenInventory().close();
            }

            //NEUTRO
            if (event.getSlot() == 31) {
                final String name = FacAPI.getFacNome(UUID);
                final String aliados = FacAPI.getAliados(name);
                final String inimigos = FacAPI.getInimigos(name);

                if (!aliados.contains(SquadName.get(UUID).getSquad()) && !inimigos.contains(SquadName.get(UUID).getSquad())) {
                    player.sendMessage(ChatColor.RED + " * Ops, este esquadrão já se encontra como ligações neutras!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }

                final String rivais = FacAPI.getRivais(FacAPI.getNomeWithTag(SquadName.get(UUID).getSquad()));
                if (rivais.contains(FacAPI.getTagWithNome(name))) {
                    if (rivais.contains(":")) {
                        final String[] all = rivais.split(":");
                        String newa = "NULL";
                        for (int i = 1; i <= all.length; i++) {
                            if (!all[i - 1].equalsIgnoreCase(FacAPI.getTagWithNome(name))) {
                                if (newa == "NULL") {
                                    newa = all[i - 1];
                                } else {
                                    newa = newa + ":" + all[i - 1];
                                }
                            }
                        }
                        FacAPI.updateRivais(FacAPI.getNomeWithTag(SquadName.get(UUID).getSquad()), newa);
                    } else {
                        FacAPI.updateRivais(FacAPI.getNomeWithTag(SquadName.get(UUID).getSquad()), "NULL");
                    }
                }

                final String amigos = FacAPI.getAmigos(FacAPI.getNomeWithTag(SquadName.get(UUID).getSquad()));
                if (amigos.contains(FacAPI.getTagWithNome(name))) {
                    if (amigos.contains(":")) {
                        final String[] all = amigos.split(":");
                        String newa = "NULL";
                        for (int i = 1; i <= all.length; i++) {
                            if (!all[i - 1].equalsIgnoreCase(FacAPI.getTagWithNome(name))) {
                                if (newa == "NULL") {
                                    newa = all[i - 1];
                                } else {
                                    newa = newa + ":" + all[i - 1];
                                }
                            }
                        }
                        FacAPI.updateAmigos(FacAPI.getNomeWithTag(SquadName.get(UUID).getSquad()), newa);
                    } else {
                        FacAPI.updateAmigos(FacAPI.getNomeWithTag(SquadName.get(UUID).getSquad()), "NULL");
                    }
                }

                if (FacAPI.getInimigos(FacAPI.getNomeWithTag(SquadName.get(UUID).getSquad())).contains(SquadName.get(UUID).getSquad())) {
                    if (FacAPI.getInimigos(FacAPI.getNomeWithTag(SquadName.get(UUID).getSquad())).contains(":")) {
                        final String[] all = FacAPI.getInimigos(FacAPI.getNomeWithTag(SquadName.get(UUID).getSquad())).split(":");
                        String newa = "NULL";
                        for (int i = 1; i <= all.length; i++) {
                            if (!all[i - 1].equalsIgnoreCase(FacAPI.getTagWithNome(name))) {
                                if (newa == "NULL") {
                                    newa = all[i - 1];
                                } else {
                                    newa = newa + ":" + all[i - 1];
                                }
                            }
                        }
                        FacAPI.updateInimigos(FacAPI.getNomeWithTag(SquadName.get(UUID).getSquad()), newa);
                    } else {
                        FacAPI.updateInimigos(FacAPI.getNomeWithTag(SquadName.get(UUID).getSquad()), "NULL");
                    }
                }

                if (FacAPI.getAliados(FacAPI.getNomeWithTag(SquadName.get(UUID).getSquad())).contains(SquadName.get(UUID).getSquad())) {
                    if (FacAPI.getAliados(FacAPI.getNomeWithTag(SquadName.get(UUID).getSquad())).contains(":")) {
                        final String[] all = FacAPI.getAliados(FacAPI.getNomeWithTag(SquadName.get(UUID).getSquad())).split(":");
                        String newa = "NULL";
                        for (int i = 1; i <= all.length; i++) {
                            if (!all[i - 1].equalsIgnoreCase(FacAPI.getTagWithNome(name))) {
                                if (newa == "NULL") {
                                    newa = all[i - 1];
                                } else {
                                    newa = newa + ":" + all[i - 1];
                                }
                            }
                        }
                        FacAPI.updateAliados(FacAPI.getNomeWithTag(SquadName.get(UUID).getSquad()), newa);
                    } else {
                        FacAPI.updateAliados(FacAPI.getNomeWithTag(SquadName.get(UUID).getSquad()), "NULL");
                    }
                }

                //---------

                if (FacAPI.getRivais(name).contains(SquadName.get(UUID).getSquad())) {
                    if (FacAPI.getRivais(name).contains(":")) {
                        final String[] all = FacAPI.getRivais(name).split(":");
                        String newa = "NULL";
                        for (int i = 1; i <= all.length; i++) {
                            if (!all[i - 1].equalsIgnoreCase(SquadName.get(UUID).getSquad())) {
                                if (newa == "NULL") {
                                    newa = all[i - 1];
                                } else {
                                    newa = newa + ":" + all[i - 1];
                                }
                            }
                        }
                        FacAPI.updateRivais(name, newa);
                    } else {
                        FacAPI.updateRivais(name, "NULL");
                    }
                }

                if (FacAPI.getAmigos(name).contains(SquadName.get(UUID).getSquad())) {
                    if (FacAPI.getAmigos(name).contains(":")) {
                        final String[] all = FacAPI.getAmigos(name).split(":");
                        String newa = "NULL";
                        for (int i = 1; i <= all.length; i++) {
                            if (!all[i - 1].equalsIgnoreCase(SquadName.get(UUID).getSquad())) {
                                if (newa == "NULL") {
                                    newa = all[i - 1];
                                } else {
                                    newa = newa + ":" + all[i - 1];
                                }
                            }
                        }
                        FacAPI.updateAmigos(name, newa);
                    } else {
                        FacAPI.updateAmigos(name, "NULL");
                    }
                }

                if (inimigos.contains(SquadName.get(UUID).getSquad())) {
                    if (inimigos.contains(":")) {
                        final String[] all = inimigos.split(":");
                        String newa = "NULL";
                        for (int i = 1; i <= all.length; i++) {
                            if (!all[i - 1].equalsIgnoreCase(SquadName.get(UUID).getSquad())) {
                                if (newa == "NULL") {
                                    newa = all[i - 1];
                                } else {
                                    newa = newa + ":" + all[i - 1];
                                }
                            }
                        }
                        FacAPI.updateInimigos(name, newa);
                    } else {
                        FacAPI.updateInimigos(name, "NULL");
                    }
                }

                if (aliados.contains(SquadName.get(UUID).getSquad())) {
                    if (aliados.contains(":")) {
                        final String[] all = aliados.split(":");
                        String newa = "NULL";
                        for (int i = 1; i <= all.length; i++) {
                            if (!all[i - 1].equalsIgnoreCase(SquadName.get(UUID).getSquad())) {
                                if (newa == "NULL") {
                                    newa = all[i - 1];
                                } else {
                                    newa = newa + ":" + all[i - 1];
                                }
                            }
                        }
                        FacAPI.updateAliados(name, newa);
                    } else {
                        FacAPI.updateAliados(name, "NULL");
                    }
                }

                player.sendMessage(ChatColor.GREEN + " * Relações definidas como neutras.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.getOpenInventory().close();
            }

            //ALIADO
            if (event.getSlot() == 29) {
                if (FacAPI.getAliados(FacAPI.getFacNome(UUID)) != null && FacAPI.getAliados(FacAPI.getFacNome(UUID)).contains(SquadName.get(UUID).getSquad())) {
                    player.sendMessage(ChatColor.RED + " * Ops, você já é aliado deste esquadrão!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                } else if (FacAPI.getAmigos(FacAPI.getFacNome(UUID)) != null && FacAPI.getAmigos(FacAPI.getFacNome(UUID)).contains(SquadName.get(UUID).getSquad())) {
                    player.sendMessage(ChatColor.RED + " * Ops, você já é aliado deste esquadrão!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (InvitationsHash.get(SquadName.get(UUID).getSquad().toUpperCase()) == null) {
                    final ArrayList<String> list = new ArrayList<>();
                    list.add(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)).toUpperCase());
                    new InvitationsHash(SquadName.get(UUID).getSquad().toUpperCase(), list).insert();
                } else {
                    final ArrayList<String> invitations = InvitationsHash.get(SquadName.get(UUID).getSquad().toUpperCase()).getInvitations();
                    if (Arrays.asList(invitations.toArray()).contains(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)).toUpperCase())) {
                        player.sendMessage(ChatColor.RED + " * Ops, você já enviou convite a este esquadrão!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.getOpenInventory().close();
                        return;
                    }
                    final ArrayList<String> list = InvitationsHash.get(SquadName.get(UUID).getSquad().toUpperCase()).getInvitations();
                    list.add(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)).toUpperCase());
                    InvitationsHash.get(SquadName.get(UUID).getSquad().toUpperCase()).setInvitations(list);
                }

                player.sendMessage(ChatColor.GREEN + " * Convite de aliação enviado!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.getOpenInventory().close();

                final String UUIDtarget = FacAPI.getLideres(FacAPI.getNomeWithTag(SquadName.get(UUID).getSquad()));
                if (Bukkit.getPlayer(java.util.UUID.fromString(UUIDtarget)) != null && Bukkit.getPlayer(java.util.UUID.fromString(UUIDtarget)).isOnline()) {
                    Bukkit.getPlayer(java.util.UUID.fromString(UUIDtarget)).sendMessage(ChatColor.GREEN + " \n * Vosso esquadrão acaba de receber um pedido de aliança!\n" +
                            "   Solicitante: " + ChatColor.GRAY + player.getName() + ChatColor.YELLOW + " [Líder]\n" + ChatColor.GREEN +
                            "   Esquadrão: " + ChatColor.GRAY + FacAPI.getFacNome(UUID) + "\n \n  Para aceitar utilize /e aceitar aliança " + FacAPI.getTagWithNome(FacAPI.getNome(UUID)) + "\n ");
                    Bukkit.getPlayer(java.util.UUID.fromString(UUIDtarget)).playSound(Bukkit.getPlayer(java.util.UUID.fromString(UUIDtarget)).getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
    }
}
