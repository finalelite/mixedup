package com.mixedup.factions.managers;

import com.mixedup.MySql;
import com.mixedup.apis.AccountAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.PoderAPI;
import com.mixedup.economy.CoinsAPI;
import com.mixedup.factions.CommandsFac;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.hashs.EconomyHash;
import com.mixedup.factions.hashs.InvitationsHash;
import com.mixedup.factions.hashs.PlayerPermHash;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class InventorySquad {

    public static Inventory permsAliado(final String UUID) {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Permissões (Aliado: " + PlayerPermHash.get(UUID).getMembro().substring(2) + "): ");

        final ItemStack acesso = new ItemStack(Material.ENDER_EYE);
        final ItemMeta metaAcesso = acesso.getItemMeta();
        metaAcesso.setDisplayName(ChatColor.YELLOW + "Acesso à base");
        final ArrayList<String> lore2 = new ArrayList<>();
        lore2.add(" ");
        if (FacAPI.getAliadoPermAcesso(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + PlayerPermHash.get(UUID).getMembro().substring(2)) == true) {
            lore2.add(ChatColor.WHITE + "Permissão: " + ChatColor.GRAY + "Habilitada");
        } else {
            lore2.add(ChatColor.WHITE + "Permissão: " + ChatColor.GRAY + "Desabilitada");
        }
        lore2.add(" ");
        metaAcesso.setLore(lore2);
        acesso.setItemMeta(metaAcesso);
        inventory.setItem(14, acesso);

        final ItemStack containers = new ItemStack(Material.CHEST);
        final ItemMeta metaContainers = containers.getItemMeta();
        metaContainers.setDisplayName(ChatColor.YELLOW + "Acessar containers");
        final ArrayList<String> lore1 = new ArrayList<>();
        lore1.add(" ");
        if (FacAPI.getAliadoPermChests(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + PlayerPermHash.get(UUID).getMembro().substring(2)) == true) {
            lore1.add(ChatColor.WHITE + "Permissão: " + ChatColor.GRAY + "Habilitada");
        } else {
            lore1.add(ChatColor.WHITE + "Permissão: " + ChatColor.GRAY + "Desabilitada");
        }
        lore1.add(" ");
        metaContainers.setLore(lore1);
        containers.setItemMeta(metaContainers);
        inventory.setItem(13, containers);

        final ItemStack construir = new ItemStack(Material.STONE_PICKAXE);
        final ItemMeta metaConstruir = construir.getItemMeta();
        metaConstruir.setDisplayName(ChatColor.YELLOW + "Construir");
        final ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        if (FacAPI.getAliadoPermBuild(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + PlayerPermHash.get(UUID).getMembro().substring(2)) == true) {
            lore.add(ChatColor.WHITE + "Permissão: " + ChatColor.GRAY + "Habilitada ");
        } else {
            lore.add(ChatColor.WHITE + "Permissão: " + ChatColor.GRAY + "Desabilitada");
        }
        lore.add(" ");
        metaConstruir.setLore(lore);
        construir.setItemMeta(metaConstruir);
        inventory.setItem(12, construir);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta metaVoltar = voltar.getItemMeta();
        metaVoltar.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(metaVoltar);
        inventory.setItem(22, voltar);

        return inventory;
    }

    public static Inventory invPermAliados(final String UUID) {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Permissões (Aliados): ");

        if (FacAPI.getAliados(FacAPI.getFacNome(UUID)).contains(":")) {
            final String[] split = FacAPI.getAliados(FacAPI.getFacNome(UUID)).split(":");

            if (split.length == 2) {
                final ItemStack aliado1 = new ItemStack(Material.PLAYER_HEAD);
                final SkullMeta metaAliado1 = (SkullMeta) aliado1.getItemMeta();
                //metaAliado1.setOwner(AccountAPI.getNick(FacAPI.getLideres(FacAPI.getNomeWithTag(split[0]))));
                metaAliado1.setDisplayName(ChatColor.GREEN + split[0]);
                aliado1.setItemMeta(metaAliado1);
                inventory.setItem(12, aliado1);

                final ItemStack aliado2 = new ItemStack(Material.PLAYER_HEAD);
                final SkullMeta metaAliado2 = (SkullMeta) aliado2.getItemMeta();
                //metaAliado2.setOwner(AccountAPI.getNick(FacAPI.getLideres(FacAPI.getNomeWithTag(split[1]))));
                metaAliado2.setDisplayName(ChatColor.GREEN + split[1]);
                aliado2.setItemMeta(metaAliado2);
                inventory.setItem(14, aliado2);
            } else if (split.length == 3) {
                final ItemStack aliado1 = new ItemStack(Material.PLAYER_HEAD);
                final SkullMeta metaAliado1 = (SkullMeta) aliado1.getItemMeta();
                //metaAliado1.setOwner(AccountAPI.getNick(FacAPI.getLideres(FacAPI.getNomeWithTag(split[0]))));
                metaAliado1.setDisplayName(ChatColor.GREEN + split[0]);
                aliado1.setItemMeta(metaAliado1);
                inventory.setItem(12, aliado1);

                final ItemStack aliado2 = new ItemStack(Material.PLAYER_HEAD);
                final SkullMeta metaAliado2 = (SkullMeta) aliado2.getItemMeta();
                //metaAliado2.setOwner(AccountAPI.getNick(FacAPI.getLideres(FacAPI.getNomeWithTag(split[1]))));
                metaAliado2.setDisplayName(ChatColor.GREEN + split[1]);
                aliado2.setItemMeta(metaAliado2);
                inventory.setItem(13, aliado2);

                final ItemStack aliado3 = new ItemStack(Material.PLAYER_HEAD);
                final SkullMeta metaAliado3 = (SkullMeta) aliado3.getItemMeta();
                //metaAliado3.setOwner(AccountAPI.getNick(FacAPI.getLideres(FacAPI.getNomeWithTag(split[2]))));
                metaAliado3.setDisplayName(ChatColor.GREEN + split[2]);
                aliado3.setItemMeta(metaAliado3);
                inventory.setItem(14, aliado3);
            }
        } else {
            final ItemStack aliado1 = new ItemStack(Material.PLAYER_HEAD);
            final SkullMeta metaAliado1 = (SkullMeta) aliado1.getItemMeta();
            //metaAliado1.setOwner(AccountAPI.getNick(FacAPI.getLideres(FacAPI.getNomeWithTag(FacAPI.getAliados(FacAPI.getFacNome(UUID))))));
            metaAliado1.setDisplayName(ChatColor.GREEN + FacAPI.getAliados(FacAPI.getFacNome(UUID)));
            aliado1.setItemMeta(metaAliado1);
            inventory.setItem(13, aliado1);
        }

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta metaVoltar = voltar.getItemMeta();
        metaVoltar.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(metaVoltar);
        inventory.setItem(18, voltar);

        return inventory;
    }

    public static Inventory perms(final String UUID) {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Permissões (Membro: " + PlayerPermHash.get(UUID).getMembro() + "): ");

        final ItemStack spawners = new ItemStack(Material.SPAWNER);
        final ItemMeta metaSpawners = spawners.getItemMeta();
        metaSpawners.setDisplayName(ChatColor.YELLOW + "Remover spawners");
        final ArrayList<String> lore6 = new ArrayList<>();
        lore6.add(" ");
        if (FacAPI.getPermSpawners(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro())) == true) {
            lore6.add(ChatColor.WHITE + "Permissão: " + ChatColor.GRAY + "Habilitada");
        } else {
            lore6.add(ChatColor.WHITE + "Permissão: " + ChatColor.GRAY + "Desabilitada");
        }
        lore6.add(" ");
        metaSpawners.setLore(lore6);
        spawners.setItemMeta(metaSpawners);
        inventory.setItem(16, spawners);

        final ItemStack expulsar = new ItemStack(Material.IRON_AXE);
        final ItemMeta metaExpulsar = expulsar.getItemMeta();
        metaExpulsar.setDisplayName(ChatColor.YELLOW + "Expulsar");
        final ArrayList<String> lore5 = new ArrayList<>();
        lore5.add(" ");
        if (FacAPI.getPermExpulsar(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro())) == true) {
            lore5.add(ChatColor.WHITE + "Permissão: " + ChatColor.GRAY + "Habilitada");
        } else {
            lore5.add(ChatColor.WHITE + "Permissão: " + ChatColor.GRAY + "Desabilitada");
        }
        lore5.add(" ");
        metaExpulsar.setLore(lore5);
        expulsar.setItemMeta(metaExpulsar);
        inventory.setItem(15, expulsar);

        final ItemStack recrutar = new ItemStack(Material.WRITABLE_BOOK);
        final ItemMeta metaRecrutar = recrutar.getItemMeta();
        metaRecrutar.setDisplayName(ChatColor.YELLOW + "Recrutar");
        final ArrayList<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        if (FacAPI.getPermRecrutar(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro())) == true) {
            lore4.add(ChatColor.WHITE + "Permissão: " + ChatColor.GRAY + "Habilitada");
        } else {
            lore4.add(ChatColor.WHITE + "Permissão: " + ChatColor.GRAY + "Desabilitada");
        }
        lore4.add(" ");
        metaRecrutar.setLore(lore4);
        recrutar.setItemMeta(metaRecrutar);
        inventory.setItem(14, recrutar);

        final ItemStack abandonar = new ItemStack(Material.DIRT);
        final ItemMeta metaAbandoanar = abandonar.getItemMeta();
        metaAbandoanar.setDisplayName(ChatColor.YELLOW + "Abandonar terras");
        final ArrayList<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        if (FacAPI.getPermAbandonar(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro())) == true) {
            lore3.add(ChatColor.WHITE + "Permissão: " + ChatColor.GRAY + "Habilitada");
        } else {
            lore3.add(ChatColor.WHITE + "Permissão: " + ChatColor.GRAY + "Desabilitada");
        }
        lore3.add(" ");
        metaAbandoanar.setLore(lore3);
        abandonar.setItemMeta(metaAbandoanar);
        inventory.setItem(13, abandonar);

        final ItemStack dominar = new ItemStack(Material.GRASS_BLOCK);
        final ItemMeta metaDominar = dominar.getItemMeta();
        metaDominar.setDisplayName(ChatColor.YELLOW + "Dominar terras");
        final ArrayList<String> lore2 = new ArrayList<>();
        lore2.add(" ");
        if (FacAPI.getPermDominar(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro())) == true) {
            lore2.add(ChatColor.WHITE + "Permissão: " + ChatColor.GRAY + "Habilitada");
        } else {
            lore2.add(ChatColor.WHITE + "Permissão: " + ChatColor.GRAY + "Desabilitada");
        }
        lore2.add(" ");
        metaDominar.setLore(lore2);
        dominar.setItemMeta(metaDominar);
        inventory.setItem(12, dominar);

        final ItemStack containers = new ItemStack(Material.CHEST);
        final ItemMeta metaContainers = containers.getItemMeta();
        metaContainers.setDisplayName(ChatColor.YELLOW + "Acessar containers");
        final ArrayList<String> lore1 = new ArrayList<>();
        lore1.add(" ");
        if (FacAPI.getPermChests(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro())) == true) {
            lore1.add(ChatColor.WHITE + "Permissão: " + ChatColor.GRAY + "Habilitada");
        } else {
            lore1.add(ChatColor.WHITE + "Permissão: " + ChatColor.GRAY + "Desabilitada");
        }
        lore1.add(" ");
        metaContainers.setLore(lore1);
        containers.setItemMeta(metaContainers);
        inventory.setItem(11, containers);

        final ItemStack construir = new ItemStack(Material.STONE_PICKAXE);
        final ItemMeta metaConstruir = construir.getItemMeta();
        metaConstruir.setDisplayName(ChatColor.YELLOW + "Construir");
        final ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        if (FacAPI.getPermBuild(PlayerUUID.getUUIDcache(PlayerPermHash.get(UUID).getMembro())) == true) {
            lore.add(ChatColor.WHITE + "Permissão: " + ChatColor.GRAY + "Habilitada ");
        } else {
            lore.add(ChatColor.WHITE + "Permissão: " + ChatColor.GRAY + "Desabilitada");
        }
        lore.add(" ");
        metaConstruir.setLore(lore);
        construir.setItemMeta(metaConstruir);
        inventory.setItem(10, construir);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta metaVoltar = voltar.getItemMeta();
        metaVoltar.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(metaVoltar);
        inventory.setItem(22, voltar);

        return inventory;
    }

    public static Inventory invPerm() {
        final Inventory inventory = Bukkit.createInventory(null, 4 * 9, "Permissões: ");

        final ItemStack membros = new ItemStack(Material.PLAYER_HEAD);
        final ItemMeta metaMembros = membros.getItemMeta();
        metaMembros.setDisplayName(ChatColor.YELLOW + "Membros");
        membros.setItemMeta(metaMembros);
        inventory.setItem(12, membros);

        final ItemStack aliados = new ItemStack(Material.PLAYER_HEAD);
        final ItemMeta metaAliados = aliados.getItemMeta();
        metaAliados.setDisplayName(ChatColor.GREEN + "Aliados");
        aliados.setItemMeta(metaAliados);
        inventory.setItem(14, CommandsFac.setSkullOwner(aliados, "77550f9a-f591-414d-874d-979a55cd5c05", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZmQ2ZDVmZDUxMmEzNDdjOGM4Y2JhYjRjYzlhZjNlNTMyNDlkOGQyMWQ1N2Y0YWE1ZTIxZGE5MzgxZGFlNmYifX19"));

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta metaVoltar = voltar.getItemMeta();
        metaVoltar.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(metaVoltar);
        inventory.setItem(31, voltar);

        return inventory;
    }

    public static Inventory invPermMembros(final String UUID) {
        final Inventory inventory = Bukkit.createInventory(null, 5 * 9, "Permissões (Membros): ");

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta metaVoltar = voltar.getItemMeta();
        metaVoltar.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(metaVoltar);
        inventory.setItem(40, voltar);

        final String nome = FacAPI.getFacNome(UUID);
        String lista = null;
        if (!FacAPI.getRecrutas(nome).equalsIgnoreCase("NULL")) {
            lista = FacAPI.getRecrutas(nome);
        }
        if (!FacAPI.getMembros(nome).equalsIgnoreCase("NULL")) {
            if (lista == null) {
                lista = FacAPI.getMembros(nome);
            } else {
                lista = lista + ":" + FacAPI.getMembros(nome);
            }
        }
        if (!FacAPI.getCapitoes(nome).equalsIgnoreCase("NULL")) {
            if (lista == null) {
                lista = FacAPI.getCapitoes(nome);
            } else {
                lista = lista + ":" + FacAPI.getCapitoes(nome);
            }
        }
        if (!FacAPI.getLideres(nome).equalsIgnoreCase("NULL")) {
            if (lista == null) {
                lista = FacAPI.getLideres(nome);
            } else {
                lista = lista + ":" + FacAPI.getLideres(nome);
            }
        }

        if (lista.contains(":")) {
            final String[] list = lista.split(":");

            int slot = 10;
            for (int i = 1; i <= 15; i++) {
                if (i <= list.length) {
                    final ItemStack item = new ItemStack(Material.PLAYER_HEAD);
                    final SkullMeta meta = (SkullMeta) item.getItemMeta();
                    //meta.setOwner(AccountAPI.getNick(list[i - 1]));
                    meta.setDisplayName(ChatColor.YELLOW + "[" + FacAPI.getHierarquia(list[i - 1]) + "] " + AccountAPI.getNick(list[i - 1]));
                    item.setItemMeta(meta);

                    if (slot >= 10 && slot <= 16) {
                        inventory.setItem(slot, item);
                        if (slot == 16) {
                            slot = 19;
                        } else {
                            slot++;
                        }
                    } else if (slot >= 19 && slot <= 25) {
                        inventory.setItem(slot, item);
                        if (slot == 25) {
                            slot = 31;
                        } else {
                            slot++;
                        }
                    } else if (slot == 31) {
                        inventory.setItem(31, item);
                    }
                } else {
                    final ItemStack item = new ItemStack(Material.PLAYER_HEAD);
                    final SkullMeta meta = (SkullMeta) item.getItemMeta();
                    meta.setDisplayName(" ");
                    item.setItemMeta(meta);

                    if (slot >= 10 && slot <= 16) {
                        inventory.setItem(slot, item);
                        if (slot == 16) {
                            slot = 19;
                        } else {
                            slot++;
                        }
                    } else if (slot >= 19 && slot <= 25) {
                        inventory.setItem(slot, item);
                        if (slot == 25) {
                            slot = 31;
                        } else {
                            slot++;
                        }
                    } else if (slot == 31) {
                        inventory.setItem(31, item);
                    }
                }
            }
        } else {
            int slot = 10;
            for (int i = 1; i <= 15; i++) {
                if (lista != null && i == 1) {
                    final ItemStack item = new ItemStack(Material.PLAYER_HEAD);
                    final SkullMeta meta = (SkullMeta) item.getItemMeta();
                    //meta.setOwner(AccountAPI.getNick(lista));
                    meta.setDisplayName(ChatColor.WHITE + "[" + FacAPI.getHierarquia(lista) + "]" + AccountAPI.getNick(lista));
                    item.setItemMeta(meta);

                    if (slot >= 10 && slot <= 16) {
                        inventory.setItem(slot, item);
                        if (slot == 16) {
                            slot = 19;
                        } else {
                            slot++;
                        }
                    } else if (slot >= 19 && slot <= 25) {
                        inventory.setItem(slot, item);
                        if (slot == 25) {
                            slot = 31;
                        } else {
                            slot++;
                        }
                    } else if (slot == 31) {
                        inventory.setItem(31, item);
                    }
                } else {
                    final ItemStack item = new ItemStack(Material.PLAYER_HEAD);
                    final SkullMeta meta = (SkullMeta) item.getItemMeta();
                    meta.setDisplayName(" ");
                    item.setItemMeta(meta);

                    if (slot >= 10 && slot <= 16) {
                        inventory.setItem(slot, item);
                        if (slot == 16) {
                            slot = 19;
                        } else {
                            slot++;
                        }
                    } else if (slot >= 19 && slot <= 25) {
                        inventory.setItem(slot, item);
                        if (slot == 25) {
                            slot = 31;
                        } else {
                            slot++;
                        }
                    } else if (slot == 31) {
                        inventory.setItem(31, item);
                    }
                }
            }
        }

        return inventory;
    }

    public static Inventory getInventory(final String name) {
        final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Esquadrão: ");

        final ItemStack rank = new ItemStack(Material.TOTEM_OF_UNDYING);
        final ItemMeta metaRank = rank.getItemMeta();
        metaRank.setDisplayName(ChatColor.YELLOW + "Rank esquadrões");
        metaRank.setLore(topSquads());
        rank.setItemMeta(metaRank);
        inventory.setItem(11, rank);

        final ItemStack help = new ItemStack(Material.PAPER);
        final ItemMeta metaHelp = help.getItemMeta();
        metaHelp.setDisplayName(ChatColor.YELLOW + "Ajuda");
        help.setItemMeta(metaHelp);
        inventory.setItem(12, help);

        final ItemStack membros = new ItemStack(Material.PLAYER_HEAD);
        final ItemMeta metaMembros = membros.getItemMeta();
        metaMembros.setDisplayName(ChatColor.YELLOW + "Membros");
        membros.setItemMeta(metaMembros);
        inventory.setItem(13, membros);

        final ItemStack base = new ItemStack(Material.ENDER_PEARL);
        final ItemMeta metaBase = base.getItemMeta();
        metaBase.setDisplayName(ChatColor.YELLOW + "Base (Teletransportador)");
        base.setItemMeta(metaBase);
        inventory.setItem(14, base);

        final ItemStack terras = new ItemStack(Material.DIRT);
        final ItemMeta metaTerras = terras.getItemMeta();
        metaTerras.setDisplayName(ChatColor.YELLOW + "Terras");
        final ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.YELLOW + " Terras: " + ChatColor.GRAY + FacAPI.getTerras(name));
        lore.add(" ");
        metaTerras.setLore(lore);
        terras.setItemMeta(metaTerras);
        inventory.setItem(15, terras);

        final ItemStack aliados = new ItemStack(Material.PLAYER_HEAD);
        final ItemMeta metaAliados = aliados.getItemMeta();
        metaAliados.setDisplayName(ChatColor.GREEN + "Aliados");
        aliados.setItemMeta(metaAliados);
        inventory.setItem(21, CommandsFac.setSkullOwner(aliados, "77550f9a-f591-414d-874d-979a55cd5c05", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZmQ2ZDVmZDUxMmEzNDdjOGM4Y2JhYjRjYzlhZjNlNTMyNDlkOGQyMWQ1N2Y0YWE1ZTIxZGE5MzgxZGFlNmYifX19"));

        final ItemStack inimigos = new ItemStack(Material.PLAYER_HEAD);
        final ItemMeta metaInimigos = inimigos.getItemMeta();
        metaInimigos.setDisplayName(ChatColor.RED + "Inimigos");
        inimigos.setItemMeta(metaInimigos);
        inventory.setItem(22, CommandsFac.setSkullOwner(inimigos, "a5b19a29-5bf8-4b14-b0b4-3c245b925928", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNGZiOTdiMTdjNjM5NTM5MjY1OGYzMjcxOGFhNDZiZWZhMWMzMWQzNTcyNjUxYzMwZjdkMmJmM2I5M2Y2ZWFkOSJ9fX0="));

        final ItemStack pedidos = new ItemStack(Material.IRON_CHESTPLATE);
        final ItemMeta metaPedidos = pedidos.getItemMeta();
        metaPedidos.setDisplayName(ChatColor.YELLOW + "Pedidos de relação");
        final ArrayList<String> loreP = new ArrayList<>();
        loreP.add(" ");
        if (InvitationsHash.get(FacAPI.getTagWithNome(name)) != null && InvitationsHash.get(FacAPI.getTagWithNome(name)).getInvitations().size() > 0) {
            final ArrayList<String> list = InvitationsHash.get(FacAPI.getTagWithNome(name)).getInvitations();
            int column = 0;
            String line = null;
            for (int i = 1; i <= list.size(); i++) {
                column++;
                if (column <= 10) {
                    if (line == null) {
                        line = ChatColor.GRAY + " -" + list.get(i - 1);
                    } else {
                        line = line + ", " + list.get(i - 1);
                    }
                }
                if (column == 10 || i == list.size()) {
                    loreP.add(line);
                    line = null;
                    column = 0;
                }
            }
        } else {
            loreP.add(ChatColor.RED + "Sem pedidos.");
        }
        loreP.add(" ");
        metaPedidos.setLore(loreP);
        pedidos.setItemMeta(metaPedidos);
        inventory.setItem(23, pedidos);

        final ItemStack infos = new ItemStack(Material.PLAYER_HEAD);
        final SkullMeta metaInfos = (SkullMeta) infos.getItemMeta();
        //metaInfos.setOwner(AccountAPI.getNick(FacAPI.getLideres(name)));
        metaInfos.setDisplayName(ChatColor.GREEN + "Informações");
        infos.setItemMeta(metaInfos);
        inventory.setItem(40, infos);

        final ItemStack desfazer = new ItemStack(Material.BARRIER);
        final ItemMeta metaDesfazer = desfazer.getItemMeta();
        metaDesfazer.setDisplayName(ChatColor.RED + "Desfazer esquadrão (!)");
        desfazer.setItemMeta(metaDesfazer);
        inventory.setItem(42, desfazer);

        final ItemStack permissions = new ItemStack(Material.WRITABLE_BOOK);
        final ItemMeta metaPerm = permissions.getItemMeta();
        metaPerm.setDisplayName(ChatColor.YELLOW + "Permissões");
        permissions.setItemMeta(metaPerm);
        inventory.setItem(38, permissions);

        return inventory;
    }

    private static ArrayList<String> squads = new ArrayList<>();

    public static ArrayList<String> topSquads() {
        if (squads.size() == 0) {
            final ArrayList<String> facName = new ArrayList<>();
            final ArrayList<String> facs = new ArrayList<>();
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Facçoes_data");
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    new EconomyHash(rs.getString("Nome"), 0).insert();
                    facName.add(rs.getString("Nome"));
                    facs.add(getMembers(rs.getString("Recrutas"), rs.getString("Membros"), rs.getString("Capitoes"), rs.getString("Lideres")));
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }

            int value = 0;
            for (int i = 1; i <= facs.size(); i++) {
                if (facs.get(i - 1).contains(":")) {
                    final String[] split = facs.get(i - 1).split(":");
                    for (int x = 1; x <= split.length; x++) {
                        value += CoinsAPI.getBlackCoins(split[x - 1]);
                    }

                    EconomyHash.get(facName.get(i - 1)).setEconomy(value);
                    value = 0;
                } else {
                    value = CoinsAPI.getBlackCoins(facs.get(i - 1));
                    EconomyHash.get(facName.get(i - 1)).setEconomy(value);
                    value = 0;
                }
            }

            for (int i = 1; i <= facs.size(); i++) {
                FacAPI.updateEconomy(facName.get(i - 1), EconomyHash.get(facName.get(i - 1)).getEconomy());
            }

            final List<String> bestSquads = getBestSquadList();
            final ArrayList<String> lore = new ArrayList<>();

            lore.add(" ");
            lore.add(ChatColor.WHITE + " Top 10 Esquadrões");
            lore.add(" ");
            lore.add(ChatColor.GREEN + " Top 1 - " + bestSquads.get(bestSquads.size() - 1));
            if (bestSquads.size() - 2 >= 0 && bestSquads.get(bestSquads.size() - 2) != null) {
                lore.add(ChatColor.GOLD + " Top 2 - " + bestSquads.get(bestSquads.size() - 2));
            } else {
                lore.add(ChatColor.GOLD + " Top 2 - Nenhum");
            }
            if (bestSquads.size() - 3 >= 0 && bestSquads.get(bestSquads.size() - 3) != null) {
                lore.add(ChatColor.YELLOW + " Top 3 - " + bestSquads.get(bestSquads.size() - 3));
            } else {
                lore.add(ChatColor.YELLOW + " Top 3 - Nenhum");
            }
            if (bestSquads.size() - 4 >= 0 && bestSquads.get(bestSquads.size() - 4) != null) {
                lore.add(ChatColor.GRAY + " Top 4 - " + bestSquads.get(bestSquads.size() - 4));
            } else {
                lore.add(ChatColor.GRAY + " Top 4 - Nenhum");
            }
            if (bestSquads.size() - 5 >= 0 && bestSquads.get(bestSquads.size() - 5) != null) {
                lore.add(ChatColor.GRAY + " Top 5 - " + bestSquads.get(bestSquads.size() - 5));
            } else {
                lore.add(ChatColor.GRAY + " Top 5 - Nenhum");
            }
            if (bestSquads.size() - 6 >= 0 && bestSquads.get(bestSquads.size() - 6) != null) {
                lore.add(ChatColor.GRAY + " Top 6 - " + bestSquads.get(bestSquads.size() - 6));
            } else {
                lore.add(ChatColor.GRAY + " Top 6 - Nenhum");
            }
            if (bestSquads.size() - 7 >= 0 && bestSquads.get(bestSquads.size() - 7) != null) {
                lore.add(ChatColor.GRAY + " Top 7 - " + bestSquads.get(bestSquads.size() - 7));
            } else {
                lore.add(ChatColor.GRAY + " Top 7 - Nenhum");
            }
            if (bestSquads.size() - 8 >= 0 && bestSquads.get(bestSquads.size() - 8) != null) {
                lore.add(ChatColor.GRAY + " Top 8 - " + bestSquads.get(bestSquads.size() - 8));
            } else {
                lore.add(ChatColor.GRAY + " Top 8 - Nenhum");
            }
            if (bestSquads.size() - 9 >= 0 && bestSquads.get(bestSquads.size() - 9) != null) {
                lore.add(ChatColor.GRAY + " Top 9 - " + bestSquads.get(bestSquads.size() - 9));
            } else {
                lore.add(ChatColor.GRAY + " Top 9 - Nenhum");
            }
            if (bestSquads.size() - 10 >= 0 && bestSquads.get(bestSquads.size() - 10) != null) {
                lore.add(ChatColor.GRAY + " Top 10 - " + bestSquads.get(bestSquads.size() - 10));
            } else {
                lore.add(ChatColor.GRAY + " Top 10 - Nenhum");
            }
            lore.add(" ");

            for (String s : lore) {
                squads.add(s);
            }
            return lore;
        } else {
            return squads;
        }
    }

    public static String getMembers(final String recrutas, final String membros, final String capitoes, final String lideres) {
        String lista = null;
        if (!recrutas.equalsIgnoreCase("NULL")) {
            lista = recrutas;
        }
        if (!membros.equalsIgnoreCase("NULL")) {
            if (lista == null) {
                lista = membros;
            } else {
                lista = lista + ":" + membros;
            }
        }
        if (!capitoes.equalsIgnoreCase("NULL")) {
            if (lista == null) {
                lista = capitoes;
            } else {
                lista = lista + ":" + capitoes;
            }
        }
        if (!lideres.equalsIgnoreCase("NULL")) {
            if (lista == null) {
                lista = lideres;
            } else {
                lista = lista + ":" + lideres;
            }
        }
        return lista;
    }

    public static int getEconomySquad(final String nome) {
        String lista = null;
        if (!FacAPI.getRecrutas(nome).equalsIgnoreCase("NULL")) {
            lista = FacAPI.getRecrutas(nome);
        }
        if (!FacAPI.getMembros(nome).equalsIgnoreCase("NULL")) {
            if (lista == null) {
                lista = FacAPI.getMembros(nome);
            } else {
                lista = lista + ":" + FacAPI.getMembros(nome);
            }
        }
        if (!FacAPI.getCapitoes(nome).equalsIgnoreCase("NULL")) {
            if (lista == null) {
                lista = FacAPI.getCapitoes(nome);
            } else {
                lista = lista + ":" + FacAPI.getCapitoes(nome);
            }
        }
        if (!FacAPI.getLideres(nome).equalsIgnoreCase("NULL")) {
            if (lista == null) {
                lista = FacAPI.getLideres(nome);
            } else {
                lista = lista + ":" + FacAPI.getLideres(nome);
            }
        }

        if (lista.contains(":")) {
            final String[] split = lista.split(":");
            int value = 0;

            for (int i = 1; i <= split.length; i++) {
                value += CoinsAPI.getBlackCoins(split[i - 1]);
            }
            return value;
        } else {
            return CoinsAPI.getBlackCoins(lista);
        }
    }

    public static int getPoderSquad(final String nome) {
        String lista = null;
        if (!FacAPI.getRecrutas(nome).equalsIgnoreCase("NULL")) {
            lista = FacAPI.getRecrutas(nome);
        }
        if (!FacAPI.getMembros(nome).equalsIgnoreCase("NULL")) {
            if (lista == null) {
                lista = FacAPI.getMembros(nome);
            } else {
                lista = lista + ":" + FacAPI.getMembros(nome);
            }
        }
        if (!FacAPI.getCapitoes(nome).equalsIgnoreCase("NULL")) {
            if (lista == null) {
                lista = FacAPI.getCapitoes(nome);
            } else {
                lista = lista + ":" + FacAPI.getCapitoes(nome);
            }
        }
        if (!FacAPI.getLideres(nome).equalsIgnoreCase("NULL")) {
            if (lista == null) {
                lista = FacAPI.getLideres(nome);
            } else {
                lista = lista + ":" + FacAPI.getLideres(nome);
            }
        }

        if (lista.contains(":")) {
            final String[] split = lista.split(":");
            int value = 0;

            for (int i = 1; i <= split.length; i++) {
                value += PoderAPI.getPoder(split[i - 1]);
            }
            return value;
        } else {
            return PoderAPI.getPoder(lista);
        }
    }

    private static List<String> getBestSquadList() {
        final List<String> list = new ArrayList<String>();
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Facçoes_data ORDER BY Economy");
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(rs.getString("Nome"));
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
}
