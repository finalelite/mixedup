package com.mixedup.factions;

import com.mixedup.MySql;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.PoderAPI;
import com.mixedup.factions.commands.*;
import com.mixedup.mcmmo.ProbabilityApi;
import com.mixedup.utils.AlternateColor;
import net.minecraft.server.v1_13_R2.NBTTagCompound;
import net.minecraft.server.v1_13_R2.NBTTagList;
import net.minecraft.server.v1_13_R2.NBTTagString;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

public class CommandsFac implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("e") || cmd.getName().equalsIgnoreCase("esquadrao")) {
            if (args.length == 0) {
                CommandE.a(UUID, player);
                return false;
            }
            if (args.length == 1) {
                // -/e convites
                if (args[0].equalsIgnoreCase("convites")) {
                    CommandConvites.a(player, UUID);
                    return false;
                }
                // -/e top
                if (args[0].equalsIgnoreCase("top")) {
                    CommandTop.a(player);
                    return false;
                }
                // -/e permissoes
                if (args[0].equalsIgnoreCase("permissoes")) {
                    CommandPerm.a(player, UUID);
                    return false;
                }
                // -/e geracao
                if (args[0].equalsIgnoreCase("geracao") || args[0].equalsIgnoreCase("geração")) {
                    CommandGeracao.a(player);
                    return false;
                }
                // -/e verterras
                if (args[0].equalsIgnoreCase("verterras")) {
                    CommandVerterras.a(player, UUID);
                    return false;
                }
                // -/e mapa
                if (args[0].equalsIgnoreCase("mapa")) {
                    CommandMap.a(player, UUID);
                    return false;
                }

                // -/e dominar
                if (args[0].equalsIgnoreCase("dominar")) {
                    CommandDominar.a(player, UUID);
                    return false;
                }
                // -/e ajuda
                if (args[0].equalsIgnoreCase("ajuda")) {
                    player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes á &n/e&c:\n \n&7 - /e ajuda\n&7 - /e top\n&7 - /e info (tag)\n&7 - /e perfil (jogador)" +
                            "\n&7 - /e membros (tag)\n&7 - /e permissoes\n&7 - /e chat\n&7 - /e base\n&7 - /e criar (tag) (nome)\n&7 - /e aceitar alianca (tag)" +
                            "\n&7 - /e convites\n&7 - /e sair\n&7 - /e aceitar convite (tag)\n&7 - /e convidar (jogador)\n&7 - /e expulsar (jogador)\n&7 - /e desfazer\n&7 - /e sethome (home)" +
                            "\n&7 - /e dominar\n&7 - /e proteger\n&7 - /e abandonar (todos/um)\n&7 - /e mapa\n&7 - /e transferir (jogador)\n&7 - /e relacao (tag)" +
                            "\n&7 - /e verterras\n&7 - /e promover (jogador)\n&7 - /e rebaixar (jogador)\n&7 - /. (mensagem)\n&7 - /a (mensagem)\n&7 - /e geracao\n&7 - /e gerar nexus\n&7 - /e mover nexus" +
                            "\n&7 - /e remover nexus\n&7 - /e reativar nexus\n \n"));
                    return false;
                }
                // -/e proteger
                if (args[0].equalsIgnoreCase("proteger")) {
                    CommandProteger.a(player);
                    return false;
                }
                // -/e base
                if (args[0].equalsIgnoreCase("base")) {
                    CommandBase.a(player, UUID);
                    return false;
                }
                // -/e sethome
                if (args[0].equalsIgnoreCase("sethome")) {
                    CommandSethome.a(player, UUID);
                    return false;
                }
                // -/e chat
                if (args[0].equalsIgnoreCase("chat")) {
                    CommandChat.a(player, UUID);
                    return false;
                }
                // -/e desfazer
                if (args[0].equalsIgnoreCase("desfazer")) {
                    CommandDesfazer.a(player, UUID);
                    return false;
                }
                // -/e sair
                if (args[0].equalsIgnoreCase("sair")) {
                    CommandSair.a(player, UUID);
                    return false;
                }
                // -/e membros
                if (args[0].equalsIgnoreCase("membros")) {
                    CommandMembros.a(player, UUID);
                    return false;
                }
            } else if (args.length == 2) {
                // -/e perfil (jogador)
                if (args[0].equalsIgnoreCase("perfil")) {
                    CommandPerfil.a(player, UUID, args);
                    return false;
                }
                // -/e abandonar (todos/um)
                if (args[0].equalsIgnoreCase("abandonar")) {
                    CommandAbandonar.a(player, UUID, args);
                    return false;
                }
                // -/e reativar nexus
                if (args[0].equalsIgnoreCase("reativar") && args[1].equalsIgnoreCase("nexus")) {
                    CommandReativar.a(player, UUID);
                    return false;
                }
                // -/e remover nexus
                if (args[0].equalsIgnoreCase("remover") && args[1].equalsIgnoreCase("nexus")) {
                   CommandRemoverN.a(player, UUID);
                    return false;
                }
                // -/e mover nexus
                if (args[0].equalsIgnoreCase("mover") && args[1].equalsIgnoreCase("nexus")) {
                    CommandMoverN.a(player, UUID);
                    return false;
                }
                // -/e gerar nexus
                if (args[0].equalsIgnoreCase("gerar") && args[1].equalsIgnoreCase("nexus")) {
                    CommandGerarN.a(player, UUID);
                    return false;
                }
                // -/e relacao (tag)
                if (args[0].equalsIgnoreCase("relacao")) {
                    CommandRelacao.a(player, UUID, args);
                    return false;
                }
                // -/e membros (tag)
                if (args[0].equalsIgnoreCase("membros")) {
                    CommandMembrosT.a(player, args);
                    return false;
                }
                // -/e rebaixar (nick)
                if (args[0].equalsIgnoreCase("rebaixar")) {
                    CommandRebaixar.a(player, UUID, args);
                    return false;
                }
                // -/e promover (nick)
                if (args[0].equalsIgnoreCase("promover")) {
                    CommandPromover.a(player, UUID, args);
                    return false;
                }
                // -/e transferir (nick)
                if (args[0].equalsIgnoreCase("transferir")) {
                    CommandTransferir.a(player, UUID, args);
                    return false;
                }
                // -/e expulsar (nick)
                if (args[0].equalsIgnoreCase("expulsar")) {
                    CommandExpulsar.a(player, UUID, args);
                    return false;
                }
                // -/e convidar (nick)
                if (args[0].equalsIgnoreCase("convidar")) {
                    CommandConvidar.a(player, UUID, args);
                    return false;
                }
            } else if (args.length == 3) {
                // -/e aceitar alianca (tag)
                if (args[0].equalsIgnoreCase("aceitar") && args[1].equalsIgnoreCase("aliança")) {
                    CommandAccAlianca.a(player, UUID, args);
                    return false;
                }
                // -/e aceitar convite (tag)
                if (args[0].equalsIgnoreCase("aceitar") && args[1].equalsIgnoreCase("convite")) {
                    CommandAccConvite.a(player, UUID, args);
                    return false;
                }
                // -/e criar (tag) (nome)
                if (args[0].equalsIgnoreCase("criar")) {
                    CommandCriar.a(player, UUID, args);
                    return false;
                }
            } else {
                player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes á &n/e&c:\n \n&7 - /e ajuda\n&7 - /e top\n&7 - /e info (tag)\n&7 - /e perfil (jogador)" +
                        "\n&7 - /e membros (tag)\n&7 - /e permissoes\n&7 - /e chat\n&7 - /e base\n&7 - /e criar (tag) (nome)\n&7 - /e aceitar alianca (tag)" +
                        "\n&7 - /e convites\n&7 - /e sair\n&7 - /e aceitar convite (tag)\n&7 - /e convidar (jogador)\n&7 - /e expulsar (jogador)\n&7 - /e desfazer\n&7 - /e sethome (home)" +
                        "\n&7 - /e dominar\n&7 - /e proteger\n&7 - /e abandonar (todos/um)\n&7 - /e mapa\n&7 - /e transferir (jogador)\n&7 - /e relacao (tag)" +
                        "\n&7 - /e verterras\n&7 - /e promover (jogador)\n&7 - /e rebaixar (jogador)\n&7 - /. (mensagem)\n&7 - /a (mensagem)\n&7 - /e geracao\n&7 - /e gerar nexus\n&7 - /e mover nexus" +
                        "\n&7 - /e remover nexus\n&7 - /e reativar nexus\n \n"));
            }
        }
        return false;
    }

    public static void removePlayerFromFac(final String UUID) {
        final String hierarquia = FacAPI.getHierarquia(UUID);
        final String nome = FacAPI.getFacNome(UUID);
        String lista = null;

        if (hierarquia.equalsIgnoreCase("Recruta")) {
            if (FacAPI.getRecrutas(nome).contains(":")) {
                final String[] list = FacAPI.getRecrutas(nome).split(":");
                for (int i = 1; i <= list.length; i++) {
                    if (!list[i - 1].equalsIgnoreCase(UUID)) {
                        if (lista == null) {
                            lista = list[i - 1];
                        } else {
                            lista = lista + ":" + list[i - 1];
                        }
                    }
                }
                FacAPI.updateRecrutas(nome, lista);
                FacAPI.updateQuantiaMembros(nome, FacAPI.getQuantiaWithNome(nome) - 1);
            } else {
                FacAPI.updateRecrutas(nome, "NULL");
                FacAPI.updateQuantiaMembros(nome, FacAPI.getQuantiaWithNome(nome) - 1);
            }
        }
        if (hierarquia.equalsIgnoreCase("Membro")) {
            if (FacAPI.getMembros(nome).contains(":")) {
                final String[] list = FacAPI.getMembros(nome).split(":");
                for (int i = 1; i <= list.length; i++) {
                    if (!list[i - 1].equalsIgnoreCase(UUID)) {
                        if (lista == null) {
                            lista = list[i - 1];
                        } else {
                            lista = lista + ":" + list[i - 1];
                        }
                    }
                }
                FacAPI.updateMembros(nome, lista);
                FacAPI.updateQuantiaMembros(nome, FacAPI.getQuantiaWithNome(nome) - 1);
            } else {
                FacAPI.updateMembros(nome, "NULL");
                FacAPI.updateQuantiaMembros(nome, FacAPI.getQuantiaWithNome(nome) - 1);
            }
        }
        if (hierarquia.equalsIgnoreCase("Capitão")) {
            if (FacAPI.getCapitoes(nome).contains(":")) {
                final String[] list = FacAPI.getCapitoes(nome).split(":");
                for (int i = 1; i <= list.length; i++) {
                    if (!list[i - 1].equalsIgnoreCase(UUID)) {
                        if (lista == null) {
                            lista = list[i - 1];
                        } else {
                            lista = lista + ":" + list[i - 1];
                        }
                    }
                }
                FacAPI.updateCapitoes(nome, lista);
                FacAPI.updateQuantiaMembros(nome, FacAPI.getQuantiaWithNome(nome) - 1);
            } else {
                FacAPI.updateCapitoes(nome, "NULL");
                FacAPI.updateQuantiaMembros(nome, FacAPI.getQuantiaWithNome(nome) - 1);
            }
        }
        if (hierarquia.equalsIgnoreCase("Líder")) {
            if (FacAPI.getLideres(nome).contains(":")) {
                final String[] list = FacAPI.getLideres(nome).split(":");
                for (int i = 1; i <= list.length; i++) {
                    if (!list[i - 1].equalsIgnoreCase(UUID)) {
                        if (lista == null) {
                            lista = list[i - 1];
                        } else {
                            lista = lista + ":" + list[i - 1];
                        }
                    }
                }
                FacAPI.updateLideres(nome, lista);
                FacAPI.updateQuantiaMembros(nome, FacAPI.getQuantiaWithNome(nome) - 1);
            } else {
                FacAPI.updateLideres(nome, "NULL");
                FacAPI.updateQuantiaMembros(nome, FacAPI.getQuantiaWithNome(nome) - 1);
            }
        }
    }

    public static double getPoderSquad(final String UUID, final boolean has, final String facname) {
        String nome = null;
        if (has == false) {
            nome = FacAPI.getFacNome(UUID);
        } else {
            nome = facname;
        }
        String lista = null;
        if (!FacAPI.getRecrutas(nome).equalsIgnoreCase("NULL")) {
            lista = FacAPI.getRecrutas(nome);
        }
        if (!FacAPI.getMembros(nome).equalsIgnoreCase("NULL")) {
            if (lista == null) {
                lista = FacAPI.getMembros(nome);
            } else {
                lista = lista + ":" + FacAPI.getMembros(nome);
            }
        }
        if (!FacAPI.getCapitoes(nome).equalsIgnoreCase("NULL")) {
            if (lista == null) {
                lista = FacAPI.getCapitoes(nome);
            } else {
                lista = lista + ":" + FacAPI.getCapitoes(nome);
            }
        }
        if (!FacAPI.getLideres(nome).equalsIgnoreCase("NULL")) {
            if (lista == null) {
                lista = FacAPI.getLideres(nome);
            } else {
                lista = lista + ":" + FacAPI.getLideres(nome);
            }
        }

        double poder = 0.0;
        if (lista.contains(":")) {
            final String[] split = lista.split(":");
            for (int i = 1; i <= split.length; i++) {
                poder += PoderAPI.getPoder(split[i - 1]);
            }
        } else {
            poder = PoderAPI.getPoder(lista);
        }

        return poder;
    }

    public static ItemStack setSkullOwner(final ItemStack itemStack, final String id, final String textureValue) {
        final net.minecraft.server.v1_13_R2.ItemStack nmsStack = CraftItemStack.asNMSCopy(itemStack);

        NBTTagCompound compound = nmsStack.getTag();
        if (compound == null) {
            compound = new NBTTagCompound();
            nmsStack.setTag(compound);
            compound = nmsStack.getTag();
        }

        final NBTTagCompound skullOwner = new NBTTagCompound();
        skullOwner.set("Id", new NBTTagString(id));
        final NBTTagCompound properties = new NBTTagCompound();
        final NBTTagList textures = new NBTTagList();
        final NBTTagCompound value = new NBTTagCompound();
        value.set("Value", new NBTTagString(textureValue));
        textures.add(value);
        properties.set("textures", textures);
        skullOwner.set("Properties", properties);

        compound.set("SkullOwner", skullOwner);
        nmsStack.setTag(compound);

        return CraftItemStack.asBukkitCopy(nmsStack);
    }

    public static void sendMap(final Player player) {
        final String UUID = PlayerUUID.getUUID(player.getName());
        String msg = null;

        int horizonte = 0;
        int vertical = 0;
        for (int i = 1; i <= 361; i++) {
            final String chunk = (player.getChunk().getX() + 9 - vertical) + ":" + (player.getChunk().getZ() - 9 + horizonte);
            horizonte++;

            if (i == 181) {
                if (msg != null) {
                    if (horizonte >= 19) {
                        msg = msg + ChatColor.WHITE + "⬛\n";
                    } else {
                        msg = msg + ChatColor.WHITE + "⬛";
                    }
                } else {
                    msg = ChatColor.WHITE + "⬛";
                }
                continue;
            }

            if (FacAPI.getChunkOwn(chunk) != null) {
                if (FacAPI.getFacNome(UUID) != null && FacAPI.getChunkOwn(chunk).equalsIgnoreCase(FacAPI.getFacNome(UUID))) {
                    if (msg != null) {
                        if (horizonte >= 19) {
                            msg = msg + ChatColor.GREEN + "⬛\n";
                        } else {
                            msg = msg + ChatColor.GREEN + "⬛";
                        }
                    } else {
                        msg = ChatColor.GREEN + "⬛";
                    }
                    continue;
                } else {
                    if (FacAPI.getAliados(FacAPI.getFacNome(UUID)) != null && FacAPI.getAliados(FacAPI.getFacNome(UUID)).contains(FacAPI.getChunkOwn(chunk))) {
                        if (msg != null) {
                            if (horizonte >= 19) {
                                msg = msg + ChatColor.BLUE + "⬛\n";
                            } else {
                                msg = msg + ChatColor.BLUE + "⬛";
                            }
                        } else {
                            msg = ChatColor.BLUE + "⬛";
                        }
                        continue;
                    } else {
                        if (FacAPI.getInimigos(FacAPI.getFacNome(UUID)) != null && FacAPI.getInimigos(FacAPI.getFacNome(UUID)).contains(FacAPI.getChunkOwn(chunk))) {
                            if (msg != null) {
                                if (horizonte >= 19) {
                                    msg = msg + ChatColor.RED + "⬛\n";
                                } else {
                                    msg = msg + ChatColor.RED + "⬛";
                                }
                            } else {
                                msg = ChatColor.RED + "⬛";
                            }
                            continue;
                        } else {
                            if (msg != null) {
                                if (horizonte >= 19) {
                                    msg = msg + ChatColor.YELLOW + "⬛\n";
                                } else {
                                    msg = msg + ChatColor.YELLOW + "⬛";
                                }
                            } else {
                                msg = ChatColor.YELLOW + "⬛";
                            }
                        }
                    }
                }
            } else {
                if (msg != null) {
                    if (horizonte >= 19) {
                        msg = msg + ChatColor.GRAY + "⬛\n";
                    } else {
                        msg = msg + ChatColor.GRAY + "⬛";
                    }
                } else {
                    msg = ChatColor.GRAY + "⬛";
                }
            }

            if (vertical == 2 && horizonte == 19) {
                msg = msg.substring(0, msg.length() - 1);
                if (CommandsFac.getCardinalDirection(player).equalsIgnoreCase("NOROESTE")) {
                    msg = msg + ChatColor.RED + "  ↖";
                } else {
                    msg = msg + ChatColor.GREEN + "  ↖";
                }

                if (CommandsFac.getCardinalDirection(player).equalsIgnoreCase("NORTE")) {
                    msg = msg + ChatColor.RED + "N";
                } else {
                    msg = msg + ChatColor.GREEN + "N";
                }

                if (CommandsFac.getCardinalDirection(player).equalsIgnoreCase("NORDESTE")) {
                    msg = msg + ChatColor.RED + "↗\n";
                } else {
                    msg = msg + ChatColor.GREEN + "↗\n";
                }
            }

            if (vertical == 3 && horizonte == 19) {
                msg = msg.substring(0, msg.length() - 1);
                if (CommandsFac.getCardinalDirection(player).equalsIgnoreCase("OESTE")) {
                    msg = msg + ChatColor.RED + "  O" + ChatColor.GREEN + "+";
                } else {
                    msg = msg + ChatColor.GREEN + "  O+";
                }

                if (CommandsFac.getCardinalDirection(player).equalsIgnoreCase("LESTE")) {
                    msg = msg + ChatColor.RED + "L\n";
                } else {
                    msg = msg + ChatColor.GREEN + "L\n";
                }
            }

            if (vertical == 4 && horizonte == 19) {
                msg = msg.substring(0, msg.length() - 1);
                if (CommandsFac.getCardinalDirection(player).equalsIgnoreCase("SUDOESTE")) {
                    msg = msg + ChatColor.RED + "  ↙";
                } else {
                    msg = msg + ChatColor.GREEN + "  ↙";
                }

                if (CommandsFac.getCardinalDirection(player).equalsIgnoreCase("SUL")) {
                    msg = msg + ChatColor.RED + "S";
                } else {
                    msg = msg + ChatColor.GREEN + "S";
                }

                if (CommandsFac.getCardinalDirection(player).equalsIgnoreCase("SUDESTE")) {
                    msg = msg + ChatColor.RED + "↘\n";
                } else {
                    msg = msg + ChatColor.GREEN + "↘\n";
                }
            }

            if (vertical == 7 && horizonte == 19) {
                msg = msg.substring(0, msg.length() - 1);
                msg = msg + ChatColor.GREEN + "  ⬛" + ChatColor.WHITE + " Seu esquadrão\n";
            }

            if (vertical == 8 && horizonte == 19) {
                msg = msg.substring(0, msg.length() - 1);
                msg = msg + ChatColor.BLUE + "  ⬛" + ChatColor.WHITE + " Aliados\n";
            }

            if (vertical == 9 && horizonte == 19) {
                msg = msg.substring(0, msg.length() - 1);
                msg = msg + ChatColor.RED + "  ⬛" + ChatColor.WHITE + " Inimigos\n";
            }

            if (vertical == 10 && horizonte == 19) {
                msg = msg.substring(0, msg.length() - 1);
                msg = msg + ChatColor.YELLOW + "  ⬛" + ChatColor.WHITE + " Zona protegida\n";
            }

            if (vertical == 11 && horizonte == 19) {
                msg = msg.substring(0, msg.length() - 1);
                msg = msg + ChatColor.GRAY + "  ⬛" + ChatColor.WHITE + " Zona livre\n";
            }

            if (vertical == 12 && horizonte == 19) {
                msg = msg.substring(0, msg.length() - 1);
                msg = msg + ChatColor.WHITE + "  ⬛" + ChatColor.WHITE + " Sua posição\n";
            }

            if (horizonte >= 19) {
                vertical++;
                horizonte = 0;
            }
        }

        player.sendMessage("\n \n \n" + msg);
    }

    public static String getCardinalDirection(final Player player) {
        double rotation = (player.getLocation().getYaw() - 90) % 360;
        if (rotation < 0) {
            rotation += 360.0;
        }
        if (0 <= rotation && rotation < 22.5) {
            return "OESTE";
            //NORTE
        } else if (22.5 <= rotation && rotation < 67.5) {
            return "NOROESTE";
            //-NORDESTE
        } else if (67.5 <= rotation && rotation < 112.5) {
            return "NORTE";
            //LESTE
        } else if (112.5 <= rotation && rotation < 157.5) {
            return "NORDESTE";
            //-SUDESTE
        } else if (157.5 <= rotation && rotation < 202.5) {
            return "LESTE";
            //SUL
        } else if (202.5 <= rotation && rotation < 247.5) {
            return "SUDESTE";
            //-SUDOESTE
        } else if (247.5 <= rotation && rotation < 292.5) {
            return "SUL";
            //OESTE
        } else if (292.5 <= rotation && rotation < 337.5) {
            return "SUDOESTE";
            //-NOROESTE
        } else if (337.5 <= rotation && rotation < 360.0) {
            return "OESTE";
            //NORTE
        } else {
            return null;
        }
    }

    public static int probabGenerator() {
        final Random random = new Random();
        int r = 0;
        if (ProbabilityApi.probab(75000) == true) {
            if (ProbabilityApi.probab(75000) == true) {
                r = random.nextInt(3);
            } else {
                r = 3 + random.nextInt(2);
            }
        } else {
            if (ProbabilityApi.probab(75000) == true) {
                r = 5 + random.nextInt(5);
            } else {
                if (ProbabilityApi.probab(75000) == true) {
                    r = 10 + random.nextInt(3);
                } else {
                    r = 10 + random.nextInt(2);
                }
            }
        }

        return r;
    }

    public static boolean haveBlocksInArea(final Location loc1, final Location loc2) {
        boolean h = false;

        for (int x = loc1.getBlockX(); x <= loc2.getBlockX(); x++) {
            for (int y = loc1.getBlockY(); y <= loc2.getBlockY(); y++) {
                for (int z = loc1.getBlockZ(); z <= loc2.getBlockZ(); z++) {
                    final Location loc = new Location(loc1.getWorld(), x, y, z);
                    if (!loc.getBlock().getType().equals(Material.AIR)) {
                        h = true;
                    }
                }
            }
        }

        return h;
    }

    public static void removeAllChunks(final String facname) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Clains_data WHERE Esquadrao = ?");
            st.setString(1, facname);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                final PreparedStatement st2 = MySql.con.prepareStatement("DELETE FROM Clains_data WHERE Chunk = ?");
                st2.setString(1, rs.getString("Chunk"));
                st2.executeUpdate();
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }
}
