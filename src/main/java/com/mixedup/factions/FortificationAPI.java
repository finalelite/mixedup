package com.mixedup.factions;

import com.mixedup.Main;
import com.mixedup.MySql;
import com.mixedup.hashs.TNTHash;
import org.bukkit.Material;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class FortificationAPI {

    private static final FortificationCache cache = new FortificationCache();

    public static void createBlock(final String location, final int life, final int type) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Fortification_data(Location, Life, LastDamage, Type) VALUES (?, ?, ?, ?)");
                    st.setString(1, location);
                    st.setInt(2, life);
                    st.setLong(3, new Date().getTime());
                    st.setInt(4, type);
                    st.executeUpdate();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void removeFortification(final String location) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Fortification_data WHERE Location = ?");
                    st.setString(1, location);
                    st.executeUpdate();

                    cache.cache.invalidate(location);
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void updateLife(final String location, final int life, final Material type) {
        if (getType(location) == 0) {
            if (type.equals(Material.END_STONE)) {
                updateType(location, 1);
            } else if (type.equals(Material.SMOOTH_STONE)) {
                updateType(location, 2);
            }
        }

        final Date now = new Date();
        final Date before = new Date();
        before.setTime(getLastDamage(location));

        final long diff = now.getTime() - before.getTime();
        final Date diffDate = new Date();
        diffDate.setTime(diff);

        int diffMinitus = diffDate.getMinutes();
        int acres = 0;

        for (int i = 0; true; i++) {
            if (diffMinitus > 10) {
                acres++;
                diffMinitus -= 10;
            } else {
                break;
            }
        }

        if (acres > 0) {
            if (getType(location) == 1) {
                if (cache.getFortification(location).getLife() < 4) {
                    final int newLife = cache.getFortification(location).getLife() + acres;

                    if (newLife > 4) {
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                try {
                                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Fortification_data SET Life = ? WHERE Location = ?");
                                    st.setString(2, location);
                                    st.setInt(1, 4);
                                    st.executeUpdate();

                                    cache.getFortification(location).setLife(4);
                                } catch (final SQLException e) {
                                    e.printStackTrace();
                                }
                            }
                        }.runTaskAsynchronously(Main.plugin);
                    } else {
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                try {
                                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Fortification_data SET Life = ? WHERE Location = ?");
                                    st.setString(2, location);
                                    st.setInt(1, newLife);
                                    st.executeUpdate();

                                    cache.getFortification(location).setLife(newLife);
                                } catch (final SQLException e) {
                                    e.printStackTrace();
                                }
                            }
                        }.runTaskAsynchronously(Main.plugin);
                    }
                }
            } else if (getType(location) == 2) {
                if (cache.getFortification(location).getLife() < 10) {
                    final int newLife = cache.getFortification(location).getLife() + acres;

                    if (newLife > 10) {
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                try {
                                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Fortification_data SET Life = ? WHERE Location = ?");
                                    st.setString(2, location);
                                    st.setInt(1, 10);
                                    st.executeUpdate();

                                    cache.getFortification(location).setLife(10);
                                } catch (final SQLException e) {
                                    e.printStackTrace();
                                }
                            }
                        }.runTaskAsynchronously(Main.plugin);
                    } else {
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                try {
                                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Fortification_data SET Life = ? WHERE Location = ?");
                                    st.setString(2, location);
                                    st.setInt(1, newLife);
                                    st.executeUpdate();

                                    cache.getFortification(location).setLife(newLife);
                                } catch (final SQLException e) {
                                    e.printStackTrace();
                                }
                            }
                        }.runTaskAsynchronously(Main.plugin);
                    }
                }
            }
        }

        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Fortification_data SET Life = ? WHERE Location = ?");
                    st.setString(2, location);
                    st.setInt(1, getLife(location, type) - life);
                    st.executeUpdate();

                    cache.getFortification(location).setLife(getLife(location, type) - life);
                    updateLastDamage(location, new Date().getTime());
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void updateLastDamage(final String location, final long lastDamage) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Fortification_data SET LastDamage = ? WHERE Location = ?");
                    st.setString(2, location);
                    st.setLong(1, lastDamage);
                    st.executeUpdate();

                    cache.getFortification(location).setLastDamage(lastDamage);
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void updateType(final String location, final int type) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Fortification_data SET Type = ? WHERE Location = ?");
                    st.setString(2, location);
                    st.setInt(1, type);
                    st.executeUpdate();

                    cache.getFortification(location).setType(type);
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static String getExist(final String location) {
        if (cache.getFortification(location) != null) {
            return cache.getFortification(location).getLocation();
        } else return null;
    }

    public static int getType(final String location) {
        if (cache.getFortification(location) == null) return 0;
        return cache.getFortification(location).getType();
    }

    public static long getLastDamage(final String location) {
        if (cache.getFortification(location) == null) return 0;
        return cache.getFortification(location).getLastDamage();
    }

    public static int getLife(final String location, final Material type) {
        if (cache.getFortification(location) == null) return 0;

        if (getType(location) == 0) {
            if (type.equals(Material.END_STONE)) {
                updateType(location, 1);
            } else if (type.equals(Material.SMOOTH_STONE)) {
                updateType(location, 2);
            }
        }

        final Date now = new Date();
        final Date before = new Date();
        before.setTime(getLastDamage(location));

        final long diff = now.getTime() - before.getTime();
        final Date diffDate = new Date();
        diffDate.setTime(diff);

        int diffMinitus = diffDate.getMinutes();
        int acres = 0;

        for (int i = 0; true; i++) {
            if (diffMinitus > 10) {
                acres++;
                diffMinitus -= 10;
            } else {
                break;
            }
        }

        if (acres > 0) {
            if (getType(location) == 1) {
                if (cache.getFortification(location).getLife() < 4) {
                    final int newLife = cache.getFortification(location).getLife() + acres;

                    if (newLife > 4) {
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                try {
                                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Fortification_data SET Life = ? WHERE Location = ?");
                                    st.setString(2, location);
                                    st.setInt(1, 4);
                                    st.executeUpdate();

                                    cache.getFortification(location).setLife(4);
                                } catch (final SQLException e) {
                                    e.printStackTrace();
                                }
                            }
                        }.runTaskAsynchronously(Main.plugin);
                    } else {
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                try {
                                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Fortification_data SET Life = ? WHERE Location = ?");
                                    st.setString(2, location);
                                    st.setInt(1, newLife);
                                    st.executeUpdate();

                                    cache.getFortification(location).setLife(newLife);
                                } catch (final SQLException e) {
                                    e.printStackTrace();
                                }
                            }
                        }.runTaskAsynchronously(Main.plugin);
                    }
                }
            } else if (getType(location) == 2) {
                if (cache.getFortification(location).getLife() < 10) {
                    final int newLife = cache.getFortification(location).getLife() + acres;

                    if (newLife > 10) {
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                try {
                                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Fortification_data SET Life = ? WHERE Location = ?");
                                    st.setString(2, location);
                                    st.setInt(1, 10);
                                    st.executeUpdate();

                                    cache.getFortification(location).setLife(10);
                                } catch (final SQLException e) {
                                    e.printStackTrace();
                                }
                            }
                        }.runTaskAsynchronously(Main.plugin);
                    } else {
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                try {
                                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Fortification_data SET Life = ? WHERE Location = ?");
                                    st.setString(2, location);
                                    st.setInt(1, newLife);
                                    st.executeUpdate();

                                    cache.getFortification(location).setLife(newLife);
                                } catch (final SQLException e) {
                                    e.printStackTrace();
                                }
                            }
                        }.runTaskAsynchronously(Main.plugin);
                    }
                }
            }
        }

        return cache.getFortification(location).getLife();
    }

    public static void removeTNTInfo(final String location) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM TNTs_data WHERE Location = ?");
                    st.setString(1, location);
                    st.executeUpdate();

                    TNTHash.cache.remove(location);
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void createTNTInfo(final String location, final int level) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO TNTs_data(Location, Nivel) VALUES (?, ?)");
                    st.setString(1, location);
                    st.setInt(2, level);
                    st.executeUpdate();

                    new TNTHash(location, level).insert();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void setCacheTnts() {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM TNTs_data");
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                new TNTHash(rs.getString("Location"), rs.getInt("Nivel")).insert();
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static int getTNTLevel(final String location) {
        if (TNTHash.get(location) != null) {
            return TNTHash.get(location).getLevel();
        } else return 0;
    }
}
