package com.mixedup.factions.commands;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.factions.CommandsFac;
import com.mixedup.factions.FacAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class CommandPromover {

    public static boolean a(Player player, String UUID, String[] args) {
        if (FacAPI.getFacNome(UUID) != null) {
            if (FacAPI.getHierarquia(UUID).equalsIgnoreCase("Líder")) {
                final String fac = FacAPI.getFacNome(UUID);

                if (PlayerUUID.getUUID(args[1]) != null) {
                    if (FacAPI.getFacNome(PlayerUUID.getUUID(args[1])).equalsIgnoreCase(fac)) {
                        if (!FacAPI.getHierarquia(PlayerUUID.getUUID(args[1])).equalsIgnoreCase("Capitão")) {
                            if (FacAPI.getHierarquia(PlayerUUID.getUUID(args[1])).equalsIgnoreCase("Recruta")) {
                                final String UUIDtarget = PlayerUUID.getUUID(args[1]);
                                CommandsFac.removePlayerFromFac(UUIDtarget);
                                if (FacAPI.getMembros(fac).equalsIgnoreCase("NULL")) {
                                    FacAPI.updateMembros(fac, UUIDtarget);
                                } else {
                                    FacAPI.updateMembros(fac, FacAPI.getMembros(fac) + ":" + UUIDtarget);
                                }
                                FacAPI.updateHierarquia(UUIDtarget, "Membro");
                                final Player target = Bukkit.getPlayerExact(args[1]);

                                if (target != null && target.isOnline()) {
                                    target.sendMessage(ChatColor.YELLOW + "\n * Você acaba de ser promovido a Membro do esquadrão!\n ");
                                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                                player.sendMessage(ChatColor.GREEN + " * Você acaba de promover " + args[1] + " á Membro!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                return false;
                            }
                            if (FacAPI.getHierarquia(PlayerUUID.getUUID(args[1])).equalsIgnoreCase("Membro")) {
                                final String UUIDtarget = PlayerUUID.getUUID(args[1]);
                                CommandsFac.removePlayerFromFac(UUIDtarget);
                                if (FacAPI.getCapitoes(fac).equalsIgnoreCase("NULL")) {
                                    FacAPI.updateCapitoes(fac, UUIDtarget);
                                } else {
                                    FacAPI.updateCapitoes(fac, FacAPI.getCapitoes(fac) + ":" + UUIDtarget);
                                }
                                FacAPI.updateHierarquia(UUIDtarget, "Capitão");
                                final Player target = Bukkit.getPlayerExact(args[1]);

                                if (target != null && target.isOnline()) {
                                    target.sendMessage(ChatColor.YELLOW + "\n * Você acaba de ser promovido a Capitão do esquadrão!\n ");
                                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                                player.sendMessage(ChatColor.GREEN + " * Você acaba de promover " + args[1] + " á Capitão!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, este player já se encontra em seu maior cargo.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, este player não está coligado ao esquadrão.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, este player não contém registros em nossa rede.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, somente líder contém acesso a este comando.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, você não está coligado a nenhum esquadrão.");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
        return false;
    }
}
