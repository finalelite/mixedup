package com.mixedup.factions.commands;

import com.mixedup.Main;
import com.mixedup.economy.CoinsAPI;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.api.RelacaoAPI;
import com.mixedup.utils.CentralizeMsg;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class CommandCriar {

    public static boolean a(Player player, String UUID, String[] args) {
        if (FacAPI.getFacNome(UUID) == null) {
            if (FacAPI.getFacNome(UUID) != null) {
                player.sendMessage(ChatColor.RED + " * Ops, você se encontra coligado a um esquadrão!\n" + ChatColor.DARK_GRAY + " * Para descoligar se utilize: " + ChatColor.GRAY + "/e sair");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                return false;
            }
            if (args[1].length() <= 3 && args[1].length() >= 2) {
                if (FacAPI.getExistTag(args[1]) == null) {
                    if (args[2].length() >= 5 && args[2].length() <= 14) {
                        if (FacAPI.getExistNome(args[2]) == null) {
                            if (CoinsAPI.getCoins(UUID) >= 10000) {
                                CoinsAPI.updateCoins(UUID, CoinsAPI.getCoins(UUID) - 10000);
                                FacAPI.createFac(args[2], args[1].toUpperCase(), UUID, CoinsAPI.getBlackCoins(UUID));
                                FacAPI.createInfoPlayer(UUID, args[2], "Líder");
                                FacAPI.permLiderCreate(UUID);
                                RelacaoAPI.createRelacoes(args[2]);
                                player.playSound(player.getLocation(), Sound.ITEM_TOTEM_USE, 1.0f, 1.0f);
                                final String line1 = CentralizeMsg.sendCenteredMessage("&c&lEsquadrão adquirido com sucesso!");
                                final String line2 = CentralizeMsg.sendCenteredMessage("&7Foram debitados de sua conta: 10.000.00$");
                                player.sendMessage("\n" + line1 + "\n" + line2 + "\n ");
                                if (Main.scoreboardManager.hasSquadScoreboard(player)) {
                                    Main.scoreboardManager.getSquadScoreboard().updateEntry(player, "squad_name");
                                    Main.scoreboardManager.getSquadScoreboard().updateEntry(player, "squad_online");
                                    Main.scoreboardManager.getSquadScoreboard().updateEntry(player, "squad_lands");
                                    Main.scoreboardManager.getSquadScoreboard().updateEntry(player, "squad_power");
                                } else {
                                    Main.scoreboardManager.getDefaultScoreboard().updateEntry(player, "squad");
                                }
                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                    @Override
                                    public void run() {
                                        player.sendMessage(ChatColor.RED + "\n (!) Ao criar uma base defina o nexus /e gerar nexus, caso\n  não haja nexus, toda a defesa fica esposta ao inimigo.\n ");
                                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_AMBIENT, 1.0F, 1.0F);
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);
                                    }
                                }, 200L);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, você não contém dinheiro para adquirir um esquadrão.");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, este nome já esta em uso.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, o nome deve conter de 5 à 14 letras.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, esta tag já esta em uso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, a tag deve conter de 2 à 3 argumentos.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, você já contém um esquadrão!");
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
        }
        return false;
    }
}
