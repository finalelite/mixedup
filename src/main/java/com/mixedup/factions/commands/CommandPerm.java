package com.mixedup.factions.commands;

import com.mixedup.factions.FacAPI;
import com.mixedup.factions.managers.InventorySquad;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class CommandPerm {

    public static boolean a(Player player, String UUID) {
        if (FacAPI.getFacNome(UUID) == null) {
            player.sendMessage(ChatColor.RED + " * Ops, você não contém esquadrão para utilizar este comando!");
            player.sendMessage(ChatColor.RED + " * Utilize " + ChatColor.GRAY + "/e ajuda" + ChatColor.RED + " para ver os comandos de esquadrões.");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            return false;
        }
        if (FacAPI.getHierarquia(UUID).equalsIgnoreCase("Líder")) {
            player.openInventory(InventorySquad.invPerm());
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, somente líder contém acesso a este comando.");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
        return false;
    }
}
