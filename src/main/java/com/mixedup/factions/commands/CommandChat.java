package com.mixedup.factions.commands;

import com.mixedup.factions.FacAPI;
import com.mixedup.factions.hashs.ChatStatus;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class CommandChat {

    public static boolean a(Player player, String UUID) {
        if (FacAPI.getFacNome(UUID) != null) {
            if (ChatStatus.get(UUID) == null) {
                new ChatStatus(UUID, true).insert();
                player.sendMessage(ChatColor.RED + " * Chat esquadrão ativado.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            } else if (ChatStatus.get(UUID).getStatus() == false) {
                ChatStatus.get(UUID).setStatus(true);
                player.sendMessage(ChatColor.RED + " * Chat esquadrão ativado.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            } else {
                ChatStatus.get(UUID).setStatus(false);
                player.sendMessage(ChatColor.RED + " * Chat esquadrão desativado.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, você não está coligado a nenhum esquadrão.");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
        return false;
    }
}
