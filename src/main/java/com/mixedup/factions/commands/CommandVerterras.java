package com.mixedup.factions.commands;

import com.mixedup.Main;
import com.mixedup.factions.hashs.IsMarkingUtil;
import org.bukkit.*;
import org.bukkit.entity.Player;

import java.util.Random;

public class CommandVerterras {

    public static boolean a(Player player, String UUID) {
        if (player.getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
            if (IsMarkingUtil.get(UUID) == null) {
                new IsMarkingUtil(UUID, true).insert();
            } else {
                if (IsMarkingUtil.get(UUID).getmarking() == true) {
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return false;
                } else {
                    IsMarkingUtil.get(UUID).setmarking(true);
                }
            }
            player.sendMessage(ChatColor.RED + " * Terra demarcada por 10 segundos!");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            final Chunk c = player.getLocation().getChunk();
            final Location center = new Location(c.getWorld(), c.getX() << 4, 64, c.getZ() << 4).add(7, 0, 7);
            center.setY(center.getWorld().getHighestBlockYAt(center));

            final int rotacao = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                @Override
                public void run() {
                    double acresc = -0.5;
                    for (int i = 1; i <= 7; i++) {
                        final Location pos1 = center.clone().add(-6 - 0.5, 0, 9 - 0.5);
                        final Location pos2 = center.clone().add(-1 - 0.5, 0, 9 - 0.5);
                        final Location pos3 = center.clone().add(4 - 0.5, 0, 9 - 0.5);
                        final Location pos4 = center.clone().add(9 - 0.5, 0, 9 - 0.5);
                        final Location pos5 = center.clone().add(9 - 0.5, 0, 4 - 0.5);
                        final Location pos6 = center.clone().add(9 - 0.5, 0, -1 - 0.5);
                        final Location pos7 = center.clone().add(9 - 0.5, 0, -6 - 0.5);
                        final Location pos8 = center.clone().add(4 - 0.5, 0, -6 - 0.5);
                        final Location pos9 = center.clone().add(-1 - 0.5, 0, -6 - 0.5);
                        final Location pos10 = center.clone().add(-6 - 0.5, 0, -6 - 0.5);
                        final Location pos11 = center.clone().add(-6 - 0.5, 0, -1 - 0.5);
                        final Location pos12 = center.clone().add(-6 - 0.5, 0, 4 - 0.5);

                        final Random random = new Random();
                        final Particle.DustOptions dust = new Particle.DustOptions(Color.fromBGR(random.nextInt(255), random.nextInt(255), random.nextInt(255)), 2);
                        player.getWorld().spawnParticle(Particle.REDSTONE, pos1.getX(), player.getLocation().getY() + acresc, pos1.getZ(), 1, 0, 0, 0, dust);
                        player.getWorld().spawnParticle(Particle.REDSTONE, pos2.getX(), player.getLocation().getY() + acresc, pos2.getZ(), 1, 0, 0, 0, dust);
                        player.getWorld().spawnParticle(Particle.REDSTONE, pos3.getX(), player.getLocation().getY() + acresc, pos3.getZ(), 1, 0, 0, 0, dust);
                        player.getWorld().spawnParticle(Particle.REDSTONE, pos4.getX(), player.getLocation().getY() + acresc, pos4.getZ(), 1, 0, 0, 0, dust);
                        player.getWorld().spawnParticle(Particle.REDSTONE, pos5.getX(), player.getLocation().getY() + acresc, pos5.getZ(), 1, 0, 0, 0, dust);
                        player.getWorld().spawnParticle(Particle.REDSTONE, pos6.getX(), player.getLocation().getY() + acresc, pos6.getZ(), 1, 0, 0, 0, dust);
                        player.getWorld().spawnParticle(Particle.REDSTONE, pos7.getX(), player.getLocation().getY() + acresc, pos7.getZ(), 1, 0, 0, 0, dust);
                        player.getWorld().spawnParticle(Particle.REDSTONE, pos8.getX(), player.getLocation().getY() + acresc, pos8.getZ(), 1, 0, 0, 0, dust);
                        player.getWorld().spawnParticle(Particle.REDSTONE, pos9.getX(), player.getLocation().getY() + acresc, pos9.getZ(), 1, 0, 0, 0, dust);
                        player.getWorld().spawnParticle(Particle.REDSTONE, pos10.getX(), player.getLocation().getY() + acresc, pos10.getZ(), 1, 0, 0, 0, dust);
                        player.getWorld().spawnParticle(Particle.REDSTONE, pos11.getX(), player.getLocation().getY() + acresc, pos11.getZ(), 1, 0, 0, 0, dust);
                        player.getWorld().spawnParticle(Particle.REDSTONE, pos12.getX(), player.getLocation().getY() + acresc, pos12.getZ(), 1, 0, 0, 0, dust);
                        acresc += 0.5;
                    }
                }
            }, 0L, 10L);

            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                @Override
                public void run() {
                    Bukkit.getScheduler().cancelTask(rotacao);
                    IsMarkingUtil.get(UUID).setmarking(false);
                }
            }, 200L);
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, para utilizar este comando você deve estar no mundo de esquadrões!");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
        return false;
    }
}
