package com.mixedup.factions.commands;

import com.mixedup.factions.FacAPI;
import com.mixedup.factions.hashs.InvitationsHash;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class CommandConvites {

    public static boolean a(Player player, String UUID) {
        if (FacAPI.getFacNome(UUID) == null) {
            player.sendMessage(ChatColor.RED + " * Ops, você não contém esquadrão para utilizar este comando!");
            player.sendMessage(ChatColor.RED + " * Utilize " + ChatColor.GRAY + "/e ajuda" + ChatColor.RED + " para ver os comandos de esquadrões.");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            return false;
        }
        if (InvitationsHash.get(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)).toUpperCase()) != null) {
            final ArrayList<String> list = InvitationsHash.get(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)).toUpperCase()).getInvitations();

            String msg = ChatColor.YELLOW + " \n * Convites de aliança: \n  " + ChatColor.GRAY + "-";
            boolean first = false;
            for (int i = 1; i <= list.size(); i++) {
                if (first == false) {
                    msg = msg + list.get(i - 1);
                } else {
                    msg = msg + ", " + list.get(i - 1);
                }
                if (i == 1) first = true;
                if (i == list.size()) {
                    msg = msg + "\n ";
                }
            }
            player.sendMessage(msg);
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, vosso esquadrão não contém convites!");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
        return false;
    }
}
