package com.mixedup.factions.commands;

import com.mixedup.apis.AccountAPI;
import com.mixedup.factions.CommandsFac;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.hashs.SquadName;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class CommandRelacao {

    public static boolean a(Player player, String UUID, String[] args) {
        if (FacAPI.getFacNome(UUID) == null) {
            player.sendMessage(ChatColor.RED + " * Ops, você não contém esquadrão para utilizar este comando!");
            player.sendMessage(ChatColor.RED + " * Utilize " + ChatColor.GRAY + "/e ajuda" + ChatColor.RED + " para ver os comandos de esquadrões.");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            return false;
        }
        if (FacAPI.getHierarquia(UUID).equalsIgnoreCase("Líder")) {
            if (args[1].length() <= 3 && args[1].length() >= 2) {
                if (FacAPI.getExistTag(args[1]) != null) {
                    if (FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)).equalsIgnoreCase(args[1])) {
                        player.sendMessage(ChatColor.RED + " * Ops, você não pode definir relações com vosso proprio esquadrão!");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        return false;
                    }

                    if (SquadName.get(UUID) == null) {
                        new SquadName(UUID, args[1].toUpperCase()).insert();
                    } else {
                        SquadName.get(UUID).setSquad(args[1].toUpperCase());
                    }
                    final ItemStack esq = new ItemStack(Material.PLAYER_HEAD);
                    final SkullMeta meta4 = (SkullMeta) esq.getItemMeta();
                    //meta4.setOwner(AccountAPI.getNick(FacAPI.getLideres(FacAPI.getNomeWithTag(args[1]))));
                    meta4.setDisplayName(ChatColor.YELLOW + "[" + args[1] + "]");
                    esq.setItemMeta(meta4);

                    final Inventory inventory = Bukkit.createInventory(null, 5 * 9, "Relação: ");
                    inventory.setItem(13, esq);
                    final ItemStack item = new ItemStack(Material.PLAYER_HEAD);
                    final ItemMeta meta = item.getItemMeta();
                    meta.setDisplayName(ChatColor.GREEN + "Aliar-se");
                    item.setItemMeta(meta);

                    final ItemStack item2 = new ItemStack(Material.PLAYER_HEAD);
                    final ItemMeta meta2 = item2.getItemMeta();
                    meta2.setDisplayName(ChatColor.GRAY + "Neutro");
                    item2.setItemMeta(meta2);

                    final ItemStack item3 = new ItemStack(Material.PLAYER_HEAD);
                    final ItemMeta meta3 = item3.getItemMeta();
                    meta3.setDisplayName(ChatColor.RED + "Rivalizar-se");
                    item3.setItemMeta(meta3);

                    inventory.setItem(29, CommandsFac.setSkullOwner(item, "77550f9a-f591-414d-874d-979a55cd5c05", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZmQ2ZDVmZDUxMmEzNDdjOGM4Y2JhYjRjYzlhZjNlNTMyNDlkOGQyMWQ1N2Y0YWE1ZTIxZGE5MzgxZGFlNmYifX19"));
                    inventory.setItem(31, CommandsFac.setSkullOwner(item2, "6f7fe493-c154-40eb-ae0a-3312ddbe01d3", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOWM0YTQxNTU0MzQyYjJjYmZiZDg5NWU5ZWM3MDg5YWU4NjFmZWM4ZjUxNjkzODIyZWMxZWIzY2EzZjE4In19fQ=="));
                    inventory.setItem(33, CommandsFac.setSkullOwner(item3, "a5b19a29-5bf8-4b14-b0b4-3c245b925928", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNGZiOTdiMTdjNjM5NTM5MjY1OGYzMjcxOGFhNDZiZWZhMWMzMWQzNTcyNjUxYzMwZjdkMmJmM2I5M2Y2ZWFkOSJ9fX0="));

                    player.openInventory(inventory);
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, este esquadrão não existe.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, a tag deve conter de 2 à 3 argumentos.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, somente líder contém acesso a este comando.");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
        return false;
    }
}
