package com.mixedup.factions.commands;

import com.mixedup.Main;
import com.mixedup.factions.CommandsFac;
import com.mixedup.factions.FacAPI;
import com.mixedup.worldProtection.hashs.ProtectionUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class CommandDominar {

    public static boolean a(Player player, String UUID) {
        if (FacAPI.getFacNome(UUID) != null) {
            if (FacAPI.getHierarquia(UUID).equalsIgnoreCase("Líder") || FacAPI.getHierarquia(UUID).equalsIgnoreCase("Capitão") || FacAPI.getPermDominar(UUID) == true) {
                if (player.getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
                    if (ProtectionUtil.playerInArea(player.getLocation()) != null) {
                        player.sendMessage(ChatColor.RED + " * Ops, você não poder dominar está area!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        return false;
                    }
                    final String chunk = player.getLocation().getChunk().getX() + ":" + player.getLocation().getChunk().getZ();
                    if (FacAPI.getChunkOwn(chunk) == null) {
                        if (FacAPI.getTerras(FacAPI.getFacNome(UUID)) < 112) {
                            if (CommandsFac.getPoderSquad(UUID, false, null) >= 3) {
                                FacAPI.createChunk(chunk, FacAPI.getFacNome(UUID));
                                FacAPI.updateTerras(FacAPI.getFacNome(UUID), FacAPI.getTerras(FacAPI.getFacNome(UUID)) + 1);
                                player.sendMessage(ChatColor.YELLOW + "\n * Terra claimada com sucesso.\n ");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                                if (FacAPI.getTerras(FacAPI.getFacNome(UUID)) == 1) {
                                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                        @Override
                                        public void run() {
                                            player.sendMessage(ChatColor.RED + "\n (!) Ao criar uma base defina o nexus /e gerar nexus, caso\n  não haja nexus, toda a defesa fica esposta ao inimigo.\n ");
                                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_AMBIENT, 1.0F, 1.0F);
                                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);
                                        }
                                    }, 200L);
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, para claimar vosso esquadrão deve conter ao menos 3 de poder!.");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, você excedeu o limite de terras!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        if (FacAPI.getChunkOwn(chunk).equalsIgnoreCase(FacAPI.getFacNome(UUID))) {
                            player.sendMessage(ChatColor.RED + " * Ops, esta terra já se encontra claimada!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else {
                            if (FacAPI.getTerras(FacAPI.getChunkOwn(chunk)) > CommandsFac.getPoderSquad(null, true, FacAPI.getChunkOwn(chunk))) {
                                final String squadChunk = FacAPI.getChunkOwn(chunk);
                                final int x = player.getLocation().getChunk().getX();
                                final int z = player.getLocation().getChunk().getZ();

                                boolean chunk1 = false;
                                boolean chunk2 = false;
                                boolean chunk3 = false;
                                boolean chunk4 = false;
                                for (int i = 1; i <= 4; i++) {
                                    if (i == 4) {
                                        final String chunkT = x + ":" + (z + 1);
                                        if (FacAPI.getChunkOwn(chunkT) != null) {
                                            if (FacAPI.getChunkOwn(chunkT).equalsIgnoreCase(squadChunk)) {
                                                chunk4 = true;
                                            }
                                        }
                                    }
                                    if (i == 3) {
                                        final String chunkT = x + ":" + (z - 1);
                                        if (FacAPI.getChunkOwn(chunkT) != null) {
                                            if (FacAPI.getChunkOwn(chunkT).equalsIgnoreCase(squadChunk)) {
                                                chunk3 = true;
                                            }
                                        }
                                    }
                                    if (i == 2) {
                                        final String chunkT = (x + 1) + ":" + z;
                                        if (FacAPI.getChunkOwn(chunkT) != null) {
                                            if (FacAPI.getChunkOwn(chunkT).equalsIgnoreCase(squadChunk)) {
                                                chunk2 = true;
                                            }
                                        }
                                    }
                                    if (i == 1) {
                                        final String chunkT = (x - 1) + ":" + z;
                                        if (FacAPI.getChunkOwn(chunkT) != null) {
                                            if (FacAPI.getChunkOwn(chunkT).equalsIgnoreCase(squadChunk)) {
                                                chunk1 = true;
                                            }
                                        }
                                    }
                                }

                                if (chunk1 == true && chunk2 == true && chunk3 == true && chunk4 == true) {
                                    player.sendMessage(ChatColor.RED + " * Ops, esta terra não pertence a borda inimiga!");
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                } else {
                                    if (FacAPI.getTerras(FacAPI.getFacNome(UUID)) < 112) {
                                        if (CommandsFac.getPoderSquad(UUID, false, null) >= 3) {
                                            FacAPI.updateTerras(FacAPI.getChunkOwn(chunk), FacAPI.getTerras(FacAPI.getFacNome(UUID)) - 1);
                                            FacAPI.updateChunk(chunk, FacAPI.getFacNome(UUID));
                                            FacAPI.updateTerras(FacAPI.getFacNome(UUID), FacAPI.getTerras(FacAPI.getFacNome(UUID)) + 1);
                                            player.sendMessage(ChatColor.YELLOW + "\n * Terra claimada com sucesso.\n ");
                                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        } else {
                                            player.sendMessage(ChatColor.RED + " * Ops, para claimar vosso esquadrão deve conter ao menos 3 de poder!.");
                                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        }
                                    } else {
                                        player.sendMessage(ChatColor.RED + " * Ops, você excedeu o limite de terras!");
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    }
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, esta terra se encontra claimada!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        }
                    }
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, para utilizar este comando você deve estar no mundo de esquadrões!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, somente líder ou capitão contém acesso a este comando.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, você não está coligado a nenhum esquadrão para efetuar este comando.");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
        return false;
    }
}
