package com.mixedup.factions.commands;

import com.mixedup.Main;
import com.mixedup.apis.AccountAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.hashs.InvitationUtil;
import com.mixedup.utils.CentralizeMsg;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class CommandConvidar {

    public static boolean a(Player player, String UUID, String[] args) {
        if (FacAPI.getFacNome(UUID) != null) {
            if (FacAPI.getHierarquia(UUID).equalsIgnoreCase("Líder") || FacAPI.getPermRecrutar(UUID) == true) {
                if (FacAPI.getQuantia(UUID) < 15) {
                    if (AccountAPI.getExistNick(args[1]) != null) {
                        final Player target = Bukkit.getPlayerExact(args[1]);

                        if (target.getName().equalsIgnoreCase(player.getName())) {
                            player.sendMessage(ChatColor.RED + " * Ops, você não pode se convidar D=");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            return false;
                        }

                        if (target != null && target.isOnline()) {
                            player.sendMessage(ChatColor.YELLOW + " * Convite enviado.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            final String line1 = CentralizeMsg.sendCenteredMessage("&e" + player.getName() + " convidou lhe á participar de seu esquadrão [" + FacAPI.getTag(UUID) + "]");
                            final String line2 = CentralizeMsg.sendCenteredMessage("&7Para aceitar utilize: &7/e aceitar convite " + FacAPI.getTag(UUID));
                            target.sendMessage(ChatColor.YELLOW + "\n" + line1 + "\n" + line2 + "\n ");
                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                            final String UUIDtarget = PlayerUUID.getUUID(target.getName());
                            if (InvitationUtil.get(UUIDtarget) == null) {
                                new InvitationUtil(UUIDtarget, true, FacAPI.getTag(UUID)).insert();
                            } else {
                                InvitationUtil.get(UUIDtarget).setInvitation(true);
                                InvitationUtil.get(UUIDtarget).setTag(FacAPI.getTag(UUID));
                            }

                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    if (InvitationUtil.get(UUIDtarget) == null) return;
                                    if (InvitationUtil.get(UUIDtarget).getInvitation() == true && InvitationUtil.get(UUIDtarget).getTag().equalsIgnoreCase(FacAPI.getTag(UUID))) {
                                        InvitationUtil.get(UUIDtarget).setInvitation(false);
                                    }
                                }
                            }, 600L);
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, este player não se encontra online.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, este player não contém registros em nossa rede.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, o esquadrão já se encontra com o limite de players atingido.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else if (FacAPI.getHierarquia(UUID).equalsIgnoreCase("Capitão")) {
                if (FacAPI.getQuantia(FacAPI.getFacNome(UUID)) < 15) {
                    if (AccountAPI.getExistNick(args[1]) != null) {
                        final Player target = Bukkit.getPlayerExact(args[1]);

                        if (target.getName().equalsIgnoreCase(player.getName())) {
                            player.sendMessage(ChatColor.RED + " * Ops, você não pode se convidar D=");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            return false;
                        }

                        if (target != null && target.isOnline()) {
                            player.sendMessage(ChatColor.YELLOW + " * Convite enviado.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            final String line1 = CentralizeMsg.sendCenteredMessage("&e" + player.getName() + " convidou á participar de seu esquadrão [" + FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + "]");
                            final String line2 = CentralizeMsg.sendCenteredMessage("&7Para aceitar utilize: &7/e aceitar convite " + FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)));
                            target.sendMessage(ChatColor.YELLOW + "\n" + line1 + "\n" + line2 + "\n ");
                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                            final String UUIDtarget = PlayerUUID.getUUID(target.getName());
                            if (InvitationUtil.get(UUIDtarget) == null) {
                                new InvitationUtil(UUIDtarget, true, FacAPI.getTagWithNome(FacAPI.getFacNome(UUID))).insert();
                            } else {
                                InvitationUtil.get(UUIDtarget).setInvitation(true);
                                InvitationUtil.get(UUIDtarget).setTag(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)));
                            }

                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    if (InvitationUtil.get(UUIDtarget).getInvitation() == true && InvitationUtil.get(UUID).getTag().equalsIgnoreCase(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)))) {
                                        InvitationUtil.get(UUIDtarget).setInvitation(false);
                                    }
                                }
                            }, 600L);
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, este player não se encontra online.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, este player não contém registros em nossa rede.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, o esquadrão já se encontra com o limite de players atingido.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, você não está coligado a nenhum esquadrão para efetuar este comando.");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
        return false;
    }
}
