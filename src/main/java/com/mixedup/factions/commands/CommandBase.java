package com.mixedup.factions.commands;

import com.mixedup.factions.FacAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class CommandBase {

    public static boolean a(Player player, String UUID) {
        if (FacAPI.getFacNome(UUID) != null) {
            if (FacAPI.getSpawn(FacAPI.getFacNome(UUID)) != null) {
                final String[] split = FacAPI.getSpawn(FacAPI.getFacNome(UUID)).split(":");
                final Location location = new Location(Bukkit.getWorld("Trappist-1b"), Double.parseDouble(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]), Float.parseFloat(split[3]), Float.parseFloat(split[4]));

                player.sendMessage(ChatColor.GREEN + " * Teletransportando para base.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.teleport(location);
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, o esquadrão ainda não contém uma base definida.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, você não está coligado a nenhum esquadrão para efetuar este comando.");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
        return false;
    }
}
