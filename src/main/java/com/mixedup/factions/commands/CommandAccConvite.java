package com.mixedup.factions.commands;

import com.mixedup.Main;
import com.mixedup.apis.AccountAPI;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.hashs.InvitationUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class CommandAccConvite {

    public static boolean a(Player player, String UUID, String[] args) {
        if (FacAPI.getFacNome(UUID) == null) {
            if (args[2].length() >= 2 && args[2].length() <= 3) {
                if (InvitationUtil.get(UUID) != null) {
                    if (InvitationUtil.get(UUID).getInvitation() == true) {
                        if (InvitationUtil.get(UUID).getTag().equalsIgnoreCase(args[2])) {
                            FacAPI.addRecruta(FacAPI.getNomeWithTag(InvitationUtil.get(UUID).getTag()), UUID);
                            FacAPI.createInfoPlayer(UUID, FacAPI.getNomeWithTag(InvitationUtil.get(UUID).getTag()), "Recruta");
                            FacAPI.playerCreatePerms(UUID);
                            InvitationUtil.get(UUID).setInvitation(false);
                            player.sendMessage(ChatColor.GREEN + "\n * Você acabou de coligar se ao esquadrão [" + InvitationUtil.get(UUID).getTag() + "].\n" + ChatColor.DARK_GRAY + " * Para mais informações utilize: " + ChatColor.GRAY + "/e ajuda\n ");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            if (Main.scoreboardManager.hasSquadScoreboard(player)) {
                                Main.scoreboardManager.getSquadScoreboard().updateEntry(player, "squad_name");
                                Main.scoreboardManager.getSquadScoreboard().updateEntry(player, "squad_online");
                                Main.scoreboardManager.getSquadScoreboard().updateEntry(player, "squad_lands");
                                Main.scoreboardManager.getSquadScoreboard().updateEntry(player, "squad_power");
                            } else {
                                Main.scoreboardManager.getDefaultScoreboard().updateEntry(player, "squad");
                            }
                            String lista = null;
                            final String nome = FacAPI.getNomeWithTag(InvitationUtil.get(UUID).getTag());
                            if (!FacAPI.getRecrutas(nome).equalsIgnoreCase("NULL")) {
                                lista = FacAPI.getRecrutas(nome);
                            }
                            if (!FacAPI.getMembros(nome).equalsIgnoreCase("NULL")) {
                                if (lista == null) {
                                    lista = FacAPI.getMembros(nome);
                                } else {
                                    lista = lista + ":" + FacAPI.getMembros(nome);
                                }
                            }
                            if (!FacAPI.getCapitoes(nome).equalsIgnoreCase("NULL")) {
                                if (lista == null) {
                                    lista = FacAPI.getCapitoes(nome);
                                } else {
                                    lista = lista + ":" + FacAPI.getCapitoes(nome);
                                }
                            }
                            if (!FacAPI.getLideres(nome).equalsIgnoreCase("NULL")) {
                                if (lista == null) {
                                    lista = FacAPI.getLideres(nome);
                                } else {
                                    lista = lista + ":" + FacAPI.getLideres(nome);
                                }
                            }

                            final String[] list = lista.split(":");
                            for (int i = 1; i <= list.length; i++) {
                                final String UUIDtarget = list[i - 1];
                                if (AccountAPI.getNick(UUIDtarget) == null) return false;
                                final Player target = Bukkit.getPlayerExact(AccountAPI.getNick(UUIDtarget));

                                if (target != null && target.isOnline()) {
                                    target.sendMessage(ChatColor.YELLOW + "\n " + player.getName() + " acabou de coligar se a [" + nome + "]\n ");
                                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                } else {
                                    continue;
                                }
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, você não recebeu convite de [" + args[2] + "].");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, você não contém nenhum convite.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, você não contém nenhum convite.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, a tag informada deve conter de 2 à 3 argumentos.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, você já se encontra coligado a um esquadrão\n" + ChatColor.DARK_GRAY + " * Para sair do esquadrão utilize: " + ChatColor.GRAY + "/e sair");
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
        }
        return false;
    }
}
