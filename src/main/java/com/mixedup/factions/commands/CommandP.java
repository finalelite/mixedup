package com.mixedup.factions.commands;

import com.mixedup.apis.AccountAPI;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.hashs.ChatStatus;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandP implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String s, final String[] args) {
        Player player = (Player) sender;
        String UUID = player.getUniqueId().toString();

        if (cmd.getName().equalsIgnoreCase(".")) {
            if (FacAPI.getFacNome(UUID) != null) {
                final String msg = String.join(" ", args);

                if (ChatStatus.get(UUID) != null && ChatStatus.get(UUID).getStatus() == true) {
                    player.sendMessage(ChatColor.RED + " * Você está com chat do esquadrão habilitado, não necessitas utilizar /. para conversar com vosso esquadrão!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }

                final String nome = FacAPI.getFacNome(UUID);
                String lista = null;
                if (!FacAPI.getRecrutas(nome).equalsIgnoreCase("NULL")) {
                    lista = FacAPI.getRecrutas(nome);
                }
                if (!FacAPI.getMembros(nome).equalsIgnoreCase("NULL")) {
                    if (lista == null) {
                        lista = FacAPI.getMembros(nome);
                    } else {
                        lista = lista + ":" + FacAPI.getMembros(nome);
                    }
                }
                if (!FacAPI.getCapitoes(nome).equalsIgnoreCase("NULL")) {
                    if (lista == null) {
                        lista = FacAPI.getCapitoes(nome);
                    } else {
                        lista = lista + ":" + FacAPI.getCapitoes(nome);
                    }
                }
                if (!FacAPI.getLideres(nome).equalsIgnoreCase("NULL")) {
                    if (lista == null) {
                        lista = FacAPI.getLideres(nome);
                    } else {
                        lista = lista + ":" + FacAPI.getLideres(nome);
                    }
                }

                if (lista.contains(":")) {
                    final String[] list = lista.split(":");

                    for (int i = 1; i <= list.length; i++) {
                        if (AccountAPI.getNick(list[i - 1]) != null) {
                            final Player target = Bukkit.getPlayerExact(AccountAPI.getNick(list[i - 1]));
                            //OfflinePlayer target = Bukkit.getOfflinePlayer(java.util.UUID.fromString(list[i - 1]));

                            if (target != null && target.isOnline()) {
                                target.sendMessage(ChatColor.YELLOW + "[" + FacAPI.getHierarquia(player.getUniqueId().toString()) + "]" + player.getName() + ChatColor.DARK_GRAY + " : " + ChatColor.GRAY + msg);
                            }
                        }
                    }
                } else {
                    player.sendMessage(ChatColor.YELLOW + "[" + FacAPI.getHierarquia(player.getUniqueId().toString()) + "]" + player.getName() + ChatColor.DARK_GRAY + " : " + ChatColor.GRAY + msg);
                }
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, você não está coligado a nenhum esquadrão.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
        return false;
    }
}
