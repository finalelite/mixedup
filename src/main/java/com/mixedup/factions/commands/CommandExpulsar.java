package com.mixedup.factions.commands;

import com.mixedup.Main;
import com.mixedup.apis.AccountAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.factions.CommandsFac;
import com.mixedup.factions.FacAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class CommandExpulsar {

    public static boolean a(Player player, String UUID, String[] args) {
        if (FacAPI.getFacNome(UUID) != null) {
            if (FacAPI.getHierarquia(UUID).equalsIgnoreCase("Líder") || FacAPI.getPermExpulsar(UUID) == true) {
                if (PlayerUUID.getUUID(args[1]) != null) {
                    final String UUIDtarget = PlayerUUID.getUUID(args[1]);
                    final Player target = Bukkit.getPlayerExact(args[1]);

                    if (player.getName().equalsIgnoreCase(args[1])) {
                        player.sendMessage(ChatColor.RED + " * Ops, você não pode fazer isto!");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        return false;
                    }

                    if (FacAPI.getFacNome(UUIDtarget).equalsIgnoreCase(FacAPI.getNome(UUID))) {
                        CommandsFac.removePlayerFromFac(UUIDtarget);
                        FacAPI.deletePlayerInfo(UUIDtarget);
                        final String nome = FacAPI.getNome(UUID);

                        player.sendMessage(ChatColor.GREEN + " * " + args[1] + " acaba de ser removido do esquadrão.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                        if (target != null && target.isOnline()) {
                            if (Main.scoreboardManager.hasSquadScoreboard(target)) {
                                Main.scoreboardManager.getSquadScoreboard().updateEntry(target, "squad_name");
                                Main.scoreboardManager.getSquadScoreboard().updateEntry(target, "squad_online");
                                Main.scoreboardManager.getSquadScoreboard().updateEntry(target, "squad_lands");
                                Main.scoreboardManager.getSquadScoreboard().updateEntry(target, "squad_power");
                            } else {
                                Main.scoreboardManager.getDefaultScoreboard().updateEntry(target, "squad");
                            }

                            target.sendMessage(ChatColor.GREEN + " * Você acaba de ser removido do esquadrão por " + player.getName() + ".");
                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }

                        String lista = null;
                        if (!FacAPI.getRecrutas(nome).equalsIgnoreCase("NULL")) {
                            lista = FacAPI.getRecrutas(nome);
                        }
                        if (!FacAPI.getMembros(nome).equalsIgnoreCase("NULL")) {
                            if (lista == null) {
                                lista = FacAPI.getMembros(nome);
                            } else {
                                lista = lista + ":" + FacAPI.getMembros(nome);
                            }
                        }
                        if (!FacAPI.getCapitoes(nome).equalsIgnoreCase("NULL")) {
                            if (lista == null) {
                                lista = FacAPI.getCapitoes(nome);
                            } else {
                                lista = lista + ":" + FacAPI.getCapitoes(nome);
                            }
                        }
                        if (!FacAPI.getLideres(nome).equalsIgnoreCase("NULL")) {
                            if (lista == null) {
                                lista = FacAPI.getLideres(nome);
                            } else {
                                lista = lista + ":" + FacAPI.getLideres(nome);
                            }
                        }

                        final String[] list = lista.split(":");
                        for (int i = 1; i <= list.length; i++) {
                            final String targetUUID = list[i - 1];
                            final Player targetP = Bukkit.getPlayerExact(AccountAPI.getNick(targetUUID));

                            if (targetP != null && targetP.isOnline()) {
                                if (!targetP.getName().equalsIgnoreCase(player.getName())) {
                                    targetP.sendMessage(ChatColor.YELLOW + "\n " + target.getName() + " acabou de ser removido do esquadrão por " + player.getName() + "!\n ");
                                    targetP.playSound(targetP.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                            } else {
                                continue;
                            }
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, este player não pertence ao esquadrão.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, este player não contém registros em nossa rede.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, somente líder contém acesso a este comando.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
        return false;
    }
}
