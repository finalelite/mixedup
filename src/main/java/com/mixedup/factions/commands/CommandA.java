package com.mixedup.factions.commands;

import com.mixedup.apis.AccountAPI;
import com.mixedup.factions.FacAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandA implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        Player player = (Player) sender;
        String UUID = player.getUniqueId().toString();

        if (cmd.getName().equalsIgnoreCase("a")) {
            if (FacAPI.getFacNome(UUID) != null) {
                Boolean sended = false;
                if (FacAPI.getAliados(FacAPI.getFacNome(UUID)) != null && !FacAPI.getAliados(FacAPI.getFacNome(UUID)).equalsIgnoreCase("NULL")) {
                    if (args.length > 0) {
                        String msg = "";
                        for (final String teste : args) {
                            msg = msg + teste + " ";
                        }
                        msg.replaceAll("§", "&");

                        if (FacAPI.getFacNome(UUID) != null) {
                            final String nome = FacAPI.getFacNome(UUID);
                            String lista = null;
                            if (!FacAPI.getRecrutas(nome).equalsIgnoreCase("NULL")) {
                                lista = FacAPI.getRecrutas(nome);
                            }
                            if (!FacAPI.getMembros(nome).equalsIgnoreCase("NULL")) {
                                if (lista == null) {
                                    lista = FacAPI.getMembros(nome);
                                } else {
                                    lista = lista + ":" + FacAPI.getMembros(nome);
                                }
                            }
                            if (!FacAPI.getCapitoes(nome).equalsIgnoreCase("NULL")) {
                                if (lista == null) {
                                    lista = FacAPI.getCapitoes(nome);
                                } else {
                                    lista = lista + ":" + FacAPI.getCapitoes(nome);
                                }
                            }
                            if (!FacAPI.getLideres(nome).equalsIgnoreCase("NULL")) {
                                if (lista == null) {
                                    lista = FacAPI.getLideres(nome);
                                } else {
                                    lista = lista + ":" + FacAPI.getLideres(nome);
                                }
                            }

                            if (lista.contains(":")) {
                                sended = true;
                                final String[] people = lista.split(":");
                                for (int x = 1; x <= people.length; x++) {
                                    final Player ps = Bukkit.getPlayer(AccountAPI.getNick(people[x - 1]));

                                    if (ps != null && ps.isOnline()) {
                                        ps.sendMessage(ChatColor.GREEN + "[" + FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + "] " + player.getName() + ChatColor.GRAY + " : " + msg);
                                    }
                                }
                            } else {
                                sended = true;
                                final Player ps = Bukkit.getPlayer(AccountAPI.getNick(lista));

                                if (ps != null && ps.isOnline()) {
                                    ps.sendMessage(ChatColor.GREEN + "[" + FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + "] " + player.getName() + ChatColor.GRAY + " : " + msg);
                                }
                            }
                        }

                        if (FacAPI.getAliados(FacAPI.getFacNome(UUID)).contains(":")) {
                            final String[] split = FacAPI.getAliados(FacAPI.getFacNome(UUID)).split(":");
                            for (int i = 1; i <= split.length; i++) {
                                final String nome = FacAPI.getNomeWithTag(split[i - 1]);
                                String lista = null;
                                if (!FacAPI.getRecrutas(nome).equalsIgnoreCase("NULL")) {
                                    lista = FacAPI.getRecrutas(nome);
                                }
                                if (!FacAPI.getMembros(nome).equalsIgnoreCase("NULL")) {
                                    if (lista == null) {
                                        lista = FacAPI.getMembros(nome);
                                    } else {
                                        lista = lista + ":" + FacAPI.getMembros(nome);
                                    }
                                }
                                if (!FacAPI.getCapitoes(nome).equalsIgnoreCase("NULL")) {
                                    if (lista == null) {
                                        lista = FacAPI.getCapitoes(nome);
                                    } else {
                                        lista = lista + ":" + FacAPI.getCapitoes(nome);
                                    }
                                }
                                if (!FacAPI.getLideres(nome).equalsIgnoreCase("NULL")) {
                                    if (lista == null) {
                                        lista = FacAPI.getLideres(nome);
                                    } else {
                                        lista = lista + ":" + FacAPI.getLideres(nome);
                                    }
                                }

                                if (lista.contains(":")) {
                                    sended = true;
                                    final String[] people = lista.split(":");
                                    for (int x = 1; x <= people.length; x++) {
                                        final Player ps = Bukkit.getPlayer(AccountAPI.getNick(people[x - 1]));

                                        if (ps != null && ps.isOnline()) {
                                            ps.sendMessage(ChatColor.GREEN + "[" + FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + "] " + player.getName() + ChatColor.GRAY + " : " + msg);
                                        }

                                    }
                                } else {
                                    sended = true;
                                    for (final Player target : Bukkit.getOnlinePlayers()) {
                                        final Player ps = Bukkit.getPlayer(AccountAPI.getNick(lista));

                                        if (ps != null && ps.isOnline()) {
                                            ps.sendMessage(ChatColor.GREEN + "[" + FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + "] " + player.getName() + ChatColor.GRAY + " : " + msg);
                                        }
                                    }
                                }
                            }
                        } else {
                            final String nome = FacAPI.getNomeWithTag(FacAPI.getAliados(FacAPI.getFacNome(UUID)));
                            String lista = null;
                            if (!FacAPI.getRecrutas(nome).equalsIgnoreCase("NULL")) {
                                lista = FacAPI.getRecrutas(nome);
                            }
                            if (!FacAPI.getMembros(nome).equalsIgnoreCase("NULL")) {
                                if (lista == null) {
                                    lista = FacAPI.getMembros(nome);
                                } else {
                                    lista = lista + ":" + FacAPI.getMembros(nome);
                                }
                            }
                            if (!FacAPI.getCapitoes(nome).equalsIgnoreCase("NULL")) {
                                if (lista == null) {
                                    lista = FacAPI.getCapitoes(nome);
                                } else {
                                    lista = lista + ":" + FacAPI.getCapitoes(nome);
                                }
                            }
                            if (!FacAPI.getLideres(nome).equalsIgnoreCase("NULL")) {
                                if (lista == null) {
                                    lista = FacAPI.getLideres(nome);
                                } else {
                                    lista = lista + ":" + FacAPI.getLideres(nome);
                                }
                            }

                            if (lista.contains(":")) {
                                sended = true;
                                final String[] people = lista.split(":");
                                for (int x = 1; x <= people.length; x++) {

                                    final Player ps = Bukkit.getPlayer(AccountAPI.getNick(people[x - 1]));

                                    if (ps != null && ps.isOnline()) {
                                        ps.sendMessage(ChatColor.GREEN + "[" + FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + "] " + player.getName() + ChatColor.GRAY + " : " + msg);
                                    }
                                }
                            } else {
                                sended = true;
                                final Player ps = Bukkit.getPlayer(AccountAPI.getNick(lista));

                                ps.sendMessage(ChatColor.GREEN + "[" + FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + "] " + player.getName() + ChatColor.GRAY + " : " + msg);
                            }
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, para conversar no aliados digite algo após o /a");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
                //if (FacAPI.getAmigos(FacAPI.getFacNome(UUID)) != null && !FacAPI.getAmigos(FacAPI.getFacNome(UUID)).equalsIgnoreCase("NULL")) {
                //                    if (args.length > 0) {
                //                        String msg = String.join(" ", args);
                //
                //                        if (FacAPI.getFacNome(UUID) != null) {
                //                            String nome = FacAPI.getFacNome(UUID);
                //                            String lista = null;
                //                            if (!FacAPI.getRecrutas(nome).equalsIgnoreCase("NULL")) {
                //                                lista = FacAPI.getRecrutas(nome);
                //                            }
                //                            if (!FacAPI.getMembros(nome).equalsIgnoreCase("NULL")) {
                //                                if (lista == null) {
                //                                    lista = FacAPI.getMembros(nome);
                //                                } else {
                //                                    lista = lista + ":" + FacAPI.getMembros(nome);
                //                                }
                //                            }
                //                            if (!FacAPI.getCapitoes(nome).equalsIgnoreCase("NULL")) {
                //                                if (lista == null) {
                //                                    lista = FacAPI.getCapitoes(nome);
                //                                } else {
                //                                    lista = lista + ":" + FacAPI.getCapitoes(nome);
                //                                }
                //                            }
                //                            if (!FacAPI.getLideres(nome).equalsIgnoreCase("NULL")) {
                //                                if (lista == null) {
                //                                    lista = FacAPI.getLideres(nome);
                //                                } else {
                //                                    lista = lista + ":" + FacAPI.getLideres(nome);
                //                                }
                //                            }
                //
                //                            if (lista.contains(":")) {
                //                                sended = true;
                //                                String[] people = lista.split(":");
                //                                for (int x = 1; x <= people.length; x++) {
                //
                //                                    Player ps = Bukkit.getPlayer(AccountAPI.getNick(people[x - 1]));
                //
                //                                    if (ps != null && ps.isOnline()) {
                //
                //                                        ps.sendMessage(ChatColor.GREEN + "[" + FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + "] " + player.getName() + ChatColor.GRAY + " : " + msg);
                //
                //                                    }
                //
                //                                }
                //                            } else {
                //                                sended = true;
                //                                Player ps = Bukkit.getPlayer(AccountAPI.getNick(lista));
                //
                //                                if (ps != null && ps.isOnline()) {
                //
                //                                    ps.sendMessage(ChatColor.GREEN + "[" + FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + "] " + player.getName() + ChatColor.GRAY + " : " + msg);
                //
                //                                }
                //
                //                            }
                //                        }
                //
                //                        if (FacAPI.getAmigos(FacAPI.getFacNome(UUID)).contains(":")) {
                //                            String[] split = FacAPI.getAmigos(FacAPI.getFacNome(UUID)).split(":");
                //                            for (int i = 1; i <= split.length; i++) {
                //                                String nome = FacAPI.getNomeWithTag(split[i - 1]);
                //                                String lista = null;
                //                                if (!FacAPI.getRecrutas(nome).equalsIgnoreCase("NULL")) {
                //                                    lista = FacAPI.getRecrutas(nome);
                //                                }
                //                                if (!FacAPI.getMembros(nome).equalsIgnoreCase("NULL")) {
                //                                    if (lista == null) {
                //                                        lista = FacAPI.getMembros(nome);
                //                                    } else {
                //                                        lista = lista + ":" + FacAPI.getMembros(nome);
                //                                    }
                //                                }
                //                                if (!FacAPI.getCapitoes(nome).equalsIgnoreCase("NULL")) {
                //                                    if (lista == null) {
                //                                        lista = FacAPI.getCapitoes(nome);
                //                                    } else {
                //                                        lista = lista + ":" + FacAPI.getCapitoes(nome);
                //                                    }
                //                                }
                //                                if (!FacAPI.getLideres(nome).equalsIgnoreCase("NULL")) {
                //                                    if (lista == null) {
                //                                        lista = FacAPI.getLideres(nome);
                //                                    } else {
                //                                        lista = lista + ":" + FacAPI.getLideres(nome);
                //                                    }
                //                                }
                //
                //                                if (lista.contains(":")) {
                //                                    sended = true;
                //                                    String[] people = lista.split(":");
                //                                    for (int x = 1; x <= people.length; x++) {
                //
                //                                        Player ps = Bukkit.getPlayer(AccountAPI.getNick(people[x - 1]));
                //
                //                                        if (ps != null && ps.isOnline()) {
                //
                //                                            ps.sendMessage(ChatColor.GREEN + "[" + FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + "] " + player.getName() + ChatColor.GRAY + " : " + msg);
                //
                //                                        }
                //
                //                                    }
                //                                } else {
                //                                    sended = true;
                //                                    Player ps = Bukkit.getPlayer(AccountAPI.getNick(lista));
                //
                //                                    if (ps != null && ps.isOnline()) {
                //
                //                                        ps.sendMessage(ChatColor.GREEN + "[" + FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + "] " + player.getName() + ChatColor.GRAY + " : " + msg);
                //
                //                                    }
                //
                //                                }
                //                            }
                //                        } else {
                //                            String nome = FacAPI.getNomeWithTag(FacAPI.getAmigos(FacAPI.getFacNome(UUID)));
                //                            String lista = null;
                //                            if (!FacAPI.getRecrutas(nome).equalsIgnoreCase("NULL")) {
                //                                lista = FacAPI.getRecrutas(nome);
                //                            }
                //                            if (!FacAPI.getMembros(nome).equalsIgnoreCase("NULL")) {
                //                                if (lista == null) {
                //                                    lista = FacAPI.getMembros(nome);
                //                                } else {
                //                                    lista = lista + ":" + FacAPI.getMembros(nome);
                //                                }
                //                            }
                //                            if (!FacAPI.getCapitoes(nome).equalsIgnoreCase("NULL")) {
                //                                if (lista == null) {
                //                                    lista = FacAPI.getCapitoes(nome);
                //                                } else {
                //                                    lista = lista + ":" + FacAPI.getCapitoes(nome);
                //                                }
                //                            }
                //                            if (!FacAPI.getLideres(nome).equalsIgnoreCase("NULL")) {
                //                                if (lista == null) {
                //                                    lista = FacAPI.getLideres(nome);
                //                                } else {
                //                                    lista = lista + ":" + FacAPI.getLideres(nome);
                //                                }
                //                            }
                //
                //                            if (lista.contains(":")) {
                //                                sended = true;
                //                                String[] people = lista.split(":");
                //                                for (int x = 1; x <= people.length; x++) {
                //
                //                                    Player ps = Bukkit.getPlayer(AccountAPI.getNick(people[x - 1]));
                //
                //                                    if (ps != null && ps.isOnline()) {
                //
                //                                        ps.sendMessage(ChatColor.GREEN + "[" + FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + "] " + player.getName() + ChatColor.GRAY + " : " + msg);
                //
                //                                    }
                //
                //                                }
                //                            } else {
                //                                sended = true;
                //                                Player ps = Bukkit.getPlayer(AccountAPI.getNick(lista));
                //
                //                                if (ps != null && ps.isOnline()) {
                //
                //                                    ps.sendMessage(ChatColor.GREEN + "[" + FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + "] " + player.getName() + ChatColor.GRAY + " : " + msg);
                //
                //                                }
                //
                //                            }
                //                        }
                //                    } else {
                //                        player.sendMessage(ChatColor.RED + " * Ops, para conversar no aliados digite algo após o /a");
                //                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                //                    }
                //                }

                if (!sended) {
                    player.sendMessage(ChatColor.RED + " * Ops, vosso esquadrão não contém aliados ou amigos!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return false;
                }
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, você não está coligado a nenhum esquadrão.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
        return false;
    }
}
