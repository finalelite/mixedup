package com.mixedup.factions.commands;

import com.mixedup.factions.managers.InventorySquad;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.List;

public class CommandTop {

    public static boolean a(Player player) {
        final List<String> lore = InventorySquad.topSquads();
        String msg = null;

        for (int i = 1; i <= lore.size(); i++) {
            if (msg == null) {
                msg = lore.get(i - 1);
            } else {
                msg = msg + "\n" + lore.get(i - 1);
            }
        }

        player.sendMessage(msg);
        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        return false;
    }
}
