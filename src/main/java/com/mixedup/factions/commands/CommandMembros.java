package com.mixedup.factions.commands;

import com.mixedup.apis.AccountAPI;
import com.mixedup.factions.FacAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class CommandMembros {

    public static boolean a(Player player, String UUID) {
        if (FacAPI.getFacNome(UUID) != null) {
            final String nome = FacAPI.getFacNome(UUID);
            String lista = null;
            if (!FacAPI.getRecrutas(nome).equalsIgnoreCase("NULL")) {
                lista = FacAPI.getRecrutas(nome);
            }
            if (!FacAPI.getMembros(nome).equalsIgnoreCase("NULL")) {
                if (lista == null) {
                    lista = FacAPI.getMembros(nome);
                } else {
                    lista = lista + ":" + FacAPI.getMembros(nome);
                }
            }
            if (!FacAPI.getCapitoes(nome).equalsIgnoreCase("NULL")) {
                if (lista == null) {
                    lista = FacAPI.getCapitoes(nome);
                } else {
                    lista = lista + ":" + FacAPI.getCapitoes(nome);
                }
            }
            if (!FacAPI.getLideres(nome).equalsIgnoreCase("NULL")) {
                if (lista == null) {
                    lista = FacAPI.getLideres(nome);
                } else {
                    lista = lista + ":" + FacAPI.getLideres(nome);
                }
            }

            final Inventory inventory = Bukkit.createInventory(null, 5 * 9, "Membros esquadrão [" + FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + "]" + ": ");

            if (lista.contains(":")) {
                final String[] list = lista.split(":");

                int slot = 10;
                for (int i = 1; i <= 15; i++) {
                    if (i <= list.length) {
                        final ItemStack item = new ItemStack(Material.PLAYER_HEAD);
                        final SkullMeta meta = (SkullMeta) item.getItemMeta();
                        //meta.setOwner(AccountAPI.getNick(list[i - 1]));
                        meta.setDisplayName(ChatColor.YELLOW + "[" + FacAPI.getHierarquia(list[i - 1]) + "] " + AccountAPI.getNick(list[i - 1]));
                        item.setItemMeta(meta);

                        if (slot >= 10 && slot <= 16) {
                            inventory.setItem(slot, item);
                            if (slot == 16) {
                                slot = 19;
                            } else {
                                slot++;
                            }
                        } else if (slot >= 19 && slot <= 25) {
                            inventory.setItem(slot, item);
                            if (slot == 25) {
                                slot = 31;
                            } else {
                                slot++;
                            }
                        } else if (slot == 31) {
                            inventory.setItem(31, item);
                        }
                    } else {
                        final ItemStack item = new ItemStack(Material.PLAYER_HEAD);
                        final SkullMeta meta = (SkullMeta) item.getItemMeta();
                        meta.setDisplayName(" ");
                        item.setItemMeta(meta);

                        if (slot >= 10 && slot <= 16) {
                            inventory.setItem(slot, item);
                            if (slot == 16) {
                                slot = 19;
                            } else {
                                slot++;
                            }
                        } else if (slot >= 19 && slot <= 25) {
                            inventory.setItem(slot, item);
                            if (slot == 25) {
                                slot = 31;
                            } else {
                                slot++;
                            }
                        } else if (slot == 31) {
                            inventory.setItem(31, item);
                        }
                    }
                }
                player.openInventory(inventory);
            } else {
                int slot = 10;
                for (int i = 1; i <= 15; i++) {
                    if (lista != null && i == 1) {
                        final ItemStack item = new ItemStack(Material.PLAYER_HEAD);
                        final SkullMeta meta = (SkullMeta) item.getItemMeta();
                        //meta.setOwner(AccountAPI.getNick(lista));
                        meta.setDisplayName(ChatColor.WHITE + "[" + FacAPI.getHierarquia(lista) + "]" + AccountAPI.getNick(lista));
                        item.setItemMeta(meta);

                        if (slot >= 10 && slot <= 16) {
                            inventory.setItem(slot, item);
                            if (slot == 16) {
                                slot = 19;
                            } else {
                                slot++;
                            }
                        } else if (slot >= 19 && slot <= 25) {
                            inventory.setItem(slot, item);
                            if (slot == 25) {
                                slot = 31;
                            } else {
                                slot++;
                            }
                        } else if (slot == 31) {
                            inventory.setItem(31, item);
                        }
                    } else {
                        final ItemStack item = new ItemStack(Material.PLAYER_HEAD);
                        final SkullMeta meta = (SkullMeta) item.getItemMeta();
                        meta.setDisplayName(" ");
                        item.setItemMeta(meta);

                        if (slot >= 10 && slot <= 16) {
                            inventory.setItem(slot, item);
                            if (slot == 16) {
                                slot = 19;
                            } else {
                                slot++;
                            }
                        } else if (slot >= 19 && slot <= 25) {
                            inventory.setItem(slot, item);
                            if (slot == 25) {
                                slot = 31;
                            } else {
                                slot++;
                            }
                        } else if (slot == 31) {
                            inventory.setItem(31, item);
                        }
                    }
                }
                player.openInventory(inventory);
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, este esquadrão não existe.");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
        return false;
    }
}
