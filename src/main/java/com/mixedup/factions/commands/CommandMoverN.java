package com.mixedup.factions.commands;

import com.mixedup.Main;
import com.mixedup.apis.HorarioAPI;
import com.mixedup.economy.CoinsAPI;
import com.mixedup.factions.CommandsFac;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.hashs.NexusHash;
import com.mixedup.utils.WorldEditUtil;
import com.sk89q.worldedit.WorldEditException;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class CommandMoverN {

    public static boolean a(Player player, String UUID) {
        if (FacAPI.getFacNome(UUID) != null) {
            if (FacAPI.getHierarquia(UUID).equalsIgnoreCase("Líder") || FacAPI.getHierarquia(UUID).equalsIgnoreCase("Capitão")) {
                if (player.getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
                    if (!FacAPI.getNexus(FacAPI.getFacNome(UUID)).equalsIgnoreCase("NULL")) {
                        if (FacAPI.getChunkOwn(player.getChunk().getX() + ":" + player.getChunk().getZ()) != null &&
                                FacAPI.getChunkOwn(player.getChunk().getX() + ":" + player.getChunk().getZ()).equalsIgnoreCase(FacAPI.getFacNome(UUID))) {
                            if (FacAPI.getSobAttack(FacAPI.getFacNome(UUID)) == false) {
                                if (FacAPI.getTimeToMoveNexus(FacAPI.getFacNome(UUID)) == 0 || FacAPI.getTimeToMoveNexus(FacAPI.getFacNome(UUID)) - new Date().getTime() < 0) {
                                    if (CoinsAPI.getCoins(UUID) >= 25000) {
                                        final String[] split = FacAPI.getNexus(FacAPI.getFacNome(UUID)).split("::");
                                        final String[] pos1 = split[0].split(":");
                                        final Location loc1 = new Location(Bukkit.getWorld("Trappist-1b"), Integer.valueOf(pos1[0]), Integer.valueOf(pos1[1]), Integer.valueOf(pos1[2]));
                                        final String[] pos2 = split[1].split(":");
                                        final Location loc2 = new Location(Bukkit.getWorld("Trappist-1b"), Integer.valueOf(pos2[0]), Integer.valueOf(pos2[1]), Integer.valueOf(pos2[2]));

                                        try {
                                            WorldEditUtil.clear(WorldEditUtil.copy(WorldEditUtil.getRegion(loc1, loc2)));
                                        } catch (final WorldEditException e) {
                                            e.printStackTrace();
                                        }

                                        final Location newpos1 = player.getLocation().add(-6, -2, -6);
                                        final Location newpos2 = player.getLocation().add(6, 7, 6);
                                        NexusHash.get(FacAPI.getFacNome(UUID)).setPos1(newpos1.getBlockX() + ":" + newpos1.getBlockY() + ":" + newpos1.getBlockZ());
                                        NexusHash.get(FacAPI.getFacNome(UUID)).setPos2(newpos2.getBlockX() + ":" + newpos2.getBlockY() + ":" + newpos2.getBlockZ());
                                        FacAPI.updateNexus(FacAPI.getFacNome(UUID), newpos1.getBlockX() + ":" + newpos1.getBlockY() + ":" + newpos1.getBlockZ() + "::" + newpos2.getBlockX() + ":" + newpos2.getBlockY() + ":" + newpos2.getBlockZ());

                                        final String chunk = player.getChunk().getX() + ":" + player.getChunk().getZ();
                                        if (FacAPI.getExistProbab(chunk) == null) {
                                            final int probab = CommandsFac.probabGenerator();
                                            FacAPI.createProbabInfo(chunk, probab);
                                        }

                                        final Date date = new Date();
                                        date.setHours(date.getHours() + 72);
                                        FacAPI.updateTimeToMoveNexus(FacAPI.getFacNome(UUID), date.getTime());
                                        CoinsAPI.updateCoins(UUID, CoinsAPI.getCoins(UUID) - 25000);

                                        try {
                                            WorldEditUtil.paste(WorldEditUtil.load(Main.plugin.getDataFolder() + File.separator + "WorldEdit/schematics/nexus"), true, player.getLocation());
                                        } catch (final WorldEditException e) {
                                            e.printStackTrace();
                                        } catch (final IOException e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        player.sendMessage(ChatColor.RED + " * Ops, para mover o nexus você deve conter $25.000,00 moedas!");
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    }
                                } else {
                                    final int time = (int) (FacAPI.getTimeToMoveNexus(FacAPI.getFacNome(UUID)) - new Date().getTime()) / 1000;
                                    player.sendMessage(ChatColor.RED + " * Ops, não faz mais de 72 horas que o nexus foi movido\n  Faltam " + HorarioAPI.getTime(time) + ", para poder mover novamente!");
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, o esquadrão está sob ataque, não é permitido alterar a\n  posição do nexus sob ataque!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, para mover o nexus você deve estar dentro\n de uma das terras do esquadrão!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, o nexus não se encontra posicionado, caso queira\n posiciona lo em sua posição utilize /e gerar nexus!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, para utilizar este comando você deve estar no mundo de esquadrões!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, somente líder ou capitão contém acesso a este comando.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, você não está coligado a nenhum esquadrão.");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
        return false;
    }
}
