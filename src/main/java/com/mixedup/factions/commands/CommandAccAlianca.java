package com.mixedup.factions.commands;

import com.mixedup.apis.AccountAPI;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.hashs.InvitationsHash;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;

public class CommandAccAlianca {

    public static boolean a(Player player, String UUID, String[] args) {
        if (FacAPI.getFacNome(UUID) == null) {
            player.sendMessage(ChatColor.RED + " * Ops, você não contém esquadrão para utilizar este comando!");
            player.sendMessage(ChatColor.RED + " * Utilize " + ChatColor.GRAY + "/e ajuda" + ChatColor.RED + " para ver os comandos de esquadrões.");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            return false;
        }
        if (FacAPI.getHierarquia(UUID).equalsIgnoreCase("Líder")) {
            if (InvitationsHash.get(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)).toUpperCase()) != null) {
                final ArrayList<String> invitations = InvitationsHash.get(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)).toUpperCase()).getInvitations();
                if (Arrays.asList(invitations.toArray()).contains(args[2].toUpperCase())) {
                    if (FacAPI.getRivais(FacAPI.getFacNome(UUID)) == null) {
                        FacAPI.createRelacoes(FacAPI.getFacNome(UUID));
                    }
                    if (FacAPI.getRivais(FacAPI.getFacNome(UUID)).contains(":")) {
                        final String[] list = FacAPI.getRivais(FacAPI.getFacNome(UUID)).split(":");
                        if (list.length >= 3) {
                            player.getOpenInventory().close();
                            player.sendMessage(ChatColor.RED + " (!) Ops, você já atingiu limite de aliados!");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            return false;
                        }
                    }

                    boolean is = false;
                    if (FacAPI.getRivais(FacAPI.getFacNome(UUID)).contains(":")) {
                        final String[] split = FacAPI.getRivais(FacAPI.getFacNome(UUID)).split(":");
                        for (int i = 1; i <= split.length; i++) {
                            if (split[i - 1].equalsIgnoreCase(FacAPI.getNomeWithTag(args[2].toUpperCase()))) {
                                is = true;
                                player.sendMessage(ChatColor.RED + " * Ops, este esquadrão já é um aliado!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                break;
                            }
                        }
                    } else if (FacAPI.getRivais(FacAPI.getFacNome(UUID)).equalsIgnoreCase(FacAPI.getNomeWithTag(args[2].toUpperCase()))) {
                        player.sendMessage(ChatColor.RED + " * Ops, este esquadrão já é um aliado!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        return false;
                    }
                    if (is == true) {
                        return false;
                    }

                    //ATUALIZANDO ALIADOS DE QUEM DEU O COMANDO
                    if (!FacAPI.getAliados(FacAPI.getFacNome(UUID)).equals("NULL")) {
                        FacAPI.updateAliados(FacAPI.getFacNome(UUID), FacAPI.getAliados(FacAPI.getFacNome(UUID)) + ":" + args[2].toUpperCase());
                    } else {
                        FacAPI.updateAliados(FacAPI.getFacNome(UUID), args[2].toUpperCase());
                    }

                    //ATUALIZANDO ALIADOS DE QUEM ENVIOU O CONVITE
                    if (!FacAPI.getAliados(FacAPI.getNomeWithTag(args[2].toUpperCase())).equals("NULL")) {
                        FacAPI.updateAliados(FacAPI.getNomeWithTag(args[2].toUpperCase()), FacAPI.getAliados(FacAPI.getNomeWithTag(args[2].toUpperCase())) + ":" + FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)));
                    } else {
                        FacAPI.updateAliados(FacAPI.getNomeWithTag(args[2].toUpperCase()), FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)));
                    }
                    invitations.remove(args[2].toUpperCase());
                    //InvitationsHash.get(FacAPI.getFacNome(UUID)).setInvitations(invitations);

                    //ATUALIZANDO AMIGOS DE QUEM DIGITOU O COMANDO
                    if (FacAPI.getAmigos(FacAPI.getFacNome(UUID)) == null) {
                        FacAPI.createRelacoes(FacAPI.getFacNome(UUID));
                        FacAPI.updateAmigos(FacAPI.getFacNome(UUID), args[2].toUpperCase());
                    } else if (FacAPI.getAmigos(FacAPI.getFacNome(UUID)).equals("NULL")) {
                        FacAPI.updateAmigos(FacAPI.getFacNome(UUID), args[2].toUpperCase());
                    } else {
                        FacAPI.updateAmigos(FacAPI.getFacNome(UUID), FacAPI.getAmigos(FacAPI.getFacNome(UUID)) + ":" + args[2].toUpperCase());
                    }

                    //ATUALIZANDO AMIGOS DE QUEM ENVIOU O CONVITE
                    if (FacAPI.getAmigos(FacAPI.getNomeWithTag(args[2].toUpperCase())) == null) {
                        FacAPI.createRelacoes(FacAPI.getNomeWithTag(args[2].toUpperCase()));
                        FacAPI.updateAmigos(FacAPI.getNomeWithTag(args[2].toUpperCase()), FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)));
                    } else if (FacAPI.getAmigos(FacAPI.getNomeWithTag(args[2].toUpperCase())).equals("NULL")) {
                        FacAPI.updateAmigos(FacAPI.getNomeWithTag(args[2].toUpperCase()), FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)));
                    } else {
                        FacAPI.updateAmigos(FacAPI.getNomeWithTag(args[2].toUpperCase()), FacAPI.getAmigos(FacAPI.getNomeWithTag(args[2].toUpperCase())) + ":" + FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)));
                    }

                    final String inimigos2 = FacAPI.getInimigos(FacAPI.getNomeWithTag(args[2].toUpperCase()));
                    if (inimigos2.contains(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)))) {
                        if (inimigos2.contains(":")) {
                            final String[] all = inimigos2.split(":");
                            String newa = "NULL";
                            for (int i = 1; i <= all.length; i++) {
                                if (!all[i - 1].equalsIgnoreCase(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)))) {
                                    if (newa == "NULL") {
                                        newa = all[i - 1];
                                    } else {
                                        newa = newa + ":" + all[i - 1];
                                    }
                                }
                            }
                            FacAPI.updateInimigos(FacAPI.getNomeWithTag(args[2].toUpperCase()), newa);
                        } else {
                            FacAPI.updateInimigos(FacAPI.getNomeWithTag(args[2].toUpperCase()), "NULL");
                        }
                    }

                    final String inimigos = FacAPI.getInimigos(FacAPI.getFacNome(UUID));
                    if (inimigos.contains(args[2].toUpperCase())) {
                        if (inimigos.contains(":")) {
                            final String[] all = inimigos.split(":");
                            String newa = "NULL";
                            for (int i = 1; i <= all.length; i++) {
                                if (!all[i - 1].equalsIgnoreCase(args[2].toUpperCase())) {
                                    if (newa == "NULL") {
                                        newa = all[i - 1];
                                    } else {
                                        newa = newa + ":" + all[i - 1];
                                    }
                                }
                            }
                            FacAPI.updateInimigos(FacAPI.getFacNome(UUID), newa);
                        } else {
                            FacAPI.updateInimigos(FacAPI.getFacNome(UUID), "NULL");
                        }
                    }

                    final String rivais2 = FacAPI.getRivais(FacAPI.getNomeWithTag(args[2].toUpperCase()));
                    if (rivais2.contains(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)))) {
                        if (rivais2.contains(":")) {
                            final String[] all = rivais2.split(":");
                            String newa = "NULL";
                            for (int i = 1; i <= all.length; i++) {
                                if (!all[i - 1].equalsIgnoreCase(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)))) {
                                    if (newa == "NULL") {
                                        newa = all[i - 1];
                                    } else {
                                        newa = newa + ":" + all[i - 1];
                                    }
                                }
                            }
                            FacAPI.updateRivais(FacAPI.getNomeWithTag(args[2].toUpperCase()), newa);
                        } else {
                            FacAPI.updateRivais(FacAPI.getNomeWithTag(args[2].toUpperCase()), "NULL");
                        }
                    }

                    final String rivais = FacAPI.getRivais(FacAPI.getFacNome(UUID));
                    if (rivais.contains(args[2].toUpperCase())) {
                        if (rivais.contains(":")) {
                            final String[] all = rivais.split(":");
                            String newa = "NULL";
                            for (int i = 1; i <= all.length; i++) {
                                if (!all[i - 1].equalsIgnoreCase(args[2].toUpperCase())) {
                                    if (newa == "NULL") {
                                        newa = all[i - 1];
                                    } else {
                                        newa = newa + ":" + all[i - 1];
                                    }
                                }
                            }
                            FacAPI.updateRivais(FacAPI.getFacNome(UUID), newa);
                        } else {
                            FacAPI.updateRivais(FacAPI.getFacNome(UUID), "NULL");
                        }
                    }

                    player.getOpenInventory().close();

                    final String nome = FacAPI.getFacNome(UUID);
                    String lista = null;
                    if (!FacAPI.getRecrutas(nome).equals("NULL")) {
                        lista = FacAPI.getRecrutas(nome);
                    }
                    if (!FacAPI.getMembros(nome).equals("NULL")) {
                        if (lista == null) {
                            lista = FacAPI.getMembros(nome);
                        } else {
                            lista = lista + ":" + FacAPI.getMembros(nome);
                        }
                    }
                    if (!FacAPI.getCapitoes(nome).equals("NULL")) {
                        if (lista == null) {
                            lista = FacAPI.getCapitoes(nome);
                        } else {
                            lista = lista + ":" + FacAPI.getCapitoes(nome);
                        }
                    }
                    if (!FacAPI.getLideres(nome).equals("NULL")) {
                        if (lista == null) {
                            lista = FacAPI.getLideres(nome);
                        } else {
                            lista = lista + ":" + FacAPI.getLideres(nome);
                        }
                    }

                    if (lista.contains(":")) {
                        final String[] list = lista.split(":");
                        for (int i = 1; i <= list.length; i++) {
                            final String targetUUID = list[i - 1];
                            for (final Player target : Bukkit.getOnlinePlayers()) {
                                if (target.getName().equalsIgnoreCase(AccountAPI.getNick(targetUUID))) {
                                    target.sendMessage(ChatColor.GREEN + "\n (!) Vosso esquadrão acaba de aliar-se á [" + args[2].toUpperCase() + "]\n ");
                                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    target.playSound(target.getLocation(), Sound.ENTITY_VEX_CHARGE, 1.0f, 1.0f);
                                }
                            }
                        }
                    } else {
                        player.sendMessage(ChatColor.GREEN + "\n (!) Vosso esquadrão acaba de aliar-se á [" + args[2].toUpperCase() + "]\n ");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VEX_CHARGE, 1.0f, 1.0f);
                    }

                    final String nome2 = FacAPI.getNomeWithTag(args[2].toUpperCase());
                    String lista2 = null;
                    if (!FacAPI.getRecrutas(nome2).equals("NULL")) {
                        lista2 = FacAPI.getRecrutas(nome2);
                    }
                    if (!FacAPI.getMembros(nome2).equals("NULL")) {
                        if (lista2 == null) {
                            lista2 = FacAPI.getMembros(nome2);
                        } else {
                            lista2 = lista2 + ":" + FacAPI.getMembros(nome2);
                        }
                    }
                    if (!FacAPI.getCapitoes(nome2).equals("NULL")) {
                        if (lista2 == null) {
                            lista2 = FacAPI.getCapitoes(nome2);
                        } else {
                            lista2 = lista2 + ":" + FacAPI.getCapitoes(nome2);
                        }
                    }
                    if (!FacAPI.getLideres(nome2).equals("NULL")) {
                        if (lista2 == null) {
                            lista2 = FacAPI.getLideres(nome2);
                        } else {
                            lista2 = lista2 + ":" + FacAPI.getLideres(nome2);
                        }
                    }

                    if (lista2.contains(":")) {
                        final String[] list2 = lista2.split(":");
                        for (int i = 1; i <= list2.length; i++) {
                            final String targetUUID = list2[i - 1];
                            for (final Player target : Bukkit.getOnlinePlayers()) {
                                if (target.getName().equalsIgnoreCase(AccountAPI.getNick(targetUUID))) {
                                    target.sendMessage(ChatColor.GREEN + "\n (!) [" + FacAPI.getTag(UUID) + "] acaba de declarar apoio a vosso esquadrão.\n ");
                                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    target.playSound(target.getLocation(), Sound.ENTITY_VEX_CHARGE, 1.0f, 1.0f);
                                }
                            }
                        }
                    } else {
                        final String targetUUID = lista2;
                        for (final Player target : Bukkit.getOnlinePlayers()) {
                            if (target.getName().equalsIgnoreCase(AccountAPI.getNick(targetUUID))) {
                                target.sendMessage(ChatColor.GREEN + "\n (!) [" + FacAPI.getTag(UUID) + "] acaba de declarar apoio a vosso esquadrão.\n ");
                                target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                target.playSound(target.getLocation(), Sound.ENTITY_VEX_CHARGE, 1.0f, 1.0f);
                            }
                        }
                    }
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, vosso esquadrão não contém convite deste esquadrão!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, vosso esquadrão não contém convites!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, somente líder contém acesso a este comando.");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
        return false;
    }
}
