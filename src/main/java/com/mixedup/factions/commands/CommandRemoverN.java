package com.mixedup.factions.commands;

import com.mixedup.apis.HorarioAPI;
import com.mixedup.economy.CoinsAPI;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.hashs.NexusHash;
import com.mixedup.utils.WorldEditUtil;
import com.sk89q.worldedit.WorldEditException;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.Date;

public class CommandRemoverN {

    public static boolean a(Player player, String UUID) {
        if (FacAPI.getFacNome(UUID) != null) {
            if (FacAPI.getHierarquia(UUID).equalsIgnoreCase("Líder") || FacAPI.getHierarquia(UUID).equalsIgnoreCase("Capitão")) {
                if (!FacAPI.getNexus(FacAPI.getFacNome(UUID)).equalsIgnoreCase("NULL")) {
                    if (FacAPI.getSobAttack(FacAPI.getFacNome(UUID)) == false) {
                        if (FacAPI.getTimeToMoveNexus(FacAPI.getFacNome(UUID)) - new Date().getTime() < 0) {
                            if (CoinsAPI.getCoins(UUID) >= 25000) {
                                final String[] split = FacAPI.getNexus(FacAPI.getFacNome(UUID)).split("::");
                                final String[] pos1 = split[0].split(":");
                                final Location loc1 = new Location(Bukkit.getWorld("Trappist-1b"), Integer.valueOf(pos1[0]), Integer.valueOf(pos1[1]), Integer.valueOf(pos1[2]));
                                final String[] pos2 = split[1].split(":");
                                final Location loc2 = new Location(Bukkit.getWorld("Trappist-1b"), Integer.valueOf(pos2[0]), Integer.valueOf(pos2[1]), Integer.valueOf(pos2[2]));

                                try {
                                    WorldEditUtil.clear(WorldEditUtil.copy(WorldEditUtil.getRegion(loc1, loc2)));
                                    player.sendMessage(ChatColor.GREEN + " * Nexus removido com sucesso.");
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    FacAPI.updateNexus(FacAPI.getFacNome(UUID), "NULL");
                                    CoinsAPI.updateCoins(UUID, CoinsAPI.getCoins(UUID) - 25000);

                                    NexusHash.get(FacAPI.getFacNome(UUID)).setPos1("NULL");
                                    NexusHash.get(FacAPI.getFacNome(UUID)).setPos2("NULL");
                                    NexusHash.get(FacAPI.getFacNome(UUID)).setSquad("NULL");
                                } catch (final WorldEditException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, para mover o nexus você deve conter $25.000,00 moedas!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else {
                            final int time = (int) (FacAPI.getTimeToMoveNexus(FacAPI.getFacNome(UUID)) - new Date().getTime()) / 1000;
                            player.sendMessage(ChatColor.RED + " * Ops, não faz mais de 72 horas que o nexus foi alterado\n  Faltam " + HorarioAPI.getTime(time) + ", para poder altera-lo novamente!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, o esquadrão está sob ataque, não é permitido remover\n  o nexus sob ataque!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, o nexus não se encontra posicionado, caso queira\n posiciona lo em sua posição utilize /e gerar nexus!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, somente líder ou capitão contém acesso a este comando.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, você não está coligado a nenhum esquadrão.");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
        return false;
    }
}
