package com.mixedup.factions.commands;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.factions.CommandsFac;
import com.mixedup.factions.FacAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class CommandRebaixar {

    public static boolean a(Player player, String UUID, String[] args) {
        if (FacAPI.getFacNome(UUID) != null) {
            if (FacAPI.getHierarquia(UUID).equalsIgnoreCase("Líder")) {
                final String fac = FacAPI.getFacNome(UUID);

                if (PlayerUUID.getUUID(args[1]) != null) {
                    if (FacAPI.getFacNome(PlayerUUID.getUUID(args[1])).equalsIgnoreCase(fac)) {
                        if (!FacAPI.getHierarquia(PlayerUUID.getUUID(args[1])).equalsIgnoreCase("Recruta")) {
                            if (FacAPI.getHierarquia(PlayerUUID.getUUID(args[1])).equalsIgnoreCase("Membro")) {
                                final String UUIDtarget = PlayerUUID.getUUID(args[1]);
                                CommandsFac.removePlayerFromFac(UUIDtarget);
                                if (FacAPI.getRecrutas(fac).equalsIgnoreCase("NULL")) {
                                    FacAPI.updateRecrutas(fac, UUIDtarget);
                                } else {
                                    FacAPI.updateRecrutas(fac, FacAPI.getRecrutas(fac) + ":" + UUIDtarget);
                                }
                                FacAPI.updateHierarquia(PlayerUUID.getUUID(args[1]), "Recruta");
                                final Player target = Bukkit.getPlayerExact(args[1]);

                                if (target != null && target.isOnline()) {
                                    target.sendMessage(ChatColor.YELLOW + "\n * Você acaba de ser rebaixado a Recruta do esquadrão!\n ");
                                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                                player.sendMessage(ChatColor.GREEN + " * Você acaba de rebaixar " + args[1] + " á Recruta!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                            if (FacAPI.getHierarquia(PlayerUUID.getUUID(args[1])).equalsIgnoreCase("Capitão")) {
                                final String UUIDtarget = PlayerUUID.getUUID(args[1]);
                                CommandsFac.removePlayerFromFac(UUIDtarget);
                                if (FacAPI.getMembros(fac).equalsIgnoreCase("NULL")) {
                                    FacAPI.updateMembros(fac, UUIDtarget);
                                } else {
                                    FacAPI.updateMembros(fac, FacAPI.getMembros(fac) + ":" + UUIDtarget);
                                }
                                FacAPI.updateHierarquia(PlayerUUID.getUUID(args[1]), "Membro");
                                final Player target = Bukkit.getPlayerExact(args[1]);

                                if (target != null && target.isOnline()) {
                                    target.sendMessage(ChatColor.YELLOW + "\n * Você acaba de ser rebaixado a Membro do esquadrão!\n ");
                                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                                player.sendMessage(ChatColor.GREEN + " * Você acaba de rebaixar " + args[1] + " á Membro!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, este player já se encontra em seu menor cargo.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, este player não está coligado ao esquadrão.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, este player não contém registros em nossa rede.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, somente líder contém acesso a este comando.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, você não está coligado a nenhum esquadrão.");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
        return false;
    }
}
