package com.mixedup.factions.commands;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.hashs.TransferLeaderProv;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class CommandTransferir {

    public static boolean a(Player player, String UUID, String[] args) {
        if (FacAPI.getFacNome(UUID) != null) {
            if (FacAPI.getHierarquia(UUID).equalsIgnoreCase("Líder")) {
                if (PlayerUUID.getUUID(args[1]) != null) {
                    final String UUIDtarget = PlayerUUID.getUUID(args[1]);
                    if (TransferLeaderProv.get(UUID) == null) {
                        new TransferLeaderProv(UUID, UUIDtarget).insert();
                    } else {
                        TransferLeaderProv.get(UUID).setUUIDtarget(UUIDtarget);
                    }

                    final ItemStack confirmar = new ItemStack(Material.LIME_DYE);
                    final ItemMeta meta = confirmar.getItemMeta();
                    meta.setDisplayName(ChatColor.GREEN + "CONFIRMAR");
                    confirmar.setItemMeta(meta);

                    final ItemStack negar = new ItemStack(Material.ROSE_RED);
                    final ItemMeta meta1 = negar.getItemMeta();
                    meta1.setDisplayName(ChatColor.RED + "NEGAR");
                    negar.setItemMeta(meta1);

                    final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Transferir liderança esquadrão: ");
                    inventory.setItem(12, confirmar);
                    inventory.setItem(14, negar);

                    player.openInventory(inventory);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, este player não contém registros em nossa rede.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, somente líder contém acesso a este comando.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, você não está coligado a nenhum esquadrão.");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
        return false;
    }
}
