package com.mixedup.factions.commands;

import com.mixedup.apis.AccountAPI;
import com.mixedup.apis.KDRApi;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.PoderAPI;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.managers.InventorySquad;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class CommandPerfil {

    public static boolean a(Player player, String UUID, String[] args) {
        if (PlayerUUID.getUUIDcache(args[1]) != null) {
            final String UUIDtarget = PlayerUUID.getUUIDcache(args[1]);

            if (FacAPI.getFacNome(UUID) != null) {
                try {
                    final String msg = " \n " + ChatColor.GREEN + "Perfil:\n \n Jogador: " + ChatColor.GREEN + args[0] + "\n Esquadrão: " + ChatColor.GRAY + FacAPI.getTagWithNome(FacAPI.getFacNome(UUIDtarget)) + " - " + FacAPI.getFacNome(UUIDtarget) + "\n " + ChatColor.GREEN + "Poder do esquadrão: " + ChatColor.GRAY + "(" +
                            InventorySquad.getPoderSquad(FacAPI.getFacNome(UUIDtarget)) + "/225)\n " + ChatColor.GREEN + "Terras do esquadrão: " + ChatColor.GRAY + FacAPI.getTerras(FacAPI.getFacNome(UUIDtarget)) + "\n " + ChatColor.GREEN + "Líder do esquadrão: " + ChatColor.GRAY +
                            AccountAPI.getNick(FacAPI.getLideres(FacAPI.getFacNome(UUIDtarget))) + "\n " + ChatColor.GREEN + "Poder: " + ChatColor.GRAY + "(" + PoderAPI.getPoder(UUIDtarget) + "/15)\n " + ChatColor.GREEN + "Posição no rank: " + ChatColor.GRAY + "\n " + ChatColor.GREEN + "KDR: " + ChatColor.GRAY +
                            KDRApi.getKills(UUIDtarget) / KDRApi.getDeaths(UUIDtarget) + "\n " + ChatColor.GREEN + "Vítimas: " + ChatColor.GRAY + KDRApi.getKills(UUIDtarget) + "\n " + ChatColor.GREEN + "Mortes: " + ChatColor.GRAY + KDRApi.getDeaths(UUIDtarget) + "\n ";
                    player.sendMessage(msg);
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } catch (final Exception e) {
                    player.sendMessage(ChatColor.RED + " * Ops, algo ocorreu errado!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, este player não contém um esquadrão!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, este player não existe.");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
        return false;
    }
}
