package com.mixedup.factions.commands;

import com.mixedup.factions.FacAPI;
import com.mixedup.factions.managers.InventorySquad;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class CommandE {

    public static boolean a(String UUID, Player player) {
        if (FacAPI.getFacNome(UUID) != null) {
            player.openInventory(InventorySquad.getInventory(FacAPI.getFacNome(UUID)));
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            return false;
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, você não contém esquadrão para utilizar este comando!");
            player.sendMessage(ChatColor.RED + " * Utilize " + ChatColor.GRAY + "/e ajuda" + ChatColor.RED + " para ver os comandos de esquadrões.");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            return false;
        }
    }
}
