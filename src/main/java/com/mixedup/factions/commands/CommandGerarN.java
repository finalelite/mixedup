package com.mixedup.factions.commands;

import com.mixedup.Main;
import com.mixedup.factions.CommandsFac;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.hashs.NexusHash;
import com.mixedup.factions.hashs.NexusUtil;
import com.mixedup.utils.WorldEditUtil;
import com.sk89q.worldedit.WorldEditException;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;

public class CommandGerarN {

    public static boolean a(Player player, String UUID) {
        if (FacAPI.getFacNome(UUID) != null) {
            if (FacAPI.getHierarquia(UUID).equalsIgnoreCase("Líder") || FacAPI.getHierarquia(UUID).equalsIgnoreCase("Capitão")) {
                if (player.getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
                    if (FacAPI.getNexus(FacAPI.getFacNome(UUID)).equalsIgnoreCase("NULL")) {
                        if (FacAPI.getChunkOwn(player.getChunk().getX() + ":" + player.getChunk().getZ()) != null &&
                                FacAPI.getChunkOwn(player.getChunk().getX() + ":" + player.getChunk().getZ()).equalsIgnoreCase(FacAPI.getFacNome(UUID))) {
                            final Location pos1 = player.getLocation().add(-6, 0, -6);
                            final Location pos2 = player.getLocation().add(6, 7, 6);

                            if (pos1.getChunk().getX() != pos2.getChunk().getX() || pos1.getChunk().getZ() != pos2.getChunk().getZ()) {
                                player.sendMessage(ChatColor.RED + " * Ops, para definir o nexus você deve se encontrar ao centro do chunk.\n" + ChatColor.GRAY + "  Para visualizar o chunk utilioze: /e verterras");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                return false;
                            }

                            if (CommandsFac.haveBlocksInArea(pos1, pos2) == false) {
                                FacAPI.updateNexus(FacAPI.getFacNome(UUID), pos1.getBlockX() + ":" + (pos1.getBlockY() - 2) + ":" + pos1.getBlockZ() + "::" + pos2.getBlockX() + ":" + pos2.getBlockY() + ":" + pos2.getBlockZ());
                                player.sendMessage(ChatColor.GREEN + " * Nexus definido com sucesso!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                                final String chunk = player.getChunk().getX() + ":" + player.getChunk().getZ();
                                if (FacAPI.getExistProbab(chunk) == null) {
                                    final int probab = CommandsFac.probabGenerator();
                                    FacAPI.createProbabInfo(chunk, probab);
                                }

                                final NexusHash nexus = new NexusHash(FacAPI.getFacNome(UUID), pos1.getBlockX() + ":" + (pos1.getBlockY() - 2) + ":" + pos1.getBlockZ(), pos2.getBlockX() + ":" + pos2.getBlockY() + ":" + pos2.getBlockZ()).insert();
                                NexusUtil.getRegions().add(nexus);

                                try {
                                    WorldEditUtil.paste(WorldEditUtil.load(Main.plugin.getDataFolder() + File.separator + "WorldEdit/schematics/nexus"), true, player.getLocation());
                                } catch (final WorldEditException e) {
                                    e.printStackTrace();
                                } catch (final IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, para definir o nexus limpe uma area de 13x13x7 entorno de ti.\n   " + ChatColor.GRAY + "(Comprimento, largura, altura)");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, para definir o nexus você deve estar dentro\n de uma das terras do esquadrão!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, o nexus já se encontra posicionado, caso queira\n alterar sua posição utilize /e mover nexus!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, para utilizar este comando você deve estar no mundo de esquadrões!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, somente líder ou capitão contém acesso a este comando.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, você não está coligado a nenhum esquadrão.");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
        return false;
    }
}
