package com.mixedup.factions.commands;

import com.mixedup.Main;
import com.mixedup.apis.AccountAPI;
import com.mixedup.factions.CommandsFac;
import com.mixedup.factions.FacAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class CommandSair {

    public static boolean a(Player player, String UUID) {
        if (FacAPI.getFacNome(UUID) != null) {
            if (FacAPI.getHierarquia(UUID).equalsIgnoreCase("Líder")) {
                player.sendMessage(ChatColor.RED + " * Você é líder deste esquadrão, para abandonar o esquadrão\n" + ChatColor.DARK_GRAY + " * Utilize: " + ChatColor.GRAY + "/e desfazer ou /e transferir (jogador)");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                return false;
            }
            final String nome = FacAPI.getFacNome(UUID);
            CommandsFac.removePlayerFromFac(UUID);
            FacAPI.deletePlayerInfo(UUID);
            FacAPI.removerPlayerPerms(UUID);

            player.sendMessage(ChatColor.GREEN + "\n * Você acabou de abandonar vosso esquadrão.\n ");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

            String lista = null;
            if (!FacAPI.getRecrutas(nome).equalsIgnoreCase("NULL")) {
                lista = FacAPI.getRecrutas(nome);
            }
            if (!FacAPI.getMembros(nome).equalsIgnoreCase("NULL")) {
                if (lista == null) {
                    lista = FacAPI.getMembros(nome);
                } else {
                    lista = lista + ":" + FacAPI.getMembros(nome);
                }
            }
            if (!FacAPI.getCapitoes(nome).equalsIgnoreCase("NULL")) {
                if (lista == null) {
                    lista = FacAPI.getCapitoes(nome);
                } else {
                    lista = lista + ":" + FacAPI.getCapitoes(nome);
                }
            }
            if (!FacAPI.getLideres(nome).equalsIgnoreCase("NULL")) {
                if (lista == null) {
                    lista = FacAPI.getLideres(nome);
                } else {
                    lista = lista + ":" + FacAPI.getLideres(nome);
                }
            }

            final String[] list = lista.split(":");
            if (Main.scoreboardManager.hasSquadScoreboard(player)) {
                Main.scoreboardManager.getSquadScoreboard().updateEntry(player, "squad_name");
                Main.scoreboardManager.getSquadScoreboard().updateEntry(player, "squad_online");
                Main.scoreboardManager.getSquadScoreboard().updateEntry(player, "squad_lands");
                Main.scoreboardManager.getSquadScoreboard().updateEntry(player, "squad_power");
            } else {
                Main.scoreboardManager.getDefaultScoreboard().updateEntry(player, "squad");
            }
            for (int i = 1; i <= list.length; i++) {
                final String UUIDtarget = list[i - 1];
                if (AccountAPI.getNick(UUIDtarget) == null) return false;
                if (AccountAPI.getNick(UUIDtarget) == null) return false;
                final Player target = Bukkit.getPlayerExact(AccountAPI.getNick(UUIDtarget));

                if (target != null && target.isOnline()) {
                    target.sendMessage(ChatColor.YELLOW + "\n " + player.getName() + " acabou de descoligar se do esquadrão!\n ");
                    if (Main.scoreboardManager.hasSquadScoreboard(player)) {
                        Main.scoreboardManager.getSquadScoreboard().updateEntry(target, "squad_online");
                        Main.scoreboardManager.getSquadScoreboard().updateEntry(target, "squad_power");
                    }
                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    continue;
                }
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, você não está coligado a nenhum esquadrão.");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
        return false;
    }
}
