package com.mixedup.factions.commands;

import com.mixedup.factions.CommandsFac;
import com.mixedup.factions.FacAPI;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class CommandGeracao {

    public static boolean a(Player player) {
        if (player.getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
            final String chunk = player.getChunk().getX() + ":" + player.getChunk().getZ();
            if (FacAPI.getExistProbab(chunk) == null) {
                final int probab = CommandsFac.probabGenerator();
                FacAPI.createProbabInfo(chunk, probab);
                if (probab <= 5) {
                    player.sendMessage(ChatColor.YELLOW + " * Probabilidade de geração de plutõnio " + ChatColor.RED + probab + "%");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else if (probab > 5 && probab <= 10) {
                    player.sendMessage(ChatColor.YELLOW + " * Probabilidade de geração de plutõnio " + ChatColor.GOLD + probab + "%");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else if (probab > 10 && probab <= 15) {
                    player.sendMessage(ChatColor.YELLOW + " * Probabilidade de geração de plutõnio " + ChatColor.GREEN + probab + "%");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                }
            } else {
                final int probab = FacAPI.getProbab(chunk);
                if (probab <= 5) {
                    player.sendMessage(ChatColor.YELLOW + " * Probabilidade de geração de plutõnio " + ChatColor.RED + probab + "%");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else if (probab > 5 && probab <= 10) {
                    player.sendMessage(ChatColor.YELLOW + " * Probabilidade de geração de plutõnio " + ChatColor.GOLD + probab + "%");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else if (probab > 10 && probab <= 15) {
                    player.sendMessage(ChatColor.YELLOW + " * Probabilidade de geração de plutõnio " + ChatColor.GREEN + probab + "%");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                }
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, para utilizar este comando você deve estar no mundo de esquadrões!");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
        return false;
    }
}
