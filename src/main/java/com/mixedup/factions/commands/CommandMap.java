package com.mixedup.factions.commands;

import com.mixedup.factions.CommandsFac;
import com.mixedup.factions.FacAPI;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class CommandMap {

    public static boolean a(Player player, String UUID) {
        if (FacAPI.getFacNome(UUID) == null) {
            player.sendMessage(ChatColor.RED + " * Ops, você não contém esquadrão para utilizar este comando!");
            player.sendMessage(ChatColor.RED + " * Utilize " + ChatColor.GRAY + "/e ajuda" + ChatColor.RED + " para ver os comandos de esquadrões.");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            return false;
        }
        if (player.getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
            CommandsFac.sendMap(player);
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, para utilizar este comando você deve estar no mundo de esquadrões!");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
        return false;
    }
}
