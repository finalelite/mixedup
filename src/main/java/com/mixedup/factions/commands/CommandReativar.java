package com.mixedup.factions.commands;

import com.mixedup.Main;
import com.mixedup.economy.CoinsAPI;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.hashs.InvadedHash;
import com.mixedup.utils.WorldEditUtil;
import com.sk89q.worldedit.WorldEditException;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;

public class CommandReativar {

    public static boolean a(Player player, String UUID) {
        if (FacAPI.getFacNome(UUID) != null) {
            if (FacAPI.getHierarquia(UUID).equalsIgnoreCase("Líder") || FacAPI.getHierarquia(UUID).equalsIgnoreCase("Capitão")) {
                if (!FacAPI.getNexus(FacAPI.getFacNome(UUID)).equalsIgnoreCase("NULL")) {
                    if (FacAPI.getSobAttack(FacAPI.getFacNome(UUID)) == false) {
                        if (FacAPI.getNexusLife(FacAPI.getFacNome(UUID)) < 0) {
                            if (CoinsAPI.getCoins(UUID) >= 50000) {
                                final String[] s = FacAPI.getNexus(FacAPI.getFacNome(UUID)).split("::");
                                final String[] split1 = s[0].split(":");
                                final String[] split2 = s[1].split(":");
                                final Location p1 = new Location(Bukkit.getWorld("Trappist-1b"), Integer.valueOf(split1[0]), Integer.valueOf(split1[1]), Integer.valueOf(split1[2]));
                                final Location p2 = new Location(Bukkit.getWorld("Trappist-1b"), Integer.valueOf(split2[0]), Integer.valueOf(split2[1]), Integer.valueOf(split2[2]));
                                final Location result = new Location(p1.getWorld(), (p1.getBlockX() + p2.getBlockX()) / 2, ((p1.getBlockY() + p2.getBlockY()) / 2) - 2, (p1.getBlockZ() + p2.getBlockZ()) / 2);

                                try {
                                    WorldEditUtil.paste(WorldEditUtil.load(Main.plugin.getDataFolder() + File.separator + "WorldEdit/schematics/nexus"), true, result);
                                    player.sendMessage(ChatColor.GREEN + " * Nexus reativado com sucesso.");
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    FacAPI.updateLifeNexus(FacAPI.getFacNome(UUID), 10);
                                    if (InvadedHash.get(FacAPI.getFacNome(UUID)) != null) {
                                        InvadedHash.get(FacAPI.getFacNome(UUID)).setStatus(false);
                                    } else {
                                        new InvadedHash(FacAPI.getFacNome(UUID), false).insert();
                                    }
                                } catch (final WorldEditException e) {
                                    e.printStackTrace();
                                } catch (final IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, para reativar o nexus você deve conter $50.000,00 moedas!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, o nexus não se encontra destruido!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, o esquadrão está sob ataque, não é permitido reativar\n  o nexus sob ataque!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, o nexus não se encontra posicionado, caso queira\n posiciona lo em sua posição utilize /e gerar nexus!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, somente líder ou capitão contém acesso a este comando.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, você não está coligado a nenhum esquadrão.");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
        return false;
    }
}
