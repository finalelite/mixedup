package com.mixedup.factions.commands;

import com.mixedup.factions.CommandsFac;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.hashs.NexusHash;
import com.mixedup.factions.hashs.NexusUtil;
import com.mixedup.utils.WorldEditUtil;
import com.sk89q.worldedit.WorldEditException;
import org.bukkit.*;
import org.bukkit.entity.Player;

public class CommandAbandonar {

    public static boolean a(Player player, String UUID, String[] args) {
        if (args[1].equalsIgnoreCase("todos")) {
            if (FacAPI.getFacNome(UUID) != null) {
                if (FacAPI.getHierarquia(UUID).equalsIgnoreCase("Líder") || FacAPI.getHierarquia(UUID).equalsIgnoreCase("Capitão") || FacAPI.getPermAbandonar(UUID) == true) {
                    if (!FacAPI.getNexus(FacAPI.getFacNome(UUID)).equalsIgnoreCase("NULL")) {
                        if (FacAPI.getTerras(FacAPI.getFacNome(UUID)) > 0) {
                            FacAPI.updateTerras(FacAPI.getFacNome(UUID), 0);
                            player.sendMessage(ChatColor.GREEN + " * Todas as terras foram abandonadas!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                            final String[] split = FacAPI.getNexus(FacAPI.getFacNome(UUID)).split("::");
                            final String[] pos1 = split[0].split(":");
                            final Location loc1 = new Location(Bukkit.getWorld("Trappist-1b"), Integer.valueOf(pos1[0]), Integer.valueOf(pos1[1]), Integer.valueOf(pos1[2]));
                            final String[] pos2 = split[1].split(":");
                            final Location loc2 = new Location(Bukkit.getWorld("Trappist-1b"), Integer.valueOf(pos2[0]), Integer.valueOf(pos2[1]), Integer.valueOf(pos2[2]));

                            try {
                                WorldEditUtil.clear(WorldEditUtil.copy(WorldEditUtil.getRegion(loc1, loc2)));
                                FacAPI.updateNexus(FacAPI.getFacNome(UUID), "NULL");
                                CommandsFac.removeAllChunks(FacAPI.getFacNome(UUID));

                                NexusHash.get(FacAPI.getFacNome(UUID)).setPos1("NULL");
                                NexusHash.get(FacAPI.getFacNome(UUID)).setPos2("NULL");
                                NexusHash.get(FacAPI.getFacNome(UUID)).setSquad("NULL");
                            } catch (final WorldEditException e) {
                                e.printStackTrace();
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, vosso esquadrão não contém nenhuma terra.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        if (FacAPI.getTerras(FacAPI.getFacNome(UUID)) > 0) {
                            FacAPI.updateTerras(FacAPI.getFacNome(UUID), 0);
                            CommandsFac.removeAllChunks(FacAPI.getFacNome(UUID));
                            player.sendMessage(ChatColor.GREEN + " * Todas as terras foram abandonadas!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, vosso esquadrão não contém nenhuma terra.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    }
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, somente líder ou capitão contém acesso a este comando.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, você não está coligado a nenhum esquadrão.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        } else if (args[1].equalsIgnoreCase("um")) {
            if (FacAPI.getFacNome(UUID) != null) {
                if (FacAPI.getHierarquia(UUID).equalsIgnoreCase("Líder") || FacAPI.getHierarquia(UUID).equalsIgnoreCase("Capitão")) {
                    if (FacAPI.getTerras(FacAPI.getFacNome(UUID)) > 0) {
                        if (player.getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
                            final String chunk = player.getChunk().getX() + ":" + player.getChunk().getZ();
                            if (FacAPI.getChunkOwn(chunk).equalsIgnoreCase(FacAPI.getFacNome(UUID))) {
                                final Chunk c = player.getLocation().getChunk();
                                final Location center = new Location(c.getWorld(), c.getX() << 4, 64, c.getZ() << 4).add(7, 0, 7);
                                if (NexusUtil.playerInArea(center) != null) {
                                    FacAPI.updateTerras(FacAPI.getFacNome(UUID), FacAPI.getTerras(FacAPI.getFacNome(UUID)) - 1);
                                    player.sendMessage(ChatColor.GREEN + " * Terra abandonada com sucesso!");
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                                    final String[] split = FacAPI.getNexus(FacAPI.getFacNome(UUID)).split("::");
                                    final String[] pos1 = split[0].split(":");
                                    final Location loc1 = new Location(Bukkit.getWorld("Trappist-1b"), Integer.valueOf(pos1[0]), Integer.valueOf(pos1[1]), Integer.valueOf(pos1[2]));
                                    final String[] pos2 = split[1].split(":");
                                    final Location loc2 = new Location(Bukkit.getWorld("Trappist-1b"), Integer.valueOf(pos2[0]), Integer.valueOf(pos2[1]), Integer.valueOf(pos2[2]));

                                    try {
                                        WorldEditUtil.clear(WorldEditUtil.copy(WorldEditUtil.getRegion(loc1, loc2)));
                                        FacAPI.updateNexus(FacAPI.getFacNome(UUID), "NULL");
                                        FacAPI.removeChunk(chunk);

                                        //--
                                    } catch (final WorldEditException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    if (FacAPI.getTerras(FacAPI.getFacNome(UUID)) > 0) {
                                        FacAPI.updateTerras(FacAPI.getFacNome(UUID), FacAPI.getTerras(FacAPI.getFacNome(UUID)) - 1);
                                        FacAPI.removeChunk(chunk);
                                        player.sendMessage(ChatColor.GREEN + " * Terra abandonada com sucesso!");
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    } else {
                                        player.sendMessage(ChatColor.RED + " * Ops, vosso esquadrão não contém nenhuma terra.");
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    }
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, você deve se encontrar em vossa terra para efetuar este comando!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, para utilizar este comando você deve estar no mundo de esquadrões!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, vosso esquadrão não contém nenhuma terra.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, somente líder ou capitão contém acesso a este comando.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, você não está coligado a nenhum esquadrão.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, algum argumento encontra-se errado!");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
        return false;
    }
}
