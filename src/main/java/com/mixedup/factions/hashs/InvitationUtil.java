package com.mixedup.factions.hashs;

import java.util.HashMap;

public class InvitationUtil {

    private static final HashMap<String, InvitationUtil> CACHE = new HashMap<String, InvitationUtil>();
    private final String UUID;
    private boolean invitation;
    private String tag;

    public InvitationUtil(final String UUID, final boolean invitation, final String tag) {
        this.UUID = UUID;
        this.invitation = invitation;
        this.tag = tag;
    }

    public static InvitationUtil get(final String UUID) {
        return InvitationUtil.CACHE.get(String.valueOf(UUID));
    }

    public InvitationUtil insert() {
        InvitationUtil.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(final String tag) {
        this.tag = tag;
    }

    public boolean getInvitation() {
        return invitation;
    }

    public void setInvitation(final boolean invitation) {
        this.invitation = invitation;
    }

    public String getUUID() {
        return UUID;
    }
}
