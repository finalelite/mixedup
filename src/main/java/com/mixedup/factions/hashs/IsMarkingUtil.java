package com.mixedup.factions.hashs;

import java.util.HashMap;

public class IsMarkingUtil {

    private static final HashMap<String, IsMarkingUtil> CACHE = new HashMap<String, IsMarkingUtil>();
    private final String UUID;
    private boolean marking;

    public IsMarkingUtil(final String UUID, final boolean marking) {
        this.UUID = UUID;
        this.marking = marking;
    }

    public static IsMarkingUtil get(final String UUID) {
        return IsMarkingUtil.CACHE.get(String.valueOf(UUID));
    }

    public IsMarkingUtil insert() {
        IsMarkingUtil.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getmarking() {
        return marking;
    }

    public void setmarking(final boolean marking) {
        this.marking = marking;
    }

    public String getUUID() {
        return UUID;
    }
}
