package com.mixedup.factions.hashs;

import org.bukkit.boss.BossBar;

import java.util.HashMap;

public class LifeNexusCache {

    public static HashMap<String, LifeNexusCache> cache = new HashMap<>();
    private final String facName;
    private final BossBar bossBar;

    public LifeNexusCache(final String facName, final BossBar bossBar) {
        this.facName = facName;
        this.bossBar = bossBar;
    }

    public static LifeNexusCache get(final String facName) {
        return LifeNexusCache.cache.get(facName);
    }

    public LifeNexusCache insert() {
        LifeNexusCache.cache.put(facName, this);
        return this;
    }

    public BossBar getBossBar() {
        return bossBar;
    }
}
