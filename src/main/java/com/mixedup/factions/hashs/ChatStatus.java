package com.mixedup.factions.hashs;

import java.util.HashMap;

public class ChatStatus {

    private static final HashMap<String, ChatStatus> CACHE = new HashMap<String, ChatStatus>();
    private final String UUID;
    private boolean Status;

    public ChatStatus(final String UUID, final boolean Status) {
        this.UUID = UUID;
        this.Status = Status;
    }

    public static ChatStatus get(final String UUID) {
        return ChatStatus.CACHE.get(String.valueOf(UUID));
    }

    public ChatStatus insert() {
        ChatStatus.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(final boolean Status) {
        this.Status = Status;
    }

    public String getUUID() {
        return UUID;
    }
}
