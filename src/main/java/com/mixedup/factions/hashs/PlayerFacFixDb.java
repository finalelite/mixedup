package com.mixedup.factions.hashs;

import java.util.HashMap;

public class PlayerFacFixDb {

    public static HashMap<String, PlayerFacFixDb> cache = new HashMap<>();
    private final String uuid;
    private boolean status;

    public PlayerFacFixDb(final String uuid, final boolean status) {
        this.uuid = uuid;
        this.status = status;
    }

    public static PlayerFacFixDb get(final String uuid) {
        return PlayerFacFixDb.cache.get(uuid);
    }

    public PlayerFacFixDb insert() {
        PlayerFacFixDb.cache.put(uuid, this);
        return this;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(final boolean status) {
        this.status = status;
    }
}
