package com.mixedup.factions.hashs;

import java.util.HashMap;

public class TntLevelUtil {

    public static HashMap<String, TntLevelUtil> CACHE = new HashMap<String, TntLevelUtil>();
    private String location;
    private int Level;

    public TntLevelUtil(final String location, final int Level) {
        this.location = location;
        this.Level = Level;
    }

    public static TntLevelUtil get(final String UUID) {
        return TntLevelUtil.CACHE.get(String.valueOf(UUID));
    }

    public TntLevelUtil insert() {
        TntLevelUtil.CACHE.put(String.valueOf(location), this);

        return this;
    }

    public int getLevel() {
        return Level;
    }

    public void setLevel(final int Level) {
        this.Level = Level;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(final String location) {
        this.location = location;
    }
}
