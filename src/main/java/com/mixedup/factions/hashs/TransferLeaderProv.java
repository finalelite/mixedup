package com.mixedup.factions.hashs;

import java.util.HashMap;

public class TransferLeaderProv {

    private static final HashMap<String, TransferLeaderProv> CACHE = new HashMap<String, TransferLeaderProv>();
    private String UUID;
    private String UUIDtarget;

    public TransferLeaderProv(final String UUID, final String UUIDtarget) {
        this.UUID = UUID;
        this.UUIDtarget = UUIDtarget;
    }

    public static TransferLeaderProv get(final String UUID) {
        return TransferLeaderProv.CACHE.get(String.valueOf(UUID));
    }

    public TransferLeaderProv insert() {
        TransferLeaderProv.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public String getUUIDtarget() {
        return UUIDtarget;
    }

    public void setUUIDtarget(final String UUIDtarget) {
        this.UUIDtarget = UUIDtarget;
    }

    public String getUUID() {
        return UUIDtarget;
    }

    public void setUUID(final String UUID) {
        this.UUID = UUID;
    }
}
