package com.mixedup.factions.hashs;

import java.util.HashMap;

public class EconomyHash {

    private static final HashMap<String, EconomyHash> CACHE = new HashMap<String, EconomyHash>();
    private final String UUID;
    private int economy;

    public EconomyHash(final String UUID, final int economy) {
        this.UUID = UUID;
        this.economy = economy;
    }

    public static EconomyHash get(final String UUID) {
        return EconomyHash.CACHE.get(String.valueOf(UUID));
    }

    public EconomyHash insert() {
        EconomyHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public int getEconomy() {
        return economy;
    }

    public void setEconomy(final int economy) {
        this.economy = economy;
    }

    public String getUUID() {
        return UUID;
    }
}
