package com.mixedup.factions.hashs;

import java.util.HashMap;

public class PlayerInZone {

    private static final HashMap<String, PlayerInZone> CACHE = new HashMap<String, PlayerInZone>();
    private String UUID;
    private String Zone;

    public PlayerInZone(final String UUID, final String Zone) {
        this.UUID = UUID;
        this.Zone = Zone;
    }

    public static PlayerInZone get(final String UUID) {
        return PlayerInZone.CACHE.get(String.valueOf(UUID));
    }

    public PlayerInZone insert() {
        PlayerInZone.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public String getZone() {
        return Zone;
    }

    public void setZone(final String Zone) {
        this.Zone = Zone;
    }

    public String getUUID() {
        return Zone;
    }

    public void setUUID(final String UUID) {
        this.UUID = UUID;
    }
}
