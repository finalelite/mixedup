package com.mixedup.factions.hashs;

import java.util.ArrayList;
import java.util.HashMap;

public class InvitationsHash {

    public static HashMap<String, InvitationsHash> CACHE = new HashMap<>();
    private final String squad;
    private ArrayList<String> invitations;

    public InvitationsHash(final String squad, final ArrayList<String> invitations) {
        this.squad = squad;
        this.invitations = invitations;
    }

    public static InvitationsHash get(final String squad) {
        return InvitationsHash.CACHE.get(squad);
    }

    public InvitationsHash insert() {
        return InvitationsHash.CACHE.put(squad, this);
    }

    public ArrayList<String> getInvitations() {
        return invitations;
    }

    public void setInvitations(final ArrayList<String> invitations) {
        this.invitations = invitations;
    }
}
