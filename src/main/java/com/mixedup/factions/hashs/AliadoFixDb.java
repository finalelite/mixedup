package com.mixedup.factions.hashs;

import java.util.HashMap;

public class AliadoFixDb {

    public static HashMap<String, AliadoFixDb> cache = new HashMap<>();
    private final String uuid;
    private boolean status;

    public AliadoFixDb(final String uuid, final boolean status) {
        this.uuid = uuid;
        this.status = status;
    }

    public static AliadoFixDb get(final String uuid) {
        return AliadoFixDb.cache.get(uuid);
    }

    public AliadoFixDb insert() {
        AliadoFixDb.cache.put(uuid, this);
        return this;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(final boolean status) {
        this.status = status;
    }
}
