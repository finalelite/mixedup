package com.mixedup.factions.hashs;

import java.util.HashMap;

public class SquadName {

    private static final HashMap<String, SquadName> CACHE = new HashMap<String, SquadName>();
    private String UUID;
    private String Squad;

    public SquadName(final String UUID, final String Squad) {
        this.UUID = UUID;
        this.Squad = Squad;
    }

    public static SquadName get(final String UUID) {
        return SquadName.CACHE.get(String.valueOf(UUID));
    }

    public SquadName insert() {
        SquadName.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public String getSquad() {
        return Squad;
    }

    public void setSquad(final String Squad) {
        this.Squad = Squad;
    }

    public String getUUID() {
        return Squad;
    }

    public void setUUID(final String UUID) {
        this.UUID = UUID;
    }
}
