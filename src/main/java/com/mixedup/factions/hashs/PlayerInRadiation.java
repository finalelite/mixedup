package com.mixedup.factions.hashs;

import java.util.HashMap;

public class PlayerInRadiation {

    private static final HashMap<String, PlayerInRadiation> CACHE = new HashMap<String, PlayerInRadiation>();
    private String UUID;
    private boolean Zone;
    private int regionID;

    public PlayerInRadiation(final String UUID, final boolean Zone, final int regionID) {
        this.UUID = UUID;
        this.Zone = Zone;
        this.regionID = regionID;
    }

    public static PlayerInRadiation get(final String UUID) {
        return PlayerInRadiation.CACHE.get(String.valueOf(UUID));
    }

    public PlayerInRadiation insert() {
        PlayerInRadiation.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public int getRegionID() {
        return regionID;
    }

    public void setRegionID(final int regionID) {
        this.regionID = regionID;
    }

    public boolean getZone() {
        return Zone;
    }

    public void setZone(final boolean Zone) {
        this.Zone = Zone;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(final String UUID) {
        this.UUID = UUID;
    }
}
