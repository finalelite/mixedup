package com.mixedup.factions.hashs;

import java.util.HashMap;

public class TimeAttachHash {

    private static final HashMap<String, TimeAttachHash> CACHE = new HashMap<String, TimeAttachHash>();
    private final String Nome;
    private long Time;

    public TimeAttachHash(final String Nome, final long Time) {
        this.Nome = Nome;
        this.Time = Time;
    }

    public static TimeAttachHash get(final String Nome) {
        return TimeAttachHash.CACHE.get(String.valueOf(Nome));
    }

    public TimeAttachHash insert() {
        TimeAttachHash.CACHE.put(String.valueOf(Nome), this);

        return this;
    }

    public long getTime() {
        return Time;
    }

    public void setTime(final long Time) {
        this.Time = Time;
    }

    public String getNome() {
        return Nome;
    }
}
