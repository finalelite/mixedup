package com.mixedup.factions.hashs;

import java.util.HashMap;

public class PermFixDb {

    public static HashMap<String, PermFixDb> cache = new HashMap<>();
    private final String uuid;
    private boolean status;

    public PermFixDb(final String uuid, final boolean status) {
        this.uuid = uuid;
        this.status = status;
    }

    public static PermFixDb get(final String uuid) {
        return PermFixDb.cache.get(uuid);
    }

    public PermFixDb insert() {
        PermFixDb.cache.put(uuid, this);
        return this;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(final boolean status) {
        this.status = status;
    }
}
