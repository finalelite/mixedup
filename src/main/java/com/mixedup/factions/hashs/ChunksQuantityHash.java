package com.mixedup.factions.hashs;

import java.util.HashMap;

public class ChunksQuantityHash {

    private static final HashMap<String, ChunksQuantityHash> CACHE = new HashMap<String, ChunksQuantityHash>();
    private final String UUID;
    private int quantity;

    public ChunksQuantityHash(final String UUID, final int quantity) {
        this.UUID = UUID;
        this.quantity = quantity;
    }

    public static ChunksQuantityHash get(final String UUID) {
        return ChunksQuantityHash.CACHE.get(String.valueOf(UUID));
    }

    public ChunksQuantityHash insert() {
        ChunksQuantityHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public int getquantity() {
        return quantity;
    }

    public void setquantity(final int quantity) {
        this.quantity = quantity;
    }

    public String getUUID() {
        return UUID;
    }
}
