package com.mixedup.factions.hashs;

import java.util.HashMap;

public class ChunksProvHash {

    public static final HashMap<String, ChunksProvHash> CACHE = new HashMap<String, ChunksProvHash>();
    private final String UUID;
    private String owner;

    public ChunksProvHash(final String UUID, final String owner) {
        this.UUID = UUID;
        this.owner = owner;
    }

    public static ChunksProvHash get(final String UUID) {
        return ChunksProvHash.CACHE.get(String.valueOf(UUID));
    }

    public ChunksProvHash insert() {
        ChunksProvHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public String getChunk() {
        return owner;
    }

    public void setChunk(final String owner) {
        this.owner = owner;
    }

    public String getUUID() {
        return UUID;
    }
}
