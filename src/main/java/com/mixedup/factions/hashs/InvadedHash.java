package com.mixedup.factions.hashs;

import java.util.HashMap;

public class InvadedHash {

    private static final HashMap<String, InvadedHash> CACHE = new HashMap<String, InvadedHash>();
    private final String UUID;
    private boolean Status;

    public InvadedHash(final String UUID, final boolean Status) {
        this.UUID = UUID;
        this.Status = Status;
    }

    public static InvadedHash get(final String UUID) {
        return InvadedHash.CACHE.get(String.valueOf(UUID));
    }

    public InvadedHash insert() {
        InvadedHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(final boolean Status) {
        this.Status = Status;
    }

    public String getUUID() {
        return UUID;
    }
}
