package com.mixedup.factions.hashs;

import java.util.HashMap;

public class NexusHash {

    private static final HashMap<String, NexusHash> CACHE = new HashMap<String, NexusHash>();
    private String squad;
    private String pos1;
    private String pos2;

    public NexusHash(final String squad, final String pos1, final String pos2) {
        this.squad = squad;
        this.pos1 = pos1;
        this.pos2 = pos2;
    }

    public static NexusHash get(final String squad) {
        return NexusHash.CACHE.get(squad);
    }

    public NexusHash insert() {
        NexusHash.CACHE.put(squad, this);

        return this;
    }

    public String getSquad() {
        return this.squad;
    }

    public void setSquad(final String squad) {
        this.squad = squad;
    }

    public String getPos1() {
        return this.pos1;
    }

    public void setPos1(final String pos1) {
        this.pos1 = pos1;
    }

    public String getPos2() {
        return this.pos2;
    }

    public void setPos2(final String pos2) {
        this.pos2 = pos2;
    }
}
