package com.mixedup.factions.hashs;

import com.mixedup.MySql;
import org.bukkit.Location;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NexusUtil {

    private static List<NexusHash> regions = new ArrayList<>();

    public static NexusHash getRegion(final String squad) {
        for (final NexusHash region : NexusUtil.getRegions()) {
            if (region.getSquad() == squad) {
                return region;
            }
        }
        return null;
    }

    public static List<NexusHash> getRegions() {
        return NexusUtil.regions;
    }

    public static void setRegions(final List<NexusHash> regions) {
        NexusUtil.regions = regions;
    }

    public static NexusHash playerInArea(final Location loc) {
        final List<NexusHash> regionList = getRegions();
        for (final NexusHash rg : regionList) {

            final String[] pos1 = rg.getPos1().split(":");
            final String[] pos2 = rg.getPos2().split(":");

            if (!pos1[0].equals("NULL") && !pos2[0].equals("NULL")) {
                final int p1x = Integer.valueOf(pos1[0]);
                final int p1y = Integer.valueOf(pos1[1]);
                final int p1z = Integer.valueOf(pos1[2]);
                final int p2x = Integer.valueOf(pos2[0]);
                final int p2y = Integer.valueOf(pos2[1]);
                final int p2z = Integer.valueOf(pos2[2]);

                final int minX = p1x < p2x ? p1x : p2x;
                final int minY = p1y < p2y ? p1y : p2y;
                final int minZ = p1z < p2z ? p1z : p2z;

                final int maxX = p1x > p2x ? p1x : p2x;
                final int maxY = p1y > p2y ? p1y : p2y;
                final int maxZ = p1z > p2z ? p1z : p2z;

                if ((loc.getBlockX() >= minX) && (loc.getBlockX() <= maxX) && (loc.getBlockY() >= minY)
                        && (loc.getBlockY() <= maxY) && (loc.getBlockZ() >= minZ) && (loc.getBlockZ() <= maxZ)) {

                    return rg;

                }
            }
        }
        return null;
    }


    public static void enableRegions() {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Facçoes_data");
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if (!rs.getString("Nexus").equals("NULL")) {
                    final String[] split = rs.getString("Nexus").split("::");
                    final String pos1 = split[0];
                    final String pos2 = split[1];
                    final NexusHash nexus = new NexusHash(rs.getString("Nome"), pos1, pos2).insert();
                    getRegions().add(nexus);
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }
}
