package com.mixedup.factions.hashs;

import java.util.HashMap;

public class FixFloodWarning {

    public static HashMap<String, FixFloodWarning> cache = new HashMap<>();
    private final String UUID;
    private boolean sended;

    public FixFloodWarning(final String UUID, final boolean sended) {
        this.UUID = UUID;
        this.sended = sended;
    }

    public static FixFloodWarning get(final String UUID) {
        return FixFloodWarning.cache.get(UUID);
    }

    public FixFloodWarning insert() {
        FixFloodWarning.cache.put(UUID, this);
        return this;
    }

    public boolean getSended() {
        return sended;
    }

    public void setSended(final boolean sended) {
        this.sended = sended;
    }
}
