package com.mixedup.factions.hashs;

import java.util.HashMap;

public class PlayerPermHash {

    public static HashMap<String, PlayerPermHash> CACHE = new HashMap<String, PlayerPermHash>();
    private String UUID;
    private String Membro;

    public PlayerPermHash(final String UUID, final String Membro) {
        this.UUID = UUID;
        this.Membro = Membro;
    }

    public static PlayerPermHash get(final String UUID) {
        return PlayerPermHash.CACHE.get(UUID);
    }

    public PlayerPermHash insert() {
        PlayerPermHash.CACHE.put(UUID, this);

        return this;
    }

    public String getUUID() {
        return this.UUID;
    }

    public void setUUID(final String UUID) {
        this.UUID = UUID;
    }

    public String getMembro() {
        return this.Membro;
    }

    public void setMembro(final String Membro) {
        this.Membro = Membro;
    }
}
