package com.mixedup.factions.hashs;

import java.util.HashMap;

public class RelacaoFixDb {

    public static HashMap<String, RelacaoFixDb> cache = new HashMap<>();
    private final String uuid;
    private boolean status;

    public RelacaoFixDb(final String uuid, final boolean status) {
        this.uuid = uuid;
        this.status = status;
    }

    public static RelacaoFixDb get(final String uuid) {
        return RelacaoFixDb.cache.get(uuid);
    }

    public RelacaoFixDb insert() {
        RelacaoFixDb.cache.put(uuid, this);
        return this;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(final boolean status) {
        this.status = status;
    }
}
