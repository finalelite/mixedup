package com.mixedup.factions.hashs;

import java.util.HashMap;

public class TypeNexusUtil {

    private static final HashMap<String, TypeNexusUtil> CACHE = new HashMap<String, TypeNexusUtil>();
    private final String UUID;
    private String type;

    public TypeNexusUtil(final String UUID, final String type) {
        this.UUID = UUID;
        this.type = type;
    }

    public static TypeNexusUtil get(final String UUID) {
        return TypeNexusUtil.CACHE.get(String.valueOf(UUID));
    }

    public TypeNexusUtil insert() {
        TypeNexusUtil.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public String gettype() {
        return type;
    }

    public void settype(final String type) {
        this.type = type;
    }

    public String getUUID() {
        return UUID;
    }
}
