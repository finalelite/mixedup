package com.mixedup.factions.hashs;

import java.util.HashMap;

public class ActiveUtil {

    public static HashMap<String, ActiveUtil> CACHE = new HashMap<String, ActiveUtil>();
    private String location;
    private boolean Activated;

    public ActiveUtil(final String location, final boolean Activated) {
        this.location = location;
        this.Activated = Activated;
    }

    public static ActiveUtil get(final String UUID) {
        return ActiveUtil.CACHE.get(String.valueOf(UUID));
    }

    public ActiveUtil insert() {
        ActiveUtil.CACHE.put(String.valueOf(location), this);

        return this;
    }

    public boolean getActivated() {
        return Activated;
    }

    public void setActivated(final boolean Activated) {
        this.Activated = Activated;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(final String location) {
        this.location = location;
    }
}
