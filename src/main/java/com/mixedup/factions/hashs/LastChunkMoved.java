package com.mixedup.factions.hashs;

import java.util.HashMap;

public class LastChunkMoved {

    private static final HashMap<String, LastChunkMoved> CACHE = new HashMap<String, LastChunkMoved>();
    private final String UUID;
    private String lastChunk;

    public LastChunkMoved(final String UUID, final String lastChunk) {
        this.UUID = UUID;
        this.lastChunk = lastChunk;
    }

    public static LastChunkMoved get(final String UUID) {
        return LastChunkMoved.CACHE.get(String.valueOf(UUID));
    }

    public LastChunkMoved insert() {
        LastChunkMoved.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public String getLastChunk() {
        return lastChunk;
    }

    public void setLastChunk(final String lastChunk) {
        this.lastChunk = lastChunk;
    }

    public String getUUID() {
        return UUID;
    }
}
