package com.mixedup.factions.cache;

import java.util.HashMap;

public class PlotsEsqCache {

    public static HashMap<String, PlotsEsqCache> CACHE = new HashMap<>();
    //Nome, Plots, Descrição, Spawn
    private final String nome;
    private String plots;
    private String desc;
    private String spawn;

    public PlotsEsqCache(final String nome, final String plots, final String desc, final String spawn) {
        this.nome = nome;
        this.plots = plots;
        this.desc = desc;
        this.spawn = spawn;
    }

    public static PlotsEsqCache get(final String nome) {
        return PlotsEsqCache.CACHE.get(nome);
    }

    public PlotsEsqCache insert() {
        PlotsEsqCache.CACHE.put(nome, this);
        return this;
    }

    public String getSpawn() {
        return spawn;
    }

    public void setSpawn(final String spawn) {
        this.spawn = spawn;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(final String desc) {
        this.desc = desc;
    }

    public String getPlots() {
        return plots;
    }

    public void setPlots(final String plots) {
        this.plots = plots;
    }
}
