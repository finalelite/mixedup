package com.mixedup.factions.cache;

import java.util.HashMap;

public class NameWithTagCache {

    public static HashMap<String, NameWithTagCache> cache = new HashMap<>();
    private final String tag;
    private final String nome;

    public NameWithTagCache(final String tag, final String nome) {
        this.tag = tag;
        this.nome = nome;
    }

    public static NameWithTagCache get(final String tag) {
        return NameWithTagCache.cache.get(tag);
    }

    public NameWithTagCache insert() {
        NameWithTagCache.cache.put(tag, this);
        return this;
    }

    public String getNome() {
        return nome;
    }
}
