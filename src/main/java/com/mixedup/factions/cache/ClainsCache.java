package com.mixedup.factions.cache;

import java.util.HashMap;

public class ClainsCache {

    public static HashMap<String, ClainsCache> CACHE = new HashMap<>();
    //Chunk, Esquadrao
    private final String chunk;
    private String squad;

    public ClainsCache(final String chunk, final String squad) {
        this.chunk = chunk;
        this.squad = squad;
    }

    public static ClainsCache get(final String chunk) {
        return ClainsCache.CACHE.get(chunk);
    }

    public ClainsCache insert() {
        ClainsCache.CACHE.put(chunk, this);
        return this;
    }

    public String getSquad() {
        return squad;
    }

    public void setSquad(final String squad) {
        this.squad = squad;
    }
}
