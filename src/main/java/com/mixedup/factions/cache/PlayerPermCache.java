package com.mixedup.factions.cache;

import java.util.HashMap;

public class PlayerPermCache {

    public static HashMap<String, PlayerPermCache> CACHE = new HashMap<>();
    //UUID, Construir, Chests, Dominar, Abandonar, Recrutar, Expulsar, Spawners
    private final String UUID;
    private boolean build;
    private boolean chest;
    private boolean dominar;
    private boolean abandonar;
    private boolean recrutar;
    private boolean expulsar;
    private boolean spawners;

    public PlayerPermCache(final String UUID, final boolean build, final boolean chest, final boolean dominar, final boolean abandonar, final boolean recrutar, final boolean expulsar, final boolean spawners) {
        this.UUID = UUID;
        this.build = build;
        this.chest = chest;
        this.dominar = dominar;
        this.abandonar = abandonar;
        this.recrutar = recrutar;
        this.expulsar = expulsar;
        this.spawners = spawners;
    }

    public static PlayerPermCache get(final String UUID) {
        return PlayerPermCache.CACHE.get(UUID);
    }

    public PlayerPermCache insert() {
        PlayerPermCache.CACHE.put(UUID, this);
        return this;
    }

    public boolean getSpawners() {
        return spawners;
    }

    public void setSpawners(final boolean spawners) {
        this.spawners = spawners;
    }

    public boolean getExpulsar() {
        return expulsar;
    }

    public void setExpulsar(final boolean expulsar) {
        this.expulsar = expulsar;
    }

    public boolean getRecrutar() {
        return recrutar;
    }

    public void setRecrutar(final boolean recrutar) {
        this.recrutar = recrutar;
    }

    public boolean getAbandonar() {
        return abandonar;
    }

    public void setAbandonar(final boolean abandonar) {
        this.abandonar = abandonar;
    }

    public boolean getDominar() {
        return dominar;
    }

    public void setDominar(final boolean dominar) {
        this.dominar = dominar;
    }

    public boolean getChest() {
        return chest;
    }

    public void setChest(final boolean chest) {
        this.chest = chest;
    }

    public boolean getBuild() {
        return build;
    }

    public void setBuild(final boolean build) {
        this.build = build;
    }
}
