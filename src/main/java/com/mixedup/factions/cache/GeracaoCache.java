package com.mixedup.factions.cache;

import java.util.HashMap;

public class GeracaoCache {

    public static HashMap<String, GeracaoCache> CACHE = new HashMap<>();
    //Chunk, Probabilidade
    private final String chunk;
    private int probability;

    public GeracaoCache(final String chunk, final int probability) {
        this.chunk = chunk;
        this.probability = probability;
    }

    public static GeracaoCache get(final String chunk) {
        return GeracaoCache.CACHE.get(chunk);
    }

    public GeracaoCache insert() {
        GeracaoCache.CACHE.put(chunk, this);
        return this;
    }

    public int getProbability() {
        return probability;
    }

    public void setProbability(final int probability) {
        this.probability = probability;
    }
}
