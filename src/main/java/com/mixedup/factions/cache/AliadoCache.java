package com.mixedup.factions.cache;

import java.util.HashMap;

public class AliadoCache {

    public static HashMap<String, AliadoCache> CACHE = new HashMap<>();
    //UUID, Construir, Chests, Acesso
    private final String UUID;
    private boolean build;
    private boolean chest;
    private boolean acess;

    public AliadoCache(final String UUID, final boolean build, final boolean chest, final boolean acess) {
        this.UUID = UUID;
        this.build = build;
        this.chest = chest;
        this.acess = acess;
    }

    public static AliadoCache get(final String UUID) {
        return AliadoCache.CACHE.get(UUID);
    }

    public AliadoCache insert() {
        AliadoCache.CACHE.put(UUID, this);
        return this;
    }

    public boolean getAcess() {
        return acess;
    }

    public void setAcess(final boolean acess) {
        this.acess = acess;
    }

    public boolean getChest() {
        return chest;
    }

    public void setChest(final boolean chest) {
        this.chest = chest;
    }

    public boolean getBuild() {
        return build;
    }

    public void setBuild(final boolean build) {
        this.build = build;
    }
}
