package com.mixedup.factions.cache;

import java.util.HashMap;

public class PlayerFacCache {

    public static HashMap<String, PlayerFacCache> CACHE = new HashMap<>();
    //UUID, Nome, Hierarquia
    private final String UUID;
    private String nome;
    private String hierarquia;

    public PlayerFacCache(final String UUID, final String nome, final String hierarquia) {
        this.UUID = UUID;
        this.nome = nome;
        this.hierarquia = hierarquia;
    }

    public static PlayerFacCache get(final String UUID) {
        return PlayerFacCache.CACHE.get(UUID);
    }

    public PlayerFacCache insert() {
        PlayerFacCache.CACHE.put(UUID, this);
        return this;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public String getHierarquia() {
        return hierarquia;
    }

    public void setHierarquia(final String hierarquia) {
        this.hierarquia = hierarquia;
    }
}
