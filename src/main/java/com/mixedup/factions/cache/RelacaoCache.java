package com.mixedup.factions.cache;

import java.util.HashMap;

public class RelacaoCache {

    public static HashMap<String, RelacaoCache> CACHE = new HashMap<>();
    //Nome, Inimigos, Aliados, Amigos, Rivais
    private final String nome;
    private String inimigos;
    private String aliados;
    private String amigos;
    private String rivais;

    public RelacaoCache(final String nome, final String inimigos, final String aliados, final String amigos, final String rivais) {
        this.nome = nome;
        this.inimigos = inimigos;
        this.aliados = aliados;
        this.amigos = amigos;
        this.rivais = rivais;
    }

    public static RelacaoCache get(final String nome) {
        return RelacaoCache.CACHE.get(nome);
    }

    public RelacaoCache insert() {
        RelacaoCache.CACHE.put(nome, this);
        return this;
    }

    public String getRivais() {
        return rivais;
    }

    public void setRivais(final String rivais) {
        this.rivais = rivais;
    }

    public String getAmigos() {
        return amigos;
    }

    public void setAmigos(final String amigos) {
        this.amigos = amigos;
    }

    public String getAliados() {
        return aliados;
    }

    public void setAliados(final String aliados) {
        this.aliados = aliados;
    }

    public String getInimigos() {
        return inimigos;
    }

    public void setInimigos(final String inimigos) {
        this.inimigos = inimigos;
    }
}
