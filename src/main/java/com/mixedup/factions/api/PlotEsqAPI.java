package com.mixedup.factions.api;

import com.mixedup.Main;
import com.mixedup.MySql;
import com.mixedup.factions.cache.PlotsEsqCache;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PlotEsqAPI {

    public static String getSpawn(final String nome) {
        if (PlotsEsqCache.get(nome) != null) {
            return PlotsEsqCache.get(nome).getSpawn();
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Plotsesq_data WHERE Nome = ?");
                st.setString(1, nome);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final String spawn = rs.getString("Spawn");
                    new PlotsEsqCache(nome, rs.getString("Plots"), rs.getString("Descrição"), spawn).insert();
                    return spawn;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public static String getSpawnDb(final String nome) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Plotsesq_data WHERE Nome = ?");
            st.setString(1, nome);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                final String spawn = rs.getString("Spawn");
                new PlotsEsqCache(nome, rs.getString("Plots"), rs.getString("Descrição"), spawn).insert();
                return spawn;
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDescricao(final String nome) {
        if (PlotsEsqCache.get(nome) != null) {
            return PlotsEsqCache.get(nome).getDesc();
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Plotsesq_data WHERE Nome = ?");
                st.setString(1, nome);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final String desc = rs.getString("Descrição");
                    new PlotsEsqCache(nome, rs.getString("Plots"), desc, rs.getString("Spawn")).insert();
                    return desc;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public static String getDescricaoDb(final String nome) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Plotsesq_data WHERE Nome = ?");
            st.setString(1, nome);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Descrição");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getPlots(final String nome) {
        if (PlotsEsqCache.get(nome) != null) {
            return PlotsEsqCache.get(nome).getPlots();
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Plotsesq_data WHERE Nome = ?");
                st.setString(1, nome);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final String plots = rs.getString("Plots");
                    new PlotsEsqCache(nome, plots, rs.getString("Descrição"), rs.getString("Spawn")).insert();
                    return plots;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public static String getPlotsDb(final String nome) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Plotsesq_data WHERE Nome = ?");
            st.setString(1, nome);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Plots");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void updatePlots(final String nome, final String plots) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Plotsesq_data SET Plots = ? WHERE Nome = ?");
                    st.setString(2, nome);
                    st.setString(1, plots);
                    st.executeUpdate();

                    if (PlotsEsqCache.get(nome) != null) {
                        PlotsEsqCache.get(nome).setPlots(plots);
                    } else {
                        new PlotsEsqCache(nome, plots, PlotEsqAPI.getDescricaoDb(nome), PlotEsqAPI.getSpawnDb(nome)).insert();
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void updateSpawn(final String nome, final String spawn) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Plotsesq_data SET Spawn = ? WHERE Nome = ?");
                    st.setString(2, nome);
                    st.setString(1, spawn);
                    st.executeUpdate();

                    if (PlotsEsqCache.get(nome) != null) {
                        PlotsEsqCache.get(nome).setSpawn(spawn);
                    } else {
                        new PlotsEsqCache(nome, PlotEsqAPI.getPlotsDb(nome), PlotEsqAPI.getDescricaoDb(nome), spawn).insert();
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void updateDescricao(final String nome, final String desc) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Plotsesq_data SET Descrição = ? WHERE Nome = ?");
                    st.setString(2, nome);
                    st.setString(1, desc);
                    st.executeUpdate();

                    if (PlotsEsqCache.get(nome) != null) {
                        PlotsEsqCache.get(nome).setDesc(desc);
                    } else {
                        new PlotsEsqCache(nome, PlotEsqAPI.getPlotsDb(nome), desc, PlotEsqAPI.getSpawnDb(nome)).insert();
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void createInfoPlot(final String nome) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Plotsesq_data(Nome, Plots, Descrição, Spawn) VALUES (?, ?, ?, ?)");
                    st.setString(1, nome);
                    st.setString(2, "NULL");
                    st.setString(3, "NULL");
                    st.setString(4, "NULL");
                    st.executeUpdate();

                    new PlotsEsqCache(nome, "NULL", "NULL", "NULL").insert();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }
}
