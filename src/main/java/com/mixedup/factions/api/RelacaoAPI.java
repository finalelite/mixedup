package com.mixedup.factions.api;

import com.mixedup.Main;
import com.mixedup.MySql;
import com.mixedup.factions.cache.RelacaoCache;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RelacaoAPI {

    public static void removeRelacao(final String nome) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Relacoes_data WHERE Name = ?");
                    st.setString(1, nome);
                    st.executeUpdate();

                    if (RelacaoCache.get(nome) != null) {
                        RelacaoCache.CACHE.remove(nome);
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static String getAliadosDB(final String nome) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Relacoes_data WHERE Nome = ?");
            st.setString(1, nome);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Aliados");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getInimigosDB(final String nome) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Relacoes_data WHERE Nome = ?");
            st.setString(1, nome);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Inimigos");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getRivaisDB(final String nome) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Relacoes_data WHERE Nome = ?");
            st.setString(1, nome);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Rivais");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void updateRivais(final String nome, final String rivais) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Relacoes_data SET Rivais = ? WHERE Nome = ?");
                    st.setString(2, nome);
                    st.setString(1, rivais);
                    st.executeUpdate();

                    if (RelacaoCache.get(nome) != null) {
                        RelacaoCache.get(nome).setRivais(rivais);
                    } else {
                        new RelacaoCache(nome, RelacaoAPI.getInimigosDB(nome), RelacaoAPI.getAliadosDB(nome), RelacaoAPI.getAmigosDB(nome), rivais).insert();
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static String getAmigosDB(final String nome) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Relacoes_data WHERE Nome = ?");
            st.setString(1, nome);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Amigos");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void updateAmigos(final String nome, final String amigos) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Relacoes_data SET Amigos = ? WHERE Nome = ?");
                    st.setString(2, nome);
                    st.setString(1, amigos);
                    st.executeUpdate();

                    if (RelacaoCache.get(nome) != null) {
                        RelacaoCache.get(nome).setAmigos(amigos);
                    } else {
                        new RelacaoCache(nome, RelacaoAPI.getInimigosDB(nome), RelacaoAPI.getAliadosDB(nome), amigos, RelacaoAPI.getRivaisDB(nome)).insert();
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void updateAliados(final String nome, final String aliados) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Relacoes_data SET Aliados = ? WHERE Nome = ?");
                    st.setString(2, nome);
                    st.setString(1, aliados);
                    st.executeUpdate();

                    if (RelacaoCache.get(nome) != null) {
                        RelacaoCache.get(nome).setAliados(aliados);
                    } else {
                        new RelacaoCache(nome, RelacaoAPI.getInimigosDB(nome), aliados, RelacaoAPI.getAmigosDB(nome), RelacaoAPI.getRivaisDB(nome)).insert();
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void updateInimigos(final String nome, final String inimigos) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Relacoes_data SET Inimigos = ? WHERE Nome = ?");
                    st.setString(2, nome);
                    st.setString(1, inimigos);
                    st.executeUpdate();

                    if (RelacaoCache.get(nome) != null) {
                        RelacaoCache.get(nome).setInimigos(inimigos);
                    } else {
                        new RelacaoCache(nome, inimigos, RelacaoAPI.getAliadosDB(nome), RelacaoAPI.getAmigosDB(nome), RelacaoAPI.getRivaisDB(nome)).insert();
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void createRelacoes(final String nome) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Relacoes_data(Nome, Inimigos, Aliados, Amigos, Rivais) VALUES (?, ?, ?, ?, ?)");
                    st.setString(1, nome);
                    st.setString(2, "NULL");
                    st.setString(3, "NULL");
                    st.setString(4, "NULL");
                    st.setString(5, "NULL");
                    st.executeUpdate();

                    new RelacaoCache(nome, "NULL", "NULL", "NULL", "NULL").insert();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }
}
