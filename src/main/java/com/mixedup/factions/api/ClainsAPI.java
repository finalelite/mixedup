package com.mixedup.factions.api;

import com.mixedup.Main;
import com.mixedup.MySql;
import com.mixedup.factions.cache.ClainsCache;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ClainsAPI {

    public static String getChunkOwn(final String chunk) {
        if (ClainsCache.get(chunk) != null) {
            return ClainsCache.get(chunk).getSquad();
        } else {
            return null;
        }
    }

    public static void setChunkInCache() {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Clains_data");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                new ClainsCache(rs.getString("Chunk"), rs.getString("Esquadrao")).insert();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeChunk(final String chunk) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Clains_data WHERE Chunk = ?");
                    st.setString(1, chunk);
                    st.executeUpdate();

                    if (ClainsCache.get(chunk) != null) {
                        ClainsCache.CACHE.remove(chunk);
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void updateChunk(final String chunk, final String tag) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Clains_data SET Esquadrao = ? WHERE Chunk = ?");
                    st.setString(2, chunk);
                    st.setString(1, tag);
                    st.executeUpdate();

                    if (ClainsCache.get(chunk) != null) {
                        ClainsCache.get(chunk).setSquad(tag);
                    } else {
                        new ClainsCache(chunk, tag).insert();
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void createChunk(final String chunk, final String tag) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Clains_data(Chunk, Esquadrao) VALUES (?, ?)");
                    st.setString(1, chunk);
                    st.setString(2, tag);
                    st.executeUpdate();

                    new ClainsCache(chunk, tag).insert();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }
}
