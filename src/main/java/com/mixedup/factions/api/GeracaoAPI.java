package com.mixedup.factions.api;

import com.mixedup.Main;
import com.mixedup.MySql;
import com.mixedup.factions.cache.GeracaoCache;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GeracaoAPI {

    public static void createProbabInfo(final String chunk, final int probab) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Geracao_data(Chunk, Probabilidade) VALUES (?, ?)");
                    st.setString(1, chunk);
                    st.setInt(2, probab);
                    st.executeUpdate();

                    new GeracaoCache(chunk, probab).insert();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static int getProbab(final String chunk) {
        if (GeracaoCache.get(chunk) != null) {
            return GeracaoCache.get(chunk).getProbability();
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Geracao_data WHERE Chunk = ?");
                st.setString(1, chunk);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final int probability = rs.getInt("Probabilidade");
                    new GeracaoCache(chunk, probability).insert();
                    return probability;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return 0;
        }
    }

    public static String getExistProbab(final String chunk) {
        if (GeracaoCache.get(chunk) != null) {
            return chunk;
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Geracao_data WHERE Chunk = ?");
                st.setString(1, chunk);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    new GeracaoCache(chunk, rs.getInt("Probabilidade")).insert();
                    return rs.getString("Chunk");
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
