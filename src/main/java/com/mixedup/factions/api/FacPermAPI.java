package com.mixedup.factions.api;

import com.mixedup.Main;
import com.mixedup.MySql;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.cache.AliadoCache;
import com.mixedup.factions.cache.PlayerPermCache;
import com.mixedup.factions.hashs.AliadoFixDb;
import com.mixedup.factions.hashs.PermFixDb;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FacPermAPI implements Listener {

    public static void createPermAliado(final String name) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Aliado_perm_fac(Name, Construir, Chests, Acesso) VALUES (?, ?, ?, ?)");
                    st.setString(1, name);
                    st.setBoolean(2, false);
                    st.setBoolean(3, false);
                    st.setBoolean(4, false);
                    st.executeUpdate();

                    new AliadoCache(name, false, false, false).insert();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void removePermAliado(final String name) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Aliado_perm_fac WHERE Name = ?");
                    st.setString(1, name);
                    st.executeUpdate();

                    AliadoCache.CACHE.remove(name);
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static String getExistDb(final String name) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Aliado_perm_fac WHERE Name = ?");
            st.setString(1, name);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Name");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean getAliadoPermAcesso(final String name) {
        if (FacPermAPI.getExistDb(name) == null) {
            FacAPI.createPermAliado(name);
        }
        if (AliadoCache.get(name) != null) {
            return AliadoCache.get(name).getAcess();
        } else {
            if (AliadoFixDb.get(name) == null) {
                if (FacPermAPI.getExistDb(name) != null) {
                    new AliadoCache(name, FacPermAPI.getAliadoPermBuildDb(name), FacPermAPI.getAliadoPermChestsDb(name), FacPermAPI.getAliadoPermAcessoDb(name)).insert();

                    return AliadoCache.get(name).getAcess();
                } else {
                    new AliadoFixDb(name, true).insert();

                    return false;
                }
            } else if (AliadoFixDb.get(name).getStatus() == true) {
                return false;
            }
        }
        return false;
    }

    public static boolean getAliadoPermAcessoDb(final String name) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Aliado_perm_fac WHERE Name = ?");
            st.setString(1, name);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Acesso");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void updateAliadoPermAcesso(final String name, final boolean status) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Aliado_perm_fac SET Acesso = ? WHERE Name = ?");
                    st.setString(2, name);
                    st.setBoolean(1, status);
                    st.executeUpdate();

                    if (AliadoCache.get(name) != null) {
                        AliadoCache.get(name).setAcess(status);
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static boolean getAliadoPermChests(final String name) {
        if (FacPermAPI.getExistDb(name) == null) {
            FacAPI.createPermAliado(name);
        }
        if (AliadoCache.get(name) != null) {
            return AliadoCache.get(name).getChest();
        } else {
            if (AliadoFixDb.get(name) == null) {
                if (FacPermAPI.getExistDb(name) != null) {
                    new AliadoCache(name, FacPermAPI.getAliadoPermBuildDb(name), FacPermAPI.getAliadoPermChestsDb(name), FacPermAPI.getAliadoPermAcessoDb(name)).insert();

                    return AliadoCache.get(name).getChest();
                } else {
                    new AliadoFixDb(name, true).insert();

                    return false;
                }
            } else if (AliadoFixDb.get(name).getStatus() == true) {
                return false;
            }
        }
        return false;
    }

    public static boolean getAliadoPermChestsDb(final String name) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Aliado_perm_fac WHERE Name = ?");
            st.setString(1, name);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Chests");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void updateAliadoPermChests(final String name, final boolean status) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Aliado_perm_fac SET Chests = ? WHERE Name = ?");
                    st.setString(2, name);
                    st.setBoolean(1, status);
                    st.executeUpdate();

                    if (AliadoCache.get(name) != null) {
                        AliadoCache.get(name).setChest(status);
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static boolean getAliadoPermBuild(final String name) {
        if (FacPermAPI.getExistDb(name) == null) {
            FacAPI.createPermAliado(name);
        }
        if (AliadoCache.get(name) != null) {
            return AliadoCache.get(name).getBuild();
        } else {
            if (AliadoFixDb.get(name) == null) {
                if (FacPermAPI.getExistDb(name) != null) {
                    new AliadoCache(name, FacPermAPI.getAliadoPermBuildDb(name), FacPermAPI.getAliadoPermChestsDb(name), FacPermAPI.getAliadoPermAcessoDb(name)).insert();

                    return AliadoCache.get(name).getBuild();
                } else {
                    new AliadoFixDb(name, true).insert();

                    return false;
                }
            } else if (AliadoFixDb.get(name).getStatus() == true) {
                return false;
            }
        }
        return false;
    }

    public static boolean getAliadoPermBuildDb(final String name) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Aliado_perm_fac WHERE Name = ?");
            st.setString(1, name);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Construir");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void updateAliadoPermBuild(final String name, final boolean status) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Aliado_perm_fac SET Construir = ? WHERE Name = ?");
                    st.setString(2, name);
                    st.setBoolean(1, status);
                    st.executeUpdate();

                    if (AliadoCache.get(name) != null) {
                        AliadoCache.get(name).setBuild(status);
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public static void permLiderCreate(final String UUID) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Player_perm_fac(UUID, Construir, Chests, Dominar, Abandonar, Recrutar, Expulsar, Spawners) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
                    st.setString(1, UUID);
                    st.setBoolean(2, true);
                    st.setBoolean(3, true);
                    st.setBoolean(4, true);
                    st.setBoolean(5, true);
                    st.setBoolean(6, true);
                    st.setBoolean(7, true);
                    st.setBoolean(8, true);
                    st.executeUpdate();

                    new PlayerPermCache(UUID, true, true, true, true, true, true, true).insert();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void playerCreatePerms(final String UUID) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Player_perm_fac(UUID, Construir, Chests, Dominar, Abandonar, Recrutar, Expulsar, Spawners) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
                    st.setString(1, UUID);
                    st.setBoolean(2, true);
                    st.setBoolean(3, true);
                    st.setBoolean(4, false);
                    st.setBoolean(5, false);
                    st.setBoolean(6, false);
                    st.setBoolean(7, false);
                    st.setBoolean(8, false);
                    st.executeUpdate();

                    new PlayerPermCache(UUID, true, true, false, false, false, false, false).insert();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void updatePermSpawners(final String UUID, final boolean status) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Player_perm_fac SET Spawners = ? WHERE UUID = ?");
                    st.setString(2, UUID);
                    st.setBoolean(1, status);
                    st.executeUpdate();

                    if (PlayerPermCache.get(UUID) != null) {
                        PlayerPermCache.get(UUID).setSpawners(status);
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void updatePermExpulsar(final String UUID, final boolean status) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Player_perm_fac SET Expulsar = ? WHERE UUID = ?");
                    st.setString(2, UUID);
                    st.setBoolean(1, status);
                    st.executeUpdate();

                    if (PlayerPermCache.get(UUID) != null) {
                        PlayerPermCache.get(UUID).setExpulsar(status);
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void updatePermRecrutar(final String UUID, final boolean status) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Player_perm_fac SET Recrutar = ? WHERE UUID = ?");
                    st.setString(2, UUID);
                    st.setBoolean(1, status);
                    st.executeUpdate();

                    if (PlayerPermCache.get(UUID) != null) {
                        PlayerPermCache.get(UUID).setRecrutar(status);
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void updatePermAbandonar(final String UUID, final boolean status) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Player_perm_fac SET Abandonar = ? WHERE UUID = ?");
                    st.setString(2, UUID);
                    st.setBoolean(1, status);
                    st.executeUpdate();

                    if (PlayerPermCache.get(UUID) != null) {
                        PlayerPermCache.get(UUID).setAbandonar(status);
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void updatePermDominar(final String UUID, final boolean status) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Player_perm_fac SET Dominar = ? WHERE UUID = ?");
                    st.setString(2, UUID);
                    st.setBoolean(1, status);
                    st.executeUpdate();

                    if (PlayerPermCache.get(UUID) != null) {
                        PlayerPermCache.get(UUID).setDominar(status);
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void updatePermChests(final String UUID, final boolean status) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Player_perm_fac SET Chests = ? WHERE UUID = ?");
                    st.setString(2, UUID);
                    st.setBoolean(1, status);
                    st.executeUpdate();

                    if (PlayerPermCache.get(UUID) != null) {
                        PlayerPermCache.get(UUID).setChest(status);
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void updatePermBuild(final String UUID, final boolean status) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Player_perm_fac SET Construir = ? WHERE UUID = ?");
                    st.setString(2, UUID);
                    st.setBoolean(1, status);
                    st.executeUpdate();

                    if (PlayerPermCache.get(UUID) != null) {
                        PlayerPermCache.get(UUID).setBuild(status);
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static String getExistPlayerDb(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Player_perm_fac WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("UUID");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean getPermSpawners(final String UUID) {
        if (PlayerPermCache.get(UUID) != null) {
            return PlayerPermCache.get(UUID).getSpawners();
        } else {
            if (PermFixDb.get(UUID) == null) {
                if (FacPermAPI.getExistPlayerDb(UUID) != null) {
                    new PlayerPermCache(UUID, FacPermAPI.getPermBuildDb(UUID), FacPermAPI.getPermChestsDb(UUID), FacPermAPI.getPermDominarDb(UUID), FacPermAPI.getPermAbandonarDb(UUID), FacPermAPI.getPermRecrutarDb(UUID), FacPermAPI.getPermExpulsarDb(UUID), FacPermAPI.getPermSpawnersDb(UUID)).insert();

                    return PlayerPermCache.get(UUID).getSpawners();
                } else {
                    new PermFixDb(UUID, true).insert();

                    return false;
                }
            } else if (PermFixDb.get(UUID).getStatus() == true) {
                return false;
            }
        }
        return false;
    }

    public static boolean getPermSpawnersDb(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Player_perm_fac WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Spawners");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean getPermExpulsar(final String UUID) {
        if (PlayerPermCache.get(UUID) != null) {
            return PlayerPermCache.get(UUID).getExpulsar();
        } else {
            if (PermFixDb.get(UUID) == null) {
                if (FacPermAPI.getExistPlayerDb(UUID) != null) {
                    new PlayerPermCache(UUID, FacPermAPI.getPermBuildDb(UUID), FacPermAPI.getPermChestsDb(UUID), FacPermAPI.getPermDominarDb(UUID), FacPermAPI.getPermAbandonarDb(UUID), FacPermAPI.getPermRecrutarDb(UUID), FacPermAPI.getPermExpulsarDb(UUID), FacPermAPI.getPermSpawnersDb(UUID)).insert();

                    return PlayerPermCache.get(UUID).getExpulsar();
                } else {
                    new PermFixDb(UUID, true).insert();

                    return false;
                }
            } else if (PermFixDb.get(UUID).getStatus() == true) {
                return false;
            }
        }
        return false;
    }

    public static boolean getPermExpulsarDb(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Player_perm_fac WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Expulsar");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean getPermRecrutar(final String UUID) {
        if (PlayerPermCache.get(UUID) != null) {
            return PlayerPermCache.get(UUID).getRecrutar();
        } else {
            if (PermFixDb.get(UUID) == null) {
                if (FacPermAPI.getExistPlayerDb(UUID) != null) {
                    new PlayerPermCache(UUID, FacPermAPI.getPermBuildDb(UUID), FacPermAPI.getPermChestsDb(UUID), FacPermAPI.getPermDominarDb(UUID), FacPermAPI.getPermAbandonarDb(UUID), FacPermAPI.getPermRecrutarDb(UUID), FacPermAPI.getPermExpulsarDb(UUID), FacPermAPI.getPermSpawnersDb(UUID)).insert();

                    return PlayerPermCache.get(UUID).getRecrutar();
                } else {
                    new PermFixDb(UUID, true).insert();

                    return false;
                }
            } else if (PermFixDb.get(UUID).getStatus() == true) {
                return false;
            }
        }
        return false;
    }

    public static boolean getPermRecrutarDb(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Player_perm_fac WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Recrutar");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean getPermAbandonar(final String UUID) {
        if (PlayerPermCache.get(UUID) != null) {
            return PlayerPermCache.get(UUID).getAbandonar();
        } else {
            if (PermFixDb.get(UUID) == null) {
                if (FacPermAPI.getExistPlayerDb(UUID) != null) {
                    new PlayerPermCache(UUID, FacPermAPI.getPermBuildDb(UUID), FacPermAPI.getPermChestsDb(UUID), FacPermAPI.getPermDominarDb(UUID), FacPermAPI.getPermAbandonarDb(UUID), FacPermAPI.getPermRecrutarDb(UUID), FacPermAPI.getPermExpulsarDb(UUID), FacPermAPI.getPermSpawnersDb(UUID)).insert();

                    return PlayerPermCache.get(UUID).getAbandonar();
                } else {
                    new PermFixDb(UUID, true).insert();

                    return false;
                }
            } else if (PermFixDb.get(UUID).getStatus() == true) {
                return false;
            }
        }
        return false;
    }

    public static boolean getPermAbandonarDb(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Player_perm_fac WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Abandonar");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean getPermDominar(final String UUID) {
        if (PlayerPermCache.get(UUID) != null) {
            return PlayerPermCache.get(UUID).getDominar();
        } else {
            if (PermFixDb.get(UUID) == null) {
                if (FacPermAPI.getExistPlayerDb(UUID) != null) {
                    new PlayerPermCache(UUID, FacPermAPI.getPermBuildDb(UUID), FacPermAPI.getPermChestsDb(UUID), FacPermAPI.getPermDominarDb(UUID), FacPermAPI.getPermAbandonarDb(UUID), FacPermAPI.getPermRecrutarDb(UUID), FacPermAPI.getPermExpulsarDb(UUID), FacPermAPI.getPermSpawnersDb(UUID)).insert();

                    return PlayerPermCache.get(UUID).getDominar();
                } else {
                    new PermFixDb(UUID, true).insert();

                    return false;
                }
            } else if (PermFixDb.get(UUID).getStatus() == true) {
                return false;
            }
        }
        return false;
    }

    public static boolean getPermDominarDb(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Player_perm_fac WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Dominar");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean getPermChests(final String UUID) {
        if (PlayerPermCache.get(UUID) != null) {
            return PlayerPermCache.get(UUID).getChest();
        } else {
            if (PermFixDb.get(UUID) == null) {
                if (FacPermAPI.getExistPlayerDb(UUID) != null) {
                    new PlayerPermCache(UUID, FacPermAPI.getPermBuildDb(UUID), FacPermAPI.getPermChestsDb(UUID), FacPermAPI.getPermDominarDb(UUID), FacPermAPI.getPermAbandonarDb(UUID), FacPermAPI.getPermRecrutarDb(UUID), FacPermAPI.getPermExpulsarDb(UUID), FacPermAPI.getPermSpawnersDb(UUID)).insert();

                    return PlayerPermCache.get(UUID).getChest();
                } else {
                    new PermFixDb(UUID, true).insert();

                    return false;
                }
            } else if (PermFixDb.get(UUID).getStatus() == true) {
                return false;
            }
        }
        return false;
    }

    public static boolean getPermChestsDb(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Player_perm_fac WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Chests");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean getPermBuild(final String UUID) {
        if (PlayerPermCache.get(UUID) != null) {
            return PlayerPermCache.get(UUID).getBuild();
        } else {
            if (PermFixDb.get(UUID) == null) {
                if (FacPermAPI.getExistPlayerDb(UUID) != null) {
                    new PlayerPermCache(UUID, FacPermAPI.getPermBuildDb(UUID), FacPermAPI.getPermChestsDb(UUID), FacPermAPI.getPermDominarDb(UUID), FacPermAPI.getPermAbandonarDb(UUID), FacPermAPI.getPermRecrutarDb(UUID), FacPermAPI.getPermExpulsarDb(UUID), FacPermAPI.getPermSpawnersDb(UUID)).insert();

                    return PlayerPermCache.get(UUID).getBuild();
                } else {
                    new PermFixDb(UUID, true).insert();

                    return false;
                }
            } else if (PermFixDb.get(UUID).getStatus() == true) {
                return false;
            }
        }
        return false;
    }

    public static boolean getPermBuildDb(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Player_perm_fac WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Construir");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void removerPlayerPerms(final String UUID) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Player_perm_fac WHERE UUID = ?");
                    st.setString(1, UUID);
                    st.executeUpdate();

                    PlayerPermCache.CACHE.remove(UUID);
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }


    //-------------------------------------------------------------------------------------------------------------------------------------------


    @EventHandler
    public void onJoin(final PlayerJoinEvent event) {
        final Player player = event.getPlayer();
        final String UUID = player.getUniqueId().toString();

        if (FacPermAPI.getExistPlayerDb(UUID) != null) {
            new PlayerPermCache(UUID, FacPermAPI.getPermBuildDb(UUID), FacPermAPI.getPermChestsDb(UUID), FacPermAPI.getPermDominarDb(UUID), FacPermAPI.getPermAbandonarDb(UUID), FacPermAPI.getPermRecrutarDb(UUID), FacPermAPI.getPermExpulsarDb(UUID), FacPermAPI.getPermSpawnersDb(UUID)).insert();

            if (PermFixDb.get(UUID) != null) {
                PermFixDb.cache.remove(UUID);
            }
        }

        if (FacAPI.getFacNome(UUID) != null && FacAPI.getAliados(FacAPI.getFacNome(UUID)) != null && FacAPI.getAliados(FacAPI.getFacNome(UUID)).contains(":")) {
            final String[] split = FacAPI.getAliados(FacAPI.getFacNome(UUID)).split(":");

            for (int i = 1; i <= split.length; i++) {
                final String name = FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + split[i - 1];
                if (FacPermAPI.getExistDb(name) != null) {
                    new AliadoCache(name, FacPermAPI.getAliadoPermBuildDb(name), FacPermAPI.getAliadoPermChestsDb(name), FacPermAPI.getAliadoPermAcessoDb(name)).insert();
                }
            }
        }
    }

    @EventHandler
    public void onQuit(final PlayerQuitEvent event) {
        final Player player = event.getPlayer();
        final String UUID = player.getUniqueId().toString();

        if (PlayerPermCache.get(UUID) != null) {
            PlayerPermCache.CACHE.remove(UUID);
            PermFixDb.cache.remove(UUID);
        }

        if (AliadoCache.get(UUID) != null) {
            AliadoCache.CACHE.remove(UUID);
            AliadoFixDb.cache.remove(UUID);
        }
    }
}
