package com.mixedup.factions.api;

import com.mixedup.Main;
import com.mixedup.MySql;
import com.mixedup.factions.cache.PlayerFacCache;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PlayerFacAPI implements Listener {

    public static void updateInfo(final String UUID, final String hierarquia) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE PlayerFac_data SET Hierarquia = ? WHERE UUID = ?");
                    st.setString(2, UUID);
                    st.setString(1, hierarquia);
                    st.executeUpdate();

                    PlayerFacCache.get(UUID).setHierarquia(hierarquia);
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }


    public static void deletePlayerInfo(final String UUID) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM PlayerFac_data WHERE UUID = ?");
                    st.setString(1, UUID);
                    st.executeUpdate();

                    PlayerFacCache.CACHE.remove(UUID);
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void updateHierarquia(final String UUID, final String hierarquia) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE PlayerFac_data SET Hierarquia = ? WHERE UUID = ?");
                    st.setString(2, UUID);
                    st.setString(1, hierarquia);
                    st.executeUpdate();

                    PlayerFacCache.get(UUID).setHierarquia(hierarquia);
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static String getHierarquia(final String UUID) {
        if (PlayerFacCache.get(UUID) != null) {
            if (!PlayerFacCache.get(UUID).getNome().equalsIgnoreCase("null")) {
                return PlayerFacCache.get(UUID).getHierarquia();
            } else {
                return null;
            }
        } else {
            String name = getFacNomeDb(UUID);
            if (name != null) {
                new PlayerFacCache(UUID, name, getHierarquiaDb(UUID)).insert();
            } else {
                new PlayerFacCache(UUID, "null", "null").insert();
            }
            if (PlayerFacCache.get(UUID).getHierarquia().equalsIgnoreCase("null")) {
                return null;
            } else {
                return PlayerFacCache.get(UUID).getHierarquia();
            }
        }
    }

    public static String getHierarquiaDb(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM PlayerFac_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Hierarquia");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getFacNome(final String UUID) {
        if (PlayerFacCache.get(UUID) != null) {
            if (!PlayerFacCache.get(UUID).getNome().equalsIgnoreCase("null")) {
                return PlayerFacCache.get(UUID).getNome();
            } else {
                return null;
            }
        } else {
            String name = getFacNomeDb(UUID);
            if (name != null) {
                new PlayerFacCache(UUID, name, getHierarquiaDb(UUID)).insert();
            } else {
                new PlayerFacCache(UUID, "null", "null").insert();
            }
            return name;
        }
    }

    public static String getFacNomeDb(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM PlayerFac_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Nome");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void createInfoPlayer(final String UUID, final String nome, final String hierarquia) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO PlayerFac_data(UUID, Nome, Hierarquia) VALUES (?, ?, ?)");
                    st.setString(1, UUID);
                    st.setString(2, nome);
                    st.setString(3, hierarquia);
                    st.executeUpdate();

                    new PlayerFacCache(UUID, nome, hierarquia).insert();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }


    //-----------------------------------------------------------------------------------------------------------------------------------


    @EventHandler
    public void onJoin(final PlayerJoinEvent event) {
        final String nome = getFacNomeDb(event.getPlayer().getUniqueId().toString());

        if (PlayerFacCache.get(event.getPlayer().getUniqueId().toString()) != null) {
            PlayerFacCache.CACHE.remove(event.getPlayer().getUniqueId().toString());
        }

        if (nome != null) {
            new PlayerFacCache(event.getPlayer().getUniqueId().toString(), nome, getHierarquiaDb(event.getPlayer().getUniqueId().toString())).insert();
        }
    }

    @EventHandler
    public void onLeft(final PlayerQuitEvent event) {
        if (PlayerFacCache.get(event.getPlayer().getUniqueId().toString()) != null) {
            PlayerFacCache.CACHE.remove(event.getPlayer().getUniqueId().toString());
        }
    }
}
