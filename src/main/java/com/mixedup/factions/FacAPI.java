package com.mixedup.factions;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.mixedup.Main;
import com.mixedup.MySql;
import com.mixedup.factions.api.*;
import com.mixedup.factions.cache.NameWithTagCache;
import com.mixedup.factions.cache.RelacaoCache;
import com.mixedup.factions.hashs.RelacaoFixDb;
import com.mixedup.mcmmo.ProbabilityApi;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

public class FacAPI {

    private static LoadingCache<String, Fac> cache;

    static {
        FacAPI.clearCache();
    }

    public static void clearCache() {
        FacAPI.cache = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterAccess(15, TimeUnit.MINUTES)
                .build(new CacheLoader<String, Fac>() {
                    public Fac load(final String name) {
                        return FacAPI.getFromSQL(name);
                    }
                });
    }

    public static Fac getFac(final String nome) {
        try {
            return FacAPI.cache.get(nome);
        } catch (final Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int getFacOnline(final String uuid) {
        final val facNome = getFacNome(uuid);
        return (int) Bukkit.getOnlinePlayers().stream()
                .filter(pla ->
                        getFacNome(pla.getUniqueId().toString()) != null && getFacNome(pla.getUniqueId().toString()).equals(facNome)).count();
    }

    public static Fac getFromSQL(final String name) {
        return new Fac(
                FacAPI.getNomeDB(name), // ok
                FacAPI.getTerrasDB(name), // ok
                FacAPI.getRecrutasDB(name), // ok
                FacAPI.getMembrosDB(name),  // ok
                FacAPI.getCapitoesDB(name), // ok
                FacAPI.getLideresDB(name),  // ok
                FacAPI.getNexusDB(name), // ok
                FacAPI.getNexusLifeDb(name), // ok
                FacAPI.getSobAttackDb(name), // ok
                FacAPI.getTimeToMoveNexusDb(name), // ok
                FacAPI.getQuantiaPlutonioDb(name), // ok
                FacAPI.getCriadorDb(name), // ok
                FacAPI.getTagWithNomeDb(name), // ok
                FacAPI.getQuantiaWithNomeDb(name)
        );
    }

    public static void createFac(final String nome, final String tag, final String criador, final int economy) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
            /*
            okay?
            TODO Default
            TODO Null values?
             */
                    final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Facçoes_data(Nome, Tag, Criador, Recrutas, Membros, Capitoes, Lideres, QuantiaMembros, Terras, Home, Nexus, Plutonio, TimeMove, Attack, NexusLife, Economy) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                    st.setString(1, nome);
                    st.setString(2, tag);
                    st.setString(3, criador);
                    st.setString(4, "NULL");
                    st.setString(5, "NULL");
                    st.setString(6, "NULL");
                    st.setString(7, criador);
                    st.setInt(8, 1);
                    st.setInt(9, 0);
                    st.setString(10, "NULL");
                    st.setString(11, "NULL");
                    st.setInt(12, 0);
                    st.setFloat(13, 0);
                    st.setBoolean(14, false);
                    st.setInt(15, 100);
                    st.setInt(16, economy);
                    st.executeUpdate();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void updateEconomy(final String nome, final int ecomony) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Facçoes_data SET Economy = ? WHERE Nome = ?");
                    st.setString(2, nome);
                    st.setInt(1, ecomony);
                    st.executeUpdate();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    //-

    public static void updateLifeNexus(final String nome, final int life) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Facçoes_data SET NexusLife = ? WHERE Nome = ?");
                    st.setString(2, nome);
                    st.setInt(1, life);
                    st.executeUpdate();

                    FacAPI.getFac(nome).setNexusLife(life);
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static int getNexusLife(final String nome) {
        return FacAPI.getFac(nome).getNexusLife();
    }

    public static int getNexusLifeDb(final String nome) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Facçoes_data WHERE Nome = ?");
            st.setString(1, nome);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("NexusLife");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    //-

    public static void updateSobAttack(final String nome, final boolean status) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Facçoes_data SET Attack = ? WHERE Nome = ?");
                    st.setString(2, nome);
                    st.setBoolean(1, status);
                    st.executeUpdate();

                    FacAPI.getFac(nome).setSobAttack(status);
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static boolean getSobAttack(final String nome) {
        return FacAPI.getFac(nome).isSobAttack();
    }

    public static boolean getSobAttackDb(final String nome) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Facçoes_data WHERE Nome = ?");
            st.setString(1, nome);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Attack");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void updateSobAttack() {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Facçoes_data SET Attack = ?");
                    st.setBoolean(1, false);
                    st.executeUpdate();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    //-

    public static void updateTimeToMoveNexus(final String nome, final long date) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Facçoes_data SET TimeMove = ? WHERE Nome = ?");
                    st.setString(2, nome);
                    st.setLong(1, date);
                    st.executeUpdate();

                    FacAPI.getFac(nome).setTimeToMoveNexus(date);
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static long getTimeToMoveNexus(final String nome) {
        return FacAPI.getFac(nome).getTimeToMoveNexus();
    }

    public static long getTimeToMoveNexusDb(final String nome) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Facçoes_data WHERE Nome = ?");
            st.setString(1, nome);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getLong("TimeMove");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    //-

    public static int getQuantiaPlutonio(final String nome) {
        return FacAPI.getFac(nome).getPlutonios();
    }

    public static int getQuantiaPlutonioDb(final String nome) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Facçoes_data WHERE Nome = ?");
            st.setString(1, nome);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("Plutonio");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    //-

    public static String getNomeDB(final String nome) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT Nome FROM Facçoes_data WHERE Nome = ?");
            st.setString(1, nome);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Nome");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int getTerras(final String nome) {
        return FacAPI.getFac(nome).getLandCount();
    }

    public static int getTerrasDB(final String nome) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Facçoes_data WHERE Nome = ?");
            st.setString(1, nome);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("Terras");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void updateTerras(final String nome, final int terras) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Facçoes_data SET Terras = ? WHERE Nome = ?");
                    st.setString(2, nome);
                    st.setInt(1, terras);
                    st.executeUpdate();
                    Bukkit.getOnlinePlayers().stream()
                            .filter(p -> FacAPI.getFacNome(p.getUniqueId().toString()) != null && FacAPI.getFacNome(p.getUniqueId().toString()).equals(nome))
                            .forEach(p -> {
                                if (Main.scoreboardManager.hasSquadScoreboard(p)) {
                                    Main.scoreboardManager.getSquadScoreboard().updateEntry(p, "squad_lands");
                                }
                            });
                    FacAPI.getFac(nome).setLandCount(terras);
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static String getNexus(final String nome) {
        return FacAPI.getFac(nome).getNexus();
    }

    public static String getNexusDB(final String nome) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Facçoes_data WHERE Nome = ?");
            st.setString(1, nome);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Nexus");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void updateNexus(final String nome, final String nexus) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Facçoes_data SET Nexus = ? WHERE Nome = ?");
                    st.setString(2, nome);
                    st.setString(1, nexus);
                    st.executeUpdate();
                    FacAPI.getFac(nome).setNexus(nexus);
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void deleteFac(final String nome) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Facçoes_data WHERE Nome = ?");
                    st.setString(1, nome);
                    st.executeUpdate();

                    FacAPI.cache.invalidate(nome);

                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void updateCriador(final String nome, final String criador) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Facçoes_data SET Criador = ? WHERE Nome = ?");
                    st.setString(2, nome);
                    st.setString(1, criador);
                    st.executeUpdate();

                    FacAPI.getFac(nome).setCreator(criador);
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void updateQuantiaMembros(final String nome, final int quantia) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Facçoes_data SET QuantiaMembros = ? WHERE Nome = ?");
                    st.setString(2, nome);
                    st.setInt(1, quantia);
                    st.executeUpdate();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void updateLideres(final String nome, final String lideres) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Facçoes_data SET Lideres = ? WHERE Nome = ?");
                    st.setString(2, nome);
                    st.setString(1, lideres);
                    st.executeUpdate();
                    FacAPI.getFac(nome).setLeaders(lideres);
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void updateCapitoes(final String nome, final String capitoes) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Facçoes_data SET Capitoes = ? WHERE Nome = ?");
                    st.setString(2, nome);
                    st.setString(1, capitoes);
                    st.executeUpdate();
                    FacAPI.getFac(nome).setCaptains(capitoes);
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void updateMembros(final String nome, final String membros) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Facçoes_data SET Membros = ? WHERE Nome = ?");
                    st.setString(2, nome);
                    st.setString(1, membros);
                    st.executeUpdate();
                    FacAPI.getFac(nome).setMembers(membros);
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void updateRecrutas(final String nome, final String recruta) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Facçoes_data SET Recrutas = ? WHERE Nome = ?");
                    st.setString(2, nome);
                    st.setString(1, recruta);
                    st.executeUpdate();
                    FacAPI.getFac(nome).setRecruits(recruta);
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static String getCriador(final String nome) {
        return FacAPI.getFac(nome).getCreator();
    }

    public static String getCriadorDb(final String nome) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Facçoes_data WHERE Nome = ?");
            st.setString(1, nome);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Criador");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getTagWithNome(final String nome) {
        return FacAPI.getFac(nome).getTag();
    }

    public static String getTagWithNomeDb(final String nome) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Facçoes_data WHERE Nome = ?");
            st.setString(1, nome);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Tag");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void addRecruta(final String nome, final String recruta) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Facçoes_data SET Recrutas = ? WHERE Nome = ?");
                    st.setString(2, nome);
                    String recrutas = null;
                    if (FacAPI.getRecrutas(nome).equals("NULL")) {
                        recrutas = recruta;
                    } else {
                        recrutas = FacAPI.getRecrutas(nome) + ":" + recruta;
                    }
                    st.setString(1, recrutas);
                    st.executeUpdate();
                    FacAPI.getFac(nome).setRecruits(recrutas);
                    FacAPI.updateQuantiaMembros(nome, FacAPI.getQuantiaWithNome(nome) + 1);
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static String getLideres(final String nome) {
        return FacAPI.getFac(nome).getLeaders();
    }

    public static String getLideresDB(final String nome) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Facçoes_data WHERE Nome = ?");
            st.setString(1, nome);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Lideres");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String getCapitoes(final String nome) {
        return FacAPI.getFac(nome).getCaptains();
    }

    public static String getCapitoesDB(final String nome) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Facçoes_data WHERE Nome = ?");
            st.setString(1, nome);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Capitoes");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getMembros(final String nome) {
        return FacAPI.getFac(nome).getMembers();
    }

    public static String getMembrosDB(final String nome) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Facçoes_data WHERE Nome = ?");
            st.setString(1, nome);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Membros");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getRecrutas(final String nome) {
        return FacAPI.getFac(nome).getRecruits();
    }

    public static String getRecrutasDB(final String nome) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Facçoes_data WHERE Nome = ?");
            st.setString(1, nome);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Recrutas");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int getQuantiaWithNome(final String nome) {
        return FacAPI.getFac(nome).getMembersAmount();
    }

    public static int getQuantiaWithNomeDb(final String nome) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Facçoes_data WHERE Nome = ?");
            st.setString(1, nome);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("QuantiaMembros");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }


    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public static String getTag(final String UUID) {
        if (FacAPI.getFac(FacAPI.getFacNome(UUID)).getCreator().equalsIgnoreCase(UUID)) {
            return FacAPI.getFac(FacAPI.getFacNome(UUID)).getTag();
        } else {
            return null;
        }
        // try {
        //            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Facçoes_data WHERE Criador = ?");
        //            st.setString(1, UUID);
        //            ResultSet rs = st.executeQuery();
        //            while (rs.next()) {
        //                return rs.getString("Tag");
        //            }
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
        //        return null;
    }

    public static String getNome(final String UUID) {
        if (FacAPI.getFac(FacAPI.getFacNome(UUID)).getCreator().equalsIgnoreCase(UUID)) {
            return FacAPI.getFac(FacAPI.getFacNome(UUID)).getName();
        } else {
            return null;
        }
        // try {
        //            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Facçoes_data WHERE Criador = ?");
        //            st.setString(1, UUID);
        //            ResultSet rs = st.executeQuery();
        //            while (rs.next()) {
        //                return rs.getString("Nome");
        //            }
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
        //        return null;
    }

    public static int getQuantia(final String UUID) {
        if (FacAPI.getFac(FacAPI.getFacNome(UUID)) != null) {
            return FacAPI.getFac(FacAPI.getFacNome(UUID)).getMembersAmount();
        } else {
            return 0;
        }
        // try {
        //            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Facçoes_data WHERE Criador = ?");
        //            st.setString(1, UUID);
        //            ResultSet rs = st.executeQuery();
        //            while (rs.next()) {
        //                return rs.getInt("QuantiaMembros");
        //            }
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
        //        return 0;
    }

    public static String getExistNome(final String nome) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Facçoes_data WHERE Nome = ?");
            st.setString(1, nome);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Nome");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getExistTag(final String tag) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Facçoes_data WHERE Tag = ?");
            st.setString(1, tag);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Tag");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getNomeWithTag(final String tag) {
        if (NameWithTagCache.get(tag) != null) {
            return NameWithTagCache.get(tag).getNome();
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Facçoes_data WHERE Tag = ?");
                st.setString(1, tag);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final String name = rs.getString("Nome");
                    new NameWithTagCache(tag, name).insert();
                    return name;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


    public static void updateInfos2() {
        new BukkitRunnable() {
            @Override
            public void run() {
                try (final val rs = MySql.con.prepareStatement("SELECT * FROM Facções_data WHERE Nome != 'NULL' AND NexusLife > 0")) {
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        };
    }


    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


    public static void updateInfos() {
        final Thread thread = new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(1000 * 600);
                } catch (final Exception e) {
                    e.printStackTrace();
                }

                try {
                    final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Facçoes_data");
                    final ResultSet rs = st.executeQuery();
                    while (rs.next()) {
                        if (FacAPI.getNexus(rs.getString("Nome")) != null && !FacAPI.getNexus(rs.getString("Nome")).equalsIgnoreCase("NULL")) {
                            final String[] s = FacAPI.getNexus(rs.getString("Nome")).split("::");
                            final String[] split1 = s[0].split(":");
                            final String[] split2 = s[1].split(":");
                            final Location p1 = new Location(Bukkit.getWorld("Trappist-1b"), Integer.valueOf(split1[0]), Integer.valueOf(split1[1]), Integer.valueOf(split1[2]));
                            final Location p2 = new Location(Bukkit.getWorld("Trappist-1b"), Integer.valueOf(split2[0]), Integer.valueOf(split2[1]), Integer.valueOf(split2[2]));
                            final Location result = p1.add(p1.subtract(p2).multiply(0.5));

                            if (rs.getInt("NexusLife") > 0) {
                                final String chunk = result.getChunk().getX() + ":" + result.getChunk().getZ();
                                final int probability = getProbab(chunk) * 1000;
                                if (ProbabilityApi.probab(probability) == true) {
                                    final PreparedStatement st2 = MySql.con.prepareStatement("UPDATE Facçoes_data SET Plutonio = ? WHERE Nome = ?");
                                    st2.setString(2, rs.getString("Nome"));
                                    st2.setInt(1, FacAPI.getQuantiaPlutonio(rs.getString("Nome")) + 2);
                                    st2.executeUpdate();

                                    FacAPI.getFac(rs.getString("Nome")).setPlutonios(FacAPI.getQuantiaPlutonio(rs.getString("Nome")) + 2);
                                } else {
                                    final PreparedStatement st2 = MySql.con.prepareStatement("UPDATE Facçoes_data SET Plutonio = ? WHERE Nome = ?");
                                    st2.setString(2, rs.getString("Nome"));
                                    st2.setInt(1, FacAPI.getQuantiaPlutonio(rs.getString("Nome")) + 1);
                                    st2.executeUpdate();

                                    FacAPI.getFac(rs.getString("Nome")).setPlutonios(FacAPI.getQuantiaPlutonio(rs.getString("Nome")) + 1);
                                }
                            }
                        }
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }

                try {
                    final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Facçoes_data");
                    final ResultSet rs = st.executeQuery();
                    while (rs.next()) {
                        if (rs.getInt("NexusLife") > 0 && rs.getInt("NexusLife") < 100) {
                            final PreparedStatement st2 = MySql.con.prepareStatement("UPDATE Facçoes_data SET NexusLife = ? WHERE Nome = ?");
                            st2.setString(2, rs.getString("Nome"));
                            st2.setInt(1, rs.getInt("NexusLife") + 1);
                            st2.executeUpdate();
                        }
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    //------------------------------------------------------------------------------------------------------------------

    public static void removeRelacao(final String nome) {
        RelacaoAPI.removeRelacao(nome);
    }

    public static String getAliados(final String nome) {
        if (RelacaoCache.get(nome) != null) {
            return RelacaoCache.get(nome).getAliados();
        } else {
            if (RelacaoFixDb.get(nome) == null) {
                if (RelacaoAPI.getAliadosDB(nome) != null) {
                    new RelacaoCache(nome, RelacaoAPI.getInimigosDB(nome), RelacaoAPI.getAliadosDB(nome), RelacaoAPI.getAmigosDB(nome), RelacaoAPI.getRivaisDB(nome)).insert();

                    return RelacaoCache.get(nome).getAliados();
                } else {
                    new RelacaoFixDb(nome, true).insert();

                    return null;
                }
            } else if (RelacaoFixDb.get(nome).getStatus() == true) {
                return null;
            }
        }
        return null;
    }

    public static String getInimigos(final String nome) {
        if (RelacaoCache.get(nome) != null) {
            return RelacaoCache.get(nome).getInimigos();
        } else {
            if (RelacaoFixDb.get(nome) == null) {
                if (RelacaoAPI.getAliadosDB(nome) != null) {
                    new RelacaoCache(nome, RelacaoAPI.getInimigosDB(nome), RelacaoAPI.getAliadosDB(nome), RelacaoAPI.getAmigosDB(nome), RelacaoAPI.getRivaisDB(nome)).insert();

                    return RelacaoCache.get(nome).getInimigos();
                } else {
                    new RelacaoFixDb(nome, true).insert();

                    return null;
                }
            } else if (RelacaoFixDb.get(nome).getStatus() == true) {
                return null;
            }
        }
        return null;
    }

    public static String getRivais(final String nome) {
        if (RelacaoCache.get(nome) != null) {
            return RelacaoCache.get(nome).getRivais();
        } else {
            if (RelacaoFixDb.get(nome) == null) {
                if (RelacaoAPI.getAliadosDB(nome) != null) {
                    new RelacaoCache(nome, RelacaoAPI.getInimigosDB(nome), RelacaoAPI.getAliadosDB(nome), RelacaoAPI.getAmigosDB(nome), RelacaoAPI.getRivaisDB(nome)).insert();

                    return RelacaoCache.get(nome).getRivais();
                } else {
                    new RelacaoFixDb(nome, true).insert();

                    return null;
                }
            } else if (RelacaoFixDb.get(nome).getStatus() == true) {
                return null;
            }
        }
        return null;
    }

    public static String getAmigos(final String nome) {
        if (RelacaoCache.get(nome) != null) {
            return RelacaoCache.get(nome).getAmigos();
        } else {
            if (RelacaoFixDb.get(nome) == null) {
                if (RelacaoAPI.getAliadosDB(nome) != null) {
                    new RelacaoCache(nome, RelacaoAPI.getInimigosDB(nome), RelacaoAPI.getAliadosDB(nome), RelacaoAPI.getAmigosDB(nome), RelacaoAPI.getRivaisDB(nome)).insert();

                    return RelacaoCache.get(nome).getAmigos();
                } else {
                    new RelacaoFixDb(nome, true).insert();

                    return null;
                }
            } else if (RelacaoFixDb.get(nome).getStatus() == true) {
                return null;
            }
        }
        return null;
    }

    public static void updateRivais(final String nome, final String rivais) {
        RelacaoAPI.updateRivais(nome, rivais);
    }

    public static void updateAmigos(final String nome, final String amigos) {
        RelacaoAPI.updateAmigos(nome, amigos);
    }

    public static void updateAliados(final String nome, final String aliados) {
        RelacaoAPI.updateAliados(nome, aliados);
    }

    public static void updateInimigos(final String nome, final String inimigos) {
        RelacaoAPI.updateInimigos(nome, inimigos);
    }

    public static void createRelacoes(final String nome) {
        RelacaoAPI.createRelacoes(nome);
    }

    //------------------------------------------------------------------------------------------------------------------

    public static void createPermAliado(final String UUID) {
        FacPermAPI.createPermAliado(UUID);
    }

    public static void removePermAliado(final String UUID) {
        FacPermAPI.removePermAliado(UUID);
    }

    public static boolean getAliadoPermAcesso(final String UUID) {
        return FacPermAPI.getAliadoPermAcesso(UUID);
    }

    public static void updateAliadoPermAcesso(final String UUID, final boolean status) {
        FacPermAPI.updateAliadoPermAcesso(UUID, status);
    }

    public static boolean getAliadoPermChests(final String UUID) {
        return FacPermAPI.getAliadoPermChests(UUID);
    }

    public static void updateAliadoPermChests(final String UUID, final boolean status) {
        FacPermAPI.updateAliadoPermChests(UUID, status);
    }

    public static boolean getAliadoPermBuild(final String UUID) {
        return FacPermAPI.getAliadoPermBuild(UUID);
    }

    public static void updateAliadoPermBuild(final String UUID, final boolean status) {
        FacPermAPI.updateAliadoPermBuild(UUID, status);
    }

    public static void permLiderCreate(final String UUID) {
        FacPermAPI.permLiderCreate(UUID);
    }

    public static void playerCreatePerms(final String UUID) {
        FacPermAPI.playerCreatePerms(UUID);
    }

    public static void updatePermSpawners(final String UUID, final boolean status) {
        FacPermAPI.updatePermSpawners(UUID, status);
    }

    public static void updatePermExpulsar(final String UUID, final boolean status) {
        FacPermAPI.updatePermExpulsar(UUID, status);
    }

    public static void updatePermRecrutar(final String UUID, final boolean status) {
        FacPermAPI.updatePermRecrutar(UUID, status);
    }

    public static void updatePermAbandonar(final String UUID, final boolean status) {
        FacPermAPI.updatePermAbandonar(UUID, status);
    }

    public static void updatePermDominar(final String UUID, final boolean status) {
        FacPermAPI.updatePermDominar(UUID, status);
    }

    public static void updatePermChests(final String UUID, final boolean status) {
        FacPermAPI.updatePermChests(UUID, status);
    }

    public static void updatePermBuild(final String UUID, final boolean status) {
        FacPermAPI.updatePermBuild(UUID, status);
    }

    public static boolean getPermSpawners(final String UUID) {
        return FacPermAPI.getPermSpawners(UUID);
    }

    public static boolean getPermExpulsar(final String UUID) {
        return FacPermAPI.getPermExpulsar(UUID);
    }

    public static boolean getPermRecrutar(final String UUID) {
        return FacPermAPI.getPermRecrutar(UUID);
    }

    public static boolean getPermAbandonar(final String UUID) {
        return FacPermAPI.getPermAbandonar(UUID);
    }

    public static boolean getPermDominar(final String UUID) {
        return FacPermAPI.getPermDominar(UUID);
    }

    public static boolean getPermChests(final String UUID) {
        return FacPermAPI.getPermChests(UUID);
    }

    public static boolean getPermBuild(final String UUID) {
        return FacPermAPI.getPermBuild(UUID);
    }

    public static void removerPlayerPerms(final String UUID) {
        FacPermAPI.removerPlayerPerms(UUID);
    }
    //------------------------------------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------------------------------------
    public static void createProbabInfo(final String chunk, final int probab) {
        GeracaoAPI.createProbabInfo(chunk, probab);
    }

    public static int getProbab(final String chunk) {
        return GeracaoAPI.getProbab(chunk);
    }

    public static String getExistProbab(final String chunk) {
        return GeracaoAPI.getExistProbab(chunk);
    }
    //------------------------------------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------------------------------------
    public static String getChunkOwn(final String chunk) {
        return ClainsAPI.getChunkOwn(chunk);
    }

    public static void removeChunk(final String chunk) {
        ClainsAPI.removeChunk(chunk);
    }

    public static void updateChunk(final String chunk, final String tag) {
        ClainsAPI.updateChunk(chunk, tag);
    }

    public static void createChunk(final String chunk, final String tag) {
        ClainsAPI.createChunk(chunk, tag);
    }
    //------------------------------------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------------------------------------
    public static String getSpawn(final String nome) {
        return PlotEsqAPI.getSpawn(nome);
    }

    public static String getDescricao(final String nome) {
        return PlotEsqAPI.getDescricao(nome);
    }

    public static String getPlots(final String nome) {
        return PlotEsqAPI.getPlots(nome);
    }

    public static void updatePlots(final String nome, final String plots) {
        PlotEsqAPI.updatePlots(nome, plots);
    }

    public static void updateSpawn(final String nome, final String spawn) {
        PlotEsqAPI.updateSpawn(nome, spawn);
    }

    public static void updateDescricao(final String nome, final String desc) {
        PlotEsqAPI.updateDescricao(nome, desc);
    }

    public static void createInfoPlot(final String nome) {
        PlotEsqAPI.createInfoPlot(nome);
    }
    //------------------------------------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------------------------------------
    public static void updateInfo(final String UUID, final String hierarquia) {
        PlayerFacAPI.updateInfo(UUID, hierarquia);
    }


    public static void deletePlayerInfo(final String UUID) {
        PlayerFacAPI.deletePlayerInfo(UUID);
    }

    public static void updateHierarquia(final String UUID, final String hierarquia) {
        PlayerFacAPI.updateHierarquia(UUID, hierarquia);
    }

    public static String getHierarquia(final String UUID) {
        return PlayerFacAPI.getHierarquia(UUID);
    }

    public static String getFacNome(final String UUID) {
        return PlayerFacAPI.getFacNome(UUID);
    }

    public static void createInfoPlayer(final String UUID, final String nome, final String hierarquia) {
        PlayerFacAPI.createInfoPlayer(UUID, nome, hierarquia);
    }
    //------------------------------------------------------------------------------------------------------------------
}
