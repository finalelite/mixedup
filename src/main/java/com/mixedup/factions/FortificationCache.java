package com.mixedup.factions;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.mixedup.MySql;
import lombok.val;

import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

public class FortificationCache {

    LoadingCache<String, FortificationEntry> cache;

    public FortificationCache() {
        this.clearCache();
    }

    public void clearCache() {
        this.cache = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterAccess(15, TimeUnit.MINUTES)
                .build(new CacheLoader<String, FortificationEntry>() {
                    public FortificationEntry load(final String location) {
                        return FortificationCache.this.getFromSQL(location);
                    }
                });
    }

    public FortificationEntry getFortification(final String location) {
        try {
            return this.cache.get(location);
        } catch (final Exception e) {
            return null;
        }
    }

    public FortificationEntry getFromSQL(final String location) {
        try (final val stmt = MySql.con.prepareStatement("SELECT * FROM Fortification_data WHERE Location = ?")) {
            stmt.setString(1, location);

            final val rs = stmt.executeQuery();

            if (rs.next())
                return new FortificationEntry(rs.getInt("ID"), rs.getString("Location"), rs.getInt("Life"), rs.getLong("LastDamage"), rs.getInt("Type"));
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
