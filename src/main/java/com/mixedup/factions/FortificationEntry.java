package com.mixedup.factions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class FortificationEntry {

    private int id;
    private String location;
    private int life;
    private long lastDamage;
    private int type;

}
