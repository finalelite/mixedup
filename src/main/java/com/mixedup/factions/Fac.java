package com.mixedup.factions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Fac {
    private String name;
    private int landCount;
    private String recruits;
    private String members;
    private String captains;
    private String leaders;
    private String nexus;
    private int nexusLife;
    private boolean sobAttack;
    private long timeToMoveNexus;
    private int plutonios;
    private String creator;
    private String tag;
    private int membersAmount;
}
