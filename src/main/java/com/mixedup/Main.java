package com.mixedup;

import br.com.finalelite.pauloo27.api.PauloAPI;
import br.com.finalelite.pauloo27.api.commands.CommandablePlugin;
import br.com.finalelite.pauloo27.api.database.PlayerCoinType;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import com.mixedup.apis.EnergiaAPI;
import com.mixedup.apis.ServerAPI;
import com.mixedup.commands.CommandScoreboard;
import com.mixedup.utils.RestartUtils;
import com.mixedup.utils.ScoreboardManager;
import com.mixedup.utils.SocketUtils;
import com.mixedup.vipAutentication.MySqlWeb;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;

public class Main extends CommandablePlugin implements PluginMessageListener {

    public static Main instance;
    public static Plugin plugin;
    public static String ip = "149.56.243.159";
    public static Map<String, Level> filterList = new HashMap<>();
    public static ScoreboardManager scoreboardManager = new ScoreboardManager();
    public static RestartUtils rUtils = new RestartUtils();
    public static Instant startedDate;

    static {
        Main.filterList.put("com.mojang.authlib.GameProfile@\\w*\\[id=[\\w|-]*,name=\\w*,properties=\\{[\\w|.|=|\\[|\\]|@]*},legacy=\\w*] \\(\\/[0-9|.]*:[0-9]*\\) lost connection: Servidor lotado!", Level.INFO);
//        filterList.put("[^]+com.mojang.authlib.exceptions.AuthenticationException: The client has sent too many requests within a certain amount of time[^]+", Level.WARNING);
    }

    public static Main getInstance() {
        return Main.instance;
    }

    public void onEnable() {
        MySql.connect();
        MySqlHub.connect();
        MySqlWeb.connect();
        SocketUtils.online();
        Main.instance = this;
        Main.plugin = this;
        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", this);
        RegistersMain.enable();
        //rUtils.startTask();

        this.getLogger().setFilter(logRecord -> {
            final val message = logRecord.getMessage();

            return (Main.filterList.entrySet().stream()
                    .noneMatch(entry ->
                            (entry.getValue() == null || (entry.getValue() == logRecord.getLevel())) &&
                                    message.matches(entry.getKey())
                    ));
        });

        PauloAPI.getInstance().getEconomyManager()
                .addListener((uuid, type, oldValue, newValue) -> {
                    final Player player;
                    if ((player = Bukkit.getPlayer(UUID.fromString(uuid))) != null) {
                        if (Main.scoreboardManager.hasSquadScoreboard(player)) {
                            if (type == PlayerCoinType.COIN)
                                Main.scoreboardManager.getSquadScoreboard().updateEntry(player, "coins");
                            else
                                Main.scoreboardManager.getSquadScoreboard().updateEntry(player, "blackcoins");
                        } else {
                            if (type == PlayerCoinType.COIN)
                                Main.scoreboardManager.getDefaultScoreboard().updateEntry(player, "coins");
                            else
                                Main.scoreboardManager.getDefaultScoreboard().updateEntry(player, "blackcoins");
                            EnergiaAPI.updateEnergia(uuid, EnergiaAPI.getEnergiaSQL(uuid));
                            Main.scoreboardManager.getDefaultScoreboard().updateEntry(player, "power");
                        }
                    }
                });

        // enable connections from hub
        ServerAPI.setRestarting(false);
        Main.startedDate = Instant.now();
        this.registerCommand(new CommandScoreboard());
    }

    public void onDisable() {
        RegistersMain.disable();
        ServerAPI.updateOnline(ServerAPI.getServerName(Bukkit.getServer().getPort()), 0);
        ServerAPI.updateOnlineStatus(ServerAPI.getServerName(Bukkit.getServer().getPort()), false);
        MySql.disconnect();
        MySqlHub.disconnect();
    }

    @Override
    public void onPluginMessageReceived(final String channel, final Player player, final byte[] message) {
        if (!channel.equals("BungeeCord")) {
            return;
        }
        final ByteArrayDataInput in = ByteStreams.newDataInput(message);
        final String subchannel = in.readUTF();

        if (subchannel.equals("PlayerList")) {
            //
        }
    }

}
