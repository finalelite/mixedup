package com.mixedup.mcmmo.skills;

import com.mixedup.Main;
import com.mixedup.apis.BreweAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.hashs.HasAddItemBrew;
import com.mixedup.hashs.RotacaoAlq;
import com.mixedup.mcmmo.McMMOApi;
import com.mixedup.mcmmo.listeners.BrewingPlayer;
import com.mixedup.mcmmo.utils.BoosterHash;
import com.mixedup.utils.AlternateColor;
import org.bukkit.*;
import org.bukkit.block.BrewingStand;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.BrewEvent;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AlquimiaEvent implements Listener {

    public static void verifyLevelUp(final Player player, final int xpAnterior) {
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (BoosterHash.get(UUID) != null) {
            if (BoosterHash.get(UUID).gettime() - (new Date().getTime() / 1000) <= 0) {
                BoosterHash.CACHE.remove(UUID);
            }
        }

        if (McMMOApi.getProximoNivelXP(UUID + "Alquimia") <= McMMOApi.getXP(UUID + "Alquimia")) {
            McMMOApi.updateLevel(UUID + "Alquimia");

            if (McMMOApi.getNivel(UUID + "Alquimia") == 100 || McMMOApi.getNivel(UUID + "Alquimia") == 200 || McMMOApi.getNivel(UUID + "Alquimia") == 300 || McMMOApi.getNivel(UUID + "Alquimia") == 400 || McMMOApi.getNivel(UUID + "Alquimia") == 500 || McMMOApi.getNivel(UUID + "Alquimia") == 600 || McMMOApi.getNivel(UUID + "Alquimia") == 700 || McMMOApi.getNivel(UUID + "Alquimia") == 800) {
                for (final Player target : Bukkit.getOnlinePlayers()) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") != 800) {
                        target.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (McMMOApi.getNivel(UUID + "Alquimia")) + "&e na habilidade de Alquimia!&e!\n \n"));
                        if (target.getName().equals(player.getName())) {
                            target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                        } else {
                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        target.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade de Alquimia!\n \n"));
                        if (target.getName().equals(player.getName())) {
                            target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                            target.playSound(target.getLocation().add(10, 0, 10), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                        } else {
                            target.playSound(target.getLocation().add(10, 0, 10), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                        }
                    }
                }
            } else {
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (McMMOApi.getNivel(UUID + "Alquimia"))) + ChatColor.YELLOW + " na habilidade de Alquimia!");
            }
        } else {

            player.sendActionBar(ChatColor.GREEN + "Alquimia (" + (McMMOApi.getXP(UUID + "Alquimia")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Alquimia")) + ")XP +" + (McMMOApi.getXP(UUID + "Alquimia") - xpAnterior) + "XP");

        }
    }

    @EventHandler
    public void onBrew(final BrewEvent event) {
        final String location = event.getBlock().getWorld().getName() + ":" + (event.getBlock().getX()) + ":" + (event.getBlock().getY()) + ":" + (event.getBlock().getZ());
        if (BrewingPlayer.get(location) == null) return;
        final String UUID = BrewingPlayer.get(location).getUUID();
        final Player player = BrewingPlayer.get(location).getPlayer();
        final ItemStack ingredient = event.getContents().getIngredient();

        if (BoosterHash.get(UUID) != null) {
            if (BoosterHash.get(UUID).gettime() - (new Date().getTime() / 1000) <= 0) {
                BoosterHash.CACHE.remove(UUID);
            }
        }

        if (ingredient.getType().equals(Material.NETHER_WART)) {
            for (int i = 1; i <= 3; i++) {
                if (event.getContents().getItem(i - 1) != null && event.getContents().getItem(i - 1).getType().equals(Material.POTION)) {
                    final PotionMeta meta = (PotionMeta) event.getContents().getItem(i - 1).getItemMeta();
                    if (meta.getCustomEffects().size() == 0) {
                        final int xpAnterior = McMMOApi.getXP(UUID + "Alquimia");
                        int xpNew = 5;
                        if (BoosterHash.get(UUID) != null) {
                            if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                xpNew += (xpNew * 25) / 100;
                            } else {
                                xpNew += (xpNew * 50) / 100;
                            }

                        }
                        McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew);
                        AlquimiaEvent.verifyLevelUp(player, xpAnterior);
                    }
                }
            }
        }

        if (ingredient.getType().equals(Material.GLOWSTONE_DUST) || ingredient.getType().equals(Material.REDSTONE)) {
            final int xpAnterior = McMMOApi.getXP(UUID + "Alquimia");
            int xpNew = 8;
            if (BoosterHash.get(UUID) != null) {
                if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                    xpNew += (xpNew * 25) / 100;
                } else {
                    xpNew += (xpNew * 50) / 100;
                }

                if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                    BoosterHash.CACHE.remove(UUID);
                }
            }
            McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew);
            AlquimiaEvent.verifyLevelUp(player, xpAnterior);
        }

        if (ingredient.getType().equals(Material.GUNPOWDER) || ingredient.getType().equals(Material.DRAGON_BREATH)) {
            final int xpAnterior = McMMOApi.getXP(UUID + "Alquimia");
            int xpNew = 12;
            if (BoosterHash.get(UUID) != null) {
                if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                    xpNew += (xpNew * 25) / 100;
                } else {
                    xpNew += (xpNew * 50) / 100;
                }

                if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                    BoosterHash.CACHE.remove(UUID);
                }
            }
            McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew);
            AlquimiaEvent.verifyLevelUp(player, xpAnterior);
        }

        if (ingredient.getType().equals(Material.FERMENTED_SPIDER_EYE)) {
            for (int i = 1; i <= 3; i++) {
                if (event.getContents().getItem(i - 1) != null && event.getContents().getItem(i - 1).getType().equals(Material.POTION)) {
                    final PotionMeta meta = (PotionMeta) event.getContents().getItem(i - 1).getItemMeta();
                    if (meta.getCustomEffects().size() == 0) {
                        final int xpAnterior = McMMOApi.getXP(UUID + "Alquimia");
                        int xpNew = 5;
                        if (BoosterHash.get(UUID) != null) {
                            if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                xpNew += (xpNew * 25) / 100;
                            } else {
                                xpNew += (xpNew * 50) / 100;
                            }

                        }
                        McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew);
                        AlquimiaEvent.verifyLevelUp(player, xpAnterior);
                    } else {
                        for (int x = 1; x <= meta.getCustomEffects().size(); x++) {
                            if (meta.getCustomEffects().get(x).getType().equals(PotionEffectType.SPEED) || meta.getCustomEffects().get(x).getType().equals(PotionEffectType.JUMP) || meta.getCustomEffects().get(x).getType().equals(PotionEffectType.REGENERATION)
                                    || meta.getCustomEffects().get(x).getType().equals(PotionEffectType.POISON) || meta.getCustomEffects().get(x).getType().equals(PotionEffectType.NIGHT_VISION)) {
                                final int xpAnterior = McMMOApi.getXP(UUID + "Alquimia");
                                int xpNew = 5;
                                if (BoosterHash.get(UUID) != null) {
                                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                        xpNew += (xpNew * 25) / 100;
                                    } else {
                                        xpNew += (xpNew * 50) / 100;
                                    }

                                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                        BoosterHash.CACHE.remove(UUID);
                                    }
                                }
                                McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew);
                                AlquimiaEvent.verifyLevelUp(player, xpAnterior);
                            }
                        }
                    }
                }
            }
        }

        if (ingredient.getType().equals(Material.SPIDER_EYE) || ingredient.getType().equals(Material.GHAST_TEAR)
                || ingredient.getType().equals(Material.RABBIT_FOOT) || ingredient.getType().equals(Material.BLAZE_POWDER) || ingredient.getType().equals(Material.GLISTERING_MELON_SLICE) || ingredient.getType().equals(Material.SUGAR)
                || ingredient.getType().equals(Material.MAGMA_CREAM) || ingredient.getType().equals(Material.PUFFERFISH) || ingredient.getType().equals(Material.GOLDEN_CARROT) || ingredient.getType().equals(Material.TURTLE_HELMET)
                || ingredient.getType().equals(Material.PHANTOM_MEMBRANE) || ingredient.getType().equals(Material.SPIDER_EYE)) {
            for (int i = 1; i <= 3; i++) {
                if (event.getContents().getItem(i - 1) != null && event.getContents().getItem(i - 1).getType().equals(Material.POTION)) {
                    final PotionMeta meta = (PotionMeta) event.getContents().getItem(i - 1).getItemMeta();
                    if (meta.getCustomEffects().size() == 0) {
                        final int xpAnterior = McMMOApi.getXP(UUID + "Alquimia");
                        int xpNew = 5;
                        if (BoosterHash.get(UUID) != null) {
                            if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                xpNew += (xpNew * 25) / 100;
                            } else {
                                xpNew += (xpNew * 50) / 100;
                            }

                        }
                        McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew);
                        AlquimiaEvent.verifyLevelUp(player, xpAnterior);
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void inventoryBrew(InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (BoosterHash.get(UUID) != null) {
            if (BoosterHash.get(UUID).gettime() - (new Date().getTime() / 1000) <= 0) {
                BoosterHash.CACHE.remove(UUID);
            }
        }

        if (event.getClick() == ClickType.LEFT) {
            if (event.getClickedInventory() != null && event.getClickedInventory().getType().equals(InventoryType.BREWING)) {

                if (event.getCursor() == null || event.getCursor().getType().equals(Material.AIR)) {
                    if (event.getSlot() == 0 || event.getSlot() == 1 || event.getSlot() == 2) {
                        if (event.getCurrentItem().getType().equals(Material.POTION) || event.getCurrentItem().getType().equals(Material.SPLASH_POTION)) {
                            final Inventory inventory = event.getClickedInventory();

                            if (event.getSlot() == 0) {
                                event.setCancelled(true);
                                final ItemStack item = inventory.getItem(0).clone();
                                event.setCursor(item);
                                player.getOpenInventory().getTopInventory().setItem(event.getSlot(), new ItemStack(Material.AIR));
                            }
                            if (event.getSlot() == 1) {
                                event.setCancelled(true);
                                final ItemStack item = inventory.getItem(0).clone();
                                event.setCursor(item);
                                player.getOpenInventory().getTopInventory().setItem(event.getSlot(), new ItemStack(Material.AIR));
                            }
                            if (event.getSlot() == 2) {
                                event.setCancelled(true);
                                final ItemStack item = inventory.getItem(0).clone();
                                event.setCursor(item);
                                player.getOpenInventory().getTopInventory().setItem(event.getSlot(), new ItemStack(Material.AIR));
                            }

                            final Location block = event.getClickedInventory().getLocation();
                            final String location = block.getWorld().getName() + ":" + (block.getBlockX()) + ":" + (block.getBlockY()) + ":" + (block.getBlockZ());
                            if (RotacaoAlq.get(location) != null && RotacaoAlq.get(location).getId() != 0) {
                                Bukkit.getScheduler().cancelTask(RotacaoAlq.get(location).getId());
                                RotacaoAlq.get(location).setId(0);
                            }
                            return;
                        } else {
                            return;
                        }
                    }
                    if (event.getSlot() == 3) {
                        if (event.getCurrentItem() != null) {
                            if (event.getCurrentItem().getType().equals(Material.LILY_PAD) || event.getCurrentItem().getType().equals(Material.GOLD_NUGGET) || event.getCurrentItem().getType().equals(Material.APPLE) || event.getCurrentItem().getType().equals(Material.SUGAR_CANE) || event.getCurrentItem().getType().equals(Material.ROTTEN_FLESH) || event.getCurrentItem().getType().equals(Material.BROWN_MUSHROOM)
                                    || event.getCurrentItem().getType().equals(Material.RED_MUSHROOM) || event.getCurrentItem().getType().equals(Material.CARROT) || event.getCurrentItem().getType().equals(Material.SLIME_BALL) || event.getCurrentItem().equals(Material.CHORUS_FRUIT) || event.getCurrentItem().equals(Material.GOLDEN_APPLE) || event.getCurrentItem().getType().equals(Material.DIAMOND_BLOCK)
                                    || event.getCurrentItem().getType().equals(Material.TURTLE_EGG) || event.getCurrentItem().getType().equals(Material.ENDER_EYE)) {
                                final Location block = event.getClickedInventory().getLocation();
                                final String location = block.getWorld().getName() + ":" + (block.getBlockX()) + ":" + (block.getBlockY()) + ":" + (block.getBlockZ());
                                BreweAPI.removeInfo(location);

                                if (RotacaoAlq.get(location) != null && RotacaoAlq.get(location).getId() != 0) {
                                    Bukkit.getScheduler().cancelTask(RotacaoAlq.get(location).getId());
                                    RotacaoAlq.get(location).setId(0);
                                }
                                return;
                            }
                        } else {
                            return;
                        }
                    }
                    return;
                }

                final Inventory inventory = player.getOpenInventory().getTopInventory();
                BrewingStand stand = (BrewingStand) event.getClickedInventory().getHolder();

                ItemStack hand = event.getCursor().clone();
                if (hand == null || hand.getType() == Material.AIR) {
                    return;
                } else {
                    if (!hand.getType().equals(Material.LILY_PAD) && !hand.getType().equals(Material.GOLD_NUGGET) && !hand.getType().equals(Material.APPLE) && !hand.getType().equals(Material.SUGAR_CANE) && !hand.getType().equals(Material.ROTTEN_FLESH) && !hand.getType().equals(Material.BROWN_MUSHROOM)
                            && !hand.getType().equals(Material.RED_MUSHROOM) && !hand.getType().equals(Material.CARROT) && !hand.getType().equals(Material.SLIME_BALL) && !hand.getType().equals(Material.CHORUS_FRUIT) && !hand.getType().equals(Material.GOLDEN_APPLE) && !hand.getType().equals(Material.DIAMOND_BLOCK)
                            && !hand.getType().equals(Material.TURTLE_EGG) && !hand.getType().equals(Material.ENDER_EYE) && !hand.getType().equals(Material.POTION)) {
                        return;
                    }
                }

                if (hand.getType().equals(Material.LILY_PAD)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 150) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.GOLD_NUGGET)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 200) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.APPLE)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 250) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.CHORUS_FRUIT)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 300) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.ROTTEN_FLESH)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 350) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.BROWN_MUSHROOM)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 400) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.RED_MUSHROOM)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 450) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.SLIME_BALL)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 500) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.CARROT)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 550) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.SUGAR_CANE)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 600) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.GOLDEN_APPLE)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 650) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.DIAMOND_BLOCK)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 700) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.TURTLE_EGG)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 750) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.ENDER_EYE)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 800) {
                        return;
                    }
                }

                final Location block = event.getClickedInventory().getLocation();
                final String location = block.getWorld().getName() + ":" + (block.getBlockX()) + ":" + (block.getBlockY()) + ":" + (block.getBlockZ());
                if (BrewingPlayer.get(location) == null) {
                    new BrewingPlayer(location, UUID, player).insert();
                } else {
                    BrewingPlayer.get(location).setUUID(UUID);
                    BrewingPlayer.get(location).setPlayer(player);
                }

                if (!hand.getType().equals(Material.POTION)) {
                    if (event.getSlot() == 3) {
                        player.getOpenInventory().getTopInventory().setItem(3, hand);
                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                player.getOpenInventory().getTopInventory().setItem(3, hand);
                            }
                        }, 1L);
                        BreweAPI.createInfo(location, hand.getType().name() + ":" + (hand.getAmount()));
                    }
                    Bukkit.getScheduler().scheduleSyncDelayedTask(Main.instance, new Runnable() {
                        @SuppressWarnings("deprecation")
                        @Override
                        public void run() {
                            if (event.getSlot() == 3) {
                                event.setCursor(new ItemStack(Material.AIR));
                            }
                        }
                    }, 1L);
                }

                int contains = 0;
                if (hand.getType().equals(Material.POTION)) {
                    if (player.getOpenInventory().getTopInventory().getItem(3) == null) {
                        return;
                    }
                    final PotionMeta meta = (PotionMeta) hand.getItemMeta();
                    if (meta.getCustomEffects().size() == 0) {
                        contains++;
                    }
                } else {
                    for (int i = 1; i <= 3; i++) {
                        if (event.getClickedInventory().getItem(i - 1) != null && event.getClickedInventory().getItem(i - 1).getType().equals(Material.POTION)) {
                            final PotionMeta meta = (PotionMeta) event.getClickedInventory().getItem(i - 1).getItemMeta();
                            if (meta.getCustomEffects().size() == 0) {
                                contains++;
                            }
                        }
                    }
                }
                if (contains == 0) {
                    return;
                }

                ItemStack item = null;
                final String[] bloco = BreweAPI.getBloco(location).split(":");
                final int quantia = Integer.valueOf(bloco[1]) - 1;
                if (bloco[0].equalsIgnoreCase("LILY_PAD")) {
                    item = new ItemStack(Material.LILY_PAD);
                    item.setAmount(quantia);
                }
                if (bloco[0].equalsIgnoreCase("GOLD_NUGGET")) {
                    item = new ItemStack(Material.GOLD_NUGGET);
                    item.setAmount(quantia);
                }
                if (bloco[0].equalsIgnoreCase("APPLE")) {
                    item = new ItemStack(Material.APPLE);
                    item.setAmount(quantia);
                }
                if (bloco[0].equalsIgnoreCase("CHORUS_FRUIT")) {
                    item = new ItemStack(Material.CHORUS_FRUIT);
                    item.setAmount(quantia);
                }
                if (bloco[0].equalsIgnoreCase("ROTTEN_FLESH")) {
                    item = new ItemStack(Material.ROTTEN_FLESH);
                    item.setAmount(quantia);
                }
                if (bloco[0].equalsIgnoreCase("BROWN_MUSHROOM")) {
                    item = new ItemStack(Material.BROWN_MUSHROOM);
                    item.setAmount(quantia);
                }
                if (bloco[0].equalsIgnoreCase("RED_MUSHROOM")) {
                    item = new ItemStack(Material.RED_MUSHROOM);
                    item.setAmount(quantia);
                }
                if (bloco[0].equalsIgnoreCase("SLIME_BALL")) {
                    item = new ItemStack(Material.SLIME_BALL);
                    item.setAmount(quantia);
                }
                if (bloco[0].equalsIgnoreCase("CARROT")) {
                    item = new ItemStack(Material.CARROT);
                    item.setAmount(quantia);
                }
                if (bloco[0].equalsIgnoreCase("SUGAR_CANE")) {
                    item = new ItemStack(Material.SUGAR_CANE);
                    item.setAmount(quantia);
                }
                if (bloco[0].equalsIgnoreCase("GOLDEN_APPLE")) {
                    item = new ItemStack(Material.GOLDEN_APPLE);
                    item.setAmount(quantia);
                }
                if (bloco[0].equalsIgnoreCase("DIAMOND_BLOCK")) {
                    item = new ItemStack(Material.DIAMOND_BLOCK);
                    item.setAmount(quantia);
                }
                if (bloco[0].equalsIgnoreCase("TURTLE_EGG")) {
                    item = new ItemStack(Material.TURTLE_EGG);
                    item.setAmount(quantia);
                }
                if (bloco[0].equalsIgnoreCase("ENDER_EYE")) {
                    item = new ItemStack(Material.TURTLE_EGG);
                    item.setAmount(quantia);
                }
                final String blockN = bloco[0] + ":" + (quantia);
                BreweAPI.updateInfo(location, blockN);

                if (item.getType().equals(Material.DIAMOND_BLOCK)) {
                    int containsPot = 0;
                    for (int i = 1; i <= 3; i++) {
                        final int slot = i - 1;
                        if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Força III (2:20)")) {
                            containsPot++;
                        }
                        if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Velocidade III (2:00)")) {
                            containsPot++;
                        }
                        if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Pressa (2:00)")) {
                            containsPot++;
                        }
                        if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Resistência (3:00)")) {
                            containsPot++;
                        }
                    }
                    if (containsPot == 0) {
                        return;
                    }
                }

                final ItemStack ingredient = player.getOpenInventory().getTopInventory().getItem(3).clone();
                final ItemStack finalItem = item;
                final int rotacao = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                    int time = 200;
                    ItemStack slot1;
                    ItemStack slot2;
                    ItemStack slot3;

                    @Override
                    public void run() {
                        if (this.time == 200) {
                            this.slot1 = event.getClickedInventory().getItem(0);
                            this.slot2 = event.getClickedInventory().getItem(1);
                            this.slot3 = event.getClickedInventory().getItem(2);
                        }
                        if (finalItem.getAmount() <= 0) {
                            this.time--;
                            stand.setBrewingTime(this.time);
                            stand.update(true);
                            stand.getInventory().setItem(3, new ItemStack(Material.AIR));
                            stand.getInventory().setItem(0, this.slot1);
                            stand.getInventory().setItem(1, this.slot2);
                            stand.getInventory().setItem(2, this.slot3);
                        } else {
                            this.time--;
                            stand.setBrewingTime(this.time);
                            stand.update(true);
                            stand.getInventory().setItem(3, finalItem);
                            stand.getInventory().setItem(0, this.slot1);
                            stand.getInventory().setItem(1, this.slot2);
                            stand.getInventory().setItem(2, this.slot3);
                        }

                        if (this.time == 0) {
                            final int xpAnterior = McMMOApi.getXP(UUID + "Alquimia");
                            int xpNew = 5;
                            if (BoosterHash.get(UUID) != null) {
                                if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                    xpNew += (xpNew * 25) / 100;
                                } else {
                                    xpNew += (xpNew * 50) / 100;
                                }

                                if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                    BoosterHash.CACHE.remove(UUID);
                                }
                            }
                            McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew);
                            AlquimiaEvent.verifyLevelUp(player, xpAnterior);

                            if (ingredient.getType().equals(Material.LILY_PAD)) {
                                final ItemStack potion = new ItemStack(Material.POTION);
                                potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                final ItemMeta meta = potion.getItemMeta();
                                meta.setDisplayName("Poção de passos aquático");
                                final List<String> lore = new ArrayList<>();
                                lore.add(ChatColor.BLUE + "Passos aquático (2:00)");
                                meta.setLore(lore);
                                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                potion.setItemMeta(meta);

                                for (int i = 1; i <= 3; i++) {
                                    if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                        final int slot = i - 1;
                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                inventory.setItem(slot, potion);
                                            }
                                        }, 1L);
                                    }
                                }
                            }
                            if (ingredient.getType().equals(Material.GOLD_NUGGET)) {
                                final ItemStack potion = new ItemStack(Material.POTION);
                                potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                final ItemMeta meta = potion.getItemMeta();
                                meta.setDisplayName("Poção de absorção");
                                final List<String> lore = new ArrayList<>();
                                lore.add(ChatColor.BLUE + "Absorção (1:50)");
                                meta.setLore(lore);
                                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                potion.setItemMeta(meta);

                                for (int i = 1; i <= 3; i++) {
                                    if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                        final int slot = i - 1;
                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                inventory.setItem(slot, potion);
                                            }
                                        }, 1L);
                                    }
                                }
                            }
                            if (ingredient.getType().equals(Material.APPLE)) {
                                final ItemStack potion = new ItemStack(Material.POTION);
                                potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                final ItemMeta meta = potion.getItemMeta();
                                meta.setDisplayName("Poção de vida bônus");
                                final List<String> lore = new ArrayList<>();
                                lore.add(ChatColor.BLUE + "Vida bônus (2:20)");
                                meta.setLore(lore);
                                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                potion.setItemMeta(meta);

                                for (int i = 1; i <= 3; i++) {
                                    if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                        final int slot = i - 1;
                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                inventory.setItem(slot, potion);
                                            }
                                        }, 1L);
                                    }
                                }
                            }
                            if (ingredient.getType().equals(Material.CHORUS_FRUIT)) {
                                final ItemStack potion = new ItemStack(Material.POTION);
                                potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                final ItemMeta meta = potion.getItemMeta();
                                meta.setDisplayName("Poção de saturação");
                                final List<String> lore = new ArrayList<>();
                                lore.add(ChatColor.BLUE + "Saturação (2:10)");
                                meta.setLore(lore);
                                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                potion.setItemMeta(meta);

                                for (int i = 1; i <= 3; i++) {
                                    if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                        final int slot = i - 1;
                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                inventory.setItem(slot, potion);
                                            }
                                        }, 1L);
                                    }
                                }
                            }
                            if (ingredient.getType().equals(Material.ROTTEN_FLESH)) {
                                final ItemStack potion = new ItemStack(Material.SPLASH_POTION);
                                potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                final ItemMeta meta = potion.getItemMeta();
                                meta.setDisplayName("Poção arremessável de fome");
                                final List<String> lore = new ArrayList<>();
                                lore.add(ChatColor.BLUE + "Fome (0:45)");
                                meta.setLore(lore);
                                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                potion.setItemMeta(meta);

                                for (int i = 1; i <= 3; i++) {
                                    if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                        final int slot = i - 1;
                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                inventory.setItem(slot, potion);
                                            }
                                        }, 1L);
                                    }
                                }
                            }
                            if (ingredient.getType().equals(Material.BROWN_MUSHROOM)) {
                                final ItemStack potion = new ItemStack(Material.SPLASH_POTION);
                                potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                final ItemMeta meta = potion.getItemMeta();
                                meta.setDisplayName("Poção arremessável de náusea");
                                final List<String> lore = new ArrayList<>();
                                lore.add(ChatColor.BLUE + "Náusea (0:30)");
                                meta.setLore(lore);
                                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                potion.setItemMeta(meta);

                                for (int i = 1; i <= 3; i++) {
                                    if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                        final int slot = i - 1;
                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                inventory.setItem(slot, potion);
                                            }
                                        }, 1L);
                                    }
                                }
                            }
                            if (ingredient.getType().equals(Material.RED_MUSHROOM)) {
                                final ItemStack potion = new ItemStack(Material.SPLASH_POTION);
                                potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                final ItemMeta meta = potion.getItemMeta();
                                meta.setDisplayName("Poção arremessável de cegueira");
                                final List<String> lore = new ArrayList<>();
                                lore.add(ChatColor.BLUE + "Cegueira (0:30)");
                                meta.setLore(lore);
                                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                potion.setItemMeta(meta);

                                for (int i = 1; i <= 3; i++) {
                                    if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                        final int slot = i - 1;
                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                inventory.setItem(slot, potion);
                                            }
                                        }, 1L);
                                    }
                                }
                            }
                            if (ingredient.getType().equals(Material.SLIME_BALL)) {
                                final ItemStack potion = new ItemStack(Material.SPLASH_POTION);
                                potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                final ItemMeta meta = potion.getItemMeta();
                                meta.setDisplayName("Poção arremessável de fadiga");
                                final List<String> lore = new ArrayList<>();
                                lore.add(ChatColor.BLUE + "Fadiga (0:35)");
                                meta.setLore(lore);
                                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                potion.setItemMeta(meta);

                                for (int i = 1; i <= 3; i++) {
                                    if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                        final int slot = i - 1;
                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                inventory.setItem(slot, potion);
                                            }
                                        }, 1L);
                                    }
                                }
                            }
                            if (ingredient.getType().equals(Material.CARROT)) {
                                final ItemStack potion = new ItemStack(Material.POTION);
                                potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                final ItemMeta meta = potion.getItemMeta();
                                meta.setDisplayName("Poção de pressa");
                                final List<String> lore = new ArrayList<>();
                                lore.add(ChatColor.BLUE + "Pressa (2:00)");
                                meta.setLore(lore);
                                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                potion.setItemMeta(meta);

                                for (int i = 1; i <= 3; i++) {
                                    if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                        final int slot = i - 1;
                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                inventory.setItem(slot, potion);
                                            }
                                        }, 1L);
                                    }
                                }
                            }
                            if (ingredient.getType().equals(Material.SUGAR_CANE)) {
                                final ItemStack potion = new ItemStack(Material.POTION);
                                potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                final ItemMeta meta = potion.getItemMeta();
                                meta.setDisplayName("Poção de velocidade");
                                final List<String> lore = new ArrayList<>();
                                lore.add(ChatColor.BLUE + "Velocidade III (2:00)");
                                meta.setLore(lore);
                                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                potion.setItemMeta(meta);

                                for (int i = 1; i <= 3; i++) {
                                    if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                        final int slot = i - 1;
                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                inventory.setItem(slot, potion);
                                            }
                                        }, 1L);
                                    }
                                }
                            }
                            if (ingredient.getType().equals(Material.GOLDEN_APPLE)) {
                                final ItemStack potion = new ItemStack(Material.POTION);
                                potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                final ItemMeta meta = potion.getItemMeta();
                                meta.setDisplayName("Poção de resistência");
                                final List<String> lore = new ArrayList<>();
                                lore.add(ChatColor.BLUE + "Resistência (3:00)");
                                meta.setLore(lore);
                                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                potion.setItemMeta(meta);

                                for (int i = 1; i <= 3; i++) {
                                    if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                        final int slot = i - 1;
                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                inventory.setItem(slot, potion);
                                            }
                                        }, 1L);
                                    }
                                }
                            }
                            if (ingredient.getType().equals(Material.TURTLE_EGG)) {
                                final ItemStack potion = new ItemStack(Material.POTION);
                                potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                final ItemMeta meta = potion.getItemMeta();
                                meta.setDisplayName("Poção de força");
                                final List<String> lore = new ArrayList<>();
                                lore.add(ChatColor.BLUE + "Força III (2:20)");
                                meta.setLore(lore);
                                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                potion.setItemMeta(meta);

                                for (int i = 1; i <= 3; i++) {
                                    if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                        final int slot = i - 1;
                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                inventory.setItem(slot, potion);
                                            }
                                        }, 1L);
                                    }
                                }
                            }
                            if (ingredient.getType().equals(Material.ENDER_EYE)) {
                                final ItemStack potion = new ItemStack(Material.POTION);
                                potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                final ItemMeta meta = potion.getItemMeta();
                                meta.setDisplayName("Poção de especial");
                                final List<String> lore = new ArrayList<>();
                                lore.add(ChatColor.BLUE + "Força II, Velocidade II, Pressa II, Resistência II (6:00)");
                                meta.setLore(lore);
                                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                potion.setItemMeta(meta);

                                for (int i = 1; i <= 3; i++) {
                                    if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                        final int slot = i - 1;
                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                inventory.setItem(slot, potion);
                                            }
                                        }, 1L);
                                    }
                                }
                            }
                            if (ingredient.getType().equals(Material.DIAMOND_BLOCK)) {
                                for (int i = 1; i <= 3; i++) {
                                    final int slot = i - 1;
                                    if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Força III (2:20)")) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de força");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Força III (5:00)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        final int xpAnteriorr = McMMOApi.getXP(UUID + "Alquimia");
                                        int xpNew2 = 8;
                                        if (BoosterHash.get(UUID) != null) {
                                            if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                xpNew2 += (xpNew2 * 25) / 100;
                                            } else {
                                                xpNew2 += (xpNew2 * 50) / 100;
                                            }

                                            if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                BoosterHash.CACHE.remove(UUID);
                                            }
                                        }
                                        McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew2);
                                        AlquimiaEvent.verifyLevelUp(player, xpAnteriorr);

                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                inventory.setItem(slot, potion);
                                            }
                                        }, 1L);
                                    }
                                    if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Velocidade III (2:00)")) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de velocidade");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Velocidade III (5:00)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        final int xpAnteriorr = McMMOApi.getXP(UUID + "Alquimia");
                                        int xpNew2 = 8;
                                        if (BoosterHash.get(UUID) != null) {
                                            if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                xpNew2 += (xpNew2 * 25) / 100;
                                            } else {
                                                xpNew2 += (xpNew2 * 50) / 100;
                                            }

                                            if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                BoosterHash.CACHE.remove(UUID);
                                            }
                                        }
                                        McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew2);
                                        AlquimiaEvent.verifyLevelUp(player, xpAnteriorr);

                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                inventory.setItem(slot, potion);
                                            }
                                        }, 1L);
                                    }
                                    if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Pressa (2:00)")) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de pressa");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Pressa (5:00)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        final int xpAnteriorr = McMMOApi.getXP(UUID + "Alquimia");
                                        int xpNew2 = 8;
                                        if (BoosterHash.get(UUID) != null) {
                                            if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                xpNew2 += (xpNew2 * 25) / 100;
                                            } else {
                                                xpNew2 += (xpNew2 * 50) / 100;
                                            }

                                            if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                BoosterHash.CACHE.remove(UUID);
                                            }
                                        }
                                        McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew2);
                                        AlquimiaEvent.verifyLevelUp(player, xpAnteriorr);

                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                inventory.setItem(slot, potion);
                                            }
                                        }, 1L);
                                    }
                                    if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Resistência (3:00)")) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de resistência");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Resistência (5:00)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        final int xpAnteriorr = McMMOApi.getXP(UUID + "Alquimia");
                                        int xpNew2 = 8;
                                        if (BoosterHash.get(UUID) != null) {
                                            if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                xpNew2 += (xpNew2 * 25) / 100;
                                            } else {
                                                xpNew2 += (xpNew2 * 50) / 100;
                                            }

                                            if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                BoosterHash.CACHE.remove(UUID);
                                            }
                                        }
                                        McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew2);
                                        AlquimiaEvent.verifyLevelUp(player, xpAnteriorr);

                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                inventory.setItem(slot, potion);
                                            }
                                        }, 1L);
                                    }
                                }
                            }
                        }
                    }
                }, 2L, 1L);

                if (RotacaoAlq.get(location) == null) {
                    new RotacaoAlq(location, rotacao).insert();
                } else {
                    RotacaoAlq.get(location).setId(rotacao);
                }

                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        Bukkit.getScheduler().cancelTask(rotacao);
                    }
                }, 201L);
            }
        } else if (event.getClick() == ClickType.RIGHT) {
            if (event.getClickedInventory() != null && event.getClickedInventory().getType().equals(InventoryType.BREWING)) {
                if (event.getCursor() == null || event.getCursor().getType().equals(Material.AIR)) {
                    if (event.getSlot() == 0 || event.getSlot() == 1 || event.getSlot() == 2) {
                        if (event.getCurrentItem().getType().equals(Material.POTION) || event.getCurrentItem().getType().equals(Material.SPLASH_POTION)) {
                            final Inventory inventory = player.getOpenInventory().getTopInventory();

                            if (event.getSlot() == 0) {
                                final ItemStack item = inventory.getItem(0).clone();
                                event.setCancelled(true);
                                event.setCursor(item);
                                player.getOpenInventory().getTopInventory().setItem(event.getSlot(), new ItemStack(Material.AIR));
                            }
                            if (event.getSlot() == 1) {
                                final ItemStack item = inventory.getItem(0).clone();
                                event.setCancelled(true);
                                event.setCursor(item);
                                player.getOpenInventory().getTopInventory().setItem(event.getSlot(), new ItemStack(Material.AIR));
                            }
                            if (event.getSlot() == 2) {
                                final ItemStack item = inventory.getItem(0).clone();
                                event.setCancelled(true);
                                event.setCursor(item);
                                player.getOpenInventory().getTopInventory().setItem(event.getSlot(), new ItemStack(Material.AIR));
                            }

                            final Location block = event.getClickedInventory().getLocation();
                            final String location = block.getWorld().getName() + ":" + (block.getBlockX()) + ":" + (block.getBlockY()) + ":" + (block.getBlockZ());
                            if (RotacaoAlq.get(location) != null && RotacaoAlq.get(location).getId() != 0) {
                                Bukkit.getScheduler().cancelTask(RotacaoAlq.get(location).getId());
                                RotacaoAlq.get(location).setId(0);
                            }
                        }
                    }
                    if (event.getSlot() == 3) {
                        if (event.getClickedInventory().getItem(3) != null && event.getClickedInventory().getItem(3).getAmount() == 1) {
                            if (event.getCurrentItem().getType().equals(Material.LILY_PAD) || event.getCurrentItem().getType().equals(Material.GOLD_NUGGET) || event.getCurrentItem().getType().equals(Material.APPLE) || event.getCurrentItem().getType().equals(Material.SUGAR_CANE) || event.getCurrentItem().getType().equals(Material.ROTTEN_FLESH) || event.getCurrentItem().getType().equals(Material.BROWN_MUSHROOM)
                                    || event.getCurrentItem().getType().equals(Material.RED_MUSHROOM) || event.getCurrentItem().getType().equals(Material.CARROT) || event.getCurrentItem().getType().equals(Material.SLIME_BALL) || event.getCurrentItem().equals(Material.CHORUS_FRUIT) || event.getCurrentItem().equals(Material.GOLDEN_APPLE) || event.getCurrentItem().getType().equals(Material.DIAMOND_BLOCK)
                                    || event.getCurrentItem().getType().equals(Material.TURTLE_EGG) || event.getCurrentItem().getType().equals(Material.ENDER_EYE)) {
                                final Location block = event.getClickedInventory().getLocation();
                                final String location = block.getWorld().getName() + ":" + (block.getBlockX()) + ":" + (block.getBlockY()) + ":" + (block.getBlockZ());
                                BreweAPI.removeInfo(location);

                                if (RotacaoAlq.get(location) != null && RotacaoAlq.get(location).getId() != 0) {
                                    Bukkit.getScheduler().cancelTask(RotacaoAlq.get(location).getId());
                                    RotacaoAlq.get(location).setId(0);
                                }
                            } else if (event.getClickedInventory().getItem(3) != null && event.getClickedInventory().getItem(3).getAmount() > 1) {
                                final ItemStack item = event.getClickedInventory().getItem(3).clone();
                                final int quantiaAn = item.getAmount();
                                final int quantiaDp = item.getAmount() / 2;
                                item.setAmount(quantiaDp - 1);
                                event.getClickedInventory().setItem(3, new ItemStack(Material.AIR));
                                event.getClickedInventory().setItem(3, item);
                                event.setCursor(new ItemStack(item.getType(), quantiaAn - quantiaDp + 1));

                                final Location block = event.getClickedInventory().getLocation();
                                final String location = block.getWorld().getName() + ":" + (block.getBlockX()) + ":" + (block.getBlockY()) + ":" + (block.getBlockZ());
                                final String[] split = BreweAPI.getBloco(location).split(":");
                                BreweAPI.updateInfo(location, split[0] + ":" + (quantiaDp));
                            }
                        }
                    }
                    return;
                }

                final Inventory inventory = player.getOpenInventory().getTopInventory();
                BrewingStand stand = (BrewingStand) event.getClickedInventory().getHolder();

                ItemStack hand = event.getCursor().clone();
                if (hand == null || hand.getType() == Material.AIR) {
                    return;
                } else {
                    if (!hand.getType().equals(Material.LILY_PAD) && !hand.getType().equals(Material.GOLD_NUGGET) && !hand.getType().equals(Material.APPLE) && !hand.getType().equals(Material.SUGAR_CANE) && !hand.getType().equals(Material.ROTTEN_FLESH) && !hand.getType().equals(Material.BROWN_MUSHROOM)
                            && !hand.getType().equals(Material.RED_MUSHROOM) && !hand.getType().equals(Material.CARROT) && !hand.getType().equals(Material.SLIME_BALL) && !hand.getType().equals(Material.CHORUS_FRUIT) && !hand.getType().equals(Material.GOLDEN_APPLE) && !hand.getType().equals(Material.DIAMOND_BLOCK)
                            && !hand.getType().equals(Material.TURTLE_EGG) && !hand.getType().equals(Material.ENDER_EYE) && !hand.getType().equals(Material.POTION)) {
                        return;
                    }
                }

                if (hand.getType().equals(Material.LILY_PAD)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 150) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.GOLD_NUGGET)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 200) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.APPLE)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 250) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.CHORUS_FRUIT)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 300) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.ROTTEN_FLESH)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 350) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.BROWN_MUSHROOM)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 400) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.RED_MUSHROOM)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 450) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.SLIME_BALL)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 500) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.CARROT)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 550) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.SUGAR_CANE)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 600) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.GOLDEN_APPLE)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 650) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.DIAMOND_BLOCK)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 700) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.TURTLE_EGG)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 750) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.ENDER_EYE)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 800) {
                        return;
                    }
                }

                final Location block = event.getClickedInventory().getLocation();
                final String location = block.getWorld().getName() + ":" + (block.getBlockX()) + ":" + (block.getBlockY()) + ":" + (block.getBlockZ());
                if (BrewingPlayer.get(location) == null) {
                    new BrewingPlayer(location, UUID, player).insert();
                } else {
                    BrewingPlayer.get(location).setUUID(UUID);
                    BrewingPlayer.get(location).setPlayer(player);
                }

                if (!hand.getType().equals(Material.POTION)) {
                    if (event.getSlot() == 3) {
                        //SE NÃO EXISTIR INFO NO MYSQL E ESTIVER ROTACIONANDO
                        if (BreweAPI.getExist(location) == null && RotacaoAlq.get(location) != null && RotacaoAlq.get(location).getId() != 0) {
                            final ItemStack item = hand.clone();
                            item.setAmount(1);
                            player.getOpenInventory().getTopInventory().setItem(3, item);
                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    player.getOpenInventory().getTopInventory().setItem(3, item);
                                }
                            }, 1L);
                            BreweAPI.createInfo(location, hand.getType().name() + ":" + (1));

                            if (RotacaoAlq.get(location) != null && RotacaoAlq.get(location).getId() != 0) {
                                if (HasAddItemBrew.get(UUID) == null) {
                                    new HasAddItemBrew(UUID, true).insert();
                                } else {
                                    HasAddItemBrew.get(UUID).setHasAdd(true);
                                }
                            }
                            //NÃO EXISTE NO MYSQL, TEM A ROTAÇÃO, MAS NAO ESTA ROTACIONANDO
                        } else if (BreweAPI.getExist(location) == null && RotacaoAlq.get(location) != null && RotacaoAlq.get(location).getId() == 0) {
                            final ItemStack item = hand.clone();
                            item.setAmount(1);
                            player.getOpenInventory().getTopInventory().setItem(3, item);
                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    player.getOpenInventory().getTopInventory().setItem(3, item);
                                }
                            }, 1L);
                            BreweAPI.createInfo(location, hand.getType().name() + ":" + (1));
                            //SE NÃO EXISTIR INFO NO MYSQL E NÃO ESTIVER ROTACIONANDO
                        } else if (BreweAPI.getExist(location) == null && RotacaoAlq.get(location) == null) {
                            final ItemStack item = hand.clone();
                            item.setAmount(1);
                            player.getOpenInventory().getTopInventory().setItem(3, item);
                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    player.getOpenInventory().getTopInventory().setItem(3, item);
                                }
                            }, 1L);
                            BreweAPI.createInfo(location, hand.getType().name() + ":" + (1));
                            //SE TIVER UMA INFORMAÇÃO E ESTIVER ROTACIONANDO
                        } else if (BreweAPI.getExist(location) != null && RotacaoAlq.get(location) != null && RotacaoAlq.get(location).getId() != 0) {
                            final String[] split = BreweAPI.getBloco(location).split(":");
                            final int quantia = Integer.valueOf(split[1]);
                            final ItemStack item = new ItemStack(Material.getMaterial(split[0]), quantia + 1);
                            player.getOpenInventory().getTopInventory().setItem(3, item);
                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    player.getOpenInventory().getTopInventory().setItem(3, item);
                                }
                            }, 1L);
                            BreweAPI.updateInfo(location, split[0] + ":" + (quantia + 1));

                            if (RotacaoAlq.get(location) != null && RotacaoAlq.get(location).getId() != 0) {
                                if (HasAddItemBrew.get(UUID) == null) {
                                    new HasAddItemBrew(UUID, true).insert();
                                } else {
                                    HasAddItemBrew.get(UUID).setHasAdd(true);
                                }
                            }
                            //SE TIVER UMA INFORMAÇÃO NO MYSQL E TIVER A ROTAÇÃO, MAS NÃO ESTIVER ACONTECENDO
                        } else if (BreweAPI.getExist(location) != null && RotacaoAlq.get(location) != null && RotacaoAlq.get(location).getId() == 0) {
                            final String[] split = BreweAPI.getBloco(location).split(":");
                            final int quantia = Integer.valueOf(split[1]);
                            final ItemStack item = new ItemStack(Material.getMaterial(split[0]), quantia + 1);
                            player.getOpenInventory().getTopInventory().setItem(3, item);
                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    player.getOpenInventory().getTopInventory().setItem(3, item);
                                }
                            }, 1L);
                            BreweAPI.updateInfo(location, split[0] + ":" + (quantia + 1));
                            //SE TIVER UMA INFORMAÇÃO NO MYSQL E NÃO ESTIVER ROTACIONANDO
                        } else if (BreweAPI.getExist(location) != null && RotacaoAlq.get(location) == null) {
                            final String[] split = BreweAPI.getBloco(location).split(":");
                            final int quantia = Integer.valueOf(split[1]);
                            final ItemStack item = new ItemStack(Material.getMaterial(split[0]), quantia + 1);
                            player.getOpenInventory().getTopInventory().setItem(3, item);
                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    player.getOpenInventory().getTopInventory().setItem(3, item);
                                }
                            }, 1L);
                            BreweAPI.updateInfo(location, split[0] + ":" + (quantia + 1));
                        }
                    }
                    Bukkit.getScheduler().scheduleSyncDelayedTask(Main.instance, new Runnable() {
                        @SuppressWarnings("deprecation")
                        @Override
                        public void run() {
                            if (event.getSlot() == 3) {
                                if (event.getCursor().getAmount() == 1) {
                                    event.setCursor(new ItemStack(Material.AIR));
                                } else {
                                    final ItemStack item = new ItemStack(hand.getType(), hand.clone().getAmount() - 1);
                                    event.setCursor(item);
                                }
                            }
                        }
                    }, 1L);
                }

                int contains = 0;
                if (hand.getType().equals(Material.POTION)) {
                    if (player.getOpenInventory().getTopInventory().getItem(3) == null) {
                        return;
                    }
                    final PotionMeta meta = (PotionMeta) hand.getItemMeta();
                    if (meta.getCustomEffects().size() == 0) {
                        contains++;
                    }
                }
                for (int i = 1; i <= 3; i++) {
                    if (event.getClickedInventory().getItem(i - 1) != null && event.getClickedInventory().getItem(i - 1).getType().equals(Material.POTION)) {
                        final PotionMeta meta = (PotionMeta) event.getClickedInventory().getItem(i - 1).getItemMeta();
                        if (meta.getCustomEffects().size() == 0) {
                            contains++;
                        }
                    }
                }
                if (contains == 0) {
                    return;
                }

                if (stand.getBrewingTime() == 0) {
                    final String[] bloco = BreweAPI.getBloco(location).split(":");
                    final int quantia = Integer.valueOf(bloco[1]) - 1;
                    final ItemStack item = new ItemStack(Material.getMaterial(bloco[0]), quantia);
                    final String blockN = bloco[0] + ":" + (quantia);
                    if (quantia == 0) {
                        BreweAPI.removeInfo(location);
                    } else {
                        BreweAPI.updateInfo(location, blockN);
                    }

                    if (item.getType().equals(Material.DIAMOND_BLOCK)) {
                        int containsPot = 0;
                        for (int i = 1; i <= 3; i++) {
                            final int slot = i - 1;
                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Força III (2:20)")) {
                                containsPot++;
                            }
                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Velocidade III (2:00)")) {
                                containsPot++;
                            }
                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Pressa (2:00)")) {
                                containsPot++;
                            }
                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Resistência (3:00)")) {
                                containsPot++;
                            }
                        }
                        if (containsPot == 0) {
                            return;
                        }
                    }

                    final ItemStack ingredient = player.getOpenInventory().getTopInventory().getItem(3).clone();
                    final ItemStack finalItem = item;
                    final int rotacao = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                        int time = 200;
                        ItemStack slot1;
                        ItemStack slot2;
                        ItemStack slot3;

                        @Override
                        public void run() {
                            if (this.time == 200) {
                                this.slot1 = event.getClickedInventory().getItem(0);
                                this.slot2 = event.getClickedInventory().getItem(1);
                                this.slot3 = event.getClickedInventory().getItem(2);
                            }
                            if (HasAddItemBrew.get(UUID) != null && HasAddItemBrew.get(UUID).getHasAdd() == true) {
                                final String[] bloco1 = BreweAPI.getBloco(location).split(":");
                                final int quantia1 = Integer.valueOf(bloco1[1]);
                                final ItemStack item1 = new ItemStack(Material.getMaterial(bloco1[0]), quantia1);

                                if (item1.getAmount() <= 0) {
                                    this.time--;
                                    stand.setBrewingTime(this.time);
                                    stand.update(true);
                                    stand.getInventory().setItem(3, new ItemStack(Material.AIR));
                                    stand.getInventory().setItem(0, this.slot1);
                                    stand.getInventory().setItem(1, this.slot2);
                                    stand.getInventory().setItem(2, this.slot3);
                                } else {
                                    this.time--;
                                    stand.setBrewingTime(this.time);
                                    stand.update(true);
                                    stand.getInventory().setItem(3, item1);
                                    stand.getInventory().setItem(0, this.slot1);
                                    stand.getInventory().setItem(1, this.slot2);
                                    stand.getInventory().setItem(2, this.slot3);
                                }

                                if (this.time == 0) {
                                    final int xpAnterior = McMMOApi.getXP(UUID + "Alquimia");
                                    int xpNew3 = 5;
                                    if (BoosterHash.get(UUID) != null) {
                                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                            xpNew3 += (xpNew3 * 25) / 100;
                                        } else {
                                            xpNew3 += (xpNew3 * 50) / 100;
                                        }

                                        if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                            BoosterHash.CACHE.remove(UUID);
                                        }
                                    }
                                    McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew3);
                                    AlquimiaEvent.verifyLevelUp(player, xpAnterior);
                                    HasAddItemBrew.get(UUID).setHasAdd(false);

                                    if (ingredient.getType().equals(Material.LILY_PAD)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de passos aquático");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Passos aquático (2:00)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.GOLD_NUGGET)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de absorção");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Absorção (1:50)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.APPLE)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de vida bônus");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Vida bônus (2:20)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.CHORUS_FRUIT)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de saturação");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Saturação (2:10)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.ROTTEN_FLESH)) {
                                        final ItemStack potion = new ItemStack(Material.SPLASH_POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção arremessável de fome");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Fome (0:45)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.BROWN_MUSHROOM)) {
                                        final ItemStack potion = new ItemStack(Material.SPLASH_POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção arremessável de náusea");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Náusea (0:30)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.RED_MUSHROOM)) {
                                        final ItemStack potion = new ItemStack(Material.SPLASH_POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção arremessável de cegueira");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Cegueira (0:30)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.SLIME_BALL)) {
                                        final ItemStack potion = new ItemStack(Material.SPLASH_POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção arremessável de fadiga");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Fadiga (0:35)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.CARROT)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de pressa");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Pressa (2:00)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.SUGAR_CANE)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de velocidade");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Velocidade III (2:00)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.GOLDEN_APPLE)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de resistência");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Resistência (3:00)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.TURTLE_EGG)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de força");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Força III (2:20)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.ENDER_EYE)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de especial");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Força II, Velocidade II, Pressa II, Resistência II (6:00)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.DIAMOND_BLOCK)) {
                                        for (int i = 1; i <= 3; i++) {
                                            final int slot = i - 1;
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Força III (2:20)")) {
                                                final ItemStack potion = new ItemStack(Material.POTION);
                                                potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                                final ItemMeta meta = potion.getItemMeta();
                                                meta.setDisplayName("Poção de força");
                                                final List<String> lore = new ArrayList<>();
                                                lore.add(ChatColor.BLUE + "Força III (5:00)");
                                                meta.setLore(lore);
                                                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                                potion.setItemMeta(meta);

                                                final int xpAnteriorr = McMMOApi.getXP(UUID + "Alquimia");
                                                int xpNew2 = 8;
                                                if (BoosterHash.get(UUID) != null) {
                                                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                        xpNew2 += (xpNew2 * 25) / 100;
                                                    } else {
                                                        xpNew2 += (xpNew2 * 50) / 100;
                                                    }

                                                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                        BoosterHash.CACHE.remove(UUID);
                                                    }
                                                }
                                                McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew2);
                                                AlquimiaEvent.verifyLevelUp(player, xpAnteriorr);

                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Velocidade III (2:00)")) {
                                                final ItemStack potion = new ItemStack(Material.POTION);
                                                potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                                final ItemMeta meta = potion.getItemMeta();
                                                meta.setDisplayName("Poção de velocidade");
                                                final List<String> lore = new ArrayList<>();
                                                lore.add(ChatColor.BLUE + "Velocidade III (5:00)");
                                                meta.setLore(lore);
                                                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                                potion.setItemMeta(meta);

                                                final int xpAnteriorr = McMMOApi.getXP(UUID + "Alquimia");
                                                int xpNew2 = 8;
                                                if (BoosterHash.get(UUID) != null) {
                                                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                        xpNew2 += (xpNew2 * 25) / 100;
                                                    } else {
                                                        xpNew2 += (xpNew2 * 50) / 100;
                                                    }

                                                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                        BoosterHash.CACHE.remove(UUID);
                                                    }
                                                }
                                                McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew2);
                                                AlquimiaEvent.verifyLevelUp(player, xpAnteriorr);

                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Pressa (2:00)")) {
                                                final ItemStack potion = new ItemStack(Material.POTION);
                                                potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                                final ItemMeta meta = potion.getItemMeta();
                                                meta.setDisplayName("Poção de pressa");
                                                final List<String> lore = new ArrayList<>();
                                                lore.add(ChatColor.BLUE + "Pressa (5:00)");
                                                meta.setLore(lore);
                                                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                                potion.setItemMeta(meta);

                                                final int xpAnteriorr = McMMOApi.getXP(UUID + "Alquimia");
                                                int xpNew2 = 8;
                                                if (BoosterHash.get(UUID) != null) {
                                                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                        xpNew2 += (xpNew2 * 25) / 100;
                                                    } else {
                                                        xpNew2 += (xpNew2 * 50) / 100;
                                                    }

                                                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                        BoosterHash.CACHE.remove(UUID);
                                                    }
                                                }
                                                McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew2);
                                                AlquimiaEvent.verifyLevelUp(player, xpAnteriorr);

                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Resistência (3:00)")) {
                                                final ItemStack potion = new ItemStack(Material.POTION);
                                                potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                                final ItemMeta meta = potion.getItemMeta();
                                                meta.setDisplayName("Poção de resistência");
                                                final List<String> lore = new ArrayList<>();
                                                lore.add(ChatColor.BLUE + "Resistência (5:00)");
                                                meta.setLore(lore);
                                                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                                potion.setItemMeta(meta);

                                                final int xpAnteriorr = McMMOApi.getXP(UUID + "Alquimia");
                                                int xpNew2 = 8;
                                                if (BoosterHash.get(UUID) != null) {
                                                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                        xpNew2 += (xpNew2 * 25) / 100;
                                                    } else {
                                                        xpNew2 += (xpNew2 * 50) / 100;
                                                    }

                                                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                        BoosterHash.CACHE.remove(UUID);
                                                    }
                                                }
                                                McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew2);
                                                AlquimiaEvent.verifyLevelUp(player, xpAnteriorr);

                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (finalItem.getAmount() <= 0) {
                                    this.time--;
                                    stand.setBrewingTime(this.time);
                                    stand.update(true);
                                    stand.getInventory().setItem(3, new ItemStack(Material.AIR));
                                    stand.getInventory().setItem(0, this.slot1);
                                    stand.getInventory().setItem(1, this.slot2);
                                    stand.getInventory().setItem(2, this.slot3);
                                } else {
                                    this.time--;
                                    stand.setBrewingTime(this.time);
                                    stand.update(true);
                                    stand.getInventory().setItem(3, finalItem);
                                    stand.getInventory().setItem(0, this.slot1);
                                    stand.getInventory().setItem(1, this.slot2);
                                    stand.getInventory().setItem(2, this.slot3);
                                }

                                if (this.time == 0) {
                                    final int xpAnterior = McMMOApi.getXP(UUID + "Alquimia");
                                    int xpNew3 = 5;
                                    if (BoosterHash.get(UUID) != null) {
                                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                            xpNew3 += (xpNew3 * 25) / 100;
                                        } else {
                                            xpNew3 += (xpNew3 * 50) / 100;
                                        }

                                        if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                            BoosterHash.CACHE.remove(UUID);
                                        }
                                    }
                                    McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew3);
                                    AlquimiaEvent.verifyLevelUp(player, xpAnterior);

                                    if (ingredient.getType().equals(Material.LILY_PAD)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de passos aquático");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Passos aquático (2:00)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.GOLD_NUGGET)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de absorção");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Absorção (1:50)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.APPLE)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de vida bônus");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Vida bônus (2:20)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.CHORUS_FRUIT)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de saturação");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Saturação (2:10)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.ROTTEN_FLESH)) {
                                        final ItemStack potion = new ItemStack(Material.SPLASH_POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção arremessável de fome");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Fome (0:45)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.BROWN_MUSHROOM)) {
                                        final ItemStack potion = new ItemStack(Material.SPLASH_POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção arremessável de náusea");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Náusea (0:30)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.RED_MUSHROOM)) {
                                        final ItemStack potion = new ItemStack(Material.SPLASH_POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção arremessável de cegueira");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Cegueira (0:30)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.SLIME_BALL)) {
                                        final ItemStack potion = new ItemStack(Material.SPLASH_POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção arremessável de fadiga");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Fadiga (0:35)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.CARROT)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de pressa");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Pressa (2:00)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.SUGAR_CANE)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de velocidade");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Velocidade III (2:00)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.GOLDEN_APPLE)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de resistência");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Resistência (3:00)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.TURTLE_EGG)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de força");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Força III (2:20)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.ENDER_EYE)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de especial");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Força II, Velocidade II, Pressa II, Resistência II (6:00)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.DIAMOND_BLOCK)) {
                                        for (int i = 1; i <= 3; i++) {
                                            final int slot = i - 1;
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Força III (2:20)")) {
                                                final ItemStack potion = new ItemStack(Material.POTION);
                                                potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                                final ItemMeta meta = potion.getItemMeta();
                                                meta.setDisplayName("Poção de força");
                                                final List<String> lore = new ArrayList<>();
                                                lore.add(ChatColor.BLUE + "Força III (5:00)");
                                                meta.setLore(lore);
                                                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                                potion.setItemMeta(meta);

                                                final int xpAnteriorr = McMMOApi.getXP(UUID + "Alquimia");
                                                int xpNew2 = 8;
                                                if (BoosterHash.get(UUID) != null) {
                                                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                        xpNew2 += (xpNew2 * 25) / 100;
                                                    } else {
                                                        xpNew2 += (xpNew2 * 50) / 100;
                                                    }

                                                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                        BoosterHash.CACHE.remove(UUID);
                                                    }
                                                }
                                                McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew2);
                                                AlquimiaEvent.verifyLevelUp(player, xpAnteriorr);

                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Velocidade III (2:00)")) {
                                                final ItemStack potion = new ItemStack(Material.POTION);
                                                potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                                final ItemMeta meta = potion.getItemMeta();
                                                meta.setDisplayName("Poção de velocidade");
                                                final List<String> lore = new ArrayList<>();
                                                lore.add(ChatColor.BLUE + "Velocidade III (5:00)");
                                                meta.setLore(lore);
                                                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                                potion.setItemMeta(meta);

                                                final int xpAnteriorr = McMMOApi.getXP(UUID + "Alquimia");
                                                int xpNew2 = 8;
                                                if (BoosterHash.get(UUID) != null) {
                                                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                        xpNew2 += (xpNew2 * 25) / 100;
                                                    } else {
                                                        xpNew2 += (xpNew2 * 50) / 100;
                                                    }

                                                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                        BoosterHash.CACHE.remove(UUID);
                                                    }
                                                }
                                                McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew2);
                                                AlquimiaEvent.verifyLevelUp(player, xpAnteriorr);

                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Pressa (2:00)")) {
                                                final ItemStack potion = new ItemStack(Material.POTION);
                                                potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                                final ItemMeta meta = potion.getItemMeta();
                                                meta.setDisplayName("Poção de pressa");
                                                final List<String> lore = new ArrayList<>();
                                                lore.add(ChatColor.BLUE + "Pressa (5:00)");
                                                meta.setLore(lore);
                                                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                                potion.setItemMeta(meta);

                                                final int xpAnteriorr = McMMOApi.getXP(UUID + "Alquimia");
                                                int xpNew2 = 8;
                                                if (BoosterHash.get(UUID) != null) {
                                                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                        xpNew2 += (xpNew2 * 25) / 100;
                                                    } else {
                                                        xpNew2 += (xpNew2 * 50) / 100;
                                                    }

                                                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                        BoosterHash.CACHE.remove(UUID);
                                                    }
                                                }
                                                McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew2);
                                                AlquimiaEvent.verifyLevelUp(player, xpAnteriorr);

                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Resistência (3:00)")) {
                                                final ItemStack potion = new ItemStack(Material.POTION);
                                                potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                                final ItemMeta meta = potion.getItemMeta();
                                                meta.setDisplayName("Poção de resistência");
                                                final List<String> lore = new ArrayList<>();
                                                lore.add(ChatColor.BLUE + "Resistência (5:00)");
                                                meta.setLore(lore);
                                                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                                potion.setItemMeta(meta);

                                                final int xpAnteriorr = McMMOApi.getXP(UUID + "Alquimia");
                                                int xpNew2 = 8;
                                                if (BoosterHash.get(UUID) != null) {
                                                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                        xpNew2 += (xpNew2 * 25) / 100;
                                                    } else {
                                                        xpNew2 += (xpNew2 * 50) / 100;
                                                    }

                                                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                        BoosterHash.CACHE.remove(UUID);
                                                    }
                                                }
                                                McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew2);
                                                AlquimiaEvent.verifyLevelUp(player, xpAnteriorr);

                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }, 2L, 1L);

                    if (RotacaoAlq.get(location) == null) {
                        new RotacaoAlq(location, rotacao).insert();
                    } else {
                        RotacaoAlq.get(location).setId(rotacao);
                    }

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            Bukkit.getScheduler().cancelTask(rotacao);
                        }
                    }, 201L);
                }
            }
        } else if (event.getClick() == ClickType.SHIFT_LEFT || event.getClick() == ClickType.SHIFT_RIGHT) {
            if (event.getClickedInventory() != null && event.getClickedInventory().getType().equals(InventoryType.BREWING)) {
                if (event.getSlot() == 0 || event.getSlot() == 1 || event.getSlot() == 2) {
                    if (event.getCurrentItem().getType().equals(Material.POTION) || event.getCurrentItem().getType().equals(Material.SPLASH_POTION)) {
                        final Inventory inventory = player.getOpenInventory().getTopInventory();

                        if (event.getSlot() == 0) {
                            for (int i = 0; i <= player.getInventory().getSize(); i++) {
                                int containsEsp = 0;
                                if (player.getInventory().getItem(i) == null) {
                                    containsEsp++;
                                }
                                if (i == player.getInventory().getSize() && containsEsp != 0) {
                                    final ItemStack item = inventory.getItem(0).clone();
                                    event.setCancelled(true);
                                    player.getInventory().addItem(item);
                                }
                            }
                        }
                        if (event.getSlot() == 1) {
                            for (int i = 0; i <= player.getInventory().getSize(); i++) {
                                int containsEsp = 0;
                                if (player.getInventory().getItem(i) == null) {
                                    containsEsp++;
                                }
                                if (i == player.getInventory().getSize() && containsEsp != 0) {
                                    final ItemStack item = inventory.getItem(1).clone();
                                    event.setCancelled(true);
                                    player.getInventory().addItem(item);
                                }
                            }
                        }
                        if (event.getSlot() == 2) {
                            for (int i = 0; i <= player.getInventory().getSize(); i++) {
                                int containsEsp = 0;
                                if (player.getInventory().getItem(i) == null) {
                                    containsEsp++;
                                }
                                if (i == player.getInventory().getSize() && containsEsp != 0) {
                                    final ItemStack item = inventory.getItem(2).clone();
                                    event.setCancelled(true);
                                    player.getInventory().addItem(item);
                                }
                            }
                        }

                        final Location block = event.getClickedInventory().getLocation();
                        final String location = block.getWorld().getName() + ":" + (block.getBlockX()) + ":" + (block.getBlockY()) + ":" + (block.getBlockZ());
                        if (RotacaoAlq.get(location) != null && RotacaoAlq.get(location).getId() != 0) {
                            Bukkit.getScheduler().cancelTask(RotacaoAlq.get(location).getId());
                            RotacaoAlq.get(location).setId(0);
                        }
                    }
                }
                if (event.getSlot() == 3) {
                    if (event.getClickedInventory().getItem(3) != null) {
                        if (event.getCurrentItem().getType().equals(Material.LILY_PAD) || event.getCurrentItem().getType().equals(Material.GOLD_NUGGET) || event.getCurrentItem().getType().equals(Material.APPLE) || event.getCurrentItem().getType().equals(Material.SUGAR_CANE) || event.getCurrentItem().getType().equals(Material.ROTTEN_FLESH) || event.getCurrentItem().getType().equals(Material.BROWN_MUSHROOM)
                                || event.getCurrentItem().getType().equals(Material.RED_MUSHROOM) || event.getCurrentItem().getType().equals(Material.CARROT) || event.getCurrentItem().getType().equals(Material.SLIME_BALL) || event.getCurrentItem().equals(Material.CHORUS_FRUIT) || event.getCurrentItem().equals(Material.GOLDEN_APPLE) || event.getCurrentItem().getType().equals(Material.DIAMOND_BLOCK)
                                || event.getCurrentItem().getType().equals(Material.TURTLE_EGG) || event.getCurrentItem().getType().equals(Material.ENDER_EYE)) {
                            final Location block = event.getClickedInventory().getLocation();
                            final String location = block.getWorld().getName() + ":" + (block.getBlockX()) + ":" + (block.getBlockY()) + ":" + (block.getBlockZ());
                            BreweAPI.removeInfo(location);

                            if (RotacaoAlq.get(location) != null && RotacaoAlq.get(location).getId() != 0) {
                                Bukkit.getScheduler().cancelTask(RotacaoAlq.get(location).getId());
                                RotacaoAlq.get(location).setId(0);
                            }
                        }
                    }
                }
            }

            if (event.getClickedInventory().getType().equals(InventoryType.PLAYER) && player.getOpenInventory().getTopInventory().getType().equals(InventoryType.BREWING)) {
                final Inventory inventory = player.getOpenInventory().getTopInventory();
                BrewingStand stand = (BrewingStand) player.getOpenInventory().getTopInventory().getHolder();

                ItemStack hand = event.getCurrentItem();
                if (hand == null || hand.getType() == Material.AIR) {
                    return;
                } else {
                    if (!hand.getType().equals(Material.LILY_PAD) && !hand.getType().equals(Material.GOLD_NUGGET) && !hand.getType().equals(Material.APPLE) && !hand.getType().equals(Material.SUGAR_CANE) && !hand.getType().equals(Material.ROTTEN_FLESH) && !hand.getType().equals(Material.BROWN_MUSHROOM)
                            && !hand.getType().equals(Material.RED_MUSHROOM) && !hand.getType().equals(Material.CARROT) && !hand.getType().equals(Material.SLIME_BALL) && !hand.getType().equals(Material.CHORUS_FRUIT) && !hand.getType().equals(Material.GOLDEN_APPLE) && !hand.getType().equals(Material.DIAMOND_BLOCK)
                            && !hand.getType().equals(Material.TURTLE_EGG) && !hand.getType().equals(Material.ENDER_EYE) && !hand.getType().equals(Material.POTION)) {
                        return;
                    }
                }

                if (hand.getType().equals(Material.LILY_PAD)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 150) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.GOLD_NUGGET)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 200) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.APPLE)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 250) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.CHORUS_FRUIT)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 300) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.ROTTEN_FLESH)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 350) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.BROWN_MUSHROOM)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 400) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.RED_MUSHROOM)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 450) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.SLIME_BALL)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 500) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.CARROT)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 550) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.SUGAR_CANE)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 600) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.GOLDEN_APPLE)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 650) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.DIAMOND_BLOCK)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 700) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.TURTLE_EGG)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 750) {
                        return;
                    }
                }
                if (hand.getType().equals(Material.ENDER_EYE)) {
                    if (McMMOApi.getNivel(UUID + "Alquimia") < 800) {
                        return;
                    }
                }

                final Location block = player.getOpenInventory().getTopInventory().getLocation();
                final String location = block.getWorld().getName() + ":" + (block.getBlockX()) + ":" + (block.getBlockY()) + ":" + (block.getBlockZ());
                if (BrewingPlayer.get(location) == null) {
                    new BrewingPlayer(location, UUID, player).insert();
                } else {
                    BrewingPlayer.get(location).setUUID(UUID);
                    BrewingPlayer.get(location).setPlayer(player);
                }

                if (!hand.getType().equals(Material.POTION)) {
                    if (BreweAPI.getExist(location) == null) {
                        player.getOpenInventory().getTopInventory().setItem(3, hand);
                        BreweAPI.createInfo(location, hand.getType().name() + ":" + (hand.getAmount()));
                        player.getOpenInventory().getBottomInventory().setItem(event.getSlot(), new ItemStack(Material.AIR));
                    } else {
                        final String[] split = BreweAPI.getBloco(location).split(":");
                        final int quantia = Integer.valueOf(split[1]);
                        final int itemQ = hand.getAmount();
                        final int espaco = 64 - quantia;

                        if (itemQ <= espaco) {
                            final ItemStack item = new ItemStack(Material.getMaterial(split[0]), quantia + itemQ);
                            player.getOpenInventory().getTopInventory().setItem(3, item);
                            player.getInventory().getItem(event.getSlot()).setType(Material.AIR);
                            BreweAPI.updateInfo(location, split[0] + ":" + (quantia + itemQ));
                        } else if (itemQ > espaco) {
                            final ItemStack item = new ItemStack(Material.getMaterial(split[0]), 64);
                            player.getOpenInventory().getTopInventory().setItem(3, item);
                            player.getInventory().getItem(event.getSlot()).setAmount(hand.getAmount() - espaco);
                            BreweAPI.updateInfo(location, split[0] + ":" + (64));
                        }

                        if (RotacaoAlq.get(location) != null && RotacaoAlq.get(location).getId() != 0) {
                            if (HasAddItemBrew.get(UUID) == null) {
                                new HasAddItemBrew(UUID, true).insert();
                            } else {
                                HasAddItemBrew.get(UUID).setHasAdd(true);
                            }
                        }
                    }
                    player.updateInventory();
                }

                int contains = 0;
                if (hand.getType().equals(Material.POTION)) {
                    if (player.getOpenInventory().getTopInventory().getItem(3) == null) {
                        return;
                    }
                    final PotionMeta meta = (PotionMeta) hand.getItemMeta();
                    if (meta.getCustomEffects().size() == 0) {
                        contains++;
                    }
                }
                for (int i = 1; i <= 3; i++) {
                    if (player.getOpenInventory().getTopInventory().getItem(i - 1) != null && player.getOpenInventory().getTopInventory().getItem(i - 1).getType().equals(Material.POTION)) {
                        final PotionMeta meta = (PotionMeta) player.getOpenInventory().getTopInventory().getItem(i - 1).getItemMeta();
                        if (meta.getCustomEffects().size() == 0) {
                            contains++;
                        }
                    }
                }
                if (contains == 0) {
                    return;
                }

                if (stand.getBrewingTime() == 0) {
                    final String[] bloco = BreweAPI.getBloco(location).split(":");
                    final int quantia = Integer.valueOf(bloco[1]) - 1;
                    final ItemStack item = new ItemStack(Material.getMaterial(bloco[0]), quantia);
                    final String blockN = bloco[0] + ":" + (quantia);
                    if (quantia == 0) {
                        BreweAPI.removeInfo(location);
                    } else {
                        BreweAPI.updateInfo(location, blockN);
                    }

                    if (item.getType().equals(Material.DIAMOND_BLOCK)) {
                        int containsPot = 0;
                        for (int i = 1; i <= 3; i++) {
                            final int slot = i - 1;
                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Força III (2:20)")) {
                                containsPot++;
                            }
                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Velocidade III (2:00)")) {
                                containsPot++;
                            }
                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Pressa (2:00)")) {
                                containsPot++;
                            }
                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Resistência (3:00)")) {
                                containsPot++;
                            }
                        }
                        if (containsPot == 0) {
                            return;
                        }
                    }

                    final ItemStack ingredient = player.getOpenInventory().getTopInventory().getItem(3).clone();
                    final ItemStack finalItem = item;
                    final int rotacao = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                        int time = 200;
                        ItemStack slot1;
                        ItemStack slot2;
                        ItemStack slot3;

                        @Override
                        public void run() {
                            if (this.time == 200) {
                                this.slot1 = player.getOpenInventory().getTopInventory().getItem(0);
                                this.slot2 = player.getOpenInventory().getTopInventory().getItem(1);
                                this.slot3 = player.getOpenInventory().getTopInventory().getItem(2);
                            }
                            if (HasAddItemBrew.get(UUID) != null && HasAddItemBrew.get(UUID).getHasAdd() == true) {
                                final String[] bloco1 = BreweAPI.getBloco(location).split(":");
                                final int quantia1 = Integer.valueOf(bloco1[1]);
                                final ItemStack item1 = new ItemStack(Material.getMaterial(bloco1[0]), quantia1);

                                if (item1.getAmount() <= 0) {
                                    this.time--;
                                    stand.setBrewingTime(this.time);
                                    stand.update(true);
                                    stand.getInventory().setItem(3, new ItemStack(Material.AIR));
                                    stand.getInventory().setItem(0, this.slot1);
                                    stand.getInventory().setItem(1, this.slot2);
                                    stand.getInventory().setItem(2, this.slot3);

                                } else {
                                    this.time--;
                                    stand.setBrewingTime(this.time);
                                    stand.update(true);
                                    stand.getInventory().setItem(3, item1);
                                    stand.getInventory().setItem(0, this.slot1);
                                    stand.getInventory().setItem(1, this.slot2);
                                    stand.getInventory().setItem(2, this.slot3);
                                }

                                if (this.time == 0) {
                                    final int xpAnterior = McMMOApi.getXP(UUID + "Alquimia");
                                    int xpNew2 = 5;
                                    if (BoosterHash.get(UUID) != null) {
                                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                            xpNew2 += (xpNew2 * 25) / 100;
                                        } else {
                                            xpNew2 += (xpNew2 * 50) / 100;
                                        }

                                        if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                            BoosterHash.CACHE.remove(UUID);
                                        }
                                    }
                                    McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew2);
                                    AlquimiaEvent.verifyLevelUp(player, xpAnterior);
                                    HasAddItemBrew.get(UUID).setHasAdd(false);
                                }
                            } else {
                                if (finalItem.getAmount() <= 0) {
                                    this.time--;
                                    stand.setBrewingTime(this.time);
                                    stand.update(true);
                                    stand.getInventory().setItem(3, new ItemStack(Material.AIR));
                                    stand.getInventory().setItem(0, this.slot1);
                                    stand.getInventory().setItem(1, this.slot2);
                                    stand.getInventory().setItem(2, this.slot3);
                                } else {
                                    this.time--;
                                    stand.setBrewingTime(this.time);
                                    stand.update(true);
                                    stand.getInventory().setItem(3, finalItem);
                                    stand.getInventory().setItem(0, this.slot1);
                                    stand.getInventory().setItem(1, this.slot2);
                                    stand.getInventory().setItem(2, this.slot3);
                                }

                                if (this.time == 0) {
                                    final int xpAnterior = McMMOApi.getXP(UUID + "Alquimia");
                                    int xpNew3 = 5;
                                    if (BoosterHash.get(UUID) != null) {
                                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                            xpNew3 += (xpNew3 * 25) / 100;
                                        } else {
                                            xpNew3 += (xpNew3 * 50) / 100;
                                        }

                                        if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                            BoosterHash.CACHE.remove(UUID);
                                        }
                                    }
                                    McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew3);
                                    AlquimiaEvent.verifyLevelUp(player, xpAnterior);

                                    if (ingredient.getType().equals(Material.LILY_PAD)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de passos aquático");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Passos aquático (2:00)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.GOLD_NUGGET)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de absorção");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Absorção (1:50)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.APPLE)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de vida bônus");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Vida bônus (2:20)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.CHORUS_FRUIT)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de saturação");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Saturação (2:10)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.ROTTEN_FLESH)) {
                                        final ItemStack potion = new ItemStack(Material.SPLASH_POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção arremessável de fome");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Fome (0:45)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.BROWN_MUSHROOM)) {
                                        final ItemStack potion = new ItemStack(Material.SPLASH_POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção arremessável de náusea");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Náusea (0:30)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.RED_MUSHROOM)) {
                                        final ItemStack potion = new ItemStack(Material.SPLASH_POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção arremessável de cegueira");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Cegueira (0:30)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.SLIME_BALL)) {
                                        final ItemStack potion = new ItemStack(Material.SPLASH_POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção arremessável de fadiga");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Fadiga (0:35)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.CARROT)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de pressa");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Pressa (2:00)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.SUGAR_CANE)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de velocidade");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Velocidade III (2:00)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.GOLDEN_APPLE)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de resistência");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Resistência (3:00)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.TURTLE_EGG)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de força");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Força III (2:20)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.ENDER_EYE)) {
                                        final ItemStack potion = new ItemStack(Material.POTION);
                                        potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                        final ItemMeta meta = potion.getItemMeta();
                                        meta.setDisplayName("Poção de especial");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(ChatColor.BLUE + "Força II, Velocidade II, Pressa II, Resistência II (6:00)");
                                        meta.setLore(lore);
                                        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                        potion.setItemMeta(meta);

                                        for (int i = 1; i <= 3; i++) {
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getType().equals(Material.POTION)) {
                                                final int slot = i - 1;
                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                    if (ingredient.getType().equals(Material.DIAMOND_BLOCK)) {
                                        for (int i = 1; i <= 3; i++) {
                                            final int slot = i - 1;
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Força III (2:20)")) {
                                                final ItemStack potion = new ItemStack(Material.POTION);
                                                potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                                final ItemMeta meta = potion.getItemMeta();
                                                meta.setDisplayName("Poção de força");
                                                final List<String> lore = new ArrayList<>();
                                                lore.add(ChatColor.BLUE + "Força III (5:00)");
                                                meta.setLore(lore);
                                                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                                potion.setItemMeta(meta);

                                                final int xpAnteriorr = McMMOApi.getXP(UUID + "Alquimia");
                                                int xpNew2 = 8;
                                                if (BoosterHash.get(UUID) != null) {
                                                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                        xpNew2 += (xpNew2 * 25) / 100;
                                                    } else {
                                                        xpNew2 += (xpNew2 * 50) / 100;
                                                    }

                                                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                        BoosterHash.CACHE.remove(UUID);
                                                    }
                                                }
                                                McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew2);
                                                AlquimiaEvent.verifyLevelUp(player, xpAnteriorr);

                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Velocidade III (2:00)")) {
                                                final ItemStack potion = new ItemStack(Material.POTION);
                                                potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                                final ItemMeta meta = potion.getItemMeta();
                                                meta.setDisplayName("Poção de velocidade");
                                                final List<String> lore = new ArrayList<>();
                                                lore.add(ChatColor.BLUE + "Velocidade III (5:00)");
                                                meta.setLore(lore);
                                                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                                potion.setItemMeta(meta);

                                                final int xpAnteriorr = McMMOApi.getXP(UUID + "Alquimia");
                                                int xpNew2 = 8;
                                                if (BoosterHash.get(UUID) != null) {
                                                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                        xpNew2 += (xpNew2 * 25) / 100;
                                                    } else {
                                                        xpNew2 += (xpNew2 * 50) / 100;
                                                    }

                                                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                        BoosterHash.CACHE.remove(UUID);
                                                    }
                                                }
                                                McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew2);
                                                AlquimiaEvent.verifyLevelUp(player, xpAnteriorr);

                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Pressa (2:00)")) {
                                                final ItemStack potion = new ItemStack(Material.POTION);
                                                potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                                final ItemMeta meta = potion.getItemMeta();
                                                meta.setDisplayName("Poção de pressa");
                                                final List<String> lore = new ArrayList<>();
                                                lore.add(ChatColor.BLUE + "Pressa (5:00)");
                                                meta.setLore(lore);
                                                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                                potion.setItemMeta(meta);

                                                final int xpAnteriorr = McMMOApi.getXP(UUID + "Alquimia");
                                                int xpNew2 = 8;
                                                if (BoosterHash.get(UUID) != null) {
                                                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                        xpNew2 += (xpNew2 * 25) / 100;
                                                    } else {
                                                        xpNew2 += (xpNew2 * 50) / 100;
                                                    }

                                                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                        BoosterHash.CACHE.remove(UUID);
                                                    }
                                                }
                                                McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew2);
                                                AlquimiaEvent.verifyLevelUp(player, xpAnteriorr);

                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                            if (inventory.getItem(i - 1) != null && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.BLUE + "Resistência (3:00)")) {
                                                final ItemStack potion = new ItemStack(Material.POTION);
                                                potion.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
                                                final ItemMeta meta = potion.getItemMeta();
                                                meta.setDisplayName("Poção de resistência");
                                                final List<String> lore = new ArrayList<>();
                                                lore.add(ChatColor.BLUE + "Resistência (5:00)");
                                                meta.setLore(lore);
                                                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                                                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                                                potion.setItemMeta(meta);

                                                final int xpAnteriorr = McMMOApi.getXP(UUID + "Alquimia");
                                                int xpNew2 = 8;
                                                if (BoosterHash.get(UUID) != null) {
                                                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                        xpNew2 += (xpNew2 * 25) / 100;
                                                    } else {
                                                        xpNew2 += (xpNew2 * 50) / 100;
                                                    }

                                                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                        BoosterHash.CACHE.remove(UUID);
                                                    }
                                                }
                                                McMMOApi.updateXP(UUID + "Alquimia", McMMOApi.getXP(UUID + "Alquimia") + xpNew2);
                                                AlquimiaEvent.verifyLevelUp(player, xpAnteriorr);

                                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        inventory.setItem(slot, potion);
                                                    }
                                                }, 1L);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }, 2L, 1L);

                    if (RotacaoAlq.get(location) == null) {
                        new RotacaoAlq(location, rotacao).insert();
                    } else {
                        RotacaoAlq.get(location).setId(rotacao);
                    }

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            Bukkit.getScheduler().cancelTask(rotacao);
                        }
                    }, 201L);
                }
            }
        }
    }
}
