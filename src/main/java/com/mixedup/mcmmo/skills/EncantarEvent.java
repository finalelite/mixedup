package com.mixedup.mcmmo.skills;

import com.mixedup.Main;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.managers.InventoryEncantar;
import com.mixedup.mcmmo.McMMOApi;
import com.mixedup.mcmmo.ProbabilityApi;
import com.mixedup.mcmmo.utils.BoosterHash;
import com.mixedup.utils.AlternateColor;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Date;
import java.util.Random;

public class EncantarEvent implements Listener {

    private static Enchantment getEnchantment(final EnchantmentStorageMeta meta) {
        // Enchantment[] enchs = Enchantment.values();
        //        Enchantment enchantment = null;
        //        for (int i = 1; i <= enchs.length; i++) {
        //            if (meta.getStoredEnchantLevel(enchs[i - 1]) != 0) {
        //                enchantment = enchs[i - 1];
        //                break;
        //            }
        //        }
        //        return enchantment;
        return meta.getStoredEnchants().keySet().stream().findFirst().orElse(null);
    }

    public static void verifyLevelUp(final Player player, final int xpAnterior, int newXp) {
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (BoosterHash.get(UUID) != null) {
            if (BoosterHash.get(UUID).gettime() - (new Date().getTime() / 1000) <= 0) {
                BoosterHash.CACHE.remove(UUID);
            }
        }

        final int xpNextLevel = McMMOApi.getProximoNivelXP(UUID + "Encantar");
        final int xpNow = McMMOApi.getXP(UUID + "Encantar");
        if (xpNextLevel <= xpNow) {
            final int nivel = McMMOApi.updateLevel(UUID + "Encantar");

            if (nivel == 100 || nivel == 200 || nivel == 300 || nivel == 400 || nivel == 500 || nivel == 600) {
                for (final Player target : Bukkit.getOnlinePlayers()) {
                    if (nivel != 600) {
                        target.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (nivel) + "&e na habilidade Encantar!&e!\n \n"));
                        if (target.getName().equals(player.getName())) {
                            target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                        } else {
                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        target.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade Encantar!\n \n"));
                        if (target.getName().equals(player.getName())) {
                            target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                            target.playSound(target.getLocation().add(10, 0, 10), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                        } else {
                            target.playSound(target.getLocation().add(10, 0, 10), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                        }
                    }
                }
            } else {
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (nivel)) + ChatColor.YELLOW + " na habilidade Encantar!");
            }
        } else {

            player.sendActionBar(ChatColor.GREEN + "Encantar (" + (xpNow) + "/" + (xpNextLevel) + ")XP +" + (newXp) + "XP");

        }
    }

    @EventHandler
    public void onEnchant(final InventoryClickEvent event) {
        try {
            final Player player = (Player) event.getWhoClicked();
            final String UUID = PlayerUUID.getUUID(player.getName());

            if (BoosterHash.get(UUID) != null) {
                if (BoosterHash.get(UUID).gettime() - (new Date().getTime() / 1000) <= 0) {
                    BoosterHash.CACHE.remove(UUID);
                }
            }

            if (event.getCursor() != null) {
                if (event.getCursor().getType().equals(Material.ENCHANTED_BOOK)) {
                    if (event.getCursor() == null || event.getClickedInventory() == null) return;
                    final ItemStack clicked = event.getClickedInventory().getItem(event.getSlot());
                    final EnchantmentStorageMeta meta = (EnchantmentStorageMeta) event.getCursor().getItemMeta();

                    if (clicked == null || clicked.getType().equals(Material.AIR)) {
                        return;
                    }

                    final ItemStack antiRadiacao = new ItemStack(Material.ENCHANTED_BOOK);
                    final ItemMeta meta4 = antiRadiacao.getItemMeta();
                    meta4.setDisplayName(ChatColor.YELLOW + "Proteção radiação");
                    antiRadiacao.setItemMeta(meta4);
                    if (event.getCursor().getItemMeta().getDisplayName().equalsIgnoreCase(antiRadiacao.getItemMeta().getDisplayName())) return;
                    if(getEnchantment(meta) == null) return;

                    event.setCancelled(true);
                    final int probability = Integer.valueOf(event.getCursor().getItemMeta().getLore().get(1).substring(9, event.getCursor().getItemMeta().getLore().get(1).length() - 1));
                    if (ProbabilityApi.probab(probability * 1000) == true) {
                        if (player.getGameMode().equals(GameMode.CREATIVE)) return;
                        final int xpAnterior = McMMOApi.getXP(UUID + "Encantar");
                        int acrescimo = 0;

                        if (meta.getStoredEnchantLevel(EncantarEvent.getEnchantment(meta)) == 1) {
                            acrescimo = 60;
                        } else if (meta.getStoredEnchantLevel(EncantarEvent.getEnchantment(meta)) == 2) {
                            acrescimo = 70;
                        } else if (meta.getStoredEnchantLevel(EncantarEvent.getEnchantment(meta)) == 3) {
                            acrescimo = 100;
                        } else if (meta.getStoredEnchantLevel(EncantarEvent.getEnchantment(meta)) == 4) {
                            acrescimo = 110;
                        } else if (meta.getStoredEnchantLevel(EncantarEvent.getEnchantment(meta)) == 5) {
                            acrescimo = 150;
                        }

                        boolean applicate = false;

                        if (EncantarEvent.getEnchantment(meta).canEnchantItem(clicked) == true) {
                            final ItemMeta metaNew = clicked.getItemMeta();
                            metaNew.addEnchant(EncantarEvent.getEnchantment(meta), meta.getStoredEnchantLevel(EncantarEvent.getEnchantment(meta)), true);
                            clicked.setItemMeta(metaNew);

                            player.playSound(player.getLocation(), Sound.BLOCK_ENCHANTMENT_TABLE_USE, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.sendMessage(ChatColor.GREEN + " * Encantamento bem sucedido.");

                            if (BoosterHash.get(UUID) != null) {
                                if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                    acrescimo += (acrescimo * 25) / 100;
                                } else {
                                    acrescimo += (acrescimo * 50) / 100;
                                }

                            }

                            McMMOApi.updateXP(UUID + "Encantar", xpAnterior + (acrescimo * 2));
                            EncantarEvent.verifyLevelUp(player, xpAnterior, acrescimo * 2);
                            applicate = true;
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, este item não aceita este encantamento!");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            return;
                        }

                        if (applicate == true) {
                            event.getCursor().setType(Material.AIR);
                            event.getCursor().setAmount(0);
                        }
                    } else {
                        if (EncantarEvent.getEnchantment(meta).canEnchantItem(clicked) == false) return;

                        event.getCursor().setType(Material.AIR);
                        event.getCursor().setAmount(0);
                        player.playSound(player.getLocation(), Sound.ENTITY_ITEM_BREAK, 1.0f, 1.0f);

                        final Random random = new Random();
                        final int rd = random.nextInt(6);
                        if (rd == 1) {
                            player.sendMessage(ChatColor.RED + " * As vezes nem mesmo os melhores magos estão em seus melhores dias.");
                        } else if (rd == 2) {
                            player.sendMessage(ChatColor.RED + " * Infelizmente você não está em um dia de sorte.");
                        } else if (rd == 3) {
                            player.sendMessage(ChatColor.RED + " * O livro se corroeu de tanta magia emanada pelo mesmo.");
                        } else if (rd == 4) {
                            player.sendMessage(ChatColor.RED + " * As vezes é melhor aceitar e seguir em frente.");
                        } else if (rd == 5) {
                            player.sendMessage(ChatColor.RED + " * Hmm...");
                        } else if (rd == 6) {
                            player.sendMessage(ChatColor.RED + " * Olhos de rã, penas de maribundo. BUUMMMMMM!!!!  ...  (?)");
                        }
                    }
                }
            }
        } catch (final Exception e) {
            event.getWhoClicked().sendMessage(ChatColor.RED + " * Ops, este livro aparentemente não funcionara para isto!");
        }
    }

    @EventHandler
    public void onClickInventory(final InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getClickedInventory() == null) return;

        if (BoosterHash.get(UUID) != null) {
            if (BoosterHash.get(UUID).gettime() - (new Date().getTime() / 1000) <= 0) {
                BoosterHash.CACHE.remove(UUID);
            }
        }

        if (event.getClickedInventory().getName().contains("Encantamentos nível 5: ")) {
            event.setCancelled(true);

            if (event.getCurrentItem() != null && !event.getCurrentItem().getType().equals(Material.AIR)) {
                if (event.getCurrentItem().getType().equals(Material.ARROW)) {
                    final ItemStack item = event.getWhoClicked().getItemInHand();
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            if (item.getItemMeta().getLore().get(1).contains("Básicos") && item.getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "Livro de encantamentos")) {
                                player.openInventory(InventoryEncantar.invNivel(UUID, true, false, false, player));
                            }
                            if (item.getItemMeta().getLore().get(1).contains("Médios") && item.getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "Livro de encantamentos")) {
                                player.openInventory(InventoryEncantar.invNivel(UUID, false, true, false, player));
                            }
                            if (item.getItemMeta().getLore().get(1).contains("Avançados") && item.getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "Livro de encantamentos")) {
                                player.openInventory(InventoryEncantar.invNivel(UUID, false, false, true, player));
                            }
                        }
                    }, 5L);
                } else {
                    InventoryEncantar.giveBook(event.getCurrentItem(), 5, player);
                }
            }
        }

        if (event.getClickedInventory().getName().contains("Encantamentos nível 4: ")) {
            event.setCancelled(true);

            if (event.getCurrentItem() != null && !event.getCurrentItem().getType().equals(Material.AIR)) {
                if (event.getCurrentItem().getType().equals(Material.ARROW)) {
                    final ItemStack item = event.getWhoClicked().getItemInHand();
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            if (item.getItemMeta().getLore().get(1).contains("Básicos") && item.getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "Livro de encantamentos")) {
                                player.openInventory(InventoryEncantar.invNivel(UUID, true, false, false, player));
                            }
                            if (item.getItemMeta().getLore().get(1).contains("Médios") && item.getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "Livro de encantamentos")) {
                                player.openInventory(InventoryEncantar.invNivel(UUID, false, true, false, player));
                            }
                            if (item.getItemMeta().getLore().get(1).contains("Avançados") && item.getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "Livro de encantamentos")) {
                                player.openInventory(InventoryEncantar.invNivel(UUID, false, false, true, player));
                            }
                        }
                    }, 5L);
                } else {
                    InventoryEncantar.giveBook(event.getCurrentItem(), 1, player);
                }
            }
        }

        if (event.getClickedInventory().getName().contains("Encantamentos nível 4: ")) {
            event.setCancelled(true);

            if (event.getCurrentItem() != null && !event.getCurrentItem().getType().equals(Material.AIR)) {
                if (event.getCurrentItem().getType().equals(Material.ARROW)) {
                    final ItemStack item = event.getWhoClicked().getItemInHand();
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            if (item.getItemMeta().getLore().get(1).contains("Básicos") && item.getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "Livro de encantamentos")) {
                                player.openInventory(InventoryEncantar.invNivel(UUID, true, false, false, player));
                            }
                            if (item.getItemMeta().getLore().get(1).contains("Médios") && item.getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "Livro de encantamentos")) {
                                player.openInventory(InventoryEncantar.invNivel(UUID, false, true, false, player));
                            }
                            if (item.getItemMeta().getLore().get(1).contains("Avançados") && item.getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "Livro de encantamentos")) {
                                player.openInventory(InventoryEncantar.invNivel(UUID, false, false, true, player));
                            }
                        }
                    }, 5L);
                } else {
                    InventoryEncantar.giveBook(event.getCurrentItem(), 3, player);
                }
            }
        }

        if (event.getClickedInventory().getName().contains("Encantamentos nível 2: ")) {
            event.setCancelled(true);

            if (event.getCurrentItem() != null && !event.getCurrentItem().getType().equals(Material.AIR)) {
                if (event.getCurrentItem().getType().equals(Material.ARROW)) {
                    final ItemStack item = event.getWhoClicked().getItemInHand();
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            if (item.getItemMeta().getLore().get(1).contains("Básicos") && item.getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "Livro de encantamentos")) {
                                player.openInventory(InventoryEncantar.invNivel(UUID, true, false, false, player));
                            }
                            if (item.getItemMeta().getLore().get(1).contains("Médios") && item.getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "Livro de encantamentos")) {
                                player.openInventory(InventoryEncantar.invNivel(UUID, false, true, false, player));
                            }
                            if (item.getItemMeta().getLore().get(1).contains("Avançados") && item.getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "Livro de encantamentos")) {
                                player.openInventory(InventoryEncantar.invNivel(UUID, false, false, true, player));
                            }
                        }
                    }, 5L);
                } else {
                    InventoryEncantar.giveBook(event.getCurrentItem(), 2, player);
                }
            }
        }

        if (event.getClickedInventory().getName().contains("Encantamentos nível 1: ")) {
            event.setCancelled(true);

            if (event.getCurrentItem() != null && !event.getCurrentItem().getType().equals(Material.AIR)) {
                if (event.getCurrentItem().getType().equals(Material.ARROW)) {
                    final ItemStack item = event.getWhoClicked().getItemInHand();
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            if (item.getItemMeta().getLore().get(1).contains("Básicos") && item.getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "Livro de encantamentos")) {
                                player.openInventory(InventoryEncantar.invNivel(UUID, true, false, false, player));
                            }
                            if (item.getItemMeta().getLore().get(1).contains("Médios") && item.getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "Livro de encantamentos")) {
                                player.openInventory(InventoryEncantar.invNivel(UUID, false, true, false, player));
                            }
                            if (item.getItemMeta().getLore().get(1).contains("Avançados") && item.getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "Livro de encantamentos")) {
                                player.openInventory(InventoryEncantar.invNivel(UUID, false, false, true, player));
                            }
                        }
                    }, 5L);
                } else {
                    InventoryEncantar.giveBook(event.getCurrentItem(), 1, player);
                }
            }
        }

        if (event.getClickedInventory().getName().equalsIgnoreCase("Encantamentos níveis: ")) {
            event.setCancelled(true);
            final ItemStack item = event.getCurrentItem();

            if (item != null && !item.getType().equals(Material.AIR)) {
                if (item.getItemMeta().getLore().get(1).contains("1")) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryEncantar.invEnchant1());
                        }
                    }, 5L);
                }
                if (item.getItemMeta().getLore().get(1).contains("2")) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryEncantar.invEnchant2());
                        }
                    }, 5L);
                }
                if (item.getItemMeta().getLore().get(1).contains("3")) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryEncantar.invEnchant3());
                        }
                    }, 5L);
                }
                if (item.getItemMeta().getLore().get(1).contains("4")) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryEncantar.invEnchant4());
                        }
                    }, 5L);
                }
                if (item.getItemMeta().getLore().get(1).contains("5")) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryEncantar.invEnchant5());
                        }
                    }, 5L);
                }
            }
        }
    }

    @EventHandler
    public void onClick(final PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getAction().equals(Action.RIGHT_CLICK_AIR)) {
            if (event.getPlayer().getItemInHand() != null) {
                if (event.getPlayer().getItemInHand().getType().equals(Material.BOOK)) {
                    final ItemStack item = event.getPlayer().getItemInHand();

                    if (item.getItemMeta().getLore().get(1).contains("Básicos") && item.getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "Livro de encantamentos")) {
                        player.openInventory(InventoryEncantar.invNivel(UUID, true, false, false, player));
                    }
                    if (item.getItemMeta().getLore().get(1).contains("Médios") && item.getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "Livro de encantamentos")) {
                        player.openInventory(InventoryEncantar.invNivel(UUID, false, true, false, player));
                    }
                    if (item.getItemMeta().getLore().get(1).contains("Avançados") && item.getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "Livro de encantamentos")) {
                        player.openInventory(InventoryEncantar.invNivel(UUID, false, false, true, player));
                    }
                }
            }
        }
    }
}
