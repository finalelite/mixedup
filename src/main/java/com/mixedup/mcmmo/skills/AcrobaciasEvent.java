package com.mixedup.mcmmo.skills;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.mcmmo.McMMOApi;
import com.mixedup.mcmmo.ProbabilityApi;
import com.mixedup.mcmmo.utils.BoosterHash;
import com.mixedup.mcmmo.utils.FallHeight;
import com.mixedup.mcmmo.utils.FallHeightNoShift;
import com.mixedup.mcmmo.utils.TookDamaged;
import com.mixedup.utils.AlternateColor;
import com.mixedup.worldProtection.hashs.ProtectionUtil;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.Date;

public class AcrobaciasEvent implements Listener {

    public static void verifyLevelUp(final Player player, final int xpAnterior, int newXp) {
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (BoosterHash.get(UUID) != null) {
            if (BoosterHash.get(UUID).gettime() - (new Date().getTime() / 1000) <= 0) {
                BoosterHash.CACHE.remove(UUID);
            }
        }

        if (McMMOApi.getProximoNivelXP(UUID + "Acrobacias") <= McMMOApi.getXP(UUID + "Acrobacias")) {
            McMMOApi.updateLevel(UUID + "Acrobacias");

            if (McMMOApi.getNivel(UUID + "Acrobacias") == 100 || McMMOApi.getNivel(UUID + "Acrobacias") == 200 || McMMOApi.getNivel(UUID + "Acrobacias") == 300 || McMMOApi.getNivel(UUID + "Acrobacias") == 400 || McMMOApi.getNivel(UUID + "Acrobacias") == 500) {
                for (final Player target : Bukkit.getOnlinePlayers()) {
                    if (McMMOApi.getNivel(UUID + "Acrobacias") != 500) {
                        target.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (McMMOApi.getNivel(UUID + "Acrobacias")) + "&e na habilidade de Acrobacias!&e!\n \n"));
                        if (target.getName().equals(player.getName())) {
                            target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                        } else {
                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        target.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade de Acrobacias!\n \n"));
                        if (target.getName().equals(player.getName())) {
                            target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                            target.playSound(target.getLocation().add(10, 0, 10), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                        } else {
                            target.playSound(target.getLocation().add(10, 0, 10), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                        }
                    }
                }
            } else {
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (McMMOApi.getNivel(UUID + "Acrobacias"))) + ChatColor.YELLOW + " na habilidade de Acrobacias!");
            }
        } else {

            player.sendActionBar(ChatColor.GREEN + "Acrobacias (" + (McMMOApi.getXP(UUID + "Acrobacias")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Acrobacias")) + ")XP +" + (newXp) + "XP");

        }
    }

    @EventHandler
    public void onDamage(final EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            final Player player = (Player) event.getEntity();
            final String UUID = PlayerUUID.getUUID(player.getName());

            if (event.getEntity().getLocation().getWorld().getName().equalsIgnoreCase("spawn") || event.getEntity().getLocation().getWorld().getName().equalsIgnoreCase("aether") || event.getEntity().getLocation().getWorld().getName().equalsIgnoreCase("guildaselect") || ProtectionUtil.playerInArea(event.getEntity().getLocation()) != null)
                return;

            if (BoosterHash.get(UUID) != null) {
                if (BoosterHash.get(UUID).gettime() - (new Date().getTime() / 1000) <= 0) {
                    BoosterHash.CACHE.remove(UUID);
                }
            }

            if (event.getCause().equals(DamageCause.FALL)) {
                if (TookDamaged.get(UUID) == null) {
                    new TookDamaged(UUID, true).insert();
                } else {
                    TookDamaged.get(UUID).settooked(true);
                }
                if (!McMMOApi.exists(UUID + "Acrobacias")) {
                    McMMOApi.createInfo(UUID + "Acrobacias");
                }

                if (FallHeight.get(UUID) != null) {
                    final int alturaY = FallHeight.get(UUID).getLastY();

                    if (alturaY != 0) {
                        final int altura = alturaY - player.getLocation().getBlockY();
                        if (altura <= 15 && player.isSneaking()) {
                            final int probability = McMMOApi.getNivel(UUID + "Acrobacias") * 100;
                            if (ProbabilityApi.probab(probability) == true) {
                                final double damage = event.getDamage() / 4;
                                if (player.getHealth() - damage <= 0) {
                                    return;
                                }
                                event.setCancelled(true);
                                player.damage(damage);

                                final int xpAnterior = McMMOApi.getXP(UUID + "Acrobacias");
                                int xpNew = 8 + (altura * 9);
                                if (BoosterHash.get(UUID) != null) {
                                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                        xpNew += (xpNew * 25) / 100;
                                    } else {
                                        xpNew += (xpNew * 50) / 100;
                                    }

                                }
                                McMMOApi.updateXP(UUID + "Acrobacias", xpAnterior + xpNew);
                                AcrobaciasEvent.verifyLevelUp(player, xpAnterior, xpNew);
                                player.sendMessage(ChatColor.RED + " * Você rolou perfeitamente!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                                TookDamaged.get(UUID).settooked(false);
                                FallHeight.get(UUID).setLastY(0);
                            } else {
                                if (player.getHealth() - event.getDamage() <= 0) {
                                    return;
                                }
                                final int xpAnterior = McMMOApi.getXP(UUID + "Acrobacias");
                                int xpNew = altura * 9;
                                if (BoosterHash.get(UUID) != null) {
                                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                        xpNew += (xpNew * 25) / 100;
                                    } else {
                                        xpNew += (xpNew * 50) / 100;
                                    }

                                }
                                McMMOApi.updateXP(UUID + "Acrobacias", xpAnterior + xpNew);
                                AcrobaciasEvent.verifyLevelUp(player, xpAnterior, xpNew);

                                TookDamaged.get(UUID).settooked(false);
                                FallHeight.get(UUID).setLastY(0);
                                return;
                            }
                        } else if (altura <= 15 && !player.isSneaking()) {
                            event.setCancelled(false);

                            TookDamaged.get(UUID).settooked(false);
                            FallHeight.get(UUID).setLastY(0);
                            return;
                        } else if (altura > 15) {
                            event.setCancelled(false);

                            TookDamaged.get(UUID).settooked(false);
                            FallHeight.get(UUID).setLastY(0);
                            return;
                        }
                    }
                }

                if (FallHeightNoShift.get(UUID) != null) {
                    final int alturaY = FallHeightNoShift.get(UUID).getLastY();

                    if (alturaY != 0) {
                        final int altura = alturaY - player.getLocation().getBlockY();
                        final int probability = McMMOApi.getNivel(UUID + "Acrobacias") * 100;
                        if (ProbabilityApi.probab(probability) == true) {
                            final double damage = event.getDamage() / 4;
                            if (player.getHealth() - (damage * 3) <= 0) {
                                return;
                            }
                            event.setCancelled(true);
                            player.damage(damage * 3);

                            final int xpAnterior = McMMOApi.getXP(UUID + "Acrobacias");
                            int xpNew = altura * 4;
                            if (BoosterHash.get(UUID) != null) {
                                if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                    xpNew += (xpNew * 25) / 100;
                                } else {
                                    xpNew += (xpNew * 50) / 100;
                                }

                                if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                    BoosterHash.CACHE.remove(UUID);
                                }
                            }
                            McMMOApi.updateXP(UUID + "Acrobacias", xpAnterior + xpNew);
                            AcrobaciasEvent.verifyLevelUp(player, xpAnterior, xpNew);
                            player.sendMessage(ChatColor.RED + " * Você rolou!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                            TookDamaged.get(UUID).settooked(false);
                            FallHeightNoShift.get(UUID).setLastY(0);
                            return;
                        } else {
                            final int xpAnterior = McMMOApi.getXP(UUID + "Acrobacias");
                            int xpNew = altura * 4;
                            if (BoosterHash.get(UUID) != null) {
                                if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                    xpNew += (xpNew * 25) / 100;
                                } else {
                                    xpNew += (xpNew * 50) / 100;
                                }

                                if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                    BoosterHash.CACHE.remove(UUID);
                                }
                            }
                            McMMOApi.updateXP(UUID + "Acrobacias", xpAnterior + xpNew);
                            AcrobaciasEvent.verifyLevelUp(player, xpAnterior, xpNew);

                            TookDamaged.get(UUID).settooked(false);
                            FallHeightNoShift.get(UUID).setLastY(0);
                            return;
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onMove(final PlayerMoveEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (player.getGameMode() == GameMode.CREATIVE || player.getGameMode() == GameMode.SPECTATOR) {
            return;
        }

        if (TookDamaged.get(UUID) == null) {
            new TookDamaged(UUID, false).insert();
        }

        if (player.isSneaking() && player.getLocation().add(0, -1, 0).getBlock().getType().equals(Material.AIR) && player.getLocation().getBlock().getType().equals(Material.AIR)) {
            if (TookDamaged.get(UUID) == null) {
                if (FallHeight.get(UUID) == null) {
                    new FallHeight(UUID, player.getLocation().getBlockY()).insert();
                } else {
                    if (FallHeight.get(UUID).getLastY() == 0) {
                        FallHeight.get(UUID).setLastY(player.getLocation().getBlockY());
                    }
                }
            } else if (TookDamaged.get(UUID).gettooked() == false) {
                if (FallHeight.get(UUID) == null) {
                    new FallHeight(UUID, player.getLocation().getBlockY()).insert();
                } else {
                    if (FallHeight.get(UUID).getLastY() == 0) {
                        FallHeight.get(UUID).setLastY(player.getLocation().getBlockY());
                    }
                }
            }
        } else if (!player.isSneaking() && player.getLocation().add(0, -1, 0).getBlock().getType().equals(Material.AIR) && player.getLocation().getBlock().getType().equals(Material.AIR)) {
            if (TookDamaged.get(UUID) == null) {
                if (FallHeightNoShift.get(UUID) == null) {
                    new FallHeightNoShift(UUID, player.getLocation().getBlockY()).insert();
                } else {
                    if (FallHeightNoShift.get(UUID).getLastY() == 0) {
                        FallHeightNoShift.get(UUID).setLastY(player.getLocation().getBlockY());
                    }
                }
            } else if (TookDamaged.get(UUID).gettooked() == false) {
                if (FallHeightNoShift.get(UUID) == null) {
                    new FallHeightNoShift(UUID, player.getLocation().getBlockY()).insert();
                } else {
                    if (FallHeightNoShift.get(UUID).getLastY() == 0) {
                        FallHeightNoShift.get(UUID).setLastY(player.getLocation().getBlockY());
                    }
                }
            }
        } else {
            if (FallHeightNoShift.get(UUID) != null && FallHeightNoShift.get(UUID).getLastY() != 0) {
                final int alturaN = FallHeightNoShift.get(UUID).getLastY() - player.getLocation().getBlockY();
                if (alturaN < 4) {
                    FallHeightNoShift.get(UUID).setLastY(0);
                }
            }

            if (FallHeight.get(UUID) != null && FallHeight.get(UUID).getLastY() != 0) {
                final int alturaN = FallHeight.get(UUID).getLastY() - player.getLocation().getBlockY();
                if (alturaN < 4) {
                    FallHeight.get(UUID).setLastY(0);
                }
            }
        }
    }
}
