package com.mixedup.mcmmo.skills;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.factions.hashs.NexusUtil;
import com.mixedup.mcmmo.McMMOApi;
import com.mixedup.mcmmo.ProbabilityApi;
import com.mixedup.mcmmo.utils.BoosterHash;
import com.mixedup.mcmmo.utils.LastDamager;
import com.mixedup.utils.AlternateColor;
import com.mixedup.worldProtection.hashs.ProtectionUtil;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.potion.PotionEffectType;

import java.util.Date;
import java.util.Random;

public class MachadoEvent implements Listener {

    @EventHandler
    public void onDamageArmor(final PlayerItemDamageEvent event) {
        final Player player = event.getPlayer();
        final String UUID = player.getUniqueId().toString();

        if (LastDamager.get(UUID) != null) {
            final String UUIDtarget = LastDamager.get(UUID).getdamager();

            final int probability = McMMOApi.getNivel(UUIDtarget + "Machado") * 60;

            if (ProbabilityApi.probab(probability) == true) {
                player.playSound(player.getLocation(), Sound.ITEM_SHIELD_BLOCK, 1.0f, 1.0f);
                event.setDamage(event.getDamage() + 1);
            }
        }
    }

    @EventHandler
    public void onHit(final EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player) {
            final Player player = (Player) event.getDamager();
            final String UUID = player.getUniqueId().toString();

            if (event.getEntity().getLocation().getWorld().getName().equalsIgnoreCase("spawn") || event.getEntity().getLocation().getWorld().getName().equalsIgnoreCase("aether") || event.getEntity().getLocation().getWorld().getName().equalsIgnoreCase("guildaselect") || ProtectionUtil.playerInArea(event.getEntity().getLocation()) != null)
                return;

            if (player.getGameMode().equals(GameMode.CREATIVE)) {
                return;
            }

            if (!player.getItemInHand().getType().equals(Material.WOODEN_AXE) && !player.getItemInHand().getType().equals(Material.STONE_AXE) && !player.getItemInHand().getType().equals(Material.GOLDEN_AXE) && !player.getItemInHand().getType().equals(Material.IRON_AXE) && !player.getItemInHand().getType().equals(Material.DIAMOND_AXE)) {
                return;
            }

            if (NexusUtil.playerInArea(event.getEntity().getLocation()) != null) return;

            if (BoosterHash.get(UUID) != null) {
                if (BoosterHash.get(UUID).gettime() - (new Date().getTime() / 1000) <= 0) {
                    BoosterHash.CACHE.remove(UUID);
                }
            }

            if (event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
                final Player target = (Player) event.getEntity();
                final String UUIDtarget = PlayerUUID.getUUID(target.getName());
                if (LastDamager.get(UUIDtarget) == null) {
                    new LastDamager(UUIDtarget, UUID).insert();
                } else {
                    LastDamager.get(UUIDtarget).setDamager(UUID);
                }
            }

            if (McMMOApi.getNivel(UUID + "Machado") == 700) {
                event.setCancelled(false);
                return;
            }

            if (event.getEntity() instanceof Player) {
                final int probability = McMMOApi.getNivel(UUID + "Machado") * 30;

                if (ProbabilityApi.probab(probability) == true) {
                    if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                        event.setDamage(event.getDamage() * 2);

                        player.sendActionBar(ChatColor.RED + " * Dano dobrado aplicado ao inimigo");
                    }
                }
            }

            final Random random = new Random();
            if (event.getEntityType().equals(EntityType.PHANTOM)) {
                int valor = random.nextInt(7);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 13;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Machado", McMMOApi.getXP(UUID + "Machado") + valor);
                verifyLevelUp(UUID, player, valor);
                return;
            }
            if (event.getEntityType().equals(EntityType.DROWNED)) {
                int valor = random.nextInt(8);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 12;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Machado", McMMOApi.getXP(UUID + "Machado") + valor);
                verifyLevelUp(UUID, player, valor);
                return;
            }
            if (event.getEntityType().equals(EntityType.GHAST)) {
                int valor = random.nextInt(8);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 11;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Machado", McMMOApi.getXP(UUID + "Machado") + valor);
                verifyLevelUp(UUID, player, valor);
                return;
            }
            if (event.getEntityType().equals(EntityType.IRON_GOLEM)) {
                int valor = random.nextInt(37);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 37;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Machado", McMMOApi.getXP(UUID + "Machado") + valor);
                verifyLevelUp(UUID, player, valor);
                return;
            }
            if (event.getEntityType().equals(EntityType.MAGMA_CUBE)) {
                int valor = random.nextInt(25);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 29;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Machado", McMMOApi.getXP(UUID + "Machado") + valor);
                verifyLevelUp(UUID, player, valor);
                return;
            }
            if (event.getEntityType().equals(EntityType.WITCH)) {
                int valor = random.nextInt(4);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 8;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Machado", McMMOApi.getXP(UUID + "Machado") + valor);
                verifyLevelUp(UUID, player, valor);
                return;
            }
            if (event.getEntityType().equals(EntityType.WITHER_SKELETON)) {
                int valor = random.nextInt(3);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 12;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Machado", McMMOApi.getXP(UUID + "Machado") + valor);
                verifyLevelUp(UUID, player, valor);
                return;
            }
            if (event.getEntityType().equals(EntityType.PIG_ZOMBIE)) {
                int valor = random.nextInt(3);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 10;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Machado", McMMOApi.getXP(UUID + "Machado") + valor);
                verifyLevelUp(UUID, player, valor);
                return;
            }
            if (event.getEntityType().equals(EntityType.SLIME)) {
                int valor = random.nextInt(6);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 11;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Machado", McMMOApi.getXP(UUID + "Machado") + valor);
                verifyLevelUp(UUID, player, valor);
                return;
            }
            if (event.getEntityType().equals(EntityType.ENDERMAN)) {
                int valor = random.nextInt(8);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 10;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Machado", McMMOApi.getXP(UUID + "Machado") + valor);
                verifyLevelUp(UUID, player, valor);
                return;
            }
            if (event.getEntityType().equals(EntityType.CREEPER)) {
                int valor = random.nextInt(3);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 9;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Machado", McMMOApi.getXP(UUID + "Machado") + valor);
                verifyLevelUp(UUID, player, valor);
                return;
            }
            if (event.getEntityType().equals(EntityType.SPIDER)) {
                int valor = random.nextInt(4);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 4;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Machado", McMMOApi.getXP(UUID + "Machado") + valor);
                verifyLevelUp(UUID, player, valor);
                return;
            }
            if (event.getEntityType().equals(EntityType.SKELETON)) {
                int valor = random.nextInt(3);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 6;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Machado", McMMOApi.getXP(UUID + "Machado") + valor);
                verifyLevelUp(UUID, player, valor);
                return;
            }
            if (event.getEntityType().equals(EntityType.ZOMBIE)) {
                int valor = random.nextInt(3);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 5;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Machado", McMMOApi.getXP(UUID + "Machado") + valor);
                verifyLevelUp(UUID, player, valor);
                return;
            }
        }
    }

    public static void verifyLevelUp(String UUID, Player player, int valor) {
        if (McMMOApi.getProximoNivelXP(UUID + "Machado") <= McMMOApi.getXP(UUID + "Machado")) {
            McMMOApi.updateLevel(UUID + "Machado");

            int nivel = McMMOApi.getNivel(UUID + "Machado");
            if (nivel == 100 || nivel == 200 || nivel == 300 || nivel == 400 || nivel == 500 || nivel == 600 || nivel == 700 || nivel == 800) {
                for (final Player target : Bukkit.getOnlinePlayers()) {
                    if (nivel != 800) {
                        target.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (nivel) + "&e na habilidade de Machado&e!\n \n"));
                        if (target.getName().equals(player.getName())) {
                            target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                        } else {
                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        target.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade de Machado!\n \n"));
                        if (target.getName().equals(player.getName())) {
                            target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                            target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                        } else {
                            target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                        }
                    }
                }
            } else {
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (nivel)) + ChatColor.YELLOW + " na habilidade de Machado!");
            }
        } else {
            player.sendActionBar(ChatColor.GREEN + "Machado (" + (McMMOApi.getXP(UUID + "Machado")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Machado")) + ")XP +" + (10 + valor) + "XP");
        }
    }
}
