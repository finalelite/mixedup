package com.mixedup.mcmmo.skills;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.mcmmo.McMMOApi;
import com.mixedup.mcmmo.utils.BoosterHash;
import com.mixedup.utils.AlternateColor;
import com.mixedup.utils.RemoveItemInv;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Date;

public class ReparacaoEvent implements Listener {

    public static void verifyLevelUp(final Player player, final int xpAnterior, int newXp) {
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (BoosterHash.get(UUID) != null) {
            if (BoosterHash.get(UUID).gettime() - (new Date().getTime() / 1000) <= 0) {
                BoosterHash.CACHE.remove(UUID);
            }
        }

        if (McMMOApi.getProximoNivelXP(UUID + "Reparacao") <= McMMOApi.getXP(UUID + "Reparacao")) {
            McMMOApi.updateLevel(UUID + "Reparacao");

            if (McMMOApi.getNivel(UUID + "Reparacao") == 100 || McMMOApi.getNivel(UUID + "Reparacao") == 200 || McMMOApi.getNivel(UUID + "Reparacao") == 300 || McMMOApi.getNivel(UUID + "Reparacao") == 400 || McMMOApi.getNivel(UUID + "Reparacao") == 500) {
                for (final Player target : Bukkit.getOnlinePlayers()) {
                    if (McMMOApi.getNivel(UUID + "Reparacao") != 500) {
                        target.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (McMMOApi.getNivel(UUID + "Reparacao")) + "&e na habilidade de Reparação!&e!\n \n"));
                        if (target.getName().equals(player.getName())) {
                            target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                        } else {
                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        target.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade de Reparação!\n \n"));
                        if (target.getName().equals(player.getName())) {
                            target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                            target.playSound(target.getLocation().add(10, 0, 10), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                        } else {
                            target.playSound(target.getLocation().add(10, 0, 10), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                        }
                    }
                }
            } else {
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (McMMOApi.getNivel(UUID + "Reparacao"))) + ChatColor.YELLOW + " na habilidade de Reparação!");
            }
        } else {

            player.sendActionBar(ChatColor.GREEN + "Reparação (" + (McMMOApi.getXP(UUID + "Reparacao")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Reparacao")) + ")XP +" + (newXp) + "XP");

        }
    }

    @EventHandler
    public void onRepair(final PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
            if (event.getClickedBlock().getType().equals(Material.ANVIL)) {
                return;
            }
        }

        if (event.getClickedBlock() != null) {
            if (event.getClickedBlock().getType().equals(Material.ANVIL) || event.getClickedBlock().getType().equals(Material.CHIPPED_ANVIL) || event.getClickedBlock().getType().equals(Material.DAMAGED_ANVIL)) {
                event.setCancelled(true);

                if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
                    return;
                }
                if (player.getItemInHand() == null) {
                    return;
                }
                if (!player.getItemInHand().getType().equals(Material.LEATHER_BOOTS) && !player.getItemInHand().getType().equals(Material.LEATHER_CHESTPLATE) && !player.getItemInHand().getType().equals(Material.LEATHER_HELMET) && !player.getItemInHand().getType().equals(Material.LEATHER_LEGGINGS)
                        && !player.getItemInHand().getType().equals(Material.WOODEN_SWORD) && !player.getItemInHand().getType().equals(Material.WOODEN_SHOVEL) && !player.getItemInHand().getType().equals(Material.WOODEN_AXE) && !player.getItemInHand().getType().equals(Material.WOODEN_PICKAXE)
                        && !player.getItemInHand().getType().equals(Material.WOODEN_HOE) && !player.getItemInHand().getType().equals(Material.GOLDEN_BOOTS) && !player.getItemInHand().getType().equals(Material.GOLDEN_CHESTPLATE) && !player.getItemInHand().getType().equals(Material.GOLDEN_HELMET)
                        && !player.getItemInHand().getType().equals(Material.GOLDEN_HELMET) && !player.getItemInHand().getType().equals(Material.GOLDEN_LEGGINGS) && !player.getItemInHand().getType().equals(Material.GOLDEN_SWORD) && !player.getItemInHand().getType().equals(Material.GOLDEN_SHOVEL)
                        && !player.getItemInHand().getType().equals(Material.GOLDEN_AXE) && !player.getItemInHand().getType().equals(Material.GOLDEN_PICKAXE) && !player.getItemInHand().getType().equals(Material.IRON_LEGGINGS) && !player.getItemInHand().getType().equals(Material.IRON_SWORD)
                        && !player.getItemInHand().getType().equals(Material.IRON_SHOVEL) && !player.getItemInHand().getType().equals(Material.IRON_AXE) && !player.getItemInHand().getType().equals(Material.IRON_PICKAXE) && !player.getItemInHand().getType().equals(Material.IRON_HOE)
                        && !player.getItemInHand().getType().equals(Material.DIAMOND_BOOTS) && !player.getItemInHand().getType().equals(Material.DIAMOND_CHESTPLATE) && !player.getItemInHand().getType().equals(Material.DIAMOND_HELMET) && !player.getItemInHand().getType().equals(Material.DIAMOND_LEGGINGS)
                        && !player.getItemInHand().getType().equals(Material.DIAMOND_SWORD) && !player.getItemInHand().getType().equals(Material.DIAMOND_SHOVEL) && !player.getItemInHand().getType().equals(Material.DIAMOND_AXE) && !player.getItemInHand().getType().equals(Material.DIAMOND_PICKAXE)
                        && !player.getItemInHand().getType().equals(Material.DIAMOND_HOE) && !player.getItemInHand().getType().equals(Material.STONE_PICKAXE) && !player.getItemInHand().getType().equals(Material.STONE_HOE) && !player.getItemInHand().getType().equals(Material.STONE_SWORD)
                        && !player.getItemInHand().getType().equals(Material.STONE_AXE) && !player.getItemInHand().getType().equals(Material.STONE_SHOVEL) && !player.getItemInHand().getType().equals(Material.BOW)) {
                    return;
                }
                if (McMMOApi.exists(UUID + "Reparacao")) {
                    McMMOApi.createInfo(UUID + "Reparacao");
                }

                final ItemStack it = player.getItemInHand();
                if (it.getDurability() <= 0) {
                    return;
                }

                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).gettime() - (new Date().getTime() / 1000) <= 0) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }

                if (player.getItemInHand().getType().equals(Material.BOW)) {
                    boolean contains = false;
                    int slot = 0;
                    for (int i = 1; i <= player.getInventory().getSize(); i++) {
                        if (contains == false) {
                            if (player.getInventory().getItem(i - 1) != null && player.getInventory().getItem(i - 1).getType().equals(Material.STRING)) {
                                contains = true;
                                slot = i - 1;
                            }
                        }
                        if (i == player.getInventory().getSize()) {
                            if (contains == true) {
                                if (McMMOApi.getNivel(UUID + "Reparacao") == 500) {
                                    final ItemStack item = player.getItemInHand();
                                    final short durabity = item.getType().getMaxDurability();
                                    item.setDurability((short) (durabity - durabity));
                                    RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.STRING), 1);
                                    player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.0f);
                                } else {
                                    final ItemStack item = player.getItemInHand();
                                    final short durabity = item.getType().getMaxDurability();
                                    item.setDurability((short) (item.getDurability() - (durabity / 4)));
                                    RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.STRING), 1);
                                    player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.0f);

                                    final int xpAnterior = McMMOApi.getXP(UUID + "Reparacao");
                                    int newXp = 30;
                                    if (BoosterHash.get(UUID) != null) {
                                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                            newXp += (newXp * 25) / 100;
                                        } else {
                                            newXp += (newXp * 50) / 100;
                                        }

                                        if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                            BoosterHash.CACHE.remove(UUID);
                                        }
                                    }
                                    McMMOApi.updateXP(UUID + "Reparacao", xpAnterior + newXp);
                                    ReparacaoEvent.verifyLevelUp(player, xpAnterior, newXp);
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, você não contém linha para efetuar está reparação!");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        }
                    }
                }

                if (player.getItemInHand().getType().equals(Material.DIAMOND_HELMET) || player.getItemInHand().getType().equals(Material.DIAMOND_CHESTPLATE) || player.getItemInHand().getType().equals(Material.DIAMOND_LEGGINGS) || player.getItemInHand().getType().equals(Material.DIAMOND_BOOTS)) {
                    if (McMMOApi.getNivel(UUID + "Reparacao") >= 300) {
                        boolean contains = false;
                        int slot = 0;
                        for (int i = 1; i <= player.getInventory().getSize(); i++) {
                            if (contains == false) {
                                if (player.getInventory().getItem(i - 1) != null && player.getInventory().getItem(i - 1).getType().equals(Material.DIAMOND)) {
                                    contains = true;
                                    slot = i - 1;
                                }
                            }
                            if (i == player.getInventory().getSize()) {
                                if (contains == true) {
                                    if (McMMOApi.getNivel(UUID + "Reparacao") == 500) {
                                        final ItemStack item = player.getItemInHand();
                                        final short durabity = item.getType().getMaxDurability();
                                        item.setDurability((short) (durabity - durabity));
                                        RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.LEATHER), 1);
                                        player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.0f);
                                    } else {
                                        final ItemStack item = player.getItemInHand();
                                        final short durabity = item.getType().getMaxDurability();
                                        item.setDurability((short) (item.getDurability() - (durabity / 4)));
                                        RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.LEATHER), 1);
                                        player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.0f);

                                        final int xpAnterior = McMMOApi.getXP(UUID + "Reparacao");
                                        int newXp = 900;
                                        if (BoosterHash.get(UUID) != null) {
                                            if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                newXp += (newXp * 25) / 100;
                                            } else {
                                                newXp += (newXp * 50) / 100;
                                            }

                                            if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                BoosterHash.CACHE.remove(UUID);
                                            }
                                        }
                                        McMMOApi.updateXP(UUID + "Reparacao", xpAnterior + newXp);
                                        ReparacaoEvent.verifyLevelUp(player, xpAnterior, newXp);
                                    }
                                } else {
                                    player.sendMessage(ChatColor.RED + " * Ops, você não contém diamantes para efetuar está reparação!");
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            }
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Você precisa de level 300 em reparação para reparar armaduras de diamante.");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
                if (player.getItemInHand().getType().equals(Material.DIAMOND_SWORD) || player.getItemInHand().getType().equals(Material.DIAMOND_HOE) || player.getItemInHand().getType().equals(Material.DIAMOND_PICKAXE) || player.getItemInHand().getType().equals(Material.DIAMOND_AXE) || player.getItemInHand().getType().equals(Material.DIAMOND_SHOVEL)) {
                    if (McMMOApi.getNivel(UUID + "Reparacao") >= 300) {
                        boolean contains = false;
                        int slot = 0;
                        for (int i = 1; i <= player.getInventory().getSize(); i++) {
                            if (contains == false) {
                                if (player.getInventory().getItem(i - 1) != null && player.getInventory().getItem(i - 1).getType().equals(Material.DIAMOND)) {
                                    contains = true;
                                    slot = i - 1;
                                }
                            }
                            if (i == player.getInventory().getSize()) {
                                if (contains == true) {
                                    if (McMMOApi.getNivel(UUID + "Reparacao") == 500) {
                                        final ItemStack item = player.getItemInHand();
                                        final short durabity = item.getType().getMaxDurability();
                                        item.setDurability((short) (durabity - durabity));
                                        RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.LEATHER), 1);
                                        player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.0f);
                                    } else {
                                        final ItemStack item = player.getItemInHand();
                                        final short durabity = item.getType().getMaxDurability();
                                        item.setDurability((short) (item.getDurability() - (durabity / 4)));
                                        RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.LEATHER), 1);
                                        player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.0f);

                                        final int xpAnterior = McMMOApi.getXP(UUID + "Reparacao");
                                        if (player.getItemInHand().getType().equals(Material.DIAMOND_SWORD) || player.getItemInHand().getType().equals(Material.DIAMOND_HOE)) {
                                            int newXp = 150;
                                            if (BoosterHash.get(UUID) != null) {
                                                if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                    newXp += (newXp * 25) / 100;
                                                } else {
                                                    newXp += (newXp * 50) / 100;
                                                }

                                                if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                    BoosterHash.CACHE.remove(UUID);
                                                }
                                            }
                                            McMMOApi.updateXP(UUID + "Reparacao", xpAnterior + newXp);
                                            ReparacaoEvent.verifyLevelUp(player, xpAnterior, newXp);
                                        }
                                        if (player.getItemInHand().getType().equals(Material.DIAMOND_AXE) || player.getItemInHand().getType().equals(Material.DIAMOND_PICKAXE)) {
                                            int newXp = 300;
                                            if (BoosterHash.get(UUID) != null) {
                                                if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                    newXp += (newXp * 25) / 100;
                                                } else {
                                                    newXp += (newXp * 50) / 100;
                                                }

                                                if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                    BoosterHash.CACHE.remove(UUID);
                                                }
                                            }
                                            McMMOApi.updateXP(UUID + "Reparacao", xpAnterior + newXp);
                                            ReparacaoEvent.verifyLevelUp(player, xpAnterior, newXp);
                                        }
                                        if (player.getItemInHand().getType().equals(Material.DIAMOND_SHOVEL)) {
                                            int newXp = 90;
                                            if (BoosterHash.get(UUID) != null) {
                                                if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                    newXp += (newXp * 25) / 100;
                                                } else {
                                                    newXp += (newXp * 50) / 100;
                                                }

                                                if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                    BoosterHash.CACHE.remove(UUID);
                                                }
                                            }
                                            McMMOApi.updateXP(UUID + "Reparacao", xpAnterior + newXp);
                                            ReparacaoEvent.verifyLevelUp(player, xpAnterior, newXp);
                                        }
                                    }
                                } else {
                                    player.sendMessage(ChatColor.RED + " * Ops, você não contém diamantes para efetuar está reparação!");
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            }
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Você precisa de level 300 em reparação para reparar armaduras de diamante.");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
                //-
                if (player.getItemInHand().getType().equals(Material.IRON_HELMET) || player.getItemInHand().getType().equals(Material.IRON_CHESTPLATE) || player.getItemInHand().getType().equals(Material.IRON_LEGGINGS) || player.getItemInHand().getType().equals(Material.IRON_BOOTS)) {
                    if (McMMOApi.getNivel(UUID + "Reparacao") >= 150) {
                        boolean contains = false;
                        int slot = 0;
                        for (int i = 1; i <= player.getInventory().getSize(); i++) {
                            if (contains == false) {
                                if (player.getInventory().getItem(i - 1) != null && player.getInventory().getItem(i - 1).getType().equals(Material.IRON_INGOT)) {
                                    contains = true;
                                    slot = i - 1;
                                }
                            }
                            if (i == player.getInventory().getSize()) {
                                if (contains == true) {
                                    if (McMMOApi.getNivel(UUID + "Reparacao") == 500) {
                                        final ItemStack item = player.getItemInHand();
                                        final short durabity = item.getType().getMaxDurability();
                                        item.setDurability((short) (durabity - durabity));
                                        RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.IRON_INGOT), 1);
                                        player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.0f);
                                    } else {
                                        final ItemStack item = player.getItemInHand();
                                        final short durabity = item.getType().getMaxDurability();
                                        item.setDurability((short) (item.getDurability() - (durabity / 4)));
                                        RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.IRON_INGOT), 1);
                                        player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.0f);

                                        final int xpAnterior = McMMOApi.getXP(UUID + "Reparacao");
                                        int newXp = 400;
                                        if (BoosterHash.get(UUID) != null) {
                                            if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                newXp += (newXp * 25) / 100;
                                            } else {
                                                newXp += (newXp * 50) / 100;
                                            }

                                            if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                BoosterHash.CACHE.remove(UUID);
                                            }
                                        }
                                        McMMOApi.updateXP(UUID + "Reparacao", xpAnterior + newXp);
                                        ReparacaoEvent.verifyLevelUp(player, xpAnterior, newXp);
                                    }
                                } else {
                                    player.sendMessage(ChatColor.RED + " * Ops, você não contém barras de ferro para efetuar está reparação!");
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            }
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Você precisa de level 150 em reparação para reparar armaduras de ferro.");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
                if (player.getItemInHand().getType().equals(Material.IRON_SWORD) || player.getItemInHand().getType().equals(Material.IRON_HOE) || player.getItemInHand().getType().equals(Material.IRON_PICKAXE) || player.getItemInHand().getType().equals(Material.IRON_AXE) || player.getItemInHand().getType().equals(Material.IRON_SHOVEL)) {
                    if (McMMOApi.getNivel(UUID + "Reparacao") >= 150) {
                        boolean contains = false;
                        int slot = 0;
                        for (int i = 1; i <= player.getInventory().getSize(); i++) {
                            if (contains == false) {
                                if (player.getInventory().getItem(i - 1) != null && player.getInventory().getItem(i - 1).getType().equals(Material.IRON_INGOT)) {
                                    contains = true;
                                    slot = i - 1;
                                }
                            }
                            if (i == player.getInventory().getSize()) {
                                if (contains == true) {
                                    if (McMMOApi.getNivel(UUID + "Reparacao") == 500) {
                                        final ItemStack item = player.getItemInHand();
                                        final short durabity = item.getType().getMaxDurability();
                                        item.setDurability((short) (durabity - durabity));
                                        RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.IRON_INGOT), 1);
                                        player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.0f);
                                    } else {
                                        final ItemStack item = player.getItemInHand();
                                        final short durabity = item.getType().getMaxDurability();
                                        item.setDurability((short) (item.getDurability() - (durabity / 4)));
                                        RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.IRON_INGOT), 1);
                                        player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.0f);

                                        final int xpAnterior = McMMOApi.getXP(UUID + "Reparacao");
                                        if (player.getItemInHand().getType().equals(Material.IRON_SWORD) || player.getItemInHand().getType().equals(Material.IRON_HOE)) {
                                            int newXp = 100;
                                            if (BoosterHash.get(UUID) != null) {
                                                if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                    newXp += (newXp * 25) / 100;
                                                } else {
                                                    newXp += (newXp * 50) / 100;
                                                }

                                                if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                    BoosterHash.CACHE.remove(UUID);
                                                }
                                            }
                                            McMMOApi.updateXP(UUID + "Reparacao", xpAnterior + newXp);
                                            ReparacaoEvent.verifyLevelUp(player, xpAnterior, newXp);
                                        }
                                        if (player.getItemInHand().getType().equals(Material.IRON_AXE) || player.getItemInHand().getType().equals(Material.IRON_PICKAXE)) {
                                            int newXp = 200;
                                            if (BoosterHash.get(UUID) != null) {
                                                if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                    newXp += (newXp * 25) / 100;
                                                } else {
                                                    newXp += (newXp * 50) / 100;
                                                }

                                                if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                    BoosterHash.CACHE.remove(UUID);
                                                }
                                            }
                                            McMMOApi.updateXP(UUID + "Reparacao", xpAnterior + newXp);
                                            ReparacaoEvent.verifyLevelUp(player, xpAnterior, newXp);
                                        }
                                        if (player.getItemInHand().getType().equals(Material.IRON_SHOVEL)) {
                                            int newXp = 60;
                                            if (BoosterHash.get(UUID) != null) {
                                                if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                    newXp += (newXp * 25) / 100;
                                                } else {
                                                    newXp += (newXp * 50) / 100;
                                                }

                                                if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                    BoosterHash.CACHE.remove(UUID);
                                                }
                                            }
                                            McMMOApi.updateXP(UUID + "Reparacao", xpAnterior + newXp);
                                            ReparacaoEvent.verifyLevelUp(player, xpAnterior, newXp);
                                        }
                                    }
                                } else {
                                    player.sendMessage(ChatColor.RED + " * Ops, você não contém barras de ferro para efetuar está reparação!");
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            }
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Você precisa de level 150 em reparação para reparar armaduras de ferro.");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
                //-
                if (player.getItemInHand().getType().equals(Material.GOLDEN_HELMET) || player.getItemInHand().getType().equals(Material.GOLDEN_CHESTPLATE) || player.getItemInHand().getType().equals(Material.GOLDEN_LEGGINGS) || player.getItemInHand().getType().equals(Material.GOLDEN_BOOTS)) {
                    if (McMMOApi.getNivel(UUID + "Reparacao") >= 50) {
                        boolean contains = false;
                        int slot = 0;
                        for (int i = 1; i <= player.getInventory().getSize(); i++) {
                            if (contains == false) {
                                if (player.getInventory().getItem(i - 1) != null && player.getInventory().getItem(i - 1).getType().equals(Material.GOLD_INGOT)) {
                                    contains = true;
                                    slot = i - 1;
                                }
                            }
                            if (i == player.getInventory().getSize()) {
                                if (contains == true) {
                                    if (McMMOApi.getNivel(UUID + "Reparacao") == 500) {
                                        final ItemStack item = player.getItemInHand();
                                        final short durabity = item.getType().getMaxDurability();
                                        item.setDurability((short) (durabity - durabity));
                                        RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.GOLD_INGOT), 1);
                                        player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.0f);
                                    } else {
                                        final ItemStack item = player.getItemInHand();
                                        final short durabity = item.getType().getMaxDurability();
                                        item.setDurability((short) (item.getDurability() - (durabity / 4)));
                                        RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.GOLD_INGOT), 1);
                                        player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.0f);

                                        final int xpAnterior = McMMOApi.getXP(UUID + "Reparacao");
                                        int newXp = 200;
                                        if (BoosterHash.get(UUID) != null) {
                                            if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                newXp += (newXp * 25) / 100;
                                            } else {
                                                newXp += (newXp * 50) / 100;
                                            }

                                            if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                BoosterHash.CACHE.remove(UUID);
                                            }
                                        }
                                        McMMOApi.updateXP(UUID + "Reparacao", xpAnterior + newXp);
                                        ReparacaoEvent.verifyLevelUp(player, xpAnterior, newXp);
                                    }
                                } else {
                                    player.sendMessage(ChatColor.RED + " * Ops, você não contém barras de ouro para efetuar está reparação!");
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            }
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Você precisa de level 50 em reparação para reparar armaduras de ouro.");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
                if (player.getItemInHand().getType().equals(Material.GOLDEN_SWORD) || player.getItemInHand().getType().equals(Material.GOLDEN_HOE) || player.getItemInHand().getType().equals(Material.GOLDEN_PICKAXE) || player.getItemInHand().getType().equals(Material.GOLDEN_AXE) || player.getItemInHand().getType().equals(Material.GOLDEN_SHOVEL)) {
                    if (McMMOApi.getNivel(UUID + "Reparacao") >= 50) {
                        boolean contains = false;
                        int slot = 0;
                        for (int i = 1; i <= player.getInventory().getSize(); i++) {
                            if (contains == false) {
                                if (player.getInventory().getItem(i - 1) != null && player.getInventory().getItem(i - 1).getType().equals(Material.GOLD_INGOT)) {
                                    contains = true;
                                    slot = i - 1;
                                }
                            }
                            if (i == player.getInventory().getSize()) {
                                if (contains == true) {
                                    if (McMMOApi.getNivel(UUID + "Reparacao") == 500) {
                                        final ItemStack item = player.getItemInHand();
                                        final short durabity = item.getType().getMaxDurability();
                                        item.setDurability((short) (durabity - durabity));
                                        RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.GOLD_INGOT), 1);
                                        player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.0f);

                                    } else {
                                        final ItemStack item = player.getItemInHand();
                                        final short durabity = item.getType().getMaxDurability();
                                        item.setDurability((short) (item.getDurability() - (durabity / 4)));
                                        RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.GOLD_INGOT), 1);
                                        player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.0f);

                                        final int xpAnterior = McMMOApi.getXP(UUID + "Reparacao");
                                        if (player.getItemInHand().getType().equals(Material.GOLDEN_SWORD) || player.getItemInHand().getType().equals(Material.GOLDEN_HOE)) {
                                            int newXp = 125;
                                            if (BoosterHash.get(UUID) != null) {
                                                if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                    newXp += (newXp * 25) / 100;
                                                } else {
                                                    newXp += (newXp * 50) / 100;
                                                }

                                                if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                    BoosterHash.CACHE.remove(UUID);
                                                }
                                            }
                                            McMMOApi.updateXP(UUID + "Reparacao", xpAnterior + newXp);
                                            ReparacaoEvent.verifyLevelUp(player, xpAnterior, newXp);
                                        }
                                        if (player.getItemInHand().getType().equals(Material.GOLDEN_AXE) || player.getItemInHand().getType().equals(Material.GOLDEN_PICKAXE)) {
                                            int newXp = 250;
                                            if (BoosterHash.get(UUID) != null) {
                                                if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                    newXp += (newXp * 25) / 100;
                                                } else {
                                                    newXp += (newXp * 50) / 100;
                                                }

                                                if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                    BoosterHash.CACHE.remove(UUID);
                                                }
                                            }
                                            McMMOApi.updateXP(UUID + "Reparacao", xpAnterior + newXp);
                                            ReparacaoEvent.verifyLevelUp(player, xpAnterior, newXp);
                                        }
                                        if (player.getItemInHand().getType().equals(Material.GOLDEN_SHOVEL)) {
                                            int newXp = 80;
                                            if (BoosterHash.get(UUID) != null) {
                                                if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                    newXp += (newXp * 25) / 100;
                                                } else {
                                                    newXp += (newXp * 50) / 100;
                                                }

                                                if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                    BoosterHash.CACHE.remove(UUID);
                                                }
                                            }
                                            McMMOApi.updateXP(UUID + "Reparacao", xpAnterior + newXp);
                                            ReparacaoEvent.verifyLevelUp(player, xpAnterior, newXp);
                                        }
                                    }
                                } else {
                                    player.sendMessage(ChatColor.RED + " * Você precisa de level 50 em reparação para reparar armaduras de ouro.");
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            }
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Você não contém nível para efetuar está reparação.");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
                if (player.getItemInHand().getType().equals(Material.STONE_SWORD) || player.getItemInHand().getType().equals(Material.STONE_HOE) || player.getItemInHand().getType().equals(Material.STONE_PICKAXE) || player.getItemInHand().getType().equals(Material.STONE_AXE) || player.getItemInHand().getType().equals(Material.STONE_SHOVEL)) {
                    boolean contains = false;
                    int slot = 0;
                    for (int i = 1; i <= player.getInventory().getSize(); i++) {
                        if (contains == false) {
                            if (player.getInventory().getItem(i - 1) != null && player.getInventory().getItem(i - 1).getType().equals(Material.COBBLESTONE)) {
                                contains = true;
                                slot = i - 1;
                            }
                        }
                        if (i == player.getInventory().getSize()) {
                            if (contains == true) {
                                if (McMMOApi.getNivel(UUID + "Reparacao") == 500) {
                                    final ItemStack item = player.getItemInHand();
                                    final short durabity = item.getType().getMaxDurability();
                                    item.setDurability((short) (durabity - durabity));
                                    RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.COBBLESTONE), 1);
                                    player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.0f);
                                } else {
                                    final ItemStack item = player.getItemInHand();
                                    final short durabity = item.getType().getMaxDurability();
                                    item.setDurability((short) (item.getDurability() - (durabity / 4)));
                                    RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.COBBLESTONE), 1);
                                    player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.0f);

                                    final int xpAnterior = McMMOApi.getXP(UUID + "Reparacao");
                                    if (player.getItemInHand().getType().equals(Material.STONE_SWORD) || player.getItemInHand().getType().equals(Material.STONE_HOE)) {
                                        int newXp = 15;
                                        if (BoosterHash.get(UUID) != null) {
                                            if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                newXp += (newXp * 25) / 100;
                                            } else {
                                                newXp += (newXp * 50) / 100;
                                            }

                                            if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                BoosterHash.CACHE.remove(UUID);
                                            }
                                        }
                                        McMMOApi.updateXP(UUID + "Reparacao", xpAnterior + newXp);
                                        ReparacaoEvent.verifyLevelUp(player, xpAnterior, newXp);
                                    }
                                    if (player.getItemInHand().getType().equals(Material.STONE_AXE) || player.getItemInHand().getType().equals(Material.STONE_PICKAXE)) {
                                        int newXp = 30;
                                        if (BoosterHash.get(UUID) != null) {
                                            if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                newXp += (newXp * 25) / 100;
                                            } else {
                                                newXp += (newXp * 50) / 100;
                                            }

                                            if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                BoosterHash.CACHE.remove(UUID);
                                            }
                                        }
                                        McMMOApi.updateXP(UUID + "Reparacao", xpAnterior + newXp);
                                        ReparacaoEvent.verifyLevelUp(player, xpAnterior, newXp);
                                    }
                                    if (player.getItemInHand().getType().equals(Material.STONE_SHOVEL)) {
                                        int newXp = 10;
                                        if (BoosterHash.get(UUID) != null) {
                                            if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                newXp += (newXp * 25) / 100;
                                            } else {
                                                newXp += (newXp * 50) / 100;
                                            }

                                            if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                BoosterHash.CACHE.remove(UUID);
                                            }
                                        }
                                        McMMOApi.updateXP(UUID + "Reparacao", xpAnterior + newXp);
                                        ReparacaoEvent.verifyLevelUp(player, xpAnterior, newXp);
                                    }
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, você não contém pedregulho para efetuar está reparação!");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        }
                    }
                }
                if (player.getItemInHand().getType().equals(Material.WOODEN_SWORD) || player.getItemInHand().getType().equals(Material.WOODEN_HOE) || player.getItemInHand().getType().equals(Material.WOODEN_PICKAXE) || player.getItemInHand().getType().equals(Material.WOODEN_AXE) || player.getItemInHand().getType().equals(Material.WOODEN_SHOVEL)) {
                    boolean contains = false;
                    int slot = 0;
                    for (int i = 1; i <= player.getInventory().getSize(); i++) {
                        if (contains == false) {
                            if (player.getInventory().getItem(i - 1) != null && player.getInventory().getItem(i - 1).getType().equals(Material.OAK_WOOD)) {
                                contains = true;
                                slot = i - 1;
                            }
                        }
                        if (i == player.getInventory().getSize()) {
                            if (contains == true) {
                                if (McMMOApi.getNivel(UUID + "Reparacao") == 500) {
                                    final ItemStack item = player.getItemInHand();
                                    final short durabity = item.getType().getMaxDurability();
                                    item.setDurability((short) (durabity - durabity));
                                    RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.LEATHER), 1);
                                    player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.0f);
                                } else {
                                    final ItemStack item = player.getItemInHand();
                                    final short durabity = item.getType().getMaxDurability();
                                    item.setDurability((short) (item.getDurability() - (durabity / 4)));
                                    RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.LEATHER), 1);
                                    player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.0f);

                                    final int xpAnterior = McMMOApi.getXP(UUID + "Reparacao");
                                    if (player.getItemInHand().getType().equals(Material.WOODEN_SWORD) || player.getItemInHand().getType().equals(Material.WOODEN_HOE)) {
                                        int newXp = 15;
                                        if (BoosterHash.get(UUID) != null) {
                                            if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                newXp += (newXp * 25) / 100;
                                            } else {
                                                newXp += (newXp * 50) / 100;
                                            }

                                            if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                BoosterHash.CACHE.remove(UUID);
                                            }
                                        }
                                        McMMOApi.updateXP(UUID + "Reparacao", xpAnterior + newXp);
                                        ReparacaoEvent.verifyLevelUp(player, xpAnterior, newXp);
                                    }
                                    if (player.getItemInHand().getType().equals(Material.WOODEN_AXE) || player.getItemInHand().getType().equals(Material.WOODEN_PICKAXE)) {
                                        int newXp = 30;
                                        if (BoosterHash.get(UUID) != null) {
                                            if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                newXp += (newXp * 25) / 100;
                                            } else {
                                                newXp += (newXp * 50) / 100;
                                            }

                                            if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                BoosterHash.CACHE.remove(UUID);
                                            }
                                        }
                                        McMMOApi.updateXP(UUID + "Reparacao", xpAnterior + newXp);
                                        ReparacaoEvent.verifyLevelUp(player, xpAnterior, newXp);
                                    }
                                    if (player.getItemInHand().getType().equals(Material.WOODEN_SHOVEL)) {
                                        int newXp = 10;
                                        if (BoosterHash.get(UUID) != null) {
                                            if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                                newXp += (newXp * 25) / 100;
                                            } else {
                                                newXp += (newXp * 50) / 100;
                                            }

                                            if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                                                BoosterHash.CACHE.remove(UUID);
                                            }
                                        }
                                        McMMOApi.updateXP(UUID + "Reparacao", xpAnterior + newXp);
                                        ReparacaoEvent.verifyLevelUp(player, xpAnterior, newXp);
                                    }
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, você não contém madeira para efetuar está reparação!");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        }
                    }
                }
                if (player.getItemInHand().getType().equals(Material.LEATHER_BOOTS) || player.getItemInHand().getType().equals(Material.LEATHER_CHESTPLATE) || player.getItemInHand().getType().equals(Material.LEATHER_HELMET) || player.getItemInHand().getType().equals(Material.LEATHER_LEGGINGS)) {
                    boolean contains = false;
                    int slot = 0;
                    for (int i = 1; i <= player.getInventory().getSize(); i++) {
                        if (contains == false) {
                            if (player.getInventory().getItem(i - 1) != null && player.getInventory().getItem(i - 1).getType().equals(Material.LEATHER)) {
                                contains = true;
                                slot = i - 1;
                            }
                        }
                        if (i == player.getInventory().getSize()) {
                            if (contains == true) {
                                if (McMMOApi.getNivel(UUID + "Reparacao") == 500) {
                                    final ItemStack item = player.getItemInHand();
                                    final short durabity = item.getType().getMaxDurability();
                                    item.setDurability((short) (durabity - durabity));
                                    RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.LEATHER), 1);
                                    player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.0f);
                                } else {
                                    final ItemStack item = player.getItemInHand();
                                    final short durabity = item.getType().getMaxDurability();
                                    item.setDurability((short) (item.getDurability() - (durabity / 4)));
                                    RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.LEATHER), 1);
                                    player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.0f);

                                    final int xpAnterior = McMMOApi.getXP(UUID + "Reparacao");
                                    int newXp = 60;
                                    if (BoosterHash.get(UUID) != null) {
                                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                            newXp += (newXp * 25) / 100;
                                        } else {
                                            newXp += (newXp * 50) / 100;
                                        }

                                    }
                                    McMMOApi.updateXP(UUID + "Reparacao", xpAnterior + newXp);
                                    ReparacaoEvent.verifyLevelUp(player, xpAnterior, newXp);
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, você não contém couro para efetuar está reparação!");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        }
                    }
                }
            }
        }

    }
}
