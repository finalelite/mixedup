package com.mixedup.mcmmo.skills;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.factions.hashs.NexusUtil;
import com.mixedup.mcmmo.McMMOApi;
import com.mixedup.mcmmo.ProbabilityApi;
import com.mixedup.mcmmo.utils.BoosterHash;
import com.mixedup.plots.espaconave.EspaçonaveUtil;
import com.mixedup.plots.ilha.IlhaUtil;
import com.mixedup.plots.terrain.TerrainsUtil;
import com.mixedup.utils.AlternateColor;
import com.mixedup.worldProtection.hashs.ProtectionUtil;
import org.bukkit.*;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;

import java.util.Date;

public class ArqueiroEvent implements Listener {

    @EventHandler
    public void onProjectileLaunch(final ProjectileLaunchEvent event) {
        final Projectile projectile = event.getEntity();
        final ProjectileSource source = projectile.getShooter();
        Player player = null;
        if (source instanceof Player) {
            player = (Player) source;
        } else {
            return;
        }
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (!(projectile instanceof Arrow)) return;

        if (ProtectionUtil.playerInArea(event.getEntity().getLocation()) != null) return;
        if (NexusUtil.playerInArea(event.getEntity().getLocation()) != null) return;

        if (event.getEntity().getWorld().getName().equalsIgnoreCase("aether") && event.getEntity().getWorld().getTime() <= 13000) {
            return;
        }

        if (event.getEntity().getLocation().getWorld().getName().equalsIgnoreCase("spawn") || event.getEntity().getLocation().getWorld().getName().equalsIgnoreCase("aether") || event.getEntity().getLocation().getWorld().getName().equalsIgnoreCase("guildaselect") || ProtectionUtil.playerInArea(event.getEntity().getLocation()) != null)
            return;

        if (BoosterHash.get(UUID) != null) {
            if (BoosterHash.get(UUID).gettime() - (new Date().getTime() / 1000) <= 0) {
                BoosterHash.CACHE.remove(UUID);
            }
        }

        if (McMMOApi.getNivel(UUID + "Arqueiro") < 150) {
            final int probability = 10000;
            if (ProbabilityApi.probab(probability) == true) {
                final ItemStack item = new ItemStack(Material.ARROW);
                player.getInventory().addItem(item);
            }
        } else if (McMMOApi.getNivel(UUID + "Arqueiro") >= 150 && McMMOApi.getNivel(UUID + "Arqueiro") < 300) {
            final int probability = 20000;
            if (ProbabilityApi.probab(probability) == true) {
                final ItemStack item = new ItemStack(Material.ARROW);
                player.getInventory().addItem(item);
            }
        } else if (McMMOApi.getNivel(UUID + "Arqueiro") >= 300 && McMMOApi.getNivel(UUID + "Arqueiro") < 450) {
            final int probability = 30000;
            if (ProbabilityApi.probab(probability) == true) {
                final ItemStack item = new ItemStack(Material.ARROW);
                player.getInventory().addItem(item);
            }
        } else if (McMMOApi.getNivel(UUID + "Arqueiro") >= 450 && McMMOApi.getNivel(UUID + "Arqueiro") <= 600) {
            final int probability = 40000;
            if (ProbabilityApi.probab(probability) == true) {
                final ItemStack item = new ItemStack(Material.ARROW);
                player.getInventory().addItem(item);
            }
        }

        final int probability = McMMOApi.getNivel(UUID + "Arqueiro") * 33;
        if (ProbabilityApi.probab(probability) == true) {
            if (McMMOApi.getNivel(UUID + "Mineracao") >= 300) {
                player.launchProjectile(Arrow.class);
                player.sendActionBar(ChatColor.RED + "Habilidade arqueiro: Flechas duplicadas");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            } else if (McMMOApi.getNivel(UUID + "Mineracao") < 300) {
                for (int i = 1; i < 3; i++) {
                    player.launchProjectile(Arrow.class);
                    player.sendActionBar(ChatColor.RED + "Habilidade arqueiro: Flechas triplicadas");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
    }

    @EventHandler
    public void entityDamage(final ProjectileHitEvent event) {

        if (ProtectionUtil.playerInArea(event.getEntity().getLocation()) != null) return;

        if (event.getEntity().getWorld().getName().equalsIgnoreCase("aether") && event.getEntity().getWorld().getTime() <= 13000) {
            return;
        }

        if (event.getEntity() instanceof Arrow) {
            final Arrow arrow = (Arrow) event.getEntity();
            final ProjectileSource shooter = arrow.getShooter();

            if (shooter instanceof Player) {
                final Player player = (Player) shooter;
                final String UUID = PlayerUUID.getUUID(player.getName());

                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).gettime() - (new Date().getTime() / 1000) <= 0) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }

                if (event.getHitEntity() instanceof Player) {
                    final Player target = (Player) event.getHitEntity();

                    if (TerrainsUtil.playerInArea(event.getEntity().getLocation()) != null) return;
                    if (IlhaUtil.playerInArea(event.getEntity().getLocation()) != null) return;
                    if (EspaçonaveUtil.playerInArea(event.getEntity().getLocation()) != null) return;

                    if (event.getEntity() instanceof Trident) return;

                    if (!player.getGameMode().equals(GameMode.CREATIVE) && McMMOApi.getNivel(UUID + "Arqueiro") != 600) {
                        final int xpAnterior = McMMOApi.getXP(UUID + "Arqueiro");
                        int xpNew2 = 2;
                        if (BoosterHash.get(UUID) != null) {
                            if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                xpNew2 += (xpNew2 * 25) / 100;
                            } else {
                                xpNew2 += (xpNew2 * 50) / 100;
                            }

                        }
                        McMMOApi.updateXP(UUID + "Arqueiro", McMMOApi.getXP(UUID + "Arqueiro") + xpNew2);

                        if (McMMOApi.getProximoNivelXP(UUID + "Arqueiro") <= McMMOApi.getXP(UUID + "Arqueiro")) {
                            McMMOApi.updateLevel(UUID + "Arqueiro");

                            if (McMMOApi.getNivel(UUID + "Arqueiro") == 100 || McMMOApi.getNivel(UUID + "Arqueiro") == 200 || McMMOApi.getNivel(UUID + "Arqueiro") == 300 || McMMOApi.getNivel(UUID + "Arqueiro") == 400 || McMMOApi.getNivel(UUID + "Arqueiro") == 500 || McMMOApi.getNivel(UUID + "Arqueiro") == 600) {
                                for (final Player online : Bukkit.getOnlinePlayers()) {
                                    if (McMMOApi.getNivel(UUID + "Arqueiro") != 600) {
                                        online.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (McMMOApi.getNivel(UUID + "Arqueiro")) + "&e na habilidade de Arqueiro&e!\n \n"));
                                        if (online.getName().equals(player.getName())) {
                                            online.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                        } else {
                                            online.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        }
                                    } else {
                                        online.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade de Arqueiro!\n \n"));
                                        if (online.getName().equals(player.getName())) {
                                            online.playSound(online.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                            online.playSound(online.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                        } else {
                                            online.playSound(online.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                        }
                                    }
                                }
                            } else {
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                                player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (McMMOApi.getNivel(UUID + "Arqueiro"))) + ChatColor.YELLOW + " na habilidade de Arqueiro!");
                            }
                        } else {

                            player.sendActionBar(ChatColor.GREEN + "Arqueiro (" + (McMMOApi.getXP(UUID + "Arqueiro")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Arqueiro")) + ")XP +" + (xpNew2) + "XP");

                        }
                    }

                    if (McMMOApi.getNivel(UUID + "Arqueiro") == 600) {
                        final int probability = 5000;
                        if (ProbabilityApi.probab(probability) == true) {
                            target.getWorld().createExplosion(target.getLocation(), 1);
                        }
                    }

                    final int probability = McMMOApi.getNivel(UUID + "Arqueiro") * 33;
                    if (ProbabilityApi.probab(probability) == true) {
                        if (McMMOApi.getXP(UUID + "Arqueiro") < 150) {
                            player.damage(3);
                            return;
                        }
                        if (McMMOApi.getXP(UUID + "Arqueiro") >= 150 && McMMOApi.getXP(UUID + "Arqueiro") < 300) {
                            player.damage(4);
                            return;
                        }
                        if (McMMOApi.getXP(UUID + "Arqueiro") >= 300 && McMMOApi.getXP(UUID + "Arqueiro") < 450) {
                            player.damage(5);
                            return;
                        }
                        if (McMMOApi.getXP(UUID + "Arqueiro") >= 450 && McMMOApi.getXP(UUID + "Arqueiro") <= 600) {
                            player.damage(6);
                            return;
                        }
                    }
                } else {
                    if (McMMOApi.getNivel(UUID + "Arqueiro") == 600) {
                        return;
                    }

                    if (player.getGameMode().equals(GameMode.CREATIVE)) {
                        return;
                    }

                    if (event.getHitEntity() == null) return;
                    if (event.getHitEntity().getType().equals(EntityType.ZOMBIE) || event.getHitEntity().getType().equals(EntityType.SPIDER) || event.getHitEntity().getType().equals(EntityType.SKELETON) || event.getHitEntity().getType().equals(EntityType.CREEPER) ||
                            event.getHitEntity().getType().equals(EntityType.WITHER_SKELETON) || event.getHitEntity().getType().equals(EntityType.WITCH) || event.getHitEntity().getType().equals(EntityType.SLIME) || event.getHitEntity().getType().equals(EntityType.GHAST)
                            || event.getHitEntity().getType().equals(EntityType.BLAZE) || event.getHitEntity().getType().equals(EntityType.ENDERMAN) || event.getHitEntity().getType().equals(EntityType.PIG_ZOMBIE)
                            || event.getHitEntity().getType().equals(EntityType.PHANTOM) || event.getHitEntity().getType().equals(EntityType.DROWNED)) {
                        final int xpAnterior = McMMOApi.getXP(UUID + "Arqueiro");
                        int xpNew2 = 3;
                        if (BoosterHash.get(UUID) != null) {
                            if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                xpNew2 += (xpNew2 * 25) / 100;
                            } else {
                                xpNew2 += (xpNew2 * 50) / 100;
                            }

                        }
                        McMMOApi.updateXP(UUID + "Arqueiro", McMMOApi.getXP(UUID + "Arqueiro") + xpNew2);

                        if (McMMOApi.getProximoNivelXP(UUID + "Arqueiro") <= McMMOApi.getXP(UUID + "Arqueiro")) {
                            McMMOApi.updateLevel(UUID + "Arqueiro");

                            if (McMMOApi.getNivel(UUID + "Arqueiro") == 100 || McMMOApi.getNivel(UUID + "Arqueiro") == 200 || McMMOApi.getNivel(UUID + "Arqueiro") == 300 || McMMOApi.getNivel(UUID + "Arqueiro") == 400 || McMMOApi.getNivel(UUID + "Arqueiro") == 500 || McMMOApi.getNivel(UUID + "Arqueiro") == 600) {
                                for (final Player target : Bukkit.getOnlinePlayers()) {
                                    if (McMMOApi.getNivel(UUID + "Arqueiro") != 600) {
                                        target.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (McMMOApi.getNivel(UUID + "Arqueiro")) + "&e na habilidade de Arqueiro&e!\n \n"));
                                        if (target.getName().equals(player.getName())) {
                                            target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                        } else {
                                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        }
                                    } else {
                                        target.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade de Arqueiro!\n \n"));
                                        if (target.getName().equals(player.getName())) {
                                            target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                            target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                        } else {
                                            target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                        }
                                    }
                                }
                            } else {
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                                player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (McMMOApi.getNivel(UUID + "Arqueiro"))) + ChatColor.YELLOW + " na habilidade de Arqueiro!");
                            }
                        } else {

                            player.sendActionBar(ChatColor.GREEN + "Arqueiro (" + (McMMOApi.getXP(UUID + "Arqueiro")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Arqueiro")) + ")XP +" + (xpNew2) + "XP");

                        }
                    }

                    if (event.getHitEntity().getType().equals(EntityType.IRON_GOLEM) || event.getHitEntity().getType().equals(EntityType.MAGMA_CUBE)) {
                        final int xpAnterior = McMMOApi.getXP(UUID + "Arqueiro");
                        int xpNew2 = 4;
                        if (BoosterHash.get(UUID) != null) {
                            if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                xpNew2 += (xpNew2 * 25) / 100;
                            } else {
                                xpNew2 += (xpNew2 * 50) / 100;
                            }

                        }
                        McMMOApi.updateXP(UUID + "Arqueiro", McMMOApi.getXP(UUID + "Arqueiro") + xpNew2);

                        if (McMMOApi.getProximoNivelXP(UUID + "Arqueiro") <= McMMOApi.getXP(UUID + "Arqueiro")) {
                            McMMOApi.updateLevel(UUID + "Arqueiro");

                            if (McMMOApi.getNivel(UUID + "Arqueiro") == 100 || McMMOApi.getNivel(UUID + "Arqueiro") == 200 || McMMOApi.getNivel(UUID + "Arqueiro") == 300 || McMMOApi.getNivel(UUID + "Arqueiro") == 400 || McMMOApi.getNivel(UUID + "Arqueiro") == 500 || McMMOApi.getNivel(UUID + "Arqueiro") == 600) {
                                for (final Player target : Bukkit.getOnlinePlayers()) {
                                    if (McMMOApi.getNivel(UUID + "Arqueiro") != 600) {
                                        target.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (McMMOApi.getNivel(UUID + "Arqueiro")) + "&e na habilidade de Arqueiro&e!\n \n"));
                                        if (target.getName().equals(player.getName())) {
                                            target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                        } else {
                                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        }
                                    } else {
                                        target.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade de Arqueiro!\n \n"));
                                        if (target.getName().equals(player.getName())) {
                                            target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                            target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                        } else {
                                            target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                        }
                                    }
                                }
                            } else {
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                                player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (McMMOApi.getNivel(UUID + "Arqueiro"))) + ChatColor.YELLOW + " na habilidade de Arqueiro!");
                            }
                        } else {

                            player.sendActionBar(ChatColor.GREEN + "Arqueiro (" + (McMMOApi.getXP(UUID + "Arqueiro")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Arqueiro")) + ")XP +" + (xpNew2) + "XP");

                        }
                    }

                    if (event.getHitEntity().getType().equals(EntityType.COW) || event.getHitEntity().getType().equals(EntityType.CHICKEN) || event.getHitEntity().getType().equals(EntityType.PIG) || event.getHitEntity().getType().equals(EntityType.SHEEP) || event.getHitEntity().getType().equals(EntityType.RABBIT)) {
                        final int xpAnterior = McMMOApi.getXP(UUID + "Arqueiro");
                        int xpNew2 = 2;
                        if (BoosterHash.get(UUID) != null) {
                            if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                                xpNew2 += (xpNew2 * 25) / 100;
                            } else {
                                xpNew2 += (xpNew2 * 50) / 100;
                            }

                        }
                        McMMOApi.updateXP(UUID + "Arqueiro", McMMOApi.getXP(UUID + "Arqueiro") + xpNew2);

                        if (McMMOApi.getProximoNivelXP(UUID + "Arqueiro") <= McMMOApi.getXP(UUID + "Arqueiro")) {
                            McMMOApi.updateLevel(UUID + "Arqueiro");

                            if (McMMOApi.getNivel(UUID + "Arqueiro") == 100 || McMMOApi.getNivel(UUID + "Arqueiro") == 200 || McMMOApi.getNivel(UUID + "Arqueiro") == 300 || McMMOApi.getNivel(UUID + "Arqueiro") == 400 || McMMOApi.getNivel(UUID + "Arqueiro") == 500 || McMMOApi.getNivel(UUID + "Arqueiro") == 600) {
                                for (final Player target : Bukkit.getOnlinePlayers()) {
                                    if (McMMOApi.getNivel(UUID + "Arqueiro") != 600) {
                                        target.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (McMMOApi.getNivel(UUID + "Arqueiro")) + "&e na habilidade de Arqueiro&e!\n \n"));
                                        if (target.getName().equals(player.getName())) {
                                            target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                        } else {
                                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        }
                                    } else {
                                        target.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade de Arqueiro!\n \n"));
                                        if (target.getName().equals(player.getName())) {
                                            target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                            target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                        } else {
                                            target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                        }
                                    }
                                }
                            } else {
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                                player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (McMMOApi.getNivel(UUID + "Arqueiro"))) + ChatColor.YELLOW + " na habilidade de Arqueiro!");
                            }
                        } else {
                            player.sendActionBar(ChatColor.GREEN + "Arqueiro (" + (McMMOApi.getXP(UUID + "Arqueiro")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Arqueiro")) + ")XP +" + (xpNew2) + "XP");
                        }
                    }
                }
            }
        }
    }
}
