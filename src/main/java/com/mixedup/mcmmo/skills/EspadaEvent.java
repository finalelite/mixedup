package com.mixedup.mcmmo.skills;

import com.mixedup.Main;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.factions.hashs.NexusUtil;
import com.mixedup.mcmmo.McMMOApi;
import com.mixedup.mcmmo.ProbabilityApi;
import com.mixedup.mcmmo.utils.BoosterHash;
import com.mixedup.mcmmo.utils.ItIsBlooding;
import com.mixedup.utils.AlternateColor;
import com.mixedup.worldProtection.hashs.ProtectionUtil;
import org.bukkit.*;
import org.bukkit.Particle.DustOptions;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.potion.PotionEffectType;

import java.util.Date;
import java.util.List;
import java.util.Random;

public class EspadaEvent implements Listener {

    @EventHandler
    public void onKilled(final EntityDeathEvent event) {
        if (event.getEntity() instanceof Player) {
            final Player player = (Player) event.getEntity();
            final String UUID = PlayerUUID.getUUID(player.getName());

            if (ItIsBlooding.get(UUID) != null && ItIsBlooding.get(UUID).getStatus() == true) {
                ItIsBlooding.get(UUID).setStatus(false);
            }
        }
    }

    @EventHandler
    public void onDamage(final EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player) {
            final Player player = (Player) event.getEntity();
            final String UUID = PlayerUUID.getUUID(player.getName());

            if (event.getDamager() instanceof Player) {
                final int probability = McMMOApi.getNivel(UUID + "Espadas") * 30;

                if (ProbabilityApi.probab(probability) == true) {
                    final Player target = (Player) event.getDamager();

                    player.sendActionBar(ChatColor.RED + " * Você esquivou e contra atacou o inimigo!");
                    player.playSound(player.getLocation(), Sound.ENCHANT_THORNS_HIT, 1.0f, 1.0f);
                    target.playSound(target.getLocation(), Sound.ENCHANT_THORNS_HIT, 1.0f, 1.0f);
                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_HURT, 1.0f, 1.0f);
                    target.damage(event.getDamage() / 2);

                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onHit(final EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player) {
            final Player player = (Player) event.getDamager();
            final String UUID = PlayerUUID.getUUID(player.getName());

            if (event.getEntity().getLocation().getWorld().getName().equalsIgnoreCase("spawn") || event.getEntity().getLocation().getWorld().getName().equalsIgnoreCase("aether") || event.getEntity().getLocation().getWorld().getName().equalsIgnoreCase("guildaselect") || ProtectionUtil.playerInArea(event.getEntity().getLocation()) != null)
                return;

            if (player.getGameMode().equals(GameMode.CREATIVE)) {
                return;
            }

            if (!player.getItemInHand().getType().equals(Material.WOODEN_SWORD) && !player.getItemInHand().getType().equals(Material.STONE_SWORD) && !player.getItemInHand().getType().equals(Material.GOLDEN_SWORD) && !player.getItemInHand().getType().equals(Material.IRON_SWORD) && !player.getItemInHand().getType().equals(Material.DIAMOND_SWORD)) {
                return;
            }

            if (NexusUtil.playerInArea(event.getEntity().getLocation()) != null) return;

            if (BoosterHash.get(UUID) != null) {
                if (BoosterHash.get(UUID).gettime() - (new Date().getTime() / 1000) <= 0) {
                    BoosterHash.CACHE.remove(UUID);
                }
            }

            if (event.getEntity() instanceof Player) {
                final int probability = McMMOApi.getNivel(UUID + "Espadas") * 40;

                if (ProbabilityApi.probab(probability) == true) {
                    final Player target = (Player) event.getEntity();
                    final String UUIDtarget = PlayerUUID.getUUID(target.getName());

                    if (ItIsBlooding.get(UUIDtarget) == null) {
                        new ItIsBlooding(UUIDtarget, true).insert();

                        target.sendActionBar(ChatColor.RED + "O inimigo lhe causo sangramento!");
                        player.sendActionBar(ChatColor.RED + "Você feriu gravemente o inimigo!");
                        if (McMMOApi.getNivel(UUID + "Espadas") <= 200) {
                            final int rotacao = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    if (ItIsBlooding.get(UUID).getStatus() == true) {
                                        final Location loc = target.getLocation();
                                        final DustOptions dust = new DustOptions(Color.RED, 2);
                                        loc.getWorld().spawnParticle(Particle.REDSTONE, loc.getX(), loc.getY(), loc.getZ(), 1, 0, 0, 0, dust);

                                        final List<Entity> entitys = target.getNearbyEntities(20, 20, 20);
                                        for (int i = 1; i <= entitys.size(); i++) {
                                            if (entitys.get(i - 1) instanceof Player) {
                                                final Player pNext = (Player) entitys.get(i - 1);
                                                final Location loc1 = target.getLocation();
                                                final DustOptions dust1 = new DustOptions(Color.RED, 2);
                                                loc1.getWorld().spawnParticle(Particle.REDSTONE, loc1.getX(), loc1.getY(), loc1.getZ(), 1, 0, 0, 0, dust1);
                                            }
                                        }
                                        target.damage(1);
                                        target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_HURT, 1.0f, 1.0f);
                                    }
                                }
                            }, 0L, 20L);
                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    ItIsBlooding.get(UUIDtarget).setStatus(false);
                                    Bukkit.getScheduler().cancelTask(rotacao);
                                    target.sendActionBar(ChatColor.RED + "O sangramento estancou-se!");
                                }
                            }, 46L);
                        }
                        if (McMMOApi.getNivel(UUID + "Espadas") >= 200 && McMMOApi.getNivel(UUID + "Espadas") <= 400) {
                            final int rotacao = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    if (ItIsBlooding.get(UUIDtarget).getStatus() == true) {
                                        final Location loc = target.getLocation();
                                        final DustOptions dust = new DustOptions(Color.RED, 2);
                                        loc.getWorld().spawnParticle(Particle.REDSTONE, loc.getX(), loc.getY(), loc.getZ(), 1, 0, 0, 0, dust);

                                        final List<Entity> entitys = target.getNearbyEntities(20, 20, 20);
                                        for (int i = 1; i <= entitys.size(); i++) {
                                            if (entitys.get(i - 1) instanceof Player) {
                                                final Player pNext = (Player) entitys.get(i - 1);
                                                final Location loc1 = target.getLocation();
                                                final DustOptions dust1 = new DustOptions(Color.RED, 2);
                                                loc1.getWorld().spawnParticle(Particle.REDSTONE, loc1.getX(), loc1.getY(), loc1.getZ(), 1, 0, 0, 0, dust1);
                                            }
                                        }
                                        target.damage(1);
                                        target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_HURT, 1.0f, 1.0f);
                                    }
                                }
                            }, 0L, 20L);
                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    ItIsBlooding.get(UUIDtarget).setStatus(false);
                                    Bukkit.getScheduler().cancelTask(rotacao);
                                    target.sendActionBar(ChatColor.RED + "O sangramento estancou-se!");
                                }
                            }, 101L);
                        }
                        if (McMMOApi.getNivel(UUID + "Espadas") >= 400 && McMMOApi.getNivel(UUID + "Espadas") <= 600) {
                            final int rotacao = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    if (ItIsBlooding.get(UUIDtarget).getStatus() == true) {
                                        final Location loc = target.getLocation();
                                        final DustOptions dust = new DustOptions(Color.RED, 2);
                                        loc.getWorld().spawnParticle(Particle.REDSTONE, loc.getX(), loc.getY(), loc.getZ(), 1, 0, 0, 0, dust);

                                        final List<Entity> entitys = target.getNearbyEntities(20, 20, 20);
                                        for (int i = 1; i <= entitys.size(); i++) {
                                            if (entitys.get(i - 1) instanceof Player) {
                                                final Player pNext = (Player) entitys.get(i - 1);
                                                final Location loc1 = target.getLocation();
                                                final DustOptions dust1 = new DustOptions(Color.RED, 2);
                                                loc1.getWorld().spawnParticle(Particle.REDSTONE, loc1.getX(), loc1.getY(), loc1.getZ(), 1, 0, 0, 0, dust1);
                                            }
                                        }
                                        target.damage(1);
                                        target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_HURT, 1.0f, 1.0f);
                                    }
                                }
                            }, 0L, 20L);
                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    ItIsBlooding.get(UUIDtarget).setStatus(false);
                                    Bukkit.getScheduler().cancelTask(rotacao);
                                    target.sendActionBar(ChatColor.RED + "O sangramento estancou-se!");
                                }
                            }, 161L);
                        }
                        if (McMMOApi.getNivel(UUID + "Espadas") >= 600 && McMMOApi.getNivel(UUID + "Espadas") <= 800) {
                            final int rotacao = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    if (ItIsBlooding.get(UUIDtarget).getStatus() == true) {
                                        final Location loc = target.getLocation();
                                        final DustOptions dust = new DustOptions(Color.RED, 2);
                                        loc.getWorld().spawnParticle(Particle.REDSTONE, loc.getX(), loc.getY(), loc.getZ(), 1, 0, 0, 0, dust);

                                        final List<Entity> entitys = target.getNearbyEntities(20, 20, 20);
                                        for (int i = 1; i <= entitys.size(); i++) {
                                            if (entitys.get(i - 1) instanceof Player) {
                                                final Player pNext = (Player) entitys.get(i - 1);
                                                final Location loc1 = target.getLocation();
                                                final DustOptions dust1 = new DustOptions(Color.RED, 2);
                                                loc1.getWorld().spawnParticle(Particle.REDSTONE, loc1.getX(), loc1.getY(), loc1.getZ(), 1, 0, 0, 0, dust1);
                                            }
                                        }
                                        target.damage(1);
                                        target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_HURT, 1.0f, 1.0f);
                                    }
                                }
                            }, 0L, 20L);
                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    ItIsBlooding.get(UUIDtarget).setStatus(false);
                                    Bukkit.getScheduler().cancelTask(rotacao);
                                    target.sendActionBar(ChatColor.RED + "O sangramento estancou-se!");
                                }
                            }, 201L);
                        }
                    } else if (ItIsBlooding.get(UUIDtarget).getStatus() == false) {
                        ItIsBlooding.get(UUIDtarget).setStatus(true);

                        target.sendActionBar(ChatColor.RED + "O inimigo lhe causo sangramento!");
                        player.sendActionBar(ChatColor.RED + "Você feriu gravemente o inimigo!");

                        if (McMMOApi.getNivel(UUID + "Espadas") <= 200) {
                            final int rotacao = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    if (ItIsBlooding.get(UUIDtarget).getStatus() == true) {
                                        final Location loc = target.getLocation();
                                        final DustOptions dust = new DustOptions(Color.RED, 2);
                                        loc.getWorld().spawnParticle(Particle.REDSTONE, loc.getX(), loc.getY(), loc.getZ(), 1, 0, 0, 0, dust);

                                        final List<Entity> entitys = target.getNearbyEntities(20, 20, 20);
                                        for (int i = 1; i <= entitys.size(); i++) {
                                            if (entitys.get(i - 1) instanceof Player) {
                                                final Player pNext = (Player) entitys.get(i - 1);
                                                final Location loc1 = target.getLocation();
                                                final DustOptions dust1 = new DustOptions(Color.RED, 2);
                                                loc1.getWorld().spawnParticle(Particle.REDSTONE, loc1.getX(), loc1.getY(), loc1.getZ(), 1, 0, 0, 0, dust1);
                                            }
                                        }
                                        target.damage(1);
                                        target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_HURT, 1.0f, 1.0f);
                                    }
                                }
                            }, 0L, 20L);
                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    ItIsBlooding.get(UUIDtarget).setStatus(false);
                                    Bukkit.getScheduler().cancelTask(rotacao);
                                    target.sendActionBar(ChatColor.RED + "O sangramento estancou-se!");
                                }
                            }, 46L);
                        }
                        if (McMMOApi.getNivel(UUID + "Espadas") >= 200 && McMMOApi.getNivel(UUID + "Espadas") <= 400) {
                            final int rotacao = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    if (ItIsBlooding.get(UUIDtarget).getStatus() == true) {
                                        final Location loc = target.getLocation();
                                        final DustOptions dust = new DustOptions(Color.RED, 2);
                                        loc.getWorld().spawnParticle(Particle.REDSTONE, loc.getX(), loc.getY(), loc.getZ(), 1, 0, 0, 0, dust);

                                        final List<Entity> entitys = target.getNearbyEntities(20, 20, 20);
                                        for (int i = 1; i <= entitys.size(); i++) {
                                            if (entitys.get(i - 1) instanceof Player) {
                                                final Player pNext = (Player) entitys.get(i - 1);
                                                final Location loc1 = target.getLocation();
                                                final DustOptions dust1 = new DustOptions(Color.RED, 2);
                                                loc1.getWorld().spawnParticle(Particle.REDSTONE, loc1.getX(), loc1.getY(), loc1.getZ(), 1, 0, 0, 0, dust1);
                                            }
                                        }
                                        target.damage(1);
                                        target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_HURT, 1.0f, 1.0f);
                                    }
                                }
                            }, 0L, 20L);
                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    ItIsBlooding.get(UUIDtarget).setStatus(false);
                                    Bukkit.getScheduler().cancelTask(rotacao);
                                    target.sendActionBar(ChatColor.RED + "O sangramento estancou-se!");
                                }
                            }, 01L);
                        }
                        if (McMMOApi.getNivel(UUID + "Espadas") >= 400 && McMMOApi.getNivel(UUID + "Espadas") <= 600) {
                            final int rotacao = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    if (ItIsBlooding.get(UUIDtarget).getStatus() == true) {
                                        final Location loc = target.getLocation();
                                        final DustOptions dust = new DustOptions(Color.RED, 2);
                                        loc.getWorld().spawnParticle(Particle.REDSTONE, loc.getX(), loc.getY(), loc.getZ(), 1, 0, 0, 0, dust);

                                        final List<Entity> entitys = target.getNearbyEntities(20, 20, 20);
                                        for (int i = 1; i <= entitys.size(); i++) {
                                            if (entitys.get(i - 1) instanceof Player) {
                                                final Player pNext = (Player) entitys.get(i - 1);
                                                final Location loc1 = target.getLocation();
                                                final DustOptions dust1 = new DustOptions(Color.RED, 2);
                                                loc1.getWorld().spawnParticle(Particle.REDSTONE, loc1.getX(), loc1.getY(), loc1.getZ(), 1, 0, 0, 0, dust1);
                                            }
                                        }
                                        target.damage(1);
                                        target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_HURT, 1.0f, 1.0f);
                                    }
                                }
                            }, 0L, 20L);
                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    ItIsBlooding.get(UUIDtarget).setStatus(false);
                                    Bukkit.getScheduler().cancelTask(rotacao);
                                    target.sendActionBar(ChatColor.RED + "O sangramento estancou-se!");
                                }
                            }, 161L);
                        }
                        if (McMMOApi.getNivel(UUID + "Espadas") >= 600 && McMMOApi.getNivel(UUID + "Espadas") <= 800) {
                            final int rotacao = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    if (ItIsBlooding.get(UUIDtarget).getStatus() == true) {
                                        final Location loc = target.getLocation();
                                        final DustOptions dust = new DustOptions(Color.RED, 2);
                                        loc.getWorld().spawnParticle(Particle.REDSTONE, loc.getX(), loc.getY(), loc.getZ(), 1, 0, 0, 0, dust);

                                        final List<Entity> entitys = target.getNearbyEntities(20, 20, 20);
                                        for (int i = 1; i <= entitys.size(); i++) {
                                            if (entitys.get(i - 1) instanceof Player) {
                                                final Player pNext = (Player) entitys.get(i - 1);
                                                final Location loc1 = target.getLocation();
                                                final DustOptions dust1 = new DustOptions(Color.RED, 2);
                                                loc1.getWorld().spawnParticle(Particle.REDSTONE, loc1.getX(), loc1.getY(), loc1.getZ(), 1, 0, 0, 0, dust1);
                                            }
                                        }
                                        target.damage(1);
                                        target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_HURT, 1.0f, 1.0f);
                                    }
                                }
                            }, 0L, 20L);
                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    ItIsBlooding.get(UUIDtarget).setStatus(false);
                                    Bukkit.getScheduler().cancelTask(rotacao);
                                    target.sendActionBar(ChatColor.RED + "O sangramento estancou-se!");
                                }
                            }, 201L);
                        }
                    }
                }
            }

            if (McMMOApi.getNivel(UUID + "Espadas") == 800) {
                event.setCancelled(false);
                return;
            }

            final Random random = new Random();
            if (event.getEntityType().equals(EntityType.PHANTOM)) {
                int valor = random.nextInt(7);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 13;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Espadas", McMMOApi.getXP(UUID + "Espadas") + valor);

                if (McMMOApi.getProximoNivelXP(UUID + "Espadas") <= McMMOApi.getXP(UUID + "Espadas")) {
                    McMMOApi.updateLevel(UUID + "Espadas");

                    if (McMMOApi.getNivel(UUID + "Espadas") == 100 || McMMOApi.getNivel(UUID + "Espadas") == 200 || McMMOApi.getNivel(UUID + "Espadas") == 300 || McMMOApi.getNivel(UUID + "Espadas") == 400 || McMMOApi.getNivel(UUID + "Espadas") == 500 || McMMOApi.getNivel(UUID + "Espadas") == 600 || McMMOApi.getNivel(UUID + "Espadas") == 700 || McMMOApi.getNivel(UUID + "Espadas") == 800) {
                        for (final Player target : Bukkit.getOnlinePlayers()) {
                            if (McMMOApi.getNivel(UUID + "Espadas") != 800) {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (McMMOApi.getNivel(UUID + "Espadas")) + "&e na habilidade de Espadas&e!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                            } else {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade de Espadas!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                }
                            }
                        }
                    } else {
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                        player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (McMMOApi.getNivel(UUID + "Espadas"))) + ChatColor.YELLOW + " na habilidade de Espadas!");
                    }
                } else {

                    player.sendActionBar(ChatColor.GREEN + "Espadas (" + (McMMOApi.getXP(UUID + "Espadas")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Espadas")) + ")XP +" + (26 + valor) + "XP");

                }
            }
            if (event.getEntityType().equals(EntityType.DROWNED)) {
                int valor = random.nextInt(8);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 12;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Espadas", McMMOApi.getXP(UUID + "Espadas") + valor);

                if (McMMOApi.getProximoNivelXP(UUID + "Espadas") <= McMMOApi.getXP(UUID + "Espadas")) {
                    McMMOApi.updateLevel(UUID + "Espadas");

                    if (McMMOApi.getNivel(UUID + "Espadas") == 100 || McMMOApi.getNivel(UUID + "Espadas") == 200 || McMMOApi.getNivel(UUID + "Espadas") == 300 || McMMOApi.getNivel(UUID + "Espadas") == 400 || McMMOApi.getNivel(UUID + "Espadas") == 500 || McMMOApi.getNivel(UUID + "Espadas") == 600 || McMMOApi.getNivel(UUID + "Espadas") == 700 || McMMOApi.getNivel(UUID + "Espadas") == 800) {
                        for (final Player target : Bukkit.getOnlinePlayers()) {
                            if (McMMOApi.getNivel(UUID + "Espadas") != 800) {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (McMMOApi.getNivel(UUID + "Espadas")) + "&e na habilidade de Espadas&e!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                            } else {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade de Espadas!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                }
                            }
                        }
                    } else {
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                        player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (McMMOApi.getNivel(UUID + "Espadas"))) + ChatColor.YELLOW + " na habilidade de Espadas!");
                    }
                } else {

                    player.sendActionBar(ChatColor.GREEN + "Espadas (" + (McMMOApi.getXP(UUID + "Espadas")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Espadas")) + ")XP +" + (24 + valor) + "XP");

                }
            }
            if (event.getEntityType().equals(EntityType.GHAST)) {
                int valor = random.nextInt(8);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 11;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Espadas", McMMOApi.getXP(UUID + "Espadas") + valor);

                if (McMMOApi.getProximoNivelXP(UUID + "Espadas") <= McMMOApi.getXP(UUID + "Espadas")) {
                    McMMOApi.updateLevel(UUID + "Espadas");

                    if (McMMOApi.getNivel(UUID + "Espadas") == 100 || McMMOApi.getNivel(UUID + "Espadas") == 200 || McMMOApi.getNivel(UUID + "Espadas") == 300 || McMMOApi.getNivel(UUID + "Espadas") == 400 || McMMOApi.getNivel(UUID + "Espadas") == 500 || McMMOApi.getNivel(UUID + "Espadas") == 600 || McMMOApi.getNivel(UUID + "Espadas") == 700 || McMMOApi.getNivel(UUID + "Espadas") == 800) {
                        for (final Player target : Bukkit.getOnlinePlayers()) {
                            if (McMMOApi.getNivel(UUID + "Espadas") != 800) {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (McMMOApi.getNivel(UUID + "Espadas")) + "&e na habilidade de Espadas&e!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                            } else {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade de Espadas!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                }
                            }
                        }
                    } else {
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                        player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (McMMOApi.getNivel(UUID + "Espadas"))) + ChatColor.YELLOW + " na habilidade de Espadas!");
                    }
                } else {

                    player.sendActionBar(ChatColor.GREEN + "Espadas (" + (McMMOApi.getXP(UUID + "Espadas")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Espadas")) + ")XP +" + (22 + valor) + "XP");

                }
            }
            if (event.getEntityType().equals(EntityType.IRON_GOLEM)) {
                int valor = random.nextInt(37);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 37;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Espadas", McMMOApi.getXP(UUID + "Espadas") + valor);

                if (McMMOApi.getProximoNivelXP(UUID + "Espadas") <= McMMOApi.getXP(UUID + "Espadas")) {
                    McMMOApi.updateLevel(UUID + "Espadas");

                    if (McMMOApi.getNivel(UUID + "Espadas") == 100 || McMMOApi.getNivel(UUID + "Espadas") == 200 || McMMOApi.getNivel(UUID + "Espadas") == 300 || McMMOApi.getNivel(UUID + "Espadas") == 400 || McMMOApi.getNivel(UUID + "Espadas") == 500 || McMMOApi.getNivel(UUID + "Espadas") == 600 || McMMOApi.getNivel(UUID + "Espadas") == 700 || McMMOApi.getNivel(UUID + "Espadas") == 800) {
                        for (final Player target : Bukkit.getOnlinePlayers()) {
                            if (McMMOApi.getNivel(UUID + "Espadas") != 800) {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (McMMOApi.getNivel(UUID + "Espadas")) + "&e na habilidade de Espadas&e!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                            } else {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade de Espadas!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                }
                            }
                        }
                    } else {
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                        player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (McMMOApi.getNivel(UUID + "Espadas"))) + ChatColor.YELLOW + " na habilidade de Espadas!");
                    }
                } else {

                    player.sendActionBar(ChatColor.GREEN + "Espadas (" + (McMMOApi.getXP(UUID + "Espadas")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Espadas")) + ")XP +" + (75 + valor) + "XP");

                }
            }
            if (event.getEntityType().equals(EntityType.MAGMA_CUBE)) {
                int valor = random.nextInt(25);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 29;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Espadas", McMMOApi.getXP(UUID + "Espadas") + valor);

                if (McMMOApi.getProximoNivelXP(UUID + "Espadas") <= McMMOApi.getXP(UUID + "Espadas")) {
                    McMMOApi.updateLevel(UUID + "Espadas");

                    if (McMMOApi.getNivel(UUID + "Espadas") == 100 || McMMOApi.getNivel(UUID + "Espadas") == 200 || McMMOApi.getNivel(UUID + "Espadas") == 300 || McMMOApi.getNivel(UUID + "Espadas") == 400 || McMMOApi.getNivel(UUID + "Espadas") == 500 || McMMOApi.getNivel(UUID + "Espadas") == 600 || McMMOApi.getNivel(UUID + "Espadas") == 700 || McMMOApi.getNivel(UUID + "Espadas") == 800) {
                        for (final Player target : Bukkit.getOnlinePlayers()) {
                            if (McMMOApi.getNivel(UUID + "Espadas") != 800) {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (McMMOApi.getNivel(UUID + "Espadas")) + "&e na habilidade de Espadas&e!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                            } else {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade de Espadas!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                }
                            }
                        }
                    } else {
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                        player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (McMMOApi.getNivel(UUID + "Espadas"))) + ChatColor.YELLOW + " na habilidade de Espadas!");
                    }
                } else {

                    player.sendActionBar(ChatColor.GREEN + "Espadas (" + (McMMOApi.getXP(UUID + "Espadas")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Espadas")) + ")XP +" + (59 + valor) + "XP");

                }
            }
            if (event.getEntityType().equals(EntityType.WITCH)) {
                int valor = random.nextInt(4);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 8;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Espadas", McMMOApi.getXP(UUID + "Espadas") + valor);

                if (McMMOApi.getProximoNivelXP(UUID + "Espadas") <= McMMOApi.getXP(UUID + "Espadas")) {
                    McMMOApi.updateLevel(UUID + "Espadas");

                    if (McMMOApi.getNivel(UUID + "Espadas") == 100 || McMMOApi.getNivel(UUID + "Espadas") == 200 || McMMOApi.getNivel(UUID + "Espadas") == 300 || McMMOApi.getNivel(UUID + "Espadas") == 400 || McMMOApi.getNivel(UUID + "Espadas") == 500 || McMMOApi.getNivel(UUID + "Espadas") == 600 || McMMOApi.getNivel(UUID + "Espadas") == 700 || McMMOApi.getNivel(UUID + "Espadas") == 800) {
                        for (final Player target : Bukkit.getOnlinePlayers()) {
                            if (McMMOApi.getNivel(UUID + "Espadas") != 800) {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (McMMOApi.getNivel(UUID + "Espadas")) + "&e na habilidade de Espadas&e!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                            } else {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade de Espadas!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                }
                            }
                        }
                    } else {
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                        player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (McMMOApi.getNivel(UUID + "Espadas"))) + ChatColor.YELLOW + " na habilidade de Espadas!");
                    }
                } else {

                    player.sendActionBar(ChatColor.GREEN + "Espadas (" + (McMMOApi.getXP(UUID + "Espadas")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Espadas")) + ")XP +" + (17 + valor) + "XP");

                }
            }
            if (event.getEntityType().equals(EntityType.WITHER_SKELETON)) {
                int valor = random.nextInt(3);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 12;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Espadas", McMMOApi.getXP(UUID + "Espadas") + valor);

                if (McMMOApi.getProximoNivelXP(UUID + "Espadas") <= McMMOApi.getXP(UUID + "Espadas")) {
                    McMMOApi.updateLevel(UUID + "Espadas");

                    if (McMMOApi.getNivel(UUID + "Espadas") == 100 || McMMOApi.getNivel(UUID + "Espadas") == 200 || McMMOApi.getNivel(UUID + "Espadas") == 300 || McMMOApi.getNivel(UUID + "Espadas") == 400 || McMMOApi.getNivel(UUID + "Espadas") == 500 || McMMOApi.getNivel(UUID + "Espadas") == 600 || McMMOApi.getNivel(UUID + "Espadas") == 700 || McMMOApi.getNivel(UUID + "Espadas") == 800) {
                        for (final Player target : Bukkit.getOnlinePlayers()) {
                            if (McMMOApi.getNivel(UUID + "Espadas") != 800) {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (McMMOApi.getNivel(UUID + "Espadas")) + "&e na habilidade de Espadas&e!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                            } else {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade de Espadas!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                }
                            }
                        }
                    } else {
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                        player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (McMMOApi.getNivel(UUID + "Espadas"))) + ChatColor.YELLOW + " na habilidade de Espadas!");
                    }
                } else {

                    player.sendActionBar(ChatColor.GREEN + "Espadas (" + (McMMOApi.getXP(UUID + "Espadas")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Espadas")) + ")XP +" + (24 + valor) + "XP");

                }
            }
            if (event.getEntityType().equals(EntityType.PIG_ZOMBIE)) {
                int valor = random.nextInt(3);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 10;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Espadas", McMMOApi.getXP(UUID + "Espadas") + valor);

                if (McMMOApi.getProximoNivelXP(UUID + "Espadas") <= McMMOApi.getXP(UUID + "Espadas")) {
                    McMMOApi.updateLevel(UUID + "Espadas");

                    if (McMMOApi.getNivel(UUID + "Espadas") == 100 || McMMOApi.getNivel(UUID + "Espadas") == 200 || McMMOApi.getNivel(UUID + "Espadas") == 300 || McMMOApi.getNivel(UUID + "Espadas") == 400 || McMMOApi.getNivel(UUID + "Espadas") == 500 || McMMOApi.getNivel(UUID + "Espadas") == 600 || McMMOApi.getNivel(UUID + "Espadas") == 700 || McMMOApi.getNivel(UUID + "Espadas") == 800) {
                        for (final Player target : Bukkit.getOnlinePlayers()) {
                            if (McMMOApi.getNivel(UUID + "Espadas") != 800) {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (McMMOApi.getNivel(UUID + "Espadas")) + "&e na habilidade de Espadas&e!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                            } else {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade de Espadas!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                }
                            }
                        }
                    } else {
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                        player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (McMMOApi.getNivel(UUID + "Espadas"))) + ChatColor.YELLOW + " na habilidade de Espadas!");
                    }
                } else {

                    player.sendActionBar(ChatColor.GREEN + "Espadas (" + (McMMOApi.getXP(UUID + "Espadas")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Espadas")) + ")XP +" + (20 + valor) + "XP");

                }
            }
            if (event.getEntityType().equals(EntityType.SLIME)) {
                int valor = random.nextInt(6);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 11;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Espadas", McMMOApi.getXP(UUID + "Espadas") + valor);

                if (McMMOApi.getProximoNivelXP(UUID + "Espadas") <= McMMOApi.getXP(UUID + "Espadas")) {
                    McMMOApi.updateLevel(UUID + "Espadas");

                    if (McMMOApi.getNivel(UUID + "Espadas") == 100 || McMMOApi.getNivel(UUID + "Espadas") == 200 || McMMOApi.getNivel(UUID + "Espadas") == 300 || McMMOApi.getNivel(UUID + "Espadas") == 400 || McMMOApi.getNivel(UUID + "Espadas") == 500 || McMMOApi.getNivel(UUID + "Espadas") == 600 || McMMOApi.getNivel(UUID + "Espadas") == 700 || McMMOApi.getNivel(UUID + "Espadas") == 800) {
                        for (final Player target : Bukkit.getOnlinePlayers()) {
                            if (McMMOApi.getNivel(UUID + "Espadas") != 800) {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (McMMOApi.getNivel(UUID + "Espadas")) + "&e na habilidade de Espadas&e!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                            } else {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade de Espadas!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                }
                            }
                        }
                    } else {
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                        player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (McMMOApi.getNivel(UUID + "Espadas"))) + ChatColor.YELLOW + " na habilidade de Espadas!");
                    }
                } else {

                    player.sendActionBar(ChatColor.GREEN + "Espadas (" + (McMMOApi.getXP(UUID + "Espadas")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Espadas")) + ")XP +" + (22 + valor) + "XP");

                }
            }
            if (event.getEntityType().equals(EntityType.ENDERMAN)) {
                int valor = random.nextInt(8);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 10;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Espadas", McMMOApi.getXP(UUID + "Espadas") + valor);

                if (McMMOApi.getProximoNivelXP(UUID + "Espadas") <= McMMOApi.getXP(UUID + "Espadas")) {
                    McMMOApi.updateLevel(UUID + "Espadas");

                    if (McMMOApi.getNivel(UUID + "Espadas") == 100 || McMMOApi.getNivel(UUID + "Espadas") == 200 || McMMOApi.getNivel(UUID + "Espadas") == 300 || McMMOApi.getNivel(UUID + "Espadas") == 400 || McMMOApi.getNivel(UUID + "Espadas") == 500 || McMMOApi.getNivel(UUID + "Espadas") == 600 || McMMOApi.getNivel(UUID + "Espadas") == 700 || McMMOApi.getNivel(UUID + "Espadas") == 800) {
                        for (final Player target : Bukkit.getOnlinePlayers()) {
                            if (McMMOApi.getNivel(UUID + "Espadas") != 800) {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (McMMOApi.getNivel(UUID + "Espadas")) + "&e na habilidade de Espadas&e!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                            } else {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade de Espadas!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                }
                            }
                        }
                    } else {
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                        player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (McMMOApi.getNivel(UUID + "Espadas"))) + ChatColor.YELLOW + " na habilidade de Espadas!");
                    }
                } else {

                    player.sendActionBar(ChatColor.GREEN + "Espadas (" + (McMMOApi.getXP(UUID + "Espadas")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Espadas")) + ")XP +" + (20 + valor) + "XP");

                }
            }
            if (event.getEntityType().equals(EntityType.CREEPER)) {
                int valor = random.nextInt(3);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 9;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Espadas", McMMOApi.getXP(UUID + "Espadas") + valor);

                if (McMMOApi.getProximoNivelXP(UUID + "Espadas") <= McMMOApi.getXP(UUID + "Espadas")) {
                    McMMOApi.updateLevel(UUID + "Espadas");

                    if (McMMOApi.getNivel(UUID + "Espadas") == 100 || McMMOApi.getNivel(UUID + "Espadas") == 200 || McMMOApi.getNivel(UUID + "Espadas") == 300 || McMMOApi.getNivel(UUID + "Espadas") == 400 || McMMOApi.getNivel(UUID + "Espadas") == 500 || McMMOApi.getNivel(UUID + "Espadas") == 600 || McMMOApi.getNivel(UUID + "Espadas") == 700 || McMMOApi.getNivel(UUID + "Espadas") == 800) {
                        for (final Player target : Bukkit.getOnlinePlayers()) {
                            if (McMMOApi.getNivel(UUID + "Espadas") != 800) {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (McMMOApi.getNivel(UUID + "Espadas")) + "&e na habilidade de Espadas&e!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                            } else {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade de Espadas!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                }
                            }
                        }
                    } else {
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                        player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (McMMOApi.getNivel(UUID + "Espadas"))) + ChatColor.YELLOW + " na habilidade de Espadas!");
                    }
                } else {

                    player.sendActionBar(ChatColor.GREEN + "Espadas (" + (McMMOApi.getXP(UUID + "Espadas")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Espadas")) + ")XP +" + (19 + valor) + "XP");

                }
            }
            if (event.getEntityType().equals(EntityType.SPIDER)) {
                int valor = random.nextInt(4);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 4;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Espadas", McMMOApi.getXP(UUID + "Espadas") + valor);

                if (McMMOApi.getProximoNivelXP(UUID + "Espadas") <= McMMOApi.getXP(UUID + "Espadas")) {
                    McMMOApi.updateLevel(UUID + "Espadas");

                    if (McMMOApi.getNivel(UUID + "Espadas") == 100 || McMMOApi.getNivel(UUID + "Espadas") == 200 || McMMOApi.getNivel(UUID + "Espadas") == 300 || McMMOApi.getNivel(UUID + "Espadas") == 400 || McMMOApi.getNivel(UUID + "Espadas") == 500 || McMMOApi.getNivel(UUID + "Espadas") == 600 || McMMOApi.getNivel(UUID + "Espadas") == 700 || McMMOApi.getNivel(UUID + "Espadas") == 800) {
                        for (final Player target : Bukkit.getOnlinePlayers()) {
                            if (McMMOApi.getNivel(UUID + "Espadas") != 800) {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (McMMOApi.getNivel(UUID + "Espadas")) + "&e na habilidade de Espadas&e!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                            } else {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade de Espadas!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                }
                            }
                        }
                    } else {
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                        player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (McMMOApi.getNivel(UUID + "Espadas"))) + ChatColor.YELLOW + " na habilidade de Espadas!");
                    }
                } else {

                    player.sendActionBar(ChatColor.GREEN + "Espadas (" + (McMMOApi.getXP(UUID + "Espadas")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Espadas")) + ")XP +" + (9 + valor) + "XP");

                }
            }
            if (event.getEntityType().equals(EntityType.SKELETON)) {
                int valor = random.nextInt(3);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 6;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Espadas", McMMOApi.getXP(UUID + "Espadas") + valor);

                if (McMMOApi.getProximoNivelXP(UUID + "Espadas") <= McMMOApi.getXP(UUID + "Espadas")) {
                    McMMOApi.updateLevel(UUID + "Espadas");

                    if (McMMOApi.getNivel(UUID + "Espadas") == 100 || McMMOApi.getNivel(UUID + "Espadas") == 200 || McMMOApi.getNivel(UUID + "Espadas") == 300 || McMMOApi.getNivel(UUID + "Espadas") == 400 || McMMOApi.getNivel(UUID + "Espadas") == 500 || McMMOApi.getNivel(UUID + "Espadas") == 600 || McMMOApi.getNivel(UUID + "Espadas") == 700 || McMMOApi.getNivel(UUID + "Espadas") == 800) {
                        for (final Player target : Bukkit.getOnlinePlayers()) {
                            if (McMMOApi.getNivel(UUID + "Espadas") != 800) {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (McMMOApi.getNivel(UUID + "Espadas")) + "&e na habilidade de Espadas&e!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                            } else {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade de Espadas!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                }
                            }
                        }
                    } else {
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                        player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (McMMOApi.getNivel(UUID + "Espadas"))) + ChatColor.YELLOW + " na habilidade de Espadas!");
                    }
                } else {

                    player.sendActionBar(ChatColor.GREEN + "Espadas (" + (McMMOApi.getXP(UUID + "Espadas")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Espadas")) + ")XP");

                }
            }
            if (event.getEntityType().equals(EntityType.ZOMBIE)) {
                int valor = random.nextInt(3);
                if (player.getFallDistance() > 0.0F && !player.isOnGround() && !player.isInsideVehicle() && !player.hasPotionEffect(PotionEffectType.BLINDNESS) && player.getLocation().getBlock().getType() != Material.LADDER && player.getLocation().getBlock().getType() != Material.VINE) {
                    valor += random.nextInt(10);
                }
                if (player.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 1;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 2) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 2;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 3) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 3;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 4) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 4;
                    }
                    if (player.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 5) {
                        final int porcento = valor / 100;
                        valor = valor + porcento * 5;
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
                    final int porcento = valor / 100;
                    valor = valor + porcento * 2;
                }
                valor += 5;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        valor += (valor * 25) / 100;
                    } else {
                        valor += (valor * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Espadas", McMMOApi.getXP(UUID + "Espadas") + valor);

                if (McMMOApi.getProximoNivelXP(UUID + "Espadas") <= McMMOApi.getXP(UUID + "Espadas")) {
                    McMMOApi.updateLevel(UUID + "Espadas");

                    if (McMMOApi.getNivel(UUID + "Espadas") == 100 || McMMOApi.getNivel(UUID + "Espadas") == 200 || McMMOApi.getNivel(UUID + "Espadas") == 300 || McMMOApi.getNivel(UUID + "Espadas") == 400 || McMMOApi.getNivel(UUID + "Espadas") == 500 || McMMOApi.getNivel(UUID + "Espadas") == 600 || McMMOApi.getNivel(UUID + "Espadas") == 700 || McMMOApi.getNivel(UUID + "Espadas") == 800) {
                        for (final Player target : Bukkit.getOnlinePlayers()) {
                            if (McMMOApi.getNivel(UUID + "Espadas") != 800) {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (McMMOApi.getNivel(UUID + "Espadas")) + "&e na habilidade de Espadas&e!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                            } else {
                                target.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade de Espadas!\n \n"));
                                if (target.getName().equals(player.getName())) {
                                    target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                } else {
                                    target.playSound(target.getLocation().add(0, 0, 0), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                }
                            }
                        }
                    } else {
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                        player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (McMMOApi.getNivel(UUID + "Espadas"))) + ChatColor.YELLOW + " na habilidade de Espadas!");
                    }
                } else {

                    player.sendActionBar(ChatColor.GREEN + "Espadas (" + (McMMOApi.getXP(UUID + "Espadas")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Espadas")) + ")XP +" + (10 + valor) + "XP");

                }
            }
        }
    }
}
