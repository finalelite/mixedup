package com.mixedup.mcmmo.skills;

import com.mixedup.Main;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.hashs.NexusUtil;
import com.mixedup.mcmmo.McMMOApi;
import com.mixedup.mcmmo.ProbabilityApi;
import com.mixedup.mcmmo.utils.BoosterHash;
import com.mixedup.plots.espaconave.EspaçonaveUtil;
import com.mixedup.plots.ilha.IlhaAPI;
import com.mixedup.plots.ilha.IlhaUtil;
import com.mixedup.plots.terrain.TerrainInfosAPI;
import com.mixedup.plots.terrain.TerrainsUtil;
import com.mixedup.utils.AlternateColor;
import com.mixedup.worldProtection.hashs.ProtectionUtil;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.Arrays;
import java.util.Date;
import java.util.Random;

public class MineracaoEvent implements Listener {

    @EventHandler
    public void onPlace(final BlockPlaceEvent event) {
        final Material[] list = {Material.STONE, Material.COAL_ORE, Material.IRON_ORE, Material.GOLD_ORE, Material.LAPIS_ORE, Material.REDSTONE_ORE, Material.DIAMOND_ORE, Material.EMERALD_ORE};
        if (Arrays.asList(list).contains(event.getBlockPlaced().getType())) {
            event.getBlock().setMetadata("mcmmo-miner", new FixedMetadataValue(Main.getInstance(), true));
        }
    }

    @EventHandler
    public void onBreak(final BlockBreakEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        final ItemStack hand = event.getPlayer().getItemInHand();

        if (ProtectionUtil.playerInArea(event.getBlock().getLocation()) != null || event.getBlock().getWorld().getName().equalsIgnoreCase("spawn") || event.getBlock().getWorld().getName().equalsIgnoreCase("guildaselect") || event.getBlock().getWorld().getName().equalsIgnoreCase("aether")) {
            return;
        }

        if (player.getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }

        if (NexusUtil.playerInArea(event.getBlock().getLocation()) != null) return;
        if (TerrainsUtil.playerInArea(event.getBlock().getLocation()) != null && !TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID().contains(UUID) && !TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).contains(UUID))
        return;
        if (IlhaUtil.playerInArea(event.getBlock().getLocation()) != null && !IlhaUtil.playerInArea(event.getBlock().getLocation()).getRegionID().contains(UUID) && !IlhaAPI.getAmigos(IlhaUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).contains(UUID))
            return;
        if (FacAPI.getChunkOwn(event.getBlock().getLocation().getChunk().getX() + ":" + event.getBlock().getLocation().getChunk().getZ()) != null)
            return;
        if (event.getBlock().hasMetadata("mcmmo-miner")) return;

        if (event.getBlock().getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
            final String chunk = event.getBlock().getLocation().getChunk().getX() + ":" + event.getBlock().getLocation().getChunk().getZ();

            if (FacAPI.getChunkOwn(chunk) != null) {
                if (FacAPI.getFacNome(UUID) != null) {
                    if (FacAPI.getChunkOwn(chunk).equalsIgnoreCase(FacAPI.getFacNome(UUID))) {
                        if (FacAPI.getPermBuild(UUID) == false) {
                            return;
                        }
                    } else {
                        if (FacAPI.getAliados(FacAPI.getChunkOwn(chunk)) != null && FacAPI.getAliados(FacAPI.getChunkOwn(chunk)).contains(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)))) {
                            if (FacAPI.getAliadoPermBuild(FacAPI.getTagWithNome(FacAPI.getChunkOwn(chunk)) + FacAPI.getTagWithNome(FacAPI.getFacNome(UUID))) == false) {
                                return;
                            }
                        } else {
                            return;
                        }
                    }
                } else {
                    return;
                }
            }
        }

        if (BoosterHash.get(UUID) != null) {
            if (BoosterHash.get(UUID).gettime() - (new Date().getTime() / 1000) <= 0) {
                BoosterHash.CACHE.remove(UUID);
            }
        }

        if (McMMOApi.getNivel(UUID + "Mineracao") == 1000) {
            if (event.getBlock().getType().equals(Material.REDSTONE_ORE) || event.getBlock().getType().equals(Material.LAPIS_ORE) || event.getBlock().getType().equals(Material.COAL_ORE)) {
                final Random random = new Random();
                final int quantia = random.nextInt(3);
                if (event.getBlock().getType().equals(Material.REDSTONE_ORE)) {
                    event.getBlock().getDrops().add(new ItemStack(Material.REDSTONE, quantia));
                }
                if (event.getBlock().getType().equals(Material.LAPIS_ORE)) {
                    event.getBlock().getDrops().add(new ItemStack(Material.LAPIS_LAZULI, event.getBlock().getDrops().size()));
                }
                if (event.getBlock().getType().equals(Material.COAL_ORE)) {
                    event.getBlock().getDrops().add(new ItemStack(Material.COAL, quantia));
                }
                event.setDropItems(true);
                return;
            }

            ItemStack item;
            if (event.getBlock().getType().equals(Material.EMERALD_ORE)) {
                item = new ItemStack(Material.EMERALD, 2 + event.getBlock().getDrops().size());
                final ItemMeta meta = item.getItemMeta();
                meta.setDisplayName(ChatColor.GREEN + "Plutônio");
                item.setItemMeta(meta);
            } else if (event.getBlock().getType().equals(Material.DIAMOND_ORE)) {
                item = new ItemStack(Material.DIAMOND, 2 + event.getBlock().getDrops().size());
            } else {
                item = new ItemStack(event.getBlock().getType(), 1);
            }
            if (item.getType().equals(Material.STONE) && hand.containsEnchantment(Enchantment.SILK_TOUCH) == false) {
                item = new ItemStack(Material.COBBLESTONE, event.getBlock().getDrops().size());
            }
            player.getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
            event.setDropItems(false);
            return;
        }

        if (!hand.getType().equals(Material.STONE_PICKAXE) && !hand.getType().equals(Material.WOODEN_PICKAXE) && !hand.getType().equals(Material.IRON_PICKAXE) && !hand.getType().equals(Material.GOLDEN_PICKAXE) && !hand.getType().equals(Material.DIAMOND_PICKAXE)) {
            return;
        }

        if (hand.containsEnchantment(Enchantment.SILK_TOUCH)) {
            if (event.getBlock().getType().equals(Material.EMERALD_ORE)) {
                final ItemStack plutonio = new ItemStack(Material.EMERALD_ORE, event.getBlock().getDrops().size());
                final ItemMeta meta = plutonio.getItemMeta();
                meta.setDisplayName(ChatColor.GREEN + "Minério de plutônio");
                plutonio.setItemMeta(meta);
                player.getWorld().dropItemNaturally(event.getBlock().getLocation(), plutonio);
                event.setDropItems(false);
            }
        }

        if (event.getBlock().getType().equals(Material.STONE) || event.getBlock().getType().equals(Material.COAL_ORE) || event.getBlock().getType().equals(Material.IRON_ORE) || event.getBlock().getType().equals(Material.GOLD_ORE) || event.getBlock().getType().equals(Material.REDSTONE_ORE) || event.getBlock().getType().equals(Material.DIAMOND_ORE) || event.getBlock().getType().equals(Material.LAPIS_ORE) || event.getBlock().getType().equals(Material.EMERALD_ORE)) {
            final int xpAnterior = McMMOApi.getXP(UUID + "Mineracao");
            int newXp = 0;
            if (event.getBlock().getType().equals(Material.STONE)) {
                newXp = xpAnterior + 3;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (3 * 25) / 100;
                    } else {
                        newXp += (3 * 50) / 100;
                    }
                }
                McMMOApi.updateXP(UUID + "Mineracao", newXp);
            } else if (event.getBlock().getType().equals(Material.COAL_ORE)) {
                newXp = xpAnterior + 10;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (10 * 25) / 100;
                    } else {
                        newXp += (10 * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Mineracao", newXp);
            } else if (event.getBlock().getType().equals(Material.IRON_ORE)) {
                newXp = xpAnterior + 25;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (25 * 25) / 100;
                    } else {
                        newXp += (25 * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Mineracao", newXp);
            } else if (event.getBlock().getType().equals(Material.GOLD_ORE)) {
                newXp = xpAnterior + 35;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (35 * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Mineracao", newXp);
            } else if (event.getBlock().getType().equals(Material.REDSTONE_ORE)) {
                newXp = xpAnterior + 15;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (15 * 25) / 100;
                    } else {
                        newXp += (15 * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Mineracao", newXp);
            } else if (event.getBlock().getType().equals(Material.DIAMOND_ORE)) {
                newXp = xpAnterior + 75;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (76 * 25) / 100;
                    } else {
                        newXp += (76 * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Mineracao", newXp);
            } else if (event.getBlock().getType().equals(Material.LAPIS_ORE)) {
                newXp = xpAnterior + 40;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (40 * 25) / 100;
                    } else {
                        newXp += (40 * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Mineracao", newXp);
            } else if (event.getBlock().getType().equals(Material.EMERALD_ORE)) {
                newXp = xpAnterior + 115;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (115 * 25) / 100;
                    } else {
                        newXp += (115 * 50) / 100;
                    }

                }
                McMMOApi.updateXP(UUID + "Mineracao", newXp);
            }

            if (McMMOApi.getProximoNivelXP(UUID + "Mineracao") <= newXp) {
                final int level = McMMOApi.updateLevel(UUID + "Mineracao");

                if (level == 100 || level == 200 || level == 300 || level == 400 || level == 500
                        || level == 600 || level == 700 || level == 800 || level == 900 || level == 1000) {
                    for (final Player target : Bukkit.getOnlinePlayers()) {
                        if (level != 1000) {
                            target.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (level) + "&e na habilidade de Mineração!&e!\n \n"));
                            if (target.getName().equals(player.getName())) {
                                target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                            } else {
                                target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else {
                            target.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade de Mineração!\n \n"));
                            if (target.getName().equals(player.getName())) {
                                target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                target.playSound(target.getLocation().add(10, 0, 10), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                            } else {
                                target.playSound(target.getLocation().add(10, 0, 10), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                            }
                        }
                    }
                } else {
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                    player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (level)) + ChatColor.YELLOW + " na habilidade de Mineração!");
                }
            } else {

                player.sendActionBar(ChatColor.GREEN + "Mineração (" + (newXp) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Mineracao")) + ")XP +" + (newXp - xpAnterior) + "XP");

            }

            final int probability = McMMOApi.getNivel(UUID + "Mineracao") * 70;
            if (ProbabilityApi.probab(probability) == true) {

                if (event.getBlock().getType().equals(Material.REDSTONE_ORE) || event.getBlock().getType().equals(Material.LAPIS_ORE) || event.getBlock().getType().equals(Material.COAL_ORE)) {

                    if (event.getPlayer().getItemInHand().containsEnchantment(Enchantment.LOOT_BONUS_BLOCKS)) {
                        final Random random = new Random();
                        final int quantia = random.nextInt(3);
                        if (event.getBlock().getType().equals(Material.REDSTONE_ORE)) {
                            event.getBlock().getDrops().add(new ItemStack(Material.REDSTONE, quantia + event.getBlock().getDrops().size()));
                        }
                        if (event.getBlock().getType().equals(Material.LAPIS_ORE)) {
                            event.getBlock().getDrops().add(new ItemStack(Material.LAPIS_LAZULI, event.getBlock().getDrops().size()));
                        }
                        if (event.getBlock().getType().equals(Material.COAL_ORE)) {
                            event.getBlock().getDrops().add(new ItemStack(Material.COAL, quantia + event.getBlock().getDrops().size()));
                        }
                        event.setDropItems(true);
                        return;
                    } else {
                        final Random random = new Random();
                        final int quantia = random.nextInt(3);
                        if (event.getBlock().getType().equals(Material.REDSTONE_ORE)) {
                            event.getBlock().getDrops().add(new ItemStack(Material.REDSTONE, quantia));
                        }
                        if (event.getBlock().getType().equals(Material.LAPIS_ORE)) {
                            event.getBlock().getDrops().add(new ItemStack(Material.LAPIS_LAZULI, event.getBlock().getDrops().size()));
                        }
                        if (event.getBlock().getType().equals(Material.COAL_ORE)) {
                            event.getBlock().getDrops().add(new ItemStack(Material.COAL, quantia));
                        }
                        event.setDropItems(true);
                        return;
                    }
                }

                if (event.getPlayer().getItemInHand().containsEnchantment(Enchantment.LOOT_BONUS_BLOCKS)) {
                    ItemStack item;
                    if (event.getBlock().getType().equals(Material.EMERALD_ORE)) {
                        item = new ItemStack(Material.EMERALD, 2 + event.getBlock().getDrops().size());
                        final ItemMeta meta = item.getItemMeta();
                        meta.setDisplayName(ChatColor.GREEN + "Plutônio");
                        item.setItemMeta(meta);
                    } else if (event.getBlock().getType().equals(Material.DIAMOND_ORE)) {
                        item = new ItemStack(Material.DIAMOND, 2 + event.getBlock().getDrops().size());
                    } else {
                        item = new ItemStack(event.getBlock().getType(), 1);
                    }
                    if (item.getType().equals(Material.STONE) && hand.containsEnchantment(Enchantment.SILK_TOUCH) == false) {
                        item = new ItemStack(Material.COBBLESTONE, event.getBlock().getDrops().size());
                    }
                    player.getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                    event.setDropItems(false);
                } else {
                    ItemStack item;
                    if (event.getBlock().getType().equals(Material.EMERALD_ORE)) {
                        item = new ItemStack(Material.EMERALD, 2);
                        final ItemMeta meta = item.getItemMeta();
                        meta.setDisplayName(ChatColor.GREEN + "Plutônio");
                        item.setItemMeta(meta);
                    } else if (event.getBlock().getType().equals(Material.DIAMOND_ORE)) {
                        item = new ItemStack(Material.DIAMOND, 2);
                    } else {
                        item = new ItemStack(event.getBlock().getType(), 1);
                    }
                    if (item.getType().equals(Material.STONE) && hand.containsEnchantment(Enchantment.SILK_TOUCH) == false) {
                        item = new ItemStack(Material.COBBLESTONE, event.getBlock().getDrops().size());
                    }
                    player.getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                    event.setDropItems(false);
                }
            } else {

                if (event.getBlock().getType().equals(Material.REDSTONE_ORE) || event.getBlock().getType().equals(Material.LAPIS_ORE) || event.getBlock().getType().equals(Material.COAL_ORE)) {
                    event.setDropItems(true);
                    return;
                }

                if (event.getPlayer().getItemInHand().containsEnchantment(Enchantment.LOOT_BONUS_BLOCKS)) {
                    ItemStack item;
                    if (event.getBlock().getType().equals(Material.EMERALD_ORE)) {
                        item = new ItemStack(Material.EMERALD, 1 + event.getBlock().getDrops().size());
                        final ItemMeta meta = item.getItemMeta();
                        meta.setDisplayName(ChatColor.GREEN + "Plutônio");
                        item.setItemMeta(meta);
                    } else if (event.getBlock().getType().equals(Material.DIAMOND_ORE)) {
                        item = new ItemStack(Material.DIAMOND, 1 + event.getBlock().getDrops().size());
                    } else {
                        item = new ItemStack(event.getBlock().getType(), 1);
                    }
                    if (item.getType().equals(Material.STONE) && hand.containsEnchantment(Enchantment.SILK_TOUCH) == false) {
                        item = new ItemStack(Material.COBBLESTONE, event.getBlock().getDrops().size());
                    }
                    player.getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                    event.setDropItems(false);
                } else {
                    ItemStack item;
                    if (event.getBlock().getType().equals(Material.EMERALD_ORE)) {
                        item = new ItemStack(Material.EMERALD, 1);
                        final ItemMeta meta = item.getItemMeta();
                        meta.setDisplayName(ChatColor.GREEN + "Plutônio");
                        item.setItemMeta(meta);
                    } else if (event.getBlock().getType().equals(Material.DIAMOND_ORE)) {
                        item = new ItemStack(Material.DIAMOND, 1);
                    } else {
                        item = new ItemStack(event.getBlock().getType(), 1);
                    }
                    if (item.getType().equals(Material.STONE) && hand.containsEnchantment(Enchantment.SILK_TOUCH) == false) {
                        item = new ItemStack(Material.COBBLESTONE, event.getBlock().getDrops().size());
                    }
                    player.getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                    event.setDropItems(false);
                }
            }
        }
    }
}
