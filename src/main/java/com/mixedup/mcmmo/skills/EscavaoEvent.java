package com.mixedup.mcmmo.skills;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.hashs.NexusUtil;
import com.mixedup.mcmmo.McMMOApi;
import com.mixedup.mcmmo.ProbabilityApi;
import com.mixedup.mcmmo.utils.BoosterHash;
import com.mixedup.plots.espaconave.EspaçonaveUtil;
import com.mixedup.plots.ilha.IlhaAPI;
import com.mixedup.plots.ilha.IlhaUtil;
import com.mixedup.plots.terrain.TerrainInfosAPI;
import com.mixedup.plots.terrain.TerrainsUtil;
import com.mixedup.utils.AlternateColor;
import com.mixedup.worldProtection.hashs.ProtectionUtil;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Date;

public class EscavaoEvent implements Listener {

    public static void verifyLevelUp(final Player player, final int xpAnterior, int newXp) {
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (BoosterHash.get(UUID) != null) {
            if (BoosterHash.get(UUID).gettime() - (new Date().getTime() / 1000) <= 0) {
                BoosterHash.CACHE.remove(UUID);
            }
        }

        if (McMMOApi.getProximoNivelXP(UUID + "Escavacao") <= McMMOApi.getXP(UUID + "Escavacao")) {
            McMMOApi.updateLevel(UUID + "Escavacao");

            if (McMMOApi.getNivel(UUID + "Escavacao") == 100 || McMMOApi.getNivel(UUID + "Escavacao") == 200 || McMMOApi.getNivel(UUID + "Escavacao") == 300 || McMMOApi.getNivel(UUID + "Escavacao") == 400 || McMMOApi.getNivel(UUID + "Escavacao") == 500 || McMMOApi.getNivel(UUID + "Escavacao") == 600 || McMMOApi.getNivel(UUID + "Escavacao") == 700 || McMMOApi.getNivel(UUID + "Escavacao") == 800 || McMMOApi.getNivel(UUID + "Escavacao") == 900 || McMMOApi.getNivel(UUID + "Escavacao") == 1000) {
                for (final Player target : Bukkit.getOnlinePlayers()) {
                    if (McMMOApi.getNivel(UUID + "Escavacao") != 1000) {
                        target.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (McMMOApi.getNivel(UUID + "Escavacao")) + "&e na habilidade de Escavacao!&e!\n \n"));
                        if (target.getName().equals(player.getName())) {
                            target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                        } else {
                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        target.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade de Escavacao!\n \n"));
                        if (target.getName().equals(player.getName())) {
                            target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                            target.playSound(target.getLocation().add(10, 0, 10), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                        } else {
                            target.playSound(target.getLocation().add(10, 0, 10), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                        }
                    }
                }
            } else {
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (McMMOApi.getNivel(UUID + "Escavacao"))) + ChatColor.YELLOW + " na habilidade de Escavacao!");
            }
        } else {

            player.sendActionBar(ChatColor.GREEN + "Escavacao (" + (McMMOApi.getXP(UUID + "Escavacao")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Escavacao")) + ")XP +" + (newXp) + "XP");

        }
    }

    @EventHandler
    public void onBreak(final BlockBreakEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        final ItemStack hand = event.getPlayer().getItemInHand();

        if (ProtectionUtil.playerInArea(event.getBlock().getLocation()) != null || event.getBlock().getWorld().getName().equalsIgnoreCase("spawn") || event.getBlock().getWorld().getName().equalsIgnoreCase("guildaselect") || event.getBlock().getWorld().getName().equalsIgnoreCase("aether")) {
            return;
        }

        if (player.getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }

        if (TerrainsUtil.playerInArea(event.getBlock().getLocation()) != null && !TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID().contains(UUID) && !TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).contains(UUID))
        return;
        if (IlhaUtil.playerInArea(event.getBlock().getLocation()) != null && !IlhaUtil.playerInArea(event.getBlock().getLocation()).getRegionID().contains(UUID) && !IlhaAPI.getAmigos(IlhaUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).contains(UUID))
            return;
        if (NexusUtil.playerInArea(event.getBlock().getLocation()) != null) return;
        if (FacAPI.getChunkOwn(event.getBlock().getLocation().getChunk().getX() + ":" + event.getBlock().getLocation().getChunk().getZ()) != null)
            return;

        if (event.getBlock().getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
            final String chunk = event.getBlock().getLocation().getChunk().getX() + ":" + event.getBlock().getLocation().getChunk().getZ();

            if (FacAPI.getChunkOwn(chunk) != null) {
                if (FacAPI.getFacNome(UUID) != null) {
                    if (FacAPI.getChunkOwn(chunk).equalsIgnoreCase(FacAPI.getFacNome(UUID))) {
                        if (FacAPI.getPermBuild(UUID) == false) {
                            return;
                        }
                    } else {
                        if (FacAPI.getAliados(FacAPI.getChunkOwn(chunk)) != null && FacAPI.getAliados(FacAPI.getChunkOwn(chunk)).contains(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)))) {
                            if (FacAPI.getAliadoPermBuild(FacAPI.getTagWithNome(FacAPI.getChunkOwn(chunk)) + FacAPI.getTagWithNome(FacAPI.getFacNome(UUID))) == false) {
                                return;
                            }
                        } else {
                            return;
                        }
                    }
                } else {
                    return;
                }
            }
        }

        if (!hand.getType().equals(Material.STONE_SHOVEL) && !hand.getType().equals(Material.WOODEN_SHOVEL) && !hand.getType().equals(Material.IRON_SHOVEL) && !hand.getType().equals(Material.GOLDEN_SHOVEL) && !hand.getType().equals(Material.DIAMOND_SHOVEL)) {
            return;
        }

        if (BoosterHash.get(UUID) != null) {
            if (BoosterHash.get(UUID).gettime() - (new Date().getTime() / 1000) <= 0) {
                BoosterHash.CACHE.remove(UUID);
            }
        }

        int level = McMMOApi.getNivel(UUID + "Escavacao");
        if (level == 1000) {
            int drops = 0;

            final int dropExtra19 = 100;
            final int xpAnterior = McMMOApi.getXP(UUID + "Escavacao");
            int xpNew = 0;
            if (ProbabilityApi.probab(dropExtra19) == true && drops < 2 && 1000 >= 1000) {
                event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.ENDER_EYE));
                xpNew = 50;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        xpNew += (xpNew * 25) / 100;
                    } else {
                        xpNew += (xpNew * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + xpNew);
                drops++;
            }

            final int dropExtra18 = 100;
            if (ProbabilityApi.probab(dropExtra18) == true && drops < 2 && level >= 950) {
                event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.TURTLE_EGG));
                xpNew = 40;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        xpNew += (xpNew * 25) / 100;
                    } else {
                        xpNew += (xpNew * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + xpNew);
                drops++;
            }

            final int dropExtra17 = 250;
            if (ProbabilityApi.probab(dropExtra17) == true && drops < 2 && level >= 900) {
                event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.BEETROOT));
                xpNew = 30;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        xpNew += (xpNew * 25) / 100;
                    } else {
                        xpNew += (xpNew * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + xpNew);
                drops++;
            }

            final int dropExtra16 = 500;
            if (ProbabilityApi.probab(dropExtra16) == true && drops < 2 && level >= 850) {
                event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.POISONOUS_POTATO));
                xpNew = 25;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        xpNew += (xpNew * 25) / 100;
                    } else {
                        xpNew += (xpNew * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + xpNew);
                drops++;
            }

            final int dropExtra15 = 750;
            if (ProbabilityApi.probab(dropExtra15) == true && drops < 2 && level >= 800) {
                event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.CHORUS_FRUIT));
                xpNew = 20;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        xpNew += (xpNew * 25) / 100;
                    } else {
                        xpNew += (xpNew * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + xpNew);
                drops++;
            }

            final int dropExtra14 = 1000;
            if (ProbabilityApi.probab(dropExtra14) == true && drops < 2 && level >= 750) {
                if (event.getBlock().getType().equals(Material.RED_SAND) || event.getBlock().getType().equals(Material.SAND) || event.getBlock().getType().equals(Material.CLAY)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.LILY_PAD));
                    xpNew = 15;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            xpNew += (xpNew * 25) / 100;
                        } else {
                            xpNew += (xpNew * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + xpNew);
                    drops++;
                }
            }

            final int dropExtra13 = 1250;
            if (ProbabilityApi.probab(dropExtra13) == true && drops < 2 && level >= 700) {
                if (event.getBlock().getType().equals(Material.CLAY) || event.getBlock().getType().equals(Material.SAND) || event.getBlock().getType().equals(Material.GRASS)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.SCUTE));
                    xpNew = 10;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            xpNew += (xpNew * 25) / 100;
                        } else {
                            xpNew += (xpNew * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + xpNew);
                    drops++;
                }
            }

            final int dropExtra12 = 1500;
            if (ProbabilityApi.probab(dropExtra12) == true && drops < 2 && level >= 650) {
                if (event.getBlock().getType().equals(Material.GRASS) || event.getBlock().getType().equals(Material.MYCELIUM) || event.getBlock().getType().equals(Material.DIRT) || event.getBlock().getType().equals(Material.SAND) || event.getBlock().getType().equals(Material.RED_SAND)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.COBWEB));
                    xpNew = 8;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            xpNew += (xpNew * 25) / 100;
                        } else {
                            xpNew += (xpNew * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + xpNew);
                    drops++;
                }
            }

            final int dropExtra11 = 1750;
            if (ProbabilityApi.probab(dropExtra11) == true && drops < 2 && level >= 600) {
                if (event.getBlock().getType().equals(Material.SAND) || event.getBlock().getType().equals(Material.CLAY)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.SEAGRASS));
                    xpNew = 8;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            xpNew += (xpNew * 25) / 100;
                        } else {
                            xpNew += (xpNew * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + xpNew);
                }
                drops++;
            }

            final int dropExtra10 = 1750;
            if (ProbabilityApi.probab(dropExtra10) == true && drops < 2 && level >= 550) {
                if (event.getBlock().getType().equals(Material.SAND)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.CLOCK));
                    xpNew = 7;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            xpNew += (xpNew * 25) / 100;
                        } else {
                            xpNew += (xpNew * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + xpNew);
                    drops++;
                }
            }

            final int dropExtra9 = 2000;
            if (ProbabilityApi.probab(dropExtra9) == true && drops < 2 && level >= 500) {
                if (event.getBlock().getType().equals(Material.CLAY) || event.getBlock().getType().equals(Material.SAND)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.BUCKET));
                    xpNew = 7;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            xpNew += (xpNew * 25) / 100;
                        } else {
                            xpNew += (xpNew * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + xpNew);
                    drops++;
                }
            }

            final int dropExtra20 = 2000;
            if (ProbabilityApi.probab(dropExtra20) == true && drops < 2 && level >= 400) {
                if (event.getBlock().getType().equals(Material.CLAY) || event.getBlock().getType().equals(Material.SAND) || event.getBlock().getType().equals(Material.GRASS)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.DRIED_KELP));
                    xpNew = 5;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            xpNew += (xpNew * 25) / 100;
                        } else {
                            xpNew += (xpNew * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + xpNew);
                    drops++;
                }
            }

            final int dropExtra7 = 2000;
            if (ProbabilityApi.probab(dropExtra7) == true && drops < 2 && level >= 350) {
                if (event.getBlock().getType().equals(Material.GRASS) || event.getBlock().getType().equals(Material.DIRT) || event.getBlock().getType().equals(Material.MYCELIUM) || event.getBlock().getType().equals(Material.RED_SAND) || event.getBlock().getType().equals(Material.SAND)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.GOLD_INGOT));
                    xpNew = 5;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            xpNew += (xpNew * 25) / 100;
                        } else {
                            xpNew += (xpNew * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + xpNew);
                    drops++;
                }
            }

            final int dropExtra21 = 2000;
            if (ProbabilityApi.probab(dropExtra21) == true && drops < 2 && level >= 350) {
                if (event.getBlock().getType().equals(Material.GRASS) || event.getBlock().getType().equals(Material.DIRT) || event.getBlock().getType().equals(Material.MYCELIUM) || event.getBlock().getType().equals(Material.RED_SAND) || event.getBlock().getType().equals(Material.SAND)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.GOLD_INGOT));
                    xpNew = 5;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            xpNew += (xpNew * 25) / 100;
                        } else {
                            xpNew += (xpNew * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + xpNew);
                    drops++;
                }
            }

            final int dropExtra6 = 2000;
            if (ProbabilityApi.probab(dropExtra6) == true && drops < 2 && level >= 300) {
                if (event.getBlock().getType().equals(Material.GRASS) || event.getBlock().getType().equals(Material.DIRT) || event.getBlock().getType().equals(Material.MYCELIUM) || event.getBlock().getType().equals(Material.RED_SAND) || event.getBlock().getType().equals(Material.SAND)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.DIAMOND));
                    xpNew = 5;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            xpNew += (xpNew * 25) / 100;
                        } else {
                            xpNew += (xpNew * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + xpNew);
                    drops++;
                }
            }

            final int dropExtra5 = 5000;
            if (ProbabilityApi.probab(dropExtra5) == true && drops < 2 && level >= 250) {
                if (event.getBlock().getType().equals(Material.GRASS) || event.getBlock().getType().equals(Material.DIRT) || event.getBlock().getType().equals(Material.MYCELIUM)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.APPLE));
                    xpNew = 3;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            xpNew += (xpNew * 25) / 100;
                        } else {
                            xpNew += (xpNew * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + xpNew);
                    drops++;
                }
            }

            final int dropExtra4 = 7000;
            if (ProbabilityApi.probab(dropExtra4) == true && drops < 2 && level >= 200) {
                if (event.getBlock().getType().equals(Material.GRASS) || event.getBlock().getType().equals(Material.DIRT)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.EGG));
                    xpNew = 3;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            xpNew += (xpNew * 25) / 100;
                        } else {
                            xpNew += (xpNew * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + xpNew);
                    drops++;
                }
            }

            final int dropExtra3 = 7000;
            if (ProbabilityApi.probab(dropExtra3) == true && drops < 2 && level >= 150) {
                if (event.getBlock().getType().equals(Material.CLAY) || event.getBlock().getType().equals(Material.GRAVEL)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.BONE));
                    xpNew = 2;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            xpNew += (xpNew * 25) / 100;
                        } else {
                            xpNew += (xpNew * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + xpNew);
                    drops++;
                }
            }

            final int dropExtra2 = 7000;
            if (ProbabilityApi.probab(dropExtra2) == true && drops < 2 && level >= 100) {
                if (event.getBlock().getType().equals(Material.CLAY) || event.getBlock().getType().equals(Material.RED_SAND) || event.getBlock().getType().equals(Material.SAND)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.SLIME_BALL));
                    xpNew = 2;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            xpNew += (xpNew * 25) / 100;
                        } else {
                            xpNew += (xpNew * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + xpNew);
                    drops++;
                }
            }

            final int dropExtra1 = 10000;
            if (ProbabilityApi.probab(dropExtra1) == true && drops < 2 && level >= 50) {
                if (event.getBlock().getType().equals(Material.GRASS) || event.getBlock().getType().equals(Material.DIRT) || event.getBlock().getType().equals(Material.RED_SAND) || event.getBlock().getType().equals(Material.SAND)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.GLOWSTONE_DUST));
                    xpNew = 2;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            xpNew += (xpNew * 25) / 100;
                        } else {
                            xpNew += (xpNew * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + xpNew);
                    drops++;
                }
            }
            return;
        }

        if (event.getBlock().getType().equals(Material.MYCELIUM) || event.getBlock().getType().equals(Material.PODZOL) || event.getBlock().getType().equals(Material.COARSE_DIRT) || event.getBlock().getType().equals(Material.CLAY) || event.getBlock().getType().equals(Material.GRAVEL) ||
                event.getBlock().getType().equals(Material.SNOW_BLOCK) || event.getBlock().getType().equals(Material.SOUL_SAND) || event.getBlock().getType().equals(Material.RED_SAND) || event.getBlock().getType().equals(Material.FARMLAND) || event.getBlock().getType().equals(Material.GRASS_PATH) ||
                event.getBlock().getType().equals(Material.GRASS) || event.getBlock().getType().equals(Material.DIRT) || event.getBlock().getType().equals(Material.SAND) || event.getBlock().getType().equals(Material.SNOW)) {
            final int xpAnterior = McMMOApi.getXP(UUID + "Escavacao");

            if (event.getBlock().getType().equals(Material.MYCELIUM)) {
                int newXp = 43;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                EscavaoEvent.verifyLevelUp(player, xpAnterior, newXp);
            }
            if (event.getBlock().getType().equals(Material.PODZOL)) {
                int newXp = 37;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                EscavaoEvent.verifyLevelUp(player, xpAnterior, newXp);
            }
            if (event.getBlock().getType().equals(Material.COARSE_DIRT)) {
                int newXp = 21;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                EscavaoEvent.verifyLevelUp(player, xpAnterior, newXp);
            }
            if (event.getBlock().getType().equals(Material.CLAY)) {
                int newXp = 14;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                EscavaoEvent.verifyLevelUp(player, xpAnterior, newXp);
            }
            if (event.getBlock().getType().equals(Material.GRAVEL)) {
                int newXp = 13;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                EscavaoEvent.verifyLevelUp(player, xpAnterior, newXp);
            }
            if (event.getBlock().getType().equals(Material.SNOW_BLOCK)) {
                int newXp = 8;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                EscavaoEvent.verifyLevelUp(player, xpAnterior, newXp);
            }
            if (event.getBlock().getType().equals(Material.SOUL_SAND)) {
                int newXp = 8;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                EscavaoEvent.verifyLevelUp(player, xpAnterior, newXp);
            }
            if (event.getBlock().getType().equals(Material.RED_SAND)) {
                int newXp = 6;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                EscavaoEvent.verifyLevelUp(player, xpAnterior, newXp);
            }
            if (event.getBlock().getType().equals(Material.FARMLAND)) {
                int newXp = 5;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                EscavaoEvent.verifyLevelUp(player, xpAnterior, newXp);
            }
            if (event.getBlock().getType().equals(Material.GRASS_PATH)) {
                int newXp = 5;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                EscavaoEvent.verifyLevelUp(player, xpAnterior, newXp);
            }
            if (event.getBlock().getType().equals(Material.GRASS)) {
                int newXp = 5;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                EscavaoEvent.verifyLevelUp(player, xpAnterior, newXp);
            }
            if (event.getBlock().getType().equals(Material.DIRT)) {
                int newXp = 3;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                EscavaoEvent.verifyLevelUp(player, xpAnterior, newXp);
            }
            if (event.getBlock().getType().equals(Material.SAND)) {
                int newXp = 3;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                EscavaoEvent.verifyLevelUp(player, xpAnterior, newXp);
            }
            if (event.getBlock().getType().equals(Material.SNOW)) {
                int newXp = 3;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                EscavaoEvent.verifyLevelUp(player, xpAnterior, newXp);
            }

            level = McMMOApi.getNivel(UUID + "Escavacao");
            final int probability = level * 100;
            if (ProbabilityApi.probab(probability) == true) {
                final ItemStack item = new ItemStack(event.getBlock().getType(), 1);
                player.getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                event.setDropItems(false);
            }

            int drops = 0;

            final int dropExtra19 = 100;
            if (ProbabilityApi.probab(dropExtra19) == true && drops < 2 && level >= 1000) {
                event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.ENDER_EYE));
                int newXp = 50;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                drops++;
            }

            final int dropExtra18 = 100;
            if (ProbabilityApi.probab(dropExtra18) == true && drops < 2 && level >= 950) {
                event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.TURTLE_EGG));
                int newXp = 40;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                drops++;
            }

            final int dropExtra17 = 250;
            if (ProbabilityApi.probab(dropExtra17) == true && drops < 2 && level >= 900) {
                event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.BEETROOT));
                int newXp = 30;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                drops++;
            }

            final int dropExtra16 = 500;
            if (ProbabilityApi.probab(dropExtra16) == true && drops < 2 && level >= 850) {
                event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.POISONOUS_POTATO));
                int newXp = 25;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                drops++;
            }

            final int dropExtra15 = 750;
            if (ProbabilityApi.probab(dropExtra15) == true && drops < 2 && level >= 800) {
                event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.CHORUS_FRUIT));
                int newXp = 20;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                drops++;
            }

            final int dropExtra14 = 1000;
            if (ProbabilityApi.probab(dropExtra14) == true && drops < 2 && level >= 750) {
                if (event.getBlock().getType().equals(Material.RED_SAND) || event.getBlock().getType().equals(Material.SAND) || event.getBlock().getType().equals(Material.CLAY)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.LILY_PAD));
                    int newXp = 15;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            newXp += (newXp * 25) / 100;
                        } else {
                            newXp += (newXp * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                    drops++;
                }
            }

            final int dropExtra13 = 1250;
            if (ProbabilityApi.probab(dropExtra13) == true && drops < 2 && level >= 700) {
                if (event.getBlock().getType().equals(Material.CLAY) || event.getBlock().getType().equals(Material.SAND) || event.getBlock().getType().equals(Material.GRASS)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.SCUTE));
                    int newXp = 10;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            newXp += (newXp * 25) / 100;
                        } else {
                            newXp += (newXp * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                    drops++;
                }
            }

            final int dropExtra12 = 1500;
            if (ProbabilityApi.probab(dropExtra12) == true && drops < 2 && level >= 650) {
                if (event.getBlock().getType().equals(Material.GRASS) || event.getBlock().getType().equals(Material.MYCELIUM) || event.getBlock().getType().equals(Material.DIRT) || event.getBlock().getType().equals(Material.SAND) || event.getBlock().getType().equals(Material.RED_SAND)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.COBWEB));
                    int newXp = 8;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            newXp += (newXp * 25) / 100;
                        } else {
                            newXp += (newXp * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                    drops++;
                }
            }

            final int dropExtra11 = 1750;
            if (ProbabilityApi.probab(dropExtra11) == true && drops < 2 && level >= 600) {
                if (event.getBlock().getType().equals(Material.SAND) || event.getBlock().getType().equals(Material.CLAY)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.SEAGRASS));
                    int newXp = 8;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            newXp += (newXp * 25) / 100;
                        } else {
                            newXp += (newXp * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                    drops++;
                }
            }

            final int dropExtra10 = 1750;
            if (ProbabilityApi.probab(dropExtra10) == true && drops < 2 && level >= 550) {
                if (event.getBlock().getType().equals(Material.SAND)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.CLOCK));
                    int newXp = 7;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            newXp += (newXp * 25) / 100;
                        } else {
                            newXp += (newXp * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                    drops++;
                }
            }

            final int dropExtra9 = 2000;
            if (ProbabilityApi.probab(dropExtra9) == true && drops < 2 && level >= 500) {
                if (event.getBlock().getType().equals(Material.CLAY) || event.getBlock().getType().equals(Material.SAND)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.BUCKET));
                    int newXp = 7;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            newXp += (newXp * 25) / 100;
                        } else {
                            newXp += (newXp * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                    drops++;
                }
            }

            final int dropExtra20 = 2000;
            if (ProbabilityApi.probab(dropExtra20) == true && drops < 2 && level >= 400) {
                if (event.getBlock().getType().equals(Material.CLAY) || event.getBlock().getType().equals(Material.SAND) || event.getBlock().getType().equals(Material.GRASS)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.DRIED_KELP));
                    int newXp = 5;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            newXp += (newXp * 25) / 100;
                        } else {
                            newXp += (newXp * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                    drops++;
                }
            }

            final int dropExtra7 = 2000;
            if (ProbabilityApi.probab(dropExtra7) == true && drops < 2 && level >= 350) {
                if (event.getBlock().getType().equals(Material.GRASS) || event.getBlock().getType().equals(Material.DIRT) || event.getBlock().getType().equals(Material.MYCELIUM) || event.getBlock().getType().equals(Material.RED_SAND) || event.getBlock().getType().equals(Material.SAND)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.GOLD_INGOT));
                    int newXp = 5;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            newXp += (newXp * 25) / 100;
                        } else {
                            newXp += (newXp * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                    drops++;
                }
            }

            final int dropExtra21 = 2000;
            if (ProbabilityApi.probab(dropExtra21) == true && drops < 2 && level >= 350) {
                if (event.getBlock().getType().equals(Material.GRASS) || event.getBlock().getType().equals(Material.DIRT) || event.getBlock().getType().equals(Material.MYCELIUM) || event.getBlock().getType().equals(Material.RED_SAND) || event.getBlock().getType().equals(Material.SAND)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.GOLD_INGOT));
                    int newXp = 5;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            newXp += (newXp * 25) / 100;
                        } else {
                            newXp += (newXp * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                    drops++;
                }
            }

            final int dropExtra6 = 2000;
            if (ProbabilityApi.probab(dropExtra6) == true && drops < 2 && level >= 300) {
                if (event.getBlock().getType().equals(Material.GRASS) || event.getBlock().getType().equals(Material.DIRT) || event.getBlock().getType().equals(Material.MYCELIUM) || event.getBlock().getType().equals(Material.RED_SAND) || event.getBlock().getType().equals(Material.SAND)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.DIAMOND));
                    int newXp = 5;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            newXp += (newXp * 25) / 100;
                        } else {
                            newXp += (newXp * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                    drops++;
                }
            }

            final int dropExtra5 = 5000;
            if (ProbabilityApi.probab(dropExtra5) == true && drops < 2 && level >= 250) {
                if (event.getBlock().getType().equals(Material.GRASS) || event.getBlock().getType().equals(Material.DIRT) || event.getBlock().getType().equals(Material.MYCELIUM)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.APPLE));
                    int newXp = 3;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            newXp += (newXp * 25) / 100;
                        } else {
                            newXp += (newXp * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                    drops++;
                }
            }

            final int dropExtra4 = 7000;
            if (ProbabilityApi.probab(dropExtra4) == true && drops < 2 && level >= 200) {
                if (event.getBlock().getType().equals(Material.GRASS) || event.getBlock().getType().equals(Material.DIRT)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.EGG));
                    int newXp = 3;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            newXp += (newXp * 25) / 100;
                        } else {
                            newXp += (newXp * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                    drops++;
                }
            }

            final int dropExtra3 = 7000;
            if (ProbabilityApi.probab(dropExtra3) == true && drops < 2 && level >= 150) {
                if (event.getBlock().getType().equals(Material.CLAY) || event.getBlock().getType().equals(Material.GRAVEL)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.BONE));
                    int newXp = 2;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            newXp += (newXp * 25) / 100;
                        } else {
                            newXp += (newXp * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                    drops++;
                }
            }

            final int dropExtra2 = 7000;
            if (ProbabilityApi.probab(dropExtra2) == true && drops < 2 && level >= 100) {
                if (event.getBlock().getType().equals(Material.CLAY) || event.getBlock().getType().equals(Material.RED_SAND) || event.getBlock().getType().equals(Material.SAND)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.SLIME_BALL));
                    int newXp = 2;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            newXp += (newXp * 25) / 100;
                        } else {
                            newXp += (newXp * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                    drops++;
                }
            }

            final int dropExtra1 = 10000;
            if (ProbabilityApi.probab(dropExtra1) == true && drops < 2 && level >= 50) {
                if (event.getBlock().getType().equals(Material.GRASS) || event.getBlock().getType().equals(Material.DIRT) || event.getBlock().getType().equals(Material.RED_SAND) || event.getBlock().getType().equals(Material.SAND)) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.GLOWSTONE_DUST));
                    int newXp = 2;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            newXp += (newXp * 25) / 100;
                        } else {
                            newXp += (newXp * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Escavacao", xpAnterior + newXp);
                    drops++;
                }
            }
        }
    }
}
