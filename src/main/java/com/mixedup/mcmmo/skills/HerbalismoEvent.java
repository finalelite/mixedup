package com.mixedup.mcmmo.skills;

import com.mixedup.Main;
import com.mixedup.MySql;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.hashs.NexusUtil;
import com.mixedup.mcmmo.McMMOApi;
import com.mixedup.mcmmo.ProbabilityApi;
import com.mixedup.mcmmo.utils.BoosterHash;
import com.mixedup.plots.espaconave.EspaçonaveAPI;
import com.mixedup.plots.espaconave.EspaçonaveUtil;
import com.mixedup.plots.ilha.IlhaAPI;
import com.mixedup.plots.ilha.IlhaUtil;
import com.mixedup.plots.terrain.TerrainInfosAPI;
import com.mixedup.plots.terrain.TerrainsUtil;
import com.mixedup.utils.AlternateColor;
import com.mixedup.worldProtection.hashs.ProtectionUtil;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Crops;
import org.bukkit.material.MaterialData;
import org.bukkit.metadata.FixedMetadataValue;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HerbalismoEvent implements Listener {

   // public static void createInfo(final String UUID, final String location) {
    //        try {
    //            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Plantacoes_data(UUID, Location) VALUES (?, ?)");
    //            st.setString(1, UUID);
    //            st.setString(2, location);
    //            st.executeUpdate();
    //        } catch (final SQLException e) {
    //            e.printStackTrace();
    //        }
    //    }
    //
    //    public static void removeInfo(final String location) {
    //        try {
    //            final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Plantacoes_data WHERE Location = ?");
    //            st.setString(1, location);
    //            st.executeUpdate();
    //        } catch (final SQLException e) {
    //            e.printStackTrace();
    //        }
    //    }
    //
    //    public static String getUUID(final String location) {
    //        try {
    //            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Plantacoes_data WHERE Location = ?");
    //            st.setString(1, location);
    //            final ResultSet rs = st.executeQuery();
    //            while (rs.next()) {
    //                return rs.getString("UUID");
    //            }
    //        } catch (final SQLException e) {
    //            e.printStackTrace();
    //        }
    //        return null;
    //    }

    public static void verifyLevelUp(final Player player, final int xpAnterior, int xpNew) {
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (BoosterHash.get(UUID) != null) {
            if (BoosterHash.get(UUID).gettime() - (new Date().getTime() / 1000) <= 0) {
                BoosterHash.CACHE.remove(UUID);
            }
        }

        final int xp = McMMOApi.getXP(UUID + "Herbalismo");
        if (McMMOApi.getProximoNivelXP(UUID + "Herbalismo") <= xp) {
            McMMOApi.updateLevel(UUID + "Herbalismo");

            final int nivel = McMMOApi.getNivel(UUID + "Herbalismo");
            if (nivel == 100 || nivel == 200 || nivel == 300 || nivel == 400 || nivel == 500
                    || nivel == 600 || nivel == 700 || nivel == 800 || nivel == 900 || nivel == 1000) {
                for (final Player target : Bukkit.getOnlinePlayers()) {
                    if (nivel != 1000) {
                        target.sendMessage(AlternateColor.alternate("\n \n &e* " + player.getName() + " &ealcançaste nível " + (nivel)) + "&e na habilidade de Herbalismo!&e!\n \n");
                        if (target.getName().equals(player.getName())) {
                            target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                        } else {
                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        target.sendMessage(AlternateColor.alternate("\n \n &e* Incrível\n &e* " + player.getName() + " &eatingiu o nível máximo na habilidade de Herbalismo!\n \n"));
                        if (target.getName().equals(player.getName())) {
                            target.playSound(target.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                            target.playSound(target.getLocation().add(10, 0, 10), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                        } else {
                            target.playSound(target.getLocation().add(10, 0, 10), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                        }
                    }
                }
            } else {
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0f);
                player.sendMessage(ChatColor.YELLOW + " * Você acabou de subir para o nível " + AlternateColor.alternate("&e" + (nivel)) + ChatColor.YELLOW + " na habilidade de Herbalismo!");
            }
        } else {
            player.sendActionBar(ChatColor.GREEN + "Herbalismo (" + (xp) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Herbalismo")) + ")XP +" + (xpNew) + "XP");
        }
    }

    //@EventHandler
    //    public void onGrow(BlockGrowEvent event) {
    //        if (event.getBlock().getWorld().getName().equalsIgnoreCase("aether") || event.getBlock().getWorld().getName().equalsIgnoreCase("world") || ProtectionUtil.playerInArea(event.getBlock().getLocation()) != null) return;
    //        Block block = event.getBlock();
    //        MaterialData data = block.getState().getData();
    //        Crops crop = new Crops();
    //        crop.setData(data.getData());
    //        String location = block.getWorld().getName() + ":" + (block.getLocation().getBlockX()) + ":" + (block.getLocation().getBlockY()) + ":" + (block.getLocation().getBlockZ());
    //        if (getUUID(location) == null) return;
    //        String UUID = getUUID(location);
    //
    //        if (BoosterHash.get(UUID) != null) {
    //            if (BoosterHash.get(UUID).gettime() - (new Date().getTime() / 1000) <= 0) {
    //                BoosterHash.CACHE.remove(UUID);
    //            }
    //        }
    //
    //        if (block.getType().equals(Material.NETHER_WART)) {
    //            int probability = McMMOApi.getNivel(UUID + "Herbalismo") * 30;
    //            if (ProbabilityApi.probab(probability) == true) {
    //                Crops data1 = new Crops();
    //                data1.setState(CropState.RIPE);
    //                event.getNewState().setData(data1);
    //                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
    //                    @Override
    //                    public void run() {
    //                        block.getState().update(true);
    //                        event.getNewState().update(true);
    //                    }
    //                }, 1L);
    //                block.getLocation().getWorld().playSound(block.getLocation(), Sound.BLOCK_GRASS_BREAK, 1.0f, 1.0f);
    //            }
    //        }
    //        if (block.getType().equals(Material.COCOA)) {
    //            int probability = McMMOApi.getNivel(UUID + "Herbalismo") * 30;
    //            if (ProbabilityApi.probab(probability) == true) {
    //                Crops data1 = new Crops();
    //                data1.setState(CropState.SMALL);
    //                event.getNewState().setData(data1);
    //                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
    //                    @Override
    //                    public void run() {
    //                        block.getState().update(true);
    //                        event.getNewState().update(true);
    //                    }
    //                }, 1L);
    //                block.getLocation().getWorld().playSound(block.getLocation(), Sound.BLOCK_GRASS_BREAK, 1.0f, 1.0f);
    //            }
    //        }
    //        if (block.getType().equals(Material.CARROTS)) {
    //            int probability = McMMOApi.getNivel(UUID + "Herbalismo") * 30;
    //            if (ProbabilityApi.probab(probability) == true) {
    //                Crops data1 = new Crops();
    //                data1.setState(CropState.RIPE);
    //                event.getNewState().setData(data1);
    //                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
    //                    @Override
    //                    public void run() {
    //                        block.getState().update(true);
    //                        event.getNewState().update(true);
    //                    }
    //                }, 1L);
    //                block.getLocation().getWorld().playSound(block.getLocation(), Sound.BLOCK_GRASS_BREAK, 1.0f, 1.0f);
    //            }
    //        }
    //        if (block.getType().equals(Material.POTATOES)) {
    //            int probability = McMMOApi.getNivel(UUID + "Herbalismo") * 30;
    //            if (ProbabilityApi.probab(probability) == true) {
    //                Crops data1 = new Crops();
    //                data1.setState(CropState.RIPE);
    //                event.getNewState().setData(data1);
    //                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
    //                    @Override
    //                    public void run() {
    //                        block.getState().update(true);
    //                        event.getNewState().update(true);
    //                    }
    //                }, 1L);
    //                block.getLocation().getWorld().playSound(block.getLocation(), Sound.BLOCK_GRASS_BREAK, 1.0f, 1.0f);
    //            }
    //        }
    //        if (block.getType().equals(Material.BEETROOTS)) {
    //            int probability = McMMOApi.getNivel(UUID + "Herbalismo") * 30;
    //            if (ProbabilityApi.probab(probability) == true) {
    //                Crops data1 = new Crops();
    //                data1.setState(CropState.SMALL);
    //                event.getNewState().setData(data1);
    //                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
    //                    @Override
    //                    public void run() {
    //                        block.getState().update(true);
    //                        event.getNewState().update(true);
    //                    }
    //                }, 1L);
    //                block.getLocation().getWorld().playSound(block.getLocation(), Sound.BLOCK_GRASS_BREAK, 1.0f, 1.0f);
    //            }
    //        }
    //        if (block.getType().equals(Material.WHEAT)) {
    //            int probability = McMMOApi.getNivel(UUID + "Herbalismo") * 30;
    //            if (ProbabilityApi.probab(probability) == true) {
    //                Crops data1 = new Crops();
    //                data1.setState(CropState.RIPE);
    //                event.getNewState().setData(data1);
    //                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
    //                    @Override
    //                    public void run() {
    //                        block.getState().update(true);
    //                        event.getNewState().update(true);
    //                    }
    //                }, 1L);
    //                block.getLocation().getWorld().playSound(block.getLocation(), Sound.BLOCK_GRASS_BREAK, 1.0f, 1.0f);
    //            }
    //        }
    //    }

    @EventHandler
    public void onPlace(final BlockPlaceEvent event) {
        final Player player = event.getPlayer();
        final Block block = event.getBlock();
        final String UUID = PlayerUUID.getUUID(player.getName());

        block.setMetadata("placed", new FixedMetadataValue(Main.getInstance(), true));

        //if (block.getType().equals(Material.WHEAT) || block.getType().equals(Material.BEETROOTS) || block.getType().equals(Material.POTATOES) || block.getType().equals(Material.CARROTS) || block.getType().equals(Material.COCOA) || block.getType().equals(Material.NETHER_WART)) {
        //            String location = block.getWorld().getName() + ":" + (block.getLocation().getBlockX()) + ":" + (block.getLocation().getBlockY()) + ":" + (block.getLocation().getBlockZ());
        //            createInfo(UUID, location);
        //        }
    }

    @EventHandler
    public void onBreak(final BlockBreakEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getBlock().getWorld().getName().equalsIgnoreCase("spawn") || event.getBlock().getWorld().getName().equalsIgnoreCase("guildaselect") || event.getBlock().getWorld().getName().equalsIgnoreCase("aether") || ProtectionUtil.playerInArea(event.getBlock().getLocation()) != null)
            return;
        if (TerrainsUtil.playerInArea(event.getBlock().getLocation()) != null && !TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID().contains(UUID) && !TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).contains(UUID))
            return;
        if (IlhaUtil.playerInArea(event.getBlock().getLocation()) != null && !IlhaUtil.playerInArea(event.getBlock().getLocation()).getRegionID().contains(UUID) && !IlhaAPI.getAmigos(IlhaUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).contains(UUID))
            return;
        if (NexusUtil.playerInArea(event.getBlock().getLocation()) != null) return;

        if (event.getBlock().getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
            final String chunk = event.getBlock().getLocation().getChunk().getX() + ":" + event.getBlock().getLocation().getChunk().getZ();

            if (FacAPI.getChunkOwn(chunk) != null) {
                if (FacAPI.getFacNome(UUID) != null) {
                    if (FacAPI.getChunkOwn(chunk).equalsIgnoreCase(FacAPI.getFacNome(UUID))) {
                        if (FacAPI.getPermBuild(UUID) == false) {
                            return;
                        }
                    } else {
                        if (FacAPI.getAliados(FacAPI.getChunkOwn(chunk)) != null && FacAPI.getAliados(FacAPI.getChunkOwn(chunk)).contains(FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)))) {
                            if (FacAPI.getAliadoPermBuild(FacAPI.getTagWithNome(FacAPI.getChunkOwn(chunk)) + FacAPI.getTagWithNome(FacAPI.getFacNome(UUID))) == false) {
                                return;
                            }
                        } else {
                            return;
                        }
                    }
                } else {
                    return;
                }
            }
        }

        if (BoosterHash.get(UUID) != null) {
            if (BoosterHash.get(UUID).gettime() - (new Date().getTime() / 1000) <= 0) {
                BoosterHash.CACHE.remove(UUID);
            }
        }

        final Block block = event.getBlock();
        final MaterialData data = block.getState().getData();
        final Crops crop = new Crops();
        crop.setData(data.getData());
        if (event.getBlock().getType().equals(Material.DEAD_BUSH)) {
            if (!event.getBlock().getMetadata("placed").isEmpty()) return;

            final int nivel = McMMOApi.getNivel(UUID + "Herbalismo");
            int probability = nivel * 15;
            if (nivel < 100) {
                probability = 300;
            }
            if (ProbabilityApi.probab(probability) == true) {
                final ItemStack papel = new ItemStack(Material.PAPER);
                final ItemMeta meta = papel.getItemMeta();
                meta.setDisplayName(ChatColor.YELLOW + "Papiro com escritos");
                final List<String> lore = new ArrayList<>();
                lore.add(" ");
                lore.add(ChatColor.YELLOW + "Escritos: " + ChatColor.DARK_GRAY + "Super raro");
                lore.add(" ");
                meta.setLore(lore);
                papel.setItemMeta(meta);

                if (nivel >= 100) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), papel);
                }
            }
        }
        if (event.getBlock().getType().equals(Material.DANDELION) || event.getBlock().getType().equals(Material.POPPY) || event.getBlock().getType().equals(Material.BLUE_ORCHID)
                || event.getBlock().getType().equals(Material.ALLIUM) || event.getBlock().getType().equals(Material.AZURE_BLUET) || event.getBlock().getType().equals(Material.RED_TULIP)
                || event.getBlock().getType().equals(Material.ORANGE_TULIP) || event.getBlock().getType().equals(Material.WHITE_TULIP) || event.getBlock().getType().equals(Material.PINK_TULIP)
                || event.getBlock().getType().equals(Material.OXEYE_DAISY) || event.getBlock().getType().equals(Material.SUNFLOWER) || event.getBlock().getType().equals(Material.LILAC)
                || event.getBlock().getType().equals(Material.ROSE_BUSH) || event.getBlock().getType().equals(Material.PEONY)) {
            final int nivel = McMMOApi.getNivel(UUID + "Herbalismo");
            int probability = nivel * 15;
            if (nivel < 100) {
                probability = 300;
            }
            if (ProbabilityApi.probab(probability) == true) {
                if (!event.getBlock().getMetadata("placed").isEmpty()) return;

                final ItemStack papel = new ItemStack(Material.PAPER);
                final ItemMeta meta = papel.getItemMeta();
                meta.setDisplayName(ChatColor.YELLOW + "Papiro com escritos");
                final List<String> lore = new ArrayList<>();
                lore.add(" ");
                lore.add(ChatColor.YELLOW + "Escritos: " + ChatColor.DARK_GRAY + "Raro");
                lore.add(" ");
                meta.setLore(lore);
                papel.setItemMeta(meta);

                if (nivel >= 50) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), papel);
                }
            }
        }
        if (event.getBlock().getType().equals(Material.LARGE_FERN)) {
            if (!event.getBlock().getMetadata("placed").isEmpty()) return;

            final int nivel = McMMOApi.getNivel(UUID + "Herbalismo");
            if (nivel != 1000) {
                final int xpAnterior = McMMOApi.getXP(UUID + "Herbalismo");
                int newXp = 10;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Herbalismo", McMMOApi.getXP(UUID + "Herbalismo") + newXp);
                verifyLevelUp(player, xpAnterior, newXp);
            }

            int probability = nivel * 15;
            if (nivel < 100) {
                probability = 300;
            }
            if (ProbabilityApi.probab(probability) == true) {
                final ItemStack papel = new ItemStack(Material.PAPER);
                final ItemMeta meta = papel.getItemMeta();
                meta.setDisplayName(ChatColor.YELLOW + "Papiro com escritos");
                final List<String> lore = new ArrayList<>();
                lore.add(" ");
                lore.add(ChatColor.YELLOW + "Escritos: " + ChatColor.DARK_GRAY + "Comum");
                lore.add(" ");
                meta.setLore(lore);
                papel.setItemMeta(meta);

                if (nivel >= 10) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), papel);
                }
            }
        }
        if (event.getBlock().getType().equals(Material.FERN) || event.getBlock().getType().equals(Material.GRASS) || event.getBlock().getType().equals(Material.MELON) || event.getBlock().getType().equals(Material.WHEAT) || event.getBlock().getType().equals(Material.CARROTS)
                || event.getBlock().getType().equals(Material.POTATOES) || event.getBlock().getType().equals(Material.PUMPKIN) || event.getBlock().getType().equals(Material.NETHER_WART) || event.getBlock().getType().equals(Material.BEETROOTS)) {
            if (!event.getBlock().getMetadata("placed").isEmpty()) return;

            final int nivel = McMMOApi.getNivel(UUID + "Herbalismo");
            if (nivel != 1000) {
                final int xpAnterior = McMMOApi.getXP(UUID + "Herbalismo");
                int newXp = 10;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Herbalismo", McMMOApi.getXP(UUID + "Herbalismo") + newXp);
                verifyLevelUp(player, xpAnterior, newXp);
            }

            int probability = nivel * 15;
            if (nivel < 100) {
                probability = 300;
            }
            if (ProbabilityApi.probab(probability) == true) {
                final ItemStack papel = new ItemStack(Material.PAPER);
                final ItemMeta meta = papel.getItemMeta();
                meta.setDisplayName(ChatColor.YELLOW + "Papiro com escritos");
                final List<String> lore = new ArrayList<>();
                lore.add(" ");
                lore.add(ChatColor.YELLOW + "Escritos: " + ChatColor.DARK_GRAY + "Comum");
                lore.add(" ");
                meta.setLore(lore);
                papel.setItemMeta(meta);

                if (nivel >= 10) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), papel);
                }
            }
        }
        if (event.getBlock().getType().equals(Material.TALL_GRASS)) {
            if (!event.getBlock().getMetadata("placed").isEmpty()) return;

            final int nivel = McMMOApi.getNivel(UUID + "Herbalismo");
            if (nivel != 1000) {
                final int xpAnterior = McMMOApi.getXP(UUID + "Herbalismo");
                int newXp = 10;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Herbalismo", McMMOApi.getXP(UUID + "Herbalismo") + newXp);
                verifyLevelUp(player, xpAnterior, newXp);
            }

            int probability = nivel * 15;
            if (nivel < 100) {
                probability = 300;
            }
            if (ProbabilityApi.probab(probability) == true) {
                final ItemStack papel = new ItemStack(Material.PAPER);
                final ItemMeta meta = papel.getItemMeta();
                meta.setDisplayName(ChatColor.YELLOW + "Papiro com escritos");
                final List<String> lore = new ArrayList<>();
                lore.add(" ");
                lore.add(ChatColor.YELLOW + "Escritos: " + ChatColor.DARK_GRAY + "Comum");
                lore.add(" ");
                meta.setLore(lore);
                papel.setItemMeta(meta);

                if (nivel >= 250) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), papel);
                }
            }
        }
        if (event.getBlock().getType().equals(Material.GRASS)) {
            if (!event.getBlock().getMetadata("placed").isEmpty()) return;

            final int nivel = McMMOApi.getNivel(UUID + "Herbalismo");
            if (nivel != 1000) {
                final int xpAnterior = McMMOApi.getXP(UUID + "Herbalismo");
                int newXp = 10;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Herbalismo", McMMOApi.getXP(UUID + "Herbalismo") + newXp);
                verifyLevelUp(player, xpAnterior, newXp);
            }

            int probability = nivel * 15;
            if (nivel < 100) {
                probability = 300;
            }
            if (ProbabilityApi.probab(probability) == true) {
                final ItemStack papel = new ItemStack(Material.PAPER);
                final ItemMeta meta = papel.getItemMeta();
                meta.setDisplayName(ChatColor.YELLOW + "Papiro com escritos");
                final List<String> lore = new ArrayList<>();
                lore.add(" ");
                lore.add(ChatColor.YELLOW + "Escritos: " + ChatColor.DARK_GRAY + "Comum");
                lore.add(" ");
                meta.setLore(lore);
                papel.setItemMeta(meta);

                if (nivel >= 250) {
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), papel);
                }
            }
        }
        if (event.getBlock().getType().equals(Material.NETHER_WART)) {
            if (crop.getState() == CropState.SMALL) {
                final int nivel = McMMOApi.getNivel(UUID + "Herbalismo");
                if (nivel != 1000) {
                    final int xpAnterior = McMMOApi.getXP(UUID + "Herbalismo");
                    int newXp = 62;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            newXp += (newXp * 25) / 100;
                        } else {
                            newXp += (newXp * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Herbalismo", McMMOApi.getXP(UUID + "Herbalismo") + newXp);
                    verifyLevelUp(player, xpAnterior, newXp);
                }

                final int probability = nivel * 50;
                if (ProbabilityApi.probab(probability) == true) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            block.setType(Material.NETHER_WART);
                        }
                    }, 10L);
                } else {
                    final String location = block.getWorld().getName() + ":" + (block.getLocation().getBlockX()) + ":" + (block.getLocation().getBlockY()) + ":" + (block.getLocation().getBlockZ());
                    //removeInfo(location);
                }
            } else {
                final String location = block.getWorld().getName() + ":" + (block.getLocation().getBlockX()) + ":" + (block.getLocation().getBlockY()) + ":" + (block.getLocation().getBlockZ());
                //removeInfo(location);
            }
        }
        if (McMMOApi.getNivel(UUID + "Herbalismo") != 1000) {
            if (event.getBlock().getType().equals(Material.BROWN_MUSHROOM)) {
                if (!event.getBlock().getMetadata("placed").isEmpty()) return;

                final int xpAnterior = McMMOApi.getXP(UUID + "Herbalismo");
                int newXp = 10;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Herbalismo", McMMOApi.getXP(UUID + "Herbalismo") + newXp);
                verifyLevelUp(player, xpAnterior, newXp);
            }
            if (event.getBlock().getType().equals(Material.RED_MUSHROOM)) {
                if (!event.getBlock().getMetadata("placed").isEmpty()) return;

                final int xpAnterior = McMMOApi.getXP(UUID + "Herbalismo");
                int newXp = 10;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Herbalismo", McMMOApi.getXP(UUID + "Herbalismo") + newXp);
                verifyLevelUp(player, xpAnterior, newXp);
            }
            if (event.getBlock().getType().equals(Material.CACTUS)) {
                if (!event.getBlock().getMetadata("placed").isEmpty()) return;

                final int xpAnterior = McMMOApi.getXP(UUID + "Herbalismo");
                int newXp = 10;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Herbalismo", McMMOApi.getXP(UUID + "Herbalismo") + newXp);
                verifyLevelUp(player, xpAnterior, newXp);
            }
            if (event.getBlock().getType().equals(Material.LILY_PAD)) {
                if (!event.getBlock().getMetadata("placed").isEmpty()) return;

                final int xpAnterior = McMMOApi.getXP(UUID + "Herbalismo");
                int newXp = 10;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Herbalismo", McMMOApi.getXP(UUID + "Herbalismo") + newXp);
                verifyLevelUp(player, xpAnterior, newXp);
            }
            if (event.getBlock().getType().equals(Material.CHORUS_FRUIT)) {
                if (!event.getBlock().getMetadata("placed").isEmpty()) return;

                final int xpAnterior = McMMOApi.getXP(UUID + "Herbalismo");
                int newXp = 62;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Herbalismo", McMMOApi.getXP(UUID + "Herbalismo") + newXp);
                verifyLevelUp(player, xpAnterior, newXp);
            }
        }
        if (event.getBlock().getType().equals(Material.COCOA)) {
            if (crop.getState() == CropState.SMALL) {
                final int nivel = McMMOApi.getNivel(UUID + "Herbalismo");
                if (nivel != 1000) {
                    final int xpAnterior = McMMOApi.getXP(UUID + "Herbalismo");
                    int newXp = 50;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            newXp += (newXp * 25) / 100;
                        } else {
                            newXp += (newXp * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Herbalismo", McMMOApi.getXP(UUID + "Herbalismo") + newXp);
                    verifyLevelUp(player, xpAnterior, newXp);
                }

                final int probability = nivel * 50;
                if (ProbabilityApi.probab(probability) == true) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            block.setType(Material.COCOA);
                        }
                    }, 10L);
                } else {
                    final String location = block.getWorld().getName() + ":" + (block.getLocation().getBlockX()) + ":" + (block.getLocation().getBlockY()) + ":" + (block.getLocation().getBlockZ());
                    //removeInfo(location);
                }
            } else {
                final String location = block.getWorld().getName() + ":" + (block.getLocation().getBlockX()) + ":" + (block.getLocation().getBlockY()) + ":" + (block.getLocation().getBlockZ());
                //removeInfo(location);
            }
        }
        if (event.getBlock().getType().equals(Material.SUGAR_CANE)) {
            if (!event.getBlock().getMetadata("placed").isEmpty()) return;

            if (McMMOApi.getNivel(UUID + "Herbalismo") != 1000) {
                final int xpAnterior = McMMOApi.getXP(UUID + "Herbalismo");
                int newXp = 45;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Herbalismo", McMMOApi.getXP(UUID + "Herbalismo") + newXp);
                verifyLevelUp(player, xpAnterior, newXp);
            }
        }
        if (event.getBlock().getType().equals(Material.CARROTS)) {
            if (crop.getState() == CropState.RIPE) {
                final int nivel = McMMOApi.getNivel(UUID + "Herbalismo");
                if (nivel != 1000) {
                    final int xpAnterior = McMMOApi.getXP(UUID + "Herbalismo");
                    int newXp = 48;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            newXp += (newXp * 25) / 100;
                        } else {
                            newXp += (newXp * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Herbalismo", McMMOApi.getXP(UUID + "Herbalismo") + newXp);
                    verifyLevelUp(player, xpAnterior, newXp);
                }

                final int probability = nivel * 50;
                if (ProbabilityApi.probab(probability) == true) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            block.setType(Material.CARROTS);
                        }
                    }, 10L);
                } else {
                    final String location = block.getWorld().getName() + ":" + (block.getLocation().getBlockX()) + ":" + (block.getLocation().getBlockY()) + ":" + (block.getLocation().getBlockZ());
                    //removeInfo(location);
                }
            } else {
                final String location = block.getWorld().getName() + ":" + (block.getLocation().getBlockX()) + ":" + (block.getLocation().getBlockY()) + ":" + (block.getLocation().getBlockZ());
                //removeInfo(location);
            }
        }
        if (event.getBlock().getType().equals(Material.PUMPKIN)) {
            if (!event.getBlock().getMetadata("placed").isEmpty()) return;

            if (McMMOApi.getNivel(UUID + "Herbalismo") != 1000) {
                final int xpAnterior = McMMOApi.getXP(UUID + "Herbalismo");
                int newXp = 45;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Herbalismo", McMMOApi.getXP(UUID + "Herbalismo") + newXp);
                verifyLevelUp(player, xpAnterior, newXp);
            }
        }
        if (event.getBlock().getType().equals(Material.POTATOES)) {
            if (crop.getState() == CropState.RIPE) {
                final int nivel = McMMOApi.getNivel(UUID + "Herbalismo");
                if (nivel != 1000) {
                    final int xpAnterior = McMMOApi.getXP(UUID + "Herbalismo");
                    int newXp = 43;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            newXp += (newXp * 25) / 100;
                        } else {
                            newXp += (newXp * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Herbalismo", McMMOApi.getXP(UUID + "Herbalismo") + newXp);
                    verifyLevelUp(player, xpAnterior, newXp);
                }

                final int probability = nivel * 50;
                if (ProbabilityApi.probab(probability) == true) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            block.setType(Material.POTATOES);
                        }
                    }, 10L);
                } else {
                    final String location = block.getWorld().getName() + ":" + (block.getLocation().getBlockX()) + ":" + (block.getLocation().getBlockY()) + ":" + (block.getLocation().getBlockZ());
                    //removeInfo(location);
                }
            } else {
                final String location = block.getWorld().getName() + ":" + (block.getLocation().getBlockX()) + ":" + (block.getLocation().getBlockY()) + ":" + (block.getLocation().getBlockZ());
                //removeInfo(location);
            }
        }
        if (event.getBlock().getType().equals(Material.MELON)) {
            if (!event.getBlock().getMetadata("placed").isEmpty()) return;

            if (McMMOApi.getNivel(UUID + "Herbalismo") != 1000) {
                final int xpAnterior = McMMOApi.getXP(UUID + "Herbalismo");
                int newXp = 55;
                if (BoosterHash.get(UUID) != null) {
                    if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                        newXp += (newXp * 25) / 100;
                    } else {
                        newXp += (newXp * 50) / 100;
                    }

                    if (BoosterHash.get(UUID).gettime() >= (new Date().getTime() / 100)) {
                        BoosterHash.CACHE.remove(UUID);
                    }
                }
                McMMOApi.updateXP(UUID + "Herbalismo", McMMOApi.getXP(UUID + "Herbalismo") + newXp);
                verifyLevelUp(player, xpAnterior, newXp);
            }
        }
        if (event.getBlock().getType().equals(Material.BEETROOTS)) {
            if (crop.getState() == CropState.SMALL) {
                final int nivel = McMMOApi.getNivel(UUID + "Herbalismo");
                if (nivel != 1000) {
                    final int xpAnterior = McMMOApi.getXP(UUID + "Herbalismo");
                    int newXp = 45;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            newXp += (newXp * 25) / 100;
                        } else {
                            newXp += (newXp * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Herbalismo", McMMOApi.getXP(UUID + "Herbalismo") + newXp);
                    verifyLevelUp(player, xpAnterior, newXp);
                }

                final int probability = nivel * 50;
                if (ProbabilityApi.probab(probability) == true) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            block.setType(Material.BEETROOTS);
                        }
                    }, 10L);
                } else {
                    final String location = block.getWorld().getName() + ":" + (block.getLocation().getBlockX()) + ":" + (block.getLocation().getBlockY()) + ":" + (block.getLocation().getBlockZ());
                    //removeInfo(location);
                }
            } else {
                final String location = block.getWorld().getName() + ":" + (block.getLocation().getBlockX()) + ":" + (block.getLocation().getBlockY()) + ":" + (block.getLocation().getBlockZ());
                //removeInfo(location);
            }
        }
        if (event.getBlock().getType().equals(Material.WHEAT)) {
            if (crop.getState() == CropState.RIPE) {
                final int nivel = McMMOApi.getNivel(UUID + "Herbalismo");
                if (nivel != 1000) {
                    final int xpAnterior = McMMOApi.getXP(UUID + "Herbalismo");
                    int newXp = 65;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("normal")) {
                            newXp += (newXp * 25) / 100;
                        } else {
                            newXp += (newXp * 50) / 100;
                        }

                    }
                    McMMOApi.updateXP(UUID + "Herbalismo", McMMOApi.getXP(UUID + "Herbalismo") + newXp);
                    verifyLevelUp(player, xpAnterior, newXp);
                }

                final int probability = nivel * 50;
                if (ProbabilityApi.probab(probability) == true) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            block.setType(Material.WHEAT);
                        }
                    }, 10L);
                } else {
                    final String location = block.getWorld().getName() + ":" + (block.getLocation().getBlockX()) + ":" + (block.getLocation().getBlockY()) + ":" + (block.getLocation().getBlockZ());
                    //removeInfo(location);
                }
            } else {
                final String location = block.getWorld().getName() + ":" + (block.getLocation().getBlockX()) + ":" + (block.getLocation().getBlockY()) + ":" + (block.getLocation().getBlockZ());
                //removeInfo(location);
            }
        }
    }
}
