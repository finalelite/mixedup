package com.mixedup.mcmmo.commands;

import com.mixedup.mcmmo.McMMOApi;
import com.mixedup.utils.CentralizeMsg;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandMineracao implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        Player player = (Player) sender;
        String UUID = player.getUniqueId().toString();

        if (cmd.getName().equalsIgnoreCase("mineracao")) {
            final String line1 = CentralizeMsg.sendCenteredMessage("&e&lMineração");
            final String line2 = CentralizeMsg.sendCenteredMessage("&eGanha experiência: &7Minerando");
            final String line3 = CentralizeMsg.sendCenteredMessage("&eNível: &7" + (McMMOApi.getNivel(UUID + "Mineracao")) + " &bXP(" + (McMMOApi.getXP(UUID + "Mineracao")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Mineracao")) + ")");
            final String line4 = CentralizeMsg.sendCenteredMessage("&e&lEfeitos");
            final String line5 = CentralizeMsg.sendCenteredMessage("&eDrops duplos: &7Recebe o dobro de minérios");
            final String line6 = CentralizeMsg.sendCenteredMessage("&e&lSeus status");
            final String line7 = CentralizeMsg.sendCenteredMessage("&eChance de drops duplos: &7" + (McMMOApi.getNivel(UUID + "Mineracao") * 0.07) + "%");
            player.sendMessage("\n" + line1 + "\n" + line2 + "\n" + line3 + "\n \n" + line4 + "\n" + line5 + "\n \n" + line6 + "\n" + line7 + "\n ");
            return true;
        }

        return false;
    }
}
