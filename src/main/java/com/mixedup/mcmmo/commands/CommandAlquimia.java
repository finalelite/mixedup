package com.mixedup.mcmmo.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandAlquimia implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        Player player = (Player) sender;
        String UUID = player.getUniqueId().toString();

        //&c--------[&aAlquimia&c]--------
        //&3Ganha experiência: &2Fazendo poções
        //&3Nível: &2100 XP&e(&61.000&e/&61.020&e)
        //&c--------[&aEfeitos&c]--------
        //&3Velocidade (300+): &2Aumenta a velocidade de fazer porções
        //&3Multipotions (150+): &2Faz poções com mais ingredientes
        //&c--------[&aSeus Status&c]--------
        //&3Multipotions nível: &21&e/&214
        //&3Ingredientes: &2Fungo do Nether, Olho de Aranha, Olho de Aranha Fermentado, Lágrimas de Ghast, Pé de Coelho, Pó de Blaze,
        // Melancia Reluzente, Açúcar, Creme de Magma, Peixe Bolha, Cenoura Dourada, Capacete de Tartaruga, Membrana de Espectro,
        // Vitória-Régia, Gold Nugget, Maçã, Cana de Açúcar, Carne Podre, Cogumelo Marrom, Cogumelo Vermelho, Cenoura, Bola de Slime,
        // Chorus Fruit, Maçã Dourada, Bloco de Diamante, Ovo de Tartaruga e Olho do Ender.

        if (cmd.getName().equalsIgnoreCase("alquimia")) {
            return true;
        }

        return false;
    }
}
