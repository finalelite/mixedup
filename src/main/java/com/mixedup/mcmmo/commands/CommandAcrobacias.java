package com.mixedup.mcmmo.commands;

import com.mixedup.mcmmo.McMMOApi;
import com.mixedup.utils.CentralizeMsg;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandAcrobacias implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        Player player = (Player) sender;
        String UUID = player.getUniqueId().toString();

        if (cmd.getName().equalsIgnoreCase("Acrobacia")) {
            final String line1 = CentralizeMsg.sendCenteredMessage("&e&lAcrobacias");
            final String line2 = CentralizeMsg.sendCenteredMessage("&eNível: &7" + (McMMOApi.getNivel(UUID + "Acrobacias")) + " &bXP(" + (McMMOApi.getXP(UUID + "Acrobacias")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Acrobacias")) + ")");
            final String line3 = CentralizeMsg.sendCenteredMessage("&e&lEfeitos");
            final String line4 = CentralizeMsg.sendCenteredMessage("&eRolar: &7Diminui ou anula o dano de queda");
            final String line5 = CentralizeMsg.sendCenteredMessage("&eRolar perfeitamente: &7Duas vezes mais eficaz que o Rolar");
            final String line6 = CentralizeMsg.sendCenteredMessage("&e&lSeus status");
            final String line7 = CentralizeMsg.sendCenteredMessage("&eChance de rolar: &7" + (McMMOApi.getNivel(UUID + "Acrobacias") * 0.1) + "%");
            final String line8 = CentralizeMsg.sendCenteredMessage("&eChance de rolar perfeitamente: &7" + ((McMMOApi.getNivel(UUID + "Acrobacias") * 0.1)) + "%");
            player.sendMessage("\n" + line1 + "\n" + line2 + "\n \n" + line3 + "\n" + line4 + "\n" + line5 + "\n \n" + line6 + "\n" + line7 + "\n" + line8 + "\n ");
            return true;
        }
        
        return false;
    }
}
