package com.mixedup.mcmmo.commands;

import com.mixedup.Main;
import com.mixedup.apis.AccountAPI;
import com.mixedup.apis.HorarioAPI;
import com.mixedup.mcmmo.McMMOApi;
import com.mixedup.mcmmo.utils.BoosterHash;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.mixedup.MySql;
import com.mixedup.mcmmo.utils.TopSkillHash;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.AbstractMap.SimpleEntry;
import java.util.concurrent.TimeUnit;

public class CommandHabilidades implements CommandExecutor {

    public static LoadingCache<SimpleEntry<String, String>, Long> cache;
    public static LoadingCache<String, String> top;

    static {
        clearCache();
    }

    public static void clearCache() {
        cache = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(30, TimeUnit.MINUTES)
                .build(new CacheLoader<SimpleEntry<String, String>, Long>() {
                    public Long load(final SimpleEntry<String, String> infos) {
                        return getNivelMcMMOFromDatabase(infos.getKey(), infos.getValue());
                    }
                });

        top = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(30, TimeUnit.MINUTES)
                .build(new CacheLoader<String, String>() {
                    public String load(final String name) {
                        return getTopMcMMODb(name);
                    }
                });
    }

    public static String getTopMcMMO(final String skill) {
        try {
            return top.get(skill);
        } catch (Exception e) {
            return "Ninguém";
        }
    }

    public static String getTopMcMMODb(final String skill) {
        try (final PreparedStatement stmt = MySql.con.prepareStatement("SELECT UUID FROM McMMO_data WHERE UUID LIKE ? ORDER BY Nivel DESC LIMIT 1")) {
            stmt.setString(1, "%" + skill);
            final ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                final String uuid = rs.getString("UUID").substring(0, 36);

                return uuid;
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return "Ninguém";
    }

    public static long getNivelMcMMO(final String uuid, final String skill) {
        try {
            return cache.get(new SimpleEntry<>(uuid, skill));
        } catch (final Exception e) {
            return -1;
        }
    }

    public static long getNivelMcMMOFromDatabase(final String uuid, final String skill) {
        try (final PreparedStatement stmt = MySql.con.prepareStatement("SELECT * FROM(SELECT (@pos:=@pos+1) pos,UUID FROM" +
                "(SELECT Nivel, UUID FROM McMMO_data WHERE UUID LIKE ?) A ORDER BY Nivel DESC) AA WHERE UUID = ?;")) {
            stmt.execute("SET @pos = 0;");

            stmt.setString(1, "%" + skill);
            stmt.setString(2, uuid + skill);
            final ResultSet rs = stmt.executeQuery();
            if (rs.next())
                return rs.getLong("pos");
            else
                return -3;
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return -2;

    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        Player player = (Player) sender;
        String UUID = player.getUniqueId().toString();

        if (cmd.getName().equalsIgnoreCase("habilidades")) {
            Date before = new Date();
            new BukkitRunnable() {
                @Override
                public void run() {
                    final Inventory inventory = Bukkit.createInventory(null, 4 * 9, "Habilidades McMMO: ");

                    final String boosterm;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("vip")) {
                            boosterm = HorarioAPI.getTime(BoosterHash.get(UUID).gettime() - (new Date().getTime() / 1000));
                        } else {
                            boosterm = "Nenhum";
                        }
                    } else {
                        boosterm = "Nenhum";
                    }

                    final String boosterv;
                    if (BoosterHash.get(UUID) != null) {
                        if (BoosterHash.get(UUID).getType().equalsIgnoreCase("membro")) {
                            boosterv = HorarioAPI.getTime(BoosterHash.get(UUID).gettime() - (new Date().getTime() / 1000));
                        } else {
                            boosterv = "Nenhum";
                        }
                    } else {
                        boosterv = "Nenhum";
                    }

                    final ItemStack escavacao = new ItemStack(Material.DIAMOND_SHOVEL);
                    final ItemMeta metaEscavacao = escavacao.getItemMeta();
                    metaEscavacao.setDisplayName(ChatColor.YELLOW + "Escavação");
                    final List<String> lore9 = new ArrayList<>();
                    lore9.add(ChatColor.WHITE + "Nível: " + ChatColor.GRAY + (McMMOApi.getNivel(UUID + "Escavacao")) + "/700");
                    lore9.add(" ");
                    lore9.add(ChatColor.WHITE + "Booster " + ChatColor.GREEN + "(VIP)" + ChatColor.WHITE + ": " + ChatColor.GRAY + boosterm);
                    lore9.add(ChatColor.WHITE + "Booster: " + ChatColor.GRAY + boosterv);
                    lore9.add(" ");
                    lore9.add(ChatColor.WHITE + "Colocação no rank: " + ChatColor.GRAY + (getNivelMcMMO(UUID, "Escavacao")) + "º");
                    lore9.add(ChatColor.WHITE + "Primeiro colocado: " + ChatColor.GRAY + AccountAPI.getNick(getTopMcMMO("Escavacao")));
                    metaEscavacao.setLore(lore9);
                    escavacao.setItemMeta(metaEscavacao);

                    final ItemStack machado = new ItemStack(Material.DIAMOND_AXE);
                    final ItemMeta metaMachado = machado.getItemMeta();
                    metaMachado.setDisplayName(ChatColor.YELLOW + "Machado");
                    final List<String> lore8 = new ArrayList<>();
                    lore8.add(ChatColor.WHITE + "Nível: " + ChatColor.GRAY + (McMMOApi.getNivel(UUID + "Machado")) + "/800");
                    lore8.add(" ");
                    lore8.add(ChatColor.WHITE + "Booster " + ChatColor.GREEN + "(VIP)" + ChatColor.WHITE + ": " + ChatColor.GRAY + boosterm);
                    lore8.add(ChatColor.WHITE + "Booster: " + ChatColor.GRAY + boosterv);
                    lore8.add(" ");
                    lore8.add(ChatColor.WHITE + "Colocação no rank: " + ChatColor.GRAY + (getNivelMcMMO(UUID, "Machado")) + "º");
                    lore8.add(ChatColor.WHITE + "Primeiro colocado: " + ChatColor.GRAY + AccountAPI.getNick(getTopMcMMO("Machado")));
                    metaMachado.setLore(lore8);
                    machado.setItemMeta(metaMachado);

                    final ItemStack mineracao = new ItemStack(Material.DIAMOND_PICKAXE);
                    final ItemMeta metaMineracao = mineracao.getItemMeta();
                    metaMineracao.setDisplayName(ChatColor.YELLOW + "Mineração");
                    final List<String> lore7 = new ArrayList<>();
                    lore7.add(ChatColor.WHITE + "Nível: " + ChatColor.GRAY + (McMMOApi.getNivel(UUID + "Mineracao")) + "/1000");
                    lore7.add(" ");
                    lore7.add(ChatColor.WHITE + "Booster " + ChatColor.GREEN + "(VIP)" + ChatColor.WHITE + ": " + ChatColor.GRAY + boosterm);
                    lore7.add(ChatColor.WHITE + "Booster: " + ChatColor.GRAY + boosterv);
                    lore7.add(" ");
                    lore7.add(ChatColor.WHITE + "Colocação no rank: " + ChatColor.GRAY + (getNivelMcMMO(UUID, "Mineracao")) + "º");
                    lore7.add(ChatColor.WHITE + "Primeiro colocado: " + ChatColor.GRAY + AccountAPI.getNick(getTopMcMMO("Mineracao")));
                    metaMineracao.setLore(lore7);
                    mineracao.setItemMeta(metaMineracao);

                    final ItemStack herbalismo = new ItemStack(Material.WHEAT);
                    final ItemMeta metaHerbalismo = herbalismo.getItemMeta();
                    metaHerbalismo.setDisplayName(ChatColor.YELLOW + "Herbalismo " + ChatColor.LIGHT_PURPLE + "NOVO!");
                    final List<String> lore6 = new ArrayList<>();
                    lore6.add(ChatColor.WHITE + "Nível: " + ChatColor.GRAY + (McMMOApi.getNivel(UUID + "Herbalismo")) + "/1000");
                    lore6.add(" ");
                    lore6.add(ChatColor.WHITE + "Booster " + ChatColor.GREEN + "(VIP)" + ChatColor.WHITE + ": " + ChatColor.GRAY + boosterm);
                    lore6.add(ChatColor.WHITE + "Booster: " + ChatColor.GRAY + boosterv);
                    lore6.add(" ");
                    lore6.add(ChatColor.WHITE + "Colocação no rank: " + ChatColor.GRAY + (getNivelMcMMO(UUID, "Herbalismo")) + "º");
                    lore6.add(ChatColor.WHITE + "Primeiro colocado: " + ChatColor.GRAY + AccountAPI.getNick(getTopMcMMO("Herbalismo")));
                    metaHerbalismo.setLore(lore6);
                    herbalismo.setItemMeta(metaHerbalismo);

                    final ItemStack reparacao = new ItemStack(Material.ANVIL);
                    final ItemMeta metaReparacao = reparacao.getItemMeta();
                    metaReparacao.setDisplayName(ChatColor.YELLOW + "Reparacão");
                    final List<String> lore5 = new ArrayList<>();
                    lore5.add(ChatColor.WHITE + "Nível: " + ChatColor.GRAY + (McMMOApi.getNivel(UUID + "Reparacao")) + "/500");
                    lore5.add(" ");
                    lore5.add(ChatColor.WHITE + "Booster " + ChatColor.GREEN + "(VIP)" + ChatColor.WHITE + ": " + ChatColor.GRAY + boosterm);
                    lore5.add(ChatColor.WHITE + "Booster: " + ChatColor.GRAY + boosterv);
                    lore5.add(" ");
                    lore5.add(ChatColor.WHITE + "Colocação no rank: " + ChatColor.GRAY + (getNivelMcMMO(UUID, "Reparacao")) + "º");
                    lore5.add(ChatColor.WHITE + "Primeiro colocado: " + ChatColor.GRAY + AccountAPI.getNick(getTopMcMMO("Reparacao")));
                    metaReparacao.setLore(lore5);
                    reparacao.setItemMeta(metaReparacao);

                    final ItemStack acrobacia = new ItemStack(Material.DIAMOND_BOOTS);
                    final ItemMeta metaAcrobacia = acrobacia.getItemMeta();
                    metaAcrobacia.setDisplayName(ChatColor.YELLOW + "Acrobacias");
                    final List<String> lore4 = new ArrayList<>();
                    lore4.add(ChatColor.WHITE + "Nível: " + ChatColor.GRAY + (McMMOApi.getNivel(UUID + "Acrobacias")) + "/500");
                    lore4.add(" ");
                    lore4.add(ChatColor.WHITE + "Booster " + ChatColor.GREEN + "(VIP)" + ChatColor.WHITE + ": " + ChatColor.GRAY + boosterm);
                    lore4.add(ChatColor.WHITE + "Booster: " + ChatColor.GRAY + boosterv);
                    lore4.add(" ");
                    lore4.add(ChatColor.WHITE + "Colocação no rank: " + ChatColor.GRAY + (getNivelMcMMO(UUID, "Acrobacias")) + "º");
                    lore4.add(ChatColor.WHITE + "Primeiro colocado: " + ChatColor.GRAY + AccountAPI.getNick(getTopMcMMO("Acrobacias")));
                    metaAcrobacia.setLore(lore4);
                    acrobacia.setItemMeta(metaAcrobacia);

                    final ItemStack encantar = new ItemStack(Material.BOOK);
                    final ItemMeta metaEncantar = encantar.getItemMeta();
                    metaEncantar.setDisplayName(ChatColor.YELLOW + "Encantar " + ChatColor.LIGHT_PURPLE + "NOVO!");
                    final List<String> lore3 = new ArrayList<>();
                    lore3.add(ChatColor.WHITE + "Nível: " + ChatColor.GRAY + (McMMOApi.getNivel(UUID + "Encantar")) + "/800");
                    lore3.add(" ");
                    lore3.add(ChatColor.WHITE + "Booster " + ChatColor.GREEN + "(VIP)" + ChatColor.WHITE + ": " + ChatColor.GRAY + boosterm);
                    lore3.add(ChatColor.WHITE + "Booster: " + ChatColor.GRAY + boosterv);
                    lore3.add(" ");
                    lore3.add(ChatColor.WHITE + "Colocação no rank: " + ChatColor.GRAY + (getNivelMcMMO(UUID, "Encantar")) + "º");
                    lore3.add(ChatColor.WHITE + "Primeiro colocado: " + ChatColor.GRAY + AccountAPI.getNick(getTopMcMMO("Encantar")));
                    metaEncantar.setLore(lore3);
                    encantar.setItemMeta(metaEncantar);

                    final ItemStack arqueiro = new ItemStack(Material.BOW);
                    final ItemMeta metaArqueiro = arqueiro.getItemMeta();
                    metaArqueiro.setDisplayName(ChatColor.YELLOW + "Arqueiro");
                    final List<String> lore2 = new ArrayList<>();
                    lore2.add(ChatColor.WHITE + "Nível: " + ChatColor.GRAY + (McMMOApi.getNivel(UUID + "Arqueiro")) + "/600");
                    lore2.add(" ");
                    lore2.add(ChatColor.WHITE + "Booster " + ChatColor.GREEN + "(VIP)" + ChatColor.WHITE + ": " + ChatColor.GRAY + boosterm);
                    lore2.add(ChatColor.WHITE + "Booster: " + ChatColor.GRAY + boosterv);
                    lore2.add(" ");
                    lore2.add(ChatColor.WHITE + "Colocação no rank: " + ChatColor.GRAY + (getNivelMcMMO(UUID, "Arqueiro")) + "º");
                    lore2.add(ChatColor.WHITE + "Primeiro colocado: " + ChatColor.GRAY + AccountAPI.getNick(getTopMcMMO("Arqueiro")));
                    metaArqueiro.setLore(lore2);
                    arqueiro.setItemMeta(metaArqueiro);

                    final ItemStack alquimia = new ItemStack(Material.BREWING_STAND);
                    final ItemMeta metaAlquimia = alquimia.getItemMeta();
                    metaAlquimia.setDisplayName(ChatColor.YELLOW + "Alquimia " + ChatColor.RED + "DESATIVADO!");
                    final List<String> lore1 = new ArrayList<>();
                    lore1.add(ChatColor.WHITE + "Nível: " + ChatColor.GRAY + (McMMOApi.getNivel(UUID + "Alquimia")) + "/800");
                    lore1.add(" ");
                    lore1.add(ChatColor.WHITE + "Booster " + ChatColor.GREEN + "(VIP)" + ChatColor.WHITE + ": " + ChatColor.GRAY + boosterm);
                    lore1.add(ChatColor.WHITE + "Booster: " + ChatColor.GRAY + boosterv);
                    lore1.add(" ");
                    lore1.add(ChatColor.WHITE + "Colocação no rank: " + ChatColor.GRAY + (getNivelMcMMO(UUID, "Alquimia")) + "º");
                    lore1.add(ChatColor.WHITE + "Primeiro colocado: " + ChatColor.GRAY + AccountAPI.getNick(getTopMcMMO("Alquimia")));
                    metaAlquimia.setLore(lore1);
                    alquimia.setItemMeta(metaAlquimia);

                    final ItemStack espadas = new ItemStack(Material.DIAMOND_SWORD);
                    final ItemMeta metaEspadas = espadas.getItemMeta();
                    metaEspadas.setDisplayName(ChatColor.YELLOW + "Espadas");
                    final List<String> lore = new ArrayList<>();
                    lore.add(ChatColor.WHITE + "Nível: " + ChatColor.GRAY + (McMMOApi.getNivel(UUID + "Espadas")) + "/800");
                    lore.add(" ");
                    lore.add(ChatColor.WHITE + "Booster " + ChatColor.GREEN + "(VIP)" + ChatColor.WHITE + ": " + ChatColor.GRAY + boosterm);
                    lore.add(ChatColor.WHITE + "Booster: " + ChatColor.GRAY + boosterv);
                    lore.add(" ");
                    lore.add(ChatColor.WHITE + "Colocação no rank: " + ChatColor.GRAY + (getNivelMcMMO(UUID, "Espadas")) + "º");
                    lore.add(ChatColor.WHITE + "Primeiro colocado: " + ChatColor.GRAY + AccountAPI.getNick(getTopMcMMO("Espadas")));
                    metaEspadas.setLore(lore);
                    espadas.setItemMeta(metaEspadas);

                    inventory.setItem(11, espadas);
                    inventory.setItem(12, alquimia);
                    inventory.setItem(13, arqueiro);
                    inventory.setItem(14, encantar);
                    inventory.setItem(15, acrobacia);
                    inventory.setItem(20, reparacao);
                    inventory.setItem(21, herbalismo);
                    inventory.setItem(22, mineracao);
                    inventory.setItem(23, machado);
                    inventory.setItem(24, escavacao);

                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            player.openInventory(inventory);
                        }
                    }.runTask(Main.plugin);
                }
            }.runTaskAsynchronously(Main.plugin);

            return true;
        }

        return false;
    }

}
