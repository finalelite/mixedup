package com.mixedup.mcmmo.commands;

import com.mixedup.mcmmo.McMMOApi;
import com.mixedup.utils.CentralizeMsg;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandEspada implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        Player player = (Player) sender;
        String UUID = player.getUniqueId().toString();

        if (cmd.getName().equalsIgnoreCase("espadas")) {
            final String line1 = CentralizeMsg.sendCenteredMessage("&e&lEspadas");
            final String line2 = CentralizeMsg.sendCenteredMessage("&eGanha experiência: &7Atacando monstros");
            final String line3 = CentralizeMsg.sendCenteredMessage("&eNível: &7" + (McMMOApi.getNivel(UUID + "Espadas")) + " &bXP(" + (McMMOApi.getXP(UUID + "Espadas")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Espadas")) + ")");
            final String line4 = CentralizeMsg.sendCenteredMessage("&e&lEfeitos");
            final String line5 = CentralizeMsg.sendCenteredMessage("&eContra-ataque: &7Reflete 50% do dano recebido");
            final String line6 = CentralizeMsg.sendCenteredMessage("&eSangramanto: &7Jogador pode sangrar ao ser hitado");
            final String line7 = CentralizeMsg.sendCenteredMessage("&e&lSeus status");
            final String line8 = CentralizeMsg.sendCenteredMessage("&eChance do contra-ataque: &7" + (McMMOApi.getNivel(UUID) * 0.037) + "%");
            final String line9 = CentralizeMsg.sendCenteredMessage("&eChance do sangramento: &7" + (McMMOApi.getNivel(UUID) * 0.037) + "%");
            int tempo = 0;
            if (McMMOApi.getNivel(UUID + "Espadas") <= 200) {
                tempo = 2;
            } else if (McMMOApi.getNivel(UUID + "Espadas") >= 200 && McMMOApi.getNivel(UUID + "Espadas") <= 400) {
                tempo = 5;
            } else if (McMMOApi.getNivel(UUID + "Espadas") >= 400 && McMMOApi.getNivel(UUID + "Espadas") <= 600) {
                tempo = 8;
            } else if (McMMOApi.getNivel(UUID + "Espadas") >= 600 && McMMOApi.getNivel(UUID + "Espadas") <= 800) {
                tempo = 10;
            }
            final String line10 = CentralizeMsg.sendCenteredMessage("&eDuração do sangramento: &7" + (tempo) + " segundos");
            player.sendMessage("\n" + line1 + "\n" + line2 + "\n" + line3 + "\n \n" + line4 + "\n" + line5 + "\n" + line6 + "\n \n" + line7 + "\n" + line8 + "\n" + line9 + "\n" + line10 + "\n ");
            return true;
        }

        return false;
    }
}
