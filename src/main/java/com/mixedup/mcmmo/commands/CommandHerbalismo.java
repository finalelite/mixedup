package com.mixedup.mcmmo.commands;

import com.mixedup.mcmmo.McMMOApi;
import com.mixedup.utils.CentralizeMsg;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandHerbalismo implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        Player player = (Player) sender;
        String UUID = player.getUniqueId().toString();

        if (cmd.getName().equalsIgnoreCase("herbalismo")) {
            final String line1 = CentralizeMsg.sendCenteredMessage("&e&lHerbalismo");
            final String line2 = CentralizeMsg.sendCenteredMessage("&eGanha experiência: &7Cultivando");
            final String line3 = CentralizeMsg.sendCenteredMessage("&eNível: %7" + (McMMOApi.getNivel(UUID + "Herbalismo")) + " &bXP(" + (McMMOApi.getXP(UUID + "Herbalismo")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Herbalismo")) + ")");
            final String line4 = CentralizeMsg.sendCenteredMessage("&e&lEfeitos");
            final String line5 = CentralizeMsg.sendCenteredMessage("&eCultivo produtivo: &7Pode encontrar tesouros e trocar los");
            final String line6 = CentralizeMsg.sendCenteredMessage("&eReplantio instantâneo: &7Pode replantar instantâneamente o cultivo");
            final String line7 = CentralizeMsg.sendCenteredMessage("&ePlantio veloz: &7O plantio pode crescer mais rápido que o normal");
            final String line8 = CentralizeMsg.sendCenteredMessage("&eSangue de cultivador: &7Tem desconto na loja ao vender ou comprar");
            final String line9 = CentralizeMsg.sendCenteredMessage("&e&lSeus status");
            final String line10 = CentralizeMsg.sendCenteredMessage("&eCultivo produtivo: &7" + (McMMOApi.getNivel(UUID + "Herbalismo")) + "/1000");
            final String line11 = CentralizeMsg.sendCenteredMessage("&eChance de replantio instantâneo: &7" + (McMMOApi.getNivel(UUID + "Herbalismo") * 0.05) + "%");
            final String line12 = CentralizeMsg.sendCenteredMessage("&eChance de plantio veloz: &7" + (McMMOApi.getNivel(UUID + "Herbalismo") * 0.03) + "%");
            int d = 0;
            int lvl = McMMOApi.getNivel(UUID + "Herbalismo");
            while (lvl >= 100) {
                d++;
                lvl -= 100;
            }
            final String line13 = CentralizeMsg.sendCenteredMessage("&eSangue de cultivador: &7Desconto &aC" + (d) + " &cV" + (d * 2));
            player.sendMessage("\n" + line1 + "\n" + line2 + "\n" + line3 + "\n \n" + line4 + "\n" + line5 + "\n" + line6 + "\n" + line7 + "\n" + line8 + "\n \n" + line9 + "\n" + line10 + "\n" + line11 + "\n" + line12 + "\n" + line13 + "\n ");
            return true;
        }

        return false;
    }
}
