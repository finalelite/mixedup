package com.mixedup.mcmmo.commands;

import com.mixedup.mcmmo.McMMOApi;
import com.mixedup.utils.CentralizeMsg;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandReparacao implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        Player player = (Player) sender;
        String UUID = player.getUniqueId().toString();

        if (cmd.getName().equalsIgnoreCase("reparacao")) {
            final String line1 = CentralizeMsg.sendCenteredMessage("&e&lReparação");
            final String line2 = CentralizeMsg.sendCenteredMessage("&eGanha experiência: &7Reparando itens");
            final String line3 = CentralizeMsg.sendCenteredMessage("&eNível: &7" + (McMMOApi.getNivel(UUID + "Reparacao")) + " &bXP(" + (McMMOApi.getXP(UUID + "Reparacao")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Reparacao")) + ")");
            final String line4 = CentralizeMsg.sendCenteredMessage("&e&lEfeitos");
            final String line5 = CentralizeMsg.sendCenteredMessage("&eReparação: &7Repara armaduras e ferramentos");
            final String line6 = CentralizeMsg.sendCenteredMessage("&eReparo de ouro lvl(50+): &7Repara armaduras e ferramentas de ouro");
            final String line7 = CentralizeMsg.sendCenteredMessage("&eReparo de ferro lvl(150+): &7Repara armaduras e ferramentas de ferro");
            final String line8 = CentralizeMsg.sendCenteredMessage("&eReparo de diamante lvl(300+): &7Repara armaduras e ferramentas de diamante");
            final String line9 = CentralizeMsg.sendCenteredMessage("&eReparo instantâneo lvl(500): &7Repara armadura completamente com 1 de gasto");
            final String line10 = CentralizeMsg.sendCenteredMessage("&e&lSeus status");
            String line11 = null;
            final int lvl = McMMOApi.getNivel(UUID + "Reparacao");
            if (lvl < 50) {
                line11 = CentralizeMsg.sendCenteredMessage("&eRepara armaduras de: &7Couro, madeira e pedra");
            } else if (lvl >= 50 && lvl < 150) {
                line11 = CentralizeMsg.sendCenteredMessage("&eRepara armaduras de: &7Couro, madeira, pedra e ouro");
            } else if (lvl >= 150 && lvl < 300) {
                line11 = CentralizeMsg.sendCenteredMessage("&eRepara armaduras de: &7Couro, madeira, pedra, ouro e ferro");
            } else if (lvl >= 300) {
                line11 = CentralizeMsg.sendCenteredMessage("&eRepara armaduras de: &7Couro, madeira, pedra, ouro, ferro e diamante");
            }
            player.sendMessage("\n" + line1 + "\n" + line2 + "\n" + line3 + "\n \n" + line4 + "\n" + line5 + "\n" + line6 + "\n" + line7 + "\n" + line8 + "\n" + line9 + "\n \n" + line10 + "\n" + line11 + "\n ");
            return true;
        }

        return false;
    }
}
