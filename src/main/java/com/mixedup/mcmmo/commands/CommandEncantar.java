package com.mixedup.mcmmo.commands;

import com.mixedup.mcmmo.McMMOApi;
import com.mixedup.utils.CentralizeMsg;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandEncantar implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        Player player = (Player) sender;
        String UUID = player.getUniqueId().toString();

        if (cmd.getName().equalsIgnoreCase("encantar")) {
            final String line1 = CentralizeMsg.sendCenteredMessage("&e&lEncantar");
            final String line2 = CentralizeMsg.sendCenteredMessage("&eGanha experiência: &7Encantando itens");
            final String line3 = CentralizeMsg.sendCenteredMessage("&eNível: &7" + (McMMOApi.getNivel(UUID + "Encantar")) + " &bXP(" + (McMMOApi.getXP(UUID + "Encantar")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Encantar")) + ")");
            final String line4 = CentralizeMsg.sendCenteredMessage("&e&lEfeitos");
            final String line5 = CentralizeMsg.sendCenteredMessage("&eEncantador: &7Pode encantar com encantamentos mais avançados");
            final String line6 = CentralizeMsg.sendCenteredMessage("&e&lSeus status");
            int lvl = 0;
            final int nivel = McMMOApi.getNivel(UUID + "Encantar");
            if (nivel < 200) {
                lvl = 1;
            } else if (nivel < 400 && nivel >= 200) {
                lvl = 2;
            } else if (nivel >= 400) {
                lvl = 3;
            }
            final String line7 = CentralizeMsg.sendCenteredMessage("&eEncantador: &7Nível: " + (lvl) + "/3 de encantamentos");
            player.sendMessage("\n" + line1 + "\n" + line2 + "\n" + line3 + "\n \n" + line4 + "\n" + line5 + "\n \n" + line6 + "\n" + line7 + "\n ");
            return true;
        }

        return false;
    }
}
