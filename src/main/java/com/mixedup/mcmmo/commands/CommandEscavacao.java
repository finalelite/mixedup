package com.mixedup.mcmmo.commands;

import com.mixedup.mcmmo.McMMOApi;
import com.mixedup.utils.CentralizeMsg;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandEscavacao implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        Player player = (Player) sender;
        String UUID = player.getUniqueId().toString();

        if (cmd.getName().equalsIgnoreCase("escavacao")) {
            final String line1 = CentralizeMsg.sendCenteredMessage("&e&lEscavação");
            final String line2 = CentralizeMsg.sendCenteredMessage("&eGanha experiência: &7Escavando e encontrando itens raros");
            final String line3 = CentralizeMsg.sendCenteredMessage("&eNível: &7" + (McMMOApi.getNivel(UUID + "Escavacao")) + " &bXP(" + (McMMOApi.getXP(UUID + "Escavacao")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Escavacao")) + ")");
            final String line4 = CentralizeMsg.sendCenteredMessage("&e&lEfeitos");
            final String line5 = CentralizeMsg.sendCenteredMessage("&eCaçador de tesouros: &7Pode encontrar itens raros");
            final String line6 = CentralizeMsg.sendCenteredMessage("&e&lSeus status");
            int a = 0;
            int lvl = McMMOApi.getNivel(UUID + "Escavacao");
            while (lvl >= 50) {
                a++;
                lvl -= 50;
            }
            final String line7 = CentralizeMsg.sendCenteredMessage("&eCaçador de tesouros: " + (a) + "/20");
            player.sendMessage("\n" + line1 + "\n" + line2 + "\n" + line3 + "\n \n" + line4 + "\n" + line5 + "\n \n" + line6 + "\n" + line7 + "\n ");
            return true;
        }

        return false;
    }
}
