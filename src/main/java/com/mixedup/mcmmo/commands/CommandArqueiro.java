package com.mixedup.mcmmo.commands;

import com.mixedup.mcmmo.McMMOApi;
import com.mixedup.utils.CentralizeMsg;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandArqueiro implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        Player player = (Player) sender;
        String UUID = player.getUniqueId().toString();

        if (cmd.getName().equalsIgnoreCase("Arqueiro")) {
            final String line1 = CentralizeMsg.sendCenteredMessage("&e&lArqueiro");
            final String line2 = CentralizeMsg.sendCenteredMessage("&eGanha experiência: &7Atacando monstros");
            final String line3 = CentralizeMsg.sendCenteredMessage("&eNível: &7" + (McMMOApi.getNivel(UUID + "Arqueiro")) + " &bXP(" + (McMMOApi.getXP(UUID + "Arqueiro")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Arqueiro")) + ")");
            final String line4 = CentralizeMsg.sendCenteredMessage("&e&lEfeitos");
            final String line5 = CentralizeMsg.sendCenteredMessage("&eAtordoamento: &7Desorienta inimigos e causa 4 de dano");
            final String line6 = CentralizeMsg.sendCenteredMessage("&eRecuperação de flechas: &7Chance de recuperar flechas");
            final String line7 = CentralizeMsg.sendCenteredMessage("&eFlecha extra: &7Chance de atirar mais de uma flecha");
            final String line8 = CentralizeMsg.sendCenteredMessage("&eFlecha explosiva: &7Chance de causar uma explosão no alvo");
            final String line9 = CentralizeMsg.sendCenteredMessage("&e&lSeus status");
            final String line10 = CentralizeMsg.sendCenteredMessage("&eChance de atordoamento: " + (McMMOApi.getNivel(UUID + "Arqueiro") * 0.033));
            final String line11 = CentralizeMsg.sendCenteredMessage("&eChance de recuperar flechas: " + (McMMOApi.getNivel(UUID + "Arqueiro") * 0.066));
            final String line12 = CentralizeMsg.sendCenteredMessage("&eChance de flecha extra: " + (McMMOApi.getNivel(UUID + "Arqueiro") * 0.025));
            final String line13 = CentralizeMsg.sendCenteredMessage("&eChance de explosão: " + (McMMOApi.getNivel(UUID + "Arqueiro") * 0.008));
            player.sendMessage("\n" + line1 + "\n" + line2 + "\n" + line3 + "\n \n" + line4 + "\n" + line5 + "\n" + line6 + "\n" + line7 + "\n" + line8 + "\n \n" + line9 + "\n" + line10 + "\n" + line11 + "\n" + line12 + "\n" + line13 + "\n ");
        }

        return false;
    }
}
