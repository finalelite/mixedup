package com.mixedup.mcmmo.commands;

import com.mixedup.mcmmo.McMMOApi;
import com.mixedup.utils.CentralizeMsg;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandMachado implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        Player player = (Player) sender;
        String UUID = player.getUniqueId().toString();

        if (cmd.getName().equalsIgnoreCase("machado")) {
            final String line1 = CentralizeMsg.sendCenteredMessage("&e&lMachado");
            final String line2 = CentralizeMsg.sendCenteredMessage("&eGanha experiência: &7Atacando monstros");
            final String line3 = CentralizeMsg.sendCenteredMessage("&eNível: &7" + (McMMOApi.getNivel(UUID + "Machado")) + " &bXP(" + (McMMOApi.getXP(UUID + "Machado")) + "/" + (McMMOApi.getProximoNivelXP(UUID + "Machado")) + ")");
            final String line4 = CentralizeMsg.sendCenteredMessage("&e&lEfeitos");
            final String line5 = CentralizeMsg.sendCenteredMessage("&eAtaque crítico: Dano dobrado");
            final String line6 = CentralizeMsg.sendCenteredMessage("&eImpacto em armaduras: &7Grande força para quebrar armaduras");
            final String line7 = CentralizeMsg.sendCenteredMessage("&e&lSeus status");
            final String line8 = CentralizeMsg.sendCenteredMessage("&eChance do ataque crítico: &7" + (McMMOApi.getNivel(UUID + "Machado") * 0.031));
            final String line9 = CentralizeMsg.sendCenteredMessage("&eImpacto em armaduras: &7Causa 1 de dano a mais nas armaduras");
            player.sendMessage("\n" + line1 + "\n" + line2 + "\n" + line3 + "\n \n" + line4 + "\n" + line5 + "\n" + line6 + "\n \n" + line7 + "\n" + line8 + "\n" + line9 + "\n ");
            return true;
        }

        return false;
    }
}
