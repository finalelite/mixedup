package com.mixedup.mcmmo;

import com.mixedup.Main;
import com.mixedup.MySql;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;

public class McMMOlog {

    public static void createLog(String uuid, String nick) {
        if (getExist(uuid) == null) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    try {
                        PreparedStatement st = MySql.con.prepareStatement("INSERT INTO McMMO_log(uuid, nick, acrobacias, alquimia, arqueiro, encantar, escavacao, espadas, herbalismo, machado, mineracao, reparacao, date) " +
                                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                        st.setString(1, uuid);
                        st.setString(2, nick);
                        st.setInt(3, 0);
                        st.setInt(4, 0);
                        st.setInt(5, 0);
                        st.setInt(6, 0);
                        st.setInt(7, 0);
                        st.setInt(8, 0);
                        st.setInt(9, 0);
                        st.setInt(10, 0);
                        st.setInt(11, 0);
                        st.setInt(12, 0);
                        st.setDate(13, new Date(new java.util.Date().getTime()));
                        st.executeUpdate();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }.runTaskAsynchronously(Main.plugin);
        }
    }

    public static String getExist(String uuid) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM McMMO_log WHERE uuid = ?");
            st.setString(1, uuid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("uuid");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void addReparacao(String uuid) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement st = MySql.con.prepareStatement("UPDATE McMMO_log SET reparacao = reparacao + 1 WHERE uuid = ?");
                    st.setString(1, uuid);
                    st.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void addMineracao(String uuid) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement st = MySql.con.prepareStatement("UPDATE McMMO_log SET mineracao = mineracao + 1 WHERE uuid = ?");
                    st.setString(1, uuid);
                    st.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void addMachado(String uuid) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement st = MySql.con.prepareStatement("UPDATE McMMO_log SET machado = machado + 1 WHERE uuid = ?");
                    st.setString(1, uuid);
                    st.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void addHerbalismo(String uuid) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement st = MySql.con.prepareStatement("UPDATE McMMO_log SET herbalismo = herbalismo + 1 WHERE uuid = ?");
                    st.setString(1, uuid);
                    st.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void addEspadas(String uuid) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement st = MySql.con.prepareStatement("UPDATE McMMO_log SET espadas = espadas + 1 WHERE uuid = ?");
                    st.setString(1, uuid);
                    st.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void addEscavacao(String uuid) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement st = MySql.con.prepareStatement("UPDATE McMMO_log SET escavacao = escavacao + 1 WHERE uuid = ?");
                    st.setString(1, uuid);
                    st.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void addEncantar(String uuid) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement st = MySql.con.prepareStatement("UPDATE McMMO_log SET encantar = encantar + 1 WHERE uuid = ?");
                    st.setString(1, uuid);
                    st.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void addArqueiro(String uuid) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement st = MySql.con.prepareStatement("UPDATE McMMO_log SET arqueiro = arqueiro + 1 WHERE uuid = ?");
                    st.setString(1, uuid);
                    st.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void addAlquimia(String uuid) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE McMMO_log SET alquimia = alquimia + 1 WHERE uuid = ?");
            st.setString(1, uuid);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void addAcrobacias(String uuid) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement st = MySql.con.prepareStatement("UPDATE McMMO_log SET acrobacias = acrobacias + 1 WHERE uuid = ?");
                    st.setString(1, uuid);
                    st.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void updateLog(String uuid, String skill) {
        java.util.Date date = new java.util.Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        
        if (skill.equalsIgnoreCase("acrobacias")) {
            addAcrobacias(uuid + ":" + localDate.getDayOfMonth() + "/" + localDate.getMonthValue() + "/" + localDate.getYear());
        } else if (skill.equalsIgnoreCase("alquimia")) {
            addAlquimia(uuid + ":" + localDate.getDayOfMonth() + "/" + localDate.getMonthValue() + "/" + localDate.getYear());
        } else if (skill.equalsIgnoreCase("arqueiro")) {
            addArqueiro(uuid + ":" + localDate.getDayOfMonth() + "/" + localDate.getMonthValue() + "/" + localDate.getYear());
        } else if (skill.equalsIgnoreCase("encantar")) {
            addEncantar(uuid + ":" + localDate.getDayOfMonth() + "/" + Calendar.MONTH + "/" + Calendar.YEAR);
        } else if (skill.equalsIgnoreCase("escavacao")) {
            addEscavacao(uuid + ":" + localDate.getDayOfMonth() + "/" + localDate.getMonthValue() + "/" + localDate.getYear());
        } else if (skill.equalsIgnoreCase("espadas")) {
            addEspadas(uuid + ":" + localDate.getDayOfMonth() + "/" + localDate.getMonthValue() + "/" + localDate.getYear());
        } else if (skill.equalsIgnoreCase("herbalismo")) {
            addHerbalismo(uuid + ":" + localDate.getDayOfMonth() + "/" + localDate.getMonthValue() + "/" + localDate.getYear());
        } else if (skill.equalsIgnoreCase("machado")) {
            addMachado(uuid + ":" + localDate.getDayOfMonth() + "/" + localDate.getMonthValue() + "/" + localDate.getYear());
        } else if (skill.equalsIgnoreCase("mineracao")) {
            addMineracao(uuid + ":" + localDate.getDayOfMonth() + "/" + localDate.getMonthValue() + "/" + localDate.getYear());
        } else if (skill.equalsIgnoreCase("reparacao")) {
            addReparacao(uuid + ":" + localDate.getDayOfMonth() + "/" + localDate.getMonthValue() + "/" + localDate.getYear());
        }
    }
}
