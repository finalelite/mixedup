package com.mixedup.mcmmo.utils;

import java.util.HashMap;

public class HasLifeBonus {

    private static final HashMap<String, HasLifeBonus> CACHE = new HashMap<String, HasLifeBonus>();
    private final String UUID;
    private boolean HasLifeBonus;
    private long time;

    public HasLifeBonus(final String UUID, final boolean HasLifeBonus, final long time) {
        this.UUID = UUID;
        this.HasLifeBonus = HasLifeBonus;
        this.time = time;
    }

    public static HasLifeBonus get(final String UUID) {
        return com.mixedup.mcmmo.utils.HasLifeBonus.CACHE.get(String.valueOf(UUID));
    }

    public HasLifeBonus insert() {
        com.mixedup.mcmmo.utils.HasLifeBonus.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getHasLifeBonus() {
        return HasLifeBonus;
    }

    public void setHasLifeBonus(final boolean HasLifeBonus) {
        this.HasLifeBonus = HasLifeBonus;
    }

    public long getTime() {
        return time;
    }

    public void setTime(final long time) {
        this.time = time;
    }

    public String getUUID() {
        return UUID;
    }
}
