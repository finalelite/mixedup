package com.mixedup.mcmmo.utils;

import java.util.HashMap;

public class TookDamaged {

    private static final HashMap<String, TookDamaged> CACHE = new HashMap<String, TookDamaged>();
    private final String UUID;
    private boolean tooked;

    public TookDamaged(final String UUID, final boolean tooked) {
        this.UUID = UUID;
        this.tooked = tooked;
    }

    public static TookDamaged get(final String UUID) {
        return TookDamaged.CACHE.get(String.valueOf(UUID));
    }

    public TookDamaged insert() {
        TookDamaged.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean gettooked() {
        return tooked;
    }

    public void settooked(final boolean tooked) {
        this.tooked = tooked;
    }

    public String getUUID() {
        return UUID;
    }
}
