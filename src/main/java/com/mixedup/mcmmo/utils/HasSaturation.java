package com.mixedup.mcmmo.utils;

import java.util.HashMap;

public class HasSaturation {

    private static final HashMap<String, HasSaturation> CACHE = new HashMap<String, HasSaturation>();
    private final String UUID;
    private boolean HasSaturation;
    private long time;

    public HasSaturation(final String UUID, final boolean HasSaturation, final long time) {
        this.UUID = UUID;
        this.HasSaturation = HasSaturation;
        this.time = time;
    }

    public static HasSaturation get(final String UUID) {
        return com.mixedup.mcmmo.utils.HasSaturation.CACHE.get(String.valueOf(UUID));
    }

    public HasSaturation insert() {
        com.mixedup.mcmmo.utils.HasSaturation.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getHasSaturation() {
        return HasSaturation;
    }

    public void setHasSaturation(final boolean HasSaturation) {
        this.HasSaturation = HasSaturation;
    }

    public long getTime() {
        return time;
    }

    public void setTime(final long time) {
        this.time = time;
    }

    public String getUUID() {
        return UUID;
    }
}
