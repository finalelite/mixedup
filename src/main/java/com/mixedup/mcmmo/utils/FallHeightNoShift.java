package com.mixedup.mcmmo.utils;

import java.util.HashMap;

public class FallHeightNoShift {

    private static final HashMap<String, FallHeightNoShift> CACHE = new HashMap<String, FallHeightNoShift>();
    private final String UUID;
    private int lastY;

    public FallHeightNoShift(final String UUID, final int lastY) {
        this.UUID = UUID;
        this.lastY = lastY;
    }

    public static FallHeightNoShift get(final String UUID) {
        return FallHeightNoShift.CACHE.get(String.valueOf(UUID));
    }

    public FallHeightNoShift insert() {
        FallHeightNoShift.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public int getLastY() {
        return lastY;
    }

    public void setLastY(final int lastY) {
        this.lastY = lastY;
    }

    public String getUUID() {
        return UUID;
    }
}
