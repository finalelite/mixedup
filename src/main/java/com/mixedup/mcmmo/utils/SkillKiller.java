package com.mixedup.mcmmo.utils;

import java.util.HashMap;

public class SkillKiller {

    public static HashMap<String, SkillKiller> CACHE = new HashMap<>();
    private final String UUID;
    private boolean able;

    public SkillKiller(final String UUID, final boolean able) {
        this.UUID = UUID;
        this.able = able;
    }

    public static SkillKiller get(final String UUID) {
        return SkillKiller.CACHE.get(UUID);
    }

    public SkillKiller insert() {
        SkillKiller.CACHE.put(UUID, this);
        return this;
    }

    public boolean getAble() {
        return able;
    }

    public void setAble(final boolean able) {
        this.able = able;
    }
}
