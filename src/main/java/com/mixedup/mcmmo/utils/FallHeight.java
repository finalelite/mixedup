package com.mixedup.mcmmo.utils;

import java.util.HashMap;

public class FallHeight {

    private static final HashMap<String, FallHeight> CACHE = new HashMap<String, FallHeight>();
    private final String UUID;
    private int lastY;

    public FallHeight(final String UUID, final int lastY) {
        this.UUID = UUID;
        this.lastY = lastY;
    }

    public static FallHeight get(final String UUID) {
        return FallHeight.CACHE.get(String.valueOf(UUID));
    }

    public FallHeight insert() {
        FallHeight.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public int getLastY() {
        return lastY;
    }

    public void setLastY(final int lastY) {
        this.lastY = lastY;
    }

    public String getUUID() {
        return UUID;
    }
}
