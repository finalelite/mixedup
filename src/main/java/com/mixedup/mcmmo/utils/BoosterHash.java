package com.mixedup.mcmmo.utils;

import java.util.HashMap;

public class BoosterHash {

    public static final HashMap<String, BoosterHash> CACHE = new HashMap<String, BoosterHash>();
    private final String UUID;
    private String type;
    private long time;

    public BoosterHash(final String UUID, final String type, final long time) {
        this.UUID = UUID;
        this.type = type;
        this.time = time;
    }

    public static BoosterHash get(final String UUID) {
        return BoosterHash.CACHE.get(String.valueOf(UUID));
    }

    public BoosterHash insert() {
        BoosterHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public long gettime() {
        return time;
    }

    public void settime(final long time) {
        this.time = time;
    }

    public String getUUID() {
        return UUID;
    }
}
