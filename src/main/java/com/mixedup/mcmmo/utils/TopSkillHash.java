package com.mixedup.mcmmo.utils;

import java.util.Date;
import java.util.HashMap;

public class TopSkillHash {

    public static HashMap<String, TopSkillHash> cache = new HashMap<>();
    private final String skill;
    private String nick;
    private Date lastUpdate;

    public TopSkillHash(final String skill, final String nick, final Date lastUpdate) {
        this.skill = skill;
        this.nick = nick;
        this.lastUpdate = lastUpdate;
    }

    public static TopSkillHash get(final String skill) {
        return TopSkillHash.cache.get(skill);
    }

    public TopSkillHash insert() {
        TopSkillHash.cache.put(skill, this);
        return this;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(final String nick) {
        this.nick = nick;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(final Date newDate) {
        lastUpdate = newDate;
    }
}
