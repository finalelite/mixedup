package com.mixedup.mcmmo.utils;

import java.util.HashMap;

public class LastDamager {

    private static final HashMap<String, LastDamager> CACHE = new HashMap<String, LastDamager>();
    private final String UUdamager;
    private String damager;

    public LastDamager(final String UUdamager, final String damager) {
        this.UUdamager = UUdamager;
        this.damager = damager;
    }

    public static LastDamager get(final String UUdamager) {
        return LastDamager.CACHE.get(String.valueOf(UUdamager));
    }

    public LastDamager insert() {
        LastDamager.CACHE.put(String.valueOf(UUdamager), this);

        return this;
    }

    public String getdamager() {
        return damager;
    }

    public void setDamager(final String damager) {
        this.damager = damager;
    }

    public String getUUID() {
        return UUdamager;
    }
}
