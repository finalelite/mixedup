package com.mixedup.mcmmo.utils;

import java.util.HashMap;

public class IsInWater {

    private static final HashMap<String, IsInWater> CACHE = new HashMap<String, IsInWater>();
    private final String UUID;
    private boolean IsInWater;
    private long time;

    public IsInWater(final String UUID, final boolean IsInWater, final long time) {
        this.UUID = UUID;
        this.IsInWater = IsInWater;
        this.time = time;
    }

    public static IsInWater get(final String UUID) {
        return com.mixedup.mcmmo.utils.IsInWater.CACHE.get(String.valueOf(UUID));
    }

    public IsInWater insert() {
        com.mixedup.mcmmo.utils.IsInWater.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getIsInWater() {
        return IsInWater;
    }

    public void setIsInWater(final boolean IsInWater) {
        this.IsInWater = IsInWater;
    }

    public long getTime() {
        return time;
    }

    public void setTime(final long time) {
        this.time = time;
    }

    public String getUUID() {
        return UUID;
    }
}
