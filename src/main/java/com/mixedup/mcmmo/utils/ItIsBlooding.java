package com.mixedup.mcmmo.utils;

import java.util.HashMap;

public class ItIsBlooding {

    private static final HashMap<String, ItIsBlooding> CACHE = new HashMap<String, ItIsBlooding>();
    private final String UUID;
    private boolean status;

    public ItIsBlooding(final String UUID, final boolean status) {
        this.UUID = UUID;
        this.status = status;
    }

    public static ItIsBlooding get(final String UUID) {
        return ItIsBlooding.CACHE.get(String.valueOf(UUID));
    }

    public ItIsBlooding insert() {
        ItIsBlooding.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(final boolean status) {
        this.status = status;
    }

    public String getUUID() {
        return UUID;
    }
}
