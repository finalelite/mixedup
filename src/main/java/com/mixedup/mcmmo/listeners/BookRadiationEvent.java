package com.mixedup.mcmmo.listeners;

import com.mixedup.mcmmo.ProbabilityApi;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Random;

public class BookRadiationEvent implements Listener {

    @EventHandler
    public void onInteract(final InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();
        final String UUID = player.getUniqueId().toString();

        final ItemStack antiRadiacao = new ItemStack(Material.ENCHANTED_BOOK);
        final ItemMeta meta4 = antiRadiacao.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Proteção radiação");
        antiRadiacao.setItemMeta(meta4);

        if (event.getCursor() != null && !event.getCursor().getType().equals(Material.AIR) && event.getCursor().getItemMeta().getDisplayName().equalsIgnoreCase(antiRadiacao.getItemMeta().getDisplayName())) {
            final ItemStack item = event.getClickedInventory().getItem(event.getSlot());

            if (item == null || item.getType().equals(Material.AIR)) return;

            event.setCancelled(true);

            if (!event.getCursor().hasItemMeta() || !event.getCursor().hasItemMeta() || !event.getCursor().getItemMeta().hasLore() || event.getCursor().getItemMeta().getLore().get(1).substring(9, event.getCursor().getItemMeta().getLore().get(1).length() - 1) == null)
                return;
            final int probability = Integer.valueOf(event.getCursor().getItemMeta().getLore().get(1).substring(9, event.getCursor().getItemMeta().getLore().get(1).length() - 1));
            if (ProbabilityApi.probab(probability * 1000) == true) {
                event.getCursor().setType(Material.AIR);
                event.getCursor().setAmount(0);

                if (!item.getType().equals(Material.DIAMOND_CHESTPLATE) && !item.getType().equals(Material.DIAMOND_HELMET) && !item.getType().equals(Material.DIAMOND_LEGGINGS) && !item.getType().equals(Material.DIAMOND_BOOTS) &&
                        !item.getType().equals(Material.GOLDEN_CHESTPLATE) && !item.getType().equals(Material.GOLDEN_HELMET) && !item.getType().equals(Material.GOLDEN_LEGGINGS) && !item.getType().equals(Material.GOLDEN_BOOTS) &&
                        !item.getType().equals(Material.IRON_CHESTPLATE) && !item.getType().equals(Material.IRON_HELMET) && !item.getType().equals(Material.IRON_LEGGINGS) && !item.getType().equals(Material.IRON_BOOTS) &&
                        !item.getType().equals(Material.CHAINMAIL_CHESTPLATE) && !item.getType().equals(Material.CHAINMAIL_HELMET) && !item.getType().equals(Material.CHAINMAIL_LEGGINGS) && !item.getType().equals(Material.CHAINMAIL_BOOTS) &&
                        !item.getType().equals(Material.LEATHER_CHESTPLATE) && !item.getType().equals(Material.LEATHER_HELMET) && !item.getType().equals(Material.LEATHER_LEGGINGS) && !item.getType().equals(Material.LEATHER_BOOTS))
                    return;

                final ItemMeta meta = item.getItemMeta();
                final ArrayList<String> lore = new ArrayList<>();
                lore.add(" ");
                lore.add(ChatColor.RED + "Invulnerável a radiação.");
                lore.add(" ");
                meta.setLore(lore);
                item.setItemMeta(meta);

                player.sendMessage(ChatColor.GREEN + " * Encantamento bem sucedido.");
            } else {
                event.getCursor().setType(Material.AIR);
                event.getCursor().setAmount(0);
                player.playSound(player.getLocation(), Sound.ENTITY_ITEM_BREAK, 1.0f, 1.0f);

                final Random random = new Random();
                final int rd = random.nextInt(6);
                if (rd == 1) {
                    player.sendMessage(ChatColor.RED + " * As vezes nem mesmo os melhores magos estão em seus melhores dias.");
                } else if (rd == 2) {
                    player.sendMessage(ChatColor.RED + " * Infelizmente você não está em um dia de sorte.");
                } else if (rd == 3) {
                    player.sendMessage(ChatColor.RED + " * O livro se corroeu de tanta magia emanada pelo mesmo.");
                } else if (rd == 4) {
                    player.sendMessage(ChatColor.RED + " * As vezes é melhor aceitar e seguir em frente.");
                } else if (rd == 5) {
                    player.sendMessage(ChatColor.RED + " * Hmm...");
                } else if (rd == 6) {
                    player.sendMessage(ChatColor.RED + " * Olhos de rã, penas de maribundo. BUUMMMMMM!!!!  ...  (?)");
                }
            }
        }
    }
}
