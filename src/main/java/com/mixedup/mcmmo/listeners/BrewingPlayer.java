package com.mixedup.mcmmo.listeners;

import org.bukkit.entity.Player;

import java.util.HashMap;

public class BrewingPlayer {

    private static final HashMap<String, BrewingPlayer> CACHE = new HashMap<String, BrewingPlayer>();
    private final String Location;
    private String UUID;
    private Player player;

    public BrewingPlayer(final String Location, final String UUID, final Player player) {
        this.Location = Location;
        this.UUID = UUID;
        this.player = player;
    }

    public static BrewingPlayer get(final String Location) {
        return BrewingPlayer.CACHE.get(String.valueOf(Location));
    }

    public BrewingPlayer insert() {
        BrewingPlayer.CACHE.put(String.valueOf(Location), this);

        return this;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(final String UUID) {
        this.UUID = UUID;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(final Player player) {
        this.player = player;
    }

    public String getLocation() {
        return Location;
    }
}
