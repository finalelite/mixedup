package com.mixedup.mcmmo.listeners;

import com.mixedup.mcmmo.utils.BoosterHash;
import com.mixedup.utils.RemoveItemInv;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Date;

public class BoosterEvent implements Listener {

    @EventHandler
    public void onInteract(final PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        final String UUID = player.getUniqueId().toString();

        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) || event.getAction().equals(Action.RIGHT_CLICK_AIR)) {
            if (event.getPlayer().getItemInHand().getType().equals(Material.BLAZE_POWDER)) {
                if (event.getPlayer().getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GREEN + "Booster mcmmo")) {
                    try {
                        final String type = event.getPlayer().getItemInHand().getLore().get(1).substring(10);
                        final int time = Integer.valueOf(event.getPlayer().getItemInHand().getLore().get(2).substring(11, event.getPlayer().getItemInHand().getLore().get(2).length() - 8));

                        if (BoosterHash.get(UUID) == null) {
                            final Date date = new Date();
                            date.setMinutes((date.getMinutes() + time));
                            new BoosterHash(UUID, type, date.getTime() / 1000).insert();
                            player.sendMessage(ChatColor.GREEN + " * Booster ativado!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.BLOCK_CONDUIT_ACTIVATE, 1.0f, 1.0f);
                            RemoveItemInv.removeItemDifferentLore(player.getInventory(), player.getItemInHand(), 1);
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, você já contém um booster ativo!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } catch (final NumberFormatException e) {
                        //-
                    }
                }
            }
        }
    }
}
