package com.mixedup.mcmmo.listeners;

import com.mixedup.mcmmo.McMMOApi;
import com.mixedup.mcmmo.McMMOCache;

public class CreateInfosSQL {

    public static void create(final String UUID) {
        McMMOCache.getSkillNameList().forEach(name -> McMMOApi.createInfo(UUID + name));
    }
}
