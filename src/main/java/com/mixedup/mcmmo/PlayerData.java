package com.mixedup.mcmmo;

import java.util.Map;

public class PlayerData {

    private final Map<String, SkillData> skills;

    public PlayerData(final Map<String, SkillData> skills) {
        this.skills = skills;
    }

    public SkillData getSkillData(final String skillName) {
        return this.skills.getOrDefault(skillName, null);
    }
}
