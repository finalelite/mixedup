package com.mixedup.mcmmo;

public class SkillData {

    private int level;
    private int xp;

    public SkillData(final int level, final int xp) {
        this.level = level;
        this.xp = xp;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(final int level) {
        this.level = level;
    }

    public int getXp() {
        return this.xp;
    }

    public void setXp(final int xp) {
        this.xp = xp;
    }
}
