package com.mixedup.mcmmo;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.mixedup.Main;
import com.mixedup.MySql;
import lombok.Getter;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class McMMOCache {

    private static final List<String> skills = Arrays.asList("Alquimia", "Acrobacias", "Arqueiro", "Encantar", "Escavacao", "Espadas", "Herbalismo", "Machado", "Mineracao", "Reparacao");
    private LoadingCache<String, PlayerData> cache;

    public static List<String> saves = new ArrayList<>();

    public McMMOCache() {
        clearCache();
        startThread();
    }

    public static List<String> getSkillNameList() {
        return skills;
    }

    public boolean isInDatabase(final String uuid, final String skill) {
        try (final PreparedStatement stmt = MySql.con.prepareStatement("SELECT UUID FROM McMMO_data WHERE UUID = ?")) {
            stmt.setString(1, uuid + skill);
            final ResultSet rs = stmt.executeQuery();
            return rs.next();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isInCache(String uuid) {

        return cache.asMap().containsKey(uuid);

    }

    public void clearCache() {
        cache = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .build(new CacheLoader<String, PlayerData>() {
                    public PlayerData load(final String uuid) {
                        return getFromSQL(uuid);
                    }
                });
    }

    public void removePlayerFromCache(final String uuid) {
        cache.invalidate(uuid);
    }

    public PlayerData getFromSQL(final String uuid) {
        return new PlayerData(getAllSkillsFromSQL(uuid));
    }

    public void startThread() {

        Thread th = new Thread(() -> {

            while(true) {

                try {

                    Thread.sleep(5000);

                } catch (Exception e) {

                    e.printStackTrace();

                }

                if(saves.isEmpty()) continue;

                saves.forEach(ss -> {

                    getSkillNameList().forEach(sm -> {

                        SkillData data = getFromCache(ss).getSkillData(sm);

                        try {

                            PreparedStatement stm = MySql.con.prepareStatement("UPDATE McMMO_data SET XPAtual = ? WHERE UUID = ?");
                            stm.setString(2, ss + sm);
                            stm.setInt(1, data.getXp());
                            stm.executeUpdate();

                        } catch (SQLException e) {

                            e.printStackTrace();

                        }

                    });

                    cache.invalidate(ss);
                    saves.remove(ss);

                });

            }

        });

        th.start();

    }

    public void setXPInSQL(String uuid, boolean async) {

        if(isInCache(uuid)) {

            PlayerData data = getFromCache(uuid);

            if(async) {

                if(!saves.contains(uuid)) saves.add(uuid);

            } else {

                getSkillNameList().forEach(sm -> {

                    SkillData skillData = data.getSkillData(sm);

                    try {

                        PreparedStatement stm = MySql.con.prepareStatement("UPDATE McMMO_data SET XPAtual = ? WHERE UUID = ?");
                        stm.setString(2, uuid + sm);
                        stm.setInt(1, skillData.getXp());
                        stm.executeUpdate();

                    } catch (SQLException e) {

                        e.printStackTrace();

                    }

                });

            }

        }

    }

    public HashMap<String, SkillData> getAllSkillsFromSQL(final String uuid) {
        try (final PreparedStatement stmt = MySql.con.prepareStatement("SELECT XPAtual, Nivel, UUID FROM McMMO_data WHERE UUID LIKE ?")) {
            stmt.setString(1, uuid + "%");
            final ResultSet rs = stmt.executeQuery();
            final HashMap<String, SkillData> data = new HashMap<>();
            while (rs.next()) {
                val skillName = rs.getString("UUID").substring(36);
                val skill = new SkillData(rs.getInt("Nivel"), rs.getInt("XPAtual"));
                data.put(skillName, skill);
            }
            return data;
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setXP(final String uuid, final String skill, final int xp) {

        SkillData data = getFromCache(uuid).getSkillData(skill);

        data.setXp(xp);

        Player player;
        if ((player = Bukkit.getPlayer(UUID.fromString(uuid))) != null && Main.scoreboardManager.hasSquadScoreboard(player))
            Main.scoreboardManager.getSquadScoreboard().updateEntry(player, "mcmmo");

    }

    public int setToNextLevel(final String uuid, final String skill) {

        SkillData data = getFromCache(uuid).getSkillData(skill);

        try {

            PreparedStatement stm = MySql.con.prepareStatement("UPDATE McMMO_data SET Nivel = Nivel + 1 WHERE UUID = ?");

            stm.setString(1, uuid + skill);
            stm.executeUpdate();

            data.setLevel(data.getLevel() + 1);
            data.setXp(0);

            return (data.getLevel());

        } catch (SQLException e) {

            e.printStackTrace();

            return 0;

        }

    }

    public PlayerData getFromCache(final String uuid) {
        try {
            return cache.get(uuid);
        } catch (final Exception e) {
            return null;
        }
    }

}
