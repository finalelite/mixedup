package com.mixedup.mcmmo.power;

import com.mixedup.mcmmo.utils.SkillKiller;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class EspadaPower implements Listener {

    @EventHandler
    public void onInteract(final PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        final String UUID = player.getUniqueId().toString();

        if (event.getAction().equals(Action.RIGHT_CLICK_AIR) && event.getPlayer().getItemInHand().getType().name().contains("SWORD")) {
            if (player.isSneaking()) {
                if (SkillKiller.get(UUID) == null) {
                    new SkillKiller(UUID, true).insert();
                    player.sendActionBar(ChatColor.GREEN + "Habilidade matador ativa!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else if (SkillKiller.get(UUID).getAble() == false) {
                    SkillKiller.get(UUID).setAble(true);
                    player.sendActionBar(ChatColor.GREEN + "Habilidade matador ativa!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    SkillKiller.get(UUID).setAble(false);
                    player.sendActionBar(ChatColor.RED + "Habilidade matador desativada!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
    }
}
