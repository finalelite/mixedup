package com.mixedup.mcmmo;

import com.mixedup.MySql;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class McMMOApi {

    public static McMMOCache cache = new McMMOCache();

    public static McMMOCache getCache() {
        return McMMOApi.cache;
    }

    public static void createInfo(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO McMMO_data(UUID, Nivel, XPAtual) VALUES (?, ?, ?)");
            st.setString(1, UUID);
            st.setInt(2, 0);
            st.setInt(3, 0);
            st.executeUpdate();
        } catch (final SQLException e) {
            if (e.getMessage().startsWith("Duplicate entry"))
                return;
            e.printStackTrace();
        }
    }

    public static int getTotalLevel(final String uuid) {
        final PlayerData data = cache.getFromCache(uuid);
        int total = 0;
        for (final String name : McMMOCache.getSkillNameList()) {
            total += data.getSkillData(name).getLevel();
        }
        return total;
    }

    public static int updateLevel(final String completeUUID) {
        final String uuid = completeUUID.substring(0, 36);
        final String skill = completeUUID.substring(36);
        McMMOlog.updateLog(uuid, skill);
        updateXP(completeUUID, 0);

        return McMMOApi.cache.setToNextLevel(uuid, skill);
    }

    public static void updateXP(final String completeUUID, final int xp) {
        final String uuid = completeUUID.substring(0, 36);
        final String skill = completeUUID.substring(36);

        //String tag = TagAPI.getTag(uuid);
        //        int bonus = 0;
        //        if (tag.equalsIgnoreCase("Vip")) {
        //            bonus = (xp / 100) * 2;
        //        } else if (tag.equalsIgnoreCase("Vip+")) {
        //            bonus = (xp / 100) * 4;
        //        } else if (tag.equalsIgnoreCase("Vip++")) {
        //            bonus = (xp / 100) * 6;
        //        } else if (tag.equalsIgnoreCase("Vip+++")) {
        //            bonus = (xp / 100) * 8;
        //        }
        cache.setXP(uuid, skill, xp);
    }

    public static int getXP(final String completeUUID) {
        final String uuid = completeUUID.substring(0, 36);
        final String skill = completeUUID.substring(36);
        return cache.getFromCache(uuid).getSkillData(skill).getXp();
    }

    public static int getProximoNivelXP(final String completeUUID) {
        return (getNivel(completeUUID) + 1) * 1200;
    }

    public static int getNivel(final String completeUUID) {
        if (completeUUID.length() < 36) {
            return 0;
        }
        final String uuid = completeUUID.substring(0, 36);
        final String skill = completeUUID.substring(36);
        try {
            return cache.getFromCache(uuid).getSkillData(skill).getLevel();
        } catch (final Exception e) {
            return 0;
        }
    }

    public static boolean exists(final String completeUUID) {
        final String uuid = completeUUID.substring(0, 36);
        final String skill = completeUUID.substring(36);
        return cache.isInDatabase(uuid, skill);
    }
}