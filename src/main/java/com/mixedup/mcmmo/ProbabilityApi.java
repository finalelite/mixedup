package com.mixedup.mcmmo;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ProbabilityApi {

    public static boolean probab(final int probability) {
        final Random random = new Random();

        final boolean has;
        final int inicio = random.nextInt(100000);
        final List<Integer> numeros = new ArrayList<>();

        int prov = inicio;
        int equa = inicio + probability;
        for (int i = 1; prov <= equa; i++) {
            if (prov <= 100000) {
                numeros.add(prov);
                prov++;
            } else {
                numeros.add(1);
                prov = 1;
                equa = equa - 100000;
            }
        }

        final int sortido = random.nextInt(100000);

        has = numeros.contains(sortido);
        numeros.clear();

        return has;
    }
}
