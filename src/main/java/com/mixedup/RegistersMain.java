package com.mixedup;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.mixedup.apis.LojaPosAPI;
import com.mixedup.apis.PainelSolarAPI;
import com.mixedup.apis.PoderAPI;
import com.mixedup.chat.MessagesAutomatics;
import com.mixedup.economy.LojaAPI;
import com.mixedup.elytra.DungeonAPI;
import com.mixedup.elytra.listeners.FixTaskLag;
import com.mixedup.elytra.regions.DungeonsUtils;
import com.mixedup.elytra.regions.PortalsUtils;
import com.mixedup.elytra.utils.RemoveArmorStands;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.FortificationAPI;
import com.mixedup.factions.api.ClainsAPI;
import com.mixedup.factions.hashs.NexusUtil;
import com.mixedup.hashs.DailyHash;
import com.mixedup.listeners.AntiAfkEvent;
import com.mixedup.listeners.BeaconTaskFix;
import com.mixedup.listeners.JetpackLoaderEvent;
import com.mixedup.listeners.PlayerCraftItens;
import com.mixedup.managers.InventoryIr;
import com.mixedup.mcmmo.McMMOApi;
import com.mixedup.plots.ilha.IlhaInfosCache;
import com.mixedup.plots.ilha.IlhasRegion;
import com.mixedup.plots.terrain.RegionsTr;
import com.mixedup.plots.terrain.TerrainCache;
import com.mixedup.plots.terrain.TerrainInfosCache;
import com.mixedup.tutorial.TutorialManager;
import com.mixedup.utils.Chat;
import com.mixedup.worldProtection.hashs.ProtectionUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

public class RegistersMain {

    public static void enable() {
        InventoryIr.build();
        RegisterCommands.enable();
        RegisterListeners.enable();
        MessagesAutomatics.enable();
        RegionsTr.enableRegions();
        IlhasRegion.enableRegions();
        PainelSolarAPI.updateEnergia();
        TerrainCache.setCache();
        TerrainInfosCache.setCache();
        IlhaInfosCache.setCache();
        NexusUtil.enableRegions();
        FacAPI.updateSobAttack();
        //FacAPI.updatePlutonio();
        //FacAPI.updateLifeNexus();
        //FacAPI.updateInfos();
        PortalsUtils.enableRegions();
        DungeonsUtils.enableRegions();
        //WorldAPI.updateTime();
        ProtectionUtil.enableRegions();
        PoderAPI.updatePoder();
        DungeonAPI.loadDungeons();
        new TutorialManager();
        AntiAfkEvent.antiAfk();
        FixTaskLag.taskDungeons();
        new PlayerCraftItens().PlayerCraftItens();
        BeaconTaskFix.task();
        ClainsAPI.setChunkInCache();
        JetpackLoaderEvent.jetpackUpdater();
        DailyHash.setCache();
        LojaPosAPI.setCache();
        LojaAPI.setCache();
        FortificationAPI.setCacheTnts();

        Chat.chatStatus = false;
    }

    public static void disable() {
        RemoveArmorStands.removeAllArmorStand();
        Bukkit.getOnlinePlayers().forEach(ss -> McMMOApi.getCache().setXPInSQL(ss.getUniqueId().toString(), false));
        Bukkit.getScheduler().cancelTasks(Main.getInstance());
        BeaconTaskFix.stopThread();
    }
}

