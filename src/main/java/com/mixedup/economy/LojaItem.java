package com.mixedup.economy;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class LojaItem {
    private int id;
    private String item;
    private int compra;
    private int venda;
    private int quantia;
}
