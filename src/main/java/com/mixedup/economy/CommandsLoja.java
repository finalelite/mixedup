package com.mixedup.economy;

import com.mixedup.apis.AccountAPI;
import com.mixedup.apis.ConfigAPI;
import com.mixedup.apis.LojaPosAPI;
import com.mixedup.apis.PlayerUUID;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class CommandsLoja implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, String[] args) {
        final Player player = (Player) sender;
        final String UUID = player.getUniqueId().toString();

        //-/mercado <nick>, /mercado criar, /mercado redefinir, /mercado deletar, /mercado anunciar (msg)
        if (cmd.getName().equalsIgnoreCase("mercado")) {
            if (args.length == 1) {
                if (args[0].equalsIgnoreCase("criar")) {
                    if (LojaPosAPI.get(UUID) != null && LojaPosAPI.get(UUID).getLocation().equalsIgnoreCase("NULL")) {
                        LojaPosAPI.cache.remove(UUID);
                    }
                    if (com.mixedup.apis.LojaPosAPI.getLocation(UUID) != null) {
                        player.sendMessage(ChatColor.RED + " * Ops, você já contém uma warp de mercado!");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        return false;
                    }
                    final Inventory inventory = Bukkit.createInventory(null, 5 * 9, "Confirmar compra warp mercado: ");

                    final ItemStack info = new ItemStack(Material.PAPER);
                    final ItemMeta meta = info.getItemMeta();
                    meta.setDisplayName(ChatColor.YELLOW + "Informações");
                    final ArrayList<String> lore = new ArrayList<>();
                    lore.add(" ");
                    lore.add(ChatColor.WHITE + "Preço: " + ChatColor.GRAY + "10.000,00$");
                    lore.add(" ");
                    lore.add(ChatColor.WHITE + "TERMOS: ");
                    lore.add(ChatColor.GRAY + "Ao adquirir está warp todos terão acesso ao");
                    lore.add(ChatColor.GRAY + "local definido, logo será de sua responsabilidade");
                    lore.add(ChatColor.GRAY + "a proteção do local!");
                    lore.add(" ");
                    meta.setLore(lore);
                    info.setItemMeta(meta);
                    inventory.setItem(13, info);

                    final ItemStack confirm = new ItemStack(Material.LIME_DYE);
                    final ItemMeta metaConfirm = confirm.getItemMeta();
                    metaConfirm.setDisplayName(ChatColor.GREEN + "Confirmar");
                    confirm.setItemMeta(metaConfirm);
                    inventory.setItem(30, confirm);

                    final ItemStack negar = new ItemStack(Material.ROSE_RED);
                    final ItemMeta metaNegar = negar.getItemMeta();
                    metaNegar.setDisplayName(ChatColor.RED + "Cancelar");
                    negar.setItemMeta(metaNegar);
                    inventory.setItem(32, negar);

                    player.openInventory(inventory);
                } else if (args[0].equalsIgnoreCase("redefinir")) {
                    if (com.mixedup.apis.LojaPosAPI.getLocation(UUID) == null) {
                        player.sendMessage(ChatColor.RED + " * Ops, você não contém uma warp para efetuar este comando!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        return false;
                    }
                    final String location = player.getWorld().getName() + ":" + player.getLocation().getX() + ":" + player.getLocation().getY() + ":" + player.getLocation().getZ() + ":" + player.getLocation().getYaw() + ":" + player.getLocation().getPitch();
                    com.mixedup.apis.LojaPosAPI.updateLocation(UUID, location);

                    player.sendMessage(ChatColor.GREEN + " * Posição de spawn de sua mercado alterado com sucesso!\n" + ChatColor.GRAY + "  /mercado " + player.getName());
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else if (args[0].equalsIgnoreCase("deletar")) {
                    if (com.mixedup.apis.LojaPosAPI.getLocation(UUID) == null) {
                        player.sendMessage(ChatColor.RED + " * Ops, você não contém uma warp para efetuar este comando!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        return false;
                    }
                    final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Deletar warp mercado: ");

                    final ItemStack confirm = new ItemStack(Material.LIME_DYE);
                    final ItemMeta metaConfirm = confirm.getItemMeta();
                    metaConfirm.setDisplayName(ChatColor.GREEN + "Confirmar");
                    confirm.setItemMeta(metaConfirm);

                    final ItemStack negar = new ItemStack(Material.ROSE_RED);
                    final ItemMeta metaNegar = negar.getItemMeta();
                    metaNegar.setDisplayName(ChatColor.RED + "Cancelar");
                    negar.setItemMeta(metaNegar);

                    inventory.setItem(12, confirm);
                    inventory.setItem(14, negar);

                    player.openInventory(inventory);
                } else {
                    String loc = com.mixedup.apis.LojaPosAPI.getLocation(PlayerUUID.getUUID(args[0]));
                    if (loc != null) {
                        String[] split = loc.split(":");
                        Location location = new Location(Bukkit.getWorld(split[0]), Double.valueOf(split[1]), Double.valueOf(split[2]), Double.valueOf(split[3]), Float.parseFloat(split[4]), Float.parseFloat(split[5]));

                        player.teleport(location);
                        player.sendMessage(ChatColor.GREEN + " * Teletransportando!");
                        player.playSound(player.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 1.0F, 1.0F);
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);
                        if(player.getAllowFlight()) {

                            player.setAllowFlight(false);
                            if(ConfigAPI.getFlyStatus(UUID)) ConfigAPI.updateFly(UUID, false);

                        }

                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, este player não contém nenhuma mercado!");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
            } else {
                player.sendMessage(ChatColor.RED + " \nComandos referentes a /Mercado: " + "\n" + " " + "\n" + ChatColor.GRAY + "/mercado" + "\n" + ChatColor.GRAY + "/mercado (nick)" + "\n" + ChatColor.GRAY + "/mercado criar" + "\n" + ChatColor.GRAY + "/mercado redefinir" + "\n" + ChatColor.GRAY + "/mercado deletar" + "\n" + " ");
            }
        }
        return false;
    }
}
