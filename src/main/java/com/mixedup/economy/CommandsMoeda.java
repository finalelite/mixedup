package com.mixedup.economy;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.mixedup.MySql;
import com.mixedup.apis.AccountAPI;
import com.mixedup.apis.ConfigAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import com.mixedup.utils.AlternateColor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class CommandsMoeda implements CommandExecutor {

    public static LoadingCache<String, Map<String, Integer>> cache;

    static {
        CommandsMoeda.clearCache();
    }

    public static void clearCache() {
        CommandsMoeda.cache = CacheBuilder.newBuilder()
                .maximumSize(3)
                .expireAfterWrite(5, TimeUnit.MINUTES)
                .build(new CacheLoader<String, Map<String, Integer>>() {
                    public Map<String, Integer> load(final String type) {
                        return CommandsMoeda.getBestMoneyTopFromDB(type);
                    }
                });
    }

    private static Map<String, Integer> getBestMoneyTop(final String type) {
        try {
            return CommandsMoeda.cache.get(type);
        } catch (final Exception e) {
            return null;
        }
//        return getBestMoneyTopFromDB(type);
    }

    private static Map<String, Integer> getBestMoneyTopFromDB(final String type) {
        final Map<String, Integer> top = new LinkedHashMap<>();
        if (type.equalsIgnoreCase("coins")) {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Coins_data ORDER BY coins DESC LIMIT 6");
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    top.put(AccountAPI.getNickfromUUID(rs.getString("UUID")), rs.getInt("coins"));
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
        } else if (type.equalsIgnoreCase("blackcoins")) {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Coins_data ORDER BY blackcoins DESC LIMIT 6");
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    top.put(AccountAPI.getNickfromUUID(rs.getString("UUID")), rs.getInt("blackcoins"));
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
        }
        return top;
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("moeda") || cmd.getName().equalsIgnoreCase("money")) {
            final DecimalFormat formatter = new DecimalFormat("#,###.00");

            if (args.length == 0) {
                if (CoinsAPI.getCoins(UUID) == 0.0) {
                    player.sendMessage(ChatColor.GREEN + " * Você não contém dinheiro algum em sua conta D=");
                } else {
                    player.sendMessage(ChatColor.GREEN + " * Você contém em sua conta: " + formatter.format(CoinsAPI.getCoins(UUID)));
                }
            }

            if (args.length == 1) {
                if (args[0].equalsIgnoreCase("ajuda")) {
                    player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes a &nmoeda&c:\n \n&7 * /moeda - Veja quanto dinheiro você contém em sua conta.\n&7 * /moeda (nick) - Veja quanto seus amigos tem de dinheiro.\n&7 * /moeda enviar (nick) (quantia) - Transfira dinheiro para conta de seus amigos.\n \n"));
                    return false;
                }

                if (args[0].equalsIgnoreCase("top")) {
                    final Map<String, Integer> moneyTop = CommandsMoeda.getBestMoneyTop("coins");
                    String msg = null;

                    for (int i = 1; i <= 6; i++) {
                        if (i == 1) {
                            msg = ChatColor.GREEN + " \n Top economia: \n \n Top 1 - " + ChatColor.GRAY + moneyTop.keySet().toArray()[0] + ChatColor.GREEN + " ($" + formatter.format(moneyTop.values().toArray()[0]) + ")\n";
                        } else if (i == 2) {
                            msg = msg + ChatColor.GOLD + " Top 2 - " + ChatColor.GRAY + moneyTop.keySet().toArray()[1] + ChatColor.GOLD + " ($" + formatter.format(moneyTop.values().toArray()[1]) + ")\n";
                        } else if (i == 3) {
                            msg = msg + ChatColor.YELLOW + " Top 3 - " + ChatColor.GRAY + moneyTop.keySet().toArray()[2] + ChatColor.YELLOW + " ($" + formatter.format(moneyTop.values().toArray()[2]) + ")\n";
                        } else if (i == 4) {
                            msg = msg + ChatColor.YELLOW + " Top 4 - " + ChatColor.GRAY + moneyTop.keySet().toArray()[3] + " ($" + formatter.format(moneyTop.values().toArray()[3]) + ")\n";
                        } else if (i == 5) {
                            msg = msg + ChatColor.YELLOW + " Top 5 - " + ChatColor.GRAY + moneyTop.keySet().toArray()[4] + " ($" + formatter.format(moneyTop.values().toArray()[4]) + ")\n";
                        } else {
                            msg = msg + ChatColor.YELLOW + " Top 6 - " + ChatColor.GRAY + moneyTop.keySet().toArray()[5] + " ($" + formatter.format(moneyTop.values().toArray()[5]) + ")\n ";
                        }
                    }

                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                    player.sendMessage(msg);
                } else {
                    if (PlayerUUID.getUUID(args[0]) != null) {
                        if (CoinsAPI.getCoins(PlayerUUID.getUUID(args[0])) == 0.0) {
                            player.sendMessage(ChatColor.GREEN + " * Este player não contém dinheiro algum em sua conta.");
                        } else {
                            player.sendMessage(ChatColor.GREEN + " * O player " + args[0] + " contém em sua conta " + formatter.format(CoinsAPI.getCoins(PlayerUUID.getUUID(args[0]))) + " moedas.");
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, este usuario não esta registrado em nossos bancos de dados D=");
                    }
                }
            }

            if (args.length == 3) {
                if (args[0].equals("depositar") || args[0].equals("enviar") || args[0].equals("send")) {
                    try {
                        final int valor = Integer.valueOf(args[2]);

                        if (valor <= 0) {
                            player.sendMessage(ChatColor.RED + " * Ops, você não pode depositar este valor.");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            return false;
                        }
                        if (PlayerUUID.getUUID(args[1]) != null) {
                            if (CoinsAPI.getCoins(UUID) >= Double.parseDouble(args[2])) {
                                if (ConfigAPI.getDepositosStatus(PlayerUUID.getUUID(args[1])) == true) {
                                    CoinsAPI.updateCoins(PlayerUUID.getUUID(args[1]), CoinsAPI.getCoins(PlayerUUID.getUUID(args[1])) + Integer.parseInt(args[2]));
                                    CoinsAPI.updateCoins(UUID, CoinsAPI.getCoins(UUID) - Integer.parseInt(args[2]));
//                                    if (Main.scoreboardManager.hasSquadScoreboard(player)) {
//
//                                    } else {
//                                        Main.scoreboardManager.getDefaultScoreboard().updateEntry(player, "coins");
//                                    }
                                    player.sendMessage(ChatColor.GREEN + " * Foram debitados de sua conta " + formatter.format(Double.parseDouble(args[2])) + " e acrescentados a conta de " + args[1] + " " + formatter.format(Double.parseDouble(args[2])) + ".");

                                    final Player target = Bukkit.getPlayerExact(args[1]);
                                    if (target.isOnline()) {
                                        target.sendMessage(ChatColor.GREEN + " * Foram depositado em sua conta " + formatter.format(Double.parseDouble(args[2])) + " moeda, por " + player.getName() + ".");
//                                        if (Main.scoreboardManager.hasSquadScoreboard(target)) {
//
//                                        } else {
//                                            Main.scoreboardManager.getDefaultScoreboard().updateEntry(target, "coins");
//                                        }
                                    }
                                } else {
                                    player.sendMessage(ChatColor.RED + "Ops, este player bloqueou a opção de receber depósitos.");
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + "Ops, você não contém dinheiro para efetuar este deposito!");
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, este usuario não esta registrado em nossos bancos de dados D=");
                        }
                    } catch (final Exception e) {
                        player.sendMessage(ChatColor.RED + "Ops, algo ocorreu de errado. Por favor certifique se do comando utilizado e tente novamente.");
                    }
                }

                if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                    if (args[0].equals("adicionar")) {
                        if (PlayerUUID.getUUID(args[1]) != null) {
                            try {
                                CoinsAPI.updateCoins(PlayerUUID.getUUID(args[1]), CoinsAPI.getCoins(PlayerUUID.getUUID(args[1])) + Integer.parseInt(args[2]));
                                player.sendMessage(ChatColor.GREEN + " * Foram adicionados a conta de " + args[1] + " " + formatter.format(Double.parseDouble(args[2])) + " moedas.");
//                                if (Main.scoreboardManager.hasSquadScoreboard(player)) {
//
//                                } else {
//                                    Main.scoreboardManager.getDefaultScoreboard().updateEntry(player, "coins");
//                                }
                                if (!player.getName().equals(args[1])) {
                                    final Player target = Bukkit.getPlayerExact(args[1]);
                                    if (target.isOnline()) {
                                        target.sendMessage(ChatColor.GREEN + " * Foram adicionados em sua conta " + formatter.format(Double.parseDouble(args[2])) + " moedas, por " + player.getName() + ".");
//                                        if (Main.scoreboardManager.hasSquadScoreboard(target)) {
//
//                                        } else {
//                                            Main.scoreboardManager.getDefaultScoreboard().updateEntry(target, "coins");
//                                        }
                                    }
                                }
                            } catch (final Exception e) {
                                player.sendMessage(ChatColor.RED + "Ops, algo ocorreu de errado. Por favor certifique se do comando utilizado e tente novamente.");
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, este usuario não esta registrado em nossos bancos de dados D=");
                        }
                    }

                    if (args[0].equals("remover")) {
                        if (PlayerUUID.getUUID(args[1]) != null) {
                            try {
                                if (CoinsAPI.getCoins(PlayerUUID.getUUID(args[1])) - Double.parseDouble(args[2]) >= 0) {
                                    CoinsAPI.updateCoins(PlayerUUID.getUUID(args[1]), CoinsAPI.getCoins(PlayerUUID.getUUID(args[1])) - Integer.parseInt(args[2]));
                                } else {
                                    CoinsAPI.updateCoins(PlayerUUID.getUUID(args[1]), 0);
                                }
                                player.sendMessage(ChatColor.GREEN + " * Foram removidos da conta de " + args[1] + " " + formatter.format(Double.parseDouble(args[2])) + " moedas.");
//                                if (Main.scoreboardManager.hasSquadScoreboard(player)) {
//
//                                } else {
//                                    Main.scoreboardManager.getDefaultScoreboard().updateEntry(player, "coins");
//                                }
                                if (!player.getName().equals(args[1])) {
                                    final Player target = Bukkit.getPlayerExact(args[1]);
                                    if (target.isOnline()) {
                                        target.sendMessage(ChatColor.GREEN + " * Foram removidos de sua conta " + formatter.format(Double.parseDouble(args[2])) + " moedas, por " + player.getName() + ".");
//                                        if (Main.scoreboardManager.hasSquadScoreboard(target)) {
//
//                                        } else {
//                                            Main.scoreboardManager.getDefaultScoreboard().updateEntry(target, "coins");
//                                        }
                                    }
                                }
                            } catch (final Exception e) {
                                player.sendMessage(ChatColor.RED + "Ops, algo ocorreu de errado. Por favor certifique se do comando utilizado e tente novamente.");
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, este usuario não esta registrado em nossos bancos de dados D=");
                        }
                    }

                    if (args[0].equals("setar")) {
                        if (PlayerUUID.getUUID(args[1]) != null) {
                            try {
                                CoinsAPI.updateCoins(PlayerUUID.getUUID(args[1]), Integer.parseInt(args[2]));
                                player.sendMessage(ChatColor.GREEN + " * Você definiu para a conta de " + args[1] + " " + formatter.format(Double.parseDouble(args[2])) + " moedas.");
//                                if (Main.scoreboardManager.hasSquadScoreboard(player)) {
//
//                                } else {
//                                    Main.scoreboardManager.getDefaultScoreboard().updateEntry(player, "coins");
//                                }
                                if (!player.getName().equals(args[1])) {
                                    final Player target = Bukkit.getPlayerExact(args[1]);
                                    if (target.isOnline()) {
                                        target.sendMessage(ChatColor.GREEN + " * Foi definido em sua conta " + formatter.format(Double.parseDouble(args[2])) + " moedas, por " + player.getName() + ".");
//                                        if (Main.scoreboardManager.hasSquadScoreboard(target)) {
//
//                                        } else {
//                                            Main.scoreboardManager.getDefaultScoreboard().updateEntry(target, "coins");
//                                        }
                                    }
                                }
                            } catch (final Exception e) {
                                player.sendMessage(ChatColor.RED + "Ops, algo ocorreu de errado. Por favor certifique se do comando utilizado e tente novamente.");
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, este usuario não esta registrado em nossos bancos de dados D=");
                        }
                    }
                }
            }

            if (args.length >= 4) {
                if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                    player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes a &nmoeda&c:\n \n&7 - /moeda\n&7 - /moeda (nick)\n&7 - /moeda depositar (nick) (quantia)\n&7 - /moeda adicionar (nick) (quantia)\n&7 - /moeda remover (nick) (quantia)\n&7 - /moeda setar (nick) (quantia)\n \n"));
                } else {
                    player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes a &nmoeda&c:\n \n&7 * /moeda - Veja quanto dinheiro você contém em sua conta.\n&7 * /moeda (nick) - Veja quanto seus amigos tem de dinheiro.\n&7 * /moeda depositar (nick) (quantia) - Transfira dinheiro para conta de seus amigos.\n \n"));
                }
            }
        }

        if (cmd.getName().equalsIgnoreCase("moedanegra") || cmd.getName().equalsIgnoreCase("blackcoin")) {
            final DecimalFormat formatter = new DecimalFormat("#,###.00");

            if (args.length == 0) {
                if (CoinsAPI.getBlackCoins(UUID) == 0.0) {
                    player.sendMessage(ChatColor.GREEN + " * Você não contém dinheiro algum em sua conta D=");
                } else {
                    player.sendMessage(ChatColor.GREEN + " * Você contém em sua conta: " + formatter.format(CoinsAPI.getBlackCoins(UUID)));
                }
            }

            if (args.length == 1) {
                if (args[0].equalsIgnoreCase("top")) {
                    final Map<String, Integer> moneyTop = CommandsMoeda.getBestMoneyTop("blackcoins");
                    String msg = null;

                    for (int i = 1; i <= 6; i++) {
                        if (i == 1) {
                            msg = ChatColor.GREEN + " \n Top economia (Moeda negra): \n \n Top 1 - " + ChatColor.GRAY + moneyTop.keySet().toArray()[0] + ChatColor.GREEN + " ($" + formatter.format(moneyTop.values().toArray()[0]) + ")\n";
                        } else if (i == 2) {
                            msg = msg + ChatColor.GOLD + " Top 2 - " + ChatColor.GRAY + moneyTop.keySet().toArray()[1] + ChatColor.GOLD + " ($" + formatter.format(moneyTop.values().toArray()[1]) + ")\n";
                        } else if (i == 3) {
                            msg = msg + ChatColor.YELLOW + " Top 3 - " + ChatColor.GRAY + moneyTop.keySet().toArray()[2] + ChatColor.YELLOW + " ($" + formatter.format(moneyTop.values().toArray()[2]) + ")\n";
                        } else if (i == 4) {
                            msg = msg + ChatColor.YELLOW + " Top 4 - " + ChatColor.GRAY + moneyTop.keySet().toArray()[3] + " ($" + formatter.format(moneyTop.values().toArray()[3]) + ")\n";
                        } else if (i == 5) {
                            msg = msg + ChatColor.YELLOW + " Top 5 - " + ChatColor.GRAY + moneyTop.keySet().toArray()[4] + " ($" + formatter.format(moneyTop.values().toArray()[4]) + ")\n";
                        } else if (i == 6) {
                            msg = msg + ChatColor.YELLOW + " Top 6 - " + ChatColor.GRAY + moneyTop.keySet().toArray()[5] + " ($" + formatter.format(moneyTop.values().toArray()[5]) + ")\n ";
                        }
                    }

                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                    player.sendMessage(msg);
                } else {
                    if (PlayerUUID.getUUID(args[0]) != null) {
                        if (CoinsAPI.getBlackCoins(PlayerUUID.getUUID(args[0])) == 0.0) {
                            player.sendMessage(ChatColor.GREEN + " * Este player não contém dinheiro algum em sua conta.");
                        } else {
                            player.sendMessage(ChatColor.GREEN + " * O player " + args[0] + " contém em sua conta " + formatter.format(CoinsAPI.getBlackCoins(PlayerUUID.getUUID(args[0]))) + " moedas.");
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, este usuario não esta registrado em nossos bancos de dados D=");
                    }
                }
            }

            if (args.length == 3) {
                if (args[0].equals("depositar") || args[0].equals("enviar") || args[0].equals("send")) {
                    try {
                        final int valor = Integer.valueOf(args[2]);

                        if (valor <= 0) {
                            player.sendMessage(ChatColor.RED + " * Ops, você não pode depositar este valor.");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            return false;
                        }
                        if (PlayerUUID.getUUID(args[1]) != null) {
                            if (CoinsAPI.getBlackCoins(UUID) >= Double.parseDouble(args[2])) {
                                if (ConfigAPI.getDepositosStatus(PlayerUUID.getUUID(args[1])) == true) {
                                    CoinsAPI.updateBlackCoins(PlayerUUID.getUUID(args[1]), CoinsAPI.getBlackCoins(PlayerUUID.getUUID(args[1])) + Integer.parseInt(args[2]));
                                    CoinsAPI.updateBlackCoins(UUID, CoinsAPI.getBlackCoins(UUID) - Integer.parseInt(args[2]));
//                                    if (Main.scoreboardManager.hasSquadScoreboard(player)) {
//
//                                    } else {
//                                        Main.scoreboardManager.getDefaultScoreboard().updateEntry(player, "coins");
//                                    }

                                    player.sendMessage(ChatColor.GREEN + " * Foram debitados de sua conta " + formatter.format(Double.parseDouble(args[2])) + " e acrescentados a conta de " + args[1] + " " + formatter.format(Double.parseDouble(args[2])) + ".");

                                    final Player target = Bukkit.getPlayerExact(args[1]);
                                    if (target.isOnline()) {
                                        target.sendMessage(ChatColor.GREEN + " * Foram depositado em sua conta " + formatter.format(Double.parseDouble(args[2])) + " moeda, por " + player.getName() + ".");
//                                        if (Main.scoreboardManager.hasSquadScoreboard(target)) {
//
//                                        } else {
//                                            Main.scoreboardManager.getDefaultScoreboard().updateEntry(target, "coins");
//                                        }
                                    }
                                } else {
                                    player.sendMessage(ChatColor.RED + "Ops, este player bloqueou a opção de receber depósitos.");
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + "Ops, você não contém dinheiro para efetuar este deposito!");
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, este usuario não esta registrado em nossos bancos de dados D=");
                        }
                    } catch (final Exception e) {
                        player.sendMessage(ChatColor.RED + "Ops, algo ocorreu de errado. Por favor certifique se do comando utilizado e tente novamente.");
                    }
                }

                if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                    if (args[0].equals("adicionar")) {
                        if (PlayerUUID.getUUID(args[1]) != null) {
                            try {
                                CoinsAPI.updateBlackCoins(PlayerUUID.getUUID(args[1]), CoinsAPI.getBlackCoins(PlayerUUID.getUUID(args[1])) + Integer.parseInt(args[2]));
                                player.sendMessage(ChatColor.GREEN + " * Foram adicionados a conta de " + args[1] + " " + formatter.format(Double.parseDouble(args[2])) + " moedas.");
//                                if (Main.scoreboardManager.hasSquadScoreboard(player)) {
//
//                                } else {
//                                    Main.scoreboardManager.getDefaultScoreboard().updateEntry(player, "coins");
//                                }
                                if (!player.getName().equals(args[1])) {
                                    final Player target = Bukkit.getPlayerExact(args[1]);
                                    if (target.isOnline()) {
                                        target.sendMessage(ChatColor.GREEN + " * Foram adicionados em sua conta " + formatter.format(Double.parseDouble(args[2])) + " moedas, por " + player.getName() + ".");
//                                        if (Main.scoreboardManager.hasSquadScoreboard(target)) {
//
//                                        } else {
//                                            Main.scoreboardManager.getDefaultScoreboard().updateEntry(target, "coins");
//                                        }
                                    }
                                }
                            } catch (final Exception e) {
                                player.sendMessage(ChatColor.RED + "Ops, algo ocorreu de errado. Por favor certifique se do comando utilizado e tente novamente.");
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, este usuario não esta registrado em nossos bancos de dados D=");
                        }
                    }

                    if (args[0].equals("remover")) {
                        if (PlayerUUID.getUUID(args[1]) != null) {
                            try {
                                if (CoinsAPI.getBlackCoins(PlayerUUID.getUUID(args[1])) - Double.parseDouble(args[2]) >= 0) {
                                    CoinsAPI.updateBlackCoins(PlayerUUID.getUUID(args[1]), CoinsAPI.getBlackCoins(PlayerUUID.getUUID(args[1])) - Integer.parseInt(args[2]));
                                } else {
                                    CoinsAPI.updateBlackCoins(PlayerUUID.getUUID(args[1]), 0);
                                }
                                player.sendMessage(ChatColor.GREEN + " * Foram removidos da conta de " + args[1] + " " + formatter.format(Double.parseDouble(args[2])) + " moedas.");
//                                if (Main.scoreboardManager.hasSquadScoreboard(player)) {
//
//                                } else {
//                                    Main.scoreboardManager.getDefaultScoreboard().updateEntry(player, "coins");
//                                }
                                if (!player.getName().equals(args[1])) {
                                    final Player target = Bukkit.getPlayerExact(args[1]);
                                    if (target.isOnline()) {
                                        target.sendMessage(ChatColor.GREEN + " * Foram removidos de sua conta " + formatter.format(Double.parseDouble(args[2])) + " moedas, por " + player.getName() + ".");
//                                        if (Main.scoreboardManager.hasSquadScoreboard(target)) {
//
//                                        } else {
//                                            Main.scoreboardManager.getDefaultScoreboard().updateEntry(target, "coins");
//                                        }
                                    }
                                }
                            } catch (final Exception e) {
                                player.sendMessage(ChatColor.RED + "Ops, algo ocorreu de errado. Por favor certifique se do comando utilizado e tente novamente.");
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, este usuario não esta registrado em nossos bancos de dados D=");
                        }
                    }

                    if (args[0].equals("setar")) {
                        if (PlayerUUID.getUUID(args[1]) != null) {
                            try {
                                CoinsAPI.updateBlackCoins(PlayerUUID.getUUID(args[1]), Integer.parseInt(args[2]));
                                player.sendMessage(ChatColor.GREEN + " * Você definiu para a conta de " + args[1] + " " + formatter.format(Double.parseDouble(args[2])) + " moedas.");
//                                if (Main.scoreboardManager.hasSquadScoreboard(player)) {
//
//                                } else {
//                                    Main.scoreboardManager.getDefaultScoreboard().updateEntry(player, "coins");
//                                }
                                if (!player.getName().equals(args[1])) {
                                    final Player target = Bukkit.getPlayerExact(args[1]);
                                    if (target.isOnline()) {
                                        target.sendMessage(ChatColor.GREEN + " * Foi definido em sua conta " + formatter.format(Double.parseDouble(args[2])) + " moedas, por " + player.getName() + ".");
//                                        if (Main.scoreboardManager.hasSquadScoreboard(target)) {
//
//                                        } else {
//                                            Main.scoreboardManager.getDefaultScoreboard().updateEntry(target, "coins");
//                                        }
                                    }
                                }
                            } catch (final Exception e) {
                                player.sendMessage(ChatColor.RED + "Ops, algo ocorreu de errado. Por favor certifique se do comando utilizado e tente novamente.");
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, este usuario não esta registrado em nossos bancos de dados D=");
                        }
                    }
                }
            }

            if (args.length >= 4) {
                if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                    player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes a &nmoeda&c:\n \n&7 - /moeda\n&7 - /moeda (nick)\n&7 - /moeda depositar (nick) (quantia)\n&7 - /moeda adicionar (nick) (quantia)\n&7 - /moeda remover (nick) (quantia)\n&7 - /moeda setar (nick) (quantia)\n \n"));
                } else {
                    player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes a &nmoeda&c:\n \n&7 * /moeda - Veja quanto dinheiro você contém em sua conta.\n&7 * /moeda (nick) - Veja quanto seus amigos tem de dinheiro.\n&7 * /moeda enviar (nick) (quantia) - Transfira dinheiro para conta de seus amigos.\n \n"));
                }
            }
        }
        return false;
    }
}