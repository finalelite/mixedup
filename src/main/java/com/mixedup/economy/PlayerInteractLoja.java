package com.mixedup.economy;

import com.mixedup.Main;
import com.mixedup.apis.AccountAPI;
import com.mixedup.utils.RemoveItemInv;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.Container;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map.Entry;

public class PlayerInteractLoja implements Listener {

    private static final DecimalFormat formatter = new DecimalFormat("#,###.00");

    private static boolean haveSpace(final Inventory inventory) {
        boolean have = false;
        for (int i = 0; i <= inventory.getSize(); i++) {
            if (inventory.getItem(i) == null || inventory.getItem(i).getType().equals(Material.AIR)) {
                have = true;
                break;
            }
        }
        return have;
    }

    public int getAmount(final Inventory inventory, final Material item) {
        int i = 0;
        for (final ItemStack is : inventory.getContents()) {
            if (is != null) {
                if (is.getType() == item) {
                    i += is.getAmount();
                }
            }
        }
        return i;
    }

    @EventHandler
    public void onBuyAndSellItem(final PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        final String UUID = player.getUniqueId().toString();

        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && event.getClickedBlock().getType() != null) {
            if (event.getClickedBlock().getType().equals(Material.SIGN) || event.getClickedBlock().getType().equals(Material.WALL_SIGN)) {
                final Sign sign = (Sign) event.getClickedBlock().getState();
                if (sign.getLine(0).equals(ChatColor.DARK_AQUA + "[Loja]")) {
                    final String loc = sign.getWorld().getName() + ":" + sign.getX() + ":" + sign.getY() + ":" + sign.getZ();

                    String owner = LojaAPI.getDono(loc);
                    if (owner != null) {
                        if (!owner.equals(UUID)) {
                            final Block block = event.getClickedBlock();
                            final org.bukkit.material.Sign sd = (org.bukkit.material.Sign) sign.getData();
                            final Block attachedBlock = block.getRelative(sd.getAttachedFace());
                            final Inventory inv = ((Container) attachedBlock.getState()).getInventory();
                            final String[] itemList = LojaAPI.getItem(loc).split(":");
                            final String[] quantiaList = sign.getLine(1).split(":");
                            final ItemStack item = new ItemStack(Material.getMaterial(itemList[0]), Integer.valueOf(quantiaList[1].substring(1)));
                            if (!sign.getLine(3).contains("C")) {
                                player.sendMessage(ChatColor.RED + "Ops, está loja não está vendendo este item no momento!");
                                return;
                            }

                            if (getAmount(inv, item.getType()) >= Integer.valueOf(quantiaList[1].substring(1))) {
                                final String[] line3 = sign.getLine(3).split(":");
                                final int compra = Integer.valueOf(line3[0].substring(2));

                                if (compra == 0) {
                                    player.sendMessage(ChatColor.RED + "Ops, você não pode comprar este item nesta loja!");
                                    return;
                                }

                                if (CoinsAPI.getCoins(UUID) >= compra) {
                                    CoinsAPI.updateCoins(UUID, CoinsAPI.getCoins(UUID) - compra);
                                    CoinsAPI.updateCoins(owner, CoinsAPI.getCoins(owner) + compra);

                                    Main.scoreboardManager.getDefaultScoreboard().updateEntry(player, "coins");
                                    player.sendMessage(ChatColor.GREEN + " * Você acaba de comprar " + quantiaList[1].substring(1) + " " + BlocksEnum.getMaterial(item).replace("_", " ") + " e foram debitados de sua conta $" + formatter.format(compra) + ".");

                                    for (Entry<Integer, ? extends ItemStack> e : inv.all(Material.getMaterial(itemList[0])).entrySet()) {
                                        ItemStack two = e.getValue();
                                        ItemStack stack = two.clone();
                                        stack.setAmount(item.getAmount());
                                        HashMap<Integer, ItemStack> items = player.getOpenInventory().getBottomInventory().addItem(stack);

                                        for (Entry<Integer, ItemStack> entry : items.entrySet()) {

                                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());

                                        }

                                        break;

                                    }

                                    RemoveItemInv.removeItemsFromType(inv, item, item.getAmount());
                                    //inv.removeItem(item);

                                    final Player target = Bukkit.getPlayer(AccountAPI.getNick(owner));

                                    if (target != null) {
                                        target.sendMessage(ChatColor.GREEN + " * Você acabou de vender um item em sua loja.");
                                        Main.scoreboardManager.getDefaultScoreboard().updateEntry(target, "coins");
                                    }
                                } else {
                                    player.sendMessage(ChatColor.GREEN + "Ops, você não contém dinheiro para efetuar está compra.");
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + "Ops, está loja não contém estoque deste item no momento.");
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, você não pode fazer isto.");
                        }
                    }
                }
            }
        } else if (event.getAction().equals(Action.LEFT_CLICK_BLOCK) && event.getClickedBlock().getType() != null) {
            if (event.getClickedBlock().getType().equals(Material.SIGN) || event.getClickedBlock().getType().equals(Material.WALL_SIGN)) {
                final Sign sign = (Sign) event.getClickedBlock().getState();

                if (sign.getLine(0).equals(ChatColor.DARK_AQUA + "[Loja]")) {
                    final String loc = sign.getWorld().getName() + ":" + sign.getX() + ":" + sign.getY() + ":" + sign.getZ();

                    String owner = LojaAPI.getDono(loc);
                    if (owner != null) {
                        if (!owner.equals(UUID)) {
                            final Block block = event.getClickedBlock();
                            final org.bukkit.material.Sign sd = (org.bukkit.material.Sign) sign.getData();
                            final Block attachedBlock = block.getRelative(sd.getAttachedFace());
                            final Inventory inv = ((Container) attachedBlock.getState()).getInventory();
                            final String[] quantiaList = sign.getLine(1).split(":");
                            final ItemStack item = new ItemStack(Material.getMaterial(LojaAPI.getItem(loc)), Integer.valueOf(quantiaList[1].substring(1)));

                            if (haveSpace(inv) == true) {
                                int venda = 0;

                                if (!sign.getLine(3).contains("V")) {
                                    player.sendMessage(ChatColor.RED + "Ops, está loja não está aceitando vendas deste item no momento.");
                                    return;
                                }

                                if (sign.getLine(3).contains(":")) {
                                    final String[] line3 = sign.getLine(3).split(":");
                                    venda = Integer.valueOf(line3[1].substring(0, line3[1].length() - 2));
                                } else {
                                    venda = Integer.valueOf(sign.getLine(3).substring(2));
                                }

                                if (venda == 0) {
                                    player.sendMessage(ChatColor.RED + "Ops, você não pode vender este item nesta loja!");
                                    return;
                                }

                                boolean custom = false;
                                int itemQuantia = 0;
                                for (int i = 1; i <= player.getInventory().getSize(); i++) {
                                    if (player.getInventory().getItem(i - 1) != null && !player.getInventory().getItem(i - 1).getType().equals(Material.AIR) && player.getInventory().getItem(i - 1).getType().equals(item.getType())) {
                                        itemQuantia += player.getInventory().getItem(i - 1).getAmount();
                                    }
                                    if(player.getInventory().getItem(i -1) != null && !player.getInventory().getItem(i -1).getType().equals(Material.AIR) && player.getInventory().getItem(i - 1).getType().equals(item.getType()) && player.getInventory().getItem(i - 1).getItemMeta() != null) {
                                        if(player.getInventory().getItem(i - 1).getItemMeta().hasDisplayName()) custom = true;
                                        if(player.getInventory().getItem(i -1).getItemMeta().hasLore()) custom = true;
                                        if(player.getInventory().getItem(i - 1).getItemMeta().hasEnchants()) custom = true;
                                    }
                                }

                                if(custom) {

                                    player.sendMessage(ChatColor.RED + " * Ops, itens customizados não são aceitos.");
                                    return;

                                }

                                if (item.getAmount() > itemQuantia) {
                                    player.sendMessage(ChatColor.RED + " * Ops, você não contém este item para vende-lo!");
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    return;
                                }

                                if (CoinsAPI.getCoins(owner) < venda) {
                                    player.sendMessage(ChatColor.RED + " * Ops, este vendedor não contém dinheiro!");
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                    return;
                                }

                                CoinsAPI.updateCoins(UUID, CoinsAPI.getCoins(UUID) + venda);
                                CoinsAPI.updateCoins(owner, CoinsAPI.getCoins(owner) - venda);

                                for (final Player target : Bukkit.getOnlinePlayers()) {
                                    if (target.getName().equalsIgnoreCase(AccountAPI.getNick(owner))) {
                                        if (target.isOnline()) {
                                            target.sendMessage(ChatColor.GREEN + " * Algum player acabou de lhe vender algum item em sua loja.");
                                            Main.scoreboardManager.getDefaultScoreboard().updateEntry(target, "coins");
                                        }
                                    }
                                }
                                Main.scoreboardManager.getDefaultScoreboard().updateEntry(player, "coins");
                                player.sendMessage(ChatColor.GREEN + " * Você acaba de vender " + quantiaList[1].substring(1) + " " + BlocksEnum.getMaterial(item).replace("_", " ") + " e foram adicionados em sua conta $" + formatter.format(venda) + ".");
                                RemoveItemInv.removeItemsFromType(player.getInventory(), item, item.getAmount());
                                //player.getInventory().removeItem(item);
                                inv.addItem(item);
                            } else {
                                player.sendMessage(ChatColor.RED + "Ops, está loja não está aceitando vendas deste item no momento.");
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, você não pode fazer isto.");
                        }
                    }
                }
            }
        }
    }
}
