package com.mixedup.economy;

import com.mixedup.Main;
import com.mixedup.MySql;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class LojaAPI {

    private static final LojaCache cache = new LojaCache();

    public static int valorCompra(final String item, final String type) {
        int r = 0;

        if (type.equalsIgnoreCase("vip"))
            r = LojaAPI.cache.getItem(item).getCompra() - (LojaAPI.cache.getItem(item).getCompra() * 7) / 100;
        if (type.equalsIgnoreCase("sanguinaria"))
            r = LojaAPI.cache.getItem(item).getCompra() + (LojaAPI.cache.getItem(item).getCompra() * 2) / 100;
        if (type.equalsIgnoreCase("ancia")) r = LojaAPI.cache.getItem(item).getCompra();
        if (type.equalsIgnoreCase("nobre"))
            r = LojaAPI.cache.getItem(item).getCompra() - (LojaAPI.cache.getItem(item).getCompra() * 2) / 100;

        return r;
    }

    public static int valorVenda(final String item, final String type) {
        int r = 0;

        if (type.equalsIgnoreCase("vip"))
            r = LojaAPI.cache.getItem(item).getVenda() + (LojaAPI.cache.getItem(item).getVenda() * 7) / 100;
        if (type.equalsIgnoreCase("sanguinaria"))
            r = LojaAPI.cache.getItem(item).getVenda() - (LojaAPI.cache.getItem(item).getVenda() * 2) / 100;
        if (type.equalsIgnoreCase("ancia")) r = LojaAPI.cache.getItem(item).getVenda();
        if (type.equalsIgnoreCase("nobre"))
            r = LojaAPI.cache.getItem(item).getVenda() + (LojaAPI.cache.getItem(item).getVenda() * 2) / 100;

        return r;
    }

    public static int itemQuantia(final String item, final String type) {
        return LojaAPI.cache.getItem(item).getQuantia();
    }

    public static String getExist(final String item) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Loja_data WHERE Item = ?");
            st.setString(1, item);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Item");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void updateValorCompra(final String item, final int valorCompra) {
        LojaAPI.cache.updateValorCompra(item, valorCompra);
    }

    public static void updateValorVenda(final String item, final int valorVenda) {
        LojaAPI.cache.updateValorVenda(item, valorVenda);
    }

    public static void updateItemQuantia(final String item, final int itemQuantia) {
        LojaAPI.cache.updateItemQuantia(item, itemQuantia);
    }

    //----------------------------------------------------------------------------------------------------------------------------------


    public static void setLoja(final String UUID, final String location, final String item) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Lojas_data(UUID, Location, Item) VALUES (?, ?, ?)");
                    st.setString(1, UUID);
                    st.setString(2, location);
                    st.setString(3, item);
                    st.executeUpdate();

                    new LojaAPI(location, UUID, item).insert();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static String getDono(final String location) {
        if (get(location) != null) {
            return get(location).getUUID();
        } else return null;
    }

    public static String getItem(final String location) {
        if (get(location) != null) {
            return get(location).getItem();
        } else return null;
    }

    public static void removeLoja(final String location) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Lojas_data WHERE Location = ?");
                    st.setString(1, location);
                    st.executeUpdate();

                    CACHE.remove(location);
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void setCache() {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Lojas_data");
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                new LojaAPI(rs.getString("Location"), rs.getString("UUID"), rs.getString("Item")).insert();
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    //------------------------------------------------------------------------------------------------------------------------------------------


    private String location;
    private String uuid;
    private String item;

    public static HashMap<String, LojaAPI> CACHE = new HashMap<>();

    public LojaAPI (String location, String uuid, String item) {
        this.location = location;
        this.uuid = uuid;
        this.item = item;
    }

    public static LojaAPI get(String location) {
        return CACHE.get(location);
    }

    public LojaAPI insert() {
        CACHE.put(this.location, this);
        return this;
    }

    public String getUUID() {
        return this.uuid;
    }

    public String getItem() {
        return this.item;
    }
}
