package com.mixedup.economy;

import br.com.finalelite.pauloo27.api.PauloAPI;
import br.com.finalelite.pauloo27.api.database.EconomyManager;
import br.com.finalelite.pauloo27.api.database.PlayerCoinType;
import com.mixedup.MySql;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CoinsAPI {

    public static EconomyManager economyManager = PauloAPI.getInstance().getEconomyManager();

    public static void setCoins(final String UUID, final int coins, final int blackcoins) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Coins_data (UUID, coins, blackcoins) VALUES (?, ?, ?)");
            st.setString(1, UUID);
            st.setInt(2, coins);
            st.setInt(3, blackcoins);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateCoins(final String uuid, final int coins) {
        economyManager.setCoin(uuid, PlayerCoinType.COIN, coins);
    }

    public static void updateBlackCoins(final String uuid, final int coins) {
        economyManager.setCoin(uuid, PlayerCoinType.BLACKCOIN, coins);

    }

    public static int getCoins(final String uuid) {
        return checkInfos(uuid);
    }

    public static int getBlackCoins(final String uuid) {
        if (economyManager.getPlayerEconomy(uuid) == null) return 0;
        return (int) economyManager.getPlayerEconomy(uuid).getBalance(PlayerCoinType.BLACKCOIN);
    }

    public static int checkInfos(final String uuid) {
        try {
            return (int) economyManager.getPlayerEconomy(uuid).getBalance(PlayerCoinType.COIN);
        } catch (final Exception e) {
            setCoins(uuid, 0, 0);
            return checkInfos(uuid);
        }
    }
}
