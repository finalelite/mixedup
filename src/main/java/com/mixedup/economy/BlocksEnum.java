package com.mixedup.economy;

import br.com.finalelite.pauloo27.api.PauloAPI;
import org.bukkit.inventory.ItemStack;

public class BlocksEnum {

    public static String getMaterial(final ItemStack block) {
        return PauloAPI.getInstance().getPortugueseLocale().getMaterialName(block.getType());
    }
}
