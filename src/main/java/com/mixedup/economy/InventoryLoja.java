package com.mixedup.economy;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class InventoryLoja {

    public static Inventory INVENTORY_LOJAELYTRAS = Bukkit.createInventory(null, 6 * 9, "Elytras: ");

    public static DecimalFormat formatter = new DecimalFormat("#,###.00");

    public static ItemStack addGlow(final ItemStack item) {

        item.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
        final ItemMeta meta = item.getItemMeta();
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        item.setItemMeta(meta);

        return item;
    }

    public static Inventory buildLojaTnts(final String type) {
        final Inventory inventory = Bukkit.createInventory(null, 4 * 9, "Explosivos: ");
        final DecimalFormat formatterEnergia = new DecimalFormat("#####.#####");

        final ItemStack creeper = new ItemStack(Material.CREEPER_SPAWN_EGG);
        final ItemMeta meta10 = creeper.getItemMeta();
        meta10.setDisplayName(ChatColor.RED + "Ovo de creeper");
        final List<String> lore10 = new ArrayList<>();
        if (LojaAPI.valorVenda("ovo_de_creeper", type) != 0 && LojaAPI.valorCompra("ovo_de_creeper", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("ovo_de_creeper", type)));
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("ovo_de_creeper", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("ovo_de_creeper", type));
            lore10.add(" ");
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            meta10.setLore(lore10);
        } else if (LojaAPI.valorCompra("ovo_de_creeper", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("ovo_de_creeper", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("ovo_de_creeper", type));
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add(" ");
            meta10.setLore(lore10);
        } else if (LojaAPI.valorVenda("ovo_de_creeper", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("ovo_de_creeper", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("ovo_de_creeper", type));
            lore10.add(" ");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore10.add(" ");
            meta10.setLore(lore10);
        }
        meta10.setLore(lore10);
        creeper.setItemMeta(meta10);
        inventory.setItem(24, creeper);

        final ItemStack ghasts = new ItemStack(Material.GHAST_SPAWN_EGG);
        final ItemMeta meta11 = ghasts.getItemMeta();
        meta11.setDisplayName(ChatColor.RED + "Ovo de ghast");
        final List<String> lore11 = new ArrayList<>();
        if (LojaAPI.valorVenda("ovo_de_ghast", type) != 0 && LojaAPI.valorCompra("ovo_de_ghast", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("ovo_de_ghast", type)));
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("ovo_de_ghast", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("ovo_de_ghast", type));
            lore11.add(" ");
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            meta11.setLore(lore11);
        } else if (LojaAPI.valorCompra("ovo_de_ghast", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("ovo_de_ghast", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("ovo_de_ghast", type));
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add(" ");
            meta11.setLore(lore11);
        } else if (LojaAPI.valorVenda("ovo_de_ghast", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("ovo_de_ghast", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("ovo_de_ghast", type));
            lore11.add(" ");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore11.add(" ");
            meta11.setLore(lore11);
        }
        meta11.setLore(lore11);
        ghasts.setItemMeta(meta11);
        inventory.setItem(25, ghasts);

        final ItemStack tntRepulsao = new ItemStack(Material.TNT);
        final ItemMeta meta3 = tntRepulsao.getItemMeta();
        meta3.setDisplayName(ChatColor.RED + "Explosivo de repulsão");
        final List<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(7000) + " + " + formatterEnergia.format(62.775) + "kW");
        lore.add("§7Quantia: §a16");
        lore.add(" ");
        meta3.setLore(lore);
        tntRepulsao.setItemMeta(meta3);
        inventory.setItem(10, InventoryLoja.addGlow(tntRepulsao));

        final ItemStack tntH = new ItemStack(Material.TNT);
        final ItemMeta meta2 = tntH.getItemMeta();
        meta2.setDisplayName(ChatColor.RED + "Explosivo de hidrogênio (5/5)");
        final List<String> lore2 = new ArrayList<>();
        lore2.add(" ");
        lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(30000) + " + " + formatterEnergia.format(502.2) + "kW");
        lore2.add("§7Quantia: §a16");
        lore2.add(" ");
        meta2.setLore(lore2);
        tntH.setItemMeta(meta2);
        inventory.setItem(13, InventoryLoja.addGlow(tntH));

        final ItemStack tntMedia = new ItemStack(Material.TNT);
        final ItemMeta meta1 = tntMedia.getItemMeta();
        meta1.setDisplayName(ChatColor.RED + "Explosivo nuclear (3/5)");
        final List<String> lore1 = new ArrayList<>();
        lore1.add(" ");
        lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(20000) + " + " + formatterEnergia.format(251.1) + "kW");
        lore1.add("§7Quantia: §a16");
        lore1.add(" ");
        meta1.setLore(lore1);
        tntMedia.setItemMeta(meta1);
        inventory.setItem(12, tntMedia);

        final ItemStack tntComum = new ItemStack(Material.TNT);
        final ItemMeta meta5 = tntComum.getItemMeta();
        meta5.setDisplayName(ChatColor.RED + "Explosivo comum (1/5)");
        final List<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(8000) + " + " + formatterEnergia.format(125.55) + "kW");
        lore4.add("§7Quantia: §a16");
        lore4.add(" ");
        meta5.setLore(lore4);
        tntComum.setItemMeta(meta5);
        inventory.setItem(11, tntComum);

        final ItemStack end = new ItemStack(Material.END_STONE);
        final ItemMeta meta6 = end.getItemMeta();
        meta6.setDisplayName(ChatColor.GREEN + "Bloco de fortificação: 4/10");
        final List<String> lore112 = new ArrayList<>();
        if (LojaAPI.valorVenda("bloco_de_fortificao_1", type) != 0 && LojaAPI.valorCompra("bloco_de_fortificao_1", type) != 0) {
            lore112.add(" ");
            lore112.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bloco_de_fortificao_1", type)));
            lore112.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bloco_de_fortificao_1", type)));
            lore112.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_fortificao_1", type));
            lore112.add(" ");
            lore112.add(" ");
            lore112.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore112.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            meta11.setLore(lore112);
        } else if (LojaAPI.valorCompra("bloco_de_fortificao_1", type) != 0) {
            lore112.add(" ");
            lore112.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bloco_de_fortificao_1", type)));
            lore112.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_fortificao_1", type));
            lore112.add(" ");
            lore112.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore112.add(" ");
            meta11.setLore(lore112);
        } else if (LojaAPI.valorVenda("bloco_de_fortificao_1", type) != 0) {
            lore112.add(" ");
            lore112.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bloco_de_fortificao_1", type)));
            lore112.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_fortificao_1", type));
            lore112.add(" ");
            lore112.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore112.add(" ");
            meta11.setLore(lore112);
        }
        meta6.setLore(lore112);
        end.setItemMeta(meta6);
        inventory.setItem(15, end);

        final ItemStack bedrock = new ItemStack(Material.SMOOTH_STONE);
        final ItemMeta meta7 = bedrock.getItemMeta();
        meta7.setDisplayName(ChatColor.GREEN + "Bloco de fortificação: 10/10");
        final List<String> lore111 = new ArrayList<>();
        if (LojaAPI.valorVenda("bloco_de_fortificao_2", type) != 0 && LojaAPI.valorCompra("bloco_de_fortificao_2", type) != 0) {
            lore111.add(" ");
            lore111.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bloco_de_fortificao_2", type)));
            lore111.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bloco_de_fortificao_2", type)));
            lore111.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_fortificao_2", type));
            lore111.add(" ");
            lore111.add(" ");
            lore111.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore111.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            meta11.setLore(lore111);
        } else if (LojaAPI.valorCompra("bloco_de_fortificao_2", type) != 0) {
            lore111.add(" ");
            lore111.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bloco_de_fortificao_2", type)));
            lore111.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_fortificao_2", type));
            lore111.add(" ");
            lore111.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore111.add(" ");
            meta11.setLore(lore111);
        } else if (LojaAPI.valorVenda("bloco_de_fortificao_2", type) != 0) {
            lore111.add(" ");
            lore111.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bloco_de_fortificao_2", type)));
            lore111.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_fortificao_2", type));
            lore111.add(" ");
            lore111.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore111.add(" ");
            meta11.setLore(lore111);
        }
        meta7.setLore(lore111);
        bedrock.setItemMeta(meta7);
        inventory.setItem(16, bedrock);


        return inventory;
    }

    public static Inventory buildLojaPainelSolar(final String type) {
        final Inventory INVENTORY_PAINEIS = Bukkit.createInventory(null, 3 * 9, "Paineis solares: ");

        final ItemStack painel1 = new ItemStack(Material.DAYLIGHT_DETECTOR);
        final ItemMeta meta1 = painel1.getItemMeta();
        meta1.setDisplayName(ChatColor.GREEN + "Painel solar: " + ChatColor.YELLOW + "100Wp");
        final List<String> lore1 = new ArrayList<>();
        if (LojaAPI.valorVenda("painel_solar_100", type) != 0 && LojaAPI.valorCompra("painel_solar_100", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("painel_solar_100", type)));
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("painel_solar_100", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("painel_solar_100", type));
            lore1.add(" ");
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            meta1.setLore(lore1);
        } else if (LojaAPI.valorCompra("painel_solar_100", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("painel_solar_100", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("painel_solar_100", type));
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add(" ");
            meta1.setLore(lore1);
        } else if (LojaAPI.valorVenda("painel_solar_100", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("painel_solar_100", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("painel_solar_100", type));
            lore1.add(" ");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore1.add(" ");
            meta1.setLore(lore1);
        }
        meta1.setLore(lore1);
        painel1.setItemMeta(meta1);

        final ItemStack painel2 = new ItemStack(Material.DAYLIGHT_DETECTOR);
        final ItemMeta meta2 = painel2.getItemMeta();
        meta2.setDisplayName(ChatColor.GREEN + "Painel solar: " + ChatColor.YELLOW + "200Wp");
        final List<String> lore2 = new ArrayList<>();
        if (LojaAPI.valorVenda("painel_solar_200", type) != 0 && LojaAPI.valorCompra("painel_solar_200", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("painel_solar_200", type)));
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("painel_solar_200", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("painel_solar_200", type));
            lore2.add(" ");
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            meta2.setLore(lore2);
        } else if (LojaAPI.valorCompra("painel_solar_200", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("painel_solar_200", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("painel_solar_200", type));
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add(" ");
            meta2.setLore(lore2);
        } else if (LojaAPI.valorVenda("painel_solar_200", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("painel_solar_200", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("painel_solar_200", type));
            lore2.add(" ");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore2.add(" ");
            meta2.setLore(lore2);
        }
        meta2.setLore(lore2);
        painel2.setItemMeta(meta2);

        INVENTORY_PAINEIS.setItem(12, painel1);
        INVENTORY_PAINEIS.setItem(14, painel2);

        return INVENTORY_PAINEIS;
    }

    public static Inventory buildLojaCaixas(final String type) {
        final Inventory INVENTORY_CAIXAS = Bukkit.createInventory(null, 3 * 9, "Caixas misteriosas: ");

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave: " + ChatColor.YELLOW + "50% sucesso");
        final List<String> lore4 = new ArrayList<>();
        if (LojaAPI.valorVenda("chave", type) != 0 && LojaAPI.valorCompra("chave", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("chave", type)));
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("chave", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("chave", type));
            lore4.add(" ");
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaChave.setLore(lore4);
        } else if (LojaAPI.valorCompra("chave", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("chave", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("chave", type));
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add(" ");
            metaChave.setLore(lore4);
        } else if (LojaAPI.valorVenda("chave", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("chave", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("chave", type));
            lore4.add(" ");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore4.add(" ");
            metaChave.setLore(lore4);
        }
        metaChave.setLore(lore4);
        chave.setItemMeta(metaChave);

        final ItemStack caixaSpawn = new ItemStack(Material.SPAWNER);
        final ItemMeta metaCaixaSpawn = caixaSpawn.getItemMeta();
        metaCaixaSpawn.setDisplayName(ChatColor.GREEN + "Caixa misteriosa: " + ChatColor.YELLOW + "Spawners");
        final List<String> lore3 = new ArrayList<String>();
        if (LojaAPI.valorVenda("caixa_spawners", type) != 0 && LojaAPI.valorCompra("caixa_spawners", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("caixa_spawners", type)));
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("caixa_spawners", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("caixa_spawners", type));
            lore3.add(" ");
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaCaixaSpawn.setLore(lore3);
        } else if (LojaAPI.valorCompra("caixa_spawners", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("caixa_spawners", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("caixa_spawners", type));
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add(" ");
            metaCaixaSpawn.setLore(lore3);
        } else if (LojaAPI.valorVenda("caixa_spawners", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("caixa_spawners", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("caixa_spawners", type));
            lore3.add(" ");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore3.add(" ");
            metaCaixaSpawn.setLore(lore3);
        }
        metaCaixaSpawn.setLore(lore3);
        caixaSpawn.setItemMeta(metaCaixaSpawn);

        final ItemStack caixaArmas = new ItemStack(Material.ENDER_CHEST);
        final ItemMeta metaCaixaArmas = caixaArmas.getItemMeta();
        metaCaixaArmas.setDisplayName(ChatColor.GREEN + "Caixa misteriosa: " + ChatColor.YELLOW + "Armas");
        final List<String> lore2 = new ArrayList<String>();
        if (LojaAPI.valorVenda("caixa_armas", type) != 0 && LojaAPI.valorCompra("caixa_armas", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("caixa_armas", type)));
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("caixa_armas", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("caixa_armas", type));
            lore2.add(" ");
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaCaixaArmas.setLore(lore2);
        } else if (LojaAPI.valorCompra("caixa_armas", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("caixa_armas", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("caixa_armas", type));
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add(" ");
            metaCaixaArmas.setLore(lore2);
        } else if (LojaAPI.valorVenda("caixa_armas", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("caixa_armas", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("caixa_armas", type));
            lore2.add(" ");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore2.add(" ");
            metaCaixaArmas.setLore(lore2);
        }
        metaCaixaArmas.setLore(lore2);
        caixaArmas.setItemMeta(metaCaixaArmas);

        final ItemStack caixaAvan = new ItemStack(Material.ENDER_CHEST);
        final ItemMeta metaCaixaAvan = caixaAvan.getItemMeta();
        metaCaixaAvan.setDisplayName(ChatColor.GREEN + "Caixa misteriosa: " + ChatColor.YELLOW + "Avançada");
        final List<String> lore1 = new ArrayList<String>();
        if (LojaAPI.valorVenda("caixa_avancada", type) != 0 && LojaAPI.valorCompra("caixa_avancada", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("caixa_avancada", type)));
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("caixa_avancada", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("caixa_avancada", type));
            lore1.add(" ");
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaCaixaAvan.setLore(lore1);
        } else if (LojaAPI.valorCompra("caixa_avancada", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("caixa_avancada", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("caixa_avancada", type));
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add(" ");
            metaCaixaAvan.setLore(lore1);
        } else if (LojaAPI.valorVenda("caixa_avancada", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("caixa_avancada", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("caixa_avancada", type));
            lore1.add(" ");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore1.add(" ");
            metaCaixaAvan.setLore(lore1);
        }
        metaCaixaAvan.setLore(lore1);
        caixaAvan.setItemMeta(metaCaixaAvan);

        final ItemStack caixa = new ItemStack(Material.CHEST);
        final ItemMeta metaCaixa = caixa.getItemMeta();
        metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa: " + ChatColor.YELLOW + "Básica");
        final List<String> lore = new ArrayList<String>();
        if (LojaAPI.valorVenda("caixa_basica", type) != 0 && LojaAPI.valorCompra("caixa_basica", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("caixa_basica", type)));
            lore.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("caixa_basica", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("caixa_basica", type));
            lore.add(" ");
            lore.add(" ");
            lore.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaCaixa.setLore(lore);
        } else if (LojaAPI.valorCompra("caixa_basica", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("caixa_basica", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("caixa_basica", type));
            lore.add(" ");
            lore.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore.add(" ");
            metaCaixa.setLore(lore);
        } else if (LojaAPI.valorVenda("caixa_basica", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("caixa_basica", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("caixa_basica", type));
            lore.add(" ");
            lore.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore.add(" ");
            metaCaixa.setLore(lore);
        }
        metaCaixa.setLore(lore);
        caixa.setItemMeta(metaCaixa);

        INVENTORY_CAIXAS.setItem(11, caixa);
        INVENTORY_CAIXAS.setItem(12, caixaAvan);
        INVENTORY_CAIXAS.setItem(13, caixaArmas);
        INVENTORY_CAIXAS.setItem(14, caixaSpawn);
        INVENTORY_CAIXAS.setItem(15, chave);

        return INVENTORY_CAIXAS;
    }

    public static Inventory buildLojaSpawners(final String type) {
        final Inventory INVENTORY_MOBSPAWNER = Bukkit.createInventory(null, 5 * 9, "Mobspawners: ");

        final ItemStack item17 = new ItemStack(Material.SPAWNER);
        final ItemMeta metaItem17 = item17.getItemMeta();
        metaItem17.setDisplayName(ChatColor.RED + "Spawner de Ghast");
        final ArrayList<String> lore17 = new ArrayList<String>();
        if (LojaAPI.valorVenda("spawner_de_ghast", type) != 0 && LojaAPI.valorCompra("spawner_de_ghast", type) != 0) {
            lore17.add(" ");
            lore17.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_ghast", type)));
            lore17.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_ghast", type)));
            lore17.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_ghast", type));
            lore17.add(" ");
            lore17.add(" ");
            lore17.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore17.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaItem17.setLore(lore17);
        } else if (LojaAPI.valorCompra("spawner_de_ghast", type) != 0) {
            lore17.add(" ");
            lore17.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_ghast", type)));
            lore17.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_ghast", type));
            lore17.add(" ");
            lore17.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore17.add(" ");
            metaItem17.setLore(lore17);
        } else if (LojaAPI.valorVenda("spawner_de_ghast", type) != 0) {
            lore17.add(" ");
            lore17.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_ghast", type)));
            lore17.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_ghast", type));
            lore17.add(" ");
            lore17.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore17.add(" ");
            metaItem17.setLore(lore17);
        }
        item17.setItemMeta(metaItem17);

        final ItemStack item16 = new ItemStack(Material.SPAWNER);
        final ItemMeta metaItem16 = item16.getItemMeta();
        metaItem16.setDisplayName(ChatColor.RED + "Spawner de Aldeão zumbi");
        final ArrayList<String> lore16 = new ArrayList<String>();
        if (LojaAPI.valorVenda("spawner_de_aldeao_zumbi", type) != 0 && LojaAPI.valorCompra("spawner_de_aldeao_zumbi", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_aldeao_zumbi", type)));
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_aldeao_zumbi", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_aldeao_zumbi", type));
            lore16.add(" ");
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaItem16.setLore(lore16);
        } else if (LojaAPI.valorCompra("spawner_de_aldeao_zumbi", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_aldeao_zumbi", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_aldeao_zumbi", type));
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add(" ");
            metaItem16.setLore(lore16);
        } else if (LojaAPI.valorVenda("spawner_de_aldeao_zumbi", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_aldeao_zumbi", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_aldeao_zumbi", type));
            lore16.add(" ");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore16.add(" ");
            metaItem16.setLore(lore16);
        }
        item16.setItemMeta(metaItem16);

        final ItemStack item15 = new ItemStack(Material.SPAWNER);
        final ItemMeta metaItem15 = item15.getItemMeta();
        metaItem15.setDisplayName(ChatColor.RED + "Spawner de Cubo magma");
        final ArrayList<String> lore15 = new ArrayList<String>();
        if (LojaAPI.valorVenda("spawner_de_cubo_magma", type) != 0 && LojaAPI.valorCompra("spawner_de_cubo_magma", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_cubo_magma", type)));
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_cubo_magma", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_cubo_magma", type));
            lore15.add(" ");
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaItem15.setLore(lore15);
        } else if (LojaAPI.valorCompra("spawner_de_cubo_magma", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_cubo_magma", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_cubo_magma", type));
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add(" ");
            metaItem15.setLore(lore15);
        } else if (LojaAPI.valorVenda("spawner_de_cubo_magma", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_cubo_magma", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_cubo_magma", type));
            lore15.add(" ");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore15.add(" ");
            metaItem15.setLore(lore15);
        }
        item15.setItemMeta(metaItem15);

        final ItemStack item14 = new ItemStack(Material.SPAWNER);
        final ItemMeta metaItem14 = item14.getItemMeta();
        metaItem14.setDisplayName(ChatColor.RED + "Spawner de Bruxa");
        final ArrayList<String> lore14 = new ArrayList<String>();
        if (LojaAPI.valorVenda("spawner_de_bruxa", type) != 0 && LojaAPI.valorCompra("spawner_de_bruxa", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_bruxa", type)));
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_bruxa", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_bruxa", type));
            lore14.add(" ");
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaItem14.setLore(lore14);
        } else if (LojaAPI.valorCompra("spawner_de_bruxa", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_bruxa", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_bruxa", type));
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add(" ");
            metaItem14.setLore(lore14);
        } else if (LojaAPI.valorVenda("spawner_de_bruxa", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_bruxa", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_bruxa", type));
            lore14.add(" ");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore14.add(" ");
            metaItem14.setLore(lore14);
        }
        item14.setItemMeta(metaItem14);

        final ItemStack item18 = new ItemStack(Material.SPAWNER);
        final ItemMeta metaItem18 = item18.getItemMeta();
        metaItem18.setDisplayName(ChatColor.RED + "Spawner de wither");
        final ArrayList<String> lore18 = new ArrayList<String>();
        if (LojaAPI.valorVenda("spawner_de_wither", type) != 0 && LojaAPI.valorCompra("spawner_de_wither", type) != 0) {
            lore18.add(" ");
            lore18.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_wither", type)));
            lore18.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_wither", type)));
            lore18.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_wither", type));
            lore18.add(" ");
            lore18.add(" ");
            lore18.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore18.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaItem18.setLore(lore18);
        } else if (LojaAPI.valorCompra("spawner_de_wither", type) != 0) {
            lore18.add(" ");
            lore18.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_wither", type)));
            lore18.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_wither", type));
            lore18.add(" ");
            lore18.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore18.add(" ");
            metaItem18.setLore(lore18);
        } else if (LojaAPI.valorVenda("spawner_de_wither", type) != 0) {
            lore18.add(" ");
            lore18.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_wither", type)));
            lore18.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_wither", type));
            lore18.add(" ");
            lore18.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore18.add(" ");
            metaItem18.setLore(lore18);
        }
        item18.setItemMeta(metaItem18);

        final ItemStack item13 = new ItemStack(Material.SPAWNER);
        final ItemMeta metaItem13 = item13.getItemMeta();
        metaItem13.setDisplayName(ChatColor.RED + "Spawner de Homem porco zumbi");
        final ArrayList<String> lore13 = new ArrayList<String>();
        if (LojaAPI.valorVenda("spawner_de_homem_porco_zumbi", type) != 0 && LojaAPI.valorCompra("spawner_de_homem_porco_zumbi", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_homem_porco_zumbi", type)));
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_homem_porco_zumbi", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_homem_porco_zumbi", type));
            lore13.add(" ");
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaItem13.setLore(lore13);
        } else if (LojaAPI.valorCompra("spawner_de_homem_porco_zumbi", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_homem_porco_zumbi", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_homem_porco_zumbi", type));
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add(" ");
            metaItem13.setLore(lore13);
        } else if (LojaAPI.valorVenda("spawner_de_homem_porco_zumbi", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_homem_porco_zumbi", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_homem_porco_zumbi", type));
            lore13.add(" ");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore13.add(" ");
            metaItem13.setLore(lore13);
        }
        item13.setItemMeta(metaItem13);

        final ItemStack item12 = new ItemStack(Material.SPAWNER);
        final ItemMeta metaItem12 = item12.getItemMeta();
        metaItem12.setDisplayName(ChatColor.RED + "Spawner de Blaze");
        final ArrayList<String> lore12 = new ArrayList<String>();
        if (LojaAPI.valorVenda("spawner_de_blaze", type) != 0 && LojaAPI.valorCompra("spawner_de_blaze", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_blaze", type)));
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_blaze", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_blaze", type));
            lore12.add(" ");
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaItem12.setLore(lore12);
        } else if (LojaAPI.valorCompra("spawner_de_blaze", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_blaze", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_blaze", type));
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add(" ");
            metaItem12.setLore(lore12);
        } else if (LojaAPI.valorVenda("spawner_de_blaze", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_blaze", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_blaze", type));
            lore12.add(" ");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore12.add(" ");
            metaItem12.setLore(lore12);
        }
        item12.setItemMeta(metaItem12);

        final ItemStack item11 = new ItemStack(Material.SPAWNER);
        final ItemMeta metaItem11 = item11.getItemMeta();
        metaItem11.setDisplayName(ChatColor.RED + "Spawner de Slime");
        final ArrayList<String> lore11 = new ArrayList<String>();
        if (LojaAPI.valorVenda("spawner_de_slime", type) != 0 && LojaAPI.valorCompra("spawner_de_slime", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_slime", type)));
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_slime", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_slime", type));
            lore11.add(" ");
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaItem11.setLore(lore11);
        } else if (LojaAPI.valorCompra("spawner_de_slime", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_slime", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_slime", type));
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add(" ");
            metaItem11.setLore(lore11);
        } else if (LojaAPI.valorVenda("spawner_de_slime", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_slime", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_slime", type));
            lore11.add(" ");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore11.add(" ");
            metaItem11.setLore(lore11);
        }
        item11.setItemMeta(metaItem11);

        final ItemStack item10 = new ItemStack(Material.SPAWNER);
        final ItemMeta metaItem10 = item10.getItemMeta();
        metaItem10.setDisplayName(ChatColor.RED + "Spawner de Aranha das cavernas");
        final ArrayList<String> lore10 = new ArrayList<String>();
        if (LojaAPI.valorVenda("spawner_de_aranha_das_cavernas", type) != 0 && LojaAPI.valorCompra("spawner_de_aranha_das_cavernas", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_aranha_das_cavernas", type)));
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_aranha_das_cavernas", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_aranha_das_cavernas", type));
            lore10.add(" ");
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaItem10.setLore(lore10);
        } else if (LojaAPI.valorCompra("spawner_de_aranha_das_cavernas", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_aranha_das_cavernas", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_aranha_das_cavernas", type));
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add(" ");
            metaItem10.setLore(lore10);
        } else if (LojaAPI.valorVenda("spawner_de_aranha_das_cavernas", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_aranha_das_cavernas", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_aranha_das_cavernas", type));
            lore10.add(" ");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore10.add(" ");
            metaItem10.setLore(lore10);
        }
        item10.setItemMeta(metaItem10);

        final ItemStack item9 = new ItemStack(Material.SPAWNER);
        final ItemMeta metaItem9 = item9.getItemMeta();
        metaItem9.setDisplayName(ChatColor.RED + "Spawner de Enderman");
        final ArrayList<String> lore9 = new ArrayList<String>();
        if (LojaAPI.valorVenda("spawner_de_enderman", type) != 0 && LojaAPI.valorCompra("spawner_de_enderman", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_enderman", type)));
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_enderman", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_enderman", type));
            lore9.add(" ");
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaItem9.setLore(lore9);
        } else if (LojaAPI.valorCompra("spawner_de_enderman", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_enderman", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_enderman", type));
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add(" ");
            metaItem9.setLore(lore9);
        } else if (LojaAPI.valorVenda("spawner_de_enderman", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_enderman", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_enderman", type));
            lore9.add(" ");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore9.add(" ");
            metaItem9.setLore(lore9);
        }
        item9.setItemMeta(metaItem9);

        final ItemStack item8 = new ItemStack(Material.SPAWNER);
        final ItemMeta metaItem8 = item8.getItemMeta();
        metaItem8.setDisplayName(ChatColor.RED + "Spawner de Creeper");
        final ArrayList<String> lore8 = new ArrayList<String>();
        if (LojaAPI.valorVenda("spawner_de_creeper", type) != 0 && LojaAPI.valorCompra("spawner_de_creeper", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_creeper", type)));
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_creeper", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_creeper", type));
            lore8.add(" ");
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaItem8.setLore(lore8);
        } else if (LojaAPI.valorCompra("spawner_de_creeper", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_creeper", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_creeper", type));
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add(" ");
            metaItem8.setLore(lore8);
        } else if (LojaAPI.valorVenda("spawner_de_creeper", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_creeper", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_creeper", type));
            lore8.add(" ");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore8.add(" ");
            metaItem8.setLore(lore8);
        }
        item8.setItemMeta(metaItem8);

        final ItemStack item7 = new ItemStack(Material.SPAWNER);
        final ItemMeta metaItem7 = item7.getItemMeta();
        metaItem7.setDisplayName(ChatColor.RED + "Spawner de Aranha");
        final ArrayList<String> lore7 = new ArrayList<String>();
        if (LojaAPI.valorVenda("spawner_de_aranha", type) != 0 && LojaAPI.valorCompra("spawner_de_aranha", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_aranha", type)));
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_aranha", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_aranha", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaItem7.setLore(lore7);
        } else if (LojaAPI.valorCompra("spawner_de_aranha", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_aranha", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_aranha", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add(" ");
            metaItem7.setLore(lore7);
        } else if (LojaAPI.valorVenda("spawner_de_aranha", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_aranha", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_aranha", type));
            lore7.add(" ");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore7.add(" ");
            metaItem7.setLore(lore7);
        }
        item7.setItemMeta(metaItem7);

        final ItemStack item6 = new ItemStack(Material.SPAWNER);
        final ItemMeta metaItem6 = item6.getItemMeta();
        metaItem6.setDisplayName(ChatColor.RED + "Spawner de Esqueleto");
        final ArrayList<String> lore6 = new ArrayList<String>();
        if (LojaAPI.valorVenda("spawner_de_esqueleto", type) != 0 && LojaAPI.valorCompra("spawner_de_esqueleto", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_esqueleto", type)));
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_esqueleto", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_esqueleto", type));
            lore6.add(" ");
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaItem6.setLore(lore6);
        } else if (LojaAPI.valorCompra("spawner_de_esqueleto", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_esqueleto", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_esqueleto", type));
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add(" ");
            metaItem6.setLore(lore6);
        } else if (LojaAPI.valorVenda("spawner_de_esqueleto", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_esqueleto", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_esqueleto", type));
            lore6.add(" ");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore6.add(" ");
            metaItem6.setLore(lore6);
        }
        item6.setItemMeta(metaItem6);

        final ItemStack item5 = new ItemStack(Material.SPAWNER);
        final ItemMeta metaItem5 = item5.getItemMeta();
        metaItem5.setDisplayName(ChatColor.RED + "Spawner de Zumbi");
        final ArrayList<String> lore5 = new ArrayList<String>();
        if (LojaAPI.valorVenda("spawner_de_zumbi", type) != 0 && LojaAPI.valorCompra("spawner_de_zumbi", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_zumbi", type)));
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_zumbi", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_zumbi", type));
            lore5.add(" ");
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaItem5.setLore(lore5);
        } else if (LojaAPI.valorCompra("spawner_de_zumbi", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_zumbi", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_zumbi", type));
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add(" ");
            metaItem5.setLore(lore5);
        } else if (LojaAPI.valorVenda("spawner_de_zumbi", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_zumbi", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_zumbi", type));
            lore5.add(" ");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore5.add(" ");
            metaItem5.setLore(lore5);
        }
        item5.setItemMeta(metaItem5);

        final ItemStack item4 = new ItemStack(Material.SPAWNER);
        final ItemMeta metaItem4 = item4.getItemMeta();
        metaItem4.setDisplayName(ChatColor.RED + "Spawner de Coelho");
        final ArrayList<String> lore4 = new ArrayList<String>();
        if (LojaAPI.valorVenda("spawner_de_coelho", type) != 0 && LojaAPI.valorCompra("spawner_de_coelho", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_coelho", type)));
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_coelho", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_coelho", type));
            lore4.add(" ");
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaItem4.setLore(lore4);
        } else if (LojaAPI.valorCompra("spawner_de_coelho", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_coelho", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_coelho", type));
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add(" ");
            metaItem4.setLore(lore4);
        } else if (LojaAPI.valorVenda("spawner_de_coelho", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_coelho", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_coelho", type));
            lore4.add(" ");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore4.add(" ");
            metaItem4.setLore(lore4);
        }
        item4.setItemMeta(metaItem4);

        final ItemStack item3 = new ItemStack(Material.SPAWNER);
        final ItemMeta metaItem3 = item3.getItemMeta();
        metaItem3.setDisplayName(ChatColor.RED + "Spawner de Ovelha");
        final ArrayList<String> lore3 = new ArrayList<String>();
        if (LojaAPI.valorVenda("spawner_de_ovelha", type) != 0 && LojaAPI.valorCompra("spawner_de_ovelha", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_ovelha", type)));
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_ovelha", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_ovelha", type));
            lore3.add(" ");
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaItem3.setLore(lore3);
        } else if (LojaAPI.valorCompra("spawner_de_ovelha", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_ovelha", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_ovelha", type));
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add(" ");
            metaItem3.setLore(lore3);
        } else if (LojaAPI.valorVenda("spawner_de_ovelha", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_ovelha", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_ovelha", type));
            lore3.add(" ");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore3.add(" ");
            metaItem3.setLore(lore3);
        }
        item3.setItemMeta(metaItem3);

        final ItemStack item2 = new ItemStack(Material.SPAWNER);
        final ItemMeta metaItem2 = item2.getItemMeta();
        metaItem2.setDisplayName(ChatColor.RED + "Spawner de Porco");
        final ArrayList<String> lore2 = new ArrayList<String>();
        if (LojaAPI.valorVenda("spawner_de_porco", type) != 0 && LojaAPI.valorCompra("spawner_de_porco", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_porco", type)));
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_porco", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_porco", type));
            lore2.add(" ");
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaItem2.setLore(lore2);
        } else if (LojaAPI.valorCompra("spawner_de_porco", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_porco", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_porco", type));
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add(" ");
            metaItem2.setLore(lore2);
        } else if (LojaAPI.valorVenda("spawner_de_porco", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_porco", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_porco", type));
            lore2.add(" ");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore2.add(" ");
            metaItem2.setLore(lore2);
        }
        item2.setItemMeta(metaItem2);

        final ItemStack item1 = new ItemStack(Material.SPAWNER);
        final ItemMeta metaItem1 = item1.getItemMeta();
        metaItem1.setDisplayName(ChatColor.RED + "Spawner de Galinha");
        final ArrayList<String> lore1 = new ArrayList<String>();
        if (LojaAPI.valorVenda("spawner_de_galinha", type) != 0 && LojaAPI.valorCompra("spawner_de_galinha", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_galinha", type)));
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_galinha", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_galinha", type));
            lore1.add(" ");
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaItem1.setLore(lore1);
        } else if (LojaAPI.valorCompra("spawner_de_galinha", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_galinha", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_galinha", type));
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add(" ");
            metaItem1.setLore(lore1);
        } else if (LojaAPI.valorVenda("spawner_de_galinha", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_galinha", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_galinha", type));
            lore1.add(" ");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore1.add(" ");
            metaItem1.setLore(lore1);
        }
        item1.setItemMeta(metaItem1);

        final ItemStack item = new ItemStack(Material.SPAWNER);
        final ItemMeta metaItem = item.getItemMeta();
        metaItem.setDisplayName(ChatColor.RED + "Spawner de Vaca");
        final ArrayList<String> lore = new ArrayList<String>();
        if (LojaAPI.valorVenda("spawner_de_vaca", type) != 0 && LojaAPI.valorCompra("spawner_de_vaca", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_vaca", type)));
            lore.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_vaca", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_vaca", type));
            lore.add(" ");
            lore.add(" ");
            lore.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaItem.setLore(lore);
        } else if (LojaAPI.valorCompra("spawner_de_vaca", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("spawner_de_vaca", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_vaca", type));
            lore.add(" ");
            lore.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore.add(" ");
            metaItem.setLore(lore);
        } else if (LojaAPI.valorVenda("spawner_de_vaca", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("spawner_de_vaca", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("spawner_de_vaca", type));
            lore.add(" ");
            lore.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore.add(" ");
            metaItem.setLore(lore);
        }
        item.setItemMeta(metaItem);

        INVENTORY_MOBSPAWNER.setItem(10, item);
        INVENTORY_MOBSPAWNER.setItem(11, item1);
        INVENTORY_MOBSPAWNER.setItem(12, item2);
        INVENTORY_MOBSPAWNER.setItem(13, item3);
        INVENTORY_MOBSPAWNER.setItem(14, item4);
        INVENTORY_MOBSPAWNER.setItem(15, item5);
        INVENTORY_MOBSPAWNER.setItem(16, item6);
        INVENTORY_MOBSPAWNER.setItem(19, item7);
        INVENTORY_MOBSPAWNER.setItem(20, item8);
        INVENTORY_MOBSPAWNER.setItem(21, item9);
        INVENTORY_MOBSPAWNER.setItem(22, item10);
        INVENTORY_MOBSPAWNER.setItem(23, item11);
        INVENTORY_MOBSPAWNER.setItem(24, item12);
        INVENTORY_MOBSPAWNER.setItem(25, item13);
        INVENTORY_MOBSPAWNER.setItem(29, item18);
        INVENTORY_MOBSPAWNER.setItem(30, item14);
        INVENTORY_MOBSPAWNER.setItem(31, item15);
        INVENTORY_MOBSPAWNER.setItem(32, item16);
        INVENTORY_MOBSPAWNER.setItem(33, item17);

        return INVENTORY_MOBSPAWNER;
    }

    public static Inventory buildLojaConcretos(final String type) {
        final Inventory INVENTORY_LOJACOLORIDOSCONCRETOS = Bukkit.createInventory(null, 5 * 9, "Concretos: ");

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta voltarMeta = voltar.getItemMeta();
        voltarMeta.setDisplayName(ChatColor.GREEN + "Voltar página.");
        voltar.setItemMeta(voltarMeta);
        INVENTORY_LOJACOLORIDOSCONCRETOS.setItem(36, voltar);


        final ItemStack preto = new ItemStack(Material.BLACK_CONCRETE_POWDER);
        final ItemMeta pretoMeta = preto.getItemMeta();
        pretoMeta.setDisplayName("§ePó de concreto preto");
        final ArrayList<String> lore16 = new ArrayList<String>();
        if (LojaAPI.valorVenda("po_de_concreto_preto", type) != 0 && LojaAPI.valorCompra("po_de_concreto_preto", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_preto", type)));
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_preto", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_preto", type));
            lore16.add(" ");
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            pretoMeta.setLore(lore16);
        } else if (LojaAPI.valorCompra("po_de_concreto_preto", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_preto", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_preto", type));
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add(" ");
            pretoMeta.setLore(lore16);
        } else if (LojaAPI.valorVenda("po_de_concreto_preto", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_preto", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_preto", type));
            lore16.add(" ");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore16.add(" ");
            pretoMeta.setLore(lore16);
        }
        preto.setItemMeta(pretoMeta);
        INVENTORY_LOJACOLORIDOSCONCRETOS.setItem(29, preto);


        final ItemStack vermelho = new ItemStack(Material.RED_CONCRETE_POWDER);
        final ItemMeta vermelhoMeta = vermelho.getItemMeta();
        vermelhoMeta.setDisplayName("§ePó de concreto vermelho");
        final ArrayList<String> lore15 = new ArrayList<String>();
        if (LojaAPI.valorVenda("po_de_concreto_vermelho", type) != 0 && LojaAPI.valorCompra("po_de_concreto_vermelho", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_vermelho", type)));
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_vermelho", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_vermelho", type));
            lore15.add(" ");
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            vermelhoMeta.setLore(lore15);
        } else if (LojaAPI.valorCompra("po_de_concreto_vermelho", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_vermelho", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_vermelho", type));
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add(" ");
            vermelhoMeta.setLore(lore15);
        } else if (LojaAPI.valorVenda("po_de_concreto_vermelho", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_vermelho", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_vermelho", type));
            lore15.add(" ");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore15.add(" ");
            vermelhoMeta.setLore(lore15);
        }
        vermelho.setItemMeta(vermelhoMeta);
        INVENTORY_LOJACOLORIDOSCONCRETOS.setItem(28, vermelho);


        final ItemStack verdescuro = new ItemStack(Material.GREEN_CONCRETE_POWDER);
        final ItemMeta verdescuroMeta = verdescuro.getItemMeta();
        verdescuroMeta.setDisplayName("§ePó de concreto verde");
        final ArrayList<String> lore14 = new ArrayList<String>();
        if (LojaAPI.valorVenda("po_de_concreto_verde", type) != 0 && LojaAPI.valorCompra("po_de_concreto_verde", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_verde", type)));
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_verde", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_verde", type));
            lore14.add(" ");
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            verdescuroMeta.setLore(lore14);
        } else if (LojaAPI.valorCompra("po_de_concreto_verde", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_verde", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_verde", type));
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add(" ");
            verdescuroMeta.setLore(lore14);
        } else if (LojaAPI.valorVenda("po_de_concreto_verde", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_verde", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_verde", type));
            lore14.add(" ");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore14.add(" ");
            verdescuroMeta.setLore(lore14);
        }
        verdescuro.setItemMeta(verdescuroMeta);
        INVENTORY_LOJACOLORIDOSCONCRETOS.setItem(25, verdescuro);


        final ItemStack marrom = new ItemStack(Material.BROWN_CONCRETE_POWDER);
        final ItemMeta marromMeta = marrom.getItemMeta();
        marromMeta.setDisplayName("§ePó de concreto marrom");
        final ArrayList<String> lore13 = new ArrayList<String>();
        if (LojaAPI.valorVenda("po_de_concreto_marrom", type) != 0 && LojaAPI.valorCompra("po_de_concreto_marrom", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_marrom", type)));
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_marrom", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_marrom", type));
            lore13.add(" ");
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            marromMeta.setLore(lore13);
        } else if (LojaAPI.valorCompra("po_de_concreto_marrom", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_marrom", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_marrom", type));
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add(" ");
            marromMeta.setLore(lore13);
        } else if (LojaAPI.valorVenda("po_de_concreto_marrom", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_marrom", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_marrom", type));
            lore13.add(" ");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore13.add(" ");
            marromMeta.setLore(lore13);
        }
        marrom.setItemMeta(marromMeta);
        INVENTORY_LOJACOLORIDOSCONCRETOS.setItem(24, marrom);


        final ItemStack azul = new ItemStack(Material.BLUE_CONCRETE_POWDER);
        final ItemMeta azulMeta = azul.getItemMeta();
        azulMeta.setDisplayName("§ePó de concreto azul");
        final ArrayList<String> lore12 = new ArrayList<String>();
        if (LojaAPI.valorVenda("po_de_concreto_azul", type) != 0 && LojaAPI.valorCompra("po_de_concreto_azul", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_azul", type)));
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_azul", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_azul", type));
            lore12.add(" ");
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            azulMeta.setLore(lore12);
        } else if (LojaAPI.valorCompra("po_de_concreto_azul", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_azul", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_azul", type));
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add(" ");
            azulMeta.setLore(lore12);
        } else if (LojaAPI.valorVenda("po_de_concreto_azul", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_azul", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_azul", type));
            lore12.add(" ");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore12.add(" ");
            azulMeta.setLore(lore12);
        }
        azul.setItemMeta(azulMeta);
        INVENTORY_LOJACOLORIDOSCONCRETOS.setItem(23, azul);


        final ItemStack lilas = new ItemStack(Material.PURPLE_CONCRETE_POWDER);
        final ItemMeta lilasMeta = lilas.getItemMeta();
        lilasMeta.setDisplayName("§ePó de concreto lílas");
        final ArrayList<String> lore11 = new ArrayList<String>();
        if (LojaAPI.valorVenda("po_de_concreto_lilas", type) != 0 && LojaAPI.valorCompra("po_de_concreto_lilas", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_lilas", type)));
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_lilas", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_lilas", type));
            lore11.add(" ");
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lilasMeta.setLore(lore11);
        } else if (LojaAPI.valorCompra("po_de_concreto_lilas", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_lilas", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_lilas", type));
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add(" ");
            lilasMeta.setLore(lore11);
        } else if (LojaAPI.valorVenda("po_de_concreto_lilas", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_lilas", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_lilas", type));
            lore11.add(" ");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore11.add(" ");
            lilasMeta.setLore(lore11);
        }
        lilas.setItemMeta(lilasMeta);
        INVENTORY_LOJACOLORIDOSCONCRETOS.setItem(22, lilas);


        final ItemStack ciano = new ItemStack(Material.CYAN_CONCRETE_POWDER);
        final ItemMeta cianoMeta = ciano.getItemMeta();
        cianoMeta.setDisplayName("§ePó de concreto ciano");
        final ArrayList<String> lore10 = new ArrayList<String>();
        if (LojaAPI.valorVenda("po_de_concreto_ciano", type) != 0 && LojaAPI.valorCompra("po_de_concreto_ciano", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_ciano", type)));
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_ciano", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_ciano", type));
            lore10.add(" ");
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            cianoMeta.setLore(lore10);
        } else if (LojaAPI.valorCompra("po_de_concreto_ciano", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_ciano", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_ciano", type));
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add(" ");
            cianoMeta.setLore(lore10);
        } else if (LojaAPI.valorVenda("po_de_concreto_ciano", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_ciano", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_ciano", type));
            lore10.add(" ");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore10.add(" ");
            cianoMeta.setLore(lore10);
        }
        ciano.setItemMeta(cianoMeta);
        INVENTORY_LOJACOLORIDOSCONCRETOS.setItem(21, ciano);


        final ItemStack cinzaclaro = new ItemStack(Material.LIGHT_GRAY_CONCRETE_POWDER);
        final ItemMeta cinzaclaroMeta = cinzaclaro.getItemMeta();
        cinzaclaroMeta.setDisplayName("§ePó de concreto cinza-claro");
        final ArrayList<String> lore9 = new ArrayList<String>();
        if (LojaAPI.valorVenda("po_de_concreto_cinza_claro", type) != 0 && LojaAPI.valorCompra("po_de_concreto_cinza_claro", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_cinza_claro", type)));
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_cinza_claro", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_cinza_claro", type));
            lore9.add(" ");
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            cinzaclaroMeta.setLore(lore9);
        } else if (LojaAPI.valorCompra("po_de_concreto_cinza_claro", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_cinza_claro", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_cinza_claro", type));
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add(" ");
            cinzaclaroMeta.setLore(lore9);
        } else if (LojaAPI.valorVenda("po_de_concreto_cinza_claro", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_cinza_claro", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_cinza_claro", type));
            lore9.add(" ");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore9.add(" ");
            cinzaclaroMeta.setLore(lore9);
        }
        cinzaclaro.setItemMeta(cinzaclaroMeta);
        INVENTORY_LOJACOLORIDOSCONCRETOS.setItem(20, cinzaclaro);


        final ItemStack cinza = new ItemStack(Material.GRAY_CONCRETE_POWDER);
        final ItemMeta cinzaMeta = cinza.getItemMeta();
        cinzaMeta.setDisplayName("§ePó de concreto cinza");
        final ArrayList<String> lore8 = new ArrayList<String>();
        if (LojaAPI.valorVenda("po_de_concreto_cinza", type) != 0 && LojaAPI.valorCompra("po_de_concreto_cinza", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_cinza", type)));
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_cinza", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_cinza", type));
            lore8.add(" ");
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            cinzaMeta.setLore(lore8);
        } else if (LojaAPI.valorCompra("po_de_concreto_cinza", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_cinza", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_cinza", type));
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add(" ");
            cinzaMeta.setLore(lore8);
        } else if (LojaAPI.valorVenda("po_de_concreto_cinza", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_cinza", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_cinza", type));
            lore8.add(" ");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore8.add(" ");
            cinzaMeta.setLore(lore8);
        }
        cinza.setItemMeta(cinzaMeta);
        INVENTORY_LOJACOLORIDOSCONCRETOS.setItem(19, cinza);


        final ItemStack rosa = new ItemStack(Material.PINK_CONCRETE_POWDER);
        final ItemMeta rosaMeta = rosa.getItemMeta();
        rosaMeta.setDisplayName("§ePó de concreto rosa");
        final ArrayList<String> lore7 = new ArrayList<String>();
        if (LojaAPI.valorVenda("po_de_concreto_rosa", type) != 0 && LojaAPI.valorCompra("po_de_concreto_rosa", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_rosa", type)));
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_rosa", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_rosa", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            rosaMeta.setLore(lore7);
        } else if (LojaAPI.valorCompra("po_de_concreto_rosa", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_rosa", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_rosa", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add(" ");
            rosaMeta.setLore(lore7);
        } else if (LojaAPI.valorVenda("po_de_concreto_rosa", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_rosa", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_rosa", type));
            lore7.add(" ");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore7.add(" ");
            rosaMeta.setLore(lore7);
        }
        rosa.setItemMeta(rosaMeta);
        INVENTORY_LOJACOLORIDOSCONCRETOS.setItem(16, rosa);


        final ItemStack verdeclaro = new ItemStack(Material.LIME_CONCRETE_POWDER);
        final ItemMeta verdeclaroMeta = verdeclaro.getItemMeta();
        verdeclaroMeta.setDisplayName("§ePó de concreto verde-limão");
        final ArrayList<String> lore6 = new ArrayList<String>();
        if (LojaAPI.valorVenda("po_de_concreto_verde_limao", type) != 0 && LojaAPI.valorCompra("po_de_concreto_verde_limao", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_verde_limao", type)));
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_verde_limao", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_verde_limao", type));
            lore6.add(" ");
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            verdeclaroMeta.setLore(lore6);
        } else if (LojaAPI.valorCompra("po_de_concreto_verde_limao", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_verde_limao", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_verde_limao", type));
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add(" ");
            verdeclaroMeta.setLore(lore6);
        } else if (LojaAPI.valorVenda("po_de_concreto_verde_limao", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_verde_limao", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_verde_limao", type));
            lore6.add(" ");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore6.add(" ");
            verdeclaroMeta.setLore(lore6);
        }
        verdeclaro.setItemMeta(verdeclaroMeta);
        INVENTORY_LOJACOLORIDOSCONCRETOS.setItem(15, verdeclaro);


        final ItemStack amarelo = new ItemStack(Material.YELLOW_CONCRETE_POWDER);
        final ItemMeta amareloMeta = amarelo.getItemMeta();
        amareloMeta.setDisplayName("§ePó de concreto amarelo");
        final ArrayList<String> lore5 = new ArrayList<String>();
        if (LojaAPI.valorVenda("po_de_concreto_amarelo", type) != 0 && LojaAPI.valorCompra("po_de_concreto_amarelo", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_amarelo", type)));
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_amarelo", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_amarelo", type));
            lore5.add(" ");
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            amareloMeta.setLore(lore5);
        } else if (LojaAPI.valorCompra("po_de_concreto_amarelo", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_amarelo", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_amarelo", type));
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add(" ");
            amareloMeta.setLore(lore5);
        } else if (LojaAPI.valorVenda("po_de_concreto_amarelo", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_amarelo", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_amarelo", type));
            lore5.add(" ");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore5.add(" ");
            amareloMeta.setLore(lore5);
        }
        amarelo.setItemMeta(amareloMeta);
        INVENTORY_LOJACOLORIDOSCONCRETOS.setItem(14, amarelo);


        final ItemStack azulclaro = new ItemStack(Material.LIGHT_BLUE_CONCRETE_POWDER);
        final ItemMeta azulclaroMeta = azulclaro.getItemMeta();
        azulclaroMeta.setDisplayName("§ePó de concreto azul-claro");
        final ArrayList<String> lore4 = new ArrayList<String>();
        if (LojaAPI.valorVenda("po_de_concreto_azul_claro", type) != 0 && LojaAPI.valorCompra("po_de_concreto_azul_claro", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_azul_claro", type)));
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_azul_claro", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_azul_claro", type));
            lore4.add(" ");
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            azulclaroMeta.setLore(lore4);
        } else if (LojaAPI.valorCompra("po_de_concreto_azul_claro", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_azul_claro", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_azul_claro", type));
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add(" ");
            azulclaroMeta.setLore(lore4);
        } else if (LojaAPI.valorVenda("po_de_concreto_azul_claro", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_azul_claro", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_azul_claro", type));
            lore4.add(" ");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore4.add(" ");
            azulclaroMeta.setLore(lore4);
        }
        azulclaro.setItemMeta(azulclaroMeta);
        INVENTORY_LOJACOLORIDOSCONCRETOS.setItem(13, azulclaro);


        final ItemStack magenta = new ItemStack(Material.MAGENTA_CONCRETE_POWDER);
        final ItemMeta magentaMeta = magenta.getItemMeta();
        magentaMeta.setDisplayName("§ePó de concreto magenta");
        final ArrayList<String> lore3 = new ArrayList<String>();
        if (LojaAPI.valorVenda("po_de_concreto_magenta", type) != 0 && LojaAPI.valorCompra("po_de_concreto_magenta", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_magenta", type)));
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_magenta", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_magenta", type));
            lore3.add(" ");
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            magentaMeta.setLore(lore3);
        } else if (LojaAPI.valorCompra("po_de_concreto_magenta", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_magenta", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_magenta", type));
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add(" ");
            magentaMeta.setLore(lore3);
        } else if (LojaAPI.valorVenda("po_de_concreto_magenta", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_magenta", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_magenta", type));
            lore3.add(" ");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore3.add(" ");
            magentaMeta.setLore(lore3);
        }
        magenta.setItemMeta(magentaMeta);
        INVENTORY_LOJACOLORIDOSCONCRETOS.setItem(12, magenta);


        final ItemStack laranja = new ItemStack(Material.ORANGE_CONCRETE_POWDER);
        final ItemMeta laranjaMeta = laranja.getItemMeta();
        laranjaMeta.setDisplayName("§ePó de concreto laranja");
        final ArrayList<String> lore2 = new ArrayList<String>();
        if (LojaAPI.valorVenda("po_de_concreto_laranja", type) != 0 && LojaAPI.valorCompra("po_de_concreto_laranja", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_laranja", type)));
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_laranja", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_laranja", type));
            lore2.add(" ");
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            laranjaMeta.setLore(lore2);
        } else if (LojaAPI.valorCompra("po_de_concreto_laranja", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_laranja", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_laranja", type));
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add(" ");
            laranjaMeta.setLore(lore2);
        } else if (LojaAPI.valorVenda("po_de_concreto_laranja", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_laranja", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_laranja", type));
            lore2.add(" ");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore2.add(" ");
            laranjaMeta.setLore(lore2);
        }
        laranja.setItemMeta(laranjaMeta);
        INVENTORY_LOJACOLORIDOSCONCRETOS.setItem(11, laranja);


        final ItemStack branco = new ItemStack(Material.WHITE_CONCRETE_POWDER);
        final ItemMeta brancoMeta = branco.getItemMeta();
        brancoMeta.setDisplayName("§ePó de concreto branco");
        final ArrayList<String> lore1 = new ArrayList<String>();
        if (LojaAPI.valorVenda("po_de_concreto_branco", type) != 0 && LojaAPI.valorCompra("po_de_concreto_branco", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_branco", type)));
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_branco", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_branco", type));
            lore1.add(" ");
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            brancoMeta.setLore(lore1);
        } else if (LojaAPI.valorCompra("po_de_concreto_branco", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_concreto_branco", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_branco", type));
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add(" ");
            brancoMeta.setLore(lore1);
        } else if (LojaAPI.valorVenda("po_de_concreto_branco", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_concreto_branco", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_concreto_branco", type));
            lore1.add(" ");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore1.add(" ");
            brancoMeta.setLore(lore1);
        }
        branco.setItemMeta(brancoMeta);
        INVENTORY_LOJACOLORIDOSCONCRETOS.setItem(10, branco);

        return INVENTORY_LOJACOLORIDOSCONCRETOS;
    }

    public static Inventory buildLojaTerracota(final String type) {
        final Inventory INVENTORY_LOJACOLORIDOSTERRACOTAS = Bukkit.createInventory(null, 5 * 9, "Terracotas: ");

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta voltarMeta = voltar.getItemMeta();
        voltarMeta.setDisplayName(ChatColor.GREEN + "Voltar página.");
        voltar.setItemMeta(voltarMeta);
        INVENTORY_LOJACOLORIDOSTERRACOTAS.setItem(36, voltar);


        final ItemStack terracota = new ItemStack(Material.TERRACOTTA);
        final ItemMeta terracotaMeta = terracota.getItemMeta();
        terracotaMeta.setDisplayName("§eTerracota");
        final ArrayList<String> lore = new ArrayList<String>();
        if (LojaAPI.valorVenda("terracota", type) != 0 && LojaAPI.valorCompra("terracota", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota", type)));
            lore.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota", type));
            lore.add(" ");
            lore.add(" ");
            lore.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            terracotaMeta.setLore(lore);
        } else if (LojaAPI.valorCompra("terracota", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota", type));
            lore.add(" ");
            lore.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore.add(" ");
            terracotaMeta.setLore(lore);
        } else if (LojaAPI.valorVenda("terracota", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota", type));
            lore.add(" ");
            lore.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore.add(" ");
            terracotaMeta.setLore(lore);
        }
        terracota.setItemMeta(terracotaMeta);
        INVENTORY_LOJACOLORIDOSTERRACOTAS.setItem(32, terracota);


        final ItemStack preto = new ItemStack(Material.BLACK_TERRACOTTA);
        final ItemMeta pretoMeta = preto.getItemMeta();
        pretoMeta.setDisplayName("§eTerracota preta");
        final ArrayList<String> lore16 = new ArrayList<String>();
        if (LojaAPI.valorVenda("terracota_preto", type) != 0 && LojaAPI.valorCompra("terracota_preto", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_preto", type)));
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_preto", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_preto", type));
            lore16.add(" ");
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            pretoMeta.setLore(lore16);
        } else if (LojaAPI.valorCompra("terracota_preto", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_preto", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_preto", type));
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add(" ");
            pretoMeta.setLore(lore16);
        } else if (LojaAPI.valorVenda("terracota_preto", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_preto", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_preto", type));
            lore16.add(" ");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore16.add(" ");
            pretoMeta.setLore(lore16);
        }
        preto.setItemMeta(pretoMeta);
        INVENTORY_LOJACOLORIDOSTERRACOTAS.setItem(31, preto);


        final ItemStack vermelho = new ItemStack(Material.RED_TERRACOTTA);
        final ItemMeta vermelhoMeta = vermelho.getItemMeta();
        vermelhoMeta.setDisplayName("§eTerracota vermelho");
        final ArrayList<String> lore15 = new ArrayList<String>();
        if (LojaAPI.valorVenda("terracota_vermelho", type) != 0 && LojaAPI.valorCompra("terracota_vermelho", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_vermelho", type)));
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_vermelho", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_vermelho", type));
            lore15.add(" ");
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            vermelhoMeta.setLore(lore15);
        } else if (LojaAPI.valorCompra("terracota_vermelho", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_vermelho", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_vermelho", type));
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add(" ");
            vermelhoMeta.setLore(lore15);
        } else if (LojaAPI.valorVenda("terracota_vermelho", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_vermelho", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_vermelho", type));
            lore15.add(" ");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore15.add(" ");
            vermelhoMeta.setLore(lore15);
        }
        vermelho.setItemMeta(vermelhoMeta);
        INVENTORY_LOJACOLORIDOSTERRACOTAS.setItem(30, vermelho);


        final ItemStack verdescuro = new ItemStack(Material.GREEN_TERRACOTTA);
        final ItemMeta verdescuroMeta = verdescuro.getItemMeta();
        verdescuroMeta.setDisplayName("§eTerracota verde");
        final ArrayList<String> lore14 = new ArrayList<String>();
        if (LojaAPI.valorVenda("terracota_verde", type) != 0 && LojaAPI.valorCompra("terracota_verde", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_verde", type)));
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_verde", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_verde", type));
            lore14.add(" ");
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            verdescuroMeta.setLore(lore14);
        } else if (LojaAPI.valorCompra("terracota_verde", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_verde", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_verde", type));
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add(" ");
            verdescuroMeta.setLore(lore14);
        } else if (LojaAPI.valorVenda("terracota_verde", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_verde", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_verde", type));
            lore14.add(" ");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore14.add(" ");
            verdescuroMeta.setLore(lore14);
        }
        verdescuro.setItemMeta(verdescuroMeta);
        INVENTORY_LOJACOLORIDOSTERRACOTAS.setItem(25, verdescuro);


        final ItemStack marrom = new ItemStack(Material.BROWN_TERRACOTTA);
        final ItemMeta marromMeta = marrom.getItemMeta();
        marromMeta.setDisplayName("§eTerracota marrom");
        final ArrayList<String> lore13 = new ArrayList<String>();
        if (LojaAPI.valorVenda("terracota_marrom", type) != 0 && LojaAPI.valorCompra("terracota_marrom", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_marrom", type)));
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_marrom", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_marrom", type));
            lore13.add(" ");
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            marromMeta.setLore(lore13);
        } else if (LojaAPI.valorCompra("terracota_marrom", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_marrom", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_marrom", type));
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add(" ");
            marromMeta.setLore(lore13);
        } else if (LojaAPI.valorVenda("terracota_marrom", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_marrom", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_marrom", type));
            lore13.add(" ");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore13.add(" ");
            marromMeta.setLore(lore13);
        }
        marrom.setItemMeta(marromMeta);
        INVENTORY_LOJACOLORIDOSTERRACOTAS.setItem(24, marrom);


        final ItemStack azul = new ItemStack(Material.BLUE_TERRACOTTA);
        final ItemMeta azulMeta = azul.getItemMeta();
        azulMeta.setDisplayName("§eTerracota azul");
        final ArrayList<String> lore12 = new ArrayList<String>();
        if (LojaAPI.valorVenda("terracota_azul", type) != 0 && LojaAPI.valorCompra("terracota_azul", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_azul", type)));
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_azul", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_azul", type));
            lore12.add(" ");
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            azulMeta.setLore(lore12);
        } else if (LojaAPI.valorCompra("terracota_azul", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_azul", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_azul", type));
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add(" ");
            azulMeta.setLore(lore12);
        } else if (LojaAPI.valorVenda("terracota_azul", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_azul", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_azul", type));
            lore12.add(" ");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore12.add(" ");
            azulMeta.setLore(lore12);
        }
        azul.setItemMeta(azulMeta);
        INVENTORY_LOJACOLORIDOSTERRACOTAS.setItem(23, azul);


        final ItemStack lilas = new ItemStack(Material.PURPLE_TERRACOTTA);
        final ItemMeta lilasMeta = lilas.getItemMeta();
        lilasMeta.setDisplayName("§eTerracota lílas");
        final ArrayList<String> lore11 = new ArrayList<String>();
        if (LojaAPI.valorVenda("terracota_lilas", type) != 0 && LojaAPI.valorCompra("terracota_lilas", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_lilas", type)));
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_lilas", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_lilas", type));
            lore11.add(" ");
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lilasMeta.setLore(lore11);
        } else if (LojaAPI.valorCompra("terracota_lilas", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_lilas", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_lilas", type));
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add(" ");
            lilasMeta.setLore(lore11);
        } else if (LojaAPI.valorVenda("terracota_lilas", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_lilas", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_lilas", type));
            lore11.add(" ");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore11.add(" ");
            lilasMeta.setLore(lore11);
        }
        lilas.setItemMeta(lilasMeta);
        INVENTORY_LOJACOLORIDOSTERRACOTAS.setItem(22, lilas);


        final ItemStack ciano = new ItemStack(Material.CYAN_TERRACOTTA);
        final ItemMeta cianoMeta = ciano.getItemMeta();
        cianoMeta.setDisplayName("§eTerracota ciano");
        final ArrayList<String> lore10 = new ArrayList<String>();
        if (LojaAPI.valorVenda("terracota_ciano", type) != 0 && LojaAPI.valorCompra("terracota_ciano", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_ciano", type)));
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_ciano", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_ciano", type));
            lore10.add(" ");
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            cianoMeta.setLore(lore10);
        } else if (LojaAPI.valorCompra("terracota_ciano", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_ciano", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_ciano", type));
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add(" ");
            cianoMeta.setLore(lore10);
        } else if (LojaAPI.valorVenda("terracota_ciano", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_ciano", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_ciano", type));
            lore10.add(" ");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore10.add(" ");
            cianoMeta.setLore(lore10);
        }
        ciano.setItemMeta(cianoMeta);
        INVENTORY_LOJACOLORIDOSTERRACOTAS.setItem(21, ciano);


        final ItemStack cinzaclaro = new ItemStack(Material.LIGHT_GRAY_TERRACOTTA);
        final ItemMeta cinzaclaroMeta = cinzaclaro.getItemMeta();
        cinzaclaroMeta.setDisplayName("§eTerracota cinza-clara");
        final ArrayList<String> lore9 = new ArrayList<String>();
        if (LojaAPI.valorVenda("terracota_cinza_clara", type) != 0 && LojaAPI.valorCompra("terracota_cinza_clara", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_cinza_clara", type)));
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_cinza_clara", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_cinza_clara", type));
            lore9.add(" ");
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            cinzaclaroMeta.setLore(lore9);
        } else if (LojaAPI.valorCompra("terracota_cinza_clara", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_cinza_clara", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_cinza_clara", type));
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add(" ");
            cinzaclaroMeta.setLore(lore9);
        } else if (LojaAPI.valorVenda("terracota_cinza_clara", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_cinza_clara", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_cinza_clara", type));
            lore9.add(" ");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore9.add(" ");
            cinzaclaroMeta.setLore(lore9);
        }
        cinzaclaro.setItemMeta(cinzaclaroMeta);
        INVENTORY_LOJACOLORIDOSTERRACOTAS.setItem(20, cinzaclaro);


        final ItemStack cinza = new ItemStack(Material.GRAY_TERRACOTTA);
        final ItemMeta cinzaMeta = cinza.getItemMeta();
        cinzaMeta.setDisplayName("§eTerracota cinza");
        final ArrayList<String> lore8 = new ArrayList<String>();
        if (LojaAPI.valorVenda("terracota_cinza", type) != 0 && LojaAPI.valorCompra("terracota_cinza", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_cinza", type)));
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_cinza", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_cinza", type));
            lore8.add(" ");
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            cinzaMeta.setLore(lore8);
        } else if (LojaAPI.valorCompra("terracota_cinza", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_cinza", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_cinza", type));
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add(" ");
            cinzaMeta.setLore(lore8);
        } else if (LojaAPI.valorVenda("terracota_cinza", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_cinza", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_cinza", type));
            lore8.add(" ");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore8.add(" ");
            cinzaMeta.setLore(lore8);
        }
        cinza.setItemMeta(cinzaMeta);
        INVENTORY_LOJACOLORIDOSTERRACOTAS.setItem(19, cinza);


        final ItemStack rosa = new ItemStack(Material.PINK_TERRACOTTA);
        final ItemMeta rosaMeta = rosa.getItemMeta();
        rosaMeta.setDisplayName("§eTerracota rosa");
        final ArrayList<String> lore7 = new ArrayList<String>();
        if (LojaAPI.valorVenda("terracota_rosa", type) != 0 && LojaAPI.valorCompra("terracota_rosa", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_rosa", type)));
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_rosa", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_rosa", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            rosaMeta.setLore(lore7);
        } else if (LojaAPI.valorCompra("terracota_rosa", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_rosa", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_rosa", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add(" ");
            rosaMeta.setLore(lore7);
        } else if (LojaAPI.valorVenda("terracota_rosa", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_rosa", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_rosa", type));
            lore7.add(" ");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore7.add(" ");
            rosaMeta.setLore(lore7);
        }
        rosa.setItemMeta(rosaMeta);
        INVENTORY_LOJACOLORIDOSTERRACOTAS.setItem(16, rosa);


        final ItemStack verdeclaro = new ItemStack(Material.LIME_TERRACOTTA);
        final ItemMeta verdeclaroMeta = verdeclaro.getItemMeta();
        verdeclaroMeta.setDisplayName("§eTerracota verde-limão");
        final ArrayList<String> lore6 = new ArrayList<String>();
        if (LojaAPI.valorVenda("terracota_verde_limao", type) != 0 && LojaAPI.valorCompra("terracota_verde_limao", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_verde_limao", type)));
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_verde_limao", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_verde_limao", type));
            lore6.add(" ");
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            verdeclaroMeta.setLore(lore6);
        } else if (LojaAPI.valorCompra("terracota_verde_limao", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_verde_limao", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_verde_limao", type));
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add(" ");
            verdeclaroMeta.setLore(lore6);
        } else if (LojaAPI.valorVenda("terracota_verde_limao", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_verde_limao", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_verde_limao", type));
            lore6.add(" ");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore6.add(" ");
            verdeclaroMeta.setLore(lore6);
        }
        verdeclaro.setItemMeta(verdeclaroMeta);
        INVENTORY_LOJACOLORIDOSTERRACOTAS.setItem(15, verdeclaro);


        final ItemStack amarelo = new ItemStack(Material.YELLOW_TERRACOTTA);
        final ItemMeta amareloMeta = amarelo.getItemMeta();
        amareloMeta.setDisplayName("§eTerracota amarela");
        final ArrayList<String> lore5 = new ArrayList<String>();
        if (LojaAPI.valorVenda("terracota_amarela", type) != 0 && LojaAPI.valorCompra("terracota_amarela", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_amarela", type)));
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_amarela", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_amarela", type));
            lore5.add(" ");
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            amareloMeta.setLore(lore5);
        } else if (LojaAPI.valorCompra("terracota_amarela", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_amarela", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_amarela", type));
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add(" ");
            amareloMeta.setLore(lore5);
        } else if (LojaAPI.valorVenda("terracota_amarela", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_amarela", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_amarela", type));
            lore5.add(" ");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore5.add(" ");
            amareloMeta.setLore(lore5);
        }
        amarelo.setItemMeta(amareloMeta);
        INVENTORY_LOJACOLORIDOSTERRACOTAS.setItem(14, amarelo);


        final ItemStack azulclaro = new ItemStack(Material.LIGHT_BLUE_TERRACOTTA);
        final ItemMeta azulclaroMeta = azulclaro.getItemMeta();
        azulclaroMeta.setDisplayName("§eTerracota azul-clara");
        final ArrayList<String> lore4 = new ArrayList<String>();
        if (LojaAPI.valorVenda("terracota_azul_clara", type) != 0 && LojaAPI.valorCompra("terracota_azul_clara", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_azul_clara", type)));
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_azul_clara", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_azul_clara", type));
            lore4.add(" ");
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            azulclaroMeta.setLore(lore4);
        } else if (LojaAPI.valorCompra("terracota_azul_clara", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_azul_clara", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_azul_clara", type));
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add(" ");
            azulclaroMeta.setLore(lore4);
        } else if (LojaAPI.valorVenda("terracota_azul_clara", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_azul_clara", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_azul_clara", type));
            lore4.add(" ");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore4.add(" ");
            azulclaroMeta.setLore(lore4);
        }
        azulclaro.setItemMeta(azulclaroMeta);
        INVENTORY_LOJACOLORIDOSTERRACOTAS.setItem(13, azulclaro);


        final ItemStack magenta = new ItemStack(Material.MAGENTA_TERRACOTTA);
        final ItemMeta magentaMeta = magenta.getItemMeta();
        magentaMeta.setDisplayName("§eTerracota magenta");
        final ArrayList<String> lore3 = new ArrayList<String>();
        if (LojaAPI.valorVenda("terracota_magenta", type) != 0 && LojaAPI.valorCompra("terracota_magenta", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_magenta", type)));
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_magenta", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_magenta", type));
            lore3.add(" ");
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            magentaMeta.setLore(lore3);
        } else if (LojaAPI.valorCompra("terracota_magenta", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_magenta", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_magenta", type));
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add(" ");
            magentaMeta.setLore(lore3);
        } else if (LojaAPI.valorVenda("terracota_magenta", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_magenta", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_magenta", type));
            lore3.add(" ");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore3.add(" ");
            magentaMeta.setLore(lore3);
        }
        magenta.setItemMeta(magentaMeta);
        INVENTORY_LOJACOLORIDOSTERRACOTAS.setItem(12, magenta);


        final ItemStack laranja = new ItemStack(Material.ORANGE_TERRACOTTA);
        final ItemMeta laranjaMeta = laranja.getItemMeta();
        laranjaMeta.setDisplayName("§eTerracota laranja");
        final ArrayList<String> lore2 = new ArrayList<String>();
        if (LojaAPI.valorVenda("terracota_laranja", type) != 0 && LojaAPI.valorCompra("terracota_laranja", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_laranja", type)));
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_laranja", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_laranja", type));
            lore2.add(" ");
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            laranjaMeta.setLore(lore2);
        } else if (LojaAPI.valorCompra("terracota_laranja", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_laranja", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_laranja", type));
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add(" ");
            laranjaMeta.setLore(lore2);
        } else if (LojaAPI.valorVenda("terracota_laranja", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_laranja", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_laranja", type));
            lore2.add(" ");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore2.add(" ");
            laranjaMeta.setLore(lore2);
        }
        laranja.setItemMeta(laranjaMeta);
        INVENTORY_LOJACOLORIDOSTERRACOTAS.setItem(11, laranja);


        final ItemStack branco = new ItemStack(Material.WHITE_TERRACOTTA);
        final ItemMeta brancoMeta = branco.getItemMeta();
        brancoMeta.setDisplayName("§eTerracota branca");
        final ArrayList<String> lore1 = new ArrayList<String>();
        if (LojaAPI.valorVenda("terracota_branca", type) != 0 && LojaAPI.valorCompra("terracota_branca", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_branca", type)));
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_branca", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_branca", type));
            lore1.add(" ");
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            brancoMeta.setLore(lore1);
        } else if (LojaAPI.valorCompra("terracota_branca", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terracota_branca", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_branca", type));
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add(" ");
            brancoMeta.setLore(lore1);
        } else if (LojaAPI.valorVenda("terracota_branca", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terracota_branca", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("terracota_branca", type));
            lore1.add(" ");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore1.add(" ");
            brancoMeta.setLore(lore1);
        }
        branco.setItemMeta(brancoMeta);
        INVENTORY_LOJACOLORIDOSTERRACOTAS.setItem(10, branco);

        return INVENTORY_LOJACOLORIDOSTERRACOTAS;
    }

    public static Inventory buildLojaGlass(final String type) {
        final Inventory INVENTORY_LOJACOLORIDOSGLASS = Bukkit.createInventory(null, 5 * 9, "Vidros: ");

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta voltarMeta = voltar.getItemMeta();
        voltarMeta.setDisplayName(ChatColor.GREEN + "Voltar página.");
        voltar.setItemMeta(voltarMeta);
        INVENTORY_LOJACOLORIDOSGLASS.setItem(36, voltar);


        final ItemStack preto = new ItemStack(Material.BLACK_STAINED_GLASS);
        final ItemMeta pretoMeta = preto.getItemMeta();
        pretoMeta.setDisplayName("§eLã preta");
        final ArrayList<String> lore16 = new ArrayList<String>();
        if (LojaAPI.valorVenda("vidro_preto", type) != 0 && LojaAPI.valorCompra("vidro_preto", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_preto", type)));
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_preto", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_preto", type));
            lore16.add(" ");
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            pretoMeta.setLore(lore16);
        } else if (LojaAPI.valorCompra("vidro_preto", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_preto", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_preto", type));
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add(" ");
            pretoMeta.setLore(lore16);
        } else if (LojaAPI.valorVenda("vidro_preto", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_preto", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_preto", type));
            lore16.add(" ");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore16.add(" ");
            pretoMeta.setLore(lore16);
        }
        preto.setItemMeta(pretoMeta);
        INVENTORY_LOJACOLORIDOSGLASS.setItem(32, preto);


        final ItemStack vermelho = new ItemStack(Material.RED_STAINED_GLASS);
        final ItemMeta vermelhoMeta = vermelho.getItemMeta();
        vermelhoMeta.setDisplayName("§eVidro vermelho");
        final ArrayList<String> lore15 = new ArrayList<String>();
        if (LojaAPI.valorVenda("vidro_vermelho", type) != 0 && LojaAPI.valorCompra("vidro_vermelho", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_vermelho", type)));
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_vermelho", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_vermelho", type));
            lore15.add(" ");
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            vermelhoMeta.setLore(lore15);
        } else if (LojaAPI.valorCompra("vidro_vermelho", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_vermelho", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_vermelho", type));
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add(" ");
            vermelhoMeta.setLore(lore15);
        } else if (LojaAPI.valorVenda("vidro_vermelho", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_vermelho", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_vermelho", type));
            lore15.add(" ");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore15.add(" ");
            vermelhoMeta.setLore(lore15);
        }
        vermelho.setItemMeta(vermelhoMeta);
        INVENTORY_LOJACOLORIDOSGLASS.setItem(31, vermelho);


        final ItemStack verdescuro = new ItemStack(Material.GREEN_STAINED_GLASS);
        final ItemMeta verdescuroMeta = verdescuro.getItemMeta();
        verdescuroMeta.setDisplayName("§eVidro verde");
        final ArrayList<String> lore14 = new ArrayList<String>();
        if (LojaAPI.valorVenda("vidro_verde", type) != 0 && LojaAPI.valorCompra("vidro_verde", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_verde", type)));
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_verde", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_verde", type));
            lore14.add(" ");
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            verdescuroMeta.setLore(lore14);
        } else if (LojaAPI.valorCompra("vidro_verde", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_verde", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_verde", type));
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add(" ");
            verdescuroMeta.setLore(lore14);
        } else if (LojaAPI.valorVenda("vidro_verde", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_verde", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_verde", type));
            lore14.add(" ");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore14.add(" ");
            verdescuroMeta.setLore(lore14);
        }
        verdescuro.setItemMeta(verdescuroMeta);
        INVENTORY_LOJACOLORIDOSGLASS.setItem(30, verdescuro);


        final ItemStack marrom = new ItemStack(Material.BROWN_STAINED_GLASS);
        final ItemMeta marromMeta = marrom.getItemMeta();
        marromMeta.setDisplayName("§eVidro marrom");
        final ArrayList<String> lore13 = new ArrayList<String>();
        if (LojaAPI.valorVenda("vidro_marrom", type) != 0 && LojaAPI.valorCompra("vidro_marrom", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_marrom", type)));
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_marrom", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_marrom", type));
            lore13.add(" ");
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            marromMeta.setLore(lore13);
        } else if (LojaAPI.valorCompra("vidro_marrom", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_marrom", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_marrom", type));
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add(" ");
            marromMeta.setLore(lore13);
        } else if (LojaAPI.valorVenda("vidro_marrom", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_marrom", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_marrom", type));
            lore13.add(" ");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore13.add(" ");
            marromMeta.setLore(lore13);
        }
        marrom.setItemMeta(marromMeta);
        INVENTORY_LOJACOLORIDOSGLASS.setItem(25, marrom);


        final ItemStack azul = new ItemStack(Material.BLUE_STAINED_GLASS);
        final ItemMeta azulMeta = azul.getItemMeta();
        azulMeta.setDisplayName("§eVidro azul");
        final ArrayList<String> lore12 = new ArrayList<String>();
        if (LojaAPI.valorVenda("vidro_azul", type) != 0 && LojaAPI.valorCompra("vidro_azul", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_azul", type)));
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_azul", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_azul", type));
            lore12.add(" ");
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            azulMeta.setLore(lore12);
        } else if (LojaAPI.valorCompra("vidro_azul", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_azul", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_azul", type));
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add(" ");
            azulMeta.setLore(lore12);
        } else if (LojaAPI.valorVenda("vidro_azul", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_azul", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_azul", type));
            lore12.add(" ");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore12.add(" ");
            azulMeta.setLore(lore12);
        }
        azul.setItemMeta(azulMeta);
        INVENTORY_LOJACOLORIDOSGLASS.setItem(24, azul);


        final ItemStack lilas = new ItemStack(Material.PURPLE_STAINED_GLASS);
        final ItemMeta lilasMeta = lilas.getItemMeta();
        lilasMeta.setDisplayName("§eVidro lílas");
        final ArrayList<String> lore11 = new ArrayList<String>();
        if (LojaAPI.valorVenda("vidro_lilas", type) != 0 && LojaAPI.valorCompra("vidro_lilas", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_lilas", type)));
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_lilas", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_lilas", type));
            lore11.add(" ");
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lilasMeta.setLore(lore11);
        } else if (LojaAPI.valorCompra("vidro_lilas", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_lilas", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_lilas", type));
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add(" ");
            lilasMeta.setLore(lore11);
        } else if (LojaAPI.valorVenda("vidro_lilas", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_lilas", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_lilas", type));
            lore11.add(" ");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore11.add(" ");
            lilasMeta.setLore(lore11);
        }
        lilas.setItemMeta(lilasMeta);
        INVENTORY_LOJACOLORIDOSGLASS.setItem(23, lilas);


        final ItemStack ciano = new ItemStack(Material.CYAN_STAINED_GLASS);
        final ItemMeta cianoMeta = ciano.getItemMeta();
        cianoMeta.setDisplayName("§eVidro ciano");
        final ArrayList<String> lore10 = new ArrayList<String>();
        if (LojaAPI.valorVenda("vidro_ciano", type) != 0 && LojaAPI.valorCompra("vidro_ciano", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_ciano", type)));
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_ciano", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_ciano", type));
            lore10.add(" ");
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            cianoMeta.setLore(lore10);
        } else if (LojaAPI.valorCompra("vidro_ciano", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_ciano", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_ciano", type));
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add(" ");
            cianoMeta.setLore(lore10);
        } else if (LojaAPI.valorVenda("vidro_ciano", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_ciano", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_ciano", type));
            lore10.add(" ");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore10.add(" ");
            cianoMeta.setLore(lore10);
        }
        ciano.setItemMeta(cianoMeta);
        INVENTORY_LOJACOLORIDOSGLASS.setItem(22, ciano);


        final ItemStack cinzaclaro = new ItemStack(Material.LIGHT_GRAY_STAINED_GLASS);
        final ItemMeta cinzaclaroMeta = cinzaclaro.getItemMeta();
        cinzaclaroMeta.setDisplayName("§eVidro cinza-claro");
        final ArrayList<String> lore9 = new ArrayList<String>();
        if (LojaAPI.valorVenda("vidro_cinza_claro", type) != 0 && LojaAPI.valorCompra("vidro_cinza_claro", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_cinza_claro", type)));
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_cinza_claro", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_cinza_claro", type));
            lore9.add(" ");
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            cinzaclaroMeta.setLore(lore9);
        } else if (LojaAPI.valorCompra("vidro_cinza_claro", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_cinza_claro", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_cinza_claro", type));
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add(" ");
            cinzaclaroMeta.setLore(lore9);
        } else if (LojaAPI.valorVenda("vidro_cinza_claro", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_cinza_claro", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_cinza_claro", type));
            lore9.add(" ");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore9.add(" ");
            cinzaclaroMeta.setLore(lore9);
        }
        cinzaclaro.setItemMeta(cinzaclaroMeta);
        INVENTORY_LOJACOLORIDOSGLASS.setItem(21, cinzaclaro);


        final ItemStack cinza = new ItemStack(Material.GRAY_STAINED_GLASS);
        final ItemMeta cinzaMeta = cinza.getItemMeta();
        cinzaMeta.setDisplayName("§eVidro cinza");
        final ArrayList<String> lore8 = new ArrayList<String>();
        if (LojaAPI.valorVenda("vidro_cinza", type) != 0 && LojaAPI.valorCompra("vidro_cinza", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_cinza", type)));
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_cinza", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_cinza", type));
            lore8.add(" ");
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            cinzaMeta.setLore(lore8);
        } else if (LojaAPI.valorCompra("vidro_cinza", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_cinza", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_cinza", type));
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add(" ");
            cinzaMeta.setLore(lore8);
        } else if (LojaAPI.valorVenda("vidro_cinza", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_cinza", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_cinza", type));
            lore8.add(" ");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore8.add(" ");
            cinzaMeta.setLore(lore8);
        }
        cinza.setItemMeta(cinzaMeta);
        INVENTORY_LOJACOLORIDOSGLASS.setItem(20, cinza);


        final ItemStack rosa = new ItemStack(Material.PINK_STAINED_GLASS);
        final ItemMeta rosaMeta = rosa.getItemMeta();
        rosaMeta.setDisplayName("§eVidro rosa");
        final ArrayList<String> lore7 = new ArrayList<String>();
        if (LojaAPI.valorVenda("vidro_rosa", type) != 0 && LojaAPI.valorCompra("vidro_rosa", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_rosa", type)));
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_rosa", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_rosa", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            rosaMeta.setLore(lore7);
        } else if (LojaAPI.valorCompra("vidro_rosa", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_rosa", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_rosa", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add(" ");
            rosaMeta.setLore(lore7);
        } else if (LojaAPI.valorVenda("vidro_rosa", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_rosa", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_rosa", type));
            lore7.add(" ");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore7.add(" ");
            rosaMeta.setLore(lore7);
        }
        rosa.setItemMeta(rosaMeta);
        INVENTORY_LOJACOLORIDOSGLASS.setItem(19, rosa);


        final ItemStack verdeclaro = new ItemStack(Material.LIME_STAINED_GLASS);
        final ItemMeta verdeclaroMeta = verdeclaro.getItemMeta();
        verdeclaroMeta.setDisplayName("§eVidro verde-limão");
        final ArrayList<String> lore6 = new ArrayList<String>();
        if (LojaAPI.valorVenda("vidro_verde_limao", type) != 0 && LojaAPI.valorCompra("vidro_verde_limao", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_verde_limao", type)));
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_verde_limao", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_verde_limao", type));
            lore6.add(" ");
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            verdeclaroMeta.setLore(lore6);
        } else if (LojaAPI.valorCompra("vidro_verde_limao", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_verde_limao", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_verde_limao", type));
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add(" ");
            verdeclaroMeta.setLore(lore6);
        } else if (LojaAPI.valorVenda("vidro_verde_limao", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_verde_limao", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_verde_limao", type));
            lore6.add(" ");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore6.add(" ");
            verdeclaroMeta.setLore(lore6);
        }
        verdeclaro.setItemMeta(verdeclaroMeta);
        INVENTORY_LOJACOLORIDOSGLASS.setItem(16, verdeclaro);


        final ItemStack amarelo = new ItemStack(Material.YELLOW_STAINED_GLASS);
        final ItemMeta amareloMeta = amarelo.getItemMeta();
        amareloMeta.setDisplayName("§eVidro amarelo");
        final ArrayList<String> lore5 = new ArrayList<String>();
        if (LojaAPI.valorVenda("vidro_amarelo", type) != 0 && LojaAPI.valorCompra("vidro_amarelo", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_amarelo", type)));
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_amarelo", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_amarelo", type));
            lore5.add(" ");
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            amareloMeta.setLore(lore5);
        } else if (LojaAPI.valorCompra("vidro_amarelo", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_amarelo", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_amarelo", type));
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add(" ");
            amareloMeta.setLore(lore5);
        } else if (LojaAPI.valorVenda("vidro_amarelo", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_amarelo", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_amarelo", type));
            lore5.add(" ");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore5.add(" ");
            amareloMeta.setLore(lore5);
        }
        amarelo.setItemMeta(amareloMeta);
        INVENTORY_LOJACOLORIDOSGLASS.setItem(15, amarelo);


        final ItemStack azulclaro = new ItemStack(Material.LIGHT_BLUE_STAINED_GLASS);
        final ItemMeta azulclaroMeta = azulclaro.getItemMeta();
        azulclaroMeta.setDisplayName("§eVidro azul-claro");
        final ArrayList<String> lore4 = new ArrayList<String>();
        if (LojaAPI.valorVenda("vidro_azul_claro", type) != 0 && LojaAPI.valorCompra("vidro_azul_claro", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_azul_claro", type)));
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_azul_claro", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_azul_claro", type));
            lore4.add(" ");
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            azulclaroMeta.setLore(lore4);
        } else if (LojaAPI.valorCompra("vidro_azul_claro", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_azul_claro", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_azul_claro", type));
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add(" ");
            azulclaroMeta.setLore(lore4);
        } else if (LojaAPI.valorVenda("vidro_azul_claro", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_azul_claro", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_azul_claro", type));
            lore4.add(" ");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore4.add(" ");
            azulclaroMeta.setLore(lore4);
        }
        azulclaro.setItemMeta(azulclaroMeta);
        INVENTORY_LOJACOLORIDOSGLASS.setItem(14, azulclaro);


        final ItemStack magenta = new ItemStack(Material.MAGENTA_STAINED_GLASS);
        final ItemMeta magentaMeta = magenta.getItemMeta();
        magentaMeta.setDisplayName("§eVidro magenta");
        final ArrayList<String> lore3 = new ArrayList<String>();
        if (LojaAPI.valorVenda("vidro_magenta", type) != 0 && LojaAPI.valorCompra("vidro_magenta", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_magenta", type)));
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_magenta", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_magenta", type));
            lore3.add(" ");
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            magentaMeta.setLore(lore3);
        } else if (LojaAPI.valorCompra("vidro_magenta", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_magenta", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_magenta", type));
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add(" ");
            magentaMeta.setLore(lore3);
        } else if (LojaAPI.valorVenda("vidro_magenta", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_magenta", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_magenta", type));
            lore3.add(" ");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore3.add(" ");
            magentaMeta.setLore(lore3);
        }
        magenta.setItemMeta(magentaMeta);
        INVENTORY_LOJACOLORIDOSGLASS.setItem(13, magenta);


        final ItemStack laranja = new ItemStack(Material.ORANGE_STAINED_GLASS);
        final ItemMeta laranjaMeta = laranja.getItemMeta();
        laranjaMeta.setDisplayName("§eVidro laranja");
        final ArrayList<String> lore2 = new ArrayList<String>();
        if (LojaAPI.valorVenda("vidro_laranja", type) != 0 && LojaAPI.valorCompra("vidro_laranja", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_laranja", type)));
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_laranja", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_laranja", type));
            lore2.add(" ");
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            laranjaMeta.setLore(lore2);
        } else if (LojaAPI.valorCompra("vidro_laranja", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_laranja", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_laranja", type));
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add(" ");
            laranjaMeta.setLore(lore2);
        } else if (LojaAPI.valorVenda("vidro_laranja", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_laranja", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_laranja", type));
            lore2.add(" ");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore2.add(" ");
            laranjaMeta.setLore(lore2);
        }
        laranja.setItemMeta(laranjaMeta);
        INVENTORY_LOJACOLORIDOSGLASS.setItem(12, laranja);


        final ItemStack branco = new ItemStack(Material.WHITE_STAINED_GLASS);
        final ItemMeta brancoMeta = branco.getItemMeta();
        brancoMeta.setDisplayName("§eVidro branco");
        final ArrayList<String> lore1 = new ArrayList<String>();
        if (LojaAPI.valorVenda("vidro_branco", type) != 0 && LojaAPI.valorCompra("vidro_branco", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_branco", type)));
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_branco", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_branco", type));
            lore1.add(" ");
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            brancoMeta.setLore(lore1);
        } else if (LojaAPI.valorCompra("vidro_branco", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro_branco", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_branco", type));
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add(" ");
            brancoMeta.setLore(lore1);
        } else if (LojaAPI.valorVenda("vidro_branco", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro_branco", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro_branco", type));
            lore1.add(" ");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore1.add(" ");
            brancoMeta.setLore(lore1);
        }
        branco.setItemMeta(brancoMeta);
        INVENTORY_LOJACOLORIDOSGLASS.setItem(11, branco);

        final ItemStack vidro = new ItemStack(Material.GLASS);
        final ItemMeta vidroMeta = vidro.getItemMeta();
        vidroMeta.setDisplayName("§eVidro");
        final ArrayList<String> lore = new ArrayList<String>();
        if (LojaAPI.valorVenda("vidro", type) != 0 && LojaAPI.valorCompra("vidro", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro", type)));
            lore.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro", type));
            lore.add(" ");
            lore.add(" ");
            lore.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            vidroMeta.setLore(lore);
        } else if (LojaAPI.valorCompra("vidro", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vidro", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro", type));
            lore.add(" ");
            lore.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore.add(" ");
            vidroMeta.setLore(lore);
        } else if (LojaAPI.valorVenda("vidro", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vidro", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("vidro", type));
            lore.add(" ");
            lore.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore.add(" ");
            vidroMeta.setLore(lore);
        }
        vidro.setItemMeta(vidroMeta);
        INVENTORY_LOJACOLORIDOSGLASS.setItem(10, vidro);

        return INVENTORY_LOJACOLORIDOSGLASS;
    }

    public static Inventory buildLojaWools(final String type) {
        final Inventory INVENTORY_LOJACOLORIDOSWOOLS = Bukkit.createInventory(null, 5 * 9, "Lãs: ");

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta voltarMeta = voltar.getItemMeta();
        voltarMeta.setDisplayName(ChatColor.GREEN + "Voltar página.");
        voltar.setItemMeta(voltarMeta);
        INVENTORY_LOJACOLORIDOSWOOLS.setItem(36, voltar);


        final ItemStack preto = new ItemStack(Material.BLACK_WOOL);
        final ItemMeta pretoMeta = preto.getItemMeta();
        pretoMeta.setDisplayName("§eLã preta");
        final ArrayList<String> lore16 = new ArrayList<String>();
        if (LojaAPI.valorVenda("la_preta", type) != 0 && LojaAPI.valorCompra("la_preta", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_preta", type)));
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_preta", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_preta", type));
            lore16.add(" ");
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            pretoMeta.setLore(lore16);
        } else if (LojaAPI.valorCompra("la_preta", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_preta", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_preta", type));
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add(" ");
            pretoMeta.setLore(lore16);
        } else if (LojaAPI.valorVenda("la_preta", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_preta", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_preta", type));
            lore16.add(" ");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore16.add(" ");
            pretoMeta.setLore(lore16);
        }
        preto.setItemMeta(pretoMeta);
        INVENTORY_LOJACOLORIDOSWOOLS.setItem(29, preto);


        final ItemStack vermelho = new ItemStack(Material.RED_WOOL);
        final ItemMeta vermelhoMeta = vermelho.getItemMeta();
        vermelhoMeta.setDisplayName("§eLã vermelho");
        final ArrayList<String> lore15 = new ArrayList<String>();
        if (LojaAPI.valorVenda("la_vermelho", type) != 0 && LojaAPI.valorCompra("la_vermelho", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_vermelho", type)));
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_vermelho", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_vermelho", type));
            lore15.add(" ");
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            vermelhoMeta.setLore(lore15);
        } else if (LojaAPI.valorCompra("la_vermelho", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_vermelho", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_vermelho", type));
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add(" ");
            vermelhoMeta.setLore(lore15);
        } else if (LojaAPI.valorVenda("la_vermelho", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_vermelho", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_vermelho", type));
            lore15.add(" ");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore15.add(" ");
            vermelhoMeta.setLore(lore15);
        }
        vermelho.setItemMeta(vermelhoMeta);
        INVENTORY_LOJACOLORIDOSWOOLS.setItem(28, vermelho);


        final ItemStack verdescuro = new ItemStack(Material.GREEN_WOOL);
        final ItemMeta verdescuroMeta = verdescuro.getItemMeta();
        verdescuroMeta.setDisplayName("§eLã verde");
        final ArrayList<String> lore14 = new ArrayList<String>();
        if (LojaAPI.valorVenda("la_verde", type) != 0 && LojaAPI.valorCompra("la_verde", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_verde", type)));
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_verde", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_verde", type));
            lore14.add(" ");
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            verdescuroMeta.setLore(lore14);
        } else if (LojaAPI.valorCompra("la_verde", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_verde", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_verde", type));
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add(" ");
            verdescuroMeta.setLore(lore14);
        } else if (LojaAPI.valorVenda("la_verde", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_verde", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_verde", type));
            lore14.add(" ");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore14.add(" ");
            verdescuroMeta.setLore(lore14);
        }
        verdescuro.setItemMeta(verdescuroMeta);
        INVENTORY_LOJACOLORIDOSWOOLS.setItem(25, verdescuro);


        final ItemStack marrom = new ItemStack(Material.BROWN_WOOL);
        final ItemMeta marromMeta = marrom.getItemMeta();
        marromMeta.setDisplayName("§eLã marrom");
        final ArrayList<String> lore13 = new ArrayList<String>();
        if (LojaAPI.valorVenda("la_marrom", type) != 0 && LojaAPI.valorCompra("la_marrom", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_marrom", type)));
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_marrom", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_marrom", type));
            lore13.add(" ");
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            marromMeta.setLore(lore13);
        } else if (LojaAPI.valorCompra("la_marrom", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_marrom", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_marrom", type));
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add(" ");
            marromMeta.setLore(lore13);
        } else if (LojaAPI.valorVenda("la_marrom", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_marrom", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_marrom", type));
            lore13.add(" ");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore13.add(" ");
            marromMeta.setLore(lore13);
        }
        marrom.setItemMeta(marromMeta);
        INVENTORY_LOJACOLORIDOSWOOLS.setItem(24, marrom);


        final ItemStack azul = new ItemStack(Material.BLUE_WOOL);
        final ItemMeta azulMeta = azul.getItemMeta();
        azulMeta.setDisplayName("§eLã azul");
        final ArrayList<String> lore12 = new ArrayList<String>();
        if (LojaAPI.valorVenda("la_azul", type) != 0 && LojaAPI.valorCompra("la_azul", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_azul", type)));
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_azul", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_azul", type));
            lore12.add(" ");
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            azulMeta.setLore(lore12);
        } else if (LojaAPI.valorCompra("la_azul", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_azul", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_azul", type));
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add(" ");
            azulMeta.setLore(lore12);
        } else if (LojaAPI.valorVenda("la_azul", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_azul", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_azul", type));
            lore12.add(" ");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore12.add(" ");
            azulMeta.setLore(lore12);
        }
        azul.setItemMeta(azulMeta);
        INVENTORY_LOJACOLORIDOSWOOLS.setItem(23, azul);


        final ItemStack lilas = new ItemStack(Material.PURPLE_WOOL);
        final ItemMeta lilasMeta = lilas.getItemMeta();
        lilasMeta.setDisplayName("§eLã lílas");
        final ArrayList<String> lore11 = new ArrayList<String>();
        if (LojaAPI.valorVenda("la_lilas", type) != 0 && LojaAPI.valorCompra("la_lilas", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_lilas", type)));
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_lilas", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_lilas", type));
            lore11.add(" ");
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lilasMeta.setLore(lore11);
        } else if (LojaAPI.valorCompra("la_lilas", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_lilas", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_lilas", type));
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add(" ");
            lilasMeta.setLore(lore11);
        } else if (LojaAPI.valorVenda("la_lilas", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_lilas", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_lilas", type));
            lore11.add(" ");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore11.add(" ");
            lilasMeta.setLore(lore11);
        }
        lilas.setItemMeta(lilasMeta);
        INVENTORY_LOJACOLORIDOSWOOLS.setItem(22, lilas);


        final ItemStack ciano = new ItemStack(Material.CYAN_WOOL);
        final ItemMeta cianoMeta = ciano.getItemMeta();
        cianoMeta.setDisplayName("§eLã ciano");
        final ArrayList<String> lore10 = new ArrayList<String>();
        if (LojaAPI.valorVenda("la_ciano", type) != 0 && LojaAPI.valorCompra("la_ciano", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_ciano", type)));
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_ciano", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_ciano", type));
            lore10.add(" ");
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            cianoMeta.setLore(lore10);
        } else if (LojaAPI.valorCompra("la_ciano", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_ciano", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_ciano", type));
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add(" ");
            cianoMeta.setLore(lore10);
        } else if (LojaAPI.valorVenda("la_ciano", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_ciano", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_ciano", type));
            lore10.add(" ");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore10.add(" ");
            cianoMeta.setLore(lore10);
        }
        ciano.setItemMeta(cianoMeta);
        INVENTORY_LOJACOLORIDOSWOOLS.setItem(21, ciano);


        final ItemStack cinzaclaro = new ItemStack(Material.LIGHT_GRAY_WOOL);
        final ItemMeta cinzaclaroMeta = cinzaclaro.getItemMeta();
        cinzaclaroMeta.setDisplayName("§eLã cinza-claro");
        final ArrayList<String> lore9 = new ArrayList<String>();
        if (LojaAPI.valorVenda("la_cinza_claro", type) != 0 && LojaAPI.valorCompra("la_cinza_claro", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_cinza_claro", type)));
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_cinza_claro", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_cinza_claro", type));
            lore9.add(" ");
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            cinzaclaroMeta.setLore(lore9);
        } else if (LojaAPI.valorCompra("la_cinza_claro", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_cinza_claro", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_cinza_claro", type));
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add(" ");
            cinzaclaroMeta.setLore(lore9);
        } else if (LojaAPI.valorVenda("la_cinza_claro", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_cinza_claro", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_cinza_claro", type));
            lore9.add(" ");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore9.add(" ");
            cinzaclaroMeta.setLore(lore9);
        }
        cinzaclaro.setItemMeta(cinzaclaroMeta);
        INVENTORY_LOJACOLORIDOSWOOLS.setItem(20, cinzaclaro);


        final ItemStack cinza = new ItemStack(Material.GRAY_WOOL);
        final ItemMeta cinzaMeta = cinza.getItemMeta();
        cinzaMeta.setDisplayName("§eLã cinza");
        final ArrayList<String> lore8 = new ArrayList<String>();
        if (LojaAPI.valorVenda("la_cinza", type) != 0 && LojaAPI.valorCompra("la_cinza", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_cinza", type)));
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_cinza", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_cinza", type));
            lore8.add(" ");
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            cinzaMeta.setLore(lore8);
        } else if (LojaAPI.valorCompra("la_cinza", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_cinza", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_cinza", type));
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add(" ");
            cinzaMeta.setLore(lore8);
        } else if (LojaAPI.valorVenda("la_cinza", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_cinza", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_cinza", type));
            lore8.add(" ");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore8.add(" ");
            cinzaMeta.setLore(lore8);
        }
        cinza.setItemMeta(cinzaMeta);
        INVENTORY_LOJACOLORIDOSWOOLS.setItem(19, cinza);


        final ItemStack rosa = new ItemStack(Material.PINK_WOOL);
        final ItemMeta rosaMeta = rosa.getItemMeta();
        rosaMeta.setDisplayName("§eLã rosa");
        final ArrayList<String> lore7 = new ArrayList<String>();
        if (LojaAPI.valorVenda("la_rosa", type) != 0 && LojaAPI.valorCompra("la_rosa", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_rosa", type)));
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_rosa", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_rosa", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            rosaMeta.setLore(lore7);
        } else if (LojaAPI.valorCompra("la_rosa", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_rosa", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_rosa", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add(" ");
            rosaMeta.setLore(lore7);
        } else if (LojaAPI.valorVenda("la_rosa", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_rosa", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_rosa", type));
            lore7.add(" ");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore7.add(" ");
            rosaMeta.setLore(lore7);
        }
        rosa.setItemMeta(rosaMeta);
        INVENTORY_LOJACOLORIDOSWOOLS.setItem(16, rosa);


        final ItemStack verdeclaro = new ItemStack(Material.LIME_WOOL);
        final ItemMeta verdeclaroMeta = verdeclaro.getItemMeta();
        verdeclaroMeta.setDisplayName("§eLã verde-limão");
        final ArrayList<String> lore6 = new ArrayList<String>();
        if (LojaAPI.valorVenda("la_verde_limao", type) != 0 && LojaAPI.valorCompra("la_verde_limao", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_verde_limao", type)));
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_verde_limao", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_verde_limao", type));
            lore6.add(" ");
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            verdeclaroMeta.setLore(lore6);
        } else if (LojaAPI.valorCompra("la_verde_limao", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_verde_limao", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_verde_limao", type));
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add(" ");
            verdeclaroMeta.setLore(lore6);
        } else if (LojaAPI.valorVenda("la_verde_limao", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_verde_limao", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_verde_limao", type));
            lore6.add(" ");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore6.add(" ");
            verdeclaroMeta.setLore(lore6);
        }
        verdeclaro.setItemMeta(verdeclaroMeta);
        INVENTORY_LOJACOLORIDOSWOOLS.setItem(15, verdeclaro);


        final ItemStack amarelo = new ItemStack(Material.YELLOW_WOOL);
        final ItemMeta amareloMeta = amarelo.getItemMeta();
        amareloMeta.setDisplayName("§eLã amarela");
        final ArrayList<String> lore5 = new ArrayList<String>();
        if (LojaAPI.valorVenda("la_amarela", type) != 0 && LojaAPI.valorCompra("la_amarela", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_amarela", type)));
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_amarela", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_amarela", type));
            lore5.add(" ");
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            amareloMeta.setLore(lore5);
        } else if (LojaAPI.valorCompra("la_amarela", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_amarela", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_amarela", type));
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add(" ");
            amareloMeta.setLore(lore5);
        } else if (LojaAPI.valorVenda("la_amarela", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_amarela", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_amarela", type));
            lore5.add(" ");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore5.add(" ");
            amareloMeta.setLore(lore5);
        }
        amarelo.setItemMeta(amareloMeta);
        INVENTORY_LOJACOLORIDOSWOOLS.setItem(14, amarelo);


        final ItemStack azulclaro = new ItemStack(Material.LIGHT_BLUE_WOOL);
        final ItemMeta azulclaroMeta = azulclaro.getItemMeta();
        azulclaroMeta.setDisplayName("§eLã azul-clara");
        final ArrayList<String> lore4 = new ArrayList<String>();
        if (LojaAPI.valorVenda("la_azul_clara", type) != 0 && LojaAPI.valorCompra("la_azul_clara", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_azul_clara", type)));
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_azul_clara", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_azul_clara", type));
            lore4.add(" ");
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            azulclaroMeta.setLore(lore4);
        } else if (LojaAPI.valorCompra("la_azul_clara", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_azul_clara", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_azul_clara", type));
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add(" ");
            azulclaroMeta.setLore(lore4);
        } else if (LojaAPI.valorVenda("la_azul_clara", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_azul_clara", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_azul_clara", type));
            lore4.add(" ");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore4.add(" ");
            azulclaroMeta.setLore(lore4);
        }
        azulclaro.setItemMeta(azulclaroMeta);
        INVENTORY_LOJACOLORIDOSWOOLS.setItem(13, azulclaro);


        final ItemStack magenta = new ItemStack(Material.MAGENTA_WOOL);
        final ItemMeta magentaMeta = magenta.getItemMeta();
        magentaMeta.setDisplayName("§eLã magenta");
        final ArrayList<String> lore3 = new ArrayList<String>();
        if (LojaAPI.valorVenda("la_magenta", type) != 0 && LojaAPI.valorCompra("la_magenta", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_magenta", type)));
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_magenta", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_magenta", type));
            lore3.add(" ");
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            magentaMeta.setLore(lore3);
        } else if (LojaAPI.valorCompra("la_magenta", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_magenta", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_magenta", type));
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add(" ");
            magentaMeta.setLore(lore3);
        } else if (LojaAPI.valorVenda("la_magenta", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_magenta", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_magenta", type));
            lore3.add(" ");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore3.add(" ");
            magentaMeta.setLore(lore3);
        }
        magenta.setItemMeta(magentaMeta);
        INVENTORY_LOJACOLORIDOSWOOLS.setItem(12, magenta);


        final ItemStack laranja = new ItemStack(Material.ORANGE_WOOL);
        final ItemMeta laranjaMeta = laranja.getItemMeta();
        laranjaMeta.setDisplayName("§eLã laranja");
        final ArrayList<String> lore2 = new ArrayList<String>();
        if (LojaAPI.valorVenda("la_laranja", type) != 0 && LojaAPI.valorCompra("la_laranja", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_laranja", type)));
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_laranja", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_laranja", type));
            lore2.add(" ");
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            laranjaMeta.setLore(lore2);
        } else if (LojaAPI.valorCompra("la_laranja", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_laranja", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_laranja", type));
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add(" ");
            laranjaMeta.setLore(lore2);
        } else if (LojaAPI.valorVenda("la_laranja", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_laranja", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_laranja", type));
            lore2.add(" ");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore2.add(" ");
            laranjaMeta.setLore(lore2);
        }
        laranja.setItemMeta(laranjaMeta);
        INVENTORY_LOJACOLORIDOSWOOLS.setItem(11, laranja);


        final ItemStack branco = new ItemStack(Material.WHITE_WOOL);
        final ItemMeta brancoMeta = branco.getItemMeta();
        brancoMeta.setDisplayName("§eLã branca");
        final ArrayList<String> lore1 = new ArrayList<String>();
        if (LojaAPI.valorVenda("la_branca", type) != 0 && LojaAPI.valorCompra("la_branca", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_branca", type)));
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_branca", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_branca", type));
            lore1.add(" ");
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            brancoMeta.setLore(lore1);
        } else if (LojaAPI.valorCompra("la_branca", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("la_branca", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_branca", type));
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add(" ");
            brancoMeta.setLore(lore1);
        } else if (LojaAPI.valorVenda("la_branca", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("la_branca", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("la_branca", type));
            lore1.add(" ");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore1.add(" ");
            brancoMeta.setLore(lore1);
        }
        branco.setItemMeta(brancoMeta);
        INVENTORY_LOJACOLORIDOSWOOLS.setItem(10, branco);

        return INVENTORY_LOJACOLORIDOSWOOLS;
    }

    public static Inventory buildLojaCorantes(final String type) {
        final Inventory INVENTORY_LOJACOLORIDOSDYES = Bukkit.createInventory(null, 5 * 9, "Corantes: ");

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta voltarMeta = voltar.getItemMeta();
        voltarMeta.setDisplayName(ChatColor.GREEN + "Voltar página.");
        voltar.setItemMeta(voltarMeta);
        INVENTORY_LOJACOLORIDOSDYES.setItem(36, voltar);


        final ItemStack preto = new ItemStack(Material.INK_SAC);
        final ItemMeta pretoMeta = preto.getItemMeta();
        pretoMeta.setDisplayName("§eBolsa de tinta");
        final ArrayList<String> lore16 = new ArrayList<String>();
        if (LojaAPI.valorVenda("bolsa_de_tinta", type) != 0 && LojaAPI.valorCompra("bolsa_de_tinta", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bolsa_de_tinta", type)));
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bolsa_de_tinta", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("bolsa_de_tinta", type));
            lore16.add(" ");
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            pretoMeta.setLore(lore16);
        } else if (LojaAPI.valorCompra("bolsa_de_tinta", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bolsa_de_tinta", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("bolsa_de_tinta", type));
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add(" ");
            pretoMeta.setLore(lore16);
        } else if (LojaAPI.valorVenda("bolsa_de_tinta", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bolsa_de_tinta", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("bolsa_de_tinta", type));
            lore16.add(" ");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore16.add(" ");
            pretoMeta.setLore(lore16);
        }
        preto.setItemMeta(pretoMeta);
        INVENTORY_LOJACOLORIDOSDYES.setItem(29, preto);


        final ItemStack vermelho = new ItemStack(Material.ROSE_RED);
        final ItemMeta vermelhoMeta = vermelho.getItemMeta();
        vermelhoMeta.setDisplayName("§eVermelho da rosa");
        final ArrayList<String> lore15 = new ArrayList<String>();
        if (LojaAPI.valorVenda("vermelho_da_rosa", type) != 0 && LojaAPI.valorCompra("vermelho_da_rosa", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vermelho_da_rosa", type)));
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vermelho_da_rosa", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("vermelho_da_rosa", type));
            lore15.add(" ");
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            vermelhoMeta.setLore(lore15);
        } else if (LojaAPI.valorCompra("vermelho_da_rosa", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vermelho_da_rosa", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("vermelho_da_rosa", type));
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add(" ");
            vermelhoMeta.setLore(lore15);
        } else if (LojaAPI.valorVenda("vermelho_da_rosa", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vermelho_da_rosa", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("vermelho_da_rosa", type));
            lore15.add(" ");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore15.add(" ");
            vermelhoMeta.setLore(lore15);
        }
        vermelho.setItemMeta(vermelhoMeta);
        INVENTORY_LOJACOLORIDOSDYES.setItem(28, vermelho);


        final ItemStack verdescuro = new ItemStack(Material.CACTUS_GREEN);
        final ItemMeta verdescuroMeta = verdescuro.getItemMeta();
        verdescuroMeta.setDisplayName("§eVerde do cacto");
        final ArrayList<String> lore14 = new ArrayList<String>();
        if (LojaAPI.valorVenda("verde_do_cacto", type) != 0 && LojaAPI.valorCompra("verde_do_cacto", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("verde_do_cacto", type)));
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("verde_do_cacto", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("verde_do_cacto", type));
            lore14.add(" ");
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            verdescuroMeta.setLore(lore14);
        } else if (LojaAPI.valorCompra("verde_do_cacto", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("verde_do_cacto", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("verde_do_cacto", type));
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add(" ");
            verdescuroMeta.setLore(lore14);
        } else if (LojaAPI.valorVenda("verde_do_cacto", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("verde_do_cacto", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("verde_do_cacto", type));
            lore14.add(" ");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore14.add(" ");
            verdescuroMeta.setLore(lore14);
        }
        verdescuro.setItemMeta(verdescuroMeta);
        INVENTORY_LOJACOLORIDOSDYES.setItem(25, verdescuro);


        final ItemStack marrom = new ItemStack(Material.COCOA_BEANS);
        final ItemMeta marromMeta = marrom.getItemMeta();
        marromMeta.setDisplayName("§eSementes de cacau");
        final ArrayList<String> lore13 = new ArrayList<String>();
        if (LojaAPI.valorVenda("sementes_de_cacau", type) != 0 && LojaAPI.valorCompra("sementes_de_cacau", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("sementes_de_cacau", type)));
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("sementes_de_cacau", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("sementes_de_cacau", type));
            lore13.add(" ");
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            marromMeta.setLore(lore13);
        } else if (LojaAPI.valorCompra("sementes_de_cacau", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("sementes_de_cacau", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("sementes_de_cacau", type));
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add(" ");
            marromMeta.setLore(lore13);
        } else if (LojaAPI.valorVenda("sementes_de_cacau", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("sementes_de_cacau", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("sementes_de_cacau", type));
            lore13.add(" ");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore13.add(" ");
            marromMeta.setLore(lore13);
        }
        marrom.setItemMeta(marromMeta);
        INVENTORY_LOJACOLORIDOSDYES.setItem(24, marrom);


        final ItemStack lapislazuli = new ItemStack(Material.LAPIS_LAZULI);
        final ItemMeta lapislazuliMeta = lapislazuli.getItemMeta();
        lapislazuliMeta.setDisplayName("§eLapís-Lazúli");
        final ArrayList<String> lore12 = new ArrayList<String>();
        if (LojaAPI.valorVenda("lapis_lazuli", type) != 0 && LojaAPI.valorCompra("lapis_lazuli", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("lapis_lazuli", type)));
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("lapis_lazuli", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("lapis_lazuli", type));
            lore12.add(" ");
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lapislazuliMeta.setLore(lore12);
        } else if (LojaAPI.valorCompra("lapis_lazuli", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("lapis_lazuli", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("lapis_lazuli", type));
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add(" ");
            lapislazuliMeta.setLore(lore12);
        } else if (LojaAPI.valorVenda("lapis_lazuli", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("lapis_lazuli", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("lapis_lazuli", type));
            lore12.add(" ");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore12.add(" ");
            lapislazuliMeta.setLore(lore12);
        }
        lapislazuli.setItemMeta(lapislazuliMeta);
        INVENTORY_LOJACOLORIDOSDYES.setItem(23, lapislazuli);


        final ItemStack lilas = new ItemStack(Material.PURPLE_DYE);
        final ItemMeta lilasMeta = lilas.getItemMeta();
        lilasMeta.setDisplayName("§eCorante lílas");
        final ArrayList<String> lore11 = new ArrayList<String>();
        if (LojaAPI.valorVenda("corante_lilas", type) != 0 && LojaAPI.valorCompra("corante_lilas", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("corante_lilas", type)));
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("corante_lilas", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_lilas", type));
            lore11.add(" ");
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lilasMeta.setLore(lore11);
        } else if (LojaAPI.valorCompra("corante_lilas", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("corante_lilas", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_lilas", type));
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add(" ");
            lilasMeta.setLore(lore11);
        } else if (LojaAPI.valorVenda("corante_lilas", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("corante_lilas", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_lilas", type));
            lore11.add(" ");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore11.add(" ");
            lilasMeta.setLore(lore11);
        }
        lilas.setItemMeta(lilasMeta);
        INVENTORY_LOJACOLORIDOSDYES.setItem(22, lilas);


        final ItemStack ciano = new ItemStack(Material.CYAN_DYE);
        final ItemMeta cianoMeta = ciano.getItemMeta();
        cianoMeta.setDisplayName("§eCorante ciano");
        final ArrayList<String> lore10 = new ArrayList<String>();
        if (LojaAPI.valorVenda("corante_ciano", type) != 0 && LojaAPI.valorCompra("corante_ciano", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("corante_ciano", type)));
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("corante_ciano", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_ciano", type));
            lore10.add(" ");
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            cianoMeta.setLore(lore10);
        } else if (LojaAPI.valorCompra("corante_ciano", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("corante_ciano", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_ciano", type));
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add(" ");
            cianoMeta.setLore(lore10);
        } else if (LojaAPI.valorVenda("corante_ciano", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("corante_ciano", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_ciano", type));
            lore10.add(" ");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore10.add(" ");
            cianoMeta.setLore(lore10);
        }
        ciano.setItemMeta(cianoMeta);
        INVENTORY_LOJACOLORIDOSDYES.setItem(21, ciano);


        final ItemStack cinzaclaro = new ItemStack(Material.LIGHT_GRAY_DYE);
        final ItemMeta cinzaclaroMeta = cinzaclaro.getItemMeta();
        cinzaclaroMeta.setDisplayName("§eCorante cinza-claro");
        final ArrayList<String> lore9 = new ArrayList<String>();
        if (LojaAPI.valorVenda("corante_cinza_claro", type) != 0 && LojaAPI.valorCompra("corante_cinza_claro", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("corante_cinza_claro", type)));
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("corante_cinza_claro", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_cinza_claro", type));
            lore9.add(" ");
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            cinzaclaroMeta.setLore(lore9);
        } else if (LojaAPI.valorCompra("corante_cinza_claro", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("corante_cinza_claro", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_cinza_claro", type));
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add(" ");
            cinzaclaroMeta.setLore(lore9);
        } else if (LojaAPI.valorVenda("corante_cinza_claro", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("corante_cinza_claro", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_cinza_claro", type));
            lore9.add(" ");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore9.add(" ");
            cinzaclaroMeta.setLore(lore9);
        }
        cinzaclaro.setItemMeta(cinzaclaroMeta);
        INVENTORY_LOJACOLORIDOSDYES.setItem(20, cinzaclaro);


        final ItemStack cinza = new ItemStack(Material.GRAY_DYE);
        final ItemMeta cinzaMeta = cinza.getItemMeta();
        cinzaMeta.setDisplayName("§eCorante cinza");
        final ArrayList<String> lore8 = new ArrayList<String>();
        if (LojaAPI.valorVenda("corante_cinza", type) != 0 && LojaAPI.valorCompra("corante_cinza", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("corante_cinza", type)));
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("corante_cinza", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_cinza", type));
            lore8.add(" ");
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            cinzaMeta.setLore(lore8);
        } else if (LojaAPI.valorCompra("corante_cinza", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("corante_cinza", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_cinza", type));
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add(" ");
            cinzaMeta.setLore(lore8);
        } else if (LojaAPI.valorVenda("corante_cinza", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("corante_cinza", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_cinza", type));
            lore8.add(" ");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore8.add(" ");
            cinzaMeta.setLore(lore8);
        }
        cinza.setItemMeta(cinzaMeta);
        INVENTORY_LOJACOLORIDOSDYES.setItem(19, cinza);


        final ItemStack rosa = new ItemStack(Material.PINK_DYE);
        final ItemMeta rosaMeta = rosa.getItemMeta();
        rosaMeta.setDisplayName("§eCorante rosa");
        final ArrayList<String> lore7 = new ArrayList<String>();
        if (LojaAPI.valorVenda("corante_rosa", type) != 0 && LojaAPI.valorCompra("corante_rosa", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("corante_rosa", type)));
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("corante_rosa", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_rosa", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            rosaMeta.setLore(lore7);
        } else if (LojaAPI.valorCompra("corante_rosa", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("corante_rosa", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_rosa", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add(" ");
            rosaMeta.setLore(lore7);
        } else if (LojaAPI.valorVenda("corante_rosa", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("corante_rosa", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_rosa", type));
            lore7.add(" ");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore7.add(" ");
            rosaMeta.setLore(lore7);
        }
        rosa.setItemMeta(rosaMeta);
        INVENTORY_LOJACOLORIDOSDYES.setItem(16, rosa);


        final ItemStack verdeclaro = new ItemStack(Material.LIME_DYE);
        final ItemMeta verdeclaroMeta = verdeclaro.getItemMeta();
        verdeclaroMeta.setDisplayName("§eCorante verde-limão");
        final ArrayList<String> lore6 = new ArrayList<String>();
        if (LojaAPI.valorVenda("corante_verde_limao", type) != 0 && LojaAPI.valorCompra("corante_verde_limao", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("corante_verde_limao", type)));
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("corante_verde_limao", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_verde_limao", type));
            lore6.add(" ");
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            verdeclaroMeta.setLore(lore6);
        } else if (LojaAPI.valorCompra("corante_verde_limao", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("corante_verde_limao", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_verde_limao", type));
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add(" ");
            verdeclaroMeta.setLore(lore6);
        } else if (LojaAPI.valorVenda("corante_verde_limao", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("corante_verde_limao", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_verde_limao", type));
            lore6.add(" ");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore6.add(" ");
            verdeclaroMeta.setLore(lore6);
        }
        verdeclaro.setItemMeta(verdeclaroMeta);
        INVENTORY_LOJACOLORIDOSDYES.setItem(15, verdeclaro);


        final ItemStack amarelo = new ItemStack(Material.DANDELION_YELLOW);
        final ItemMeta amareloMeta = amarelo.getItemMeta();
        amareloMeta.setDisplayName("§eAmarelo do dente-de-leão");
        final ArrayList<String> lore5 = new ArrayList<String>();
        if (LojaAPI.valorVenda("amarelo_do_dente_de_leao", type) != 0 && LojaAPI.valorCompra("amarelo_do_dente_de_leao", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("amarelo_do_dente_de_leao", type)));
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("amarelo_do_dente_de_leao", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("amarelo_do_dente_de_leao", type));
            lore5.add(" ");
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            amareloMeta.setLore(lore5);
        } else if (LojaAPI.valorCompra("amarelo_do_dente_de_leao", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("amarelo_do_dente_de_leao", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("amarelo_do_dente_de_leao", type));
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add(" ");
            amareloMeta.setLore(lore5);
        } else if (LojaAPI.valorVenda("amarelo_do_dente_de_leao", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("amarelo_do_dente_de_leao", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("amarelo_do_dente_de_leao", type));
            lore5.add(" ");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore5.add(" ");
            amareloMeta.setLore(lore5);
        }
        amarelo.setItemMeta(amareloMeta);
        INVENTORY_LOJACOLORIDOSDYES.setItem(14, amarelo);


        final ItemStack azulclaro = new ItemStack(Material.LIGHT_BLUE_DYE);
        final ItemMeta azulclaroMeta = azulclaro.getItemMeta();
        azulclaroMeta.setDisplayName("§eCorante azul-claro");
        final ArrayList<String> lore4 = new ArrayList<String>();
        if (LojaAPI.valorVenda("corante_azul_claro", type) != 0 && LojaAPI.valorCompra("corante_azul_claro", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("corante_azul_claro", type)));
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("corante_azul_claro", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_azul_claro", type));
            lore4.add(" ");
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            azulclaroMeta.setLore(lore4);
        } else if (LojaAPI.valorCompra("corante_azul_claro", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("corante_azul_claro", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_azul_claro", type));
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add(" ");
            azulclaroMeta.setLore(lore4);
        } else if (LojaAPI.valorVenda("corante_azul_claro", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("corante_azul_claro", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_azul_claro", type));
            lore4.add(" ");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore4.add(" ");
            azulclaroMeta.setLore(lore4);
        }
        azulclaro.setItemMeta(azulclaroMeta);
        INVENTORY_LOJACOLORIDOSDYES.setItem(13, azulclaro);


        final ItemStack magenta = new ItemStack(Material.MAGENTA_DYE);
        final ItemMeta magentaMeta = magenta.getItemMeta();
        magentaMeta.setDisplayName("§eCorante magenta");
        final ArrayList<String> lore3 = new ArrayList<String>();
        if (LojaAPI.valorVenda("corante_magenta", type) != 0 && LojaAPI.valorCompra("corante_magenta", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("corante_magenta", type)));
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("corante_magenta", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_magenta", type));
            lore3.add(" ");
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            magentaMeta.setLore(lore3);
        } else if (LojaAPI.valorCompra("corante_magenta", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("corante_magenta", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_magenta", type));
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add(" ");
            magentaMeta.setLore(lore3);
        } else if (LojaAPI.valorVenda("corante_magenta", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("corante_magenta", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_magenta", type));
            lore3.add(" ");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore3.add(" ");
            magentaMeta.setLore(lore3);
        }
        magenta.setItemMeta(magentaMeta);
        INVENTORY_LOJACOLORIDOSDYES.setItem(12, magenta);


        final ItemStack laranja = new ItemStack(Material.ORANGE_DYE);
        final ItemMeta laranjaMeta = laranja.getItemMeta();
        laranjaMeta.setDisplayName("§eCorante laranja");
        final ArrayList<String> lore2 = new ArrayList<String>();
        if (LojaAPI.valorVenda("corante_laranja", type) != 0 && LojaAPI.valorCompra("corante_laranja", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("corante_laranja", type)));
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("corante_laranja", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_laranja", type));
            lore2.add(" ");
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            laranjaMeta.setLore(lore2);
        } else if (LojaAPI.valorCompra("corante_laranja", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("corante_laranja", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_laranja", type));
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add(" ");
            laranjaMeta.setLore(lore2);
        } else if (LojaAPI.valorVenda("corante_laranja", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("corante_laranja", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("corante_laranja", type));
            lore2.add(" ");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore2.add(" ");
            laranjaMeta.setLore(lore2);
        }
        laranja.setItemMeta(laranjaMeta);
        INVENTORY_LOJACOLORIDOSDYES.setItem(11, laranja);


        final ItemStack branco = new ItemStack(Material.BONE_MEAL);
        final ItemMeta brancoMeta = branco.getItemMeta();
        brancoMeta.setDisplayName("§eFarinha de osso");
        final ArrayList<String> lore1 = new ArrayList<String>();
        if (LojaAPI.valorVenda("farinha_de_osso", type) != 0 && LojaAPI.valorCompra("farinha_de_osso", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("farinha_de_osso", type)));
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("farinha_de_osso", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("farinha_de_osso", type));
            lore1.add(" ");
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            brancoMeta.setLore(lore1);
        } else if (LojaAPI.valorCompra("farinha_de_osso", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("farinha_de_osso", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("farinha_de_osso", type));
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add(" ");
            brancoMeta.setLore(lore1);
        } else if (LojaAPI.valorVenda("farinha_de_osso", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("farinha_de_osso", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("farinha_de_osso", type));
            lore1.add(" ");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore1.add(" ");
            brancoMeta.setLore(lore1);
        }
        branco.setItemMeta(brancoMeta);
        INVENTORY_LOJACOLORIDOSDYES.setItem(10, branco);

        return INVENTORY_LOJACOLORIDOSDYES;
    }

    public static Inventory buildLojaColoridos() {
        final Inventory INVENTORY_LOJACOLORIDOS = Bukkit.createInventory(null, 3 * 9, "Coloridos: ");

        final ItemStack concrete = new ItemStack(Material.WHITE_CONCRETE_POWDER);
        final ItemMeta concreteMeta = concrete.getItemMeta();
        concreteMeta.setDisplayName(ChatColor.GREEN + "Concretos");
        concrete.setItemMeta(concreteMeta);
        INVENTORY_LOJACOLORIDOS.setItem(15, concrete);

        final ItemStack clays = new ItemStack(Material.TERRACOTTA);
        final ItemMeta claysMeta = clays.getItemMeta();
        claysMeta.setDisplayName(ChatColor.GREEN + "Terracotas");
        clays.setItemMeta(claysMeta);
        INVENTORY_LOJACOLORIDOS.setItem(14, clays);

        final ItemStack vidros = new ItemStack(Material.WHITE_STAINED_GLASS);
        final ItemMeta vidrosMeta = vidros.getItemMeta();
        vidrosMeta.setDisplayName(ChatColor.GREEN + "Vidros");
        vidros.setItemMeta(vidrosMeta);
        INVENTORY_LOJACOLORIDOS.setItem(13, vidros);

        final ItemStack las = new ItemStack(Material.WHITE_WOOL);
        final ItemMeta lasMeta = las.getItemMeta();
        lasMeta.setDisplayName(ChatColor.GREEN + "Lãs");
        las.setItemMeta(lasMeta);
        INVENTORY_LOJACOLORIDOS.setItem(12, las);

        final ItemStack dyes = new ItemStack(Material.INK_SAC);
        final ItemMeta dyesMeta = dyes.getItemMeta();
        dyesMeta.setDisplayName(ChatColor.GREEN + "Corantes");
        dyes.setItemMeta(dyesMeta);
        INVENTORY_LOJACOLORIDOS.setItem(11, dyes);

        return INVENTORY_LOJACOLORIDOS;
    }

    public static Inventory buildLojaVarios(final String type) {
        final Inventory INVENTORY_LOJAVARIOS = Bukkit.createInventory(null, 6 * 9, "Varios: ");

        final ItemStack armaduradiamondhorse = new ItemStack(Material.DIAMOND_HORSE_ARMOR);
        final ItemMeta armaduradiamondhorseMeta = armaduradiamondhorse.getItemMeta();
        armaduradiamondhorseMeta.setDisplayName("§eArmadura de diamante para cavalo");
        final ArrayList<String> lore24 = new ArrayList<String>();
        if (LojaAPI.valorVenda("armadura_de_diamante_para_cavalo", type) != 0 && LojaAPI.valorCompra("armadura_de_diamante_para_cavalo", type) != 0) {
            lore24.add(" ");
            lore24.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("armadura_de_diamante_para_cavalo", type)));
            lore24.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("armadura_de_diamante_para_cavalo", type)));
            lore24.add("§7Quantia: §a" + LojaAPI.itemQuantia("armadura_de_diamante_para_cavalo", type));
            lore24.add(" ");
            lore24.add(" ");
            lore24.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore24.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            armaduradiamondhorseMeta.setLore(lore24);
        } else if (LojaAPI.valorCompra("armadura_de_diamante_para_cavalo", type) != 0) {
            lore24.add(" ");
            lore24.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("armadura_de_diamante_para_cavalo", type)));
            lore24.add("§7Quantia: §a" + LojaAPI.itemQuantia("armadura_de_diamante_para_cavalo", type));
            lore24.add(" ");
            lore24.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore24.add(" ");
            armaduradiamondhorseMeta.setLore(lore24);
        } else if (LojaAPI.valorVenda("armadura_de_diamante_para_cavalo", type) != 0) {
            lore24.add(" ");
            lore24.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("armadura_de_diamante_para_cavalo", type)));
            lore24.add("§7Quantia: §a" + LojaAPI.itemQuantia("armadura_de_diamante_para_cavalo", type));
            lore24.add(" ");
            lore24.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore24.add(" ");
            armaduradiamondhorseMeta.setLore(lore24);
        }
        armaduradiamondhorse.setItemMeta(armaduradiamondhorseMeta);
        INVENTORY_LOJAVARIOS.setItem(41, armaduradiamondhorse);


        final ItemStack armaduragoldhorse = new ItemStack(Material.GOLDEN_HORSE_ARMOR);
        final ItemMeta armaduragoldhorseMeta = armaduragoldhorse.getItemMeta();
        armaduragoldhorseMeta.setDisplayName("§eArmadura de ouro para cavalo");
        final ArrayList<String> lore23 = new ArrayList<String>();
        if (LojaAPI.valorVenda("armadura_de_ouro_para_cavalo", type) != 0 && LojaAPI.valorCompra("armadura_de_ouro_para_cavalo", type) != 0) {
            lore23.add(" ");
            lore23.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("armadura_de_ouro_para_cavalo", type)));
            lore23.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("armadura_de_ouro_para_cavalo", type)));
            lore23.add("§7Quantia: §a" + LojaAPI.itemQuantia("armadura_de_ouro_para_cavalo", type));
            lore23.add(" ");
            lore23.add(" ");
            lore23.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore23.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            armaduragoldhorseMeta.setLore(lore23);
        } else if (LojaAPI.valorCompra("armadura_de_ouro_para_cavalo", type) != 0) {
            lore23.add(" ");
            lore23.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("armadura_de_ouro_para_cavalo", type)));
            lore23.add("§7Quantia: §a" + LojaAPI.itemQuantia("armadura_de_ouro_para_cavalo", type));
            lore23.add(" ");
            lore23.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore23.add(" ");
            armaduragoldhorseMeta.setLore(lore23);
        } else if (LojaAPI.valorVenda("armadura_de_ouro_para_cavalo", type) != 0) {
            lore23.add(" ");
            lore23.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("armadura_de_ouro_para_cavalo", type)));
            lore23.add("§7Quantia: §a" + LojaAPI.itemQuantia("armadura_de_ouro_para_cavalo", type));
            lore23.add(" ");
            lore23.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore23.add(" ");
            armaduragoldhorseMeta.setLore(lore23);
        }
        armaduragoldhorse.setItemMeta(armaduragoldhorseMeta);
        INVENTORY_LOJAVARIOS.setItem(40, armaduragoldhorse);


        final ItemStack armaduraironhorse = new ItemStack(Material.IRON_HORSE_ARMOR);
        final ItemMeta armaduraironhorseMeta = armaduraironhorse.getItemMeta();
        armaduraironhorseMeta.setDisplayName("§eArmadura de ferro para cavalo");
        final ArrayList<String> lore22 = new ArrayList<String>();
        if (LojaAPI.valorVenda("armadura_de_ferro_para_cavalo", type) != 0 && LojaAPI.valorCompra("armadura_de_ferro_para_cavalo", type) != 0) {
            lore22.add(" ");
            lore22.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("armadura_de_ferro_para_cavalo", type)));
            lore22.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("armadura_de_ferro_para_cavalo", type)));
            lore22.add("§7Quantia: §a" + LojaAPI.itemQuantia("armadura_de_ferro_para_cavalo", type));
            lore22.add(" ");
            lore22.add(" ");
            lore22.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore22.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            armaduraironhorseMeta.setLore(lore22);
        } else if (LojaAPI.valorCompra("armadura_de_ferro_para_cavalo", type) != 0) {
            lore22.add(" ");
            lore22.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("armadura_de_ferro_para_cavalo", type)));
            lore22.add("§7Quantia: §a" + LojaAPI.itemQuantia("armadura_de_ferro_para_cavalo", type));
            lore22.add(" ");
            lore22.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore22.add(" ");
            armaduraironhorseMeta.setLore(lore22);
        } else if (LojaAPI.valorVenda("armadura_de_ferro_para_cavalo", type) != 0) {
            lore22.add(" ");
            lore22.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("armadura_de_ferro_para_cavalo", type)));
            lore22.add("§7Quantia: §a" + LojaAPI.itemQuantia("armadura_de_ferro_para_cavalo", type));
            lore22.add(" ");
            lore22.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore22.add(" ");
            armaduraironhorseMeta.setLore(lore22);
        }
        armaduraironhorse.setItemMeta(armaduraironhorseMeta);
        INVENTORY_LOJAVARIOS.setItem(39, armaduraironhorse);


        final ItemStack trilho = new ItemStack(Material.RAIL);
        final ItemMeta trilhoMeta = trilho.getItemMeta();
        trilhoMeta.setDisplayName("§eTrilho");
        final ArrayList<String> lore21 = new ArrayList<String>();
        if (LojaAPI.valorVenda("trilho", type) != 0 && LojaAPI.valorCompra("trilho", type) != 0) {
            lore21.add(" ");
            lore21.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("trilho", type)));
            lore21.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("trilho", type)));
            lore21.add("§7Quantia: §a" + LojaAPI.itemQuantia("trilho", type));
            lore21.add(" ");
            lore21.add(" ");
            lore21.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore21.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            trilhoMeta.setLore(lore21);
        } else if (LojaAPI.valorCompra("trilho", type) != 0) {
            lore21.add(" ");
            lore21.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("trilho", type)));
            lore21.add("§7Quantia: §a" + LojaAPI.itemQuantia("trilho", type));
            lore21.add(" ");
            lore21.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore21.add(" ");
            trilhoMeta.setLore(lore21);
        } else if (LojaAPI.valorVenda("trilho", type) != 0) {
            lore21.add(" ");
            lore21.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("trilho", type)));
            lore21.add("§7Quantia: §a" + LojaAPI.itemQuantia("trilho", type));
            lore21.add(" ");
            lore21.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore21.add(" ");
            trilhoMeta.setLore(lore21);
        }
        trilho.setItemMeta(trilhoMeta);
        INVENTORY_LOJAVARIOS.setItem(34, trilho);


        final ItemStack ladder = new ItemStack(Material.LADDER);
        final ItemMeta ladderMeta = ladder.getItemMeta();
        ladderMeta.setDisplayName("§eEscada");
        final ArrayList<String> lore20 = new ArrayList<String>();
        if (LojaAPI.valorVenda("escada", type) != 0 && LojaAPI.valorCompra("escada", type) != 0) {
            lore20.add(" ");
            lore20.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("escada", type)));
            lore20.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("escada", type)));
            lore20.add("§7Quantia: §a" + LojaAPI.itemQuantia("escada", type));
            lore20.add(" ");
            lore20.add(" ");
            lore20.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore20.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            ladderMeta.setLore(lore20);
        } else if (LojaAPI.valorCompra("escada", type) != 0) {
            lore20.add(" ");
            lore20.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("escada", type)));
            lore20.add("§7Quantia: §a" + LojaAPI.itemQuantia("escada", type));
            lore20.add(" ");
            lore20.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore20.add(" ");
            ladderMeta.setLore(lore20);
        } else if (LojaAPI.valorVenda("escada", type) != 0) {
            lore20.add(" ");
            lore20.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("escada", type)));
            lore20.add("§7Quantia: §a" + LojaAPI.itemQuantia("escada", type));
            lore20.add(" ");
            lore20.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore20.add(" ");
            ladderMeta.setLore(lore20);
        }
        ladder.setItemMeta(ladderMeta);
        INVENTORY_LOJAVARIOS.setItem(33, ladder);


        final ItemStack snow = new ItemStack(Material.SNOW);
        final ItemMeta snowMeta = snow.getItemMeta();
        snowMeta.setDisplayName("§eNeve");
        final ArrayList<String> lore19 = new ArrayList<String>();
        if (LojaAPI.valorVenda("neve", type) != 0 && LojaAPI.valorCompra("neve", type) != 0) {
            lore19.add(" ");
            lore19.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("neve", type)));
            lore19.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("neve", type)));
            lore19.add("§7Quantia: §a" + LojaAPI.itemQuantia("neve", type));
            lore19.add(" ");
            lore19.add(" ");
            lore19.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore19.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            snowMeta.setLore(lore19);
        } else if (LojaAPI.valorCompra("neve", type) != 0) {
            lore19.add(" ");
            lore19.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("neve", type)));
            lore19.add("§7Quantia: §a" + LojaAPI.itemQuantia("neve", type));
            lore19.add(" ");
            lore19.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore19.add(" ");
            snowMeta.setLore(lore19);
        } else if (LojaAPI.valorVenda("neve", type) != 0) {
            lore19.add(" ");
            lore19.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("neve", type)));
            lore19.add("§7Quantia: §a" + LojaAPI.itemQuantia("neve", type));
            lore19.add(" ");
            lore19.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore19.add(" ");
            snowMeta.setLore(lore19);
        }
        snow.setItemMeta(snowMeta);
        INVENTORY_LOJAVARIOS.setItem(32, snow);


        final ItemStack snowblock = new ItemStack(Material.SNOW_BLOCK);
        final ItemMeta snowblockMeta = snowblock.getItemMeta();
        snowblockMeta.setDisplayName("§eBloco de neve");
        final ArrayList<String> lore18 = new ArrayList<String>();
        if (LojaAPI.valorVenda("bloco_de_neve", type) != 0 && LojaAPI.valorCompra("bloco_de_neve", type) != 0) {
            lore18.add(" ");
            lore18.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bloco_de_neve", type)));
            lore18.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bloco_de_neve", type)));
            lore18.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_neve", type));
            lore18.add(" ");
            lore18.add(" ");
            lore18.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore18.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            snowblockMeta.setLore(lore18);
        } else if (LojaAPI.valorCompra("bloco_de_neve", type) != 0) {
            lore18.add(" ");
            lore18.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bloco_de_neve", type)));
            lore18.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_neve", type));
            lore18.add(" ");
            lore18.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore18.add(" ");
            snowblockMeta.setLore(lore18);
        } else if (LojaAPI.valorVenda("bloco_de_neve", type) != 0) {
            lore18.add(" ");
            lore18.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bloco_de_neve", type)));
            lore18.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_neve", type));
            lore18.add(" ");
            lore18.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore18.add(" ");
            snowblockMeta.setLore(lore18);
        }
        snowblock.setItemMeta(snowblockMeta);
        INVENTORY_LOJAVARIOS.setItem(31, snowblock);


        final ItemStack tesoura = new ItemStack(Material.SHEARS);
        final ItemMeta tesouraMeta = tesoura.getItemMeta();
        tesouraMeta.setDisplayName("§eTesoura");
        final ArrayList<String> lore17 = new ArrayList<String>();
        if (LojaAPI.valorVenda("tesoura", type) != 0 && LojaAPI.valorCompra("tesoura", type) != 0) {
            lore17.add(" ");
            lore17.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tesoura", type)));
            lore17.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tesoura", type)));
            lore17.add("§7Quantia: §a" + LojaAPI.itemQuantia("tesoura", type));
            lore17.add(" ");
            lore17.add(" ");
            lore17.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore17.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            tesouraMeta.setLore(lore17);
        } else if (LojaAPI.valorCompra("tesoura", type) != 0) {
            lore17.add(" ");
            lore17.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tesoura", type)));
            lore17.add("§7Quantia: §a" + LojaAPI.itemQuantia("tesoura", type));
            lore17.add(" ");
            lore17.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore17.add(" ");
            tesouraMeta.setLore(lore17);
        } else if (LojaAPI.valorVenda("tesoura", type) != 0) {
            lore17.add(" ");
            lore17.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tesoura", type)));
            lore17.add("§7Quantia: §a" + LojaAPI.itemQuantia("tesoura", type));
            lore17.add(" ");
            lore17.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore17.add(" ");
            tesouraMeta.setLore(lore17);
        }
        tesoura.setItemMeta(tesouraMeta);
        INVENTORY_LOJAVARIOS.setItem(30, tesoura);


        final ItemStack armorstand = new ItemStack(Material.ARMOR_STAND);
        final ItemMeta armorstandMeta = armorstand.getItemMeta();
        armorstandMeta.setDisplayName("§eSuporte de armaduras");
        final ArrayList<String> lore16 = new ArrayList<String>();
        if (LojaAPI.valorVenda("suporte_de_armaduras", type) != 0 && LojaAPI.valorCompra("suporte_de_armaduras", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("suporte_de_armaduras", type)));
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("suporte_de_armaduras", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("suporte_de_armaduras", type));
            lore16.add(" ");
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            armorstandMeta.setLore(lore16);
        } else if (LojaAPI.valorCompra("suporte_de_armaduras", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("suporte_de_armaduras", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("suporte_de_armaduras", type));
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add(" ");
            armorstandMeta.setLore(lore16);
        } else if (LojaAPI.valorVenda("suporte_de_armaduras", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("suporte_de_armaduras", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("suporte_de_armaduras", type));
            lore16.add(" ");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore16.add(" ");
            armorstandMeta.setLore(lore16);
        }
        armorstand.setItemMeta(armorstandMeta);
        INVENTORY_LOJAVARIOS.setItem(29, armorstand);


        final ItemStack banner = new ItemStack(Material.WHITE_BANNER);
        final ItemMeta bannerMeta = banner.getItemMeta();
        bannerMeta.setDisplayName("§eEstandarte branco");
        final ArrayList<String> lore15 = new ArrayList<String>();
        if (LojaAPI.valorVenda("estandarte_branco", type) != 0 && LojaAPI.valorCompra("estandarte_branco", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("estandarte_branco", type)));
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("estandarte_branco", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("estandarte_branco", type));
            lore15.add(" ");
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            bannerMeta.setLore(lore15);
        } else if (LojaAPI.valorCompra("estandarte_branco", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("estandarte_branco", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("estandarte_branco", type));
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add(" ");
            bannerMeta.setLore(lore15);
        } else if (LojaAPI.valorVenda("estandarte_branco", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("estandarte_branco", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("estandarte_branco", type));
            lore15.add(" ");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore15.add(" ");
            bannerMeta.setLore(lore15);
        }
        banner.setItemMeta(bannerMeta);
        INVENTORY_LOJAVARIOS.setItem(28, banner);


        final ItemStack etiqueta = new ItemStack(Material.NAME_TAG);
        final ItemMeta etiquetaMeta = etiqueta.getItemMeta();
        etiquetaMeta.setDisplayName("§eEtiqueta");
        final ArrayList<String> lore14 = new ArrayList<String>();
        if (LojaAPI.valorVenda("etiqueta", type) != 0 && LojaAPI.valorCompra("etiqueta", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("etiqueta", type)));
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("etiqueta", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("etiqueta", type));
            lore14.add(" ");
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            etiquetaMeta.setLore(lore14);
        } else if (LojaAPI.valorCompra("etiqueta", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("etiqueta", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("etiqueta", type));
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add(" ");
            etiquetaMeta.setLore(lore14);
        } else if (LojaAPI.valorVenda("etiqueta", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("etiqueta", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("etiqueta", type));
            lore14.add(" ");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore14.add(" ");
            etiquetaMeta.setLore(lore14);
        }
        etiqueta.setItemMeta(etiquetaMeta);
        INVENTORY_LOJAVARIOS.setItem(25, etiqueta);


        final ItemStack laço = new ItemStack(Material.LEAD);
        final ItemMeta laçoMeta = laço.getItemMeta();
        laçoMeta.setDisplayName("§eLaço");
        final ArrayList<String> lore13 = new ArrayList<String>();
        if (LojaAPI.valorVenda("laço", type) != 0 && LojaAPI.valorCompra("laço", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("laço", type)));
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("laço", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("laço", type));
            lore13.add(" ");
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            laçoMeta.setLore(lore13);
        } else if (LojaAPI.valorCompra("laço", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("laço", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("laço", type));
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add(" ");
            laçoMeta.setLore(lore13);
        } else if (LojaAPI.valorVenda("laço", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("laço", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("laço", type));
            lore13.add(" ");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore13.add(" ");
            laçoMeta.setLore(lore13);
        }
        laço.setItemMeta(laçoMeta);
        INVENTORY_LOJAVARIOS.setItem(24, laço);


        final ItemStack dragaoskull = new ItemStack(Material.DRAGON_HEAD);
        final ItemMeta dragaoskullMeta = dragaoskull.getItemMeta();
        dragaoskullMeta.setDisplayName("§eCabeça de dragão");
        final ArrayList<String> lore12 = new ArrayList<String>();
        if (LojaAPI.valorVenda("cabeça_de_dragao", type) != 0 && LojaAPI.valorCompra("cabeça_de_dragao", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("cabeça_de_dragao", type)));
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("cabeça_de_dragao", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("cabeça_de_dragao", type));
            lore12.add(" ");
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            dragaoskullMeta.setLore(lore12);
        } else if (LojaAPI.valorCompra("cabeça_de_dragao", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("cabeça_de_dragao", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("cabeça_de_dragao", type));
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add(" ");
            dragaoskullMeta.setLore(lore12);
        } else if (LojaAPI.valorVenda("cabeça_de_dragao", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("cabeça_de_dragao", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("cabeça_de_dragao", type));
            lore12.add(" ");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore12.add(" ");
            dragaoskullMeta.setLore(lore12);
        }
        dragaoskull.setItemMeta(dragaoskullMeta);
        INVENTORY_LOJAVARIOS.setItem(23, dragaoskull);


        final ItemStack creeperskull = new ItemStack(Material.CREEPER_HEAD);
        final ItemMeta creeperskullMeta = creeperskull.getItemMeta();
        creeperskullMeta.setDisplayName("§eCabeça de creeper");
        final ArrayList<String> lore11 = new ArrayList<String>();
        if (LojaAPI.valorVenda("cabeça_de_creeper", type) != 0 && LojaAPI.valorCompra("cabeça_de_creeper", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("cabeça_de_creeper", type)));
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("cabeça_de_creeper", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("cabeça_de_creeper", type));
            lore11.add(" ");
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            creeperskullMeta.setLore(lore11);
        } else if (LojaAPI.valorCompra("cabeça_de_creeper", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("cabeça_de_creeper", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("cabeça_de_creeper", type));
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add(" ");
            creeperskullMeta.setLore(lore11);
        } else if (LojaAPI.valorVenda("cabeça_de_creeper", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("cabeça_de_creeper", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("cabeça_de_creeper", type));
            lore11.add(" ");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore11.add(" ");
            creeperskullMeta.setLore(lore11);
        }
        creeperskull.setItemMeta(creeperskullMeta);
        INVENTORY_LOJAVARIOS.setItem(22, creeperskull);


        final ItemStack zumbiskull = new ItemStack(Material.ZOMBIE_HEAD);
        final ItemMeta zumbiskullMeta = zumbiskull.getItemMeta();
        zumbiskullMeta.setDisplayName("§eCabeça de zumbi");
        final ArrayList<String> lore10 = new ArrayList<String>();
        if (LojaAPI.valorVenda("cabeça_de_zumbi", type) != 0 && LojaAPI.valorCompra("cabeça_de_zumbi", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("cabeça_de_zumbi", type)));
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("cabeça_de_zumbi", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("cabeça_de_zumbi", type));
            lore10.add(" ");
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            zumbiskullMeta.setLore(lore10);
        } else if (LojaAPI.valorCompra("cabeça_de_zumbi", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("cabeça_de_zumbi", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("cabeça_de_zumbi", type));
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add(" ");
            zumbiskullMeta.setLore(lore10);
        } else if (LojaAPI.valorVenda("cabeça_de_zumbi", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("cabeça_de_zumbi", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("cabeça_de_zumbi", type));
            lore10.add(" ");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore10.add(" ");
            zumbiskullMeta.setLore(lore10);
        }
        zumbiskull.setItemMeta(zumbiskullMeta);
        INVENTORY_LOJAVARIOS.setItem(21, zumbiskull);


        final ItemStack skeletonwitherskull = new ItemStack(Material.WITHER_SKELETON_SKULL);
        final ItemMeta skeletonwitherskullMeta = skeletonwitherskull.getItemMeta();
        skeletonwitherskullMeta.setDisplayName("§eCrânio de esqueleto Wither");
        final ArrayList<String> lore9 = new ArrayList<String>();
        if (LojaAPI.valorVenda("cranio_de_esqueleto_whiter_whiter", type) != 0 && LojaAPI.valorCompra("cranio_de_esqueleto_whiter", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("cranio_de_esqueleto_whiter", type)));
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("cranio_de_esqueleto_whiter", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("cranio_de_esqueleto_whiter", type));
            lore9.add(" ");
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            skeletonwitherskullMeta.setLore(lore9);
        } else if (LojaAPI.valorCompra("cranio_de_esqueleto_whiter", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("cranio_de_esqueleto_whiter", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("cranio_de_esqueleto_whiter", type));
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add(" ");
            skeletonwitherskullMeta.setLore(lore9);
        } else if (LojaAPI.valorVenda("cranio_de_esqueleto_whiter", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("cranio_de_esqueleto_whiter", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("cranio_de_esqueleto_whiter", type));
            lore9.add(" ");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore9.add(" ");
            skeletonwitherskullMeta.setLore(lore9);
        }
        skeletonwitherskull.setItemMeta(skeletonwitherskullMeta);
        INVENTORY_LOJAVARIOS.setItem(20, skeletonwitherskull);


        final ItemStack skeletonskull = new ItemStack(Material.SKELETON_SKULL);
        final ItemMeta skeletonskullMeta = skeletonskull.getItemMeta();
        skeletonskullMeta.setDisplayName("§eCrânio de esqueleto");
        final ArrayList<String> lore8 = new ArrayList<String>();
        if (LojaAPI.valorVenda("cranio_de_esqueleto", type) != 0 && LojaAPI.valorCompra("cranio_de_esqueleto", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("cranio_de_esqueleto", type)));
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("cranio_de_esqueleto", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("cranio_de_esqueleto", type));
            lore8.add(" ");
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            skeletonskullMeta.setLore(lore8);
        } else if (LojaAPI.valorCompra("cranio_de_esqueleto", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("cranio_de_esqueleto", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("cranio_de_esqueleto", type));
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add(" ");
            skeletonskullMeta.setLore(lore8);
        } else if (LojaAPI.valorVenda("cranio_de_esqueleto", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("cranio_de_esqueleto", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("cranio_de_esqueleto", type));
            lore8.add(" ");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore8.add(" ");
            skeletonskullMeta.setLore(lore8);
        }
        skeletonskull.setItemMeta(skeletonskullMeta);
        INVENTORY_LOJAVARIOS.setItem(19, skeletonskull);


        final ItemStack map = new ItemStack(Material.MAP);
        final ItemMeta mapMeta = map.getItemMeta();
        mapMeta.setDisplayName("§eMapa em branco");
        final ArrayList<String> lore7 = new ArrayList<String>();
        if (LojaAPI.valorVenda("mapa_em_branco", type) != 0 && LojaAPI.valorCompra("mapa_em_branco", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("mapa_em_branco", type)));
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("mapa_em_branco", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("mapa_em_branco", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            mapMeta.setLore(lore7);
        } else if (LojaAPI.valorCompra("mapa_em_branco", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("mapa_em_branco", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("mapa_em_branco", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add(" ");
            mapMeta.setLore(lore7);
        } else if (LojaAPI.valorVenda("mapa_em_branco", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("mapa_em_branco", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("mapa_em_branco", type));
            lore7.add(" ");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore7.add(" ");
            mapMeta.setLore(lore7);
        }
        map.setItemMeta(mapMeta);
        INVENTORY_LOJAVARIOS.setItem(16, map);


        final ItemStack clock = new ItemStack(Material.CLOCK);
        final ItemMeta clockMeta = clock.getItemMeta();
        clockMeta.setDisplayName("§eRelógio");
        final ArrayList<String> lore6 = new ArrayList<String>();
        if (LojaAPI.valorVenda("relogio", type) != 0 && LojaAPI.valorCompra("relogio", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("relogio", type)));
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("relogio", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("relogio", type));
            lore6.add(" ");
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            clockMeta.setLore(lore6);
        } else if (LojaAPI.valorCompra("relogio", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("relogio", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("relogio", type));
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add(" ");
            clockMeta.setLore(lore6);
        } else if (LojaAPI.valorVenda("relogio", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("relogio", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("relogio", type));
            lore6.add(" ");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore6.add(" ");
            clockMeta.setLore(lore6);
        }
        clock.setItemMeta(clockMeta);
        INVENTORY_LOJAVARIOS.setItem(15, clock);


        final ItemStack compass = new ItemStack(Material.COMPASS);
        final ItemMeta compassMeta = compass.getItemMeta();
        compassMeta.setDisplayName("§eBússola");
        final ArrayList<String> lore5 = new ArrayList<String>();
        if (LojaAPI.valorVenda("bussola", type) != 0 && LojaAPI.valorCompra("bussola", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bussola", type)));
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bussola", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("bussola", type));
            lore5.add(" ");
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            compassMeta.setLore(lore5);
        } else if (LojaAPI.valorCompra("bussola", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bussola", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("bussola", type));
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add(" ");
            compassMeta.setLore(lore5);
        } else if (LojaAPI.valorVenda("bussola", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bussola", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("bussola", type));
            lore5.add(" ");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore5.add(" ");
            compassMeta.setLore(lore5);
        }
        compass.setItemMeta(compassMeta);
        INVENTORY_LOJAVARIOS.setItem(14, compass);


        final ItemStack blueice = new ItemStack(Material.BLUE_ICE);
        final ItemMeta blueiceMeta = blueice.getItemMeta();
        blueiceMeta.setDisplayName("§eGelo azul");
        final ArrayList<String> lore4 = new ArrayList<String>();
        if (LojaAPI.valorVenda("gelo_azul", type) != 0 && LojaAPI.valorCompra("gelo_azul", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("gelo_azul", type)));
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("gelo_azul", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("gelo_azul", type));
            lore4.add(" ");
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            blueiceMeta.setLore(lore4);
        } else if (LojaAPI.valorCompra("gelo_azul", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("gelo_azul", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("gelo_azul", type));
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add(" ");
            blueiceMeta.setLore(lore4);
        } else if (LojaAPI.valorVenda("gelo_azul", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("gelo_azul", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("gelo_azul", type));
            lore4.add(" ");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore4.add(" ");
            blueiceMeta.setLore(lore4);
        }
        blueice.setItemMeta(blueiceMeta);
        INVENTORY_LOJAVARIOS.setItem(13, blueice);


        final ItemStack packetice = new ItemStack(Material.PACKED_ICE);
        final ItemMeta packeticeMeta = packetice.getItemMeta();
        packeticeMeta.setDisplayName("§eGelo compacto");
        final ArrayList<String> lore3 = new ArrayList<String>();
        if (LojaAPI.valorVenda("gelo_compacto", type) != 0 && LojaAPI.valorCompra("gelo_compacto", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("gelo_compacto", type)));
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("gelo_compacto", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("gelo_compacto", type));
            lore3.add(" ");
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            packeticeMeta.setLore(lore3);
        } else if (LojaAPI.valorCompra("gelo_compacto", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("gelo_compacto", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("gelo_compacto", type));
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add(" ");
            packeticeMeta.setLore(lore3);
        } else if (LojaAPI.valorVenda("gelo_compacto", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("gelo_compacto", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("gelo_compacto", type));
            lore3.add(" ");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore3.add(" ");
            packeticeMeta.setLore(lore3);
        }
        packetice.setItemMeta(packeticeMeta);
        INVENTORY_LOJAVARIOS.setItem(12, packetice);


        final ItemStack ice = new ItemStack(Material.ICE);
        final ItemMeta iceMeta = ice.getItemMeta();
        iceMeta.setDisplayName("§eGelo");
        final ArrayList<String> lore2 = new ArrayList<String>();
        if (LojaAPI.valorVenda("gelo", type) != 0 && LojaAPI.valorCompra("gelo", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("gelo", type)));
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("gelo", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("gelo", type));
            lore2.add(" ");
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            iceMeta.setLore(lore2);
        } else if (LojaAPI.valorCompra("gelo", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("gelo", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("gelo", type));
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add(" ");
            iceMeta.setLore(lore2);
        } else if (LojaAPI.valorVenda("gelo", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("gelo", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("gelo", type));
            lore2.add(" ");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore2.add(" ");
            iceMeta.setLore(lore2);
        }
        ice.setItemMeta(iceMeta);
        INVENTORY_LOJAVARIOS.setItem(11, ice);


        final ItemStack shulkerbox = new ItemStack(Material.PURPLE_SHULKER_BOX);
        final ItemMeta shulkerboxMeta = shulkerbox.getItemMeta();
        shulkerboxMeta.setDisplayName("§eCaixa de shulker");
        final ArrayList<String> lore1 = new ArrayList<String>();
        if (LojaAPI.valorVenda("caixa_de_shulker", type) != 0 && LojaAPI.valorCompra("caixa_de_shulker", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("caixa_de_shulker", type)));
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("caixa_de_shulker", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("caixa_de_shulker", type));
            lore1.add(" ");
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            shulkerboxMeta.setLore(lore1);
        } else if (LojaAPI.valorCompra("caixa_de_shulker", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("caixa_de_shulker", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("caixa_de_shulker", type));
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add(" ");
            shulkerboxMeta.setLore(lore1);
        } else if (LojaAPI.valorVenda("caixa_de_shulker", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("caixa_de_shulker", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("caixa_de_shulker", type));
            lore1.add(" ");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore1.add(" ");
            shulkerboxMeta.setLore(lore1);
        }
        shulkerbox.setItemMeta(shulkerboxMeta);
        INVENTORY_LOJAVARIOS.setItem(10, shulkerbox);

        return INVENTORY_LOJAVARIOS;
    }

    public static Inventory buildLojaAlquimia(final String type) {
        final Inventory INVENTORY_LOJAALQUIMIA = Bukkit.createInventory(null, 5 * 9, "Alquimia: ");

        final ItemStack estante = new ItemStack(Material.BOOKSHELF);
        final ItemMeta estanteMeta = estante.getItemMeta();
        estanteMeta.setDisplayName("§eEstante");
        final ArrayList<String> lore17 = new ArrayList<String>();
        if (LojaAPI.valorVenda("estante", type) != 0 && LojaAPI.valorCompra("estante", type) != 0) {
            lore17.add(" ");
            lore17.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("estante", type)));
            lore17.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("estante", type)));
            lore17.add("§7Quantia: §a" + LojaAPI.itemQuantia("estante", type));
            lore17.add(" ");
            lore17.add(" ");
            lore17.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore17.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            estanteMeta.setLore(lore17);
        } else if (LojaAPI.valorCompra("estante", type) != 0) {
            lore17.add(" ");
            lore17.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("estante", type)));
            lore17.add("§7Quantia: §a" + LojaAPI.itemQuantia("estante", type));
            lore17.add(" ");
            lore17.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore17.add(" ");
            estanteMeta.setLore(lore17);
        } else if (LojaAPI.valorVenda("estante", type) != 0) {
            lore17.add(" ");
            lore17.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("estante", type)));
            lore17.add("§7Quantia: §a" + LojaAPI.itemQuantia("estante", type));
            lore17.add(" ");
            lore17.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore17.add(" ");
            estanteMeta.setLore(lore17);
        }
        estante.setItemMeta(estanteMeta);
        INVENTORY_LOJAALQUIMIA.setItem(32, estante);


        final ItemStack anvil = new ItemStack(Material.ANVIL);
        final ItemMeta anvilMeta = anvil.getItemMeta();
        anvilMeta.setDisplayName("§eBigorna");
        final ArrayList<String> lore16 = new ArrayList<String>();
        if (LojaAPI.valorVenda("bigorna", type) != 0 && LojaAPI.valorCompra("bigorna", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bigorna", type)));
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bigorna", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("bigorna", type));
            lore16.add(" ");
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            anvilMeta.setLore(lore16);
        } else if (LojaAPI.valorCompra("bigorna", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bigorna", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("bigorna", type));
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add(" ");
            anvilMeta.setLore(lore16);
        } else if (LojaAPI.valorVenda("bigorna", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bigorna", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("bigorna", type));
            lore16.add(" ");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore16.add(" ");
            anvilMeta.setLore(lore16);
        }
        anvil.setItemMeta(anvilMeta);
        INVENTORY_LOJAALQUIMIA.setItem(31, anvil);


        final ItemStack enchantmentable = new ItemStack(Material.ENCHANTING_TABLE);
        final ItemMeta enchantmentableMeta = enchantmentable.getItemMeta();
        enchantmentableMeta.setDisplayName("§eMesa de encantamentos");
        final ArrayList<String> lore15 = new ArrayList<String>();
        if (LojaAPI.valorVenda("mesa_de_encantamentos", type) != 0 && LojaAPI.valorCompra("mesa_de_encantamentos", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("mesa_de_encantamentos", type)));
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("mesa_de_encantamentos", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("mesa_de_encantamentos", type));
            lore15.add(" ");
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            enchantmentableMeta.setLore(lore15);
        } else if (LojaAPI.valorCompra("mesa_de_encantamentos", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("mesa_de_encantamentos", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("mesa_de_encantamentos", type));
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add(" ");
            enchantmentableMeta.setLore(lore15);
        } else if (LojaAPI.valorVenda("mesa_de_encantamentos", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("mesa_de_encantamentos", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("mesa_de_encantamentos", type));
            lore15.add(" ");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore15.add(" ");
            enchantmentableMeta.setLore(lore15);
        }
        enchantmentable.setItemMeta(enchantmentableMeta);
        INVENTORY_LOJAALQUIMIA.setItem(30, enchantmentable);


        final ItemStack bafodragao = new ItemStack(Material.DRAGON_BREATH);
        final ItemMeta bafodragaoMeta = bafodragao.getItemMeta();
        bafodragaoMeta.setDisplayName("§eBafo de dragão");
        final ArrayList<String> lore14 = new ArrayList<String>();
        if (LojaAPI.valorVenda("bafo_de_dragao", type) != 0 && LojaAPI.valorCompra("bafo_de_dragao", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bafo_de_dragao", type)));
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bafo_de_dragao", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("bafo_de_dragao", type));
            lore14.add(" ");
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            bafodragaoMeta.setLore(lore14);
        } else if (LojaAPI.valorCompra("bafo_de_dragao", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bafo_de_dragao", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("bafo_de_dragao", type));
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add(" ");
            bafodragaoMeta.setLore(lore14);
        } else if (LojaAPI.valorVenda("bafo_de_dragao", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bafo_de_dragao", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("bafo_de_dragao", type));
            lore14.add(" ");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore14.add(" ");
            bafodragaoMeta.setLore(lore14);
        }
        bafodragao.setItemMeta(bafodragaoMeta);
        INVENTORY_LOJAALQUIMIA.setItem(25, bafodragao);


        final ItemStack membranaphantom = new ItemStack(Material.PHANTOM_MEMBRANE);
        final ItemMeta membranaphantomMeta = membranaphantom.getItemMeta();
        membranaphantomMeta.setDisplayName("§eMembrana de phantom");
        final ArrayList<String> lore13 = new ArrayList<String>();
        if (LojaAPI.valorVenda("membrana_de_phantom", type) != 0 && LojaAPI.valorCompra("membrana_de_phantom", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("membrana_de_phantom", type)));
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("membrana_de_phantom", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("membrana_de_phantom", type));
            lore13.add(" ");
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            membranaphantomMeta.setLore(lore13);
        } else if (LojaAPI.valorCompra("membrana_de_phantom", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("membrana_de_phantom", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("membrana_de_phantom", type));
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add(" ");
            membranaphantomMeta.setLore(lore13);
        } else if (LojaAPI.valorVenda("membrana_de_phantom", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("membrana_de_phantom", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("membrana_de_phantom", type));
            lore13.add(" ");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore13.add(" ");
            membranaphantomMeta.setLore(lore13);
        }
        membranaphantom.setItemMeta(membranaphantomMeta);
        INVENTORY_LOJAALQUIMIA.setItem(24, membranaphantom);


        final ItemStack frascovidro = new ItemStack(Material.GLASS_BOTTLE);
        final ItemMeta frascovidroMeta = frascovidro.getItemMeta();
        frascovidroMeta.setDisplayName("§eFrasco de vidro");
        final ArrayList<String> lore12 = new ArrayList<String>();
        if (LojaAPI.valorVenda("frasco_de_vidro", type) != 0 && LojaAPI.valorCompra("frasco_de_vidro", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("frasco_de_vidro", type)));
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("frasco_de_vidro", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("frasco_de_vidro", type));
            lore12.add(" ");
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            frascovidroMeta.setLore(lore12);
        } else if (LojaAPI.valorCompra("frasco_de_vidro", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("frasco_de_vidro", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("frasco_de_vidro", type));
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add(" ");
            frascovidroMeta.setLore(lore12);
        } else if (LojaAPI.valorVenda("frasco_de_vidro", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("frasco_de_vidro", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("frasco_de_vidro", type));
            lore12.add(" ");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore12.add(" ");
            frascovidroMeta.setLore(lore12);
        }
        frascovidro.setItemMeta(frascovidroMeta);
        INVENTORY_LOJAALQUIMIA.setItem(23, frascovidro);


        final ItemStack pecoelho = new ItemStack(Material.RABBIT_FOOT);
        final ItemMeta pecoelhoMeta = pecoelho.getItemMeta();
        pecoelhoMeta.setDisplayName("§ePé de coelho");
        final ArrayList<String> lore11 = new ArrayList<String>();
        if (LojaAPI.valorVenda("pe_de_coelho", type) != 0 && LojaAPI.valorCompra("pe_de_coelho", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pe_de_coelho", type)));
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pe_de_coelho", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("pe_de_coelho", type));
            lore11.add(" ");
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            pecoelhoMeta.setLore(lore11);
        } else if (LojaAPI.valorCompra("pe_de_coelho", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pe_de_coelho", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("pe_de_coelho", type));
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add(" ");
            pecoelhoMeta.setLore(lore11);
        } else if (LojaAPI.valorVenda("pe_de_coelho", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pe_de_coelho", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("pe_de_coelho", type));
            lore11.add(" ");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore11.add(" ");
            pecoelhoMeta.setLore(lore11);
        }
        pecoelho.setItemMeta(pecoelhoMeta);
        INVENTORY_LOJAALQUIMIA.setItem(22, pecoelho);


        final ItemStack lagrimaghast = new ItemStack(Material.GHAST_TEAR);
        final ItemMeta lagrimaghastMeta = lagrimaghast.getItemMeta();
        lagrimaghastMeta.setDisplayName("§eLágrima de ghast");
        final ArrayList<String> lore10 = new ArrayList<String>();
        if (LojaAPI.valorVenda("lagrima_de_ghast", type) != 0 && LojaAPI.valorCompra("lagrima_de_ghast", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("lagrima_de_ghast", type)));
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("lagrima_de_ghast", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("lagrima_de_ghast", type));
            lore10.add(" ");
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lagrimaghastMeta.setLore(lore10);
        } else if (LojaAPI.valorCompra("lagrima_de_ghast", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("lagrima_de_ghast", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("lagrima_de_ghast", type));
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add(" ");
            lagrimaghastMeta.setLore(lore10);
        } else if (LojaAPI.valorVenda("lagrima_de_ghast", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("lagrima_de_ghast", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("lagrima_de_ghast", type));
            lore10.add(" ");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore10.add(" ");
            lagrimaghastMeta.setLore(lore10);
        }
        lagrimaghast.setItemMeta(lagrimaghastMeta);
        INVENTORY_LOJAALQUIMIA.setItem(21, lagrimaghast);


        final ItemStack spidereye = new ItemStack(Material.SPIDER_EYE);
        final ItemMeta spidereyeMeta = spidereye.getItemMeta();
        spidereyeMeta.setDisplayName("§eOlho de aranha");
        final ArrayList<String> lore9 = new ArrayList<String>();
        if (LojaAPI.valorVenda("olho_de_aranha", type) != 0 && LojaAPI.valorCompra("olho_de_aranha", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("olho_de_aranha", type)));
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("olho_de_aranha", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("olho_de_aranha", type));
            lore9.add(" ");
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            spidereyeMeta.setLore(lore9);
        } else if (LojaAPI.valorCompra("olho_de_aranha", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("olho_de_aranha", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("olho_de_aranha", type));
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add(" ");
            spidereyeMeta.setLore(lore9);
        } else if (LojaAPI.valorVenda("olho_de_aranha", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("olho_de_aranha", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("olho_de_aranha", type));
            lore9.add(" ");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore9.add(" ");
            spidereyeMeta.setLore(lore9);
        }
        spidereye.setItemMeta(spidereyeMeta);
        INVENTORY_LOJAALQUIMIA.setItem(20, spidereye);


        final ItemStack suportpoçoes = new ItemStack(Material.BREWING_STAND);
        final ItemMeta suportpoçoesMeta = suportpoçoes.getItemMeta();
        suportpoçoesMeta.setDisplayName("§eSuporte de poções");
        final ArrayList<String> lore8 = new ArrayList<String>();
        if (LojaAPI.valorVenda("suporte_de_poçoes", type) != 0 && LojaAPI.valorCompra("suporte_de_poçoes", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("suporte_de_poçoes", type)));
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("suporte_de_poçoes", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("suporte_de_poçoes", type));
            lore8.add(" ");
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            suportpoçoesMeta.setLore(lore8);
        } else if (LojaAPI.valorCompra("suporte_de_poçoes", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("suporte_de_poçoes", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("suporte_de_poçoes", type));
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add(" ");
            suportpoçoesMeta.setLore(lore8);
        } else if (LojaAPI.valorVenda("suporte_de_poçoes", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("suporte_de_poçoes", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("suporte_de_poçoes", type));
            lore8.add(" ");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore8.add(" ");
            suportpoçoesMeta.setLore(lore8);
        }
        suportpoçoes.setItemMeta(suportpoçoesMeta);
        INVENTORY_LOJAALQUIMIA.setItem(19, suportpoçoes);


        final ItemStack glowstonedust = new ItemStack(Material.GLOWSTONE_DUST);
        final ItemMeta glowstonedustMeta = glowstonedust.getItemMeta();
        glowstonedustMeta.setDisplayName("§ePó de pedra luminosa");
        final ArrayList<String> lore7 = new ArrayList<String>();
        if (LojaAPI.valorVenda("po_de_pedra_luminosa", type) != 0 && LojaAPI.valorCompra("po_de_pedra_luminosa", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_pedra_luminosa", type)));
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_pedra_luminosa", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_pedra_luminosa", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            glowstonedustMeta.setLore(lore7);
        } else if (LojaAPI.valorCompra("po_de_pedra_luminosa", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_pedra_luminosa", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_pedra_luminosa", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add(" ");
            glowstonedustMeta.setLore(lore7);
        } else if (LojaAPI.valorVenda("po_de_pedra_luminosa", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_pedra_luminosa", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_pedra_luminosa", type));
            lore7.add(" ");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore7.add(" ");
            glowstonedustMeta.setLore(lore7);
        }
        glowstonedust.setItemMeta(glowstonedustMeta);
        INVENTORY_LOJAALQUIMIA.setItem(16, glowstonedust);


        final ItemStack sugar = new ItemStack(Material.SUGAR);
        final ItemMeta sugarMeta = sugar.getItemMeta();
        sugarMeta.setDisplayName("§eAçúcar");
        final ArrayList<String> lore6 = new ArrayList<String>();
        if (LojaAPI.valorVenda("açucar", type) != 0 && LojaAPI.valorCompra("açucar", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("açucar", type)));
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("açucar", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("açucar", type));
            lore6.add(" ");
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            sugarMeta.setLore(lore6);
        } else if (LojaAPI.valorCompra("açucar", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("açucar", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("açucar", type));
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add(" ");
            sugarMeta.setLore(lore6);
        } else if (LojaAPI.valorVenda("açucar", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("açucar", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("açucar", type));
            lore6.add(" ");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore6.add(" ");
            sugarMeta.setLore(lore6);
        }
        sugar.setItemMeta(sugarMeta);
        INVENTORY_LOJAALQUIMIA.setItem(15, sugar);


        final ItemStack blazepowder = new ItemStack(Material.BLAZE_POWDER);
        final ItemMeta blazepowderMeta = blazepowder.getItemMeta();
        blazepowderMeta.setDisplayName("§ePó de blaze");
        final ArrayList<String> lore5 = new ArrayList<String>();
        if (LojaAPI.valorVenda("po_de_blaze", type) != 0 && LojaAPI.valorCompra("po_de_blaze", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_blaze", type)));
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_blaze", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_blaze", type));
            lore5.add(" ");
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            blazepowderMeta.setLore(lore5);
        } else if (LojaAPI.valorCompra("po_de_blaze", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_blaze", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_blaze", type));
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add(" ");
            blazepowderMeta.setLore(lore5);
        } else if (LojaAPI.valorVenda("po_de_blaze", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_blaze", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_blaze", type));
            lore5.add(" ");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore5.add(" ");
            blazepowderMeta.setLore(lore5);
        }
        blazepowder.setItemMeta(blazepowderMeta);
        INVENTORY_LOJAALQUIMIA.setItem(14, blazepowder);


        final ItemStack melonreluzente = new ItemStack(Material.GLISTERING_MELON_SLICE);
        final ItemMeta melonreluzenteMeta = melonreluzente.getItemMeta();
        melonreluzenteMeta.setDisplayName("§eFatia de melancia reluzente");
        final ArrayList<String> lore4 = new ArrayList<String>();
        if (LojaAPI.valorVenda("fatia_de_melancia_reluzente", type) != 0 && LojaAPI.valorCompra("fatia_de_melancia_reluzente", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("fatia_de_melancia_reluzente", type)));
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("fatia_de_melancia_reluzente", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("fatia_de_melancia_reluzente", type));
            lore4.add(" ");
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            melonreluzenteMeta.setLore(lore4);
        } else if (LojaAPI.valorCompra("fatia_de_melancia_reluzente", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("fatia_de_melancia_reluzente", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("fatia_de_melancia_reluzente", type));
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add(" ");
            melonreluzenteMeta.setLore(lore4);
        } else if (LojaAPI.valorVenda("fatia_de_melancia_reluzente", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("fatia_de_melancia_reluzente", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("fatia_de_melancia_reluzente", type));
            lore4.add(" ");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore4.add(" ");
            melonreluzenteMeta.setLore(lore4);
        }
        melonreluzente.setItemMeta(melonreluzenteMeta);
        INVENTORY_LOJAALQUIMIA.setItem(13, melonreluzente);


        final ItemStack magmacream = new ItemStack(Material.MAGMA_CREAM);
        final ItemMeta magmacreamMeta = magmacream.getItemMeta();
        magmacreamMeta.setDisplayName("§eCreme de magma");
        final ArrayList<String> lore3 = new ArrayList<String>();
        if (LojaAPI.valorVenda("creme_de_magma", type) != 0 && LojaAPI.valorCompra("creme_de_magma", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("creme_de_magma", type)));
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("creme_de_magma", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("creme_de_magma", type));
            lore3.add(" ");
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            magmacreamMeta.setLore(lore3);
        } else if (LojaAPI.valorCompra("creme_de_magma", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("creme_de_magma", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("creme_de_magma", type));
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add(" ");
            magmacreamMeta.setLore(lore3);
        } else if (LojaAPI.valorVenda("creme_de_magma", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("creme_de_magma", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("creme_de_magma", type));
            lore3.add(" ");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore3.add(" ");
            magmacreamMeta.setLore(lore3);
        }
        magmacream.setItemMeta(magmacreamMeta);
        INVENTORY_LOJAALQUIMIA.setItem(12, magmacream);


        final ItemStack spidereyeferm = new ItemStack(Material.FERMENTED_SPIDER_EYE);
        final ItemMeta spidereyefermMeta = spidereyeferm.getItemMeta();
        spidereyefermMeta.setDisplayName("§eOlho de aranha fermentado");
        final ArrayList<String> lore2 = new ArrayList<String>();
        if (LojaAPI.valorVenda("olho_de_aranha_fermentado", type) != 0 && LojaAPI.valorCompra("olho_de_aranha_fermentado", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("olho_de_aranha_fermentado", type)));
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("olho_de_aranha_fermentado", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("olho_de_aranha_fermentado", type));
            lore2.add(" ");
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            spidereyefermMeta.setLore(lore2);
        } else if (LojaAPI.valorCompra("olho_de_aranha_fermentado", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("olho_de_aranha_fermentado", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("olho_de_aranha_fermentado", type));
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add(" ");
            spidereyefermMeta.setLore(lore2);
        } else if (LojaAPI.valorVenda("olho_de_aranha_fermentado", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("olho_de_aranha_fermentado", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("olho_de_aranha_fermentado", type));
            lore2.add(" ");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore2.add(" ");
            spidereyefermMeta.setLore(lore2);
        }
        spidereyeferm.setItemMeta(spidereyefermMeta);
        INVENTORY_LOJAALQUIMIA.setItem(11, spidereyeferm);


        final ItemStack blazerod = new ItemStack(Material.BLAZE_ROD);
        final ItemMeta blazerodMeta = blazerod.getItemMeta();
        blazerodMeta.setDisplayName("§eVara de blaze");
        final ArrayList<String> lore1 = new ArrayList<String>();
        if (LojaAPI.valorVenda("vara_de_blaze", type) != 0 && LojaAPI.valorCompra("vara_de_blaze", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vara_de_blaze", type)));
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vara_de_blaze", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("vara_de_blaze", type));
            lore1.add(" ");
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            blazerodMeta.setLore(lore1);
        } else if (LojaAPI.valorCompra("vara_de_blaze", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vara_de_blaze", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("vara_de_blaze", type));
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add(" ");
            blazerodMeta.setLore(lore1);
        } else if (LojaAPI.valorVenda("vara_de_blaze", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vara_de_blaze", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("vara_de_blaze", type));
            lore1.add(" ");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore1.add(" ");
            blazerodMeta.setLore(lore1);
        }
        blazerod.setItemMeta(blazerodMeta);
        INVENTORY_LOJAALQUIMIA.setItem(10, blazerod);

        return INVENTORY_LOJAALQUIMIA;
    }

    public static Inventory buildLojaRedstone(final String type) {
        final Inventory INVENTORY_LOJAREDSTONE = Bukkit.createInventory(null, 4 * 9, "Redstone: ");

        final ItemStack lampadaredstone = new ItemStack(Material.REDSTONE_LAMP);
        final ItemMeta lampadaredstoneMeta = lampadaredstone.getItemMeta();
        lampadaredstoneMeta.setDisplayName("§eLâmpada de redstone");
        final ArrayList<String> lore10 = new ArrayList<String>();
        if (LojaAPI.valorVenda("lampada_de_redstone", type) != 0 && LojaAPI.valorCompra("lampada_de_redstone", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("lampada_de_redstone", type)));
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("lampada_de_redstone", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("lampada_de_redstone", type));
            lore10.add(" ");
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lampadaredstoneMeta.setLore(lore10);
        } else if (LojaAPI.valorCompra("lampada_de_redstone", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("lampada_de_redstone", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("lampada_de_redstone", type));
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add(" ");
            lampadaredstoneMeta.setLore(lore10);
        } else if (LojaAPI.valorVenda("lampada_de_redstone", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("lampada_de_redstone", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("lampada_de_redstone", type));
            lore10.add(" ");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore10.add(" ");
            lampadaredstoneMeta.setLore(lore10);
        }
        lampadaredstone.setItemMeta(lampadaredstoneMeta);
        INVENTORY_LOJAREDSTONE.setItem(23, lampadaredstone);


        final ItemStack ganchoarmadilha = new ItemStack(Material.TRIPWIRE_HOOK);
        final ItemMeta ganchoarmadilhaMeta = ganchoarmadilha.getItemMeta();
        ganchoarmadilhaMeta.setDisplayName("§eGancho de armadilha");
        final ArrayList<String> lore9 = new ArrayList<String>();
        if (LojaAPI.valorVenda("gancho_de_armadilha", type) != 0 && LojaAPI.valorCompra("gancho_de_armadilha", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("gancho_de_armadilha", type)));
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("gancho_de_armadilha", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("gancho_de_armadilha", type));
            lore9.add(" ");
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            ganchoarmadilhaMeta.setLore(lore9);
        } else if (LojaAPI.valorCompra("gancho_de_armadilha", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("gancho_de_armadilha", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("gancho_de_armadilha", type));
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add(" ");
            ganchoarmadilhaMeta.setLore(lore9);
        } else if (LojaAPI.valorVenda("gancho_de_armadilha", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("gancho_de_armadilha", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("gancho_de_armadilha", type));
            lore9.add(" ");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore9.add(" ");
            ganchoarmadilhaMeta.setLore(lore9);
        }
        ganchoarmadilha.setItemMeta(ganchoarmadilhaMeta);
        INVENTORY_LOJAREDSTONE.setItem(22, ganchoarmadilha);


        final ItemStack piston = new ItemStack(Material.PISTON);
        final ItemMeta pistonMeta = piston.getItemMeta();
        pistonMeta.setDisplayName("§ePistão");
        final ArrayList<String> lore8 = new ArrayList<String>();
        if (LojaAPI.valorVenda("pistao", type) != 0 && LojaAPI.valorCompra("pistao", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pistao", type)));
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pistao", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("pistao", type));
            lore8.add(" ");
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            pistonMeta.setLore(lore8);
        } else if (LojaAPI.valorCompra("pistao", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pistao", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("pistao", type));
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add(" ");
            pistonMeta.setLore(lore8);
        } else if (LojaAPI.valorVenda("pistao", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pistao", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("pistao", type));
            lore8.add(" ");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore8.add(" ");
            pistonMeta.setLore(lore8);
        }
        piston.setItemMeta(pistonMeta);
        INVENTORY_LOJAREDSTONE.setItem(21, piston);


        final ItemStack hopper = new ItemStack(Material.HOPPER);
        final ItemMeta hopperMeta = hopper.getItemMeta();
        hopperMeta.setDisplayName("§eFunil");
        final ArrayList<String> lore7 = new ArrayList<String>();
        if (LojaAPI.valorVenda("funil", type) != 0 && LojaAPI.valorCompra("funil", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("funil", type)));
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("funil", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("funil", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            hopperMeta.setLore(lore7);
        } else if (LojaAPI.valorCompra("funil", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("funil", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("funil", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add(" ");
            hopperMeta.setLore(lore7);
        } else if (LojaAPI.valorVenda("funil", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("funil", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("funil", type));
            lore7.add(" ");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore7.add(" ");
            hopperMeta.setLore(lore7);
        }
        hopper.setItemMeta(hopperMeta);
        INVENTORY_LOJAREDSTONE.setItem(16, hopper);


        final ItemStack observer = new ItemStack(Material.OBSERVER);
        final ItemMeta observerMeta = observer.getItemMeta();
        observerMeta.setDisplayName("§eObservador");
        final ArrayList<String> lore6 = new ArrayList<String>();
        if (LojaAPI.valorVenda("observador", type) != 0 && LojaAPI.valorCompra("observador", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("observador", type)));
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("observador", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("observador", type));
            lore6.add(" ");
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            observerMeta.setLore(lore6);
        } else if (LojaAPI.valorCompra("observador", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("observador", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("observador", type));
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add(" ");
            observerMeta.setLore(lore6);
        } else if (LojaAPI.valorVenda("observador", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("observador", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("observador", type));
            lore6.add(" ");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore6.add(" ");
            observerMeta.setLore(lore6);
        }
        observer.setItemMeta(observerMeta);
        INVENTORY_LOJAREDSTONE.setItem(15, observer);


        final ItemStack dropper = new ItemStack(Material.DROPPER);
        final ItemMeta dropperMeta = dropper.getItemMeta();
        dropperMeta.setDisplayName("§eLiberador");
        final ArrayList<String> lore5 = new ArrayList<String>();
        if (LojaAPI.valorVenda("liberador", type) != 0 && LojaAPI.valorCompra("liberador", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("liberador", type)));
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("liberador", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("liberador", type));
            lore5.add(" ");
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            dropperMeta.setLore(lore5);
        } else if (LojaAPI.valorCompra("liberador", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("liberador", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("liberador", type));
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add(" ");
            dropperMeta.setLore(lore5);
        } else if (LojaAPI.valorVenda("liberador", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("liberador", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("liberador", type));
            lore5.add(" ");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore5.add(" ");
            dropperMeta.setLore(lore5);
        }
        dropper.setItemMeta(dropperMeta);
        INVENTORY_LOJAREDSTONE.setItem(14, dropper);


        final ItemStack dispenser = new ItemStack(Material.DISPENSER);
        final ItemMeta dispenserMeta = dispenser.getItemMeta();
        dispenserMeta.setDisplayName("§eEjetor");
        final ArrayList<String> lore4 = new ArrayList<String>();
        if (LojaAPI.valorVenda("ejetor", type) != 0 && LojaAPI.valorCompra("ejetor", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("ejetor", type)));
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("ejetor", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("ejetor", type));
            lore4.add(" ");
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            dispenserMeta.setLore(lore4);
        } else if (LojaAPI.valorCompra("ejetor", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("ejetor", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("ejetor", type));
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add(" ");
            dispenserMeta.setLore(lore4);
        } else if (LojaAPI.valorVenda("ejetor", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("ejetor", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("ejetor", type));
            lore4.add(" ");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore4.add(" ");
            dispenserMeta.setLore(lore4);
        }
        dispenser.setItemMeta(dispenserMeta);
        INVENTORY_LOJAREDSTONE.setItem(13, dispenser);


        final ItemStack comparadoredstone = new ItemStack(Material.COMPARATOR);
        final ItemMeta comparadoredstoneMeta = comparadoredstone.getItemMeta();
        comparadoredstoneMeta.setDisplayName("§eComparador de redstone");
        final ArrayList<String> lore3 = new ArrayList<String>();
        if (LojaAPI.valorVenda("comparador_de_redstone", type) != 0 && LojaAPI.valorCompra("comparador_de_redstone", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("comparador_de_redstone", type)));
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("comparador_de_redstone", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("comparador_de_redstone", type));
            lore3.add(" ");
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            comparadoredstoneMeta.setLore(lore3);
        } else if (LojaAPI.valorCompra("comparador_de_redstone", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("comparador_de_redstone", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("comparador_de_redstone", type));
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add(" ");
            comparadoredstoneMeta.setLore(lore3);
        } else if (LojaAPI.valorVenda("comparador_de_redstone", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("comparador_de_redstone", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("comparador_de_redstone", type));
            lore3.add(" ");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore3.add(" ");
            comparadoredstoneMeta.setLore(lore3);
        }
        comparadoredstone.setItemMeta(comparadoredstoneMeta);
        INVENTORY_LOJAREDSTONE.setItem(12, comparadoredstone);


        final ItemStack repetidoredstone = new ItemStack(Material.REPEATER);
        final ItemMeta repetidoredstoneMeta = repetidoredstone.getItemMeta();
        repetidoredstoneMeta.setDisplayName("§eRepetidor de redstone");
        final ArrayList<String> lore2 = new ArrayList<String>();
        if (LojaAPI.valorVenda("repetidor_de_redstone", type) != 0 && LojaAPI.valorCompra("repetidor_de_redstone", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("repetidor_de_redstone", type)));
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("repetidor_de_redstone", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("repetidor_de_redstone", type));
            lore2.add(" ");
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            repetidoredstoneMeta.setLore(lore2);
        } else if (LojaAPI.valorCompra("repetidor_de_redstone", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("repetidor_de_redstone", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("repetidor_de_redstone", type));
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add(" ");
            repetidoredstoneMeta.setLore(lore2);
        } else if (LojaAPI.valorVenda("repetidor_de_redstone", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("repetidor_de_redstone", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("repetidor_de_redstone", type));
            lore2.add(" ");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore2.add(" ");
            repetidoredstoneMeta.setLore(lore2);
        }
        repetidoredstone.setItemMeta(repetidoredstoneMeta);
        INVENTORY_LOJAREDSTONE.setItem(11, repetidoredstone);


        final ItemStack redstone = new ItemStack(Material.REDSTONE);
        final ItemMeta redstoneMeta = redstone.getItemMeta();
        redstoneMeta.setDisplayName("§ePó de redstone");
        final ArrayList<String> lore1 = new ArrayList<String>();
        if (LojaAPI.valorVenda("po_de_redstone", type) != 0 && LojaAPI.valorCompra("po_de_redstone", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_redstone", type)));
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_redstone", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_redstone", type));
            lore1.add(" ");
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            redstoneMeta.setLore(lore1);
        } else if (LojaAPI.valorCompra("po_de_redstone", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("po_de_redstone", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_redstone", type));
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add(" ");
            redstoneMeta.setLore(lore1);
        } else if (LojaAPI.valorVenda("po_de_redstone", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("po_de_redstone", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("po_de_redstone", type));
            lore1.add(" ");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore1.add(" ");
            redstoneMeta.setLore(lore1);
        }
        redstone.setItemMeta(redstoneMeta);
        INVENTORY_LOJAREDSTONE.setItem(10, redstone);

        return INVENTORY_LOJAREDSTONE;
    }

    public static Inventory buildLojaComidas(final String type) {
        final Inventory INVENTORY_LOJACOMIDAS = Bukkit.createInventory(null, 4 * 9, "Comidas: ");

        final ItemStack goldencarrot = new ItemStack(Material.GOLDEN_CARROT);
        final ItemMeta goldencarrotMeta = goldencarrot.getItemMeta();
        goldencarrotMeta.setDisplayName("§eCenoura dourada");
        final ArrayList<String> lore14 = new ArrayList<String>();
        if (LojaAPI.valorVenda("cenoura_dourada", type) != 0 && LojaAPI.valorCompra("cenoura_dourada", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("cenoura_dourada", type)));
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("cenoura_dourada", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("cenoura_dourada", type));
            lore14.add(" ");
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            goldencarrotMeta.setLore(lore14);
        } else if (LojaAPI.valorCompra("cenoura_dourada", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("cenoura_dourada", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("cenoura_dourada", type));
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add(" ");
            goldencarrotMeta.setLore(lore14);
        } else if (LojaAPI.valorVenda("cenoura_dourada", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("cenoura_dourada", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("cenoura_dourada", type));
            lore14.add(" ");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore14.add(" ");
            goldencarrotMeta.setLore(lore14);
        }
        goldencarrot.setItemMeta(goldencarrotMeta);
        INVENTORY_LOJACOMIDAS.setItem(25, goldencarrot);


        final ItemStack frangoassado = new ItemStack(Material.COOKED_CHICKEN);
        final ItemMeta frangoassadoMeta = frangoassado.getItemMeta();
        frangoassadoMeta.setDisplayName("§eFrango assado");
        final ArrayList<String> lore13 = new ArrayList<String>();
        if (LojaAPI.valorVenda("frango_assado", type) != 0 && LojaAPI.valorCompra("frango_assado", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("frango_assado", type)));
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("frango_assado", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("frango_assado", type));
            lore13.add(" ");
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            frangoassadoMeta.setLore(lore13);
        } else if (LojaAPI.valorCompra("frango_assado", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("frango_assado", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("frango_assado", type));
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add(" ");
            frangoassadoMeta.setLore(lore13);
        } else if (LojaAPI.valorVenda("frango_assado", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("frango_assado", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("frango_assado", type));
            lore13.add(" ");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore13.add(" ");
            frangoassadoMeta.setLore(lore13);
        }
        frangoassado.setItemMeta(frangoassadoMeta);
        INVENTORY_LOJACOMIDAS.setItem(24, frangoassado);


        final ItemStack carneiroassado = new ItemStack(Material.COOKED_MUTTON);
        final ItemMeta carneiroassadoMeta = carneiroassado.getItemMeta();
        carneiroassadoMeta.setDisplayName("§eCarneiro assado");
        final ArrayList<String> lore12 = new ArrayList<String>();
        if (LojaAPI.valorVenda("carneiro_assado", type) != 0 && LojaAPI.valorCompra("carneiro_assado", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("carneiro_assado", type)));
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("carneiro_assado", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("carneiro_assado", type));
            lore12.add(" ");
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            carneiroassadoMeta.setLore(lore12);
        } else if (LojaAPI.valorCompra("carneiro_assado", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("carneiro_assado", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("carneiro_assado", type));
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add(" ");
            carneiroassadoMeta.setLore(lore12);
        } else if (LojaAPI.valorVenda("carneiro_assado", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("carneiro_assado", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("carneiro_assado", type));
            lore12.add(" ");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore12.add(" ");
            carneiroassadoMeta.setLore(lore12);
        }
        carneiroassado.setItemMeta(carneiroassadoMeta);
        INVENTORY_LOJACOMIDAS.setItem(23, carneiroassado);


        final ItemStack file = new ItemStack(Material.COOKED_BEEF);
        final ItemMeta fileMeta = file.getItemMeta();
        fileMeta.setDisplayName("§eFilé");
        final ArrayList<String> lore11 = new ArrayList<String>();
        if (LojaAPI.valorVenda("file", type) != 0 && LojaAPI.valorCompra("file", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("file", type)));
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("file", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("file", type));
            lore11.add(" ");
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            fileMeta.setLore(lore11);
        } else if (LojaAPI.valorCompra("file", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("file", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("file", type));
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add(" ");
            fileMeta.setLore(lore11);
        } else if (LojaAPI.valorVenda("file", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("file", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("file", type));
            lore11.add(" ");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore11.add(" ");
            fileMeta.setLore(lore11);
        }
        file.setItemMeta(fileMeta);
        INVENTORY_LOJACOMIDAS.setItem(22, file);


        final ItemStack beterraba = new ItemStack(Material.BEETROOT);
        final ItemMeta beterrabaMeta = beterraba.getItemMeta();
        beterrabaMeta.setDisplayName("§eBeterraba");
        final ArrayList<String> lore10 = new ArrayList<String>();
        if (LojaAPI.valorVenda("beterraba", type) != 0 && LojaAPI.valorCompra("beterraba", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("beterraba", type)));
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("beterraba", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("beterraba", type));
            lore10.add(" ");
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            beterrabaMeta.setLore(lore10);
        } else if (LojaAPI.valorCompra("beterraba", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("beterraba", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("beterraba", type));
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add(" ");
            beterrabaMeta.setLore(lore10);
        } else if (LojaAPI.valorVenda("beterraba", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("beterraba", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("beterraba", type));
            lore10.add(" ");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore10.add(" ");
            beterrabaMeta.setLore(lore10);
        }
        beterraba.setItemMeta(beterrabaMeta);
        INVENTORY_LOJACOMIDAS.setItem(21, beterraba);


        final ItemStack salmaoassado = new ItemStack(Material.COOKED_SALMON);
        final ItemMeta salmaoassadoMeta = salmaoassado.getItemMeta();
        salmaoassadoMeta.setDisplayName("§eSalmão assado");
        final ArrayList<String> lore9 = new ArrayList<String>();
        if (LojaAPI.valorVenda("salmao_assado", type) != 0 && LojaAPI.valorCompra("salmao_assado", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("salmao_assado", type)));
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("salmao_assado", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("salmao_assado", type));
            lore9.add(" ");
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            salmaoassadoMeta.setLore(lore9);
        } else if (LojaAPI.valorCompra("salmao_assado", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("salmao_assado", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("salmao_assado", type));
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add(" ");
            salmaoassadoMeta.setLore(lore9);
        } else if (LojaAPI.valorVenda("salmao_assado", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("salmao_assado", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("salmao_assado", type));
            lore9.add(" ");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore9.add(" ");
            salmaoassadoMeta.setLore(lore9);
        }
        salmaoassado.setItemMeta(salmaoassadoMeta);
        INVENTORY_LOJACOMIDAS.setItem(20, salmaoassado);


        final ItemStack bacalhauassado = new ItemStack(Material.COOKED_COD);
        final ItemMeta bacalhauassadoMeta = bacalhauassado.getItemMeta();
        bacalhauassadoMeta.setDisplayName("§eBacalhau assado");
        final ArrayList<String> lore8 = new ArrayList<String>();
        if (LojaAPI.valorVenda("bacalhau_assado", type) != 0 && LojaAPI.valorCompra("bacalhau_assado", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bacalhau_assado", type)));
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bacalhau_assado", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("bacalhau_assado", type));
            lore8.add(" ");
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            bacalhauassadoMeta.setLore(lore8);
        } else if (LojaAPI.valorCompra("bacalhau_assado", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bacalhau_assado", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("bacalhau_assado", type));
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add(" ");
            bacalhauassadoMeta.setLore(lore8);
        } else if (LojaAPI.valorVenda("bacalhau_assado", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bacalhau_assado", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("bacalhau_assado", type));
            lore8.add(" ");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore8.add(" ");
            bacalhauassadoMeta.setLore(lore8);
        }
        bacalhauassado.setItemMeta(bacalhauassadoMeta);
        INVENTORY_LOJACOMIDAS.setItem(19, bacalhauassado);


        final ItemStack carrot = new ItemStack(Material.CARROT);
        final ItemMeta carrotMeta = carrot.getItemMeta();
        carrotMeta.setDisplayName("§eCenoura");
        final ArrayList<String> lore7 = new ArrayList<String>();
        if (LojaAPI.valorVenda("cenoura", type) != 0 && LojaAPI.valorCompra("cenoura", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("cenoura", type)));
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("cenoura", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("cenoura", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            carrotMeta.setLore(lore7);
        } else if (LojaAPI.valorCompra("cenoura", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("cenoura", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("cenoura", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add(" ");
            carrotMeta.setLore(lore7);
        } else if (LojaAPI.valorVenda("cenoura", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("cenoura", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("cenoura", type));
            lore7.add(" ");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore7.add(" ");
            carrotMeta.setLore(lore7);
        }
        carrot.setItemMeta(carrotMeta);
        INVENTORY_LOJACOMIDAS.setItem(16, carrot);


        final ItemStack potato = new ItemStack(Material.POTATO);
        final ItemMeta potatoMeta = potato.getItemMeta();
        potatoMeta.setDisplayName("§eBatata");
        final ArrayList<String> lore6 = new ArrayList<String>();
        if (LojaAPI.valorVenda("batata", type) != 0 && LojaAPI.valorCompra("batata", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("batata", type)));
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("batata", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("batata", type));
            lore6.add(" ");
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            potatoMeta.setLore(lore6);
        } else if (LojaAPI.valorCompra("batata", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("batata", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("batata", type));
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add(" ");
            potatoMeta.setLore(lore6);
        } else if (LojaAPI.valorVenda("batata", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("batata", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("batata", type));
            lore6.add(" ");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore6.add(" ");
            potatoMeta.setLore(lore6);
        }
        potato.setItemMeta(potatoMeta);
        INVENTORY_LOJACOMIDAS.setItem(15, potato);


        final ItemStack melon = new ItemStack(Material.MELON_SLICE);
        final ItemMeta melonMeta = melon.getItemMeta();
        melonMeta.setDisplayName("§eFatia de melancia");
        final ArrayList<String> lore5 = new ArrayList<String>();
        if (LojaAPI.valorVenda("fatia_de_melancia", type) != 0 && LojaAPI.valorCompra("fatia_de_melancia", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("fatia_de_melancia", type)));
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("fatia_de_melancia", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("fatia_de_melancia", type));
            lore5.add(" ");
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            melonMeta.setLore(lore5);
        } else if (LojaAPI.valorCompra("fatia_de_melancia", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("fatia_de_melancia", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("fatia_de_melancia", type));
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add(" ");
            melonMeta.setLore(lore5);
        } else if (LojaAPI.valorVenda("fatia_de_melancia", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("fatia_de_melancia", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("fatia_de_melancia", type));
            lore5.add(" ");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore5.add(" ");
            melonMeta.setLore(lore5);
        }
        melon.setItemMeta(melonMeta);
        INVENTORY_LOJACOMIDAS.setItem(14, melon);


        final ItemStack cookie = new ItemStack(Material.COOKIE);
        final ItemMeta cookieMeta = cookie.getItemMeta();
        cookieMeta.setDisplayName("§eBiscoito");
        final ArrayList<String> lore4 = new ArrayList<String>();
        if (LojaAPI.valorVenda("biscoito", type) != 0 && LojaAPI.valorCompra("biscoito", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("biscoito", type)));
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("biscoito", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("biscoito", type));
            lore4.add(" ");
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            cookieMeta.setLore(lore4);
        } else if (LojaAPI.valorCompra("biscoito", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("biscoito", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("biscoito", type));
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add(" ");
            cookieMeta.setLore(lore4);
        } else if (LojaAPI.valorVenda("biscoito", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("biscoito", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("biscoito", type));
            lore4.add(" ");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore4.add(" ");
            cookieMeta.setLore(lore4);
        }
        cookie.setItemMeta(cookieMeta);
        INVENTORY_LOJACOMIDAS.setItem(13, cookie);


        final ItemStack apple = new ItemStack(Material.APPLE);
        final ItemMeta appleMeta = apple.getItemMeta();
        appleMeta.setDisplayName("§eMaçã");
        final ArrayList<String> lore3 = new ArrayList<String>();
        if (LojaAPI.valorVenda("maça", type) != 0 && LojaAPI.valorCompra("maça", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("maça", type)));
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("maça", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("maça", type));
            lore3.add(" ");
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            appleMeta.setLore(lore3);
        } else if (LojaAPI.valorCompra("maça", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("maça", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("maça", type));
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add(" ");
            appleMeta.setLore(lore3);
        } else if (LojaAPI.valorVenda("maça", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("maça", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("maça", type));
            lore3.add(" ");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore3.add(" ");
            appleMeta.setLore(lore3);
        }
        apple.setItemMeta(appleMeta);
        INVENTORY_LOJACOMIDAS.setItem(12, apple);


        final ItemStack cake = new ItemStack(Material.CAKE);
        final ItemMeta cakeMeta = cake.getItemMeta();
        cakeMeta.setDisplayName("§eBolo");
        final ArrayList<String> lore2 = new ArrayList<String>();
        if (LojaAPI.valorVenda("bolo", type) != 0 && LojaAPI.valorCompra("bolo", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bolo", type)));
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bolo", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("bolo", type));
            lore2.add(" ");
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            cakeMeta.setLore(lore2);
        } else if (LojaAPI.valorCompra("bolo", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bolo", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("bolo", type));
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add(" ");
            cakeMeta.setLore(lore2);
        } else if (LojaAPI.valorVenda("bolo", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bolo", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("bolo", type));
            lore2.add(" ");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore2.add(" ");
            cakeMeta.setLore(lore2);
        }
        cake.setItemMeta(cakeMeta);
        INVENTORY_LOJACOMIDAS.setItem(11, cake);


        final ItemStack bread = new ItemStack(Material.BREAD);
        final ItemMeta breadMeta = bread.getItemMeta();
        breadMeta.setDisplayName("§ePão");
        final ArrayList<String> lore1 = new ArrayList<String>();
        if (LojaAPI.valorVenda("pao", type) != 0 && LojaAPI.valorCompra("pao", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pao", type)));
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pao", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("pao", type));
            lore1.add(" ");
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            breadMeta.setLore(lore1);
        } else if (LojaAPI.valorCompra("pao", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pao", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("pao", type));
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add(" ");
            breadMeta.setLore(lore1);
        } else if (LojaAPI.valorVenda("pao", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pao", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("pao", type));
            lore1.add(" ");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore1.add(" ");
            breadMeta.setLore(lore1);
        }
        bread.setItemMeta(breadMeta);
        INVENTORY_LOJACOMIDAS.setItem(10, bread);

        return INVENTORY_LOJACOMIDAS;
    }

    public static Inventory buildLojaMaterial2(final String type) {
        final Inventory INVENTORY_LOJAMATERIAIS2 = Bukkit.createInventory(null, 4 * 9, "Materiais: ");

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta voltarMeta = voltar.getItemMeta();
        voltarMeta.setDisplayName(ChatColor.GREEN + "Voltar página.");
        voltar.setItemMeta(voltarMeta);
        INVENTORY_LOJAMATERIAIS2.setItem(27, voltar);


        final ItemStack pelecoelho = new ItemStack(Material.RABBIT_HIDE);
        final ItemMeta pelecoelhoMeta = pelecoelho.getItemMeta();
        pelecoelhoMeta.setDisplayName("§ePele de coelho");
        final ArrayList<String> lore13 = new ArrayList<String>();
        if (LojaAPI.valorVenda("pele_de_coelho", type) != 0 && LojaAPI.valorCompra("pele_de_coelho", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pele_de_coelho", type)));
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pele_de_coelho", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("pele_de_coelho", type));
            lore13.add(" ");
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            pelecoelhoMeta.setLore(lore13);
        } else if (LojaAPI.valorCompra("pele_de_coelho", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pele_de_coelho", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("pele_de_coelho", type));
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add(" ");
            pelecoelhoMeta.setLore(lore13);
        } else if (LojaAPI.valorVenda("pele_de_coelho", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pele_de_coelho", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("pele_de_coelho", type));
            lore13.add(" ");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore13.add(" ");
            pelecoelhoMeta.setLore(lore13);
        }
        pelecoelho.setItemMeta(pelecoelhoMeta);
        INVENTORY_LOJAMATERIAIS2.setItem(25, pelecoelho);


        final ItemStack pécoelho = new ItemStack(Material.RABBIT_FOOT);
        final ItemMeta pécoelhoMeta = pécoelho.getItemMeta();
        pécoelhoMeta.setDisplayName("§ePé de coelho");
        final ArrayList<String> lore12 = new ArrayList<String>();
        if (LojaAPI.valorVenda("pe_de_coelho", type) != 0 && LojaAPI.valorCompra("pe_de_coelho", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pe_de_coelho", type)));
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pe_de_coelho", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("pe_de_coelho", type));
            lore12.add(" ");
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            pécoelhoMeta.setLore(lore12);
        } else if (LojaAPI.valorCompra("pe_de_coelho", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pe_de_coelho", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("pe_de_coelho", type));
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add(" ");
            pécoelhoMeta.setLore(lore12);
        } else if (LojaAPI.valorVenda("pe_de_coelho", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pe_de_coelho", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("pe_de_coelho", type));
            lore12.add(" ");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore12.add(" ");
            pécoelhoMeta.setLore(lore12);
        }
        pécoelho.setItemMeta(pécoelhoMeta);
        INVENTORY_LOJAMATERIAIS2.setItem(24, pécoelho);


        final ItemStack membranaphantom = new ItemStack(Material.PHANTOM_MEMBRANE);
        final ItemMeta membranaphantomMeta = membranaphantom.getItemMeta();
        membranaphantomMeta.setDisplayName("§eMembrana de phantom");
        final ArrayList<String> lore11 = new ArrayList<String>();
        if (LojaAPI.valorVenda("membrana_de_phantom", type) != 0 && LojaAPI.valorCompra("membrana_de_phantom", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("membrana_de_phantom", type)));
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("membrana_de_phantom", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("membrana_de_phantom", type));
            lore11.add(" ");
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            membranaphantomMeta.setLore(lore11);
        } else if (LojaAPI.valorCompra("membrana_de_phantom", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("membrana_de_phantom", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("membrana_de_phantom", type));
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add(" ");
            membranaphantomMeta.setLore(lore11);
        } else if (LojaAPI.valorVenda("membrana_de_phantom", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("membrana_de_phantom", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("membrana_de_phantom", type));
            lore11.add(" ");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore11.add(" ");
            membranaphantomMeta.setLore(lore11);
        }
        membranaphantom.setItemMeta(membranaphantomMeta);
        INVENTORY_LOJAMATERIAIS2.setItem(23, membranaphantom);


        final ItemStack conchanautilo = new ItemStack(Material.NAUTILUS_SHELL);
        final ItemMeta conchanautiloMeta = conchanautilo.getItemMeta();
        conchanautiloMeta.setDisplayName("§eConcha de náutilo");
        final ArrayList<String> lore10 = new ArrayList<String>();
        if (LojaAPI.valorVenda("concha_de_nautilo", type) != 0 && LojaAPI.valorCompra("concha_de_nautilo", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("concha_de_nautilo", type)));
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("concha_de_nautilo", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("concha_de_nautilo", type));
            lore10.add(" ");
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            conchanautiloMeta.setLore(lore10);
        } else if (LojaAPI.valorCompra("concha_de_nautilo", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("concha_de_nautilo", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("concha_de_nautilo", type));
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add(" ");
            conchanautiloMeta.setLore(lore10);
        } else if (LojaAPI.valorVenda("concha_de_nautilo", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("concha_de_nautilo", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("concha_de_nautilo", type));
            lore10.add(" ");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore10.add(" ");
            conchanautiloMeta.setLore(lore10);
        }
        conchanautilo.setItemMeta(conchanautiloMeta);
        INVENTORY_LOJAMATERIAIS2.setItem(22, conchanautilo);


        final ItemStack cascashulker = new ItemStack(Material.SHULKER_SHELL);
        final ItemMeta cascashulkerMeta = cascashulker.getItemMeta();
        cascashulkerMeta.setDisplayName("§eCasca de shulker");
        final ArrayList<String> lore9 = new ArrayList<String>();
        if (LojaAPI.valorVenda("casca_de_shulker", type) != 0 && LojaAPI.valorCompra("casca_de_shulker", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("casca_de_shulker", type)));
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("casca_de_shulker", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("casca_de_shulker", type));
            lore9.add(" ");
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            cascashulkerMeta.setLore(lore9);
        } else if (LojaAPI.valorCompra("casca_de_shulker", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("casca_de_shulker", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("casca_de_shulker", type));
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add(" ");
            cascashulkerMeta.setLore(lore9);
        } else if (LojaAPI.valorVenda("casca_de_shulker", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("casca_de_shulker", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("casca_de_shulker", type));
            lore9.add(" ");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore9.add(" ");
            cascashulkerMeta.setLore(lore9);
        }
        cascashulker.setItemMeta(cascashulkerMeta);
        INVENTORY_LOJAMATERIAIS2.setItem(21, cascashulker);


        final ItemStack flint = new ItemStack(Material.FLINT);
        final ItemMeta flintMeta = flint.getItemMeta();
        flintMeta.setDisplayName("§eSílex");
        final ArrayList<String> lore8 = new ArrayList<String>();
        if (LojaAPI.valorVenda("silex", type) != 0 && LojaAPI.valorCompra("silex", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("silex", type)));
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("silex", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("silex", type));
            lore8.add(" ");
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            flintMeta.setLore(lore8);
        } else if (LojaAPI.valorCompra("silex", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("silex", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("silex", type));
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add(" ");
            flintMeta.setLore(lore8);
        } else if (LojaAPI.valorVenda("silex", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("silex", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("silex", type));
            lore8.add(" ");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore8.add(" ");
            flintMeta.setLore(lore8);
        }
        flint.setItemMeta(flintMeta);
        INVENTORY_LOJAMATERIAIS2.setItem(20, flint);


        final ItemStack tartarugadrop = new ItemStack(Material.TURTLE_HELMET);
        final ItemMeta tartarugadropMeta = tartarugadrop.getItemMeta();
        tartarugadropMeta.setDisplayName("§eCarapaça de tartaruga");
        final ArrayList<String> lore7 = new ArrayList<String>();
        if (LojaAPI.valorVenda("carapaça_de_tartaruga", type) != 0 && LojaAPI.valorCompra("carapaça_de_tartaruga", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("carapaça_de_tartaruga", type)));
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("carapaça_de_tartaruga", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("carapaça_de_tartaruga", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            tartarugadropMeta.setLore(lore7);
        } else if (LojaAPI.valorCompra("carapaça_de_tartaruga", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("carapaça_de_tartaruga", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("carapaça_de_tartaruga", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add(" ");
            tartarugadropMeta.setLore(lore7);
        } else if (LojaAPI.valorVenda("carapaça_de_tartaruga", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("carapaça_de_tartaruga", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("carapaça_de_tartaruga", type));
            lore7.add(" ");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore7.add(" ");
            tartarugadropMeta.setLore(lore7);
        }
        tartarugadrop.setItemMeta(tartarugadropMeta);
        INVENTORY_LOJAMATERIAIS2.setItem(19, tartarugadrop);


        final ItemStack ovo = new ItemStack(Material.EGG);
        final ItemMeta ovoMeta = ovo.getItemMeta();
        ovoMeta.setDisplayName("§eOvo");
        final ArrayList<String> lore6 = new ArrayList<String>();
        if (LojaAPI.valorVenda("ovo", type) != 0 && LojaAPI.valorCompra("ovo", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("ovo", type)));
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("ovo", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("ovo", type));
            lore6.add(" ");
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            ovoMeta.setLore(lore6);
        } else if (LojaAPI.valorCompra("ovo", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("ovo", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("ovo", type));
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add(" ");
            ovoMeta.setLore(lore6);
        } else if (LojaAPI.valorVenda("ovo", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("ovo", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("ovo", type));
            lore6.add(" ");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore6.add(" ");
            ovoMeta.setLore(lore6);
        }
        ovo.setItemMeta(ovoMeta);
        INVENTORY_LOJAMATERIAIS2.setItem(16, ovo);


        final ItemStack pena = new ItemStack(Material.FEATHER);
        final ItemMeta penaMeta = pena.getItemMeta();
        penaMeta.setDisplayName("§ePena");
        final ArrayList<String> lore5 = new ArrayList<String>();
        if (LojaAPI.valorVenda("pena", type) != 0 && LojaAPI.valorCompra("pena", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pena", type)));
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pena", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("pena", type));
            lore5.add(" ");
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            penaMeta.setLore(lore5);
        } else if (LojaAPI.valorCompra("pena", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pena", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("pena", type));
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add(" ");
            penaMeta.setLore(lore5);
        } else if (LojaAPI.valorVenda("pena", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pena", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("pena", type));
            lore5.add(" ");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore5.add(" ");
            penaMeta.setLore(lore5);
        }
        pena.setItemMeta(penaMeta);
        INVENTORY_LOJAMATERIAIS2.setItem(15, pena);


        final ItemStack frango = new ItemStack(Material.CHICKEN);
        final ItemMeta frangoMeta = frango.getItemMeta();
        frangoMeta.setDisplayName("§eFrango cru");
        final ArrayList<String> lore4 = new ArrayList<String>();
        if (LojaAPI.valorVenda("frango_cru", type) != 0 && LojaAPI.valorCompra("frango_cru", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("frango_cru", type)));
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("frango_cru", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("frango_cru", type));
            lore4.add(" ");
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            frangoMeta.setLore(lore4);
        } else if (LojaAPI.valorCompra("frango_cru", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("frango_cru", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("frango_cru", type));
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add(" ");
            frangoMeta.setLore(lore4);
        } else if (LojaAPI.valorVenda("frango_cru", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("frango_cru", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("frango_cru", type));
            lore4.add(" ");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore4.add(" ");
            frangoMeta.setLore(lore4);
        }
        frango.setItemMeta(frangoMeta);
        INVENTORY_LOJAMATERIAIS2.setItem(14, frango);


        final ItemStack salmao = new ItemStack(Material.SALMON);
        final ItemMeta salmaoMeta = salmao.getItemMeta();
        salmaoMeta.setDisplayName("§eSalmão cru");
        final ArrayList<String> lore3 = new ArrayList<String>();
        if (LojaAPI.valorVenda("salmao_cru", type) != 0 && LojaAPI.valorCompra("salmao_cru", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("salmao_cru", type)));
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("salmao_cru", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("salmao_cru", type));
            lore3.add(" ");
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            salmaoMeta.setLore(lore3);
        } else if (LojaAPI.valorCompra("salmao_cru", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("salmao_cru", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("salmao_cru", type));
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add(" ");
            salmaoMeta.setLore(lore3);
        } else if (LojaAPI.valorVenda("salmao_cru", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("salmao_cru", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("salmao_cru", type));
            lore3.add(" ");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore3.add(" ");
            salmaoMeta.setLore(lore3);
        }
        salmao.setItemMeta(salmaoMeta);
        INVENTORY_LOJAMATERIAIS2.setItem(13, salmao);


        final ItemStack bacalhau = new ItemStack(Material.COOKED_COD);
        final ItemMeta bacalhauMeta = bacalhau.getItemMeta();
        bacalhauMeta.setDisplayName("§eBacalhau cru");
        final ArrayList<String> lore2 = new ArrayList<String>();
        if (LojaAPI.valorVenda("bacalhau_cru", type) != 0 && LojaAPI.valorCompra("bacalhau_cru", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bacalhau_cru", type)));
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bacalhau_cru", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("bacalhau_cru", type));
            lore2.add(" ");
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            bacalhauMeta.setLore(lore2);
        } else if (LojaAPI.valorCompra("bacalhau_cru", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bacalhau_cru", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("bacalhau_cru", type));
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add(" ");
            bacalhauMeta.setLore(lore2);
        } else if (LojaAPI.valorVenda("bacalhau_cru", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bacalhau_cru", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("bacalhau_cru", type));
            lore2.add(" ");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore2.add(" ");
            bacalhauMeta.setLore(lore2);
        }
        bacalhau.setItemMeta(bacalhauMeta);
        INVENTORY_LOJAMATERIAIS2.setItem(12, bacalhau);


        final ItemStack baiacu = new ItemStack(Material.PUFFERFISH);
        final ItemMeta baiacuMeta = baiacu.getItemMeta();
        baiacuMeta.setDisplayName("§eBaiacu");
        final ArrayList<String> lore1 = new ArrayList<String>();
        if (LojaAPI.valorVenda("baiacu", type) != 0 && LojaAPI.valorCompra("baiacu", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("baiacu", type)));
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("baiacu", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("baiacu", type));
            lore1.add(" ");
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            baiacuMeta.setLore(lore1);
        } else if (LojaAPI.valorCompra("baiacu", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("baiacu", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("baiacu", type));
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add(" ");
            baiacuMeta.setLore(lore1);
        } else if (LojaAPI.valorVenda("baiacu", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("baiacu", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("baiacu", type));
            lore1.add(" ");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore1.add(" ");
            baiacuMeta.setLore(lore1);
        }
        baiacu.setItemMeta(baiacuMeta);
        INVENTORY_LOJAMATERIAIS2.setItem(11, baiacu);


        final ItemStack slimeball = new ItemStack(Material.SLIME_BALL);
        final ItemMeta slimeballMeta = slimeball.getItemMeta();
        slimeballMeta.setDisplayName("§eBola de slime");
        final ArrayList<String> lore = new ArrayList<String>();
        if (LojaAPI.valorVenda("bola_de_slime", type) != 0 && LojaAPI.valorCompra("bola_de_slime", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bola_de_slime", type)));
            lore.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bola_de_slime", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("bola_de_slime", type));
            lore.add(" ");
            lore.add(" ");
            lore.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            slimeballMeta.setLore(lore);
        } else if (LojaAPI.valorCompra("bola_de_slime", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bola_de_slime", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("bola_de_slime", type));
            lore.add(" ");
            lore.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore.add(" ");
            slimeballMeta.setLore(lore);
        } else if (LojaAPI.valorVenda("bola_de_slime", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bola_de_slime", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("bola_de_slime", type));
            lore.add(" ");
            lore.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore.add(" ");
            slimeballMeta.setLore(lore);
        }
        slimeball.setItemMeta(slimeballMeta);
        INVENTORY_LOJAMATERIAIS2.setItem(10, slimeball);

        return INVENTORY_LOJAMATERIAIS2;
    }

    public static Inventory buildLojaMateriais1(final String type) {
        final Inventory INVENTORY_LOJAMATERIAIS1 = Bukkit.createInventory(null, 6 * 9, "Materiais: ");

        final ItemStack proxima = new ItemStack(Material.ARROW);
        final ItemMeta proximaMeta = proxima.getItemMeta();
        proximaMeta.setDisplayName(ChatColor.GREEN + "Próxima página.");
        proxima.setItemMeta(proximaMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(53, proxima);


        final ItemStack lagrimaghast = new ItemStack(Material.GHAST_TEAR);
        final ItemMeta lagrimaghastMeta = lagrimaghast.getItemMeta();
        lagrimaghastMeta.setDisplayName("§eLágrima de ghast");
        final ArrayList<String> lore27 = new ArrayList<String>();
        if (LojaAPI.valorVenda("lagrima_de_ghast", type) != 0 && LojaAPI.valorCompra("lagrima_de_ghast", type) != 0) {
            lore27.add(" ");
            lore27.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("lagrima_de_ghast", type)));
            lore27.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("lagrima_de_ghast", type)));
            lore27.add("§7Quantia: §a" + LojaAPI.itemQuantia("lagrima_de_ghast", type));
            lore27.add(" ");
            lore27.add(" ");
            lore27.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore27.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lagrimaghastMeta.setLore(lore27);
        } else if (LojaAPI.valorCompra("lagrima_de_ghast", type) != 0) {
            lore27.add(" ");
            lore27.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("lagrima_de_ghast", type)));
            lore27.add("§7Quantia: §a" + LojaAPI.itemQuantia("lagrima_de_ghast", type));
            lore27.add(" ");
            lore27.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore27.add(" ");
            lagrimaghastMeta.setLore(lore27);
        } else if (LojaAPI.valorVenda("lagrima_de_ghast", type) != 0) {
            lore27.add(" ");
            lore27.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("lagrima_de_ghast", type)));
            lore27.add("§7Quantia: §a" + LojaAPI.itemQuantia("lagrima_de_ghast", type));
            lore27.add(" ");
            lore27.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore27.add(" ");
            lagrimaghastMeta.setLore(lore27);
        }
        lagrimaghast.setItemMeta(lagrimaghastMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(43, lagrimaghast);


        final ItemStack goldnugget = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta goldnuggetMeta = goldnugget.getItemMeta();
        goldnuggetMeta.setDisplayName("§ePepita de ouro");
        final ArrayList<String> lore26 = new ArrayList<String>();
        if (LojaAPI.valorVenda("pepita_de_ouro", type) != 0 && LojaAPI.valorCompra("pepita_de_ouro", type) != 0) {
            lore26.add(" ");
            lore26.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pepita_de_ouro", type)));
            lore26.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pepita_de_ouro", type)));
            lore26.add("§7Quantia: §a" + LojaAPI.itemQuantia("pepita_de_ouro", type));
            lore26.add(" ");
            lore26.add(" ");
            lore26.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore26.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            goldnuggetMeta.setLore(lore26);
        } else if (LojaAPI.valorCompra("pepita_de_ouro", type) != 0) {
            lore26.add(" ");
            lore26.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pepita_de_ouro", type)));
            lore26.add("§7Quantia: §a" + LojaAPI.itemQuantia("pepita_de_ouro", type));
            lore26.add(" ");
            lore26.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore26.add(" ");
            goldnuggetMeta.setLore(lore26);
        } else if (LojaAPI.valorVenda("pepita_de_ouro", type) != 0) {
            lore26.add(" ");
            lore26.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pepita_de_ouro", type)));
            lore26.add("§7Quantia: §a" + LojaAPI.itemQuantia("pepita_de_ouro", type));
            lore26.add(" ");
            lore26.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore26.add(" ");
            goldnuggetMeta.setLore(lore26);
        }
        goldnugget.setItemMeta(goldnuggetMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(42, goldnugget);


        final ItemStack blazerod = new ItemStack(Material.BLAZE_ROD);
        final ItemMeta blazerodMeta = blazerod.getItemMeta();
        blazerodMeta.setDisplayName("§eVara de blaze");
        final ArrayList<String> lore25 = new ArrayList<String>();
        if (LojaAPI.valorVenda("vara_de_blaze", type) != 0 && LojaAPI.valorCompra("vara_de_blaze", type) != 0) {
            lore25.add(" ");
            lore25.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vara_de_blaze", type)));
            lore25.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vara_de_blaze", type)));
            lore25.add("§7Quantia: §a" + LojaAPI.itemQuantia("vara_de_blaze", type));
            lore25.add(" ");
            lore25.add(" ");
            lore25.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore25.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            blazerodMeta.setLore(lore25);
        } else if (LojaAPI.valorCompra("vara_de_blaze", type) != 0) {
            lore25.add(" ");
            lore25.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vara_de_blaze", type)));
            lore25.add("§7Quantia: §a" + LojaAPI.itemQuantia("vara_de_blaze", type));
            lore25.add(" ");
            lore25.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore25.add(" ");
            blazerodMeta.setLore(lore25);
        } else if (LojaAPI.valorVenda("vara_de_blaze", type) != 0) {
            lore25.add(" ");
            lore25.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vara_de_blaze", type)));
            lore25.add("§7Quantia: §a" + LojaAPI.itemQuantia("vara_de_blaze", type));
            lore25.add(" ");
            lore25.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore25.add(" ");
            blazerodMeta.setLore(lore25);
        }
        blazerod.setItemMeta(blazerodMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(41, blazerod);


        final ItemStack polvora = new ItemStack(Material.GUNPOWDER);
        final ItemMeta polvoraMeta = polvora.getItemMeta();
        polvoraMeta.setDisplayName("§ePólvora");
        final ArrayList<String> lore24 = new ArrayList<String>();
        if (LojaAPI.valorVenda("polvora", type) != 0 && LojaAPI.valorCompra("polvora", type) != 0) {
            lore24.add(" ");
            lore24.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("polvora", type)));
            lore24.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("polvora", type)));
            lore24.add("§7Quantia: §a" + LojaAPI.itemQuantia("polvora", type));
            lore24.add(" ");
            lore24.add(" ");
            lore24.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore24.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            polvoraMeta.setLore(lore24);
        } else if (LojaAPI.valorCompra("polvora", type) != 0) {
            lore24.add(" ");
            lore24.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("polvora", type)));
            lore24.add("§7Quantia: §a" + LojaAPI.itemQuantia("polvora", type));
            lore24.add(" ");
            lore24.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore24.add(" ");
            polvoraMeta.setLore(lore24);
        } else if (LojaAPI.valorVenda("polvora", type) != 0) {
            lore24.add(" ");
            lore24.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("polvora", type)));
            lore24.add("§7Quantia: §a" + LojaAPI.itemQuantia("polvora", type));
            lore24.add(" ");
            lore24.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore24.add(" ");
            polvoraMeta.setLore(lore24);
        }
        polvora.setItemMeta(polvoraMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(40, polvora);


        final ItemStack carnepodre = new ItemStack(Material.ROTTEN_FLESH);
        final ItemMeta carnepodreMeta = carnepodre.getItemMeta();
        carnepodreMeta.setDisplayName("§eCarne podre");
        final ArrayList<String> lore23 = new ArrayList<String>();
        if (LojaAPI.valorVenda("carne_podre", type) != 0 && LojaAPI.valorCompra("carne_podre", type) != 0) {
            lore23.add(" ");
            lore23.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("carne_podre", type)));
            lore23.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("carne_podre", type)));
            lore23.add("§7Quantia: §a" + LojaAPI.itemQuantia("carne_podre", type));
            lore23.add(" ");
            lore23.add(" ");
            lore23.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore23.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            carnepodreMeta.setLore(lore23);
        } else if (LojaAPI.valorCompra("carne_podre", type) != 0) {
            lore23.add(" ");
            lore23.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("carne_podre", type)));
            lore23.add("§7Quantia: §a" + LojaAPI.itemQuantia("carne_podre", type));
            lore23.add(" ");
            lore23.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore23.add(" ");
            carnepodreMeta.setLore(lore23);
        } else if (LojaAPI.valorVenda("carne_podre", type) != 0) {
            lore23.add(" ");
            lore23.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("carne_podre", type)));
            lore23.add("§7Quantia: §a" + LojaAPI.itemQuantia("carne_podre", type));
            lore23.add(" ");
            lore23.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore23.add(" ");
            carnepodreMeta.setLore(lore23);
        }
        carnepodre.setItemMeta(carnepodreMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(39, carnepodre);


        final ItemStack olhoaranha = new ItemStack(Material.SPIDER_EYE);
        final ItemMeta olhoaranhaMeta = olhoaranha.getItemMeta();
        olhoaranhaMeta.setDisplayName("§eOlho de aranha");
        final ArrayList<String> lore22 = new ArrayList<String>();
        if (LojaAPI.valorVenda("olho_de_aranha", type) != 0 && LojaAPI.valorCompra("olho_de_aranha", type) != 0) {
            lore22.add(" ");
            lore22.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("olho_de_aranha", type)));
            lore22.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("olho_de_aranha", type)));
            lore22.add("§7Quantia: §a" + LojaAPI.itemQuantia("olho_de_aranha", type));
            lore22.add(" ");
            lore22.add(" ");
            lore22.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore22.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            olhoaranhaMeta.setLore(lore22);
        } else if (LojaAPI.valorCompra("olho_de_aranha", type) != 0) {
            lore22.add(" ");
            lore22.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("olho_de_aranha", type)));
            lore22.add("§7Quantia: §a" + LojaAPI.itemQuantia("olho_de_aranha", type));
            lore22.add(" ");
            lore22.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore22.add(" ");
            olhoaranhaMeta.setLore(lore22);
        } else if (LojaAPI.valorVenda("olho_de_aranha", type) != 0) {
            lore22.add(" ");
            lore22.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("olho_de_aranha", type)));
            lore22.add("§7Quantia: §a" + LojaAPI.itemQuantia("olho_de_aranha", type));
            lore22.add(" ");
            lore22.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore22.add(" ");
            olhoaranhaMeta.setLore(lore22);
        }
        olhoaranha.setItemMeta(olhoaranhaMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(38, olhoaranha);


        final ItemStack osso = new ItemStack(Material.BONE);
        final ItemMeta ossoMeta = osso.getItemMeta();
        ossoMeta.setDisplayName("§eOsso");
        final ArrayList<String> lore21 = new ArrayList<String>();
        if (LojaAPI.valorVenda("osso", type) != 0 && LojaAPI.valorCompra("osso", type) != 0) {
            lore21.add(" ");
            lore21.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("osso", type)));
            lore21.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("osso", type)));
            lore21.add("§7Quantia: §a" + LojaAPI.itemQuantia("osso", type));
            lore21.add(" ");
            lore21.add(" ");
            lore21.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore21.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            ossoMeta.setLore(lore21);
        } else if (LojaAPI.valorCompra("osso", type) != 0) {
            lore21.add(" ");
            lore21.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("osso", type)));
            lore21.add("§7Quantia: §a" + LojaAPI.itemQuantia("osso", type));
            lore21.add(" ");
            lore21.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore21.add(" ");
            ossoMeta.setLore(lore21);
        } else if (LojaAPI.valorVenda("osso", type) != 0) {
            lore21.add(" ");
            lore21.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("osso", type)));
            lore21.add("§7Quantia: §a" + LojaAPI.itemQuantia("osso", type));
            lore21.add(" ");
            lore21.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore21.add(" ");
            ossoMeta.setLore(lore21);
        }
        osso.setItemMeta(ossoMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(37, osso);


        final ItemStack linha = new ItemStack(Material.STRING);
        final ItemMeta linhaMeta = linha.getItemMeta();
        linhaMeta.setDisplayName("§eLinha");
        final ArrayList<String> lore20 = new ArrayList<String>();
        if (LojaAPI.valorVenda("linha", type) != 0 && LojaAPI.valorCompra("linha", type) != 0) {
            lore20.add(" ");
            lore20.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("linha", type)));
            lore20.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("linha", type)));
            lore20.add("§7Quantia: §a" + LojaAPI.itemQuantia("linha", type));
            lore20.add(" ");
            lore20.add(" ");
            lore20.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore20.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            linhaMeta.setLore(lore20);
        } else if (LojaAPI.valorCompra("linha", type) != 0) {
            lore20.add(" ");
            lore20.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("linha", type)));
            lore20.add("§7Quantia: §a" + LojaAPI.itemQuantia("linha", type));
            lore20.add(" ");
            lore20.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore20.add(" ");
            linhaMeta.setLore(lore20);
        } else if (LojaAPI.valorVenda("linha", type) != 0) {
            lore20.add(" ");
            lore20.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("linha", type)));
            lore20.add("§7Quantia: §a" + LojaAPI.itemQuantia("linha", type));
            lore20.add(" ");
            lore20.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore20.add(" ");
            linhaMeta.setLore(lore20);
        }
        linha.setItemMeta(linhaMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(34, linha);


        final ItemStack couro = new ItemStack(Material.LEATHER);
        final ItemMeta couroMeta = couro.getItemMeta();
        couroMeta.setDisplayName("§eCouro");
        final ArrayList<String> lore19 = new ArrayList<String>();
        if (LojaAPI.valorVenda("couro", type) != 0 && LojaAPI.valorCompra("couro", type) != 0) {
            lore19.add(" ");
            lore19.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("couro", type)));
            lore19.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("couro", type)));
            lore19.add("§7Quantia: §a" + LojaAPI.itemQuantia("couro", type));
            lore19.add(" ");
            lore19.add(" ");
            lore19.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore19.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            couroMeta.setLore(lore19);
        } else if (LojaAPI.valorCompra("couro", type) != 0) {
            lore19.add(" ");
            lore19.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("couro", type)));
            lore19.add("§7Quantia: §a" + LojaAPI.itemQuantia("couro", type));
            lore19.add(" ");
            lore19.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore19.add(" ");
            couroMeta.setLore(lore19);
        } else if (LojaAPI.valorVenda("couro", type) != 0) {
            lore19.add(" ");
            lore19.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("couro", type)));
            lore19.add("§7Quantia: §a" + LojaAPI.itemQuantia("couro", type));
            lore19.add(" ");
            lore19.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore19.add(" ");
            couroMeta.setLore(lore19);
        }
        couro.setItemMeta(couroMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(33, couro);


        final ItemStack sementecacau = new ItemStack(Material.COCOA_BEANS);
        final ItemMeta sementecacauMeta = sementecacau.getItemMeta();
        sementecacauMeta.setDisplayName("§eSementes de cacau");
        final ArrayList<String> lore18 = new ArrayList<String>();
        if (LojaAPI.valorVenda("semente_de_cacau", type) != 0 && LojaAPI.valorCompra("semente_de_cacau", type) != 0) {
            lore18.add(" ");
            lore18.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("semente_de_cacau", type)));
            lore18.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("semente_de_cacau", type)));
            lore18.add("§7Quantia: §a" + LojaAPI.itemQuantia("semente_de_cacau", type));
            lore18.add(" ");
            lore18.add(" ");
            lore18.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore18.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            sementecacauMeta.setLore(lore18);
        } else if (LojaAPI.valorCompra("semente_de_cacau", type) != 0) {
            lore18.add(" ");
            lore18.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("semente_de_cacau", type)));
            lore18.add("§7Quantia: §a" + LojaAPI.itemQuantia("semente_de_cacau", type));
            lore18.add(" ");
            lore18.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore18.add(" ");
            sementecacauMeta.setLore(lore18);
        } else if (LojaAPI.valorVenda("semente_de_cacau", type) != 0) {
            lore18.add(" ");
            lore18.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("semente_de_cacau", type)));
            lore18.add("§7Quantia: §a" + LojaAPI.itemQuantia("semente_de_cacau", type));
            lore18.add(" ");
            lore18.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore18.add(" ");
            sementecacauMeta.setLore(lore18);
        }
        sementecacau.setItemMeta(sementecacauMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(32, sementecacau);


        final ItemStack trigo = new ItemStack(Material.WHEAT);
        final ItemMeta trigoMeta = trigo.getItemMeta();
        trigoMeta.setDisplayName("§eTrigo");
        final ArrayList<String> lore17 = new ArrayList<String>();
        if (LojaAPI.valorVenda("trigo", type) != 0 && LojaAPI.valorCompra("trigo", type) != 0) {
            lore17.add(" ");
            lore17.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("trigo", type)));
            lore17.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("trigo", type)));
            lore17.add("§7Quantia: §a" + LojaAPI.itemQuantia("trigo", type));
            lore17.add(" ");
            lore17.add(" ");
            lore17.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore17.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            trigoMeta.setLore(lore17);
        } else if (LojaAPI.valorCompra("trigo", type) != 0) {
            lore17.add(" ");
            lore17.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("trigo", type)));
            lore17.add("§7Quantia: §a" + LojaAPI.itemQuantia("trigo", type));
            lore17.add(" ");
            lore17.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore17.add(" ");
            trigoMeta.setLore(lore17);
        } else if (LojaAPI.valorVenda("trigo", type) != 0) {
            lore17.add(" ");
            lore17.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("trigo", type)));
            lore17.add("§7Quantia: §a" + LojaAPI.itemQuantia("trigo", type));
            lore17.add(" ");
            lore17.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore17.add(" ");
            trigoMeta.setLore(lore17);
        }
        trigo.setItemMeta(trigoMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(31, trigo);


        final ItemStack cactus = new ItemStack(Material.CACTUS);
        final ItemMeta cactusMeta = cactus.getItemMeta();
        cactusMeta.setDisplayName("§eCacto");
        final ArrayList<String> lore16 = new ArrayList<String>();
        if (LojaAPI.valorVenda("cacto", type) != 0 && LojaAPI.valorCompra("cacto", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("cacto", type)));
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("cacto", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("cacto", type));
            lore16.add(" ");
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            cactusMeta.setLore(lore16);
        } else if (LojaAPI.valorCompra("cacto", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("cacto", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("cacto", type));
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add(" ");
            cactusMeta.setLore(lore16);
        } else if (LojaAPI.valorVenda("cacto", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("cacto", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("cacto", type));
            lore16.add(" ");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore16.add(" ");
            cactusMeta.setLore(lore16);
        }
        cactus.setItemMeta(cactusMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(30, cactus);


        final ItemStack sugarcane = new ItemStack(Material.SUGAR_CANE);
        final ItemMeta sugarcaneMeta = sugarcane.getItemMeta();
        sugarcaneMeta.setDisplayName("§eCana-de-açúcar");
        final ArrayList<String> lore15 = new ArrayList<String>();
        if (LojaAPI.valorVenda("cana_de_açucar", type) != 0 && LojaAPI.valorCompra("cana_de_açucar", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("cana_de_açucar", type)));
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("cana_de_açucar", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("cana_de_açucar", type));
            lore15.add(" ");
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            sugarcaneMeta.setLore(lore15);
        } else if (LojaAPI.valorCompra("cana_de_açucar", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("cana_de_açucar", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("cana_de_açucar", type));
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add(" ");
            sugarcaneMeta.setLore(lore15);
        } else if (LojaAPI.valorVenda("cana_de_açucar", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("cana_de_açucar", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("cana_de_açucar", type));
            lore15.add(" ");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore15.add(" ");
            sugarcaneMeta.setLore(lore15);
        }
        sugarcane.setItemMeta(sugarcaneMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(29, sugarcane);


        final ItemStack netherwart = new ItemStack(Material.NETHER_WART);
        final ItemMeta netherwartMeta = netherwart.getItemMeta();
        netherwartMeta.setDisplayName("§eFungo do nether");
        final ArrayList<String> lore14 = new ArrayList<String>();
        if (LojaAPI.valorVenda("fungo_do_nether", type) != 0 && LojaAPI.valorCompra("fungo_do_nether", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("fungo_do_nether", type)));
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("fungo_do_nether", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("fungo_do_nether", type));
            lore14.add(" ");
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            netherwartMeta.setLore(lore14);
        } else if (LojaAPI.valorCompra("fungo_do_nether", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("fungo_do_nether", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("fungo_do_nether", type));
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add(" ");
            netherwartMeta.setLore(lore14);
        } else if (LojaAPI.valorVenda("fungo_do_nether", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("fungo_do_nether", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("fungo_do_nether", type));
            lore14.add(" ");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore14.add(" ");
            netherwartMeta.setLore(lore14);
        }
        netherwart.setItemMeta(netherwartMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(28, netherwart);


        final ItemStack coalblock = new ItemStack(Material.COAL_ORE);
        final ItemMeta coalblockMeta = coalblock.getItemMeta();
        coalblockMeta.setDisplayName("§eMinério de carvão");
        final ArrayList<String> lore13 = new ArrayList<String>();
        if (LojaAPI.valorVenda("minerio_de_carvao", type) != 0 && LojaAPI.valorCompra("minerio_de_carvao", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("minerio_de_carvao", type)));
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("minerio_de_carvao", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("minerio_de_carvao", type));
            lore13.add(" ");
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            coalblockMeta.setLore(lore13);
        } else if (LojaAPI.valorCompra("minerio_de_carvao", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("minerio_de_carvao", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("minerio_de_carvao", type));
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add(" ");
            coalblockMeta.setLore(lore13);
        } else if (LojaAPI.valorVenda("minerio_de_carvao", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("minerio_de_carvao", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("minerio_de_carvao", type));
            lore13.add(" ");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore13.add(" ");
            coalblockMeta.setLore(lore13);
        }
        coalblock.setItemMeta(coalblockMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(25, coalblock);


        final ItemStack redstoneblock = new ItemStack(Material.REDSTONE_ORE);
        final ItemMeta redstoneblockMeta = redstoneblock.getItemMeta();
        redstoneblockMeta.setDisplayName("§eMinério de redstone");
        final ArrayList<String> lore12 = new ArrayList<String>();
        if (LojaAPI.valorVenda("minerio_de_redstone", type) != 0 && LojaAPI.valorCompra("minerio_de_redstone", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("minerio_de_redstone", type)));
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("minerio_de_redstone", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("minerio_de_redstone", type));
            lore12.add(" ");
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            redstoneblockMeta.setLore(lore12);
        } else if (LojaAPI.valorCompra("minerio_de_redstone", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("minerio_de_redstone", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("minerio_de_redstone", type));
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add(" ");
            redstoneblockMeta.setLore(lore12);
        } else if (LojaAPI.valorVenda("minerio_de_redstone", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("minerio_de_redstone", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("minerio_de_redstone", type));
            lore12.add(" ");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore12.add(" ");
            redstoneblockMeta.setLore(lore12);
        }
        redstoneblock.setItemMeta(redstoneblockMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(24, redstoneblock);


        final ItemStack lapisblock = new ItemStack(Material.LAPIS_ORE);
        final ItemMeta lapisblockMeta = lapisblock.getItemMeta();
        lapisblockMeta.setDisplayName("§eMinério de lápis-lazúli");
        final ArrayList<String> lore11 = new ArrayList<String>();
        if (LojaAPI.valorVenda("minerio_de_lapis_lazuli", type) != 0 && LojaAPI.valorCompra("minerio_de_lapis_lazuli", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("minerio_de_lapis_lazuli", type)));
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("minerio_de_lapis_lazuli", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("minerio_de_lapis_lazuli", type));
            lore11.add(" ");
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lapisblockMeta.setLore(lore11);
        } else if (LojaAPI.valorCompra("minerio_de_lapis_lazuli", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("minerio_de_lapis_lazuli", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("minerio_de_lapis_lazuli", type));
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add(" ");
            lapisblockMeta.setLore(lore11);
        } else if (LojaAPI.valorVenda("minerio_de_lapis_lazuli", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("minerio_de_lapis_lazuli", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("minerio_de_lapis_lazuli", type));
            lore11.add(" ");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore11.add(" ");
            lapisblockMeta.setLore(lore11);
        }
        lapisblock.setItemMeta(lapisblockMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(23, lapisblock);


        final ItemStack ironblock = new ItemStack(Material.IRON_ORE);
        final ItemMeta ironblockMeta = ironblock.getItemMeta();
        ironblockMeta.setDisplayName("§eMinério de ferro");
        final ArrayList<String> lore10 = new ArrayList<String>();
        if (LojaAPI.valorVenda("minerio_de_ferro", type) != 0 && LojaAPI.valorCompra("minerio_de_ferro", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("minerio_de_ferro", type)));
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("minerio_de_ferro", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("minerio_de_ferro", type));
            lore10.add(" ");
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            ironblockMeta.setLore(lore10);
        } else if (LojaAPI.valorCompra("minerio_de_ferro", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("minerio_de_ferro", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("minerio_de_ferro", type));
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add(" ");
            ironblockMeta.setLore(lore10);
        } else if (LojaAPI.valorVenda("minerio_de_ferro", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("minerio_de_ferro", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("minerio_de_ferro", type));
            lore10.add(" ");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore10.add(" ");
            ironblockMeta.setLore(lore10);
        }
        ironblock.setItemMeta(ironblockMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(22, ironblock);


        final ItemStack goldblock = new ItemStack(Material.GOLD_ORE);
        final ItemMeta goldblockMeta = goldblock.getItemMeta();
        goldblockMeta.setDisplayName("§eMinério de ouro");
        final ArrayList<String> lore9 = new ArrayList<String>();
        if (LojaAPI.valorVenda("minerio_de_ouro", type) != 0 && LojaAPI.valorCompra("minerio_de_ouro", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("minerio_de_ouro", type)));
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("minerio_de_ouro", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("minerio_de_ouro", type));
            lore9.add(" ");
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            goldblockMeta.setLore(lore9);
        } else if (LojaAPI.valorCompra("minerio_de_ouro", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("minerio_de_ouro", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("minerio_de_ouro", type));
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add(" ");
            goldblockMeta.setLore(lore9);
        } else if (LojaAPI.valorVenda("minerio_de_ouro", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("minerio_de_ouro", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("minerio_de_ouro", type));
            lore9.add(" ");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore9.add(" ");
            goldblockMeta.setLore(lore9);
        }
        goldblock.setItemMeta(goldblockMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(21, goldblock);


        final ItemStack diamondblock = new ItemStack(Material.DIAMOND_ORE);
        final ItemMeta diamondblockMeta = diamondblock.getItemMeta();
        diamondblockMeta.setDisplayName("§eMinério de diamante");
        final ArrayList<String> lore8 = new ArrayList<String>();
        if (LojaAPI.valorVenda("minerio_de_diamante", type) != 0 && LojaAPI.valorCompra("minerio_de_diamante", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("minerio_de_diamante", type)));
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("minerio_de_diamante", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("minerio_de_diamante", type));
            lore8.add(" ");
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            diamondblockMeta.setLore(lore8);
        } else if (LojaAPI.valorCompra("minerio_de_diamante", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("minerio_de_diamante", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("minerio_de_diamante", type));
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add(" ");
            diamondblockMeta.setLore(lore8);
        } else if (LojaAPI.valorVenda("minerio_de_diamante", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("minerio_de_diamante", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("minerio_de_diamante", type));
            lore8.add(" ");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore8.add(" ");
            diamondblockMeta.setLore(lore8);
        }
        diamondblock.setItemMeta(diamondblockMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(20, diamondblock);


        final ItemStack uranioblock = new ItemStack(Material.EMERALD_ORE);
        final ItemMeta uranioblockMeta = uranioblock.getItemMeta();
        uranioblockMeta.setDisplayName("§eMinério de urânio");
        final ArrayList<String> lore7 = new ArrayList<String>();
        if (LojaAPI.valorVenda("minerio_de_uranio", type) != 0 && LojaAPI.valorCompra("minerio_de_uranio", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("minerio_de_uranio", type)));
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("minerio_de_uranio", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("minerio_de_uranio", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            uranioblockMeta.setLore(lore7);
        } else if (LojaAPI.valorCompra("minerio_de_uranio", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("minerio_de_uranio", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("minerio_de_uranio", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add(" ");
            uranioblockMeta.setLore(lore7);
        } else if (LojaAPI.valorVenda("minerio_de_uranio", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("minerio_de_uranio", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("minerio_de_uranio", type));
            lore7.add(" ");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore7.add(" ");
            uranioblockMeta.setLore(lore7);
        }
        uranioblock.setItemMeta(uranioblockMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(19, uranioblock);


        final ItemStack coal = new ItemStack(Material.COAL);
        final ItemMeta coalMeta = coal.getItemMeta();
        coalMeta.setDisplayName("§eCarvão");
        final ArrayList<String> lore6 = new ArrayList<String>();
        if (LojaAPI.valorVenda("carvao", type) != 0 && LojaAPI.valorCompra("carvao", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("carvao", type)));
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("carvao", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("carvao", type));
            lore6.add(" ");
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            coalMeta.setLore(lore6);
        } else if (LojaAPI.valorCompra("carvao", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("carvao", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("carvao", type));
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add(" ");
            coalMeta.setLore(lore6);
        } else if (LojaAPI.valorVenda("carvao", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("carvao", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("carvao", type));
            lore6.add(" ");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore6.add(" ");
            coalMeta.setLore(lore6);
        }
        coal.setItemMeta(coalMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(16, coal);


        final ItemStack redstone = new ItemStack(Material.REDSTONE);
        final ItemMeta redstoneMeta = redstone.getItemMeta();
        redstoneMeta.setDisplayName("§eRedstone");
        final ArrayList<String> lore5 = new ArrayList<String>();
        if (LojaAPI.valorVenda("redstone", type) != 0 && LojaAPI.valorCompra("redstone", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("redstone", type)));
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("redstone", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("redstone", type));
            lore5.add(" ");
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            redstoneMeta.setLore(lore5);
        } else if (LojaAPI.valorCompra("redstone", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("redstone", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("redstone", type));
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add(" ");
            redstoneMeta.setLore(lore5);
        } else if (LojaAPI.valorVenda("redstone", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("redstone", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("redstone", type));
            lore5.add(" ");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore5.add(" ");
            redstoneMeta.setLore(lore5);
        }
        redstone.setItemMeta(redstoneMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(15, redstone);


        final ItemStack lapis = new ItemStack(Material.LAPIS_LAZULI);
        final ItemMeta lapisMeta = lapis.getItemMeta();
        lapisMeta.setDisplayName("§eLápis-lazúli");
        final ArrayList<String> lore4 = new ArrayList<String>();
        if (LojaAPI.valorVenda("lapis_lazuli", type) != 0 && LojaAPI.valorCompra("lapis_lazuli", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("lapis_lazuli", type)));
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("lapis_lazuli", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("lapis_lazuli", type));
            lore4.add(" ");
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lapisMeta.setLore(lore4);
        } else if (LojaAPI.valorCompra("lapis_lazuli", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("lapis_lazuli", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("lapis_lazuli", type));
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add(" ");
            lapisMeta.setLore(lore4);
        } else if (LojaAPI.valorVenda("lapis_lazuli", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("lapis_lazuli", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("lapis_lazuli", type));
            lore4.add(" ");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore4.add(" ");
            lapisMeta.setLore(lore4);
        }
        lapis.setItemMeta(lapisMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(14, lapis);


        final ItemStack iron = new ItemStack(Material.IRON_INGOT);
        final ItemMeta ironMeta = iron.getItemMeta();
        ironMeta.setDisplayName("§eBarra de ferro");
        final ArrayList<String> lore3 = new ArrayList<String>();
        if (LojaAPI.valorVenda("barra_de_ferro", type) != 0 && LojaAPI.valorCompra("barra_de_ferro", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("barra_de_ferro", type)));
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("barra_de_ferro", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("barra_de_ferro", type));
            lore3.add(" ");
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            ironMeta.setLore(lore3);
        } else if (LojaAPI.valorCompra("barra_de_ferro", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("barra_de_ferro", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("barra_de_ferro", type));
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add(" ");
            ironMeta.setLore(lore3);
        } else if (LojaAPI.valorVenda("barra_de_ferro", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("barra_de_ferro", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("barra_de_ferro", type));
            lore3.add(" ");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore3.add(" ");
            ironMeta.setLore(lore3);
        }
        iron.setItemMeta(ironMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(13, iron);


        final ItemStack gold = new ItemStack(Material.GOLD_INGOT);
        final ItemMeta goldMeta = gold.getItemMeta();
        goldMeta.setDisplayName("§eBarra de ouro");
        final ArrayList<String> lore2 = new ArrayList<String>();
        if (LojaAPI.valorVenda("barra_de_ouro", type) != 0 && LojaAPI.valorCompra("barra_de_ouro", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("barra_de_ouro", type)));
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("barra_de_ouro", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("barra_de_ouro", type));
            lore2.add(" ");
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            goldMeta.setLore(lore2);
        } else if (LojaAPI.valorCompra("barra_de_ouro", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("barra_de_ouro", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("barra_de_ouro", type));
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add(" ");
            goldMeta.setLore(lore2);
        } else if (LojaAPI.valorVenda("barra_de_ouro", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("barra_de_ouro", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("barra_de_ouro", type));
            lore2.add(" ");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore2.add(" ");
            goldMeta.setLore(lore2);
        }
        gold.setItemMeta(goldMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(12, gold);


        final ItemStack diamond = new ItemStack(Material.DIAMOND);
        final ItemMeta diamondMeta = diamond.getItemMeta();
        diamondMeta.setDisplayName("§eDiamante");
        final ArrayList<String> lore1 = new ArrayList<String>();
        if (LojaAPI.valorVenda("diamante", type) != 0 && LojaAPI.valorCompra("diamante", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("diamante", type)));
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("diamante", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("diamante", type));
            lore1.add(" ");
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            diamondMeta.setLore(lore1);
        } else if (LojaAPI.valorCompra("diamante", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("diamante", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("diamante", type));
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add(" ");
            diamondMeta.setLore(lore1);
        } else if (LojaAPI.valorVenda("diamante", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("diamante", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("diamante", type));
            lore1.add(" ");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore1.add(" ");
            diamondMeta.setLore(lore1);
        }
        diamond.setItemMeta(diamondMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(11, diamond);


        final ItemStack uranio = new ItemStack(Material.EMERALD);
        final ItemMeta uranioMeta = uranio.getItemMeta();
        uranioMeta.setDisplayName("§eUrânio");
        final ArrayList<String> lore = new ArrayList<String>();
        if (LojaAPI.valorVenda("uranio", type) != 0 && LojaAPI.valorCompra("uranio", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("uranio", type)));
            lore.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("uranio", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("uranio", type));
            lore.add(" ");
            lore.add(" ");
            lore.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            uranioMeta.setLore(lore);
        } else if (LojaAPI.valorCompra("uranio", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("uranio", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("uranio", type));
            lore.add(" ");
            lore.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore.add(" ");
            uranioMeta.setLore(lore);
        } else if (LojaAPI.valorVenda("uranio", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("uranio", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("uranio", type));
            lore.add(" ");
            lore.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore.add(" ");
            uranioMeta.setLore(lore);
        }
        uranio.setItemMeta(uranioMeta);
        INVENTORY_LOJAMATERIAIS1.setItem(10, uranio);

        return INVENTORY_LOJAMATERIAIS1;
    }

    public static Inventory buildLojaArmas(final String type) {
        final Inventory INVENTORY_LOJAARMAS = Bukkit.createInventory(null, 5 * 9, "Armas: ");

        final ItemStack regen = new ItemStack(Material.SPLASH_POTION, 1, (short) 16385);
        final ItemMeta regenMeta = regen.getItemMeta();
        regenMeta.setDisplayName("§ePoção arremessável de regeneração");
        final ArrayList<String> lore16 = new ArrayList<String>();
        if (LojaAPI.valorVenda("poça_arremesavel_de_renegeraçao", type) != 0 && LojaAPI.valorCompra("poça_arremesavel_de_renegeraçao", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("poça_arremesavel_de_renegeraçao", type)));
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("poça_arremesavel_de_renegeraçao", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("poça_arremesavel_de_renegeraçao", type));
            lore16.add(" ");
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            regenMeta.setLore(lore16);
        } else if (LojaAPI.valorCompra("poça_arremesavel_de_renegeraçao", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("poça_arremesavel_de_renegeraçao", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("poça_arremesavel_de_renegeraçao", type));
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add(" ");
            regenMeta.setLore(lore16);
        } else if (LojaAPI.valorVenda("poça_arremesavel_de_renegeraçao", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("poça_arremesavel_de_renegeraçao", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("poça_arremesavel_de_renegeraçao", type));
            lore16.add(" ");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore16.add(" ");
            regenMeta.setLore(lore16);
        }
        regen.setItemMeta(regenMeta);
        INVENTORY_LOJAARMAS.setItem(32, regen);


        final ItemStack goldenapple2 = new ItemStack(Material.ENCHANTED_GOLDEN_APPLE);
        final ItemMeta goldenapple2Meta = goldenapple2.getItemMeta();
        goldenapple2Meta.setDisplayName("§eMaçã dourada encantada");
        final ArrayList<String> lore15 = new ArrayList<String>();
        if (LojaAPI.valorVenda("maça_dourada_encantada", type) != 0 && LojaAPI.valorCompra("maça_dourada_encantada", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("maça_dourada_encantada", type)));
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("maça_dourada_encantada", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("maça_dourada_encantada", type));
            lore15.add(" ");
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            goldenapple2Meta.setLore(lore15);
        } else if (LojaAPI.valorCompra("maça_dourada_encantada", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("maça_dourada_encantada", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("maça_dourada_encantada", type));
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add(" ");
            goldenapple2Meta.setLore(lore15);
        } else if (LojaAPI.valorVenda("maça_dourada_encantada", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("maça_dourada_encantada", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("maça_dourada_encantada", type));
            lore15.add(" ");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore15.add(" ");
            goldenapple2Meta.setLore(lore15);
        }
        goldenapple2.setItemMeta(goldenapple2Meta);
        INVENTORY_LOJAARMAS.setItem(31, goldenapple2);


        final ItemStack goldenapple = new ItemStack(Material.GOLDEN_APPLE);
        final ItemMeta goldenappleMeta = goldenapple.getItemMeta();
        goldenappleMeta.setDisplayName("§eMaçã dourada");
        final ArrayList<String> lore14 = new ArrayList<String>();
        if (LojaAPI.valorVenda("maça_dourada", type) != 0 && LojaAPI.valorCompra("maça_dourada", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("maça_dourada", type)));
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("maça_dourada", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("maça_dourada", type));
            lore14.add(" ");
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            goldenappleMeta.setLore(lore14);
        } else if (LojaAPI.valorCompra("maça_dourada", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("maça_dourada", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("maça_dourada", type));
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add(" ");
            goldenappleMeta.setLore(lore14);
        } else if (LojaAPI.valorVenda("maça_dourada", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("maça_dourada", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("maça_dourada", type));
            lore14.add(" ");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore14.add(" ");
            goldenappleMeta.setLore(lore14);
        }
        goldenapple.setItemMeta(goldenappleMeta);
        INVENTORY_LOJAARMAS.setItem(30, goldenapple);


        final ItemStack totem = new ItemStack(Material.TOTEM_OF_UNDYING);
        final ItemMeta totemMeta = totem.getItemMeta();
        totemMeta.setDisplayName("§eTotem da imortalidade");
        final ArrayList<String> lore13 = new ArrayList<String>();
        if (LojaAPI.valorVenda("totem", type) != 0 && LojaAPI.valorCompra("totem", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("totem", type)));
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("totem", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("totem", type));
            lore13.add(" ");
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            totemMeta.setLore(lore13);
        } else if (LojaAPI.valorCompra("totem", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("totem", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("totem", type));
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add(" ");
            totemMeta.setLore(lore13);
        } else if (LojaAPI.valorVenda("totem", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("totem", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("totem", type));
            lore13.add(" ");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore13.add(" ");
            totemMeta.setLore(lore13);
        }
        totem.setItemMeta(totemMeta);
        INVENTORY_LOJAARMAS.setItem(25, totem);


        final ItemStack trident = new ItemStack(Material.TRIDENT);
        final ItemMeta tridentMeta = trident.getItemMeta();
        tridentMeta.setDisplayName("§eTridente");
        final ArrayList<String> lore12 = new ArrayList<String>();
        if (LojaAPI.valorVenda("tridente", type) != 0 && LojaAPI.valorCompra("tridente", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tridente", type)));
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tridente", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("tridente", type));
            lore12.add(" ");
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            tridentMeta.setLore(lore12);
        } else if (LojaAPI.valorCompra("tridente", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tridente", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("tridente", type));
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add(" ");
            tridentMeta.setLore(lore12);
        } else if (LojaAPI.valorVenda("tridente", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tridente", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("tridente", type));
            lore12.add(" ");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore12.add(" ");
            tridentMeta.setLore(lore12);
        }
        trident.setItemMeta(tridentMeta);
        INVENTORY_LOJAARMAS.setItem(24, trident);


        final ItemStack ironsword = new ItemStack(Material.IRON_SWORD);
        final ItemMeta ironswordMeta = ironsword.getItemMeta();
        ironswordMeta.setDisplayName("§eBotas de ferro");
        final ArrayList<String> lore11 = new ArrayList<String>();
        if (LojaAPI.valorVenda("botas_de_ferro", type) != 0 && LojaAPI.valorCompra("botas_de_ferro", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("botas_de_ferro", type)));
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("botas_de_ferro", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("botas_de_ferro", type));
            lore11.add(" ");
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            ironswordMeta.setLore(lore11);
        } else if (LojaAPI.valorCompra("botas_de_ferro", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("botas_de_ferro", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("botas_de_ferro", type));
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add(" ");
            ironswordMeta.setLore(lore11);
        } else if (LojaAPI.valorVenda("botas_de_ferro", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("botas_de_ferro", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("botas_de_ferro", type));
            lore11.add(" ");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore11.add(" ");
            ironswordMeta.setLore(lore11);
        }
        ironsword.setItemMeta(ironswordMeta);
        INVENTORY_LOJAARMAS.setItem(23, ironsword);


        final ItemStack botairon = new ItemStack(Material.IRON_BOOTS);
        final ItemMeta botaironMeta = botairon.getItemMeta();
        botaironMeta.setDisplayName("§eBotas de ferro");
        final ArrayList<String> lore10 = new ArrayList<String>();
        if (LojaAPI.valorVenda("botas_de_ferro", type) != 0 && LojaAPI.valorCompra("botas_de_ferro", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("botas_de_ferro", type)));
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("botas_de_ferro", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("botas_de_ferro", type));
            lore10.add(" ");
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            botaironMeta.setLore(lore10);
        } else if (LojaAPI.valorCompra("botas_de_ferro", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("botas_de_ferro", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("botas_de_ferro", type));
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add(" ");
            botaironMeta.setLore(lore10);
        } else if (LojaAPI.valorVenda("botas_de_ferro", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("botas_de_ferro", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("botas_de_ferro", type));
            lore10.add(" ");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore10.add(" ");
            botaironMeta.setLore(lore10);
        }
        botairon.setItemMeta(botaironMeta);
        INVENTORY_LOJAARMAS.setItem(22, botairon);


        final ItemStack calcairon = new ItemStack(Material.IRON_LEGGINGS);
        final ItemMeta calcaironMeta = calcairon.getItemMeta();
        calcaironMeta.setDisplayName("§eCalças de ferro");
        final ArrayList<String> lore9 = new ArrayList<String>();
        if (LojaAPI.valorVenda("calcas_de_ferro", type) != 0 && LojaAPI.valorCompra("calcas_de_ferro", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("calcas_de_ferro", type)));
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("calcas_de_ferro", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("calcas_de_ferro", type));
            lore9.add(" ");
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            calcaironMeta.setLore(lore9);
        } else if (LojaAPI.valorCompra("calcas_de_ferro", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("calcas_de_ferro", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("calcas_de_ferro", type));
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add(" ");
            calcaironMeta.setLore(lore9);
        } else if (LojaAPI.valorVenda("calcas_de_ferro", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("calcas_de_ferro", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("calcas_de_ferro", type));
            lore9.add(" ");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore9.add(" ");
            calcaironMeta.setLore(lore9);
        }
        calcairon.setItemMeta(calcaironMeta);
        INVENTORY_LOJAARMAS.setItem(21, calcairon);


        final ItemStack peitoraliron = new ItemStack(Material.IRON_CHESTPLATE);
        final ItemMeta peitoralironMeta = peitoraliron.getItemMeta();
        peitoralironMeta.setDisplayName("§ePeitoral de ferro");
        final ArrayList<String> lore8 = new ArrayList<String>();
        if (LojaAPI.valorVenda("peitoral_de_ferro", type) != 0 && LojaAPI.valorCompra("peitoral_de_ferro", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("peitoral_de_ferro", type)));
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("peitoral_de_ferro", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("peitoral_de_ferro", type));
            lore8.add(" ");
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            peitoralironMeta.setLore(lore8);
        } else if (LojaAPI.valorCompra("peitoral_de_ferro", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("peitoral_de_ferro", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("peitoral_de_ferro", type));
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add(" ");
            peitoralironMeta.setLore(lore8);
        } else if (LojaAPI.valorVenda("peitoral_de_ferro", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("peitoral_de_ferro", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("peitoral_de_ferro", type));
            lore8.add(" ");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore8.add(" ");
            peitoralironMeta.setLore(lore8);
        }
        peitoraliron.setItemMeta(peitoralironMeta);
        INVENTORY_LOJAARMAS.setItem(20, peitoraliron);


        final ItemStack capaceteiron = new ItemStack(Material.IRON_HELMET);
        final ItemMeta capaceteironMeta = capaceteiron.getItemMeta();
        capaceteironMeta.setDisplayName("§eCapacete de ferro");
        final ArrayList<String> lore7 = new ArrayList<String>();
        if (LojaAPI.valorVenda("capacete_de_ferro", type) != 0 && LojaAPI.valorCompra("capacete_de_ferro", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("capacete_de_ferro", type)));
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("capacete_de_ferro", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("capacete_de_ferro", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            capaceteironMeta.setLore(lore7);
        } else if (LojaAPI.valorCompra("capacete_de_ferro", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("capacete_de_ferro", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("capacete_de_ferro", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add(" ");
            capaceteironMeta.setLore(lore7);
        } else if (LojaAPI.valorVenda("capacete_de_ferro", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("capacete_de_ferro", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("capacete_de_ferro", type));
            lore7.add(" ");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore7.add(" ");
            capaceteironMeta.setLore(lore7);
        }
        capaceteiron.setItemMeta(capaceteironMeta);
        INVENTORY_LOJAARMAS.setItem(19, capaceteiron);


        final ItemStack shield = new ItemStack(Material.SHIELD);
        final ItemMeta shieldMeta = shield.getItemMeta();
        shieldMeta.setDisplayName("§eEscudo");
        final ArrayList<String> lore6 = new ArrayList<String>();
        if (LojaAPI.valorVenda("escudo", type) != 0 && LojaAPI.valorCompra("escudo", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("escudo", type)));
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("escudo", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("escudo", type));
            lore6.add(" ");
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            shieldMeta.setLore(lore6);
        } else if (LojaAPI.valorCompra("escudo", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("escudo", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("escudo", type));
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add(" ");
            shieldMeta.setLore(lore6);
        } else if (LojaAPI.valorVenda("escudo", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("escudo", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("escudo", type));
            lore6.add(" ");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore6.add(" ");
            shieldMeta.setLore(lore6);
        }
        shield.setItemMeta(shieldMeta);
        INVENTORY_LOJAARMAS.setItem(16, shield);


        final ItemStack arco = new ItemStack(Material.BOW);
        final ItemMeta arcoMeta = arco.getItemMeta();
        arcoMeta.setDisplayName("§eArco");
        final ArrayList<String> lore5 = new ArrayList<String>();
        if (LojaAPI.valorVenda("arco", type) != 0 && LojaAPI.valorCompra("arco", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("arco", type)));
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("arco", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("arco", type));
            lore5.add(" ");
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            arcoMeta.setLore(lore5);
        } else if (LojaAPI.valorCompra("arco", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("arco", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("arco", type));
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add(" ");
            arcoMeta.setLore(lore5);
        } else if (LojaAPI.valorVenda("arco", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("arco", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("arco", type));
            lore5.add(" ");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore5.add(" ");
            arcoMeta.setLore(lore5);
        }
        arco.setItemMeta(arcoMeta);
        INVENTORY_LOJAARMAS.setItem(15, arco);


        final ItemStack swordima = new ItemStack(Material.DIAMOND_SWORD);
        final ItemMeta swordimaMeta = swordima.getItemMeta();
        swordimaMeta.setDisplayName("§eEspada de diamante");
        final ArrayList<String> lore4 = new ArrayList<String>();
        if (LojaAPI.valorVenda("espada_de_diamante", type) != 0 && LojaAPI.valorCompra("espada_de_diamante", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("espada_de_diamante", type)));
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("espada_de_diamante", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("espada_de_diamante", type));
            lore4.add(" ");
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            swordimaMeta.setLore(lore4);
        } else if (LojaAPI.valorCompra("espada_de_diamante", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("espada_de_diamante", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("espada_de_diamante", type));
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add(" ");
            swordimaMeta.setLore(lore4);
        } else if (LojaAPI.valorVenda("espada_de_diamante", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("espada_de_diamante", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("espada_de_diamante", type));
            lore4.add(" ");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore4.add(" ");
            swordimaMeta.setLore(lore4);
        }
        swordima.setItemMeta(swordimaMeta);
        INVENTORY_LOJAARMAS.setItem(14, swordima);


        final ItemStack botadima = new ItemStack(Material.DIAMOND_BOOTS);
        final ItemMeta botadimaMeta = botadima.getItemMeta();
        botadimaMeta.setDisplayName("§eBotas de diamante");
        final ArrayList<String> lore3 = new ArrayList<String>();
        if (LojaAPI.valorVenda("botas_de_diamante", type) != 0 && LojaAPI.valorCompra("botas_de_diamante", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("botas_de_diamante", type)));
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("botas_de_diamante", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("botas_de_diamante", type));
            lore3.add(" ");
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            botadimaMeta.setLore(lore3);
        } else if (LojaAPI.valorCompra("botas_de_diamante", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("botas_de_diamante", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("botas_de_diamante", type));
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add(" ");
            botadimaMeta.setLore(lore3);
        } else if (LojaAPI.valorVenda("botas_de_diamante", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("botas_de_diamante", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("botas_de_diamante", type));
            lore3.add(" ");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore3.add(" ");
            botadimaMeta.setLore(lore3);
        }
        botadima.setItemMeta(botadimaMeta);
        INVENTORY_LOJAARMAS.setItem(13, botadima);


        final ItemStack calcadima = new ItemStack(Material.DIAMOND_LEGGINGS);
        final ItemMeta calcadimaMeta = calcadima.getItemMeta();
        calcadimaMeta.setDisplayName("§eCalças de diamante");
        final ArrayList<String> lore2 = new ArrayList<String>();
        if (LojaAPI.valorVenda("calças_de_diamante", type) != 0 && LojaAPI.valorCompra("calças_de_diamante", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("calças_de_diamante", type)));
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("calças_de_diamante", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("calças_de_diamante", type));
            lore2.add(" ");
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            calcadimaMeta.setLore(lore2);
        } else if (LojaAPI.valorCompra("calças_de_diamante", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("calças_de_diamante", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("calças_de_diamante", type));
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add(" ");
            calcadimaMeta.setLore(lore2);
        } else if (LojaAPI.valorVenda("calças_de_diamante", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("calças_de_diamante", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("calças_de_diamante", type));
            lore2.add(" ");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore2.add(" ");
            calcadimaMeta.setLore(lore2);
        }
        calcadima.setItemMeta(calcadimaMeta);
        INVENTORY_LOJAARMAS.setItem(12, calcadima);


        final ItemStack peitoraldima = new ItemStack(Material.DIAMOND_CHESTPLATE);
        final ItemMeta peitoraldimaMeta = peitoraldima.getItemMeta();
        peitoraldimaMeta.setDisplayName("§ePeitoral de diamante");
        final ArrayList<String> lore1 = new ArrayList<String>();
        if (LojaAPI.valorVenda("peitoral_de_diamante", type) != 0 && LojaAPI.valorCompra("peitoral_de_diamante", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("peitoral_de_diamante", type)));
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("peitoral_de_diamante", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("peitoral_de_diamante", type));
            lore1.add(" ");
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            peitoraldimaMeta.setLore(lore1);
        } else if (LojaAPI.valorCompra("peitoral_de_diamante", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("peitoral_de_diamante", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("peitoral_de_diamante", type));
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add(" ");
            peitoraldimaMeta.setLore(lore1);
        } else if (LojaAPI.valorVenda("peitoral_de_diamante", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("peitoral_de_diamante", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("peitoral_de_diamante", type));
            lore1.add(" ");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore1.add(" ");
            peitoraldimaMeta.setLore(lore1);
        }
        peitoraldima.setItemMeta(peitoraldimaMeta);
        INVENTORY_LOJAARMAS.setItem(11, peitoraldima);


        final ItemStack capacetedima = new ItemStack(Material.DIAMOND_HELMET);
        final ItemMeta capacetedimaMeta = capacetedima.getItemMeta();
        capacetedimaMeta.setDisplayName("§eCapacete de diamante");
        final ArrayList<String> lore = new ArrayList<String>();
        if (LojaAPI.valorVenda("capacete_de_diamante", type) != 0 && LojaAPI.valorCompra("capacete_de_diamante", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("capacete_de_diamante", type)));
            lore.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("capacete_de_diamante", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("capacete_de_diamante", type));
            lore.add(" ");
            lore.add(" ");
            lore.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            capacetedimaMeta.setLore(lore);
        } else if (LojaAPI.valorCompra("capacete_de_diamante", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("capacete_de_diamante", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("capacete_de_diamante", type));
            lore.add(" ");
            lore.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore.add(" ");
            capacetedimaMeta.setLore(lore);
        } else if (LojaAPI.valorVenda("capacete_de_diamante", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("capacete_de_diamante", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("capacete_de_diamante", type));
            lore.add(" ");
            lore.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore.add(" ");
            capacetedimaMeta.setLore(lore);
        }
        capacetedima.setItemMeta(capacetedimaMeta);
        INVENTORY_LOJAARMAS.setItem(10, capacetedima);

        return INVENTORY_LOJAARMAS;
    }

    public static Inventory buildLojaDecorativos2(final String type) {
        final Inventory INVENTORY_LOJADECORATIVOS2 = Bukkit.createInventory(null, 4 * 9, "Decorativos: ");

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta voltarMeta = voltar.getItemMeta();
        voltarMeta.setDisplayName(ChatColor.GREEN + "Voltar página.");
        voltar.setItemMeta(voltarMeta);
        INVENTORY_LOJADECORATIVOS2.setItem(27, voltar);


        final ItemStack itemframe = new ItemStack(Material.ITEM_FRAME);
        final ItemMeta itemframeMeta = itemframe.getItemMeta();
        itemframeMeta.setDisplayName("§eMoldura");
        final ArrayList<String> lore10 = new ArrayList<String>();
        if (LojaAPI.valorVenda("moldura", type) != 0 && LojaAPI.valorCompra("moldura", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("moldura", type)));
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("moldura", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("moldura", type));
            lore10.add(" ");
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            itemframeMeta.setLore(lore10);
        } else if (LojaAPI.valorCompra("moldura", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("moldura", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("moldura", type));
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add(" ");
            itemframeMeta.setLore(lore10);
        } else if (LojaAPI.valorVenda("moldura", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("moldura", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("moldura", type));
            lore10.add(" ");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore10.add(" ");
            itemframeMeta.setLore(lore10);
        }
        itemframe.setItemMeta(itemframeMeta);
        INVENTORY_LOJADECORATIVOS2.setItem(24, itemframe);


        final ItemStack flowerpot = new ItemStack(Material.FLOWER_POT);
        final ItemMeta flowerpotMeta = flowerpot.getItemMeta();
        flowerpotMeta.setDisplayName("§eVaso de flor");
        final ArrayList<String> lore11 = new ArrayList<String>();
        if (LojaAPI.valorVenda("vaso_de_flor", type) != 0 && LojaAPI.valorCompra("vaso_de_flor", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vaso_de_flor", type)));
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vaso_de_flor", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("vaso_de_flor", type));
            lore11.add(" ");
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            flowerpotMeta.setLore(lore11);
        } else if (LojaAPI.valorCompra("vaso_de_flor", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vaso_de_flor", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("vaso_de_flor", type));
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add(" ");
            flowerpotMeta.setLore(lore11);
        } else if (LojaAPI.valorVenda("vaso_de_flor", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vaso_de_flor", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("vaso_de_flor", type));
            lore11.add(" ");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore11.add(" ");
            flowerpotMeta.setLore(lore11);
        }
        flowerpot.setItemMeta(flowerpotMeta);
        INVENTORY_LOJADECORATIVOS2.setItem(23, flowerpot);


        final ItemStack cauldron = new ItemStack(Material.CAULDRON);
        final ItemMeta cauldronMeta = cauldron.getItemMeta();
        cauldronMeta.setDisplayName("§eCaldeirão");
        final ArrayList<String> lore9 = new ArrayList<String>();
        if (LojaAPI.valorVenda("caldeirao", type) != 0 && LojaAPI.valorCompra("caldeirao", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("caldeirao", type)));
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("caldeirao", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("caldeirao", type));
            lore9.add(" ");
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            cauldronMeta.setLore(lore9);
        } else if (LojaAPI.valorCompra("caldeirao", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("caldeirao", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("caldeirao", type));
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add(" ");
            cauldronMeta.setLore(lore9);
        } else if (LojaAPI.valorVenda("caldeirao", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("caldeirao", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("caldeirao", type));
            lore9.add(" ");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore9.add(" ");
            cauldronMeta.setLore(lore9);
        }
        cauldron.setItemMeta(cauldronMeta);
        INVENTORY_LOJADECORATIVOS2.setItem(22, cauldron);


        final ItemStack jukebox = new ItemStack(Material.JUKEBOX);
        final ItemMeta jukeboxMeta = jukebox.getItemMeta();
        jukeboxMeta.setDisplayName("§eToca-discos");
        final ArrayList<String> lore8 = new ArrayList<String>();
        if (LojaAPI.valorVenda("toca_discos", type) != 0 && LojaAPI.valorCompra("toca_discos", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("toca_discos", type)));
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("toca_discos", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("toca_discos", type));
            lore8.add(" ");
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            jukeboxMeta.setLore(lore8);
        } else if (LojaAPI.valorCompra("toca_discos", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("toca_discos", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("toca_discos", type));
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add(" ");
            jukeboxMeta.setLore(lore8);
        } else if (LojaAPI.valorVenda("toca_discos", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("toca_discos", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("toca_discos", type));
            lore8.add(" ");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore8.add(" ");
            jukeboxMeta.setLore(lore8);
        }
        jukebox.setItemMeta(jukeboxMeta);
        INVENTORY_LOJADECORATIVOS2.setItem(21, jukebox);


        final ItemStack largefern = new ItemStack(Material.LARGE_FERN);
        final ItemMeta largefernMeta = largefern.getItemMeta();
        largefernMeta.setDisplayName("§eSamambaia grande");
        final ArrayList<String> lore7 = new ArrayList<String>();
        if (LojaAPI.valorVenda("samambaia_grande", type) != 0 && LojaAPI.valorCompra("samambaia_grande", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("samambaia_grande", type)));
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("samambaia_grande", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("samambaia_grande", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            largefernMeta.setLore(lore7);
        } else if (LojaAPI.valorCompra("samambaia_grande", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("samambaia_grande", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("samambaia_grande", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add(" ");
            largefernMeta.setLore(lore7);
        } else if (LojaAPI.valorVenda("samambaia_grande", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("samambaia_grande", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("samambaia_grande", type));
            lore7.add(" ");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore7.add(" ");
            largefernMeta.setLore(lore7);
        }
        largefern.setItemMeta(largefernMeta);
        INVENTORY_LOJADECORATIVOS2.setItem(20, largefern);


        final ItemStack gramalta = new ItemStack(Material.TALL_GRASS);
        final ItemMeta gramaltaMeta = gramalta.getItemMeta();
        gramaltaMeta.setDisplayName("§eGrama alta");
        final ArrayList<String> lore6 = new ArrayList<String>();
        if (LojaAPI.valorVenda("grama_alta", type) != 0 && LojaAPI.valorCompra("grama_alta", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("grama_alta", type)));
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("grama_alta", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("grama_alta", type));
            lore6.add(" ");
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            gramaltaMeta.setLore(lore6);
        } else if (LojaAPI.valorCompra("grama_alta", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("grama_alta", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("grama_alta", type));
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add(" ");
            gramaltaMeta.setLore(lore6);
        } else if (LojaAPI.valorVenda("grama_alta", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("grama_alta", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("grama_alta", type));
            lore6.add(" ");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore6.add(" ");
            gramaltaMeta.setLore(lore6);
        }
        gramalta.setItemMeta(gramaltaMeta);
        INVENTORY_LOJADECORATIVOS2.setItem(16, gramalta);


        final ItemStack peonia = new ItemStack(Material.PEONY);
        final ItemMeta peoniaMeta = peonia.getItemMeta();
        peoniaMeta.setDisplayName("§ePeônia");
        final ArrayList<String> lore5 = new ArrayList<String>();
        if (LojaAPI.valorVenda("peonia", type) != 0 && LojaAPI.valorCompra("peonia", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("peonia", type)));
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("peonia", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("peonia", type));
            lore5.add(" ");
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            peoniaMeta.setLore(lore5);
        } else if (LojaAPI.valorCompra("peonia", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("peonia", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("peonia", type));
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add(" ");
            peoniaMeta.setLore(lore5);
        } else if (LojaAPI.valorVenda("peonia", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("peonia", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("peonia", type));
            lore5.add(" ");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore5.add(" ");
            peoniaMeta.setLore(lore5);
        }
        peonia.setItemMeta(peoniaMeta);
        INVENTORY_LOJADECORATIVOS2.setItem(15, peonia);


        final ItemStack roseira = new ItemStack(Material.ROSE_BUSH);
        final ItemMeta roseiraMeta = roseira.getItemMeta();
        roseiraMeta.setDisplayName("§eRoseira");
        final ArrayList<String> lore4 = new ArrayList<String>();
        if (LojaAPI.valorVenda("roseira", type) != 0 && LojaAPI.valorCompra("roseira", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("roseira", type)));
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("roseira", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("roseira", type));
            lore4.add(" ");
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            roseiraMeta.setLore(lore4);
        } else if (LojaAPI.valorCompra("roseira", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("roseira", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("roseira", type));
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add(" ");
            roseiraMeta.setLore(lore4);
        } else if (LojaAPI.valorVenda("roseira", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("roseira", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("roseira", type));
            lore4.add(" ");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore4.add(" ");
            roseiraMeta.setLore(lore4);
        }
        roseira.setItemMeta(roseiraMeta);
        INVENTORY_LOJADECORATIVOS2.setItem(14, roseira);


        final ItemStack violeta = new ItemStack(Material.LILAC);
        final ItemMeta violetaMeta = violeta.getItemMeta();
        violetaMeta.setDisplayName("§eVioleta");
        final ArrayList<String> lore3 = new ArrayList<String>();
        if (LojaAPI.valorVenda("violeta", type) != 0 && LojaAPI.valorCompra("violeta", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("violeta", type)));
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("violeta", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("violeta", type));
            lore3.add(" ");
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            violetaMeta.setLore(lore3);
        } else if (LojaAPI.valorCompra("violeta", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("violeta", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("violeta", type));
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add(" ");
            violetaMeta.setLore(lore3);
        } else if (LojaAPI.valorVenda("violeta", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("violeta", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("violeta", type));
            lore3.add(" ");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore3.add(" ");
            violetaMeta.setLore(lore3);
        }
        violeta.setItemMeta(violetaMeta);
        INVENTORY_LOJADECORATIVOS2.setItem(13, violeta);


        final ItemStack girassol = new ItemStack(Material.SUNFLOWER);
        final ItemMeta girassolMeta = girassol.getItemMeta();
        girassolMeta.setDisplayName("§eGirassol");
        final ArrayList<String> lore2 = new ArrayList<String>();
        if (LojaAPI.valorVenda("girassol", type) != 0 && LojaAPI.valorCompra("girassol", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("girassol", type)));
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("girassol", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("girassol", type));
            lore2.add(" ");
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            girassolMeta.setLore(lore2);
        } else if (LojaAPI.valorCompra("girassol", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("girassol", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("girassol", type));
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add(" ");
            girassolMeta.setLore(lore2);
        } else if (LojaAPI.valorVenda("girassol", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("girassol", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("girassol", type));
            lore2.add(" ");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore2.add(" ");
            girassolMeta.setLore(lore2);
        }
        girassol.setItemMeta(girassolMeta);
        INVENTORY_LOJADECORATIVOS2.setItem(12, girassol);


        final ItemStack cobblewall2 = new ItemStack(Material.MOSSY_COBBLESTONE_WALL);
        final ItemMeta cobblewall2Meta = cobblewall2.getItemMeta();
        cobblewall2Meta.setDisplayName("§eParede de pedregulho musgoso");
        final ArrayList<String> lore1 = new ArrayList<String>();
        if (LojaAPI.valorVenda("parede_de_pedregulho_musgoso", type) != 0 && LojaAPI.valorCompra("parede_de_pedregulho_musgoso", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("parede_de_pedregulho_musgoso", type)));
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("parede_de_pedregulho_musgoso", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("parede_de_pedregulho_musgoso", type));
            lore1.add(" ");
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            cobblewall2Meta.setLore(lore1);
        } else if (LojaAPI.valorCompra("parede_de_pedregulho_musgoso", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("parede_de_pedregulho_musgoso", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("parede_de_pedregulho_musgoso", type));
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add(" ");
            cobblewall2Meta.setLore(lore1);
        } else if (LojaAPI.valorVenda("parede_de_pedregulho_musgoso", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("parede_de_pedregulho_musgoso", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("parede_de_pedregulho_musgoso", type));
            lore1.add(" ");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore1.add(" ");
            cobblewall2Meta.setLore(lore1);
        }
        cobblewall2.setItemMeta(cobblewall2Meta);
        INVENTORY_LOJADECORATIVOS2.setItem(11, cobblewall2);


        final ItemStack cobblewall = new ItemStack(Material.COBBLESTONE_WALL);
        final ItemMeta cobblewallMeta = cobblewall.getItemMeta();
        cobblewallMeta.setDisplayName("§eParede de pedregulho");
        final ArrayList<String> lore = new ArrayList<String>();
        if (LojaAPI.valorVenda("parede_de_pedregulho", type) != 0 && LojaAPI.valorCompra("parede_de_pedregulho", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("parede_de_pedregulho", type)));
            lore.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("parede_de_pedregulho", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("parede_de_pedregulho", type));
            lore.add(" ");
            lore.add(" ");
            lore.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            cobblewallMeta.setLore(lore);
        } else if (LojaAPI.valorCompra("parede_de_pedregulho", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("parede_de_pedregulho", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("parede_de_pedregulho", type));
            lore.add(" ");
            lore.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore.add(" ");
            cobblewallMeta.setLore(lore);
        } else if (LojaAPI.valorVenda("parede_de_pedregulho", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("parede_de_pedregulho", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("parede_de_pedregulho", type));
            lore.add(" ");
            lore.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore.add(" ");
            cobblewallMeta.setLore(lore);
        }
        cobblewall.setItemMeta(cobblewallMeta);
        INVENTORY_LOJADECORATIVOS2.setItem(10, cobblewall);

        return INVENTORY_LOJADECORATIVOS2;
    }

    public static Inventory buildLojaDecorativos1(final String type) {
        final Inventory INVENTORY_LOJADECORATIVOS1 = Bukkit.createInventory(null, 6 * 9, "Decorativos: ");

        final ItemStack proxima = new ItemStack(Material.ARROW);
        final ItemMeta proximaMeta = proxima.getItemMeta();
        proximaMeta.setDisplayName(ChatColor.GREEN + "Próxima página.");
        proxima.setItemMeta(proximaMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(53, proxima);


        final ItemStack lilypad = new ItemStack(Material.LILY_PAD);
        final ItemMeta lilypadMeta = lilypad.getItemMeta();
        lilypadMeta.setDisplayName("§eVitória-régia");
        final ArrayList<String> lore27 = new ArrayList<String>();
        if (LojaAPI.valorVenda("vitoria_regia", type) != 0 && LojaAPI.valorCompra("vitoria_regia", type) != 0) {
            lore27.add(" ");
            lore27.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vitoria_regia", type)));
            lore27.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vitoria_regia", type)));
            lore27.add("§7Quantia: §a" + LojaAPI.itemQuantia("vitoria_regia", type));
            lore27.add(" ");
            lore27.add(" ");
            lore27.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore27.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lilypadMeta.setLore(lore27);
        } else if (LojaAPI.valorCompra("vitoria_regia", type) != 0) {
            lore27.add(" ");
            lore27.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vitoria_regia", type)));
            lore27.add("§7Quantia: §a" + LojaAPI.itemQuantia("vitoria_regia", type));
            lore27.add(" ");
            lore27.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore27.add(" ");
            lilypadMeta.setLore(lore27);
        } else if (LojaAPI.valorVenda("vitoria_regia", type) != 0) {
            lore27.add(" ");
            lore27.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vitoria_regia", type)));
            lore27.add("§7Quantia: §a" + LojaAPI.itemQuantia("vitoria_regia", type));
            lore27.add(" ");
            lore27.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore27.add(" ");
            lilypadMeta.setLore(lore27);
        }
        lilypad.setItemMeta(lilypadMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(43, lilypad);


        final ItemStack vine = new ItemStack(Material.VINE);
        final ItemMeta vineMeta = vine.getItemMeta();
        vineMeta.setDisplayName("§eVinhas");
        final ArrayList<String> lore26 = new ArrayList<String>();
        if (LojaAPI.valorVenda("vinhas", type) != 0 && LojaAPI.valorCompra("vinhas", type) != 0) {
            lore26.add(" ");
            lore26.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vinhas", type)));
            lore26.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vinhas", type)));
            lore26.add("§7Quantia: §a" + LojaAPI.itemQuantia("vinhas", type));
            lore26.add(" ");
            lore26.add(" ");
            lore26.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore26.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            vineMeta.setLore(lore26);
        } else if (LojaAPI.valorCompra("vinhas", type) != 0) {
            lore26.add(" ");
            lore26.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vinhas", type)));
            lore26.add("§7Quantia: §a" + LojaAPI.itemQuantia("vinhas", type));
            lore26.add(" ");
            lore26.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore26.add(" ");
            vineMeta.setLore(lore26);
        } else if (LojaAPI.valorVenda("vinhas", type) != 0) {
            lore26.add(" ");
            lore26.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vinhas", type)));
            lore26.add("§7Quantia: §a" + LojaAPI.itemQuantia("vinhas", type));
            lore26.add(" ");
            lore26.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore26.add(" ");
            vineMeta.setLore(lore26);
        }
        vine.setItemMeta(vineMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(42, vine);


        final ItemStack ironbar = new ItemStack(Material.IRON_BARS);
        final ItemMeta ironbarMeta = ironbar.getItemMeta();
        ironbarMeta.setDisplayName("§eGrades de ferro");
        final ArrayList<String> lore25 = new ArrayList<String>();
        if (LojaAPI.valorVenda("grades_de_ferro", type) != 0 && LojaAPI.valorCompra("grades_de_ferro", type) != 0) {
            lore25.add(" ");
            lore25.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("grades_de_ferro", type)));
            lore25.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("grades_de_ferro", type)));
            lore25.add("§7Quantia: §a" + LojaAPI.itemQuantia("grades_de_ferro", type));
            lore25.add(" ");
            lore25.add(" ");
            lore25.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore25.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            ironbarMeta.setLore(lore25);
        } else if (LojaAPI.valorCompra("grades_de_ferro", type) != 0) {
            lore25.add(" ");
            lore25.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("grades_de_ferro", type)));
            lore25.add("§7Quantia: §a" + LojaAPI.itemQuantia("grades_de_ferro", type));
            lore25.add(" ");
            lore25.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore25.add(" ");
            ironbarMeta.setLore(lore25);
        } else if (LojaAPI.valorVenda("grades_de_ferro", type) != 0) {
            lore25.add(" ");
            lore25.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("grades_de_ferro", type)));
            lore25.add("§7Quantia: §a" + LojaAPI.itemQuantia("grades_de_ferro", type));
            lore25.add(" ");
            lore25.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore25.add(" ");
            ironbarMeta.setLore(lore25);
        }
        ironbar.setItemMeta(ironbarMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(41, ironbar);


        final ItemStack caulecogu = new ItemStack(Material.MUSHROOM_STEM);
        final ItemMeta caulecoguMeta = caulecogu.getItemMeta();
        caulecoguMeta.setDisplayName("§eCaule de cogumelo");
        final ArrayList<String> lore24 = new ArrayList<String>();
        if (LojaAPI.valorVenda("caule_de_cogumelo", type) != 0 && LojaAPI.valorCompra("caule_de_cogumelo", type) != 0) {
            lore24.add(" ");
            lore24.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("caule_de_cogumelo", type)));
            lore24.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("caule_de_cogumelo", type)));
            lore24.add("§7Quantia: §a" + LojaAPI.itemQuantia("caule_de_cogumelo", type));
            lore24.add(" ");
            lore24.add(" ");
            lore24.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore24.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            caulecoguMeta.setLore(lore24);
        } else if (LojaAPI.valorCompra("caule_de_cogumelo", type) != 0) {
            lore24.add(" ");
            lore24.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("caule_de_cogumelo", type)));
            lore24.add("§7Quantia: §a" + LojaAPI.itemQuantia("caule_de_cogumelo", type));
            lore24.add(" ");
            lore24.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore24.add(" ");
            caulecoguMeta.setLore(lore24);
        } else if (LojaAPI.valorVenda("caule_de_cogumelo", type) != 0) {
            lore24.add(" ");
            lore24.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("caule_de_cogumelo", type)));
            lore24.add("§7Quantia: §a" + LojaAPI.itemQuantia("caule_de_cogumelo", type));
            lore24.add(" ");
            lore24.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore24.add(" ");
            caulecoguMeta.setLore(lore24);
        }
        caulecogu.setItemMeta(caulecoguMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(40, caulecogu);


        final ItemStack blococogumelov = new ItemStack(Material.RED_MUSHROOM_BLOCK);
        final ItemMeta blococogumelovMeta = blococogumelov.getItemMeta();
        blococogumelovMeta.setDisplayName("§eBloco de cogumelo vermelho");
        final ArrayList<String> lore23 = new ArrayList<String>();
        if (LojaAPI.valorVenda("bloco_de_cogumelo_marrom", type) != 0 && LojaAPI.valorCompra("bloco_de_cogumelo_marrom", type) != 0) {
            lore23.add(" ");
            lore23.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bloco_de_cogumelo_marrom", type)));
            lore23.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bloco_de_cogumelo_marrom", type)));
            lore23.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_cogumelo_marrom", type));
            lore23.add(" ");
            lore23.add(" ");
            lore23.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore23.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            blococogumelovMeta.setLore(lore23);
        } else if (LojaAPI.valorCompra("bloco_de_cogumelo_marrom", type) != 0) {
            lore23.add(" ");
            lore23.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bloco_de_cogumelo_marrom", type)));
            lore23.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_cogumelo_marrom", type));
            lore23.add(" ");
            lore23.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore23.add(" ");
            blococogumelovMeta.setLore(lore23);
        } else if (LojaAPI.valorVenda("bloco_de_cogumelo_marrom", type) != 0) {
            lore23.add(" ");
            lore23.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bloco_de_cogumelo_marrom", type)));
            lore23.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_cogumelo_marrom", type));
            lore23.add(" ");
            lore23.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore23.add(" ");
            blococogumelovMeta.setLore(lore23);
        }
        blococogumelov.setItemMeta(blococogumelovMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(39, blococogumelov);


        final ItemStack blococogumelom = new ItemStack(Material.BROWN_MUSHROOM_BLOCK);
        final ItemMeta blococogumelomMeta = blococogumelom.getItemMeta();
        blococogumelomMeta.setDisplayName("§eBloco de cogumelo marrom");
        final ArrayList<String> lore22 = new ArrayList<String>();
        if (LojaAPI.valorVenda("bloco_de_cogumelo_marrom", type) != 0 && LojaAPI.valorCompra("bloco_de_cogumelo_marrom", type) != 0) {
            lore22.add(" ");
            lore22.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bloco_de_cogumelo_marrom", type)));
            lore22.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bloco_de_cogumelo_marrom", type)));
            lore22.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_cogumelo_marrom", type));
            lore22.add(" ");
            lore22.add(" ");
            lore22.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore22.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            blococogumelomMeta.setLore(lore22);
        } else if (LojaAPI.valorCompra("bloco_de_cogumelo_marrom", type) != 0) {
            lore22.add(" ");
            lore22.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bloco_de_cogumelo_marrom", type)));
            lore22.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_cogumelo_marrom", type));
            lore22.add(" ");
            lore22.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore22.add(" ");
            blococogumelomMeta.setLore(lore22);
        } else if (LojaAPI.valorVenda("bloco_de_cogumelo_marrom", type) != 0) {
            lore22.add(" ");
            lore22.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bloco_de_cogumelo_marrom", type)));
            lore22.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_cogumelo_marrom", type));
            lore22.add(" ");
            lore22.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore22.add(" ");
            blococogumelomMeta.setLore(lore22);
        }
        blococogumelom.setItemMeta(blococogumelomMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(38, blococogumelom);


        final ItemStack pumpkini = new ItemStack(Material.JACK_O_LANTERN);
        final ItemMeta pumpkiniMeta = pumpkini.getItemMeta();
        pumpkiniMeta.setDisplayName("§eAbóbora de Halloween");
        final ArrayList<String> lore21 = new ArrayList<String>();
        if (LojaAPI.valorVenda("abobora_de_halloween", type) != 0 && LojaAPI.valorCompra("abobora_de_halloween", type) != 0) {
            lore21.add(" ");
            lore21.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("abobora_de_halloween", type)));
            lore21.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("abobora_de_halloween", type)));
            lore21.add("§7Quantia: §a" + LojaAPI.itemQuantia("abobora_de_halloween", type));
            lore21.add(" ");
            lore21.add(" ");
            lore21.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore21.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            pumpkiniMeta.setLore(lore21);
        } else if (LojaAPI.valorCompra("abobora_de_halloween", type) != 0) {
            lore21.add(" ");
            lore21.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("abobora_de_halloween", type)));
            lore21.add("§7Quantia: §a" + LojaAPI.itemQuantia("abobora_de_halloween", type));
            lore21.add(" ");
            lore21.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore21.add(" ");
            pumpkiniMeta.setLore(lore21);
        } else if (LojaAPI.valorVenda("abobora_de_halloween", type) != 0) {
            lore21.add(" ");
            lore21.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("abobora_de_halloween", type)));
            lore21.add("§7Quantia: §a" + LojaAPI.itemQuantia("abobora_de_halloween", type));
            lore21.add(" ");
            lore21.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore21.add(" ");
            pumpkiniMeta.setLore(lore21);
        }
        pumpkini.setItemMeta(pumpkiniMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(37, pumpkini);


        final ItemStack pumpkin = new ItemStack(Material.PUMPKIN);
        final ItemMeta pumpkinMeta = pumpkin.getItemMeta();
        pumpkinMeta.setDisplayName("§eAbóbora esculpida");
        final ArrayList<String> lore20 = new ArrayList<String>();
        if (LojaAPI.valorVenda("abobora_esculpida", type) != 0 && LojaAPI.valorCompra("abobora_esculpida", type) != 0) {
            lore20.add(" ");
            lore20.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("abobora_esculpida", type)));
            lore20.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("abobora_esculpida", type)));
            lore20.add("§7Quantia: §a" + LojaAPI.itemQuantia("abobora_esculpida", type));
            lore20.add(" ");
            lore20.add(" ");
            lore20.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore20.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            pumpkinMeta.setLore(lore20);
        } else if (LojaAPI.valorCompra("abobora_esculpida", type) != 0) {
            lore20.add(" ");
            lore20.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("abobora_esculpida", type)));
            lore20.add("§7Quantia: §a" + LojaAPI.itemQuantia("abobora_esculpida", type));
            lore20.add(" ");
            lore20.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore20.add(" ");
            pumpkinMeta.setLore(lore20);
        } else if (LojaAPI.valorVenda("abobora_esculpida", type) != 0) {
            lore20.add(" ");
            lore20.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("abobora_esculpida", type)));
            lore20.add("§7Quantia: §a" + LojaAPI.itemQuantia("abobora_esculpida", type));
            lore20.add(" ");
            lore20.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore20.add(" ");
            pumpkinMeta.setLore(lore20);
        }
        pumpkin.setItemMeta(pumpkinMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(34, pumpkin);


        final ItemStack chorusf = new ItemStack(Material.CHORUS_FLOWER);
        final ItemMeta chorusfMeta = chorusf.getItemMeta();
        chorusfMeta.setDisplayName("§eFlor do coro");
        final ArrayList<String> lore19 = new ArrayList<String>();
        if (LojaAPI.valorVenda("flor_do_coro", type) != 0 && LojaAPI.valorCompra("flor_do_coro", type) != 0) {
            lore19.add(" ");
            lore19.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("flor_do_coro", type)));
            lore19.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("flor_do_coro", type)));
            lore19.add("§7Quantia: §a" + LojaAPI.itemQuantia("flor_do_coro", type));
            lore19.add(" ");
            lore19.add(" ");
            lore19.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore19.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            chorusfMeta.setLore(lore19);
        } else if (LojaAPI.valorCompra("flor_do_coro", type) != 0) {
            lore19.add(" ");
            lore19.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("flor_do_coro", type)));
            lore19.add("§7Quantia: §a" + LojaAPI.itemQuantia("flor_do_coro", type));
            lore19.add(" ");
            lore19.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore19.add(" ");
            chorusfMeta.setLore(lore19);
        } else if (LojaAPI.valorVenda("flor_do_coro", type) != 0) {
            lore19.add(" ");
            lore19.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("flor_do_coro", type)));
            lore19.add("§7Quantia: §a" + LojaAPI.itemQuantia("flor_do_coro", type));
            lore19.add(" ");
            lore19.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore19.add(" ");
            chorusfMeta.setLore(lore19);
        }
        chorusf.setItemMeta(chorusfMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(33, chorusf);


        final ItemStack chorusp = new ItemStack(Material.CHORUS_PLANT);
        final ItemMeta choruspMeta = chorusp.getItemMeta();
        choruspMeta.setDisplayName("§ePlanta do coro");
        final ArrayList<String> lore18 = new ArrayList<String>();
        if (LojaAPI.valorVenda("planta_do_coro", type) != 0 && LojaAPI.valorCompra("planta_do_coro", type) != 0) {
            lore18.add(" ");
            lore18.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("planta_do_coro", type)));
            lore18.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("planta_do_coro", type)));
            lore18.add("§7Quantia: §a" + LojaAPI.itemQuantia("planta_do_coro", type));
            lore18.add(" ");
            lore18.add(" ");
            lore18.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore18.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            choruspMeta.setLore(lore18);
        } else if (LojaAPI.valorCompra("planta_do_coro", type) != 0) {
            lore18.add(" ");
            lore18.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("planta_do_coro", type)));
            lore18.add("§7Quantia: §a" + LojaAPI.itemQuantia("planta_do_coro", type));
            lore18.add(" ");
            lore18.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore18.add(" ");
            choruspMeta.setLore(lore18);
        } else if (LojaAPI.valorVenda("planta_do_coro", type) != 0) {
            lore18.add(" ");
            lore18.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("planta_do_coro", type)));
            lore18.add("§7Quantia: §a" + LojaAPI.itemQuantia("planta_do_coro", type));
            lore18.add(" ");
            lore18.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore18.add(" ");
            choruspMeta.setLore(lore18);
        }
        chorusp.setItemMeta(choruspMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(32, chorusp);


        final ItemStack endrod = new ItemStack(Material.END_ROD);
        final ItemMeta endrodMeta = endrod.getItemMeta();
        endrodMeta.setDisplayName("§eVara do end");
        final ArrayList<String> lore17 = new ArrayList<String>();
        if (LojaAPI.valorVenda("vara_do_end", type) != 0 && LojaAPI.valorCompra("vara_do_end", type) != 0) {
            lore17.add(" ");
            lore17.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vara_do_end", type)));
            lore17.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vara_do_end", type)));
            lore17.add("§7Quantia: §a" + LojaAPI.itemQuantia("vara_do_end", type));
            lore17.add(" ");
            lore17.add(" ");
            lore17.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore17.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            endrodMeta.setLore(lore17);
        } else if (LojaAPI.valorCompra("vara_do_end", type) != 0) {
            lore17.add(" ");
            lore17.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("vara_do_end", type)));
            lore17.add("§7Quantia: §a" + LojaAPI.itemQuantia("vara_do_end", type));
            lore17.add(" ");
            lore17.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore17.add(" ");
            endrodMeta.setLore(lore17);
        } else if (LojaAPI.valorVenda("vara_do_end", type) != 0) {
            lore17.add(" ");
            lore17.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("vara_do_end", type)));
            lore17.add("§7Quantia: §a" + LojaAPI.itemQuantia("vara_do_end", type));
            lore17.add(" ");
            lore17.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore17.add(" ");
            endrodMeta.setLore(lore17);
        }
        endrod.setItemMeta(endrodMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(31, endrod);


        final ItemStack coguv = new ItemStack(Material.RED_MUSHROOM);
        final ItemMeta coguvMeta = coguv.getItemMeta();
        coguvMeta.setDisplayName("§eCogumelo vermelho");
        final ArrayList<String> lore16 = new ArrayList<String>();
        if (LojaAPI.valorVenda("cogumelo_vermelho", type) != 0 && LojaAPI.valorCompra("cogumelo_vermelho", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("cogumelo_vermelho", type)));
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("cogumelo_vermelho", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("cogumelo_vermelho", type));
            lore16.add(" ");
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            coguvMeta.setLore(lore16);
        } else if (LojaAPI.valorCompra("cogumelo_vermelho", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("cogumelo_vermelho", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("cogumelo_vermelho", type));
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add(" ");
            coguvMeta.setLore(lore16);
        } else if (LojaAPI.valorVenda("cogumelo_vermelho", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("cogumelo_vermelho", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("cogumelo_vermelho", type));
            lore16.add(" ");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore16.add(" ");
            coguvMeta.setLore(lore16);
        }
        coguv.setItemMeta(coguvMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(30, coguv);


        final ItemStack cogum = new ItemStack(Material.BROWN_MUSHROOM);
        final ItemMeta cogumMeta = cogum.getItemMeta();
        cogumMeta.setDisplayName("§eCogumelo marrom");
        final ArrayList<String> lore15 = new ArrayList<String>();
        if (LojaAPI.valorVenda("cogumelo_marrom", type) != 0 && LojaAPI.valorCompra("cogumelo_marrom", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("cogumelo_marrom", type)));
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("cogumelo_marrom", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("cogumelo_marrom", type));
            lore15.add(" ");
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            cogumMeta.setLore(lore15);
        } else if (LojaAPI.valorCompra("cogumelo_marrom", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("cogumelo_marrom", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("cogumelo_marrom", type));
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add(" ");
            cogumMeta.setLore(lore15);
        } else if (LojaAPI.valorVenda("cogumelo_marrom", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("cogumelo_marrom", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("cogumelo_marrom", type));
            lore15.add(" ");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore15.add(" ");
            cogumMeta.setLore(lore15);
        }
        cogum.setItemMeta(cogumMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(29, cogum);


        final ItemStack margarida = new ItemStack(Material.PINK_TULIP);
        final ItemMeta margaridaMeta = margarida.getItemMeta();
        margaridaMeta.setDisplayName("§eTulipa rosa");
        final ArrayList<String> lore14 = new ArrayList<String>();
        if (LojaAPI.valorVenda("tulipa_rosa", type) != 0 && LojaAPI.valorCompra("tulipa_rosa", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tulipa_rosa", type)));
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tulipa_rosa", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("tulipa_rosa", type));
            lore14.add(" ");
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            margaridaMeta.setLore(lore14);
        } else if (LojaAPI.valorCompra("tulipa_rosa", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tulipa_rosa", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("tulipa_rosa", type));
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add(" ");
            margaridaMeta.setLore(lore14);
        } else if (LojaAPI.valorVenda("tulipa_rosa", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tulipa_rosa", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("tulipa_rosa", type));
            lore14.add(" ");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore14.add(" ");
            margaridaMeta.setLore(lore14);
        }
        margarida.setItemMeta(margaridaMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(28, margarida);


        final ItemStack tulipar = new ItemStack(Material.PINK_TULIP);
        final ItemMeta tuliparMeta = tulipar.getItemMeta();
        tuliparMeta.setDisplayName("§eTulipa rosa");
        final ArrayList<String> lore13 = new ArrayList<String>();
        if (LojaAPI.valorVenda("tulipa_rosa", type) != 0 && LojaAPI.valorCompra("tulipa_rosa", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tulipa_rosa", type)));
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tulipa_rosa", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("tulipa_rosa", type));
            lore13.add(" ");
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            tuliparMeta.setLore(lore13);
        } else if (LojaAPI.valorCompra("tulipa_rosa", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tulipa_rosa", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("tulipa_rosa", type));
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add(" ");
            tuliparMeta.setLore(lore13);
        } else if (LojaAPI.valorVenda("tulipa_rosa", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tulipa_rosa", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("tulipa_rosa", type));
            lore13.add(" ");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore13.add(" ");
            tuliparMeta.setLore(lore13);
        }
        tulipar.setItemMeta(tuliparMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(25, tulipar);


        final ItemStack tulipab = new ItemStack(Material.WHITE_TULIP);
        final ItemMeta tulipabMeta = tulipab.getItemMeta();
        tulipabMeta.setDisplayName("§eTulipa branca");
        final ArrayList<String> lore12 = new ArrayList<String>();
        if (LojaAPI.valorVenda("tulipa_branca", type) != 0 && LojaAPI.valorCompra("tulipa_branca", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tulipa_branca", type)));
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tulipa_branca", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("tulipa_branca", type));
            lore12.add(" ");
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            tulipabMeta.setLore(lore12);
        } else if (LojaAPI.valorCompra("tulipa_branca", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tulipa_branca", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("tulipa_branca", type));
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add(" ");
            tulipabMeta.setLore(lore12);
        } else if (LojaAPI.valorVenda("tulipa_branca", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tulipa_branca", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("tulipa_branca", type));
            lore12.add(" ");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore12.add(" ");
            tulipabMeta.setLore(lore12);
        }
        tulipab.setItemMeta(tulipabMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(24, tulipab);


        final ItemStack tulipal = new ItemStack(Material.ORANGE_TULIP);
        final ItemMeta tulipalMeta = tulipal.getItemMeta();
        tulipalMeta.setDisplayName("§eTulipa laranja");
        final ArrayList<String> lore11 = new ArrayList<String>();
        if (LojaAPI.valorVenda("tulipa_laranja", type) != 0 && LojaAPI.valorCompra("tulipa_laranja", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tulipa_laranja", type)));
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tulipa_laranja", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("tulipa_laranja", type));
            lore11.add(" ");
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            tulipalMeta.setLore(lore11);
        } else if (LojaAPI.valorCompra("tulipa_laranja", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tulipa_laranja", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("tulipa_laranja", type));
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add(" ");
            tulipalMeta.setLore(lore11);
        } else if (LojaAPI.valorVenda("tulipa_laranja", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tulipa_laranja", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("tulipa_laranja", type));
            lore11.add(" ");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore11.add(" ");
            tulipalMeta.setLore(lore11);
        }
        tulipal.setItemMeta(tulipalMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(23, tulipal);


        final ItemStack tulipav = new ItemStack(Material.RED_TULIP);
        final ItemMeta tulipavMeta = tulipav.getItemMeta();
        tulipavMeta.setDisplayName("§eTulipa vermelha");
        final ArrayList<String> lore10 = new ArrayList<String>();
        if (LojaAPI.valorVenda("tulipa_vermelha", type) != 0 && LojaAPI.valorCompra("tulipa_vermelha", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tulipa_vermelha", type)));
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tulipa_vermelha", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("tulipa_vermelha", type));
            lore10.add(" ");
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            tulipavMeta.setLore(lore10);
        } else if (LojaAPI.valorCompra("tulipa_vermelha", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tulipa_vermelha", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("tulipa_vermelha", type));
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add(" ");
            tulipavMeta.setLore(lore10);
        } else if (LojaAPI.valorVenda("tulipa_vermelha", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tulipa_vermelha", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("tulipa_vermelha", type));
            lore10.add(" ");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore10.add(" ");
            tulipavMeta.setLore(lore10);
        }
        tulipav.setItemMeta(tulipavMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(22, tulipav);


        final ItemStack silvazul = new ItemStack(Material.AZURE_BLUET);
        final ItemMeta silvazulMeta = silvazul.getItemMeta();
        silvazulMeta.setDisplayName("§eFlor silvestre azul");
        final ArrayList<String> lore9 = new ArrayList<String>();
        if (LojaAPI.valorVenda("flor_silvestre_azul", type) != 0 && LojaAPI.valorCompra("flor_silvestre_azul", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("flor_silvestre_azul", type)));
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("flor_silvestre_azul", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("flor_silvestre_azul", type));
            lore9.add(" ");
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            silvazulMeta.setLore(lore9);
        } else if (LojaAPI.valorCompra("flor_silvestre_azul", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("flor_silvestre_azul", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("flor_silvestre_azul", type));
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add(" ");
            silvazulMeta.setLore(lore9);
        } else if (LojaAPI.valorVenda("flor_silvestre_azul", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("flor_silvestre_azul", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("flor_silvestre_azul", type));
            lore9.add(" ");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore9.add(" ");
            silvazulMeta.setLore(lore9);
        }
        silvazul.setItemMeta(silvazulMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(21, silvazul);


        final ItemStack hortensi = new ItemStack(Material.ALLIUM);
        final ItemMeta hortensiMeta = hortensi.getItemMeta();
        hortensiMeta.setDisplayName("§eHortência");
        final ArrayList<String> lore8 = new ArrayList<String>();
        if (LojaAPI.valorVenda("hortencia", type) != 0 && LojaAPI.valorCompra("hortencia", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("hortencia", type)));
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("hortencia", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("hortencia", type));
            lore8.add(" ");
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            hortensiMeta.setLore(lore8);
        } else if (LojaAPI.valorCompra("hortencia", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("hortencia", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("hortencia", type));
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add(" ");
            hortensiMeta.setLore(lore8);
        } else if (LojaAPI.valorVenda("hortencia", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("hortencia", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("hortencia", type));
            lore8.add(" ");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore8.add(" ");
            hortensiMeta.setLore(lore8);
        }
        hortensi.setItemMeta(hortensiMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(20, hortensi);


        final ItemStack blue = new ItemStack(Material.BLUE_ORCHID);
        final ItemMeta blueMeta = blue.getItemMeta();
        blueMeta.setDisplayName("§eOrquídea azul");
        final ArrayList<String> lore7 = new ArrayList<String>();
        if (LojaAPI.valorVenda("orquidea_azul", type) != 0 && LojaAPI.valorCompra("orquidea_azul", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("orquidea_azul", type)));
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("orquidea_azul", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("orquidea_azul", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            blueMeta.setLore(lore7);
        } else if (LojaAPI.valorCompra("orquidea_azul", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("orquidea_azul", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("orquidea_azul", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add(" ");
            blueMeta.setLore(lore7);
        } else if (LojaAPI.valorVenda("orquidea_azul", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("orquidea_azul", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("orquidea_azul", type));
            lore7.add(" ");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore7.add(" ");
            blueMeta.setLore(lore7);
        }
        blue.setItemMeta(blueMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(19, blue);


        final ItemStack seapickle = new ItemStack(Material.SEA_PICKLE);
        final ItemMeta seapickleMeta = seapickle.getItemMeta();
        seapickleMeta.setDisplayName("§ePirossomo");
        final ArrayList<String> lore6 = new ArrayList<String>();
        if (LojaAPI.valorVenda("pirossomo", type) != 0 && LojaAPI.valorCompra("pirossomo", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pirossomo", type)));
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pirossomo", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("pirossomo", type));
            lore6.add(" ");
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            seapickleMeta.setLore(lore6);
        } else if (LojaAPI.valorCompra("pirossomo", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pirossomo", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("pirossomo", type));
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add(" ");
            seapickleMeta.setLore(lore6);
        } else if (LojaAPI.valorVenda("pirossomo", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pirossomo", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("pirossomo", type));
            lore6.add(" ");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore6.add(" ");
            seapickleMeta.setLore(lore6);
        }
        seapickle.setItemMeta(seapickleMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(16, seapickle);


        final ItemStack seagrass = new ItemStack(Material.SEAGRASS);
        final ItemMeta seagrassMeta = seagrass.getItemMeta();
        seagrassMeta.setDisplayName("§eErva marinha");
        final ArrayList<String> lore5 = new ArrayList<String>();
        if (LojaAPI.valorVenda("erva_marinha", type) != 0 && LojaAPI.valorCompra("erva_marinha", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("erva_marinha", type)));
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("erva_marinha", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("erva_marinha", type));
            lore5.add(" ");
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            seagrassMeta.setLore(lore5);
        } else if (LojaAPI.valorCompra("erva_marinha", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("erva_marinha", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("erva_marinha", type));
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add(" ");
            seagrassMeta.setLore(lore5);
        } else if (LojaAPI.valorVenda("erva_marinha", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("erva_marinha", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("erva_marinha", type));
            lore5.add(" ");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore5.add(" ");
            seagrassMeta.setLore(lore5);
        }
        seagrass.setItemMeta(seagrassMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(15, seagrass);


        final ItemStack fern = new ItemStack(Material.FERN);
        final ItemMeta fernMeta = fern.getItemMeta();
        fernMeta.setDisplayName("§eSamambaia");
        final ArrayList<String> lore4 = new ArrayList<String>();
        if (LojaAPI.valorVenda("samambaia", type) != 0 && LojaAPI.valorCompra("samambaia", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("samambaia", type)));
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("samambaia", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("samambaia", type));
            lore4.add(" ");
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            fernMeta.setLore(lore4);
        } else if (LojaAPI.valorCompra("samambaia", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("samambaia", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("samambaia", type));
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add(" ");
            fernMeta.setLore(lore4);
        } else if (LojaAPI.valorVenda("samambaia", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("samambaia", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("samambaia", type));
            lore4.add(" ");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore4.add(" ");
            fernMeta.setLore(lore4);
        }
        fern.setItemMeta(fernMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(14, fern);


        final ItemStack grama = new ItemStack(Material.GRASS);
        final ItemMeta gramaMeta = grama.getItemMeta();
        gramaMeta.setDisplayName("§eGrama");
        final ArrayList<String> lore3 = new ArrayList<String>();
        if (LojaAPI.valorVenda("grama", type) != 0 && LojaAPI.valorCompra("grama", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("grama", type)));
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("grama", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("grama", type));
            lore3.add(" ");
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            gramaMeta.setLore(lore3);
        } else if (LojaAPI.valorCompra("grama", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("grama", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("grama", type));
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add(" ");
            gramaMeta.setLore(lore3);
        } else if (LojaAPI.valorVenda("grama", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("grama", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("grama", type));
            lore3.add(" ");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore3.add(" ");
            gramaMeta.setLore(lore3);
        }
        grama.setItemMeta(gramaMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(13, grama);


        final ItemStack fjungle = new ItemStack(Material.JUNGLE_LEAVES);
        final ItemMeta fjungleMeta = fjungle.getItemMeta();
        fjungleMeta.setDisplayName("§eFolhas da selva");
        final ArrayList<String> lore2 = new ArrayList<String>();
        if (LojaAPI.valorVenda("folhas_da_selva", type) != 0 && LojaAPI.valorCompra("folhas_da_selva", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("folhas_da_selva", type)));
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("folhas_da_selva", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("folhas_da_selva", type));
            lore2.add(" ");
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            fjungleMeta.setLore(lore2);
        } else if (LojaAPI.valorCompra("folhas_da_selva", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("folhas_da_selva", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("folhas_da_selva", type));
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add(" ");
            fjungleMeta.setLore(lore2);
        } else if (LojaAPI.valorVenda("folhas_da_selva", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("folhas_da_selva", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("folhas_da_selva", type));
            lore2.add(" ");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore2.add(" ");
            fjungleMeta.setLore(lore2);
        }
        fjungle.setItemMeta(fjungleMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(12, fjungle);


        final ItemStack fpinheiro = new ItemStack(Material.SPRUCE_LEAVES);
        final ItemMeta fpinheiroMeta = fpinheiro.getItemMeta();
        fpinheiroMeta.setDisplayName("§eFolhas de pinheiro");
        final ArrayList<String> lore1 = new ArrayList<String>();
        if (LojaAPI.valorVenda("folhas_de_pinheiro", type) != 0 && LojaAPI.valorCompra("folhas_de_pinheiro", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("folhas_de_pinheiro", type)));
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("folhas_de_pinheiro", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("folhas_de_pinheiro", type));
            lore1.add(" ");
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            fpinheiroMeta.setLore(lore1);
        } else if (LojaAPI.valorCompra("folhas_de_pinheiro", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("folhas_de_pinheiro", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("folhas_de_pinheiro", type));
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add(" ");
            fpinheiroMeta.setLore(lore1);
        } else if (LojaAPI.valorVenda("folhas_de_pinheiro", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("folhas_de_pinheiro", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("folhas_de_pinheiro", type));
            lore1.add(" ");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore1.add(" ");
            fpinheiroMeta.setLore(lore1);
        }
        fpinheiro.setItemMeta(fpinheiroMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(11, fpinheiro);


        final ItemStack fcarvalho = new ItemStack(Material.OAK_LEAVES);
        final ItemMeta fcarvalhoMeta = fcarvalho.getItemMeta();
        fcarvalhoMeta.setDisplayName("§eFolhas de carvalho");
        final ArrayList<String> lore = new ArrayList<String>();
        if (LojaAPI.valorVenda("folhas_de_carvalho", type) != 0 && LojaAPI.valorCompra("folhas_de_carvalho", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("folhas_de_carvalho", type)));
            lore.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("folhas_de_carvalho", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("folhas_de_carvalho", type));
            lore.add(" ");
            lore.add(" ");
            lore.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            fcarvalhoMeta.setLore(lore);
        } else if (LojaAPI.valorCompra("folhas_de_carvalho", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("folhas_de_carvalho", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("folhas_de_carvalho", type));
            lore.add(" ");
            lore.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore.add(" ");
            fcarvalhoMeta.setLore(lore);
        } else if (LojaAPI.valorVenda("folhas_de_carvalho", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("folhas_de_carvalho", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("folhas_de_carvalho", type));
            lore.add(" ");
            lore.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore.add(" ");
            fcarvalhoMeta.setLore(lore);
        }
        fcarvalho.setItemMeta(fcarvalhoMeta);
        INVENTORY_LOJADECORATIVOS1.setItem(10, fcarvalho);

        return INVENTORY_LOJADECORATIVOS1;
    }

    public static Inventory buildLojaBlocos2(final String type) {
        final Inventory INVENTORY_LOJABLOCOS2 = Bukkit.createInventory(null, 5 * 9, "Blocos: ");

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta voltarMeta = voltar.getItemMeta();
        voltarMeta.setDisplayName(ChatColor.GREEN + "Voltar página.");
        voltar.setItemMeta(voltarMeta);
        INVENTORY_LOJABLOCOS2.setItem(36, voltar);


        final ItemStack driedkelp = new ItemStack(Material.DRIED_KELP_BLOCK);
        final ItemMeta driedkelpMeta = driedkelp.getItemMeta();
        driedkelpMeta.setDisplayName("§eBloco de algas secas");
        final ArrayList<String> lore16 = new ArrayList<String>();
        if (LojaAPI.valorVenda("bloco_de_algas_secas", type) != 0 && LojaAPI.valorCompra("bloco_de_algas_secas", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bloco_de_algas_secas", type)));
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bloco_de_algas_secas", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_algas_secas", type));
            lore16.add(" ");
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            driedkelpMeta.setLore(lore16);
        } else if (LojaAPI.valorCompra("bloco_de_algas_secas", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bloco_de_algas_secas", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_algas_secas", type));
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add(" ");
            driedkelpMeta.setLore(lore16);
        } else if (LojaAPI.valorVenda("bloco_de_algas_secas", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bloco_de_algas_secas", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_algas_secas", type));
            lore16.add(" ");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore16.add(" ");
            driedkelpMeta.setLore(lore16);
        }
        driedkelp.setItemMeta(driedkelpMeta);
        INVENTORY_LOJABLOCOS2.setItem(32, driedkelp);


        final ItemStack redbrick = new ItemStack(Material.RED_NETHER_BRICKS);
        final ItemMeta redbrickMeta = redbrick.getItemMeta();
        redbrickMeta.setDisplayName("§eTijolos vermelhos do nether");
        final ArrayList<String> lore15 = new ArrayList<String>();
        if (LojaAPI.valorVenda("tijolos_vermelhos_do_nether", type) != 0 && LojaAPI.valorCompra("tijolos_vermelhos_do_nether", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tijolos_vermelhos_do_nether", type)));
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tijolos_vermelhos_do_nether", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("tijolos_vermelhos_do_nether", type));
            lore15.add(" ");
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            redbrickMeta.setLore(lore15);
        } else if (LojaAPI.valorCompra("tijolos_vermelhos_do_nether", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tijolos_vermelhos_do_nether", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("tijolos_vermelhos_do_nether", type));
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add(" ");
            redbrickMeta.setLore(lore15);
        } else if (LojaAPI.valorVenda("tijolos_vermelhos_do_nether", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tijolos_vermelhos_do_nether", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("tijolos_vermelhos_do_nether", type));
            lore15.add(" ");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore15.add(" ");
            redbrickMeta.setLore(lore15);
        }
        redbrick.setItemMeta(redbrickMeta);
        INVENTORY_LOJABLOCOS2.setItem(31, redbrick);


        final ItemStack netherwart = new ItemStack(Material.NETHER_WART_BLOCK);
        final ItemMeta netherwartMeta = netherwart.getItemMeta();
        netherwartMeta.setDisplayName("§eBloco de fungo do nether");
        final ArrayList<String> lore14 = new ArrayList<String>();
        if (LojaAPI.valorVenda("bloco_de_fungo_do_nether", type) != 0 && LojaAPI.valorCompra("bloco_de_fungo_do_nether", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bloco_de_fungo_do_nether", type)));
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bloco_de_fungo_do_nether", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_fungo_do_nether", type));
            lore14.add(" ");
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            netherwartMeta.setLore(lore14);
        } else if (LojaAPI.valorCompra("bloco_de_fungo_do_nether", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bloco_de_fungo_do_nether", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_fungo_do_nether", type));
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add(" ");
            netherwartMeta.setLore(lore14);
        } else if (LojaAPI.valorVenda("bloco_de_fungo_do_nether", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bloco_de_fungo_do_nether", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_fungo_do_nether", type));
            lore14.add(" ");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore14.add(" ");
            netherwartMeta.setLore(lore14);
        }
        netherwart.setItemMeta(netherwartMeta);
        INVENTORY_LOJABLOCOS2.setItem(30, netherwart);


        final ItemStack magma = new ItemStack(Material.MAGMA_BLOCK);
        final ItemMeta magmaMeta = magma.getItemMeta();
        magmaMeta.setDisplayName("§eBloco de magma");
        final ArrayList<String> lore13 = new ArrayList<String>();
        if (LojaAPI.valorVenda("bloco_de_magma", type) != 0 && LojaAPI.valorCompra("bloco_de_magma", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bloco_de_magma", type)));
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bloco_de_magma", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_magma", type));
            lore13.add(" ");
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            magmaMeta.setLore(lore13);
        } else if (LojaAPI.valorCompra("bloco_de_magma", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bloco_de_magma", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_magma", type));
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add(" ");
            magmaMeta.setLore(lore13);
        } else if (LojaAPI.valorVenda("bloco_de_magma", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bloco_de_magma", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_magma", type));
            lore13.add(" ");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore13.add(" ");
            magmaMeta.setLore(lore13);
        }
        magma.setItemMeta(magmaMeta);
        INVENTORY_LOJABLOCOS2.setItem(25, magma);


        final ItemStack arenitovl = new ItemStack(Material.CUT_RED_SANDSTONE);
        final ItemMeta arenitovlMeta = arenitovl.getItemMeta();
        arenitovlMeta.setDisplayName("§eArenito vermelho lapidado");
        final ArrayList<String> lore12 = new ArrayList<String>();
        if (LojaAPI.valorVenda("arenito_vermelho_lapidado", type) != 0 && LojaAPI.valorCompra("arenito_vermelho_lapidado", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("arenito_vermelho_lapidado", type)));
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("arenito_vermelho_lapidado", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("arenito_vermelho_lapidado", type));
            lore12.add(" ");
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            arenitovlMeta.setLore(lore12);
        } else if (LojaAPI.valorCompra("arenito_vermelho_lapidado", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("arenito_vermelho_lapidado", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("arenito_vermelho_lapidado", type));
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add(" ");
            arenitovlMeta.setLore(lore12);
        } else if (LojaAPI.valorVenda("arenito_vermelho_lapidado", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("arenito_vermelho_lapidado", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("arenito_vermelho_lapidado", type));
            lore12.add(" ");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore12.add(" ");
            arenitovlMeta.setLore(lore12);
        }
        arenitovl.setItemMeta(arenitovlMeta);
        INVENTORY_LOJABLOCOS2.setItem(24, arenitovl);


        final ItemStack arenitovt = new ItemStack(Material.CHISELED_RED_SANDSTONE);
        final ItemMeta arenitovtMeta = arenitovt.getItemMeta();
        arenitovtMeta.setDisplayName("§eArenito vermelho talhado");
        final ArrayList<String> lore11 = new ArrayList<String>();
        if (LojaAPI.valorVenda("arenito_vermelho_talhado", type) != 0 && LojaAPI.valorCompra("arenito_vermelho_talhado", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("arenito_vermelho_talhado", type)));
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("arenito_vermelho_talhado", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("arenito_vermelho_talhado", type));
            lore11.add(" ");
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            arenitovtMeta.setLore(lore11);
        } else if (LojaAPI.valorCompra("arenito_vermelho_talhado", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("arenito_vermelho_talhado", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("arenito_vermelho_talhado", type));
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add(" ");
            arenitovtMeta.setLore(lore11);
        } else if (LojaAPI.valorVenda("arenito_vermelho_talhado", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("arenito_vermelho_talhado", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("arenito_vermelho_talhado", type));
            lore11.add(" ");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore11.add(" ");
            arenitovtMeta.setLore(lore11);
        }
        arenitovt.setItemMeta(arenitovtMeta);
        INVENTORY_LOJABLOCOS2.setItem(23, arenitovt);


        final ItemStack arenitov = new ItemStack(Material.RED_SANDSTONE);
        final ItemMeta arenitovMeta = arenitov.getItemMeta();
        arenitovMeta.setDisplayName("§eArenito vermelho");
        final ArrayList<String> lore10 = new ArrayList<String>();
        if (LojaAPI.valorVenda("arenito_vermelho", type) != 0 && LojaAPI.valorCompra("arenito_vermelho", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("arenito_vermelho", type)));
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("arenito_vermelho", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("arenito_vermelho", type));
            lore10.add(" ");
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            arenitovMeta.setLore(lore10);
        } else if (LojaAPI.valorCompra("arenito_vermelho", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("arenito_vermelho", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("arenito_vermelho", type));
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add(" ");
            arenitovMeta.setLore(lore10);
        } else if (LojaAPI.valorVenda("arenito_vermelho", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("arenito_vermelho", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("arenito_vermelho", type));
            lore10.add(" ");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore10.add(" ");
            arenitovMeta.setLore(lore10);
        }
        arenitov.setItemMeta(arenitovMeta);
        INVENTORY_LOJABLOCOS2.setItem(22, arenitov);


        final ItemStack sealantern = new ItemStack(Material.SEA_LANTERN);
        final ItemMeta sealanternMeta = sealantern.getItemMeta();
        sealanternMeta.setDisplayName("§eLanterna do mar");
        final ArrayList<String> lore9 = new ArrayList<String>();
        if (LojaAPI.valorVenda("lanterna_do_mar", type) != 0 && LojaAPI.valorCompra("lanterna_do_mar", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("lanterna_do_mar", type)));
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("lanterna_do_mar", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("lanterna_do_mar", type));
            lore9.add(" ");
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            sealanternMeta.setLore(lore9);
        } else if (LojaAPI.valorCompra("lanterna_do_mar", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("lanterna_do_mar", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("lanterna_do_mar", type));
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add(" ");
            sealanternMeta.setLore(lore9);
        } else if (LojaAPI.valorVenda("lanterna_do_mar", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("lanterna_do_mar", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("lanterna_do_mar", type));
            lore9.add(" ");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore9.add(" ");
            sealanternMeta.setLore(lore9);
        }
        sealantern.setItemMeta(sealanternMeta);
        INVENTORY_LOJABLOCOS2.setItem(21, sealantern);


        final ItemStack prismarinee = new ItemStack(Material.DARK_PRISMARINE);
        final ItemMeta prismarineeMeta = prismarinee.getItemMeta();
        prismarineeMeta.setDisplayName("§ePrismarinho escuro");
        final ArrayList<String> lore8 = new ArrayList<String>();
        if (LojaAPI.valorVenda("prismarinho_escuro", type) != 0 && LojaAPI.valorCompra("prismarinho_escuro", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("prismarinho_escuro", type)));
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("prismarinho_escuro", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("prismarinho_escuro", type));
            lore8.add(" ");
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            prismarineeMeta.setLore(lore8);
        } else if (LojaAPI.valorCompra("prismarinho_escuro", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("prismarinho_escuro", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("prismarinho_escuro", type));
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add(" ");
            prismarineeMeta.setLore(lore8);
        } else if (LojaAPI.valorVenda("prismarinho_escuro", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("prismarinho_escuro", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("prismarinho_escuro", type));
            lore8.add(" ");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore8.add(" ");
            prismarineeMeta.setLore(lore8);
        }
        prismarinee.setItemMeta(prismarineeMeta);
        INVENTORY_LOJABLOCOS2.setItem(20, prismarinee);


        final ItemStack prismarine = new ItemStack(Material.PRISMARINE);
        final ItemMeta prismarineMeta = prismarine.getItemMeta();
        prismarineMeta.setDisplayName("§ePrismarinho");
        final ArrayList<String> lore7 = new ArrayList<String>();
        if (LojaAPI.valorVenda("prismarinho", type) != 0 && LojaAPI.valorCompra("prismarinho", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("prismarinho", type)));
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("prismarinho", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("prismarinho", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            prismarineMeta.setLore(lore7);
        } else if (LojaAPI.valorCompra("prismarinho", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("prismarinho", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("prismarinho", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add(" ");
            prismarineMeta.setLore(lore7);
        } else if (LojaAPI.valorVenda("prismarinho", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("prismarinho", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("prismarinho", type));
            lore7.add(" ");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore7.add(" ");
            prismarineMeta.setLore(lore7);
        }
        prismarine.setItemMeta(prismarineMeta);
        INVENTORY_LOJABLOCOS2.setItem(19, prismarine);


        final ItemStack quartzb = new ItemStack(Material.QUARTZ_BLOCK);
        final ItemMeta quartzbMeta = quartzb.getItemMeta();
        quartzbMeta.setDisplayName("§eBloco de quartzo");
        final ArrayList<String> lore6 = new ArrayList<String>();
        if (LojaAPI.valorVenda("bloco_de_quartzo", type) != 0 && LojaAPI.valorCompra("bloco_de_quartzo", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bloco_de_quartzo", type)));
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bloco_de_quartzo", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_quartzo", type));
            lore6.add(" ");
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            quartzbMeta.setLore(lore6);
        } else if (LojaAPI.valorCompra("bloco_de_quartzo", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bloco_de_quartzo", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_quartzo", type));
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add(" ");
            quartzbMeta.setLore(lore6);
        } else if (LojaAPI.valorVenda("bloco_de_quartzo", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bloco_de_quartzo", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_quartzo", type));
            lore6.add(" ");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore6.add(" ");
            quartzbMeta.setLore(lore6);
        }
        quartzb.setItemMeta(quartzbMeta);
        INVENTORY_LOJABLOCOS2.setItem(16, quartzb);


        final ItemStack endstone = new ItemStack(Material.END_STONE);
        final ItemMeta endstoneMeta = endstone.getItemMeta();
        endstoneMeta.setDisplayName("§ePedra do end");
        final ArrayList<String> lore5 = new ArrayList<String>();
        if (LojaAPI.valorVenda("pedra_do_end", type) != 0 && LojaAPI.valorCompra("pedra_do_end", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pedra_do_end", type)));
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pedra_do_end", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("pedra_do_end", type));
            lore5.add(" ");
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            endstoneMeta.setLore(lore5);
        } else if (LojaAPI.valorCompra("pedra_do_end", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pedra_do_end", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("pedra_do_end", type));
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add(" ");
            endstoneMeta.setLore(lore5);
        } else if (LojaAPI.valorVenda("pedra_do_end", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pedra_do_end", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("pedra_do_end", type));
            lore5.add(" ");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore5.add(" ");
            endstoneMeta.setLore(lore5);
        }
        endstone.setItemMeta(endstoneMeta);
        INVENTORY_LOJABLOCOS2.setItem(15, endstone);


        final ItemStack netherb = new ItemStack(Material.NETHER_BRICKS);
        final ItemMeta netherbMeta = netherb.getItemMeta();
        netherbMeta.setDisplayName("§eTijolos do nether");
        final ArrayList<String> lore4 = new ArrayList<String>();
        if (LojaAPI.valorVenda("tijolos_do_nether", type) != 0 && LojaAPI.valorCompra("tijolos_do_nether", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tijolos_do_nether", type)));
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tijolos_do_nether", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("tijolos_do_nether", type));
            lore4.add(" ");
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            netherbMeta.setLore(lore4);
        } else if (LojaAPI.valorCompra("tijolos_do_nether", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tijolos_do_nether", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("tijolos_do_nether", type));
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add(" ");
            netherbMeta.setLore(lore4);
        } else if (LojaAPI.valorVenda("tijolos_do_nether", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tijolos_do_nether", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("tijolos_do_nether", type));
            lore4.add(" ");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore4.add(" ");
            netherbMeta.setLore(lore4);
        }
        netherb.setItemMeta(netherbMeta);
        INVENTORY_LOJABLOCOS2.setItem(14, netherb);


        final ItemStack mycel = new ItemStack(Material.MYCELIUM);
        final ItemMeta mycelMeta = mycel.getItemMeta();
        mycelMeta.setDisplayName("§eMicélio");
        final ArrayList<String> lore3 = new ArrayList<String>();
        if (LojaAPI.valorVenda("micelio", type) != 0 && LojaAPI.valorCompra("micelio", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("micelio", type)));
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("micelio", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("micelio", type));
            lore3.add(" ");
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            mycelMeta.setLore(lore3);
        } else if (LojaAPI.valorCompra("micelio", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("micelio", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("micelio", type));
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add(" ");
            mycelMeta.setLore(lore3);
        } else if (LojaAPI.valorVenda("micelio", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("micelio", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("micelio", type));
            lore3.add(" ");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore3.add(" ");
            mycelMeta.setLore(lore3);
        }
        mycel.setItemMeta(mycelMeta);
        INVENTORY_LOJABLOCOS2.setItem(13, mycel);


        final ItemStack brick = new ItemStack(Material.CLAY);
        final ItemMeta brickMeta = brick.getItemMeta();
        brickMeta.setDisplayName("§eArgila");
        final ArrayList<String> lore2 = new ArrayList<String>();
        if (LojaAPI.valorVenda("argila", type) != 0 && LojaAPI.valorCompra("argila", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("argila", type)));
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("argila", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("argila", type));
            lore2.add(" ");
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            brickMeta.setLore(lore2);
        } else if (LojaAPI.valorCompra("argila", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("argila", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("argila", type));
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add(" ");
            brickMeta.setLore(lore2);
        } else if (LojaAPI.valorVenda("argila", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("argila", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("argila", type));
            lore2.add(" ");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore2.add(" ");
            brickMeta.setLore(lore2);
        }
        brick.setItemMeta(brickMeta);
        INVENTORY_LOJABLOCOS2.setItem(12, brick);


        final ItemStack clay = new ItemStack(Material.CLAY);
        final ItemMeta clayMeta = clay.getItemMeta();
        clayMeta.setDisplayName("§eArgila");
        final ArrayList<String> lore1 = new ArrayList<String>();
        if (LojaAPI.valorVenda("argila", type) != 0 && LojaAPI.valorCompra("argila", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("argila", type)));
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("argila", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("argila", type));
            lore1.add(" ");
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            clayMeta.setLore(lore1);
        } else if (LojaAPI.valorCompra("argila", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("argila", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("argila", type));
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add(" ");
            clayMeta.setLore(lore1);
        } else if (LojaAPI.valorVenda("argila", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("argila", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("argila", type));
            lore1.add(" ");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore1.add(" ");
            clayMeta.setLore(lore1);
        }
        clay.setItemMeta(clayMeta);
        INVENTORY_LOJABLOCOS2.setItem(11, clay);


        final ItemStack purpura = new ItemStack(Material.PURPUR_BLOCK);
        final ItemMeta purpuraMeta = purpura.getItemMeta();
        purpuraMeta.setDisplayName("§eBloco de púrpura");
        final ArrayList<String> lore = new ArrayList<String>();
        if (LojaAPI.valorVenda("bloco_de_purpura", type) != 0 && LojaAPI.valorCompra("bloco_de_purpura", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bloco_de_purpura", type)));
            lore.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bloco_de_purpura", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_purpura", type));
            lore.add(" ");
            lore.add(" ");
            lore.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            purpuraMeta.setLore(lore);
        } else if (LojaAPI.valorCompra("bloco_de_purpura", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("bloco_de_purpura", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_purpura", type));
            lore.add(" ");
            lore.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore.add(" ");
            purpuraMeta.setLore(lore);
        } else if (LojaAPI.valorVenda("bloco_de_purpura", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("bloco_de_purpura", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("bloco_de_purpura", type));
            lore.add(" ");
            lore.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore.add(" ");
            purpuraMeta.setLore(lore);
        }
        purpura.setItemMeta(purpuraMeta);
        INVENTORY_LOJABLOCOS2.setItem(10, purpura);

        return INVENTORY_LOJABLOCOS2;
    }

    public static Inventory buildLojaBlocos1(final String type) {
        final Inventory INVENTORY_LOJABLOCOS1 = Bukkit.createInventory(null, 6 * 9, "Blocos: ");

        final ItemStack proxima = new ItemStack(Material.ARROW);
        final ItemMeta proximaMeta = proxima.getItemMeta();
        proximaMeta.setDisplayName(ChatColor.GREEN + "Próxima página.");
        proxima.setItemMeta(proximaMeta);
        INVENTORY_LOJABLOCOS1.setItem(53, proxima);

        final ItemStack mossyc = new ItemStack(Material.MOSSY_COBBLESTONE);
        final ItemMeta mossycMeta = mossyc.getItemMeta();
        mossycMeta.setDisplayName("§ePedregulho musgoso");
        final ArrayList<String> lore27 = new ArrayList<String>();
        if (LojaAPI.valorVenda("pedregulho_musgoso", type) != 0 && LojaAPI.valorCompra("pedregulho_musgoso", type) != 0) {
            lore27.add(" ");
            lore27.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pedregulho_musgoso", type)));
            lore27.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pedregulho_musgoso", type)));
            lore27.add("§7Quantia: §a" + LojaAPI.itemQuantia("pedregulho_musgoso", type));
            lore27.add(" ");
            lore27.add(" ");
            lore27.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore27.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            mossycMeta.setLore(lore27);
        } else if (LojaAPI.valorCompra("pedregulho_musgoso", type) != 0) {
            lore27.add(" ");
            lore27.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pedregulho_musgoso", type)));
            lore27.add("§7Quantia: §a" + LojaAPI.itemQuantia("pedregulho_musgoso", type));
            lore27.add(" ");
            lore27.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore27.add(" ");
            mossycMeta.setLore(lore27);
        } else if (LojaAPI.valorVenda("pedregulho_musgoso", type) != 0) {
            lore27.add(" ");
            lore27.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pedregulho_musgoso", type)));
            lore27.add("§7Quantia: §a" + LojaAPI.itemQuantia("pedregulho_musgoso", type));
            lore27.add(" ");
            lore27.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore27.add(" ");
            mossycMeta.setLore(lore27);
        }
        mossyc.setItemMeta(mossycMeta);
        INVENTORY_LOJABLOCOS1.setItem(43, mossyc);


        final ItemStack brick = new ItemStack(Material.BRICKS);
        final ItemMeta brickMeta = brick.getItemMeta();
        brickMeta.setDisplayName("§eTijolos");
        final ArrayList<String> lore26 = new ArrayList<String>();
        if (LojaAPI.valorVenda("tijolos", type) != 0 && LojaAPI.valorCompra("tijolos", type) != 0) {
            lore26.add(" ");
            lore26.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tijolos", type)));
            lore26.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tijolos", type)));
            lore26.add("§7Quantia: §a" + LojaAPI.itemQuantia("tijolos", type));
            lore26.add(" ");
            lore26.add(" ");
            lore26.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore26.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            brickMeta.setLore(lore26);
        } else if (LojaAPI.valorCompra("tijolos", type) != 0) {
            lore26.add(" ");
            lore26.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tijolos", type)));
            lore26.add("§7Quantia: §a" + LojaAPI.itemQuantia("tijolos", type));
            lore26.add(" ");
            lore26.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore26.add(" ");
            brickMeta.setLore(lore26);
        } else if (LojaAPI.valorVenda("tijolos", type) != 0) {
            lore26.add(" ");
            lore26.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tijolos", type)));
            lore26.add("§7Quantia: §a" + LojaAPI.itemQuantia("tijolos", type));
            lore26.add(" ");
            lore26.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore26.add(" ");
            brickMeta.setLore(lore26);
        }
        brick.setItemMeta(brickMeta);
        INVENTORY_LOJABLOCOS1.setItem(42, brick);


        final ItemStack pedralisa = new ItemStack(Material.SMOOTH_STONE);
        final ItemMeta pedralisaMeta = pedralisa.getItemMeta();
        pedralisaMeta.setDisplayName("§ePedra lisa");
        final ArrayList<String> lore25 = new ArrayList<String>();
        if (LojaAPI.valorVenda("pedra_lisa", type) != 0 && LojaAPI.valorCompra("pedra_lisa", type) != 0) {
            lore25.add(" ");
            lore25.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pedra_lisa", type)));
            lore25.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pedra_lisa", type)));
            lore25.add("§7Quantia: §a" + LojaAPI.itemQuantia("pedra_lisa", type));
            lore25.add(" ");
            lore25.add(" ");
            lore25.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore25.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            pedralisaMeta.setLore(lore25);
        } else if (LojaAPI.valorCompra("pedra_lisa", type) != 0) {
            lore25.add(" ");
            lore25.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pedra_lisa", type)));
            lore25.add("§7Quantia: §a" + LojaAPI.itemQuantia("pedra_lisa", type));
            lore25.add(" ");
            lore25.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore25.add(" ");
            pedralisaMeta.setLore(lore25);
        } else if (LojaAPI.valorVenda("pedra_lisa", type) != 0) {
            lore25.add(" ");
            lore25.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pedra_lisa", type)));
            lore25.add("§7Quantia: §a" + LojaAPI.itemQuantia("pedra_lisa", type));
            lore25.add(" ");
            lore25.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore25.add(" ");
            pedralisaMeta.setLore(lore25);
        }
        pedralisa.setItemMeta(pedralisaMeta);
        INVENTORY_LOJABLOCOS1.setItem(41, pedralisa);


        final ItemStack sandstone3 = new ItemStack(Material.SMOOTH_SANDSTONE);
        final ItemMeta sandstone3Meta = sandstone3.getItemMeta();
        sandstone3Meta.setDisplayName("§eArenito liso");
        final ArrayList<String> lore24 = new ArrayList<String>();
        if (LojaAPI.valorVenda("arenito_liso", type) != 0 && LojaAPI.valorCompra("arenito_liso", type) != 0) {
            lore24.add(" ");
            lore24.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("arenito_liso", type)));
            lore24.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("arenito_liso", type)));
            lore24.add("§7Quantia: §a" + LojaAPI.itemQuantia("arenito_liso", type));
            lore24.add(" ");
            lore24.add(" ");
            lore24.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore24.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            sandstone3Meta.setLore(lore24);
        } else if (LojaAPI.valorCompra("arenito_liso", type) != 0) {
            lore24.add(" ");
            lore24.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("arenito_liso", type)));
            lore24.add("§7Quantia: §a" + LojaAPI.itemQuantia("arenito_liso", type));
            lore24.add(" ");
            lore24.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore24.add(" ");
            sandstone3Meta.setLore(lore24);
        } else if (LojaAPI.valorVenda("arenito_liso", type) != 0) {
            lore24.add(" ");
            lore24.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("arenito_liso", type)));
            lore24.add("§7Quantia: §a" + LojaAPI.itemQuantia("arenito_liso", type));
            lore24.add(" ");
            lore24.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore24.add(" ");
            sandstone3Meta.setLore(lore24);
        }
        sandstone3.setItemMeta(sandstone3Meta);
        INVENTORY_LOJABLOCOS1.setItem(40, sandstone3);


        final ItemStack sandstone2 = new ItemStack(Material.CUT_SANDSTONE);
        final ItemMeta sandstone2Meta = sandstone2.getItemMeta();
        sandstone2Meta.setDisplayName("§eArenito lapidado");
        final ArrayList<String> lore23 = new ArrayList<String>();
        if (LojaAPI.valorVenda("arenito_lapidado", type) != 0 && LojaAPI.valorCompra("arenito_lapidado", type) != 0) {
            lore23.add(" ");
            lore23.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("arenito_lapidado", type)));
            lore23.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("arenito_lapidado", type)));
            lore23.add("§7Quantia: §a" + LojaAPI.itemQuantia("arenito_lapidado", type));
            lore23.add(" ");
            lore23.add(" ");
            lore23.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore23.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            sandstone2Meta.setLore(lore23);
        } else if (LojaAPI.valorCompra("arenito_lapidado", type) != 0) {
            lore23.add(" ");
            lore23.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("arenito_lapidado", type)));
            lore23.add("§7Quantia: §a" + LojaAPI.itemQuantia("arenito_lapidado", type));
            lore23.add(" ");
            lore23.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore23.add(" ");
            sandstone2Meta.setLore(lore23);
        } else if (LojaAPI.valorVenda("arenito_lapidado", type) != 0) {
            lore23.add(" ");
            lore23.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("arenito_lapidado", type)));
            lore23.add("§7Quantia: §a" + LojaAPI.itemQuantia("arenito_lapidado", type));
            lore23.add(" ");
            lore23.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore23.add(" ");
            sandstone2Meta.setLore(lore23);
        }
        sandstone2.setItemMeta(sandstone2Meta);
        INVENTORY_LOJABLOCOS1.setItem(39, sandstone2);


        final ItemStack sandstone1 = new ItemStack(Material.CHISELED_SANDSTONE);
        final ItemMeta sandstone1Meta = sandstone1.getItemMeta();
        sandstone1Meta.setDisplayName("§eArenito talhado");
        final ArrayList<String> lore22 = new ArrayList<String>();
        if (LojaAPI.valorVenda("arenito_talhado", type) != 0 && LojaAPI.valorCompra("arenito_talhado", type) != 0) {
            lore22.add(" ");
            lore22.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("arenito_talhado", type)));
            lore22.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("arenito_talhado", type)));
            lore22.add("§7Quantia: §a" + LojaAPI.itemQuantia("arenito_talhado", type));
            lore22.add(" ");
            lore22.add(" ");
            lore22.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore22.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            sandstone1Meta.setLore(lore22);
        } else if (LojaAPI.valorCompra("arenito_talhado", type) != 0) {
            lore22.add(" ");
            lore22.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("arenito_talhado", type)));
            lore22.add("§7Quantia: §a" + LojaAPI.itemQuantia("arenito_talhado", type));
            lore22.add(" ");
            lore22.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore22.add(" ");
            sandstone1Meta.setLore(lore22);
        } else if (LojaAPI.valorVenda("arenito_talhado", type) != 0) {
            lore22.add(" ");
            lore22.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("arenito_talhado", type)));
            lore22.add("§7Quantia: §a" + LojaAPI.itemQuantia("arenito_talhado", type));
            lore22.add(" ");
            lore22.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore22.add(" ");
            sandstone1Meta.setLore(lore22);
        }
        sandstone1.setItemMeta(sandstone1Meta);
        INVENTORY_LOJABLOCOS1.setItem(38, sandstone1);


        final ItemStack sandstone = new ItemStack(Material.SANDSTONE);
        final ItemMeta sandstoneMeta = sandstone.getItemMeta();
        sandstoneMeta.setDisplayName("§eArenito");
        final ArrayList<String> lore21 = new ArrayList<String>();
        if (LojaAPI.valorVenda("arenito", type) != 0 && LojaAPI.valorCompra("arenito", type) != 0) {
            lore21.add(" ");
            lore21.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("arenito", type)));
            lore21.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("arenito", type)));
            lore21.add("§7Quantia: §a" + LojaAPI.itemQuantia("arenito", type));
            lore21.add(" ");
            lore21.add(" ");
            lore21.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore21.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            sandstoneMeta.setLore(lore21);
        } else if (LojaAPI.valorCompra("arenito", type) != 0) {
            lore21.add(" ");
            lore21.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("arenito", type)));
            lore21.add("§7Quantia: §a" + LojaAPI.itemQuantia("arenito", type));
            lore21.add(" ");
            lore21.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore21.add(" ");
            sandstoneMeta.setLore(lore21);
        } else if (LojaAPI.valorVenda("arenito", type) != 0) {
            lore21.add(" ");
            lore21.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("arenito", type)));
            lore21.add("§7Quantia: §a" + LojaAPI.itemQuantia("arenito", type));
            lore21.add(" ");
            lore21.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore21.add(" ");
            sandstoneMeta.setLore(lore21);
        }
        sandstone.setItemMeta(sandstoneMeta);
        INVENTORY_LOJABLOCOS1.setItem(37, sandstone);


        final ItemStack carvalhoe = new ItemStack(Material.DARK_OAK_LOG);
        final ItemMeta carvalhoeMeta = carvalhoe.getItemMeta();
        carvalhoeMeta.setDisplayName("§eTronco de carvalho escuro");
        final ArrayList<String> lore20 = new ArrayList<String>();
        if (LojaAPI.valorVenda("tronco_de_carvalho_escuro", type) != 0 && LojaAPI.valorCompra("tronco_de_carvalho_escuro", type) != 0) {
            lore20.add(" ");
            lore20.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tronco_de_carvalho_escuro", type)));
            lore20.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tronco_de_carvalho_escuro", type)));
            lore20.add("§7Quantia: §a" + LojaAPI.itemQuantia("tronco_de_carvalho_escuro", type));
            lore20.add(" ");
            lore20.add(" ");
            lore20.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore20.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            carvalhoeMeta.setLore(lore20);
        } else if (LojaAPI.valorCompra("tronco_de_carvalho_escuro", type) != 0) {
            lore20.add(" ");
            lore20.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tronco_de_carvalho_escuro", type)));
            lore20.add("§7Quantia: §a" + LojaAPI.itemQuantia("tronco_de_carvalho_escuro", type));
            lore20.add(" ");
            lore20.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore20.add(" ");
            carvalhoeMeta.setLore(lore20);
        } else if (LojaAPI.valorVenda("tronco_de_carvalho_escuro", type) != 0) {
            lore20.add(" ");
            lore20.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tronco_de_carvalho_escuro", type)));
            lore20.add("§7Quantia: §a" + LojaAPI.itemQuantia("tronco_de_carvalho_escuro", type));
            lore20.add(" ");
            lore20.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore20.add(" ");
            carvalhoeMeta.setLore(lore20);
        }
        carvalhoe.setItemMeta(carvalhoeMeta);
        INVENTORY_LOJABLOCOS1.setItem(34, carvalhoe);


        final ItemStack acacia = new ItemStack(Material.ACACIA_LOG);
        final ItemMeta acaciaMeta = acacia.getItemMeta();
        acaciaMeta.setDisplayName("§eTronco de acácia");
        final ArrayList<String> lore19 = new ArrayList<String>();
        if (LojaAPI.valorVenda("tronco_de_acacia", type) != 0 && LojaAPI.valorCompra("tronco_de_acacia", type) != 0) {
            lore19.add(" ");
            lore19.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tronco_de_acacia", type)));
            lore19.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tronco_de_acacia", type)));
            lore19.add("§7Quantia: §a" + LojaAPI.itemQuantia("tronco_de_acacia", type));
            lore19.add(" ");
            lore19.add(" ");
            lore19.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore19.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            acaciaMeta.setLore(lore19);
        } else if (LojaAPI.valorCompra("tronco_de_acacia", type) != 0) {
            lore19.add(" ");
            lore19.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tronco_de_acacia", type)));
            lore19.add("§7Quantia: §a" + LojaAPI.itemQuantia("tronco_de_acacia", type));
            lore19.add(" ");
            lore19.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore19.add(" ");
            acaciaMeta.setLore(lore19);
        } else if (LojaAPI.valorVenda("tronco_de_acacia", type) != 0) {
            lore19.add(" ");
            lore19.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tronco_de_acacia", type)));
            lore19.add("§7Quantia: §a" + LojaAPI.itemQuantia("tronco_de_acacia", type));
            lore19.add(" ");
            lore19.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore19.add(" ");
            acaciaMeta.setLore(lore19);
        }
        acacia.setItemMeta(acaciaMeta);
        INVENTORY_LOJABLOCOS1.setItem(33, acacia);


        final ItemStack selva = new ItemStack(Material.JUNGLE_LOG);
        final ItemMeta selvaMeta = selva.getItemMeta();
        selvaMeta.setDisplayName("§eTronco da selva");
        final ArrayList<String> lore18 = new ArrayList<String>();
        if (LojaAPI.valorVenda("tronco_da_selva", type) != 0 && LojaAPI.valorCompra("tronco_da_selva", type) != 0) {
            lore18.add(" ");
            lore18.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tronco_da_selva", type)));
            lore18.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tronco_da_selva", type)));
            lore18.add("§7Quantia: §a" + LojaAPI.itemQuantia("tronco_da_selva", type));
            lore18.add(" ");
            lore18.add(" ");
            lore18.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore18.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            selvaMeta.setLore(lore18);
        } else if (LojaAPI.valorCompra("tronco_da_selva", type) != 0) {
            lore18.add(" ");
            lore18.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tronco_da_selva", type)));
            lore18.add("§7Quantia: §a" + LojaAPI.itemQuantia("tronco_da_selva", type));
            lore18.add(" ");
            lore18.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore18.add(" ");
            selvaMeta.setLore(lore18);
        } else if (LojaAPI.valorVenda("tronco_da_selva", type) != 0) {
            lore18.add(" ");
            lore18.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tronco_da_selva", type)));
            lore18.add("§7Quantia: §a" + LojaAPI.itemQuantia("tronco_da_selva", type));
            lore18.add(" ");
            lore18.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore18.add(" ");
            selvaMeta.setLore(lore18);
        }
        selva.setItemMeta(selvaMeta);
        INVENTORY_LOJABLOCOS1.setItem(32, selva);


        final ItemStack eucalipto = new ItemStack(Material.BIRCH_LOG);
        final ItemMeta eucaliptoMeta = eucalipto.getItemMeta();
        eucaliptoMeta.setDisplayName("§eTronco de eucalipto");
        final ArrayList<String> lore17 = new ArrayList<String>();
        if (LojaAPI.valorVenda("tronco_de_eucalipto", type) != 0 && LojaAPI.valorCompra("tronco_de_eucalipto", type) != 0) {
            lore17.add(" ");
            lore17.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tronco_de_eucalipto", type)));
            lore17.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tronco_de_eucalipto", type)));
            lore17.add("§7Quantia: §a" + LojaAPI.itemQuantia("tronco_de_eucalipto", type));
            lore17.add(" ");
            lore17.add(" ");
            lore17.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore17.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            eucaliptoMeta.setLore(lore17);
        } else if (LojaAPI.valorCompra("tronco_de_eucalipto", type) != 0) {
            lore17.add(" ");
            lore17.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tronco_de_eucalipto", type)));
            lore17.add("§7Quantia: §a" + LojaAPI.itemQuantia("tronco_de_eucalipto", type));
            lore17.add(" ");
            lore17.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore17.add(" ");
            eucaliptoMeta.setLore(lore17);
        } else if (LojaAPI.valorVenda("tronco_de_eucalipto", type) != 0) {
            lore17.add(" ");
            lore17.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tronco_de_eucalipto", type)));
            lore17.add("§7Quantia: §a" + LojaAPI.itemQuantia("tronco_de_eucalipto", type));
            lore17.add(" ");
            lore17.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore17.add(" ");
            eucaliptoMeta.setLore(lore17);
        }
        eucalipto.setItemMeta(eucaliptoMeta);
        INVENTORY_LOJABLOCOS1.setItem(31, eucalipto);


        final ItemStack pinheiro = new ItemStack(Material.SPRUCE_LOG);
        final ItemMeta pinheiroMeta = pinheiro.getItemMeta();
        pinheiroMeta.setDisplayName("§eTronco de pinheiro");
        final ArrayList<String> lore16 = new ArrayList<String>();
        if (LojaAPI.valorVenda("tronco_de_pinheiro", type) != 0 && LojaAPI.valorCompra("tronco_de_pinheiro", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tronco_de_pinheiro", type)));
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tronco_de_pinheiro", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("tronco_de_pinheiro", type));
            lore16.add(" ");
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            pinheiroMeta.setLore(lore16);
        } else if (LojaAPI.valorCompra("tronco_de_pinheiro", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tronco_de_pinheiro", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("tronco_de_pinheiro", type));
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add(" ");
            pinheiroMeta.setLore(lore16);
        } else if (LojaAPI.valorVenda("tronco_de_pinheiro", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tronco_de_pinheiro", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("tronco_de_pinheiro", type));
            lore16.add(" ");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore16.add(" ");
            pinheiroMeta.setLore(lore16);
        }
        pinheiro.setItemMeta(pinheiroMeta);
        INVENTORY_LOJABLOCOS1.setItem(30, pinheiro);


        final ItemStack carvalho = new ItemStack(Material.OAK_LOG);
        final ItemMeta carvalhoMeta = carvalho.getItemMeta();
        carvalhoMeta.setDisplayName("§eTronco de carvalho");
        final ArrayList<String> lore15 = new ArrayList<String>();
        if (LojaAPI.valorVenda("tronco_de_carvalho", type) != 0 && LojaAPI.valorCompra("tronco_de_carvalho", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tronco_de_carvalho", type)));
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tronco_de_carvalho", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("tronco_de_carvalho", type));
            lore15.add(" ");
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            carvalhoMeta.setLore(lore15);
        } else if (LojaAPI.valorCompra("tronco_de_carvalho", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("tronco_de_carvalho", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("tronco_de_carvalho", type));
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add(" ");
            carvalhoMeta.setLore(lore15);
        } else if (LojaAPI.valorVenda("tronco_de_carvalho", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("tronco_de_carvalho", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("tronco_de_carvalho", type));
            lore15.add(" ");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore15.add(" ");
            carvalhoMeta.setLore(lore15);
        }
        carvalho.setItemMeta(carvalhoMeta);
        INVENTORY_LOJABLOCOS1.setItem(29, carvalho);


        final ItemStack gravel = new ItemStack(Material.GRAVEL);
        final ItemMeta gravelMeta = gravel.getItemMeta();
        gravelMeta.setDisplayName("§eCascalho");
        final ArrayList<String> lore14 = new ArrayList<String>();
        if (LojaAPI.valorVenda("cascalho", type) != 0 && LojaAPI.valorCompra("cascalho", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("cascalho", type)));
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("cascalho", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("cascalho", type));
            lore14.add(" ");
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            gravelMeta.setLore(lore14);
        } else if (LojaAPI.valorCompra("cascalho", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("cascalho", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("cascalho", type));
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add(" ");
            gravelMeta.setLore(lore14);
        } else if (LojaAPI.valorVenda("areia_vermelha", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("cascalho", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("cascalho", type));
            lore14.add(" ");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore14.add(" ");
            gravelMeta.setLore(lore14);
        }
        gravel.setItemMeta(gravelMeta);
        INVENTORY_LOJABLOCOS1.setItem(28, gravel);


        final ItemStack sandred = new ItemStack(Material.RED_SAND);
        final ItemMeta sandredMeta = sandred.getItemMeta();
        sandredMeta.setDisplayName("§eAreia vermelha");
        final ArrayList<String> lore13 = new ArrayList<String>();
        if (LojaAPI.valorVenda("areia_vermelha", type) != 0 && LojaAPI.valorCompra("areia_vermelha", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("areia_vermelha", type)));
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("areia_vermelha", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("areia_vermelha", type));
            lore13.add(" ");
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            sandredMeta.setLore(lore13);
        } else if (LojaAPI.valorCompra("areia_vermelha", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("areia_vermelha", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("areia_vermelha", type));
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add(" ");
            sandredMeta.setLore(lore13);
        } else if (LojaAPI.valorVenda("areia_vermelha", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("areia_vermelha", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("areia_vermelha", type));
            lore13.add(" ");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore13.add(" ");
            sandredMeta.setLore(lore13);
        }
        sandred.setItemMeta(sandredMeta);
        INVENTORY_LOJABLOCOS1.setItem(25, sandred);


        final ItemStack sand = new ItemStack(Material.SAND);
        final ItemMeta sandMeta = sand.getItemMeta();
        sandMeta.setDisplayName("§eAreia");
        final ArrayList<String> lore12 = new ArrayList<String>();
        if (LojaAPI.valorVenda("areia", type) != 0 && LojaAPI.valorCompra("areia", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("areia", type)));
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("areia", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("areia", type));
            lore12.add(" ");
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            sandMeta.setLore(lore12);
        } else if (LojaAPI.valorCompra("areia", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("areia", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("areia", type));
            lore12.add(" ");
            lore12.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore12.add(" ");
            sandMeta.setLore(lore12);
        } else if (LojaAPI.valorVenda("areia", type) != 0) {
            lore12.add(" ");
            lore12.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("areia", type)));
            lore12.add("§7Quantia: §a" + LojaAPI.itemQuantia("areia", type));
            lore12.add(" ");
            lore12.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore12.add(" ");
            sandMeta.setLore(lore12);
        }
        sand.setItemMeta(sandMeta);
        INVENTORY_LOJABLOCOS1.setItem(24, sand);


        final ItemStack podzol = new ItemStack(Material.PODZOL);
        final ItemMeta podzolMeta = podzol.getItemMeta();
        podzolMeta.setDisplayName("§ePodzol");
        final ArrayList<String> lore11 = new ArrayList<String>();
        if (LojaAPI.valorVenda("podzol", type) != 0 && LojaAPI.valorCompra("podzol", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("podzol", type)));
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("podzol", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("podzol", type));
            lore11.add(" ");
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            podzolMeta.setLore(lore11);
        } else if (LojaAPI.valorCompra("podzol", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("podzol", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("podzol", type));
            lore11.add(" ");
            lore11.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore11.add(" ");
            podzolMeta.setLore(lore11);
        } else if (LojaAPI.valorVenda("podzol", type) != 0) {
            lore11.add(" ");
            lore11.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("podzol", type)));
            lore11.add("§7Quantia: §a" + LojaAPI.itemQuantia("podzol", type));
            lore11.add(" ");
            lore11.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore11.add(" ");
            podzolMeta.setLore(lore11);
        }
        podzol.setItemMeta(podzolMeta);
        INVENTORY_LOJABLOCOS1.setItem(23, podzol);


        final ItemStack terrag = new ItemStack(Material.COARSE_DIRT);
        final ItemMeta terragMeta = terrag.getItemMeta();
        terragMeta.setDisplayName("§eTerra grossa");
        final ArrayList<String> lore10 = new ArrayList<String>();
        if (LojaAPI.valorVenda("terra_grossa", type) != 0 && LojaAPI.valorCompra("terra_grossa", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terra_grossa", type)));
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terra_grossa", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("terra_grossa", type));
            lore10.add(" ");
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            terragMeta.setLore(lore10);
        } else if (LojaAPI.valorCompra("terra_grossa", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terra_grossa", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("terra_grossa", type));
            lore10.add(" ");
            lore10.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore10.add(" ");
            terragMeta.setLore(lore10);
        } else if (LojaAPI.valorVenda("terra_grossa", type) != 0) {
            lore10.add(" ");
            lore10.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terra_grossa", type)));
            lore10.add("§7Quantia: §a" + LojaAPI.itemQuantia("terra_grossa", type));
            lore10.add(" ");
            lore10.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore10.add(" ");
            terragMeta.setLore(lore10);
        }
        terrag.setItemMeta(terragMeta);
        INVENTORY_LOJABLOCOS1.setItem(22, terrag);


        final ItemStack terra = new ItemStack(Material.DIRT);
        final ItemMeta terraMeta = terra.getItemMeta();
        terraMeta.setDisplayName("§eTerra");
        final ArrayList<String> lore9 = new ArrayList<String>();
        if (LojaAPI.valorVenda("terra", type) != 0 && LojaAPI.valorCompra("terra", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terra", type)));
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terra", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("terra", type));
            lore9.add(" ");
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            terraMeta.setLore(lore9);
        } else if (LojaAPI.valorCompra("terra", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("terra", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("terra", type));
            lore9.add(" ");
            lore9.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore9.add(" ");
            terraMeta.setLore(lore9);
        } else if (LojaAPI.valorVenda("terra", type) != 0) {
            lore9.add(" ");
            lore9.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("terra", type)));
            lore9.add("§7Quantia: §a" + LojaAPI.itemQuantia("terra", type));
            lore9.add(" ");
            lore9.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore9.add(" ");
            terraMeta.setLore(lore9);
        }
        terra.setItemMeta(terraMeta);
        INVENTORY_LOJABLOCOS1.setItem(21, terra);


        final ItemStack grama = new ItemStack(Material.GRASS_BLOCK);
        final ItemMeta gramaMeta = grama.getItemMeta();
        gramaMeta.setDisplayName("§eGrama");
        final ArrayList<String> lore8 = new ArrayList<String>();
        if (LojaAPI.valorVenda("grama", type) != 0 && LojaAPI.valorCompra("grama", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("grama", type)));
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("grama", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("grama", type));
            lore8.add(" ");
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            gramaMeta.setLore(lore8);
        } else if (LojaAPI.valorCompra("grama", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("grama", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("grama", type));
            lore8.add(" ");
            lore8.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore8.add(" ");
            gramaMeta.setLore(lore8);
        } else if (LojaAPI.valorVenda("grama", type) != 0) {
            lore8.add(" ");
            lore8.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("grama", type)));
            lore8.add("§7Quantia: §a" + LojaAPI.itemQuantia("grama", type));
            lore8.add(" ");
            lore8.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore8.add(" ");
            gramaMeta.setLore(lore8);
        }
        grama.setItemMeta(gramaMeta);
        INVENTORY_LOJABLOCOS1.setItem(20, grama);


        final ItemStack cobblestone = new ItemStack(Material.COBBLESTONE);
        final ItemMeta cobblestoneMeta = cobblestone.getItemMeta();
        cobblestoneMeta.setDisplayName("§ePedregulho");
        final ArrayList<String> lore7 = new ArrayList<String>();
        if (LojaAPI.valorVenda("pedregulho", type) != 0 && LojaAPI.valorCompra("pedregulho", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pedregulho", type)));
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pedregulho", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("pedregulho", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            cobblestoneMeta.setLore(lore7);
        } else if (LojaAPI.valorCompra("pedregulho", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pedregulho", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("pedregulho", type));
            lore7.add(" ");
            lore7.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore7.add(" ");
            cobblestoneMeta.setLore(lore7);
        } else if (LojaAPI.valorVenda("pedregulho", type) != 0) {
            lore7.add(" ");
            lore7.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pedregulho", type)));
            lore7.add("§7Quantia: §a" + LojaAPI.itemQuantia("pedregulho", type));
            lore7.add(" ");
            lore7.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore7.add(" ");
            cobblestoneMeta.setLore(lore7);
        }
        cobblestone.setItemMeta(cobblestoneMeta);
        INVENTORY_LOJABLOCOS1.setItem(19, cobblestone);


        final ItemStack andesitop = new ItemStack(Material.POLISHED_ANDESITE);
        final ItemMeta andesitopMeta = andesitop.getItemMeta();
        andesitopMeta.setDisplayName("§eAndesito polido");
        final ArrayList<String> lore6 = new ArrayList<String>();
        if (LojaAPI.valorVenda("andesito_polido", type) != 0 && LojaAPI.valorCompra("andesito_polido", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("andesito_polido", type)));
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("andesito_polido", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("andesito_polido", type));
            lore6.add(" ");
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            andesitopMeta.setLore(lore6);
        } else if (LojaAPI.valorCompra("andesito_polido", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("andesito_polido", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("andesito_polido", type));
            lore6.add(" ");
            lore6.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore6.add(" ");
            andesitopMeta.setLore(lore6);
        } else if (LojaAPI.valorVenda("andesito_polido", type) != 0) {
            lore6.add(" ");
            lore6.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("andesito_polido", type)));
            lore6.add("§7Quantia: §a" + LojaAPI.itemQuantia("andesito_polido", type));
            lore6.add(" ");
            lore6.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore6.add(" ");
            andesitopMeta.setLore(lore6);
        }
        andesitop.setItemMeta(andesitopMeta);
        INVENTORY_LOJABLOCOS1.setItem(16, andesitop);


        final ItemStack andesito = new ItemStack(Material.ANDESITE);
        final ItemMeta andesitoMeta = andesito.getItemMeta();
        andesitoMeta.setDisplayName("§eAndesito");
        final ArrayList<String> lore5 = new ArrayList<String>();
        if (LojaAPI.valorVenda("andesito", type) != 0 && LojaAPI.valorCompra("andesito", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("andesito", type)));
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("andesito", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("andesito", type));
            lore5.add(" ");
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            andesitoMeta.setLore(lore5);
        } else if (LojaAPI.valorCompra("andesito", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("andesito", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("andesito", type));
            lore5.add(" ");
            lore5.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore5.add(" ");
            andesitoMeta.setLore(lore5);
        } else if (LojaAPI.valorVenda("andesito", type) != 0) {
            lore5.add(" ");
            lore5.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("andesito", type)));
            lore5.add("§7Quantia: §a" + LojaAPI.itemQuantia("andesito", type));
            lore5.add(" ");
            lore5.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore5.add(" ");
            andesitoMeta.setLore(lore5);
        }
        andesito.setItemMeta(andesitoMeta);
        INVENTORY_LOJABLOCOS1.setItem(15, andesito);


        final ItemStack dioritop = new ItemStack(Material.POLISHED_DIORITE);
        final ItemMeta dioritopMeta = dioritop.getItemMeta();
        dioritopMeta.setDisplayName("§eDiorito polido");
        final ArrayList<String> lore4 = new ArrayList<String>();
        if (LojaAPI.valorVenda("diorito_polido", type) != 0 && LojaAPI.valorCompra("diorito_polido", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("diorito_polido", type)));
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("diorito_polido", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("diorito_polido", type));
            lore4.add(" ");
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            dioritopMeta.setLore(lore4);
        } else if (LojaAPI.valorCompra("diorito_polido", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("diorito_polido", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("diorito_polido", type));
            lore4.add(" ");
            lore4.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore4.add(" ");
            dioritopMeta.setLore(lore4);
        } else if (LojaAPI.valorVenda("diorito_polido", type) != 0) {
            lore4.add(" ");
            lore4.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("diorito_polido", type)));
            lore4.add("§7Quantia: §a" + LojaAPI.itemQuantia("diorito_polido", type));
            lore4.add(" ");
            lore4.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore4.add(" ");
            dioritopMeta.setLore(lore4);
        }
        dioritop.setItemMeta(dioritopMeta);
        INVENTORY_LOJABLOCOS1.setItem(14, dioritop);


        final ItemStack diorito = new ItemStack(Material.DIORITE);
        final ItemMeta dioritoMeta = diorito.getItemMeta();
        dioritoMeta.setDisplayName("§eDiorito");
        final ArrayList<String> lore3 = new ArrayList<String>();
        if (LojaAPI.valorVenda("diorito", type) != 0 && LojaAPI.valorCompra("diorito", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("diorito", type)));
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("diorito", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("diorito", type));
            lore3.add(" ");
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            dioritoMeta.setLore(lore3);
        } else if (LojaAPI.valorCompra("diorito", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("diorito", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("diorito", type));
            lore3.add(" ");
            lore3.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore3.add(" ");
            dioritoMeta.setLore(lore3);
        } else if (LojaAPI.valorVenda("diorito", type) != 0) {
            lore3.add(" ");
            lore3.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("diorito", type)));
            lore3.add("§7Quantia: §a" + LojaAPI.itemQuantia("diorito", type));
            lore3.add(" ");
            lore3.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore3.add(" ");
            dioritoMeta.setLore(lore3);
        }
        diorito.setItemMeta(dioritoMeta);
        INVENTORY_LOJABLOCOS1.setItem(13, diorito);


        final ItemStack granitop = new ItemStack(Material.POLISHED_GRANITE);
        final ItemMeta granitopMeta = granitop.getItemMeta();
        granitopMeta.setDisplayName("§eGranito polido");
        final ArrayList<String> lore2 = new ArrayList<String>();
        if (LojaAPI.valorVenda("granito_polido", type) != 0 && LojaAPI.valorCompra("granito_polido", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("granito_polido", type)));
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("granito_polido", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("granito_polido", type));
            lore2.add(" ");
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            granitopMeta.setLore(lore2);
        } else if (LojaAPI.valorCompra("granito_polido", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("granito_polido", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("granito_polido", type));
            lore2.add(" ");
            lore2.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore2.add(" ");
            granitopMeta.setLore(lore2);
        } else if (LojaAPI.valorVenda("granito_polido", type) != 0) {
            lore2.add(" ");
            lore2.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("granito_polido", type)));
            lore2.add("§7Quantia: §a" + LojaAPI.itemQuantia("granito_polido", type));
            lore2.add(" ");
            lore2.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore2.add(" ");
            granitopMeta.setLore(lore2);
        }
        granitop.setItemMeta(granitopMeta);
        INVENTORY_LOJABLOCOS1.setItem(12, granitop);


        final ItemStack granito = new ItemStack(Material.GRANITE);
        final ItemMeta granitoMeta = granito.getItemMeta();
        granitoMeta.setDisplayName("§eGranito");
        final ArrayList<String> lore1 = new ArrayList<String>();
        if (LojaAPI.valorVenda("granito", type) != 0 && LojaAPI.valorCompra("granito", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("granito", type)));
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("granito", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("granito", type));
            lore1.add(" ");
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            granitoMeta.setLore(lore1);
        } else if (LojaAPI.valorCompra("granito", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("granito", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("granito", type));
            lore1.add(" ");
            lore1.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore1.add(" ");
            granitoMeta.setLore(lore1);
        } else if (LojaAPI.valorVenda("granito", type) != 0) {
            lore1.add(" ");
            lore1.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("granito", type)));
            lore1.add("§7Quantia: §a" + LojaAPI.itemQuantia("granito", type));
            lore1.add(" ");
            lore1.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore1.add(" ");
            granitoMeta.setLore(lore1);
        }
        granito.setItemMeta(granitoMeta);
        INVENTORY_LOJABLOCOS1.setItem(11, granito);


        final ItemStack pedra = new ItemStack(Material.STONE);
        final ItemMeta pedraMeta = pedra.getItemMeta();
        pedraMeta.setDisplayName("§ePedra");
        final ArrayList<String> lore = new ArrayList<String>();
        if (LojaAPI.valorVenda("pedra", type) != 0 && LojaAPI.valorCompra("pedra", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pedra", type)));
            lore.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pedra", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("pedra", type));
            lore.add(" ");
            lore.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            pedraMeta.setLore(lore);
        } else if (LojaAPI.valorCompra("pedra", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[COMPRA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorCompra("pedra", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("pedra", type));
            lore.add(" ");
            lore.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore.add(" ");
            pedraMeta.setLore(lore);
        } else if (LojaAPI.valorVenda("pedra", type) != 0) {
            lore.add(" ");
            lore.add("§7Preço §c[VENDA]: §a$" + InventoryLoja.formatter.format(LojaAPI.valorVenda("pedra", type)));
            lore.add("§7Quantia: §a" + LojaAPI.itemQuantia("pedra", type));
            lore.add(" ");
            lore.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore.add(" ");
            pedraMeta.setLore(lore);
        }
        pedra.setItemMeta(pedraMeta);
        INVENTORY_LOJABLOCOS1.setItem(10, pedra);

        return INVENTORY_LOJABLOCOS1;
    }
}
