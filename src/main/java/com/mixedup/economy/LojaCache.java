package com.mixedup.economy;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.mixedup.MySql;
import lombok.val;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

public class LojaCache {

    private LoadingCache<String, LojaItem> cache;

    public LojaCache() {
        this.clearCache();
    }

    public LojaItem getItem(final String item) {
        try {
            return this.cache.get(item);
        } catch (final Exception e) {
            return null;
        }
    }

    public void removeItem(final String item) {
        this.cache.invalidate(item);
    }

    public void clearCache() {
        this.cache = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterAccess(10, TimeUnit.MINUTES)
                .build(new CacheLoader<String, LojaItem>() {
                    public LojaItem load(final String item) {
                        return LojaCache.this.getFromSQL(item);
                    }
                });
    }

    private LojaItem getFromSQL(final String item) {
        try (final val stmt = MySql.con.prepareStatement("SELECT * FROM Loja_data WHERE Item = ?")) {
            stmt.setString(1, item);

            final val rs = stmt.executeQuery();
            if (rs.next())
                return new LojaItem(rs.getInt("ID"), rs.getString("Item"), rs.getInt("Compra"), rs.getInt("Venda"), rs.getInt("Quantia"));
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void updateValorCompra(final String item, final int valorCompra) {
        try (final PreparedStatement st = MySql.con.prepareStatement("UPDATE Loja_data SET Compra = ? WHERE Item = ?")) {
            st.setString(2, item);
            st.setInt(1, valorCompra);
            st.executeUpdate();

            this.getItem(item).setCompra(valorCompra);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateValorVenda(final String item, final int valorVenda) {
        try (final PreparedStatement st = MySql.con.prepareStatement("UPDATE Loja_data SET Venda = ? WHERE Item = ?")) {
            st.setString(2, item);
            st.setInt(1, valorVenda);
            st.executeUpdate();

            this.getItem(item).setVenda(valorVenda);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateItemQuantia(final String item, final int quantia) {
        try (final PreparedStatement st = MySql.con.prepareStatement("UPDATE Loja_data SET Quantia = ? WHERE Item = ?")) {
            st.setString(2, item);
            st.setInt(1, quantia);
            st.executeUpdate();

            this.getItem(item).setQuantia(quantia);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }
}
