package com.mixedup.economy;

import com.mixedup.Main;
import com.mixedup.MySql;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PlayerCreateShopEvent implements Listener {

    public static boolean isNumeric(final String str) {
        try {
            final double d = Double.parseDouble(str);
        } catch (final NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    @EventHandler
    public void onBreak(final BlockBreakEvent event) {
        if (event.getBlock().getType().equals(Material.SIGN) || event.getBlock().getType().equals(Material.WALL_SIGN)) {
            Sign sign = (Sign) event.getBlock().getState();
            if (sign.getLine(0).equalsIgnoreCase(ChatColor.DARK_AQUA + "[Loja]")) {
                LojaAPI.removeLoja(event.getBlock().getWorld().getName() + ":" + event.getBlock().getLocation().getBlockX() + ":" + event.getBlock().getLocation().getBlockY() + ":" + event.getBlock().getLocation().getBlockZ());
            }
        }
    }

    @EventHandler
    public void onCreate(final SignChangeEvent event) {

        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getLine(0).equals("Loja") && isNumeric(event.getLine(1)) == true && event.getLine(2).isEmpty()) {
            if (event.getLine(3).contains("C") || event.getLine(3).contains("V")) {
                if (event.getLine(3).contains("C") && event.getLine(3).contains("V") && event.getLine(3).contains(":")) {

                    final Sign sign = (Sign) event.getBlock().getState();

                    int i = 0;
                    try {
                        final PreparedStatement st = MySql.con.prepareStatement("SELECT COUNT(*) FROM Lojas_data WHERE UUID = ?");
                        st.setString(1, UUID);
                        final ResultSet rs = st.executeQuery();
                        while (rs.next()) {
                            i = rs.getInt("COUNT(*)");
                        }

                        if (i == 10 && TagAPI.getTag(UUID).equals("Membro")) {
                            player.sendMessage(ChatColor.RED + " * Ops, você já contém 10 lojas. Para poder criar mais lojas adquira " + ChatColor.GOLD + "VIP" + ChatColor.RED + " em nosso site e obtenha vantagens como esta.");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            sign.getBlock().breakNaturally();
                            return;
                        } else if (i == 25 && !TagAPI.getTag(UUID).equals("Membro")) {
                            player.sendMessage(ChatColor.RED + " * Ops, você não pode criar mais lojas. Pois atingiu o limite de lojas permito por player.");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            sign.getBlock().breakNaturally();
                            return;
                        }

                    } catch (final SQLException e) {
                        e.printStackTrace();
                    }

                    final Block block = event.getBlock();
                    final org.bukkit.material.Sign sd = (org.bukkit.material.Sign) sign.getData();
                    final Block attachedBlock = block.getRelative(sd.getAttachedFace());
                    if (attachedBlock.getType() != Material.CHEST && attachedBlock.getType() != Material.TRAPPED_CHEST) {
                        player.sendMessage(ChatColor.RED + "Ops, para criar uma loja você deve deixar está placa diretamente conectada a algum baú.");
                        return;
                    }

                    final String[] valores = event.getLine(3).split(":");
                    if (isNumeric(valores[0].substring(2)) == true && isNumeric(valores[1].substring(0, valores[1].length() - 2)) == true) {
                        final int compra = Integer.valueOf(valores[0].substring(2));
                        final int venda = Integer.valueOf(valores[1].substring(0, valores[1].length() - 2));

                        event.setLine(0, ChatColor.DARK_AQUA + "[Loja]");
                        event.setLine(1, "Quantia: " + event.getLine(1));
                        event.setLine(2, ChatColor.RED + "Á definir..");
                        event.setLine(3, "C " + compra + ":" + venda + " V");

                        player.sendMessage(ChatColor.GREEN + " * Loja criada, clique com o botão dinheiro segurando o item desejado para designa lo a esta loja.");

                        Bukkit.getScheduler().scheduleSyncDelayedTask(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                if (sign.getLine(2).equals(ChatColor.RED + "Á definir..")) {
                                    sign.getBlock().breakNaturally();
                                    player.sendMessage(ChatColor.RED + " * Ops, você não definiu nenhum item a loja e ela foi removida.");
                                }
                            }
                        }, 600L);
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, o valor informado aparentemente não é um número.");
                    }
                } else if (!event.getLine(3).contains(":") && event.getLine(3).contains("C") || event.getLine(3).contains("V")) {
                    if (event.getLine(3).contains("V")) {

                        final Sign sign = (Sign) event.getBlock().getState();

                        int i = 0;
                        try {
                            final PreparedStatement st = MySql.con.prepareStatement("SELECT COUNT(*) FROM Lojas_data WHERE UUID = ?");
                            st.setString(1, UUID);
                            final ResultSet rs = st.executeQuery();
                            while (rs.next()) {
                                i = rs.getInt("COUNT(*)");
                            }

                            if (i == 10 && TagAPI.getTag(UUID).equals("Membro")) {
                                player.sendMessage(ChatColor.RED + " * Ops, você já contém 10 lojas. Para poder criar mais lojas adquira " + ChatColor.GOLD + "VIP" + ChatColor.RED + " em nosso site e obtenha vantagens como esta.");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                sign.getBlock().breakNaturally();
                                return;
                            } else if (i == 25 && !TagAPI.getTag(UUID).equals("Membro")) {
                                player.sendMessage(ChatColor.RED + " * Ops, você não pode criar mais lojas. Pois atingiu o limite de lojas permito por player.");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                sign.getBlock().breakNaturally();
                                return;
                            }

                        } catch (final SQLException e) {
                            e.printStackTrace();
                        }

                        final Block block = event.getBlock();
                        final org.bukkit.material.Sign sd = (org.bukkit.material.Sign) sign.getData();
                        final Block attachedBlock = block.getRelative(sd.getAttachedFace());
                        if (attachedBlock.getType() != Material.CHEST && attachedBlock.getType() != Material.TRAPPED_CHEST) {
                            player.sendMessage(ChatColor.RED + "Ops, para criar uma loja você deve deixar está placa diretamente conectada a algum baú.");
                            return;
                        }

                        final String[] valores = event.getLine(3).split(":");
                        if (isNumeric(event.getLine(3).substring(2)) == true) {
                            final int venda = Integer.valueOf(valores[0].substring(2));

                            event.setLine(0, ChatColor.DARK_AQUA + "[Loja]");
                            event.setLine(1, "Quantia: " + event.getLine(1));
                            event.setLine(2, ChatColor.RED + "Á definir..");
                            event.setLine(3, "V " + venda);

                            player.sendMessage(ChatColor.GREEN + " * Loja criada, clique com o botão dinheiro segurando o item desejado para designa lo a esta loja.");

                            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    if (sign.getLine(2).equals(ChatColor.RED + "Á definir..")) {
                                        sign.getBlock().breakNaturally();
                                        player.sendMessage(ChatColor.RED + " * Ops, você não definiu nenhum item a loja e ela foi removida.");
                                    }
                                }
                            }, 600L);
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, o valor informado aparentemente não é um número.");
                        }
                    } else if (event.getLine(3).contains("C")) {

                        final Sign sign = (Sign) event.getBlock().getState();

                        int i = 0;
                        try {
                            final PreparedStatement st = MySql.con.prepareStatement("SELECT COUNT(*) FROM Lojas_data WHERE UUID = ?");
                            st.setString(1, UUID);
                            final ResultSet rs = st.executeQuery();
                            while (rs.next()) {
                                i = rs.getInt("COUNT(*)");
                            }

                            if (i == 10 && TagAPI.getTag(UUID).equals("Membro")) {
                                player.sendMessage(ChatColor.RED + " * Ops, você já contém 10 lojas. Para poder criar mais lojas adquira " + ChatColor.GOLD + "VIP" + ChatColor.RED + " em nosso site e obtenha vantagens como esta.");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                sign.getBlock().breakNaturally();
                                return;
                            } else if (i == 25 && !TagAPI.getTag(UUID).equals("Membro")) {
                                player.sendMessage(ChatColor.RED + " * Ops, você não pode criar mais lojas. Pois atingiu o limite de lojas permito por player.");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                sign.getBlock().breakNaturally();
                                return;
                            }

                        } catch (final SQLException e) {
                            e.printStackTrace();
                        }

                        final Block block = event.getBlock();
                        final org.bukkit.material.Sign sd = (org.bukkit.material.Sign) sign.getData();
                        final Block attachedBlock = block.getRelative(sd.getAttachedFace());
                        if (attachedBlock.getType() != Material.CHEST && attachedBlock.getType() != Material.TRAPPED_CHEST) {
                            player.sendMessage(ChatColor.RED + "Ops, para criar uma loja você deve deixar está placa diretamente conectada a algum baú.");
                            return;
                        }

                        final String[] valores = event.getLine(3).split(":");
                        if (isNumeric(event.getLine(3).substring(2)) == true) {
                            final int compra = Integer.valueOf(valores[0].substring(2));

                            event.setLine(0, ChatColor.DARK_AQUA + "[Loja]");
                            event.setLine(1, "Quantia: " + event.getLine(1));
                            event.setLine(2, ChatColor.RED + "Á definir..");
                            event.setLine(3, "C " + compra);

                            player.sendMessage(ChatColor.GREEN + " * Loja criada, clique com o botão dinheiro segurando o item desejado para designa lo a esta loja.");

                            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    if (sign.getLine(2).equals(ChatColor.RED + "Á definir..")) {
                                        sign.getBlock().breakNaturally();
                                        player.sendMessage(ChatColor.RED + " * Ops, você não definiu nenhum item a loja e ela foi removida.");
                                    }
                                }
                            }, 600L);
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, o valor informado aparentemente não é um número.");
                        }
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, não conseguimos criar está loja, pois algum argumento está errado.");
                }
            }
        }
    }

    @EventHandler
    public void onClick(final PlayerInteractEvent event) {

        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && event.getClickedBlock().getType() != null) {
            if (event.getClickedBlock().getType().equals(Material.SIGN) || event.getClickedBlock().getType().equals(Material.WALL_SIGN)) {
                final Sign sign = (Sign) event.getClickedBlock().getState();
                event.setCancelled(true);

                if (sign.getLine(0).equals(ChatColor.DARK_AQUA + "[Loja]")) {
                    if (!event.getPlayer().getItemInHand().getType().equals(Material.AIR)) {
                        if (sign.getLine(2).equals(ChatColor.RED + "Á definir..")) {

                            int i = 0;
                            try {
                                final PreparedStatement st = MySql.con.prepareStatement("SELECT COUNT(*) FROM Lojas_data WHERE UUID = ?");
                                st.setString(1, UUID);
                                final ResultSet rs = st.executeQuery();
                                while (rs.next()) {
                                    i = rs.getInt("COUNT(*)");
                                }

                                if (i == 10 && TagAPI.getTag(UUID).equals("Membro")) {
                                    player.sendMessage(ChatColor.RED + " * Ops, você já contém 10 lojas. Para poder criar mais lojas adquira " + ChatColor.GOLD + "VIP" + ChatColor.RED + " em nosso site e obtenha vantagens como esta.");
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                    sign.getBlock().breakNaturally();
                                    return;
                                } else if (i == 25 && !TagAPI.getTag(UUID).equals("Membro")) {
                                    player.sendMessage(ChatColor.RED + " * Ops, você não pode criar mais lojas. Pois atingiu o limite de lojas permito por player.");
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                    sign.getBlock().breakNaturally();
                                    return;
                                }

                            } catch (final SQLException e) {
                                e.printStackTrace();
                            }

                            final String loc = event.getClickedBlock().getWorld().getName() + ":" + event.getClickedBlock().getX() + ":" + event.getClickedBlock().getY() + ":" + event.getClickedBlock().getZ();
                            final String item = String.valueOf(event.getPlayer().getItemInHand().getType().name());
                            LojaAPI.setLoja(UUID, loc, item);
                            sign.setLine(2, event.getPlayer().getItemInHand().getType().name());
                            sign.update();

                            player.sendMessage(ChatColor.GREEN + " * Loja criada com sucesso.");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    }
                }
            }
        }
    }
}