package com.mixedup.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CommandAjuda implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, final String[] args) {
        final String msg = " \n §a/ir §8- §7Veja todos destinos disponíveis no servidor.\n " +
                "§a/kit §8- §7Veja todos kits disponíveis no servidor.\n " +
                "§a/esquadrao ajuda §8- §7Ver comandos referentes à esquadrões.\n " +
                "§a/moeda ajuda §8- §7Ver comandos referentes à moeda.\n " +
                "§a/home ajuda §8- §7Ver comandos referentes à home.\n " +
                "§a/terreno §8 §7Sistema de casa para a guilda Nobre e Sanguinária.\n " +
                "§a/ilha §8 §7Sistema de casa para a guilda Anciã.\n ";
        sender.sendMessage(msg);
        return false;
    }
}
