package com.mixedup.commands;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import com.mixedup.utils.AlternateColor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandGamemode implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        // -/gm (nick) (gamemode)
        if (cmd.getName().equalsIgnoreCase("gamemode") || cmd.getName().equalsIgnoreCase("gm")) {
            if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor") || TagAPI.getTag(UUID).equals("Admin")) {
                if (args.length > 2 || args.length < 1) {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/gamemode (0, 1, 2, 3) ou /gm (0, 1, 2 ,3)");
                    return false;
                }
                if (args.length == 2) {
                    final Player target = Bukkit.getPlayerExact(args[0]);

                    if (target != null && target.isOnline()) {
                        if (args[1].equals("") || args[1].equals("1") || args[1].equals("2") || args[1].equals("3") || args[1].equals("survival") || args[1].equals("criativo") || args[1].equals("adventure") || args[1].equals("espectador")) {
                            if (args[1].equals("0")) {
                                target.setGameMode(GameMode.SURVIVAL);
                                target.sendMessage(ChatColor.GREEN + " * Gamemode setado para " + AlternateColor.alternate("&a&nSURVIVAL") + ChatColor.GREEN + ", por " + player.getName() + ".");
                                player.sendMessage(ChatColor.GREEN + " * Gamemode de " + args[0] + " setado para " + AlternateColor.alternate("&a&nSURVIVAL") + ChatColor.GREEN + ".");
                            }
                            if (args[1].equals("1")) {
                                target.setGameMode(GameMode.CREATIVE);
                                target.sendMessage(ChatColor.GREEN + " * Gamemode setado para " + AlternateColor.alternate("&a&nCRIATIVO") + ChatColor.GREEN + ", por " + player.getName() + ".");
                                player.sendMessage(ChatColor.GREEN + " * Gamemode de " + args[0] + " setado para " + AlternateColor.alternate("&a&nCRIATIVO") + ChatColor.GREEN + ".");
                            }
                            if (args[1].equals("2")) {
                                target.setGameMode(GameMode.ADVENTURE);
                                target.sendMessage(ChatColor.GREEN + " * Gamemode setado para " + AlternateColor.alternate("&a&nADVENTURE") + ChatColor.GREEN + ", por " + player.getName() + ".");
                                player.sendMessage(ChatColor.GREEN + " * Gamemode de " + args[0] + " setado para " + AlternateColor.alternate("&a&nADVENTURE") + ChatColor.GREEN + ".");
                            }
                            if (args[1].equals("3")) {
                                target.setGameMode(GameMode.SPECTATOR);
                                target.sendMessage(ChatColor.GREEN + " * Gamemode setado para " + AlternateColor.alternate("&a&nESPECTADOR") + ChatColor.GREEN + ", por " + player.getName() + ".");
                                player.sendMessage(ChatColor.GREEN + " * Gamemode de " + args[0] + " setado para " + AlternateColor.alternate("&a&nESPECTADOR") + ChatColor.GREEN + ".");
                            }
                            if (args[1].equals("survival")) {
                                target.setGameMode(GameMode.SURVIVAL);
                                target.sendMessage(ChatColor.GREEN + " * Gamemode setado para " + AlternateColor.alternate("&a&nSURVIVAL") + ChatColor.GREEN + ", por " + player.getName() + ".");
                                player.sendMessage(ChatColor.GREEN + " * Gamemode de " + args[0] + " setado para " + AlternateColor.alternate("&a&nSURVIVAL") + ChatColor.GREEN + ".");
                            }
                            if (args[1].equals("criativo")) {
                                target.setGameMode(GameMode.CREATIVE);
                                target.sendMessage(ChatColor.GREEN + " * Gamemode setado para " + AlternateColor.alternate("&a&nCRIATIVO") + ChatColor.GREEN + ", por " + player.getName() + ".");
                                player.sendMessage(ChatColor.GREEN + " * Gamemode de " + args[0] + " setado para " + AlternateColor.alternate("&a&nCRIATIVO") + ChatColor.GREEN + ".");
                            }
                            if (args[1].equals("adventure")) {
                                target.setGameMode(GameMode.ADVENTURE);
                                target.sendMessage(ChatColor.GREEN + " * Gamemode setado para " + AlternateColor.alternate("&a&nADVENTURE") + ChatColor.GREEN + ", por " + player.getName() + ".");
                                player.sendMessage(ChatColor.GREEN + " * Gamemode de " + args[0] + " setado para " + AlternateColor.alternate("&a&nADVENTURE") + ChatColor.GREEN + ".");
                            }
                            if (args[1].equals("espectador")) {
                                target.setGameMode(GameMode.SPECTATOR);
                                target.sendMessage(ChatColor.GREEN + " * Gamemode setado para " + AlternateColor.alternate("&a&nESPECTADOR") + ChatColor.GREEN + ", por " + player.getName() + ".");
                                player.sendMessage(ChatColor.GREEN + " * Gamemode de " + args[0] + " setado para " + AlternateColor.alternate("&a&nESPECTADOR") + ChatColor.GREEN + ".");
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, veja qual argumento foi utilizado para definir o gamemode e tente novamente.");
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, este player não existe ou não está online no momento.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                }
                if (args.length == 1) {
                    if (args[0].equals("0") || args[0].equals("1") || args[0].equals("2") || args[0].equals("3") || args[0].equals("survival") || args[0].equals("criativo") || args[0].equals("adventure") || args[0].equals("espectador")) {
                        if (args[0].equals("0")) {
                            player.setGameMode(GameMode.SURVIVAL);
                            player.sendMessage(ChatColor.GREEN + " * Gamemode setado para " + AlternateColor.alternate("&a&nSURVIVAL") + ChatColor.GREEN + ".");
                        }
                        if (args[0].equals("1")) {
                            player.setGameMode(GameMode.CREATIVE);
                            player.sendMessage(ChatColor.GREEN + " * Gamemode setado para " + AlternateColor.alternate("&a&nCRIATIVO") + ChatColor.GREEN + ".");
                        }
                        if (args[0].equals("2")) {
                            player.setGameMode(GameMode.ADVENTURE);
                            player.sendMessage(ChatColor.GREEN + " * Gamemode setado para " + AlternateColor.alternate("&a&nADVENTURE") + ChatColor.GREEN + ".");
                        }
                        if (args[0].equals("3")) {
                            player.setGameMode(GameMode.SPECTATOR);
                            player.sendMessage(ChatColor.GREEN + " * Gamemode setado para " + AlternateColor.alternate("&a&nESPECTADOR") + ChatColor.GREEN + ".");
                        }
                        if (args[0].equals("survival")) {
                            player.setGameMode(GameMode.SURVIVAL);
                            player.sendMessage(ChatColor.GREEN + " * Gamemode setado para " + AlternateColor.alternate("&a&nSURVIVAL") + ChatColor.GREEN + ".");
                        }
                        if (args[0].equals("criativo")) {
                            player.setGameMode(GameMode.CREATIVE);
                            player.sendMessage(ChatColor.GREEN + " * Gamemode setado para " + AlternateColor.alternate("&a&nCRIATIVO") + ChatColor.GREEN + ".");
                        }
                        if (args[0].equals("adventure")) {
                            player.setGameMode(GameMode.ADVENTURE);
                            player.sendMessage(ChatColor.GREEN + " * Gamemode setado para " + AlternateColor.alternate("&a&nADVENTURE") + ChatColor.GREEN + ".");
                        }
                        if (args[0].equals("espectador")) {
                            player.setGameMode(GameMode.SPECTATOR);
                            player.sendMessage(ChatColor.GREEN + " * Gamemode setado para " + AlternateColor.alternate("&a&nESPECTADOR") + ChatColor.GREEN + ".");
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, veja qual argumento foi utilizado para definir o gamemode e tente novamente.");
                    }
                }
            }
        }

        return false;
    }
}
