package com.mixedup.commands;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import com.mixedup.utils.LuzUtil;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Arrays;

public class CommandLuz implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("luz")) {
            final String tag = TagAPI.getTag(UUID);
            final String[] perm = {"Vip", "Vip+", "Vip++", "Vip+++", "Master", "Supervisor", "Admin", "Moderador", "Suporte", "Youtuber"};

            if (Arrays.asList(perm).contains(tag)) {
                if (LuzUtil.get(UUID) == null) {
                    new LuzUtil(UUID, true).insert();

                    player.sendMessage(ChatColor.GREEN + " * Luz ativada com sucesso!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    final PotionEffect effect = new PotionEffect(PotionEffectType.NIGHT_VISION, 999999, 1, true, false);
                    player.addPotionEffect(effect);
                } else if (LuzUtil.get(UUID).getstatus() == false) {
                    LuzUtil.get(UUID).setstatus(true);

                    player.sendMessage(ChatColor.GREEN + " * Luz ativada com sucesso!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    final PotionEffect effect = new PotionEffect(PotionEffectType.NIGHT_VISION, 999999, 1, true, false);
                    player.addPotionEffect(effect);
                } else {
                    LuzUtil.get(UUID).setstatus(false);

                    player.sendMessage(ChatColor.RED + " * Luz desativada com sucesso!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.removePotionEffect(PotionEffectType.NIGHT_VISION);
                }
            } else {
                player.sendMessage(ChatColor.GRAY + " * Adquira vantagens como estas adquirindo " + ChatColor.GREEN + "VIP" + ChatColor.GRAY + " em nosso site." + "\n" + ChatColor.YELLOW + " * www.finalelite.com.br");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
        return false;
    }
}
