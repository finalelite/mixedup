package com.mixedup.commands;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandTppos implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("tppos")) {
            if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor") || TagAPI.getTag(UUID).equals("Admin")) {
                if (args.length == 3) {
                    try {
                        final int x = Integer.valueOf(args[0]);
                        final int y = Integer.valueOf(args[1]);
                        final int z = Integer.valueOf(args[2]);
                        final Location location = new Location(player.getWorld(), x, y, z);

                        player.teleport(location);
                        player.sendMessage(ChatColor.YELLOW + " * Teletransportando para X" + args[0] + " Y" + args[1] + " Z" + args[2]);
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } catch (final Exception e) {
                        player.sendMessage(ChatColor.RED + "Ops, nos informe coordenadas existentes para efetuar este comando!");
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/tppos (X) (Y) (Z)");
                }
            }
        }
        return false;
    }
}
