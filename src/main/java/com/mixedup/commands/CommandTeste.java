package com.mixedup.commands;

import com.mixedup.Main;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.elytra.utils.RemoveArmorStands;
import com.mixedup.managers.InventoryBanqueiro;
import com.mixedup.managers.InventoryElytra;
import com.mixedup.utils.AlternateColor;
import com.mixedup.utils.Hologram;
import com.mixedup.utils.WorldEditUtil;
import com.sk89q.worldedit.WorldEditException;
import net.minecraft.server.v1_13_R2.*;
import org.bukkit.Material;
import org.bukkit.*;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Merchant;
import org.bukkit.inventory.MerchantRecipe;
import org.bukkit.inventory.meta.FireworkEffectMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CommandTeste implements CommandExecutor {

    public static ItemStack addGlow(final ItemStack item) {

        item.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
        final ItemMeta meta = item.getItemMeta();
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        item.setItemMeta(meta);

        return item;
    }

    public static void giveAsas(final Player player) {
        //SANGUINARIA - ANCIA - NOBRE

        for (int i = 1; i <= 3; i++) {
            ItemStack elytra = new ItemStack(Material.ELYTRA);
            final ItemMeta metaElytra = elytra.getItemMeta();
            metaElytra.setDisplayName("Asa Sanguinaria [Nivel " + i + "]");
            metaElytra.addEnchant(Enchantment.PROTECTION_FIRE, 1 * i, true);
            metaElytra.addEnchant(Enchantment.PROTECTION_FALL, 1 * i, true);
            metaElytra.addEnchant(Enchantment.DURABILITY, 1 * i, true);
            metaElytra.addEnchant(Enchantment.THORNS, 1 * i, true);
            metaElytra.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1 * i, true);
            elytra.setItemMeta(metaElytra);

            final net.minecraft.server.v1_13_R2.ItemStack nmsStack = CraftItemStack.asNMSCopy(elytra);
            final NBTTagCompound compound = (nmsStack.hasTag()) ? nmsStack.getTag() : new NBTTagCompound();
            final NBTTagList modifiers = new NBTTagList();
            final NBTTagCompound speed = new NBTTagCompound();
            final NBTTagCompound maxHealth = new NBTTagCompound();
            final NBTTagCompound attackDamage = new NBTTagCompound();
            final NBTTagCompound armor = new NBTTagCompound();

            speed.set("AttributeName", new NBTTagString("generic.movementSpeed"));
            speed.set("Name", new NBTTagString("generic.movementSpeed"));
            speed.set("Amount", new NBTTagDouble(0.1 * i));
            speed.set("Operation", new NBTTagInt(1));
            speed.set("UUIDLeast", new NBTTagInt(894654));
            speed.set("UUIDMost", new NBTTagInt(2872));
            speed.set("Slot", new NBTTagString("chest"));

            maxHealth.set("AttributeName", new NBTTagString("generic.maxHealth"));
            maxHealth.set("Name", new NBTTagString("generic.maxHealth"));
            maxHealth.set("Amount", new NBTTagDouble(0.1 * i));
            maxHealth.set("Operation", new NBTTagInt(1));
            maxHealth.set("UUIDLeast", new NBTTagInt(894654));
            maxHealth.set("UUIDMost", new NBTTagInt(2872));
            maxHealth.set("Slot", new NBTTagString("chest"));

            attackDamage.set("AttributeName", new NBTTagString("generic.attackDamage"));
            attackDamage.set("Name", new NBTTagString("generic.attackDamage"));
            attackDamage.set("Amount", new NBTTagDouble(0.1 * i));
            attackDamage.set("Operation", new NBTTagInt(1));
            attackDamage.set("UUIDLeast", new NBTTagInt(894654));
            attackDamage.set("UUIDMost", new NBTTagInt(2872));
            attackDamage.set("Slot", new NBTTagString("chest"));

            armor.set("AttributeName", new NBTTagString("generic.armor"));
            armor.set("Name", new NBTTagString("generic.armor"));
            armor.set("Amount", new NBTTagDouble(0.1 * i));
            armor.set("Operation", new NBTTagInt(1));
            armor.set("UUIDLeast", new NBTTagInt(894654));
            armor.set("UUIDMost", new NBTTagInt(2872));
            armor.set("Slot", new NBTTagString("chest"));

            modifiers.add(speed);
            modifiers.add(maxHealth);
            modifiers.add(attackDamage);
            modifiers.add(armor);
            compound.set("AttributeModifiers", modifiers);
            nmsStack.setTag(compound);
            elytra = CraftItemStack.asBukkitCopy(nmsStack);
            player.getInventory().addItem(elytra);
        }

        for (int i = 1; i <= 3; i++) {
            ItemStack elytra = new ItemStack(Material.ELYTRA);
            final ItemMeta metaElytra = elytra.getItemMeta();
            metaElytra.setDisplayName("Asa Ancia [Nivel " + i + "]");
            metaElytra.addEnchant(Enchantment.PROTECTION_PROJECTILE, 1 * i, true);
            metaElytra.addEnchant(Enchantment.PROTECTION_FALL, 1 * i, true);
            metaElytra.addEnchant(Enchantment.DURABILITY, 1 * i, true);
            metaElytra.addEnchant(Enchantment.THORNS, 1 * i, true);
            metaElytra.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1 * i, true);
            elytra.setItemMeta(metaElytra);

            final net.minecraft.server.v1_13_R2.ItemStack nmsStack = CraftItemStack.asNMSCopy(elytra);
            final NBTTagCompound compound = (nmsStack.hasTag()) ? nmsStack.getTag() : new NBTTagCompound();
            final NBTTagList modifiers = new NBTTagList();
            final NBTTagCompound speed = new NBTTagCompound();
            final NBTTagCompound maxHealth = new NBTTagCompound();
            final NBTTagCompound attackDamage = new NBTTagCompound();
            final NBTTagCompound armor = new NBTTagCompound();

            speed.set("AttributeName", new NBTTagString("generic.movementSpeed"));
            speed.set("Name", new NBTTagString("generic.movementSpeed"));
            speed.set("Amount", new NBTTagDouble(0.1 * i));
            speed.set("Operation", new NBTTagInt(1));
            speed.set("UUIDLeast", new NBTTagInt(894654));
            speed.set("UUIDMost", new NBTTagInt(2872));
            speed.set("Slot", new NBTTagString("chest"));

            maxHealth.set("AttributeName", new NBTTagString("generic.maxHealth"));
            maxHealth.set("Name", new NBTTagString("generic.maxHealth"));
            maxHealth.set("Amount", new NBTTagDouble(0.1 * i));
            maxHealth.set("Operation", new NBTTagInt(1));
            maxHealth.set("UUIDLeast", new NBTTagInt(894654));
            maxHealth.set("UUIDMost", new NBTTagInt(2872));
            maxHealth.set("Slot", new NBTTagString("chest"));

            attackDamage.set("AttributeName", new NBTTagString("generic.attackDamage"));
            attackDamage.set("Name", new NBTTagString("generic.attackDamage"));
            attackDamage.set("Amount", new NBTTagDouble(0.1 * i));
            attackDamage.set("Operation", new NBTTagInt(1));
            attackDamage.set("UUIDLeast", new NBTTagInt(894654));
            attackDamage.set("UUIDMost", new NBTTagInt(2872));
            attackDamage.set("Slot", new NBTTagString("chest"));

            armor.set("AttributeName", new NBTTagString("generic.armor"));
            armor.set("Name", new NBTTagString("generic.armor"));
            armor.set("Amount", new NBTTagDouble(0.1 * i));
            armor.set("Operation", new NBTTagInt(1));
            armor.set("UUIDLeast", new NBTTagInt(894654));
            armor.set("UUIDMost", new NBTTagInt(2872));
            armor.set("Slot", new NBTTagString("chest"));

            modifiers.add(speed);
            modifiers.add(maxHealth);
            modifiers.add(attackDamage);
            modifiers.add(armor);
            compound.set("AttributeModifiers", modifiers);
            nmsStack.setTag(compound);
            elytra = CraftItemStack.asBukkitCopy(nmsStack);
            player.getInventory().addItem(elytra);
        }

        for (int i = 1; i <= 3; i++) {
            ItemStack elytra = new ItemStack(Material.ELYTRA);
            final ItemMeta metaElytra = elytra.getItemMeta();
            metaElytra.setDisplayName("Asa Nobre [Nivel " + i + "]");
            metaElytra.addEnchant(Enchantment.PROTECTION_EXPLOSIONS, 1 * i, true);
            metaElytra.addEnchant(Enchantment.PROTECTION_FALL, 1 * i, true);
            metaElytra.addEnchant(Enchantment.DURABILITY, 1 * i, true);
            metaElytra.addEnchant(Enchantment.THORNS, 1 * i, true);
            metaElytra.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1 * i, true);
            elytra.setItemMeta(metaElytra);

            final net.minecraft.server.v1_13_R2.ItemStack nmsStack = CraftItemStack.asNMSCopy(elytra);
            final NBTTagCompound compound = (nmsStack.hasTag()) ? nmsStack.getTag() : new NBTTagCompound();
            final NBTTagList modifiers = new NBTTagList();
            final NBTTagCompound speed = new NBTTagCompound();
            final NBTTagCompound maxHealth = new NBTTagCompound();
            final NBTTagCompound attackDamage = new NBTTagCompound();
            final NBTTagCompound armor = new NBTTagCompound();

            speed.set("AttributeName", new NBTTagString("generic.movementSpeed"));
            speed.set("Name", new NBTTagString("generic.movementSpeed"));
            speed.set("Amount", new NBTTagDouble(0.1 * i));
            speed.set("Operation", new NBTTagInt(1));
            speed.set("UUIDLeast", new NBTTagInt(894654));
            speed.set("UUIDMost", new NBTTagInt(2872));
            speed.set("Slot", new NBTTagString("chest"));

            maxHealth.set("AttributeName", new NBTTagString("generic.maxHealth"));
            maxHealth.set("Name", new NBTTagString("generic.maxHealth"));
            maxHealth.set("Amount", new NBTTagDouble(0.1 * i));
            maxHealth.set("Operation", new NBTTagInt(1));
            maxHealth.set("UUIDLeast", new NBTTagInt(894654));
            maxHealth.set("UUIDMost", new NBTTagInt(2872));
            maxHealth.set("Slot", new NBTTagString("chest"));

            attackDamage.set("AttributeName", new NBTTagString("generic.attackDamage"));
            attackDamage.set("Name", new NBTTagString("generic.attackDamage"));
            attackDamage.set("Amount", new NBTTagDouble(0.1 * i));
            attackDamage.set("Operation", new NBTTagInt(1));
            attackDamage.set("UUIDLeast", new NBTTagInt(894654));
            attackDamage.set("UUIDMost", new NBTTagInt(2872));
            attackDamage.set("Slot", new NBTTagString("chest"));

            armor.set("AttributeName", new NBTTagString("generic.armor"));
            armor.set("Name", new NBTTagString("generic.armor"));
            armor.set("Amount", new NBTTagDouble(0.1 * i));
            armor.set("Operation", new NBTTagInt(1));
            armor.set("UUIDLeast", new NBTTagInt(894654));
            armor.set("UUIDMost", new NBTTagInt(2872));
            armor.set("Slot", new NBTTagString("chest"));

            modifiers.add(speed);
            modifiers.add(maxHealth);
            modifiers.add(attackDamage);
            modifiers.add(armor);
            compound.set("AttributeModifiers", modifiers);
            nmsStack.setTag(compound);
            elytra = CraftItemStack.asBukkitCopy(nmsStack);
            player.getInventory().addItem(elytra);
        }
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("teste")) {
            if (!player.isOp()) return false;

            if (args[0].equalsIgnoreCase("boss")) {
                if (args.length == 2) {
                    final BossBar b = Bukkit.createBossBar(args[1], BarColor.GREEN, BarStyle.SEGMENTED_20);
                    b.addPlayer(player);
                    b.setVisible(true);
                    b.setProgress(1);

                    Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            b.setProgress(b.getProgress() - 0.05);
                        }
                    }, 0L, 100L);
                }
            }
            if (args[0].equalsIgnoreCase("removerarmor")) {
                for (final Entity entity : player.getLocation().getNearbyEntities(5, 5, 5)) {
                    if (entity.getType().equals(EntityType.ARMOR_STAND)) {
                        entity.remove();
                    }
                }
            }
            if (args[0].equalsIgnoreCase("20")) {
                final ItemStack papel = new ItemStack(Material.PAPER);
                final ItemMeta meta = papel.getItemMeta();
                meta.setDisplayName(ChatColor.YELLOW + "Papiro com escritos");
                final List<String> lore = new ArrayList<>();
                lore.add(" ");
                lore.add(ChatColor.YELLOW + "Escritos: " + ChatColor.DARK_GRAY + "Raro");
                lore.add(" ");
                meta.setLore(lore);
                papel.setItemMeta(meta);
                player.getInventory().addItem(papel);

                final ItemStack papel1 = new ItemStack(Material.PAPER);
                final ItemMeta meta1 = papel1.getItemMeta();
                meta1.setDisplayName(ChatColor.YELLOW + "Papiro com escritos");
                final List<String> lore1 = new ArrayList<>();
                lore1.add(" ");
                lore1.add(ChatColor.YELLOW + "Escritos: " + ChatColor.DARK_GRAY + "Ultra raro");
                lore1.add(" ");
                meta1.setLore(lore1);
                papel1.setItemMeta(meta1);
                player.getInventory().addItem(papel1);

                final ItemStack papel2 = new ItemStack(Material.PAPER);
                final ItemMeta meta2 = papel2.getItemMeta();
                meta2.setDisplayName(ChatColor.YELLOW + "Papiro com escritos");
                final List<String> lore2 = new ArrayList<>();
                lore2.add(" ");
                lore2.add(ChatColor.YELLOW + "Escritos: " + ChatColor.DARK_GRAY + "Mega raro");
                lore2.add(" ");
                meta2.setLore(lore2);
                papel2.setItemMeta(meta2);
                player.getInventory().addItem(papel2);
            }
            if (args[0].equalsIgnoreCase("19")) {
                try {
                    WorldEditUtil.paste(WorldEditUtil.load(Main.plugin.getDataFolder() + File.separator + "WorldEdit/schematics/ilha"), true, player.getLocation());
                } catch (final WorldEditException e) {
                    e.printStackTrace();
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }
            if (args[0].equalsIgnoreCase("18")) {
                com.mixedup.npc.NPC.destroyUpd(Integer.valueOf(args[1]), player);
            }
            if (args[0].equalsIgnoreCase("17")) {
                CommandTeste.giveAsas(player);
            }
            if (args[0].equalsIgnoreCase("16")) {
                final ItemStack moeda = new ItemStack(Material.CARROT_ON_A_STICK);
                final ItemMeta meta = moeda.getItemMeta();
                meta.setDisplayName(ChatColor.YELLOW + "Saco de moedas");
                final ArrayList<String> lore = new ArrayList<>();
                lore.add(" ");
                final Random random = new Random();
                lore.add(ChatColor.DARK_GRAY + "Moeda(s): " + ChatColor.GRAY + args[1]);
                lore.add(" ");
                meta.setLore(lore);
                moeda.setItemMeta(meta);
                player.getInventory().addItem(moeda);
            }
            if (args[0].equalsIgnoreCase("15")) {
                player.openInventory(InventoryBanqueiro.getInventory(UUID));
            }
            if (args[0].equalsIgnoreCase("14")) {
                player.openInventory(InventoryElytra.firstInv());
            }
            if (args[0].equalsIgnoreCase("13")) {
                final ItemStack elytra = new ItemStack(Material.ELYTRA);
                final ItemMeta meta = elytra.getItemMeta();
                meta.setDisplayName(AlternateColor.alternate("§o§8Asa Nobre §l§d[Nivel 3]"));
                elytra.setItemMeta(meta);

                player.getInventory().addItem(elytra);
            }
            if (args[0].equalsIgnoreCase("12")) {
                final StringBuilder build = new StringBuilder();
                for (int i = 1; i < args.length; i++) {
                    build.append(" ");
                    build.append(args[i]);
                }
                final String msg = build.toString();

                final ItemStack elytra = new ItemStack(Material.ELYTRA);
                final ItemMeta meta = elytra.getItemMeta();
                meta.setDisplayName(AlternateColor.alternate(msg.substring(1)));
                elytra.setItemMeta(meta);

                player.getInventory().addItem(elytra);
            }
            if (args[0].equalsIgnoreCase("11")) {
                final Entity entity = player.getLocation().getWorld().spawnEntity(player.getLocation().add(0, 3, 0), EntityType.GHAST);

                final String[] lifeText = {ChatColor.YELLOW + String.valueOf(30)};
                final ArmorStand lifeGhast = Hologram.createHologramReturnStand(entity.getLocation(), lifeText);
                lifeGhast.setCustomNameVisible(true);
                entity.setPassenger(lifeGhast);
            }
            if (args[0].equalsIgnoreCase("10")) {
                RemoveArmorStands.removeAllArmorStand();
                player.sendMessage("removido");
            }
            if (args[0].equalsIgnoreCase("9")) {
                final String[] texts = {ChatColor.GREEN + args[0], ChatColor.GRAY + args[1]};
                final ArmorStand holograma = Hologram.createHologramReturnStand(player.getLocation(), texts);
            }
            if (args[0].equalsIgnoreCase("8")) {
                player.getLocation().getWorld().spawnEntity(player.getLocation().add(0, 3, 0), EntityType.GHAST);
            }
            if (args[0].equalsIgnoreCase("7")) {
                final Entity entity = player.getLocation().getWorld().spawnEntity(player.getLocation().add(0, 3, 0), EntityType.GHAST);
                entity.setCustomName("Nível-1");
                entity.setCustomNameVisible(false);
            }
            if (args[0].equals("6")) {
                final ItemStack tntRepulsao = new ItemStack(Material.TNT, 48);
                final ItemMeta meta3 = tntRepulsao.getItemMeta();
                meta3.setDisplayName(ChatColor.RED + "Explosivo de repulsão");
                tntRepulsao.setItemMeta(meta3);
                player.getInventory().addItem(CommandTeste.addGlow(tntRepulsao));

                final ItemStack tntH = new ItemStack(Material.TNT, 48);
                final ItemMeta meta2 = tntH.getItemMeta();
                meta2.setDisplayName(ChatColor.RED + "Explosivo de hidrogênio");
                final List<String> lore2 = new ArrayList<>();
                lore2.add(" ");
                lore2.add(ChatColor.GRAY + "Dano: 5/5");
                lore2.add(" ");
                meta2.setLore(lore2);
                tntH.setItemMeta(meta2);
                player.getInventory().addItem(CommandTeste.addGlow(tntH));

                final ItemStack tntMedia = new ItemStack(Material.TNT, 48);
                final ItemMeta meta1 = tntMedia.getItemMeta();
                meta1.setDisplayName(ChatColor.RED + "Explosivo nuclear");
                final List<String> lore1 = new ArrayList<>();
                lore1.add(" ");
                lore1.add(ChatColor.GRAY + "Dano: 3/5");
                lore1.add(" ");
                meta1.setLore(lore1);
                tntMedia.setItemMeta(meta1);
                player.getInventory().addItem(tntMedia);

                final ItemStack tntComum = new ItemStack(Material.TNT, 48);
                final ItemMeta meta5 = tntComum.getItemMeta();
                meta5.setDisplayName(ChatColor.RED + "Explosivo comum");
                final List<String> lore4 = new ArrayList<>();
                lore4.add(" ");
                lore4.add(ChatColor.GRAY + "Dano: 1/5");
                lore4.add(" ");
                meta5.setLore(lore4);
                tntComum.setItemMeta(meta5);
                player.getInventory().addItem(tntComum);
            }
            if (args[0].equals("5")) {
                final ItemStack playerDrop4 = new ItemStack(Material.FIREWORK_STAR, 1);
                final ItemMeta meta4 = playerDrop4.getItemMeta();
                meta4.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
                meta4.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                meta4.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                meta4.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                meta4.addItemFlags(ItemFlag.HIDE_DESTROYS);
                meta4.addItemFlags(ItemFlag.HIDE_PLACED_ON);
                meta4.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                final ArrayList<String> lore4 = new ArrayList<>();
                lore4.add(" ");
                lore4.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Velocidade");
                lore4.add(" ");
                meta4.setLore(lore4);
                final FireworkEffectMeta metaFw4 = (FireworkEffectMeta) meta4;
                final FireworkEffect aa4 = FireworkEffect.builder().withColor(Color.BLUE).build();
                metaFw4.setEffect(aa4);
                playerDrop4.setItemMeta(metaFw4);

                final ItemStack playerDrop3 = new ItemStack(Material.FIREWORK_STAR, 1);
                final ItemMeta meta3 = playerDrop3.getItemMeta();
                meta3.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                meta3.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                meta3.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                meta3.addItemFlags(ItemFlag.HIDE_DESTROYS);
                meta3.addItemFlags(ItemFlag.HIDE_PLACED_ON);
                meta3.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                meta3.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
                final ArrayList<String> lore3 = new ArrayList<>();
                lore3.add(" ");
                lore3.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Super pulo");
                lore3.add(" ");
                meta3.setLore(lore3);
                final FireworkEffectMeta metaFw3 = (FireworkEffectMeta) meta3;
                final FireworkEffect aa3 = FireworkEffect.builder().withColor(Color.LIME).build();
                metaFw3.setEffect(aa3);
                playerDrop3.setItemMeta(metaFw3);

                final ItemStack playerDrop2 = new ItemStack(Material.FIREWORK_STAR, 1);
                final ItemMeta meta2 = playerDrop2.getItemMeta();
                meta2.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                meta2.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                meta2.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                meta2.addItemFlags(ItemFlag.HIDE_DESTROYS);
                meta2.addItemFlags(ItemFlag.HIDE_PLACED_ON);
                meta2.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                meta2.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
                final ArrayList<String> lore2 = new ArrayList<>();
                lore2.add(" ");
                lore2.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Regeneração");
                lore2.add(" ");
                meta2.setLore(lore2);
                final FireworkEffectMeta metaFw2 = (FireworkEffectMeta) meta2;
                final FireworkEffect aa2 = FireworkEffect.builder().withColor(Color.FUCHSIA).build();
                metaFw2.setEffect(aa2);
                playerDrop2.setItemMeta(metaFw2);

                final ItemStack playerDrop1 = new ItemStack(Material.FIREWORK_STAR, 1);
                final ItemMeta meta1 = playerDrop1.getItemMeta();
                meta1.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                meta1.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                meta1.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                meta1.addItemFlags(ItemFlag.HIDE_DESTROYS);
                meta1.addItemFlags(ItemFlag.HIDE_PLACED_ON);
                meta1.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                meta1.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
                final ArrayList<String> lore1 = new ArrayList<>();
                lore1.add(" ");
                lore1.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Pressa");
                lore1.add(" ");
                meta1.setLore(lore1);
                final FireworkEffectMeta metaFw1 = (FireworkEffectMeta) meta1;
                final FireworkEffect aa1 = FireworkEffect.builder().withColor(Color.NAVY).build();
                metaFw1.setEffect(aa1);
                playerDrop1.setItemMeta(metaFw1);

                final ItemStack playerDrop = new ItemStack(Material.FIREWORK_STAR, 1);
                final ItemMeta meta = playerDrop.getItemMeta();
                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
                meta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
                meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                meta.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
                final ArrayList<String> lore = new ArrayList<>();
                lore.add(" ");
                lore.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Força");
                lore.add(" ");
                meta.setLore(lore);
                final FireworkEffectMeta metaFw = (FireworkEffectMeta) meta;
                final FireworkEffect aa = FireworkEffect.builder().withColor(Color.RED).build();
                metaFw.setEffect(aa);
                playerDrop.setItemMeta(metaFw);

                player.getInventory().addItem(playerDrop);
                player.getInventory().addItem(playerDrop1);
                player.getInventory().addItem(playerDrop2);
                player.getInventory().addItem(playerDrop3);
                player.getInventory().addItem(playerDrop4);
            }
            if (args[0].equals("4")) {
                final String[] text = {AlternateColor.alternate("&eConfigurar"), AlternateColor.alternate("&eFarm")};
                Hologram.createHologram(player.getLocation(), text);
            }
            if (args[0].equals("3")) {
                try {
                    WorldEditUtil.paste(WorldEditUtil.load(Main.plugin.getDataFolder() + File.separator + "WorldEdit/schematics/espaconave-rpl"), false, player.getLocation());
                } catch (final IOException e) {
                    e.printStackTrace();
                } catch (final WorldEditException e) {
                    e.printStackTrace();
                }
            }
            if (args[0].equals("1")) {
                player.sendMessage(player.getLocation().getChunk().getX() + "X:Z" + player.getLocation().getChunk().getZ());
            }
            if (args[0].equals("2")) {
                final Merchant merchant = Bukkit.createMerchant("Ancião de trocas");

                // setup trading recipes:
                final List<MerchantRecipe> merchantRecipes = new ArrayList<MerchantRecipe>();
                final ItemStack buyItem1 = new ItemStack(Material.BOOK);
                final MerchantRecipe recipe = new MerchantRecipe(buyItem1, 10000); // no max-uses limit
                final MerchantRecipe recipe1 = new MerchantRecipe(buyItem1, 10000); // no max-uses limit
                final MerchantRecipe recipe2 = new MerchantRecipe(buyItem1, 10000); // no max-uses limit
                recipe.setExperienceReward(false); // no experience rewards
                recipe1.setExperienceReward(false); // no experience rewards
                recipe2.setExperienceReward(false); // no experience rewards

                final ItemStack papel = new ItemStack(Material.PAPER);
                final ItemMeta meta = papel.getItemMeta();
                meta.setDisplayName(ChatColor.YELLOW + "Papiro com escritos");
                final List<String> lore = new ArrayList<>();
                lore.add(" ");
                lore.add(ChatColor.YELLOW + "Escritos: " + ChatColor.DARK_GRAY + "Raro");
                lore.add(" ");
                meta.setLore(lore);
                papel.setItemMeta(meta);
                recipe.addIngredient(papel);

                final ItemStack papel1 = new ItemStack(Material.PAPER);
                final ItemMeta meta1 = papel1.getItemMeta();
                meta1.setDisplayName(ChatColor.YELLOW + "Papiro com escritos");
                final List<String> lore1 = new ArrayList<>();
                lore1.add(" ");
                lore1.add(ChatColor.YELLOW + "Escritos: " + ChatColor.DARK_GRAY + "Ultra raro");
                lore1.add(" ");
                meta1.setLore(lore1);
                papel1.setItemMeta(meta1);
                recipe1.addIngredient(papel1);

                final ItemStack papel2 = new ItemStack(Material.PAPER);
                final ItemMeta meta2 = papel2.getItemMeta();
                meta2.setDisplayName(ChatColor.YELLOW + "Papiro com escritos");
                final List<String> lore2 = new ArrayList<>();
                lore2.add(" ");
                lore2.add(ChatColor.YELLOW + "Escritos: " + ChatColor.DARK_GRAY + "Mega raro");
                lore2.add(" ");
                meta2.setLore(lore2);
                papel2.setItemMeta(meta2);
                recipe2.addIngredient(papel2);

                merchantRecipes.add(recipe);
                merchantRecipes.add(recipe1);
                merchantRecipes.add(recipe2);

                merchant.setRecipes(merchantRecipes);

                player.openMerchant(merchant, true);
            }
        }
        return false;
    }
}
