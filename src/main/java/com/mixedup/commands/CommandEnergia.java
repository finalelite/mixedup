package com.mixedup.commands;

import com.mixedup.apis.EnergiaAPI;
import com.mixedup.utils.AlternateColor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;

public class CommandEnergia implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player player = (Player) sender;
        final String UUID = player.getUniqueId().toString();

        final DecimalFormat formatterEnergia = new DecimalFormat("#####.#####");

        if (cmd.getName().equalsIgnoreCase("energia")) {
            if (args.length == 0) {
                player.sendMessage(ChatColor.GREEN + " * Você contém, " + formatterEnergia.format(EnergiaAPI.getEnergia(UUID)) + "kW.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                return false;
            }
            if (args.length == 1) {
                Player target = Bukkit.getPlayerExact(args[0]);

                if (target != null) {
                    player.sendMessage(ChatColor.GREEN + " * Você contém, " + formatterEnergia.format(EnergiaAPI.getEnergia(target.getUniqueId().toString())) + "kW.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes á &nenergia&c:\n \n&7 - /energia\n&7 - /energia (nick)\n \n"));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
        return false;
    }
}
