package com.mixedup.commands;

import br.com.finalelite.pauloo27.api.PauloAPI;
import com.mixedup.apis.GuildaAPI;
import com.mixedup.plots.terrain.TerrainsUtil;
import com.mixedup.worldProtection.hashs.ProtectionUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

public class RandomTeleportCommand implements CommandExecutor {

    private final HashMap<Player, Date> cooldowns = new HashMap<>();

    public int random(final int min, final int max, final int div) {

        final int res = min + ((new Random()).nextInt(max - min + 1));

        for (int i = res; i < res + div; i++) {

            if (i % div == 0) {
                return i;
            }

        }

        return -1;

    }

    private Location createLocation() {

        final int x = this.random(4, 13000, 4);
        final int z = this.random(4, 13000, 4);

        if (x != -1 && z != -1) {

            return new Location(Bukkit.getWorld("skygrid"), x, 76, z);

        } else {

            return null;

        }

    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String lb, final String[] args) {

        if (sender instanceof Player) {

            final Player p = (Player) sender;

            if (this.cooldowns.containsKey(p)) {

                if (this.cooldowns.get(p).after(new Date())) {

                    p.sendMessage(ChatColor.RED + " * Aguarde para poder executar esse comando novamente.");
                    return true;

                } else {

                    this.cooldowns.remove(p);

                }

            }

            final String guild = GuildaAPI.getGuilda(p.getUniqueId().toString());

            if ("sanguinaria".equals(guild)) {

                if (p.getWorld().getName().equalsIgnoreCase("skygrid")) {

                    final Location loc = this.createLocation();

                    if (loc != null) {

                        loc.setX(loc.getX() + 0.5);
                        loc.setZ(loc.getZ() + 0.5);

                        boolean block = false;

                        if (loc.getBlock().getType().equals(Material.AIR) || loc.getBlock().getType().equals(Material.LAVA) || loc.getBlock().getType().equals(Material.WATER) || loc.getBlock().getType().equals(Material.COBWEB)) {

                            block = true;

                        }

                        loc.setY(loc.getY() + 1);

                        if (!loc.getBlock().getType().equals(Material.AIR)) {

                            block = true;

                        }

                        loc.setY(loc.getY() + 1);

                        if (!loc.getBlock().getType().equals(Material.AIR)) {

                            block = true;

                        }

                        if (ProtectionUtil.playerInArea(loc) != null) block = true;
                        if (TerrainsUtil.playerInArea(loc) != null) block = true;

                        if (!block) {

                            loc.setY(loc.getY() - 1);
                            p.teleport(loc);
                            p.sendMessage(ChatColor.GREEN + " * Você foi teleportado com sucesso.");

                            if(PauloAPI.getInstance().getAccountInfo(p).getRole().isMajorStaff()) return true;
                            final Calendar calendar = Calendar.getInstance();

                            calendar.add(Calendar.MINUTE, 5);

                            this.cooldowns.put(p, calendar.getTime());
                            return true;

                        } else {

                            p.sendMessage(ChatColor.RED + " * Ops, não foi possível encontrar um local seguro para você, tente novamente.");
                            return true;

                        }

                    } else {

                        p.sendMessage(ChatColor.RED + " * Ops, não foi possível encontrar um local seguro para você, tente novamente.");
                        return true;

                    }

                } else {

                    p.sendMessage(ChatColor.RED + " * Opa, você não pode executar este comando neste mundo.");
                    return true;

                }

            } else {

                p.sendMessage(ChatColor.RED + " * Desculpe, apenas membros da guilda sanguinária podem executar este comando.");
                return true;

            }

        } else {

            sender.sendMessage(ChatColor.RED + " * Opa, este comando funciona apenas para jogadores.");
            return true;

        }

    }
}
