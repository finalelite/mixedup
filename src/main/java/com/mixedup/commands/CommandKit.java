package com.mixedup.commands;

import com.mixedup.managers.InventoryKits;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandKit implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;

        if (cmd.getName().equalsIgnoreCase("kit")) {
            player.openInventory(InventoryKits.firstInvKits());
        }
        return false;
    }
}
