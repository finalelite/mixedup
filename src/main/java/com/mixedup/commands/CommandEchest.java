package com.mixedup.commands;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandEchest implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("echest")) {
            if (args.length == 0) {
                player.openInventory(player.getEnderChest());
                player.playSound(player.getLocation(), Sound.BLOCK_ENDER_CHEST_OPEN, 1.0f, 1.0f);
            } else if (args.length == 1) {
                if (TagAPI.getTag(UUID).equalsIgnoreCase("Master") || TagAPI.getTag(UUID).equalsIgnoreCase("Supervisor") || TagAPI.getTag(UUID).equalsIgnoreCase("Admin") || TagAPI.getTag(UUID).equalsIgnoreCase("Moderador")) {
                    final Player target = Bukkit.getPlayerExact(args[0]);

                    if (target != null && target.isOnline()) {
                        player.openInventory(target.getEnderChest());
                        player.playSound(player.getLocation(), Sound.BLOCK_ENDER_CHEST_OPEN, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, este player não se encontra online!");
                        return false;
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/echest");
                    return false;
                }
            } else {
                player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/echest");
                return false;
            }
        }

        return false;
    }
}
