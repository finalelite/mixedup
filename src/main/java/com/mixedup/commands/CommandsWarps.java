package com.mixedup.commands;

import com.mixedup.Main;
import com.mixedup.apis.ConfigAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import com.mixedup.apis.WarpAPI;
import com.mixedup.plots.terrain.RegionTr;
import com.mixedup.plots.terrain.TerrainInfosAPI;
import com.mixedup.plots.terrain.TerrainsUtil;
import com.mixedup.utils.AlternateColor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class CommandsWarps implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        // -/warp criar (nome) -/warp deletar (nome) -/warp (nome) -/warp listar
        if (cmd.getName().equalsIgnoreCase("warp")) {
            if (args.length == 1) {
                if (args[0].equalsIgnoreCase("listar")) {
                    final List<String> list = WarpAPI.getAllWarps(UUID);
                    String msg = ChatColor.YELLOW + " \n * Warps lista: \n " + ChatColor.GRAY + " ";

                    if (list.size() == 0 || list == null) {
                        player.sendMessage(ChatColor.RED + " * Ops, você não contém warps!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        return false;
                    }

                    for (int i = 1; i <= list.size(); i++) {
                        if (i == 1) {
                            msg = msg + list.get(1 - 1);
                        } else {
                            msg = msg + ", " + list.get(i - 1);
                        }
                    }

                    player.sendMessage(msg + ".\n ");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                } else {
                    if (WarpAPI.getLocation(UUID, args[0]) != null) {
                        final String[] split = WarpAPI.getLocation(UUID, args[0]).split(":");
                        final Location location = new Location(Bukkit.getWorld(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]), Double.parseDouble(split[3]), Float.parseFloat(split[4]), Float.parseFloat(split[5]));

                        if (TerrainsUtil.playerInArea(location) != null) {
                            final RegionTr region = TerrainsUtil.playerInArea(location);
                            if (!region.getRegionID().contains(UUID)) {
                                if (!TerrainInfosAPI.getAmigos(region.getRegionID()).contains(UUID)) {
                                    player.sendMessage(ChatColor.RED + " * Ops, você não pode acessar está warp! (Motivo: Terreno protegido)");
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    return false;
                                }
                            }
                        }

                        player.teleport(location);
                        player.sendMessage(ChatColor.GREEN + " * Teletransportando para " + args[0] + ".");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 1.0f, 1.0f);

                        if (player.getAllowFlight() == true) {
                            final String tag = TagAPI.getTag(player.getUniqueId().toString());
                            if (tag != null && !(tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Moderador") || tag.equalsIgnoreCase("Admin"))) {
                                player.setAllowFlight(false);
                                ConfigAPI.updateFly(UUID, false);
                            }
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, esta warp não existe!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                }
            } else if (args.length == 2) {
                if (args[0].equalsIgnoreCase("criar")) {
                    if (TagAPI.getTag(UUID).equalsIgnoreCase("Membro")) {
                        final List<String> list = WarpAPI.getAllWarps(UUID);

                        final String tag = TagAPI.getTag(UUID);
                        int quantia = 10;
                        if (tag.equalsIgnoreCase("Vip")) {
                            quantia += 10;
                        } else if (tag.equalsIgnoreCase("Vip+")) {
                            quantia += 20;
                        } else if (tag.equalsIgnoreCase("Vip++")) {
                            quantia += 30;
                        } else if (tag.equalsIgnoreCase("Vip+++")) {
                            quantia += 40;
                        }

                        if (TerrainsUtil.playerInArea(player.getLocation()) != null) {
                            final RegionTr region = TerrainsUtil.playerInArea(player.getLocation());
                            if (!region.getRegionID().contains(UUID)) {
                                if (!TerrainInfosAPI.getAmigos(region.getRegionID()).contains(UUID)) {
                                    player.sendMessage(ChatColor.RED + " * Ops, você não tem permissão de criar warp neste local!");
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    return false;
                                }
                            }
                        }

                        if (list != null && list.size() == quantia) {
                            player.sendMessage(ChatColor.RED + " * Limite de warps atingida!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    player.sendMessage(ChatColor.GRAY + " * Adquira vantagens como estas adquirindo " + ChatColor.GREEN + "VIP" + ChatColor.GRAY + " em nosso site." + "\n" + ChatColor.YELLOW + " * www.finalelite.com.br");
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                            }, 40L);
                        } else {
                            if (WarpAPI.getLocation(UUID, args[1]) == null) {
                                final String location = player.getWorld().getName() + ":" + player.getLocation().getX() + ":" + player.getLocation().getY() + ":" + player.getLocation().getZ() + ":" + player.getLocation().getYaw() + ":" + player.getLocation().getPitch();
                                WarpAPI.createWarp(UUID, args[1], location);
                                player.sendMessage(ChatColor.GREEN + " * Warp criada com sucesso!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            } else {
                                player.sendMessage(ChatColor.GREEN + " * Ops, esta já existe!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        }
                    } else {
                        final List<String> list = WarpAPI.getAllWarps(UUID);
                        if (list != null && list.size() == 20) {
                            player.sendMessage(ChatColor.RED + " * Limite de warps atingida!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else {
                            if (WarpAPI.getLocation(UUID, args[1]) == null) {
                                final String location = player.getWorld().getName() + ":" + player.getLocation().getX() + ":" + player.getLocation().getY() + ":" + player.getLocation().getZ() + ":" + player.getLocation().getYaw() + ":" + player.getLocation().getPitch();
                                WarpAPI.createWarp(UUID, args[1], location);
                                player.sendMessage(ChatColor.GREEN + " * Warp criada com sucesso!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            } else {
                                player.sendMessage(ChatColor.GREEN + " * Ops, esta já existe!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        }
                    }
                } else if (args[0].equalsIgnoreCase("deletar")) {
                    if (WarpAPI.getLocation(UUID, args[1]) != null) {
                        WarpAPI.removeWarp(args[1], UUID);
                        player.sendMessage(ChatColor.GREEN + " * Warp deletada com sucesso!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, está warp não existe!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes á &n/warp&c:\n \n&7 - /warp (nome)\n&7 - /warp listar\n&7 - /warp (criar) (nome)\n&7 - /warp (deletar) (nome)\n \n"));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes á &n/warp&c:\n \n&7 - /warp (nome)\n&7 - /warp listar\n&7 - /warp (criar) (nome)\n&7 - /warp (deletar) (nome)\n \n"));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
        return false;
    }
}
