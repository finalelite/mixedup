package com.mixedup.commands;

import com.mixedup.Main;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import com.mixedup.tutorial.TutorialManager;
import com.mixedup.tutorial.TutorialStage;
import com.mixedup.utils.CriarItem;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class CommandTutorial implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, final String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Comando disponível apenas para jogadores.");
            return false;
        }
        final Player player = (Player) sender;
        final String tag = TagAPI.getTag(PlayerUUID.getUUID(player.getName()));
        final boolean hasPermission = tag.equals("Master") || tag.equals("Supervisor");

        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("ir")) {
                if (TutorialManager.get().getStages().isEmpty()) {
                    player.sendMessage("§c* Ainda não há um tutorial.");
                    return false;
                }

                TutorialManager.get().runAll(player, TutorialManager.get()::removeFromTutorial);

            } else if (args[0].equalsIgnoreCase("sair")) {
                if (TutorialManager.get().inTutorial(player)) {
                    TutorialManager.get().removeFromTutorial(player);

                    player.sendMessage("");
                    player.sendMessage(" §a* Você saiu do tutorial!");
                    player.sendMessage(" §a* Esperamos que tenha entendido tudo!");
                    player.sendMessage(" §a* Mas caso ainda tenha dúvidas, não hesite");
                    player.sendMessage(" §a* em contatar um membro da equipe!");
                    player.sendMessage("");
                } else {
                    player.sendMessage("§c* Você não está no tutorial.");
                }

            } else if (args[0].equalsIgnoreCase("lista")) {
                if (TutorialManager.get().getStages().isEmpty()) {
                    player.sendMessage("§c * Não há nenhum tutorial criado.");
                    return false;
                }

                player.sendMessage("");
                player.sendMessage("     §aLista de todos tutoriais disponíveis: ");
                player.sendMessage(" §7(utilize /tutorial <tutorial> para ir para um específico)");
                TutorialManager.get().getStages().entrySet().stream().map(entry -> " §f" + entry.getKey() + ". §7" + entry.getValue().getName()).forEach(player::sendMessage);
                player.sendMessage("");

            } else {
                final TutorialStage stage = TutorialManager.get().getStage(args[0]);

                if (stage == null) {
                    player.sendMessage(String.format("§c* O tutorial '%s' não existe. Você digitou certo?", args[0]));
                    return false;
                }

                TutorialManager.get().runStage(player, stage);
            }
        } else if (args.length == 2) {
            if (!hasPermission) {
                player.sendMessage("§c* Você não tem permissão para executar esse comando.");
                return false;
            }

            if (args[0].equalsIgnoreCase("criar")) {
                TutorialManager.get().addStage(new TutorialStage(args[1], 300, new ArrayList<>(), new ArrayList<>()));
                player.sendMessage(String.format("§a* '%s' foi adicionado no cache, agora adicione locais e a mensagem ao estágio.", args[1]));

            } else if (args[0].equalsIgnoreCase("addloc")) {
                final TutorialStage stage = TutorialManager.get().getStage(args[1]);

                if (stage == null) {
                    player.sendMessage(String.format("§c* O estágio '%s' não existe. Você digitou certo?", args[1]));
                    return false;
                }

                stage.getLocations().add(player.getLocation());
                TutorialManager.get().saveStage(stage);
                player.sendMessage(String.format("§a* Local adicionado ao estágio '%s'.", args[1]));

            } else if (args[0].equalsIgnoreCase("verlocs")) {
                final TutorialStage stage = TutorialManager.get().getStage(args[1]);

                if (stage == null) {
                    player.sendMessage(String.format("§c* O estágio '%s' não existe. Você digitou certo?", args[1]));
                    return false;
                }

                player.sendMessage("");
                player.sendMessage("     §aLista de locais do estágio '" + stage.getName() + "': ");
                for (int i = 0; i < stage.getLocations().size(); i++) {
                    final Location l = stage.getLocations().get(i);
                    player.sendMessage(" §f" + i + ". §7Mundo: " + l.getWorld().getName() + " (X: " + l.getX() + ", Y: " + l.getY() + ", Z: " + l.getZ() + ")");
                }
                player.sendMessage("");

            } else if (args[0].equalsIgnoreCase("editarmensagem")) {
                final TutorialStage stage = TutorialManager.get().getStage(args[1]);

                if (stage == null) {
                    player.sendMessage(String.format("§c* O estágio '%s' não existe. Você digitou certo?", args[1]));
                    return false;
                }

                final ItemStack book = CriarItem.add(Material.WRITABLE_BOOK, "§aEditar mensagem §7(" + stage.getName() + ")");
                final BookMeta bookMeta = (BookMeta) book.getItemMeta();
                bookMeta.setPages(stage.getMessage());
                book.setItemMeta(bookMeta);

                player.getInventory().addItem(book);
                player.setMetadata(TutorialManager.EDITING_MESSAGE_METADATA, new FixedMetadataValue(Main.getInstance(), ""));
                player.sendMessage("§a* Você recebeu um livro para editar a mensagem do estágio, ao finalizar, assine o livro.");

            } else if (args[0].equalsIgnoreCase("settempo")) {
                final TutorialStage stage = TutorialManager.get().getStage(args[1]);

                if (stage == null) {
                    player.sendMessage(String.format("§c* O estágio '%s' não existe. Você digitou certo?", args[1]));
                    return false;
                }

                long time = -1;
                try {
                    time = Long.parseLong(args[2]);
                } catch (final NumberFormatException e) {
                    player.sendMessage(" §c* O tempo precisa ser um número.");
                    return false;
                }

                if (time < 0) {
                    player.sendMessage(" §c* Tempo inválido.");
                    return false;
                }

                stage.setTime(TimeUnit.SECONDS.toMillis(time));
                player.sendMessage(" §a* Tempo do estágio '" + stage.getName() + "' definido para " + time + " segundos.");
            }
        } else if (args.length == 3) {
            if (args[0].equalsIgnoreCase("removeloc")) {
                final TutorialStage stage = TutorialManager.get().getStage(args[1]);

                if (stage == null) {
                    player.sendMessage(String.format("§c* O estágio '%s' não existe. Você digitou certo?", args[1]));
                    return false;
                }

                int index = -1;
                try {
                    index = Integer.parseInt(args[2]);
                } catch (final NumberFormatException e) {
                    player.sendMessage(" §c* O índice precisa ser um número.");
                    return false;
                }

                if (index < 0 || index + 1 > stage.getLocations().size()) {
                    player.sendMessage(" §c* Índice inválido.");
                    return false;
                }

                stage.getLocations().remove(index);
                TutorialManager.get().saveStage(stage);
                player.sendMessage(" §a* Local " + index + " do estágio '" + stage.getName() + "' removido com sucesso.");
            }
        } else {
            player.sendMessage("");
            player.sendMessage(" §a/tutorial ir §8- §7Veja o tutorial completo.");
            player.sendMessage(" §a/tutorial <assunto> §8- §7Vá pra um assunto específico do tutorial.");
            player.sendMessage(" §a/tutorial lista §8- §7Veja a lista de assuntos.");
            player.sendMessage(" §a/tutorial sair §8- §7Sair do tutorial.");
            if (hasPermission) {
                player.sendMessage(" §c/tutorial criar <assunto> §8- §7Criar um novo assunto/estágio no tutorial.");
                player.sendMessage(" §c/tutorial addloc <assunto> §8- §7Adicionar um ponto de teleporte em um assunto/estágio.");
                player.sendMessage(" §c/tutorial removeloc <assunto> <índice> §8- §7Remover um ponto de teleporte de um assunto/estágio.");
                player.sendMessage(" §c/tutorial verlocs <assunto> §8- §7Ver todos pontos de teleporte de um assunto/estágio.");
                player.sendMessage(" §c/tutorial settempo <assunto> <tempo> §8- §7Definir o tempo do assunto/estágio em segundos.");
                player.sendMessage(" §c/tutorial editarmensagem <assunto> §8- §7Editar a mensagem de um assunto/estágio.");
            }
            player.sendMessage("");
        }

        return false;
    }
}
