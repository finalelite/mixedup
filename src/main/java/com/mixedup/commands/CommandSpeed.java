package com.mixedup.commands;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandSpeed implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("speed") || cmd.getName().equalsIgnoreCase("velocidade")) {
            if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor") || TagAPI.getTag(UUID).equals("Admin")) {
                if (args.length == 0) {
                    if (player.isFlying()) {
                        player.setFlySpeed(0.2f);
                    } else {
                        player.setWalkSpeed(0.2f);
                    }
                    player.sendMessage(ChatColor.GREEN + " * Velocidade setada ao modo normal.");
                } else if (args.length == 1) {
                    if (args[0].equals("0")) {
                        if (player.isFlying()) {
                            player.setFlySpeed(0.0f);
                        } else {
                            player.setWalkSpeed(0.0f);
                        }
                        player.sendMessage(ChatColor.GREEN + " * Velocidade setada ao nível 0.");
                    }
                    if (args[0].equals("1")) {
                        if (player.isFlying()) {
                            player.setFlySpeed(0.1f);
                        } else {
                            player.setWalkSpeed(0.1f);
                        }
                        player.sendMessage(ChatColor.GREEN + " * Velocidade setada ao nível 1.");
                    }
                    if (args[0].equals("2")) {
                        if (player.isFlying()) {
                            player.setFlySpeed(0.2f);
                        } else {
                            player.setWalkSpeed(0.2f);
                        }
                        player.sendMessage(ChatColor.GREEN + " * Velocidade setada ao nível 2.");
                    }
                    if (args[0].equals("3")) {
                        if (player.isFlying()) {
                            player.setFlySpeed(0.3f);
                        } else {
                            player.setWalkSpeed(0.3f);
                        }
                        player.sendMessage(ChatColor.GREEN + " * Velocidade setada ao nível 3.");
                    }
                    if (args[0].equals("4")) {
                        if (player.isFlying()) {
                            player.setFlySpeed(0.4f);
                        } else {
                            player.setWalkSpeed(0.4f);
                        }
                        player.sendMessage(ChatColor.GREEN + " * Velocidade setada ao nível 4.");
                    }
                    if (args[0].equals("5")) {
                        if (player.isFlying()) {
                            player.setFlySpeed(0.5f);
                        } else {
                            player.setWalkSpeed(0.5f);
                        }
                        player.sendMessage(ChatColor.GREEN + " * Velocidade setada ao nível 5.");
                    }
                    if (args[0].equals("6")) {
                        if (player.isFlying()) {
                            player.setFlySpeed(0.6f);
                        } else {
                            player.setWalkSpeed(0.6f);
                        }
                        player.sendMessage(ChatColor.GREEN + " * Velocidade setada ao nível 6.");
                    }
                    if (args[0].equals("7")) {
                        if (player.isFlying()) {
                            player.setFlySpeed(0.7f);
                        } else {
                            player.setWalkSpeed(0.7f);
                        }
                        player.sendMessage(ChatColor.GREEN + " * Velocidade setada ao nível 7.");
                    }
                    if (args[0].equals("8")) {
                        if (player.isFlying()) {
                            player.setFlySpeed(0.8f);
                        } else {
                            player.setWalkSpeed(0.8f);
                        }
                        player.sendMessage(ChatColor.GREEN + " * Velocidade setada ao nível 8.");
                    }
                    if (args[0].equals("9")) {
                        if (player.isFlying()) {
                            player.setFlySpeed(0.9f);
                        } else {
                            player.setWalkSpeed(0.9f);
                        }
                        player.sendMessage(ChatColor.GREEN + " * Velocidade setada ao nível 9.");
                    }
                    if (args[0].equals("10")) {
                        if (player.isFlying()) {
                            player.setFlySpeed(1);
                        } else {
                            player.setWalkSpeed(1);
                        }
                        player.sendMessage(ChatColor.GREEN + " * Velocidade setada ao nível 10.");
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/speed (numero) ou /velocidade (numero)");
                }
            }
        }
        return false;
    }
}
