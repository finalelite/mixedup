package com.mixedup.commands;

import com.mixedup.Main;
import com.mixedup.MySqlHub;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.ServerAPI;
import com.mixedup.apis.TagAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CommandManutencao implements CommandExecutor {

    private static List<String> getNumberInOrder() {
        final List<String> list = new ArrayList<String>();
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Online_server ORDER BY Online");
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if (rs.getString("Server").contains("Hub") && ServerAPI.getManutencao(rs.getString("Server")) == false) {
                    list.add(rs.getString("Server"));
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("manutencao")) {
            if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                if (args.length == 1) {
                    if (args[0].equals("ativar")) {
                        if (ServerAPI.getManutencao("Factions") == false) {
                            ServerAPI.updateManutencao("Factions", true);
                            player.sendMessage(ChatColor.GREEN + "\n \n * Manutenção ativada.\n \n");

                            for (final Player target : Bukkit.getOnlinePlayers()) {
                                if (TagAPI.getTag(PlayerUUID.getUUID(target.getName())).equals("Master") || TagAPI.getTag(PlayerUUID.getUUID(target.getName())).equals("Supervisor") || TagAPI.getTag(PlayerUUID.getUUID(target.getName())).equals("Admin") || TagAPI.getTag(PlayerUUID.getUUID(target.getName())).equals("Moderador") || TagAPI.getTag(PlayerUUID.getUUID(target.getName())).equals("Suporte")) {
                                    return false;
                                }

                                for (int i = 0; i < 100; i++) {
                                    target.sendMessage(" ");
                                }
                                target.sendMessage(ChatColor.RED + " * MANUTENÇÃO ATIVADA, VOLTAMOS EM INSTANTES!\n ");

                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                                            final DataOutputStream out = new DataOutputStream(b);

                                            out.writeUTF("Connect");
                                            out.writeUTF(CommandManutencao.getNumberInOrder().get(CommandManutencao.getNumberInOrder().size()));
                                            ServerAPI.updateOnline("Factions", ServerAPI.getOnlineServer("Factions") - 1);

                                            target.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                                        } catch (final Exception e) {
                                            player.sendMessage(ChatColor.RED + "Ops, algo ocorreu de errado!");
                                        }
                                    }
                                }, 20L);
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, este servidor já está com manutenção ativa!");
                        }
                    } else if (args[0].equals("desativar")) {
                        if (ServerAPI.getManutencao("Factions") == true) {
                            ServerAPI.updateManutencao("Factions", false);
                            player.sendMessage(ChatColor.GREEN + "\n \n * Manutenção desativada.\n \n");
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, este servidor já está com manutenção desativada!");
                        }
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/manutencao ativar ou /manutencao desativar");
                }
            }
        }
        return false;
    }
}
