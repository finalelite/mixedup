package com.mixedup.commands;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandTp implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("tp")) {
            if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor") || TagAPI.getTag(UUID).equals("Admin") || TagAPI.getTag(UUID).equals("Moderador")) {
                if (args.length == 1) {
                    final Player target = Bukkit.getPlayerExact(args[0]);

                    if (target == null) {
                        player.sendMessage(ChatColor.RED + " * Ops, o player referido pode não estar online ou não existir!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        return false;
                    }

                    if (target.isOnline()) {
                        GameMode gameMode = null;
                        if (player.getGameMode().equals(GameMode.CREATIVE)) gameMode = GameMode.CREATIVE;
                        if (player.getGameMode().equals(GameMode.SURVIVAL)) gameMode = GameMode.SURVIVAL;
                        if (player.getGameMode().equals(GameMode.SPECTATOR)) gameMode = GameMode.SPECTATOR;
                        if (player.getGameMode().equals(GameMode.ADVENTURE)) gameMode = GameMode.ADVENTURE;

                        player.teleport(target.getLocation());
                        player.sendMessage(ChatColor.GREEN + " * Teletransportando.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.setGameMode(gameMode);
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, este player não está online no momento!");
                    }
                } else if (args.length == 2) {
                    final Player player1 = Bukkit.getPlayerExact(args[0]);
                    final Player player2 = Bukkit.getPlayerExact(args[1]);

                    if (player1.isOnline() && player2.isOnline()) {
                        player1.teleport(player2.getLocation());
                        player.sendMessage(ChatColor.YELLOW + " * " + args[0] + " teletransportado para " + args[1] + ".");
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, algum dos players referidos pode não estar online ou não existir!");
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/tp (nick) ou /tp (nick) (nick)");
                }
            }
        }
        return false;
    }
}
