package com.mixedup.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class CommandLixeira implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = player.getUniqueId().toString();

        if (cmd.getName().equalsIgnoreCase("lixo") || cmd.getName().equalsIgnoreCase("lixeira")) {
            final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Lixeira: ");

            final ItemStack depositar = new ItemStack(Material.MUSIC_DISC_CHIRP);
            final ItemMeta metaPosi = depositar.getItemMeta();
            metaPosi.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
            metaPosi.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
            metaPosi.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
            metaPosi.addItemFlags(ItemFlag.HIDE_PLACED_ON);
            metaPosi.addItemFlags(ItemFlag.HIDE_DESTROYS);
            metaPosi.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            metaPosi.setDisplayName(ChatColor.GREEN + "Deletar");
            depositar.setItemMeta(metaPosi);
            inventory.setItem(53, depositar);

            player.openInventory(inventory);
            player.playSound(player.getLocation(), Sound.BLOCK_CHEST_OPEN, 1.0f, 1.0f);
        }
        return false;
    }
}
