package com.mixedup.commands;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandKill implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("kill")) {
            if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor") || TagAPI.getTag(UUID).equals("Admin")) {
                if (args.length == 0) {
                    player.setHealth(0);
                    player.sendMessage(ChatColor.RED + "Porque você fez isso? Você precisa se tratar =*(");
                } else if (args.length == 1) {
                    final Player target = Bukkit.getPlayerExact(args[0]);

                    if (target.isOnline()) {
                        target.setHealth(0);
                        player.sendMessage(ChatColor.GREEN + " * Player " + args[0] + " foi assasinado com sucesso.");
                        target.sendMessage(ChatColor.RED + "Ithh.. Você foi morto por " + player.getName() + ".");
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, este player não está online no momento.");
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/kill (nick)");
                }
            }
        }
        return false;
    }
}
