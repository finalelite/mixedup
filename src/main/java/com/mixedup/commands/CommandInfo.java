package com.mixedup.commands;

import com.mixedup.apis.KDRApi;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.PoderAPI;
import com.mixedup.economy.CoinsAPI;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;

public class CommandInfo implements CommandExecutor {

    private static final DecimalFormat formatter = new DecimalFormat("#,###.00");

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;

        if (cmd.getName().equalsIgnoreCase("info")) {
            if (args.length == 1) {
                if (PlayerUUID.getUUIDcache(args[0]) != null) {
                    final String UUID = PlayerUUID.getUUIDcache(args[0]);

                    int KDR = 0;
                    if (KDRApi.getKills(UUID) != 0 && KDRApi.getDeaths(UUID) != 0) {
                        KDR = KDRApi.getKills(UUID) / KDRApi.getDeaths(UUID);
                    }
                    final int Kills = KDRApi.getKills(UUID);
                    final int Deaths = KDRApi.getDeaths(UUID);

                    final String msg = " \n " + ChatColor.YELLOW + "Informações: \n \n Jogador: " + ChatColor.GRAY + args[0] + "\n " + ChatColor.YELLOW + "Esquadrão: " + ChatColor.GRAY + "Sem esquadrão\n " + ChatColor.YELLOW + "Poder: " + ChatColor.GRAY + "(" + PoderAPI.getPoder(UUID) + "/15)\n " +
                            ChatColor.YELLOW + "Moedas: " + ChatColor.GRAY + "$" + CommandInfo.formatter.format(CoinsAPI.getCoins(UUID)) + "\n " + ChatColor.YELLOW + "Moeda negra: " + ChatColor.GRAY + CommandInfo.formatter.format(CoinsAPI.getBlackCoins(UUID)) + "\n " + ChatColor.YELLOW + "Posição no ranking: " + ChatColor.GRAY + "\n " +
                            ChatColor.YELLOW + "KDR: " + ChatColor.GRAY + KDR + "\n " + ChatColor.YELLOW + "Vítimas: " + ChatColor.GRAY + Kills + "\n " + ChatColor.YELLOW + "Mortes: " + ChatColor.GRAY + Deaths + "\n ";
                    player.sendMessage(msg);
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, este player não existe.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/info (nick)");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
        return false;
    }
}
