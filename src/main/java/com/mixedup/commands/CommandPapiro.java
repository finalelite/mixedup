package com.mixedup.commands;

import com.mixedup.apis.TagAPI;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;
import java.util.Map.Entry;

public class CommandPapiro implements CommandExecutor {

    private static ItemStack getItem(final String type) {
        if (type.equalsIgnoreCase("raro")) {
            final ItemStack papel = new ItemStack(Material.PAPER);
            final ItemMeta meta = papel.getItemMeta();
            meta.setDisplayName(ChatColor.YELLOW + "Papiro com escritos");
            final List<String> lore = new ArrayList<>();
            lore.add(" ");
            lore.add(ChatColor.YELLOW + "Escritos: " + ChatColor.DARK_GRAY + "Raro");
            lore.add(" ");
            meta.setLore(lore);
            papel.setItemMeta(meta);

            return papel;
        } else if (type.equalsIgnoreCase("ultra_raro")) {
            final ItemStack papel1 = new ItemStack(Material.PAPER);
            final ItemMeta meta1 = papel1.getItemMeta();
            meta1.setDisplayName(ChatColor.YELLOW + "Papiro com escritos");
            final List<String> lore1 = new ArrayList<>();
            lore1.add(" ");
            lore1.add(ChatColor.YELLOW + "Escritos: " + ChatColor.DARK_GRAY + "Ultra raro");
            lore1.add(" ");
            meta1.setLore(lore1);
            papel1.setItemMeta(meta1);

            return papel1;
        } else if (type.equalsIgnoreCase("mega_raro")) {
            final ItemStack papel2 = new ItemStack(Material.PAPER);
            final ItemMeta meta2 = papel2.getItemMeta();
            meta2.setDisplayName(ChatColor.YELLOW + "Papiro com escritos");
            final List<String> lore2 = new ArrayList<>();
            lore2.add(" ");
            lore2.add(ChatColor.YELLOW + "Escritos: " + ChatColor.DARK_GRAY + "Mega raro");
            lore2.add(" ");
            meta2.setLore(lore2);
            papel2.setItemMeta(meta2);

            return papel2;
        } else return null;
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = player.getUniqueId().toString();

        // -/papiro ceder (tipo) (quantia)
        if (cmd.getName().equalsIgnoreCase("papiro")) {
            if (TagAPI.getTag(UUID).equalsIgnoreCase("Master") || TagAPI.getTag(UUID).equalsIgnoreCase("Supervisor")) {
                if (args.length == 3) {
                    if (args[0].equalsIgnoreCase("ceder")) {
                        final String[] types = {"raro", "mega_raro", "ultra_raro"};

                        if (Arrays.asList(types).contains(args[1])) {
                            try {
                                final int amount = Integer.valueOf(args[2]);
                                final ItemStack item = CommandPapiro.getItem(args[1]);
                                item.setAmount(amount);

                                final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(item);
                                for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                    player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                                }
                                player.sendMessage(ChatColor.GREEN + " * Papiro cedido com sucesso!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            } catch (final NumberFormatException e) {
                                player.sendMessage(ChatColor.RED + " * Ops, a quantia apresenta algum erro!");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, o tipo informado não existe!");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Comando correto: " + ChatColor.GRAY + "/tnt ceder (tipo) (quantia)");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + " * Comando correto: " + ChatColor.GRAY + "/tnt ceder (tipo) (quantia)");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage("§c* Você não tem permissão para executar esse comando.");
            }
        }

        return false;
    }
}
