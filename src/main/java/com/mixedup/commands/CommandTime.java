package com.mixedup.commands;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import com.mixedup.apis.WorldAPI;
import com.mixedup.utils.AlternateColor;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandTime implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("weather") || cmd.getName().equalsIgnoreCase("clima")) {
            if (TagAPI.getTag(UUID).equalsIgnoreCase("Master") || TagAPI.getTag(UUID).equalsIgnoreCase("Supervisor")) {
                if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("travar") || args[0].equalsIgnoreCase("lock")) {
                        if (WorldAPI.getExistWorld(player.getWorld().getName()) == null) {
                            WorldAPI.createWorldInfo(player.getWorld().getName(), false, true);
                            player.sendMessage(ChatColor.GREEN + " * Tempo travado com sucesso.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else {
                            WorldAPI.updateWeatherLock(player.getWorld().getName(), true);
                            player.sendMessage(ChatColor.GREEN + " * Tempo travado com sucesso.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else if (args[0].equalsIgnoreCase("destravar") || args[0].equalsIgnoreCase("unlock")) {
                        if (WorldAPI.getExistWorld(player.getWorld().getName()) == null) {
                            player.sendMessage(ChatColor.RED + " * Ops, o tempo deste mundo não se encontra travado.");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            return false;
                        } else {
                            WorldAPI.updateWeatherLock(player.getWorld().getName(), false);
                            player.sendMessage(ChatColor.GREEN + " * Tempo destravado com sucesso.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else if (args[0].equalsIgnoreCase("rain") || args[0].equalsIgnoreCase("chuva")) {
                        player.getWorld().setStorm(true);
                        player.sendMessage(ChatColor.GREEN + " * Chuva habilitada com sucesso.");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        return false;
                    } else if (args[0].equalsIgnoreCase("clear") || args[0].equalsIgnoreCase("limpar")) {
                        player.getWorld().setStorm(false);
                        player.getWorld().setThundering(false);
                        player.sendMessage(ChatColor.GREEN + " * Chuva desabilitada com sucesso.");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        return false;
                    } else {
                        player.sendMessage(ChatColor.RED + " \nComandos referente á " + AlternateColor.alternate("&nclima") + ChatColor.RED + ": " + "\n" + " " + "\n" + ChatColor.GRAY + "/clima (limpar/chuva)\n /clima (travar/destravar)\n ");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                }
            }
        }

        if (cmd.getName().equalsIgnoreCase("time") || cmd.getName().equalsIgnoreCase("tempo")) {
            if (TagAPI.getTag(UUID).equalsIgnoreCase("Master") || TagAPI.getTag(UUID).equalsIgnoreCase("Supervisor")) {
                if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("travar") || args[0].equalsIgnoreCase("lock")) {
                        if (WorldAPI.getExistWorld(player.getWorld().getName()) == null) {
                            WorldAPI.createWorldInfo(player.getWorld().getName(), true, false);
                            player.sendMessage(ChatColor.GREEN + " * Tempo travado com sucesso.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else {
                            WorldAPI.updateTimeLock(player.getWorld().getName(), true);
                            player.sendMessage(ChatColor.GREEN + " * Tempo travado com sucesso.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else if (args[0].equalsIgnoreCase("destravar") || args[0].equalsIgnoreCase("unlock")) {
                        if (WorldAPI.getExistWorld(player.getWorld().getName()) == null) {
                            player.sendMessage(ChatColor.RED + " * Ops, o tempo deste mundo não se encontra travado.");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            return false;
                        } else {
                            WorldAPI.updateTimeLock(player.getWorld().getName(), false);
                            player.sendMessage(ChatColor.GREEN + " * Tempo destravado com sucesso.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " \nComandos referente á " + AlternateColor.alternate("&ntempo") + ChatColor.RED + ": " + "\n" + " " + "\n" + ChatColor.GRAY + "/tempo definir (time)\n /tempo (travar/destravar)\n ");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else if (args.length == 2) {
                    if (args[0].equalsIgnoreCase("set") || args[0].equalsIgnoreCase("definir")) {
                        try {
                            final int tempo = Integer.valueOf(args[1]);
                            player.getWorld().setTime(tempo);
                            player.sendMessage(ChatColor.GREEN + " * Tempo de " + player.getWorld().getName() + " definido para " + tempo + ".");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } catch (final NumberFormatException e) {
                            player.sendMessage(ChatColor.RED + " * Ops, o numero informado contém algum erro.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " \nComandos referente á " + AlternateColor.alternate("&ntempo") + ChatColor.RED + ": " + "\n" + " " + "\n" + ChatColor.GRAY + "/tempo definir (time)\n /tempo (travar/destravar)\n ");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + " \nComandos referente á " + AlternateColor.alternate("&ntempo") + ChatColor.RED + ": " + "\n" + " " + "\n" + ChatColor.GRAY + "/tempo definir (time)\n /tempo (travar/destravar)\n ");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
