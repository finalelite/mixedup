package com.mixedup.commands;

import com.mixedup.utils.RemoveItemInv;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class CommandCompactar implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;

        if (cmd.getName().equalsIgnoreCase("compactar")) {
            if (args.length == 0) {
                if (player.getItemInHand() != null && !player.getItemInHand().getType().equals(Material.AIR) && player.getItemInHand().getType().equals(Material.IRON_INGOT)) {
                    int quantia = 0;
                    for (int i = 0; i < player.getInventory().getSize(); i++) {
                        if (player.getInventory().getItem(i) != null) {
                            if (player.getInventory().getItem(i).getType().equals(Material.IRON_INGOT)) {
                                if (quantia == 0) {
                                    quantia = player.getInventory().getItem(i).getAmount();
                                } else {
                                    quantia = quantia + player.getInventory().getItem(i).getAmount();
                                }
                            } else {
                                continue;
                            }
                        } else {
                            continue;
                        }
                    }

                    boolean finish = false;
                    int block = 0;
                    int remove = 0;

                    int y = 0;
                    for (int x = 0; x <= y; x++) {
                        if (quantia >= 9) {
                            block++;
                            quantia = quantia - 9;
                            if (remove == 0) {
                                remove = 9;
                            } else {
                                remove = remove + 9;
                            }
                            y++;
                        } else {
                            y = -1;
                        }
                    }

                    if (block != 0 && remove != 0) {
                        RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.IRON_INGOT), remove);
                        final HashMap<Integer, ItemStack> nope2 = player.getInventory().addItem(new ItemStack(Material.IRON_BLOCK, block));
                        for (final Entry<Integer, ItemStack> entry : nope2.entrySet()) {
                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                        }

                        player.sendMessage(ChatColor.GREEN + " * Blocos compactados!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você não contém ferros o suficiente para efetuar este comando!");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                    finish = false;
                    block = 0;
                    remove = 0;
                    quantia = 0;
                } else if (player.getItemInHand() != null && !player.getItemInHand().getType().equals(Material.AIR) && player.getItemInHand().getType().equals(Material.GOLD_INGOT)) {
                    int quantia = 0;
                    for (int i = 0; i < player.getInventory().getSize(); i++) {
                        if (player.getInventory().getItem(i) != null) {
                            if (player.getInventory().getItem(i).getType().equals(Material.GOLD_INGOT)) {
                                if (quantia == 0) {
                                    quantia = player.getInventory().getItem(i).getAmount();
                                } else {
                                    quantia = quantia + player.getInventory().getItem(i).getAmount();
                                }
                            } else {
                                continue;
                            }
                        } else {
                            continue;
                        }
                    }

                    boolean finish = false;
                    int block = 0;
                    int remove = 0;

                    int y = 0;
                    for (int x = 0; x <= y; x++) {
                        if (quantia >= 9) {
                            block++;
                            quantia = quantia - 9;
                            if (remove == 0) {
                                remove = 9;
                            } else {
                                remove = remove + 9;
                            }
                            y++;
                        } else {
                            y = -1;
                        }
                    }

                    if (block != 0 && remove != 0) {
                        RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.GOLD_INGOT), remove);
                        final HashMap<Integer, ItemStack> nope2 = player.getInventory().addItem(new ItemStack(Material.GOLD_BLOCK, block));
                        for (final Entry<Integer, ItemStack> entry : nope2.entrySet()) {
                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                        }

                        player.sendMessage(ChatColor.GREEN + " * Blocos compactados!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você não contém ferros o suficiente para efetuar este comando!");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                    finish = false;
                    block = 0;
                    remove = 0;
                    quantia = 0;
                } else if (player.getItemInHand() != null && !player.getItemInHand().getType().equals(Material.AIR) && player.getItemInHand().getType().equals(Material.COAL)) {
                    int quantia = 0;
                    for (int i = 0; i < player.getInventory().getSize(); i++) {
                        if (player.getInventory().getItem(i) != null) {
                            if (player.getInventory().getItem(i).getType().equals(Material.COAL)) {
                                if (quantia == 0) {
                                    quantia = player.getInventory().getItem(i).getAmount();
                                } else {
                                    quantia = quantia + player.getInventory().getItem(i).getAmount();
                                }
                            } else {
                                continue;
                            }
                        } else {
                            continue;
                        }
                    }

                    boolean finish = false;
                    int block = 0;
                    int remove = 0;

                    int y = 0;
                    for (int x = 0; x <= y; x++) {
                        if (quantia >= 9) {
                            block++;
                            quantia = quantia - 9;
                            if (remove == 0) {
                                remove = 9;
                            } else {
                                remove = remove + 9;
                            }
                            y++;
                        } else {
                            y = -1;
                        }
                    }

                    if (block != 0 && remove != 0) {
                        RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.COAL), remove);
                        final HashMap<Integer, ItemStack> nope2 = player.getInventory().addItem(new ItemStack(Material.COAL_BLOCK, block));
                        for (final Entry<Integer, ItemStack> entry : nope2.entrySet()) {
                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                        }

                        player.sendMessage(ChatColor.GREEN + " * Blocos compactados!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você não contém ferros o suficiente para efetuar este comando!");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                    finish = false;
                    block = 0;
                    remove = 0;
                    quantia = 0;
                } else if (player.getItemInHand() != null && !player.getItemInHand().getType().equals(Material.AIR) && player.getItemInHand().getType().equals(Material.REDSTONE)) {
                    int quantia = 0;
                    for (int i = 0; i < player.getInventory().getSize(); i++) {
                        if (player.getInventory().getItem(i) != null) {
                            if (player.getInventory().getItem(i).getType().equals(Material.REDSTONE)) {
                                if (quantia == 0) {
                                    quantia = player.getInventory().getItem(i).getAmount();
                                } else {
                                    quantia = quantia + player.getInventory().getItem(i).getAmount();
                                }
                            } else {
                                continue;
                            }
                        } else {
                            continue;
                        }
                    }

                    boolean finish = false;
                    int block = 0;
                    int remove = 0;

                    int y = 0;
                    for (int x = 0; x <= y; x++) {
                        if (quantia >= 9) {
                            block++;
                            quantia = quantia - 9;
                            if (remove == 0) {
                                remove = 9;
                            } else {
                                remove = remove + 9;
                            }
                            y++;
                        } else {
                            y = -1;
                        }
                    }

                    if (block != 0 && remove != 0) {
                        RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.REDSTONE), remove);
                        final HashMap<Integer, ItemStack> nope2 = player.getInventory().addItem(new ItemStack(Material.REDSTONE_BLOCK, block));
                        for (final Entry<Integer, ItemStack> entry : nope2.entrySet()) {
                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                        }

                        player.sendMessage(ChatColor.GREEN + " * Blocos compactados!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você não contém ferros o suficiente para efetuar este comando!");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                    finish = false;
                    block = 0;
                    remove = 0;
                    quantia = 0;
                } else if (player.getItemInHand() != null && !player.getItemInHand().getType().equals(Material.AIR) && player.getItemInHand().getType().equals(Material.LAPIS_LAZULI)) {
                    int quantia = 0;
                    for (int i = 0; i < player.getInventory().getSize(); i++) {
                        if (player.getInventory().getItem(i) != null) {
                            if (player.getInventory().getItem(i).getType().equals(Material.LAPIS_LAZULI)) {
                                if (quantia == 0) {
                                    quantia = player.getInventory().getItem(i).getAmount();
                                } else {
                                    quantia = quantia + player.getInventory().getItem(i).getAmount();
                                }
                            } else {
                                continue;
                            }
                        } else {
                            continue;
                        }
                    }

                    boolean finish = false;
                    int block = 0;
                    int remove = 0;

                    int y = 0;
                    for (int x = 0; x <= y; x++) {
                        if (quantia >= 9) {
                            block++;
                            quantia = quantia - 9;
                            if (remove == 0) {
                                remove = 9;
                            } else {
                                remove = remove + 9;
                            }
                            y++;
                        } else {
                            y = -1;
                        }
                    }

                    if (block != 0 && remove != 0) {
                        RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.LAPIS_LAZULI), remove);
                        final HashMap<Integer, ItemStack> nope2 = player.getInventory().addItem(new ItemStack(Material.LAPIS_BLOCK, block));
                        for (final Entry<Integer, ItemStack> entry : nope2.entrySet()) {
                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                        }

                        player.sendMessage(ChatColor.GREEN + " * Blocos compactados!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você não contém ferros o suficiente para efetuar este comando!");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                    finish = false;
                    block = 0;
                    remove = 0;
                    quantia = 0;
                } else if (player.getItemInHand() != null && !player.getItemInHand().getType().equals(Material.AIR) && player.getItemInHand().getType().equals(Material.CHARCOAL)) {
                    int quantia = 0;
                    for (int i = 0; i < player.getInventory().getSize(); i++) {
                        if (player.getInventory().getItem(i) != null) {
                            if (player.getInventory().getItem(i).getType().equals(Material.CHARCOAL)) {
                                if (quantia == 0) {
                                    quantia = player.getInventory().getItem(i).getAmount();
                                } else {
                                    quantia = quantia + player.getInventory().getItem(i).getAmount();
                                }
                            } else {
                                continue;
                            }
                        } else {
                            continue;
                        }
                    }

                    boolean finish = false;
                    int block = 0;
                    int remove = 0;

                    int y = 0;
                    for (int x = 0; x <= y; x++) {
                        if (quantia >= 9) {
                            block++;
                            quantia = quantia - 9;
                            if (remove == 0) {
                                remove = 9;
                            } else {
                                remove = remove + 9;
                            }
                            y++;
                        } else {
                            y = -1;
                        }
                    }

                    if (block != 0 && remove != 0) {
                        RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.CHARCOAL), remove);
                        final HashMap<Integer, ItemStack> nope2 = player.getInventory().addItem(new ItemStack(Material.COAL_BLOCK, block));
                        for (final Entry<Integer, ItemStack> entry : nope2.entrySet()) {
                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                        }

                        player.sendMessage(ChatColor.GREEN + " * Blocos compactados!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você não contém ferros o suficiente para efetuar este comando!");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                    finish = false;
                    block = 0;
                    remove = 0;
                    quantia = 0;
                } else if (player.getItemInHand() != null && !player.getItemInHand().getType().equals(Material.AIR) && player.getItemInHand().getType().equals(Material.DIAMOND)) {
                    int quantia = 0;
                    for (int i = 0; i < player.getInventory().getSize(); i++) {
                        if (player.getInventory().getItem(i) != null) {
                            if (player.getInventory().getItem(i).getType().equals(Material.DIAMOND)) {
                                if (quantia == 0) {
                                    quantia = player.getInventory().getItem(i).getAmount();
                                } else {
                                    quantia = quantia + player.getInventory().getItem(i).getAmount();
                                }
                            } else {
                                continue;
                            }
                        } else {
                            continue;
                        }
                    }

                    boolean finish = false;
                    int block = 0;
                    int remove = 0;

                    int y = 0;
                    for (int x = 0; x <= y; x++) {
                        if (quantia >= 9) {
                            block++;
                            quantia = quantia - 9;
                            if (remove == 0) {
                                remove = 9;
                            } else {
                                remove = remove + 9;
                            }
                            y++;
                        } else {
                            y = -1;
                        }
                    }

                    if (block != 0 && remove != 0) {
                        RemoveItemInv.removeItemsFromType(player.getInventory(), new ItemStack(Material.DIAMOND), remove);
                        final HashMap<Integer, ItemStack> nope2 = player.getInventory().addItem(new ItemStack(Material.DIAMOND_BLOCK, block));
                        for (final Entry<Integer, ItemStack> entry : nope2.entrySet()) {
                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                        }

                        player.sendMessage(ChatColor.GREEN + " * Blocos compactados!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você não contém ferros o suficiente para efetuar este comando!");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                    finish = false;
                    block = 0;
                    remove = 0;
                    quantia = 0;
                }
            } else {
                player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/compactar");
            }
        }
        return false;
    }
}
