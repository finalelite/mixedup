package com.mixedup.commands;

import com.mixedup.Main;
import com.mixedup.apis.AccountAPI;
import com.mixedup.apis.ConfigAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.hashs.Tpa;
import com.mixedup.utils.AlternateColor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandTpa implements CommandExecutor {

    private int task;

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("tpa")) {
            if (args.length == 1) {
                if (args[0].equals("aceitar")) {
                    if (Tpa.get(UUID) != null && Tpa.get(UUID).getStatus() == false && !Tpa.get(UUID).getUUIDtarget().equals("NULL")) {
                        player.sendMessage(ChatColor.GREEN + " * Pedido de teletransporte aceito. O player será teletransportado em 3 segundos.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);
                        Tpa.get(UUID).setStatus(true);

                        final Player target = Bukkit.getPlayerExact(AccountAPI.getNick(Tpa.get(UUID).getUUIDtarget()));
                        target.sendMessage(ChatColor.GREEN + " * Pedido de teletransporte aceito.");

                        target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);
                        target.teleport(player.getLocation());

                        if (target.getAllowFlight() == true) {
                            target.setAllowFlight(false);
                            if (ConfigAPI.getFlyStatus(target.getUniqueId().toString()) == true) {
                                ConfigAPI.updateFly(target.getUniqueId().toString(), false);
                            }
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você não contém nenhum pedido de teletransporte!");
                    }
                } else if (args[0].equals("negar")) {
                    if (Tpa.get(UUID) != null && Tpa.get(UUID).getStatus() == false && !Tpa.get(UUID).getUUIDtarget().equals("NULL")) {
                        player.sendMessage(ChatColor.GREEN + " * Pedido de teletransporte negado.");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0F, 1.0F);

                        final Player target = Bukkit.getPlayerExact(AccountAPI.getNick(Tpa.get(UUID).getUUIDtarget()));
                        if (target.isOnline()) {
                            target.sendMessage(ChatColor.RED + " * Pedido de teletransporte negado.");
                            target.playSound(target.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0F, 1.0F);
                        }

                        Tpa.get(UUID).setUUIDtarget("NULL");
                        Tpa.get(UUID).setStatus(true);
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você não contém nenhum pedido de teletransporte!");
                    }
                } else if (args[0].equals("habilitar")) {
                    if (ConfigAPI.getTpaStatus(UUID) == false) {
                        ConfigAPI.updateTpa(UUID, true);
                        player.sendMessage(ChatColor.GREEN + " * Tpa habilitado com sucesso.");
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, o vosso Tpa já esta habilitado!");
                    }
                } else if (args[0].equals("desabilitar")) {
                    if (ConfigAPI.getTpaStatus(UUID) == true) {
                        ConfigAPI.updateTpa(UUID, false);
                        player.sendMessage(ChatColor.GREEN + " * Tpa desabilitado com sucesso.");
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, o vosso Tpa já esta desabilitado!");
                    }
                } else {
                    final Player target = Bukkit.getPlayerExact(args[0]);

                    if (target == null) {
                        player.sendMessage(ChatColor.RED + "Ops, este player não existe ou não está online no momento.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        return false;
                    }

                    if (target.isOnline()) {
                        final String UUIDtarget = target.getUniqueId().toString();
                        if (ConfigAPI.getTpaStatus(UUIDtarget) == true) {
                            target.sendMessage(ChatColor.YELLOW + "\n \n * " + player.getName() + " lhe enviou um pedido de teletransporte.\n" + ChatColor.YELLOW + " * Utilize: " + ChatColor.GREEN + "/tpa aceitar" + ChatColor.YELLOW + " ou " + ChatColor.RED + "/tpa negar\n \n");
                            player.sendMessage(ChatColor.GREEN + " * Pedido de teletransporte enviado.");

                            if (Tpa.get(UUIDtarget) == null) {
                                new Tpa(UUIDtarget, UUID, false).insert();
                            } else {
                                Tpa.get(UUIDtarget).setUUIDtarget(UUID);
                                Tpa.get(UUIDtarget).setStatus(false);
                            }

                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    if (Tpa.get(UUIDtarget).getStatus() == false) {
                                        Tpa.get(UUIDtarget).setUUIDtarget("NULL");
                                        player.sendMessage(ChatColor.RED + " * Um pedido de teletransporte acabou de expirar.");
                                    }
                                }
                            }, 600L);
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, este player não deseja receber pedidos de teletransporte no momento.");
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, este player não está online no momento.");
                    }
                }
            } else {
                player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes á &ntpa&c:\n \n&7/tpa (nick)\n/tpa aceitar\n/tpa negar\n/tpa habilitar\n/tpa desabilitar\n \n"));
            }
        }
        return false;
    }
}
