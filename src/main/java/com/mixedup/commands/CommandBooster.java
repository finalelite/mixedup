package com.mixedup.commands;

import com.mixedup.apis.TagAPI;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class CommandBooster implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = player.getUniqueId().toString();

        // -/booster ceder (type) (tempo) (quantia)
        if (cmd.getName().equalsIgnoreCase("booster")) {
            if (TagAPI.getTag(UUID).equalsIgnoreCase("Master") || TagAPI.getTag(UUID).equalsIgnoreCase("Supervisor")) {
                if (args.length == 4) {
                    if (args[0].equalsIgnoreCase("ceder")) {
                        if (args[1].equalsIgnoreCase("vip") || args[1].equalsIgnoreCase("membro")) {
                            try {
                                final int quantia = Integer.valueOf(args[3]);
                                final int tempo = Integer.valueOf(args[2]);

                                final ItemStack booster = new ItemStack(Material.BLAZE_POWDER, quantia);
                                final ItemMeta meta = booster.getItemMeta();
                                meta.setDisplayName(ChatColor.GREEN + "Booster mcmmo");
                                final ArrayList<String> lore = new ArrayList<>();
                                lore.add(" ");
                                if (args[1].equalsIgnoreCase("vip")) {
                                    lore.add(ChatColor.WHITE + "Tipo: " + ChatColor.GRAY + "Vip");
                                }
                                if (args[1].equalsIgnoreCase("membro")) {
                                    lore.add(ChatColor.WHITE + "Tipo: " + ChatColor.GRAY + "Membro");
                                }
                                lore.add(ChatColor.WHITE + "Tempo: " + ChatColor.GRAY + tempo + " minutos");
                                lore.add(" ");
                                meta.setLore(lore);
                                booster.setItemMeta(meta);

                                player.sendMessage(ChatColor.GREEN + " * Booster cedido com sucesso!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                                final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(booster);
                                for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                    player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                                }
                            } catch (final NumberFormatException e) {
                                //--
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/booster ceder (tipo) (tempo) (quantia)");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/booster ceder (tipo) (tempo) (quantia)");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/booster ceder (tipo) (tempo) (quantia)");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage("§c* Você não tem permissão para executar esse comando.");
            }
        }

        return false;
    }
}
