package com.mixedup.commands;

import br.com.finalelite.pauloo27.api.commands.BaseCommand;
import com.mixedup.Main;
import lombok.val;

public class CommandScoreboard extends BaseCommand {
    public CommandScoreboard() {
        super("scoreboard");

        this.playerListener = cmd -> {
            final val result = Main.scoreboardManager.toggleScoreboardToPlayer(cmd.getSender());
            cmd.reply("&aScoreboard " + (result ? "ativada" : "desativada") + ".");
            return true;
        };
    }
}
