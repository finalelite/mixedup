package com.mixedup.commands;

import com.mixedup.apis.TagAPI;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class CommandAmuleto implements CommandExecutor {

    private static ItemStack getItem(final String args) {
        if (args.equalsIgnoreCase("1")) {
            final ItemStack frag1 = new ItemStack(Material.FIREWORK_STAR);
            final ItemMeta metaFrag1 = frag1.getItemMeta();
            metaFrag1.setDisplayName(ChatColor.GREEN + "Fragmento de amuleto");
            final ArrayList<String> lore = new ArrayList<>();
            lore.add(" ");
            lore.add(ChatColor.WHITE + "Compactado: " + ChatColor.GRAY + "Tipo 1");
            lore.add(" ");
            metaFrag1.setLore(lore);
            frag1.setItemMeta(metaFrag1);

            return frag1;
        } else if (args.equalsIgnoreCase("2")) {
            final ItemStack frag1 = new ItemStack(Material.PURPLE_DYE);
            final ItemMeta metaFrag1 = frag1.getItemMeta();
            metaFrag1.setDisplayName(ChatColor.GREEN + "Fragmento de amuleto");
            final ArrayList<String> lore = new ArrayList<>();
            lore.add(" ");
            lore.add(ChatColor.WHITE + "Compactado: " + ChatColor.GRAY + "Tipo 2");
            lore.add(" ");
            metaFrag1.setLore(lore);
            frag1.setItemMeta(metaFrag1);

            return CommandAmuleto.addGlow(frag1);
        } else if (args.equalsIgnoreCase("3")) {
            final ItemStack frag1 = new ItemStack(Material.ORANGE_DYE);
            final ItemMeta metaFrag1 = frag1.getItemMeta();
            metaFrag1.setDisplayName(ChatColor.GREEN + "Fragmento de amuleto");
            final ArrayList<String> lore = new ArrayList<>();
            lore.add(" ");
            lore.add(ChatColor.WHITE + "Compactado: " + ChatColor.GRAY + "Tipo 3");
            lore.add(" ");
            metaFrag1.setLore(lore);
            frag1.setItemMeta(metaFrag1);

            return CommandAmuleto.addGlow(frag1);
        } else if (args.equalsIgnoreCase("amuleto")) {
            final ItemStack amuleto = new ItemStack(Material.NAME_TAG);
            final ItemMeta metaAmulet = amuleto.getItemMeta();
            metaAmulet.setDisplayName(ChatColor.GOLD + "Amuleto wither");
            final ArrayList<String> lore = new ArrayList<>();
            lore.add(" ");
            lore.add(ChatColor.GRAY + "Este amuleto serve como proteção e também");
            lore.add(ChatColor.GRAY + "garante o sucesso ao spawnar um wither!");
            lore.add(" ");
            metaAmulet.setLore(lore);
            amuleto.setItemMeta(metaAmulet);

            return CommandAmuleto.addGlow(amuleto);
        } else {
            return null;
        }
    }

    public static ItemStack addGlow(final ItemStack item) {

        item.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
        final ItemMeta meta = item.getItemMeta();
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        item.setItemMeta(meta);

        return item;
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = player.getUniqueId().toString();

        // -/amuleto ceder (tipo) (quantia)
        if (cmd.getName().equalsIgnoreCase("amuleto")) {
            if (TagAPI.getTag(UUID).equalsIgnoreCase("Master") || TagAPI.getTag(UUID).equalsIgnoreCase("Supervisor")) {
                if (args.length == 3) {
                    if (args[0].equalsIgnoreCase("ceder")) {
                        final String[] types = {"1", "2", "3", "amuleto"};

                        if (Arrays.asList(types).contains(args[1])) {
                            try {
                                final int amount = Integer.valueOf(args[2]);
                                final ItemStack item = CommandAmuleto.getItem(args[1]);
                                item.setAmount(amount);

                                final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(item);
                                for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                    player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                                }
                                player.sendMessage(ChatColor.GREEN + " * Amuleto cedido com sucesso!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            } catch (final NumberFormatException e) {
                                player.sendMessage(ChatColor.RED + " * Ops, a quantia apresenta algum erro!");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, o tipo informado não existe!");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Comando correto: " + ChatColor.GRAY + "/amuleto ceder (tipo) (quantia)");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + " * Comando correto: " + ChatColor.GRAY + "/amuleto ceder (tipo) (quantia)");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage("§c* Você não tem permissão para executar esse comando.");
            }
        }
        return false;
    }
}
