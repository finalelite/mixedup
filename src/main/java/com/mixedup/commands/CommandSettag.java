package com.mixedup.commands;

import com.mixedup.Main;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.ServerAPI;
import com.mixedup.apis.TagAPI;
import com.mixedup.utils.AlternateColor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

public class CommandSettag implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        String uuid = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("tag")) {
            if (TagAPI.getTag(uuid).equals("Master") || TagAPI.getTag(uuid).equals("Supervisor")) {
                if (args.length < 2 || args.length > 3) {
                    player.sendMessage(ChatColor.RED + "Comandos referentes á Tag: " + "\n" + " " + "\n" + ChatColor.GRAY + "/tag setar (nick) (tag)" + "\n" + ChatColor.GRAY + "/tag remover (nick)" + "\n" + " ");
                    return false;
                }
                if (args.length == 2) {
                    if (args[0].equals("remover")) {
                        uuid = PlayerUUID.getUUID(args[1]);
                        final Player target = Bukkit.getPlayerExact(args[1]);
                        if (TagAPI.getTag(uuid) != null) {
                            TagAPI.updateTag(uuid, "Membro");
                            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + target.getName());
                            player.sendMessage(ChatColor.GREEN + "Tag de " + ChatColor.RED + args[1] + ChatColor.GREEN + " removida com sucesso!");
                            if (!player.equals(target) && target != null) {
                                target.sendMessage(ChatColor.GREEN + "Sua tag for removida e setada para membro por " + ChatColor.RED + player.getName() + ChatColor.GREEN + "!");
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, aparentemente este player não contém registros em nosso banco de dados!");
                            return false;
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Comandos referentes á Tag: " + "\n" + " " + "\n" + ChatColor.GRAY + "/tag setar (nick) (tag)" + "\n" + ChatColor.GRAY + "/tag remover (nick)" + "\n" + " ");
                        return false;
                    }
                }
                if (args.length == 3) {
                    if (args[0].equals("setar")) {
                        if (PlayerUUID.getUUID(args[1]) != null) {
                            uuid = PlayerUUID.getUUID(args[1]);

                            if (args[2].equals("Master")) {
                                TagAPI.updateTag(uuid, "Master");
                                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);

                                if (!player.getName().equals(args[1])) {
                                    if (uuid != null) {
                                        TagAPI.updateTag(uuid, "Master");
                                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);

                                        if (ServerAPI.getStaffOnlineExist(uuid) != null) {
                                            ServerAPI.updateStaffOnlineTag(uuid, "Master");
                                        }

                                        try {
                                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                                            final DataOutputStream out = new DataOutputStream(b);

                                            out.writeUTF("Message");
                                            out.writeUTF(args[1]);
                                            out.writeUTF(ChatColor.YELLOW + "Você acaba de receber a tag " + ChatColor.GOLD + "[GameMaster]" + ChatColor.YELLOW + " de " + AlternateColor.alternate("&e&n" + player.getName()));
                                            player.sendMessage(ChatColor.GREEN + "Tag " + ChatColor.GOLD + "[GameMaster]" + ChatColor.GREEN + " definida para " + ChatColor.RED + args[1] + ChatColor.GREEN + ".");

                                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                                        } catch (final Exception e) {
                                            player.sendMessage(ChatColor.RED + "Ops, este player aparentemente está offline no momento!");
                                        }
                                    } else {
                                        player.sendMessage(ChatColor.RED + "Ops, este player não está online no momento!");
                                        return false;
                                    }
                                } else {
                                    player.sendMessage(ChatColor.GREEN + "Sua tag foi definida para " + ChatColor.GOLD + "[GameMaster]" + ChatColor.GREEN + ".");
                                    TagAPI.updateTag(uuid, "Master");
                                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);
                                    return false;
                                }
                            }

                            if (args[2].equals("Supervisor")) {
                                TagAPI.updateTag(uuid, "Supervisor");
                                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);

                                if (!player.getName().equals(args[1])) {
                                    if (uuid != null) {
                                        TagAPI.updateTag(uuid, "Supervisor");
                                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);

                                        if (ServerAPI.getStaffOnlineExist(uuid) != null) {
                                            ServerAPI.updateStaffOnlineTag(uuid, "Supervisor");
                                        }

                                        try {
                                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                                            final DataOutputStream out = new DataOutputStream(b);

                                            out.writeUTF("Message");
                                            out.writeUTF(args[1]);
                                            out.writeUTF(ChatColor.YELLOW + "Você acaba de receber a tag " + ChatColor.DARK_RED + "[Supervisor]" + ChatColor.YELLOW + " de " + AlternateColor.alternate("&e&n" + player.getName()));
                                            player.sendMessage(ChatColor.GREEN + "Tag " + ChatColor.DARK_RED + "[Supervisor]" + ChatColor.GREEN + " definida para " + ChatColor.RED + args[1] + ChatColor.GREEN + ".");

                                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                                        } catch (final Exception e) {
                                            player.sendMessage(ChatColor.RED + "Ops, este player aparentemente está offline no momento!");
                                        }
                                    } else {
                                        player.sendMessage(ChatColor.RED + "Ops, este player não está online no momento!");
                                        return false;
                                    }
                                } else {
                                    player.sendMessage(ChatColor.GREEN + "Sua tag foi definida para " + ChatColor.DARK_RED + "[Supervisor]" + ChatColor.GREEN + ".");
                                    TagAPI.updateTag(uuid, "Supervisor");
                                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);
                                    return false;
                                }
                            }

                            if (args[2].equals("Admin")) {
                                TagAPI.updateTag(uuid, "Admin");
                                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);

                                if (!player.getName().equals(args[1])) {
                                    if (uuid != null) {
                                        TagAPI.updateTag(uuid, "Admin");
                                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);

                                        if (ServerAPI.getStaffOnlineExist(uuid) != null) {
                                            ServerAPI.updateStaffOnlineTag(uuid, "Admin");
                                        }

                                        try {
                                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                                            final DataOutputStream out = new DataOutputStream(b);

                                            out.writeUTF("Message");
                                            out.writeUTF(args[1]);
                                            out.writeUTF(ChatColor.YELLOW + "Você acaba de receber a tag " + ChatColor.RED + "[Admin]" + ChatColor.YELLOW + " de " + AlternateColor.alternate("&e&n" + player.getName()));
                                            player.sendMessage(ChatColor.GREEN + "Tag " + ChatColor.RED + "[Admin]" + ChatColor.GREEN + " definida para " + ChatColor.RED + args[1] + ChatColor.GREEN + ".");

                                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                                        } catch (final Exception e) {
                                            player.sendMessage(ChatColor.RED + "Ops, este player aparentemente está offline no momento!");
                                        }
                                    } else {
                                        player.sendMessage(ChatColor.RED + "Ops, este player não está online no momento!");
                                        return false;
                                    }
                                } else {
                                    player.sendMessage(ChatColor.GREEN + "Sua tag foi definida para " + ChatColor.RED + "[Admin]" + ChatColor.GREEN + ".");
                                    TagAPI.updateTag(uuid, "Admin");
                                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);
                                    return false;
                                }
                            }

                            if (args[2].equals("Moderador")) {
                                TagAPI.updateTag(uuid, "Moderador");
                                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);

                                if (!player.getName().equals(args[1])) {
                                    if (uuid != null) {
                                        TagAPI.updateTag(uuid, "Moderador");
                                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);

                                        if (ServerAPI.getStaffOnlineExist(uuid) != null) {
                                            ServerAPI.updateStaffOnlineTag(uuid, "Moderador");
                                        }

                                        try {
                                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                                            final DataOutputStream out = new DataOutputStream(b);

                                            out.writeUTF("Message");
                                            out.writeUTF(args[1]);
                                            out.writeUTF(ChatColor.YELLOW + "Você acaba de receber a tag " + ChatColor.DARK_GREEN + "[Moderador]" + ChatColor.YELLOW + " de " + AlternateColor.alternate("&e&n" + player.getName()));
                                            player.sendMessage(ChatColor.GREEN + "Tag " + ChatColor.DARK_GREEN + "[Moderador]" + ChatColor.GREEN + " definida para " + ChatColor.RED + args[1] + ChatColor.GREEN + ".");

                                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                                        } catch (final Exception e) {
                                            player.sendMessage(ChatColor.RED + "Ops, este player aparentemente está offline no momento!");
                                        }
                                    } else {
                                        player.sendMessage(ChatColor.RED + "Ops, este player não está online no momento!");
                                        return false;
                                    }
                                } else {
                                    player.sendMessage(ChatColor.GREEN + "Sua tag foi definida para " + ChatColor.DARK_GREEN + "[Moderador]" + ChatColor.GREEN + ".");
                                    TagAPI.updateTag(uuid, "Moderador");
                                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);
                                    return false;
                                }
                            }

                            if (args[2].equals("Suporte")) {
                                TagAPI.updateTag(uuid, "Suporte");
                                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);

                                if (!player.getName().equals(args[1])) {
                                    if (uuid != null) {
                                        TagAPI.updateTag(uuid, "Suporte");
                                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);

                                        if (ServerAPI.getStaffOnlineExist(uuid) != null) {
                                            ServerAPI.updateStaffOnlineTag(uuid, "Suporte");
                                        }

                                        try {
                                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                                            final DataOutputStream out = new DataOutputStream(b);

                                            out.writeUTF("Message");
                                            out.writeUTF(args[1]);
                                            out.writeUTF(ChatColor.YELLOW + "Você acaba de receber a tag " + ChatColor.GREEN + "[Suporte]" + ChatColor.YELLOW + " de " + AlternateColor.alternate("&e&n" + player.getName()));
                                            player.sendMessage(ChatColor.GREEN + "Tag " + ChatColor.GREEN + "[Suporte]" + ChatColor.GREEN + " definida para " + ChatColor.RED + args[1] + ChatColor.GREEN + ".");

                                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                                        } catch (final Exception e) {
                                            player.sendMessage(ChatColor.RED + "Ops, este player aparentemente está offline no momento!");
                                        }
                                    } else {
                                        player.sendMessage(ChatColor.RED + "Ops, este player não está online no momento!");
                                        return false;
                                    }
                                } else {
                                    player.sendMessage(ChatColor.GREEN + "Sua tag foi definida para " + ChatColor.GREEN + "[Suporte]" + ChatColor.GREEN + ".");
                                    TagAPI.updateTag(uuid, "Suporte");
                                    return false;
                                }
                            }

                            if (args[2].equals("Youtuber")) {
                                TagAPI.updateTag(uuid, "Youtuber");
                                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);

                                if (!player.getName().equals(args[1])) {
                                    if (uuid != null) {
                                        TagAPI.updateTag(uuid, "Youtuber");
                                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);

                                        try {
                                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                                            final DataOutputStream out = new DataOutputStream(b);

                                            out.writeUTF("Message");
                                            out.writeUTF(args[1]);
                                            out.writeUTF(ChatColor.YELLOW + "Você acaba de receber a tag " + ChatColor.RED + "[Youtuber]" + ChatColor.YELLOW + " de " + AlternateColor.alternate("&e&n" + player.getName()));
                                            player.sendMessage(ChatColor.GREEN + "Tag " + ChatColor.RED + "[Youtuber]" + ChatColor.GREEN + " definida para " + ChatColor.RED + args[1] + ChatColor.GREEN + ".");

                                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                                        } catch (final Exception e) {
                                            player.sendMessage(ChatColor.RED + "Ops, este player aparentemente está offline no momento!");
                                        }
                                    } else {
                                        player.sendMessage(ChatColor.RED + "Ops, este player não está online no momento!");
                                        return false;
                                    }
                                } else {
                                    player.sendMessage(ChatColor.GREEN + "Sua tag foi definida para " + ChatColor.RED + "[Youtuber]" + ChatColor.GREEN + ".");
                                    TagAPI.updateTag(uuid, "Youtuber");
                                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);
                                    return false;
                                }
                            }

                            if (args[2].equals("Vip")) {
                                TagAPI.updateTag(uuid, "Vip");
                                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);

                                if (!player.getName().equals(args[1])) {
                                    if (uuid != null) {
                                        TagAPI.updateTag(uuid, "Vip");
                                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);

                                        if (ServerAPI.getStaffOnlineExist(uuid) != null) {
                                            ServerAPI.updateStaffOnlineTag(uuid, "Vip");
                                        }

                                        try {
                                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                                            final DataOutputStream out = new DataOutputStream(b);

                                            out.writeUTF("Message");
                                            out.writeUTF(args[1]);
                                            out.writeUTF(ChatColor.YELLOW + "Você acaba de receber a tag " + ChatColor.YELLOW + "[Conde]" + ChatColor.YELLOW + " de " + AlternateColor.alternate("&e&n" + player.getName()));
                                            player.sendMessage(ChatColor.GREEN + "Tag " + ChatColor.YELLOW + "[Conde]" + ChatColor.GREEN + " definida para " + ChatColor.RED + args[1] + ChatColor.GREEN + ".");

                                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                                        } catch (final Exception e) {
                                            player.sendMessage(ChatColor.RED + "Ops, este player aparentemente está offline no momento!");
                                        }
                                    } else {
                                        player.sendMessage(ChatColor.RED + "Ops, este player não está online no momento!");
                                        return false;
                                    }
                                } else {
                                    player.sendMessage(ChatColor.GREEN + "Sua tag foi definida para " + ChatColor.YELLOW + "[Conde]" + ChatColor.GREEN + ".");
                                    TagAPI.updateTag(uuid, "Vip");
                                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);
                                    return false;
                                }

                                if (args[2].equals("Vip+")) {
                                    TagAPI.updateTag(uuid, "Vip+");
                                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);

                                    if (!player.getName().equals(args[1])) {
                                        if (uuid != null) {
                                            TagAPI.updateTag(uuid, "Vip+");
                                            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);

                                            if (ServerAPI.getStaffOnlineExist(uuid) != null) {
                                                ServerAPI.updateStaffOnlineTag(uuid, "Vip+");
                                            }

                                            try {
                                                final ByteArrayOutputStream b = new ByteArrayOutputStream();
                                                final DataOutputStream out = new DataOutputStream(b);

                                                out.writeUTF("Message");
                                                out.writeUTF(args[1]);
                                                out.writeUTF(ChatColor.YELLOW + "Você acaba de receber a tag " + ChatColor.AQUA + "[Lord]" + ChatColor.YELLOW + " de " + AlternateColor.alternate("&e&n" + player.getName()));
                                                player.sendMessage(ChatColor.GREEN + "Tag " + ChatColor.AQUA + "[Lord]" + ChatColor.GREEN + " definida para " + ChatColor.RED + args[1] + ChatColor.GREEN + ".");

                                                player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                                            } catch (final Exception e) {
                                                player.sendMessage(ChatColor.RED + "Ops, este player aparentemente está offline no momento!");
                                            }
                                        } else {
                                            player.sendMessage(ChatColor.RED + "Ops, este player não está online no momento!");
                                            return false;
                                        }
                                    } else {
                                        player.sendMessage(ChatColor.GREEN + "Sua tag foi definida para " + ChatColor.AQUA + "[Lord]" + ChatColor.GREEN + ".");
                                        TagAPI.updateTag(uuid, "Vip+");
                                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);
                                        return false;
                                    }
                                    if (args[2].equals("Vip++")) {
                                        TagAPI.updateTag(uuid, "Vip++");
                                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);

                                        if (!player.getName().equals(args[1])) {
                                            if (uuid != null) {
                                                TagAPI.updateTag(uuid, "Vip++");
                                                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);

                                                if (ServerAPI.getStaffOnlineExist(uuid) != null) {
                                                    ServerAPI.updateStaffOnlineTag(uuid, "Vip++");
                                                }

                                                try {
                                                    final ByteArrayOutputStream b = new ByteArrayOutputStream();
                                                    final DataOutputStream out = new DataOutputStream(b);

                                                    out.writeUTF("Message");
                                                    out.writeUTF(args[1]);
                                                    out.writeUTF(ChatColor.YELLOW + "Você acaba de receber a tag " + ChatColor.DARK_PURPLE + "[Titan]" + ChatColor.YELLOW + " de " + AlternateColor.alternate("&e&n" + player.getName()));
                                                    player.sendMessage(ChatColor.GREEN + "Tag " + ChatColor.AQUA + "[Titan]" + ChatColor.DARK_PURPLE + " definida para " + ChatColor.RED + args[1] + ChatColor.GREEN + ".");

                                                    player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                                                } catch (final Exception e) {
                                                    player.sendMessage(ChatColor.RED + "Ops, este player aparentemente está offline no momento!");
                                                }
                                            } else {
                                                player.sendMessage(ChatColor.RED + "Ops, este player não está online no momento!");
                                                return false;
                                            }
                                        } else {
                                            player.sendMessage(ChatColor.GREEN + "Sua tag foi definida para " + ChatColor.DARK_PURPLE + "[Titan]" + ChatColor.GREEN + ".");
                                            TagAPI.updateTag(uuid, "Vip++");
                                            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tagupdate " + args[1]);
                                            return false;
                                        }
                                    }
                                }
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, aparentemente este player não contém registros em nosso banco de dados!");
                            return false;
                        }
                    }
                }
            }
            return false;
        }
        return false;
    }
}
