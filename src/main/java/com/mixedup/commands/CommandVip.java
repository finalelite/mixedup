package com.mixedup.commands;

import br.com.finalelite.pauloo27.api.PauloAPI;
import com.mixedup.Main;
import com.mixedup.apis.ConfigAPI;
import com.mixedup.apis.HorarioAPI;
import com.mixedup.apis.RegionsSpawnAPI;
import com.mixedup.apis.TagAPI;
import com.mixedup.economy.CoinsAPI;
import com.mixedup.utils.AlternateColor;
import com.mixedup.vipAutentication.AuthAPI;
import com.mixedup.vipAutentication.VipData;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

public class CommandVip implements CommandExecutor {

    private static HashMap<String, VipData> cache = new HashMap<>();

    public static HashMap<String, VipData> getCache() {

        return cache;

    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player player = (Player) sender;
        final String UUID = player.getUniqueId().toString();

        // -/vip, /vip info, /vip setar (nick) (vip) (tempo)
        if (cmd.getName().equalsIgnoreCase("vip")) {
            if (!TagAPI.getTag(UUID).equalsIgnoreCase("Membro")) {
                if (args.length == 0) {
                    final String[] loc = RegionsSpawnAPI.getLocation("area-vip").split(":");
                    final Location location = new Location(Bukkit.getWorld(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), Float.parseFloat(loc[4]), Float.parseFloat(loc[5]));
                    player.sendMessage(ChatColor.GREEN + "\n * Teletransportando para Área vip\n  ");
                    player.setAllowFlight(true);
                    ConfigAPI.updateFly(UUID, true);
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.teleport(location);
                    return false;
                }

                if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("info")) {

                        if(PauloAPI.getInstance().getAccountInfo(player).getRole().isVIP()) {

                            if(!cache.containsKey(UUID)) cache.put(UUID, new VipData(AuthAPI.getDateEnds(UUID), AuthAPI.getEterno(UUID)));

                            player.sendMessage("\n " + ChatColor.RED + "Informações sobre o seu VIP: \n \n " +
                                    ChatColor.WHITE + " Jogador: " + ChatColor.GRAY + player.getName() + ChatColor.WHITE + "\n " +
                                    ChatColor.WHITE + " VIP: " + ChatColor.translateAlternateColorCodes('&', PauloAPI.getInstance().getAccountInfo(player).getRole().getColor() + PauloAPI.getInstance().getAccountInfo(player).getRole().getDisplayName()) + ChatColor.WHITE + "\n " +
                                    ChatColor.WHITE + " Duração: " + (cache.get(UUID).isEternal() ? ChatColor.RED + "eterno" : ChatColor.GREEN + HorarioAPI.getTime((cache.get(UUID).getEnd().getTime() / 1000) - (new Date().getTime() / 1000))) + "\n ");

                            return true;

                        } else {

                            player.sendMessage(ChatColor.RED + " * Whoops, você não é VIP.");
                            return true;

                        }

                    } else {
                        player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes a &nvip&c:\n \n&7 * /vip\n&7 * /vip info\n \n"));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else if (args.length == 4) {
                    final String tag = TagAPI.getTag(UUID);

                    //vip setar (nick) (vip) (tempo)
                    if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Supervisor")) {
                        if (args[0].equalsIgnoreCase("setar")) {
                            final Player target = Bukkit.getPlayer(args[1]);

                            if (target != null && target.isOnline()) {
                                if (args[2].equals("Vip") || args[2].equals("Vip+") || args[2].equals("Vip++") || args[2].equals("Vip+++")) {
                                    if (args[3].equalsIgnoreCase("1-mês") || args[3].equalsIgnoreCase("2-meses") || args[3].equalsIgnoreCase("eterno")) {
                                        if (!args[3].equalsIgnoreCase("eterno")) {
                                            AuthAPI.createIndo(target.getUniqueId().toString(), args[2], "MixedUP", true, null);
                                        } else {
                                            final java.util.Date date = new java.util.Date();
                                            int month = 0;
                                            if (args[3].equals("1-mês")) {
                                                month = 1;
                                            } else if (args[3].equals("2-meses")) {
                                                month = 2;
                                            }
                                            date.setMonth(date.getMonth() + month);
                                            AuthAPI.createIndo(target.getUniqueId().toString(), args[2], "MixedUP", false, new java.sql.Date(date.getTime()));
                                        }

                                        TagAPI.updateTag(target.getUniqueId().toString(), AuthAPI.getTagVip(target.getUniqueId().toString()));

                                        PauloAPI.getInstance().getAccountsCache().removeFromCache(target.getUniqueId().toString());
                                        if (Main.scoreboardManager.hasSquadScoreboard(target)) {
                                            Main.scoreboardManager.getSquadScoreboard().updateEntry(target, "group");
                                        } else {
                                            Main.scoreboardManager.getDefaultScoreboard().updateEntry(target, "group");
                                        }
                                        Main.scoreboardManager.getTablistManager().setPlayerGroup(target, Main.scoreboardManager.getTablistManager().getGroupIdByName(PauloAPI.getInstance().getAccountInfo(target).getRole().getDisplayName()));
                                        Main.scoreboardManager.updateTabList(target);

                                        if (TagAPI.getTag(target.getUniqueId().toString()).equalsIgnoreCase("Vip")) {
                                            CoinsAPI.updateCoins(target.getUniqueId().toString(), CoinsAPI.getCoins(target.getUniqueId().toString()) + 10000);
                                            Main.scoreboardManager.getSquadScoreboard().updateEntry(target, "coins");
                                        } else if (TagAPI.getTag(target.getUniqueId().toString()).equalsIgnoreCase("Vip+")) {
                                            CoinsAPI.updateCoins(target.getUniqueId().toString(), CoinsAPI.getCoins(target.getUniqueId().toString()) + 15000);
                                            Main.scoreboardManager.getSquadScoreboard().updateEntry(target, "coins");
                                        } else if (TagAPI.getTag(target.getUniqueId().toString()).equalsIgnoreCase("Vip++")) {
                                            CoinsAPI.updateCoins(target.getUniqueId().toString(), CoinsAPI.getCoins(target.getUniqueId().toString()) + 20000);
                                            Main.scoreboardManager.getSquadScoreboard().updateEntry(target, "coins");
                                        } else if (TagAPI.getTag(target.getUniqueId().toString()).equalsIgnoreCase("Vip+++")) {
                                            CoinsAPI.updateCoins(target.getUniqueId().toString(), CoinsAPI.getCoins(target.getUniqueId().toString()) + 25000);
                                            Main.scoreboardManager.getSquadScoreboard().updateEntry(target, "coins");
                                        }

                                        for (final Player targets : Bukkit.getOnlinePlayers()) {
                                            targets.playSound(targets.getLocation(), Sound.ENTITY_WITHER_DEATH, 1.0f, 1.0f);
                                            if (TagAPI.getTag(target.getUniqueId().toString()).equalsIgnoreCase("Vip")) {
                                                targets.sendTitle(ChatColor.YELLOW + target.getName(), ChatColor.GRAY + "Tornou-se " + ChatColor.YELLOW + "[Conde]", 20, 60, 20);
                                            } else if (TagAPI.getTag(target.getUniqueId().toString()).equalsIgnoreCase("Vip+")) {
                                                targets.sendTitle(ChatColor.AQUA + target.getName(), ChatColor.GRAY + "Tornou-se " + ChatColor.AQUA + "[Lord]", 20, 60, 20);
                                            } else if (TagAPI.getTag(target.getUniqueId().toString()).equalsIgnoreCase("Vip++")) {
                                                targets.sendTitle(ChatColor.DARK_RED + target.getName(), ChatColor.GRAY + "Tornou-se " + ChatColor.DARK_RED + "[Duque]", 20, 60, 20);
                                            } else if (TagAPI.getTag(target.getUniqueId().toString()).equalsIgnoreCase("Vip+++")) {
                                                targets.sendTitle(ChatColor.DARK_PURPLE + target.getName(), ChatColor.GRAY + "Tornou-se " + ChatColor.DARK_PURPLE + "[Titan]", 20, 60, 20);
                                                targets.getWorld().spawnEntity(targets.getLocation(), EntityType.FIREWORK);
                                            }
                                        }
                                    } else {
                                        player.sendMessage(ChatColor.RED + " * Ops, este tempo não existe ou não se enquadra com o mesmo!");
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    }
                                } else {
                                    player.sendMessage(ChatColor.RED + " * Ops, este VIP não existe!");
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, este player não se encontra online no momento!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        }
                    } else {
                        player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes a &nvip&c:\n \n&7 * /vip\n&7 * /vip info\n \n"));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes a &nvip&c:\n \n&7 * /vip\n&7 * /vip info\n \n"));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
