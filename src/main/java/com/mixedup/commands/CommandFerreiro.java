package com.mixedup.commands;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import com.mixedup.npc.NPCFunctions;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class CommandFerreiro implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("ferreiro")) {
            final String tag = TagAPI.getTag(UUID);
            final String[] perm = {"Vip", "Vip+", "Vip++", "Vip+++", "Master", "Supervisor", "Admin", "Moderador", "Suporte", "Youtuber"};

            if (Arrays.asList(perm).contains(tag)) {
                NPCFunctions.invFerreiro(player);
                player.sendMessage(ChatColor.RED + " * Ferreiro aberto.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            } else {
                player.sendMessage(ChatColor.GRAY + " * Adquira vantagens como estas adquirindo " + ChatColor.GREEN + "VIP" + ChatColor.GRAY + " em nosso site." + "\n" + ChatColor.YELLOW + " * www.finalelite.com.br");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
        return false;
    }
}
