package com.mixedup.commands;

import com.mixedup.Main;
import com.mixedup.apis.PlayerUUID;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

public class CommandLobby implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("lobby")) {
            try {
                final ByteArrayOutputStream b = new ByteArrayOutputStream();
                final DataOutputStream out = new DataOutputStream(b);

                out.writeUTF("Connect");
                out.writeUTF("Hub1");

                player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
            } catch (final Exception e) {
                player.sendMessage(ChatColor.RED + "Ops, algo ocorreu de errado!");
            }
        }

        return false;
    }
}
