package com.mixedup.commands;

import com.mixedup.apis.TagAPI;
import com.mixedup.managers.InventoryBanqueiro;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandBanqueiro implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = player.getUniqueId().toString();

        if (cmd.getName().equalsIgnoreCase("banqueiro")) {
            if (!TagAPI.getTag(UUID).equalsIgnoreCase("Membro")) {
                player.openInventory(InventoryBanqueiro.getInventory(UUID));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            } else {
                player.sendMessage(ChatColor.GRAY + " * Adquira vantagens como estas adquirindo " + ChatColor.GREEN + "VIP" + ChatColor.GRAY + " em nosso site." + "\n" + ChatColor.YELLOW + " * www.finalelite.com.br");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
        return false;
    }
}
