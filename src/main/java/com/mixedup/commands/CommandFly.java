package com.mixedup.commands;

import com.mixedup.apis.ConfigAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import com.mixedup.listeners.ArenaEvents;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandFly implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("fly") || cmd.getName().equalsIgnoreCase("voar")) {
            if (args.length > 0) {
                player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/fly ou /voar");
                return false;
            }
            if (args.length == 0) {
                if (!TagAPI.getTag(UUID).equals("Membro")) {
                    if (ArenaEvents.playerInArea(player.getLocation()) == true) {
                        player.sendMessage(ChatColor.RED + " * Ops, proibido habilitar fly na arena!");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        return false;
                    } else if (ConfigAPI.getFlyStatus(UUID) == true) {
                        player.setAllowFlight(false);
                        player.sendMessage(ChatColor.GREEN + " * Fly desabilitado com sucesso.");
                        ConfigAPI.updateFly(UUID, false);
                    } else {
                        if (player.getLocation().getWorld().getName().equalsIgnoreCase("spawn")) {
                            player.setAllowFlight(true);
                            player.sendMessage(ChatColor.GREEN + " * Fly habilitado com sucesso.");
                            ConfigAPI.updateFly(UUID, true);
                        } else {
                            final String tag = TagAPI.getTag(player.getUniqueId().toString());
                            if (tag != null && !(tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Moderador") || tag.equalsIgnoreCase("Admin"))) {
                                player.sendMessage(ChatColor.RED + " * Ops, voar é apenas permito dentro do spawn/area-vip!");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            } else {
                                player.setAllowFlight(true);
                                player.sendMessage(ChatColor.GREEN + " * Fly habilitado com sucesso.");
                                ConfigAPI.updateFly(UUID, true);
                            }
                        }
                    }
                } else {
                    player.sendMessage(ChatColor.GRAY + " * Adquira vantagens como estas adquirindo " + ChatColor.GREEN + "VIP" + ChatColor.GRAY + " em nosso site." + "\n" + ChatColor.YELLOW + " * www.finalelite.com.br");
                }
            }
        }
        return false;
    }
}
