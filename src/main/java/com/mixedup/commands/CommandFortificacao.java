package com.mixedup.commands;

import com.mixedup.apis.TagAPI;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class CommandFortificacao implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = player.getUniqueId().toString();

        // -/fortificacao ceder (type) (quantia)
        if (cmd.getName().equalsIgnoreCase("fortificacao")) {
            if (TagAPI.getTag(UUID).equalsIgnoreCase("Master") || TagAPI.getTag(UUID).equalsIgnoreCase("Supervisor")) {
                if (args.length == 3) {
                    if (args[0].equalsIgnoreCase("ceder")) {
                        if (args[1].equalsIgnoreCase("1") || args[1].equalsIgnoreCase("2")) {
                            try {
                                final int quantia = Integer.valueOf(args[2]);

                                final ItemStack end = new ItemStack(Material.END_STONE, quantia);
                                final ItemMeta meta6 = end.getItemMeta();
                                meta6.setDisplayName(ChatColor.GREEN + "Bloco de fortificação");
                                final ArrayList<String> lore5 = new ArrayList<>();
                                lore5.add(" ");
                                lore5.add(ChatColor.GRAY + "Defesa: " + ChatColor.RED + "4/10");
                                lore5.add(" ");
                                meta6.setLore(lore5);
                                end.setItemMeta(meta6);

                                final ItemStack bedrock = new ItemStack(Material.SMOOTH_STONE, quantia);
                                final ItemMeta meta7 = bedrock.getItemMeta();
                                meta7.setDisplayName(ChatColor.GREEN + "Bloco de fortificação");
                                final ArrayList<String> lore7 = new ArrayList<>();
                                lore7.add(" ");
                                lore7.add(ChatColor.GRAY + "Defesa: " + ChatColor.GREEN + "10/10");
                                lore7.add(" ");
                                meta7.setLore(lore7);
                                bedrock.setItemMeta(meta7);

                                ItemStack item = null;
                                if (args[1].equalsIgnoreCase("1")) {
                                    item = end;
                                } else if (args[1].equalsIgnoreCase("2")) {
                                    item = bedrock;
                                }

                                player.sendMessage(ChatColor.GREEN + " * Bloco de fortificação cedido com sucesso!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                                final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(item);
                                for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                    player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                                }
                            } catch (final NumberFormatException e) {
                                //--
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/fortificacao ceder (tipo) (quantia)");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/fortificacao ceder (tipo) (quantia)");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/fortificacao ceder (tipo) (quantia)");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }

        return false;
    }
}
