package com.mixedup.commands;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandHeal implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("heal")) {
            if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor") || TagAPI.getTag(UUID).equals("Admin")) {
                if (args.length == 0) {
                    player.setHealth(20);
                    player.sendMessage(ChatColor.GREEN + " * Sua vida foi regenerado com sucesso.");
                } else if (args.length == 1) {
                    final Player target = Bukkit.getPlayerExact(args[0]);

                    if (target.isOnline()) {
                        target.setHealth(20);
                        player.sendMessage(ChatColor.GREEN + " * Você acabou de regenerar a vida de " + args[0] + ".");
                        target.sendMessage(ChatColor.GREEN + " * Sua vida foi regenerada por " + player.getName() + ".");
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, este player não está online no momento.");
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/heal");
                }
            }
        }
        return false;
    }
}
