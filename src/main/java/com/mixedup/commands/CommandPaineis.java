package com.mixedup.commands;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CommandPaineis implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("painel")) {
            if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                if (args.length == 3) {
                    if (args[0].equals("ceder")) {
                        if (args[1].equals("100")) {
                            try {
                                final int quantia = Integer.valueOf(args[2]);

                                final ItemStack painel1 = new ItemStack(Material.DAYLIGHT_DETECTOR, quantia);
                                final ItemMeta meta1 = painel1.getItemMeta();
                                meta1.setDisplayName(ChatColor.YELLOW + "Painel solar");
                                final List<String> lore1 = new ArrayList<>();
                                lore1.add(" ");
                                lore1.add(ChatColor.DARK_GRAY + "Modelo: " + ChatColor.RED + "100Wp");
                                lore1.add(" ");
                                meta1.setLore(lore1);
                                painel1.setItemMeta(meta1);

                                final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(painel1);
                                for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                    player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                                }
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.sendMessage(ChatColor.GREEN + " * Foram cedidos " + quantia + " paineis solares de " + args[1] + "Wp.");
                            } catch (final NumberFormatException e) {
                                player.sendMessage(ChatColor.RED + " * Ops, quantia informada contém algum erro.");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else if (args[1].equals("200")) {
                            try {
                                final int quantia = Integer.valueOf(args[2]);

                                final ItemStack painel1 = new ItemStack(Material.DAYLIGHT_DETECTOR, quantia);
                                final ItemMeta meta1 = painel1.getItemMeta();
                                meta1.setDisplayName(ChatColor.YELLOW + "Painel solar");
                                final List<String> lore1 = new ArrayList<>();
                                lore1.add(" ");
                                lore1.add(ChatColor.DARK_GRAY + "Modelo: " + ChatColor.RED + "200Wp");
                                lore1.add(" ");
                                meta1.setLore(lore1);
                                painel1.setItemMeta(meta1);

                                final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(painel1);
                                for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                    player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                                }
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.sendMessage(ChatColor.GREEN + " * Foram cedidos " + quantia + " paineis solares de " + args[1] + "Wp.");
                            } catch (final NumberFormatException e) {
                                player.sendMessage(ChatColor.RED + " * Ops, quantia informada contém algum erro.");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, tipo informado contém algum erro.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/painel ceder (tipo) (quantia)");
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/painel ceder (tipo) (quantia)");
                }
            } else {
                player.sendMessage("§c* Você não tem permissão para executar esse comando.");
            }
        }
        return false;
    }
}
