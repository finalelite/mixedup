package com.mixedup.commands;

import com.mixedup.Main;
import com.mixedup.apis.TagAPI;
import lombok.val;
import lombok.var;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandRestart implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String lb, final String[] args) {
        var canUse = true;

        if (sender instanceof Player) {
            final String uuid = ((Player) sender).getUniqueId().toString();

            canUse = TagAPI.getTag(uuid) != null && (TagAPI.getTag(uuid).equalsIgnoreCase("Master") ||
                    TagAPI.getTag(uuid).equalsIgnoreCase("Supervisor"));
        }
        if (canUse) {

            if (args.length == 0) {
                sender.sendMessage(ChatColor.GREEN + " * Iniciando temporizador de reinicio...");
                Main.rUtils.restart();
            } else {
                final val action = args[0];

                if (action.equalsIgnoreCase("help") || action.equalsIgnoreCase("ajuda")) {
                    sender.sendMessage(ChatColor.YELLOW + "/reiniciar - Inicia o temporizador.");
                    sender.sendMessage(ChatColor.YELLOW + "/reiniciar on/off- Ativa/Desativa o reinicio automático " +
                            "(toda vez que iniciado ele é ativado, sendo assim, não se aplica em restarts futuros).");
                    sender.sendMessage(ChatColor.YELLOW + "/reiniciar cancelar- Cancela o reinicio.");
                    return true;
                } else if (action.equalsIgnoreCase("on")) {
                    Main.rUtils.setAutoRestart(true);
                    sender.sendMessage(ChatColor.GREEN + "Ativado.");
                    return true;
                } else if (action.equalsIgnoreCase("off")) {
                    Main.rUtils.setAutoRestart(false);
                    sender.sendMessage(ChatColor.RED + "Desativado.");
                    return true;
                } else if (action.equalsIgnoreCase("cancelar")) {
                    if (Main.rUtils.isRestarting()) {
                        Main.rUtils.cancel();
                        sender.sendMessage(ChatColor.GREEN + "Cancelado.");
                        return true;
                    } else {
                        sender.sendMessage(ChatColor.RED + "Nada para cancelar.");
                    }
                }

            }
        } else {
            sender.sendMessage(ChatColor.RED + " * Você não tem permissão para executar este comando.");
        }

        return false;
    }

}
