package com.mixedup.commands;

import br.com.finalelite.pauloo27.api.PauloAPI;
import com.mixedup.Main;
import com.mixedup.apis.GuildaAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.RegionsSpawnAPI;
import com.mixedup.apis.TagAPI;
import com.mixedup.hashs.GuildaSelectProv;
import com.mixedup.utils.AlternateColor;
import lombok.val;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

public class CommandsGuilda implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        // -/guilda setar <nick> <guilda>
        if (cmd.getName().equalsIgnoreCase("guilda")) {
            if (args.length == 1) {
                if (args[0].equalsIgnoreCase("info")) {
                    return false;
                }
            }

            final String tag = TagAPI.getTag(UUID);
            if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Supervisor")) {
                if (args.length == 3) {
                    if (args[0].equalsIgnoreCase("setar")) {
                        final Player target = Bukkit.getPlayerExact(args[1]);

                        if (target != null && target.isOnline()) {
                            final String[] list = {"nobre", "ancia", "sanguinaria"};

                            if (Arrays.asList(list).contains(args[2])) {
                                GuildaAPI.updateGuilda(target.getUniqueId().toString(), args[2]);
                                player.sendMessage(ChatColor.GREEN + " * Guilda atualizada com sucesso!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                final val sb = Main.scoreboardManager;
                                if (!sb.hasSquadScoreboard(player)) {
                                    sb.getDefaultScoreboard().updateEntry(player, "guild");
                                    sb.getDefaultScoreboard().updateEntry(player, "guild_rank");
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, defina uma guilda existente!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, este player não se encontra online!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/guilda setar (nick) (guilda)");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/guilda setar (nick) (guilda)");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage("§c* Você não tem permissão para executar esse comando.");
            }
        }

        if (cmd.getName().equalsIgnoreCase("guildasanguinariaconfirmar")) {
            if (GuildaAPI.getGuilda(UUID) == null) {
                if (GuildaSelectProv.get(UUID).getStatus() == true) {
                    GuildaAPI.setGuilda(UUID, "sanguinaria");
                    player.sendMessage(AlternateColor.alternate("\n \n&a * Você acabou de se tornar membro da Guilda Sanguinaria.\n&7 * Para ter mais informações sobre sua Guilda utilize /guilda info\n "));

                    final String[] region = RegionsSpawnAPI.getLocation("spawn").split(":");
                    final Location location = new Location(Bukkit.getWorld(region[0]), Double.parseDouble(region[1]), Double.parseDouble(region[2]), Double.parseDouble(region[3]), Float.parseFloat(region[4]), Float.parseFloat(region[5]));
                    player.teleport(location);
                    player.getInventory().setChestplate(new ItemStack(Material.CHAINMAIL_CHESTPLATE));
                    player.getInventory().setHelmet(new ItemStack(Material.CHAINMAIL_HELMET));
                    player.getInventory().setLeggings(new ItemStack(Material.CHAINMAIL_LEGGINGS));
                    player.getInventory().setBoots(new ItemStack(Material.CHAINMAIL_BOOTS));
                    player.getInventory().addItem(new ItemStack(Material.BREAD, 5));
                    final val sb = Main.scoreboardManager;
                    PauloAPI.getInstance().getAccountsCache().removeFromCache(player);
                    if (!sb.hasSquadScoreboard(player)) {
                        sb.getDefaultScoreboard().updateEntry(player, "guild");
                        sb.getDefaultScoreboard().updateEntry(player, "guild_rank");
                    }
                    for (final Player target : Bukkit.getOnlinePlayers()) {
                        target.showPlayer(Main.plugin, player);
                        player.showPlayer(Main.plugin, target);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, este pedido expirou. Por favor selecione novamente a Guilda desejada e clique em confirmar.");
                }
            }
        }

        if (cmd.getName().equalsIgnoreCase("guildaanciaconfirmar")) {
            if (GuildaAPI.getGuilda(UUID) == null) {
                if (GuildaSelectProv.get(UUID).getStatus() == true) {
                    GuildaAPI.setGuilda(UUID, "ancia");
                    player.sendMessage(AlternateColor.alternate("\n \n&a * Você acabou de se tornar membro da Guilda Anciã.\n&7 * Para ter mais informações sobre sua Guilda utilize /guilda info\n "));

                    final String[] region = RegionsSpawnAPI.getLocation("spawn").split(":");
                    final Location location = new Location(Bukkit.getWorld(region[0]), Double.parseDouble(region[1]), Double.parseDouble(region[2]), Double.parseDouble(region[3]), Float.parseFloat(region[4]), Float.parseFloat(region[5]));
                    player.teleport(location);
                    player.getInventory().setChestplate(new ItemStack(Material.CHAINMAIL_CHESTPLATE));
                    player.getInventory().setHelmet(new ItemStack(Material.CHAINMAIL_HELMET));
                    player.getInventory().setLeggings(new ItemStack(Material.CHAINMAIL_LEGGINGS));
                    player.getInventory().setBoots(new ItemStack(Material.CHAINMAIL_BOOTS));
                    player.getInventory().addItem(new ItemStack(Material.BREAD, 5));
                    final val sb = Main.scoreboardManager;
                    PauloAPI.getInstance().getAccountsCache().removeFromCache(player);
                    if (!sb.hasSquadScoreboard(player)) {
                        sb.getDefaultScoreboard().updateEntry(player, "guild");
                        sb.getDefaultScoreboard().updateEntry(player, "guild_rank");
                    }
                    for (final Player target : Bukkit.getOnlinePlayers()) {
                        target.showPlayer(Main.plugin, player);
                        player.showPlayer(Main.plugin, target);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, este pedido expirou. Por favor selecione novamente a Guilda desejada e clique em confirmar.");
                }
            }
        }

        if (cmd.getName().equalsIgnoreCase("guildanobreconfirmar")) {
            if (GuildaAPI.getGuilda(UUID) == null) {
                if (GuildaSelectProv.get(UUID).getStatus() == true) {
                    GuildaAPI.setGuilda(UUID, "nobre");
                    player.sendMessage(AlternateColor.alternate("\n \n&a * Você acabou de se tornar membro da Guilda Nobre.\n&7 * Para ter mais informações sobre sua Guilda utilize /guilda info\n "));

                    final String[] region = RegionsSpawnAPI.getLocation("spawn").split(":");
                    final Location location = new Location(Bukkit.getWorld(region[0]), Double.parseDouble(region[1]), Double.parseDouble(region[2]), Double.parseDouble(region[3]), Float.parseFloat(region[4]), Float.parseFloat(region[5]));
                    player.teleport(location);
                    player.getInventory().setChestplate(new ItemStack(Material.CHAINMAIL_CHESTPLATE));
                    player.getInventory().setHelmet(new ItemStack(Material.CHAINMAIL_HELMET));
                    player.getInventory().setLeggings(new ItemStack(Material.CHAINMAIL_LEGGINGS));
                    player.getInventory().setBoots(new ItemStack(Material.CHAINMAIL_BOOTS));
                    player.getInventory().addItem(new ItemStack(Material.BREAD, 5));
                    final val sb = Main.scoreboardManager;
                    PauloAPI.getInstance().getAccountsCache().removeFromCache(player);
                    if (!sb.hasSquadScoreboard(player)) {
                        sb.getDefaultScoreboard().updateEntry(player, "guild");
                        sb.getDefaultScoreboard().updateEntry(player, "guild_rank");
                    }
                    for (final Player target : Bukkit.getOnlinePlayers()) {
                        target.showPlayer(Main.plugin, player);
                        player.showPlayer(Main.plugin, target);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, este pedido expirou. Por favor selecione novamente a Guilda desejada e clique em confirmar.");
                }
            }
        }
        return false;
    }
}
