package com.mixedup.commands;

import com.mixedup.apis.TagAPI;
import com.mixedup.utils.AlternateColor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandAnuncio implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player player = (Player) sender;
        final String UUID = player.getUniqueId().toString();

        if (cmd.getName().equalsIgnoreCase("anunciar")) {
            if (TagAPI.getTag(UUID).equalsIgnoreCase("Master") || TagAPI.getTag(UUID).equalsIgnoreCase("Supervisor")) {
                if (args.length > 0) {
                    final String msg = String.join(" ", args);
                    for (final Player target : Bukkit.getOnlinePlayers()) {
                        target.sendMessage(AlternateColor.alternate(" \n&3Anúncio: &7") + ChatColor.translateAlternateColorCodes('&', msg) + "\n ");
                        target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/anunciar (msg)");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
