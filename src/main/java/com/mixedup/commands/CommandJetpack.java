package com.mixedup.commands;

import com.mixedup.apis.TagAPI;
import net.minecraft.server.v1_13_R2.NBTTagCompound;
import net.minecraft.server.v1_13_R2.NBTTagInt;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;

public class CommandJetpack implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();

        if (cmd.getName().equalsIgnoreCase("jetpack")) {
            if (args.length == 1) {
                if (args[0].equalsIgnoreCase("ceder")) {
                    String[] staff = {"Master", "Supervisor", "Admin"};

                    if (Arrays.asList(staff).contains(TagAPI.getTag(uuid))) {
                        ItemStack jetpack = new ItemStack(Material.GOLDEN_CHESTPLATE);
                        ItemMeta meta = jetpack.getItemMeta();
                        meta.setDisplayName(ChatColor.YELLOW + "Jetpack");
                        ArrayList<String> lore = new ArrayList<>();
                        lore.add(" ");
                        lore.add(ChatColor.GRAY + "Para ativar/desativar aperte SHIFT,");
                        lore.add(ChatColor.GRAY + "sobre o chão!");
                        lore.add(" ");
                        meta.setLore(lore);
                        jetpack.setItemMeta(meta);

                        net.minecraft.server.v1_13_R2.ItemStack nms = CraftItemStack.asNMSCopy(jetpack);
                        NBTTagCompound compound = (nms.hasTag()) ? nms.getTag() : new NBTTagCompound();
                        compound.set("energy", new NBTTagInt(600));
                        nms.setTag(compound);

                        player.getInventory().addItem(CraftItemStack.asBukkitCopy(nms));
                    }
                }
            }
        }
        return false;
    }
}
