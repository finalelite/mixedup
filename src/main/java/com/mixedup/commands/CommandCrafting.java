package com.mixedup.commands;

import com.mixedup.apis.TagAPI;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class CommandCrafting implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;

        if (cmd.getName().equalsIgnoreCase("crafting") || cmd.getName().equalsIgnoreCase("craftar") || cmd.getName().equalsIgnoreCase("craft")) {
            final String tag = TagAPI.getTag(player.getUniqueId().toString());
            final String[] perm = {"Vip", "Vip+", "Vip++", "Vip+++", "Master", "Supervisor", "Admin", "Moderador", "Suporte", "Youtuber"};

            if (Arrays.asList(perm).contains(tag)) {
                if (args.length == 0) {
                    player.openWorkbench(player.getLocation(), true);
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.sendMessage(ChatColor.GREEN + " * Crafting aberto.");
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/crafting, /craftar ou /craftar");
                }
            } else {
                player.sendMessage(ChatColor.GRAY + " * Adquira vantagens como estas adquirindo " + ChatColor.GREEN + "VIP" + ChatColor.GRAY + " em nosso site." + "\n" + ChatColor.YELLOW + " * www.finalelite.com.br");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
        return false;
    }
}
