package com.mixedup.commands;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.ServerAPI;
import com.mixedup.apis.TagAPI;
import com.mixedup.elytra.utils.RemoveArmorStands;
import com.mixedup.utils.RestartUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandStop implements CommandExecutor {

    int tick = 1;
    int tempo = 15;

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        if (cmd.getName().equalsIgnoreCase("desligar")) {
            boolean canUse = true;
            if (sender instanceof Player) {
                final Player player = (Player) sender;
                final String UUID = PlayerUUID.getUUID(player.getName());
                canUse = TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor");
            }
            if (canUse) {
                ServerAPI.updateOnlineStatus("Factions", false);
                Bukkit.broadcastMessage(ChatColor.RED + "\n \n * Servidor desligando!\n \n");
                RemoveArmorStands.removeAllArmorStand();
                for (final Player target : Bukkit.getOnlinePlayers()) {
                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }

                RestartUtils.movePlayersToHub1();


                if (Bukkit.getOnlinePlayers().size() != 0)
                    Bukkit.getOnlinePlayers().forEach(p -> p.kickPlayer("..."));

                Bukkit.shutdown();
            }
        }
        return false;
    }
}
