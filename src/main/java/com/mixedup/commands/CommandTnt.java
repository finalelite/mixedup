package com.mixedup.commands;

import com.mixedup.apis.TagAPI;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;
import java.util.Map.Entry;

public class CommandTnt implements CommandExecutor {

    private static ItemStack getItem(final String type) {
        if (type.equalsIgnoreCase("comum")) {
            final ItemStack tntComum = new ItemStack(Material.TNT, 48);
            final ItemMeta meta5 = tntComum.getItemMeta();
            meta5.setDisplayName(ChatColor.RED + "Explosivo comum");
            final List<String> lore4 = new ArrayList<>();
            lore4.add(" ");
            lore4.add(ChatColor.GRAY + "Dano: 1/5");
            lore4.add(" ");
            meta5.setLore(lore4);
            tntComum.setItemMeta(meta5);

            return tntComum;
        } else if (type.equalsIgnoreCase("nuclear")) {
            final ItemStack tntMedia = new ItemStack(Material.TNT, 48);
            final ItemMeta meta1 = tntMedia.getItemMeta();
            meta1.setDisplayName(ChatColor.RED + "Explosivo nuclear");
            final List<String> lore1 = new ArrayList<>();
            lore1.add(" ");
            lore1.add(ChatColor.GRAY + "Dano: 3/5");
            lore1.add(" ");
            meta1.setLore(lore1);
            tntMedia.setItemMeta(meta1);

            return tntMedia;
        } else if (type.equalsIgnoreCase("hidrogênio")) {
            final ItemStack tntH = new ItemStack(Material.TNT, 48);
            final ItemMeta meta2 = tntH.getItemMeta();
            meta2.setDisplayName(ChatColor.RED + "Explosivo de hidrogênio");
            final List<String> lore2 = new ArrayList<>();
            lore2.add(" ");
            lore2.add(ChatColor.GRAY + "Dano: 5/5");
            lore2.add(" ");
            meta2.setLore(lore2);
            tntH.setItemMeta(meta2);

            return CommandTnt.addGlow(tntH);
        } else if (type.equalsIgnoreCase("repulsão")) {
            final ItemStack tntRepulsao = new ItemStack(Material.TNT, 48);
            final ItemMeta meta3 = tntRepulsao.getItemMeta();
            meta3.setDisplayName(ChatColor.RED + "Explosivo de repulsão");
            tntRepulsao.setItemMeta(meta3);

            return CommandTnt.addGlow(tntRepulsao);
        } else return null;
    }

    public static ItemStack addGlow(final ItemStack item) {

        item.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 0);
        final ItemMeta meta = item.getItemMeta();
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        item.setItemMeta(meta);

        return item;
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = player.getUniqueId().toString();

        // -/tnt ceder (tipo) (quantia)
        if (cmd.getName().equalsIgnoreCase("tnt")) {
            if (TagAPI.getTag(UUID).equalsIgnoreCase("Master") || TagAPI.getTag(UUID).equalsIgnoreCase("Supervisor")) {
                if (args.length == 3) {
                    if (args[0].equalsIgnoreCase("ceder")) {
                        final String[] types = {"comum", "nuclear", "hidrogênio", "repulsão"};

                        if (Arrays.asList(types).contains(args[1])) {
                            try {
                                final int amount = Integer.valueOf(args[2]);
                                final ItemStack item = CommandTnt.getItem(args[1]);
                                item.setAmount(amount);

                                final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(item);
                                for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                    player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                                }
                                player.sendMessage(ChatColor.GREEN + " * TNT cedida com sucesso!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            } catch (final NumberFormatException e) {
                                player.sendMessage(ChatColor.RED + " * Ops, a quantia apresenta algum erro!");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, o tipo informado não existe!");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Comando correto: " + ChatColor.GRAY + "/tnt ceder (tipo) (quantia)");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + " * Comando correto: " + ChatColor.GRAY + "/tnt ceder (tipo) (quantia)");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }

        return false;
    }
}
