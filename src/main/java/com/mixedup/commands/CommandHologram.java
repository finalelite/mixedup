package com.mixedup.commands;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import com.mixedup.utils.AlternateColor;
import com.mixedup.utils.Hologram;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class CommandHologram implements CommandExecutor {

    private static ArmorStand getArmorStand(final Location location) {
        ArmorStand armorStand = null;

        final ArrayList<Entity> entitys = new ArrayList<>();
        for (final Entity entity : location.getNearbyEntities(2, 2, 2)) {
            if (entity.getType().equals(EntityType.ARMOR_STAND)) {
                armorStand = (ArmorStand) entity;
            }
        }

        Entity entityProv = null;
        for (int i = 1; i <= entitys.size(); i++) {
            if (i != entitys.size()) {
                if (entityProv == null) {
                    if (entitys.get(i - 1).getLocation().getY() < entitys.get(i).getLocation().getY()) {
                        entityProv = entitys.get(i - 1);
                    } else if (entitys.get(i - 1).getLocation().getY() > entitys.get(i).getLocation().getY()) {
                        entityProv = entitys.get(i);
                    }
                } else {
                    if (entityProv.getLocation().getY() < entitys.get(i).getLocation().getY()) {
                        entityProv = entitys.get(i);
                    }
                }
            }
        }

        return armorStand;
    }

    private static int getQuantityArmorStand(final Location location) {
        int quantia = 0;
        for (final Entity entity : location.getNearbyEntities(2, 2, 2)) {
            if (entity.getType().equals(EntityType.ARMOR_STAND)) {
                quantia++;
            }
        }

        return quantia;
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("holograma")) {
            if (TagAPI.getTag(UUID).equalsIgnoreCase("Master") || TagAPI.getTag(UUID).equalsIgnoreCase("Supervisor")) {
                if (args[0].equalsIgnoreCase("criar")) {
                    if (args.length >= 2) {
                        final StringBuilder build = new StringBuilder();
                        for (int i = 1; i < args.length; i++) {
                            build.append(" ");
                            build.append(args[i]);
                        }
                        final String msg = build.toString();
                        final String texto = ChatColor.translateAlternateColorCodes('&', msg);

                        final String[] text = {texto};
                        Hologram.createHologram(player.getLocation(), text);
                        player.sendMessage(ChatColor.GREEN + " * Holograma criado com sucesso.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes á &n/holograma&c:\n \n&7 - /holograma criar (texto)\n&7 - /holograma add (texto)\n&7 - /holograma remover\n \n"));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else if (args[0].equalsIgnoreCase("add")) {
                    if (args.length >= 2) {
                        final StringBuilder build = new StringBuilder();
                        for (int i = 1; i < args.length; i++) {
                            build.append(" ");
                            build.append(args[i]);
                        }
                        final String msg = build.toString();
                        final String texto = ChatColor.translateAlternateColorCodes('&', msg);

                        if (CommandHologram.getArmorStand(player.getLocation()) == null) {
                            player.sendMessage(ChatColor.RED + " * Ops, não há nenhum holograma por perto!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            return false;
                        }

                        final String[] text = {texto};
                        Hologram.createHologram(CommandHologram.getArmorStand(player.getLocation()).getLocation().add(0, -0.15, 0), text);
                        player.sendMessage(ChatColor.GREEN + " * Holograma atualizado com sucesso.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes á &n/holograma&c:\n \n&7 - /holograma criar (texto)\n&7 - /holograma add (texto)\n&7 - /holograma remover\n \n"));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else if (args[0].equalsIgnoreCase("remover")) {
                    if (args.length == 1) {
                        if (CommandHologram.getArmorStand(player.getLocation()) == null) {
                            player.sendMessage(ChatColor.RED + " * Ops, não há nenhum holograma por perto!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            return false;
                        }

                        for (int i = 1; i <= CommandHologram.getQuantityArmorStand(player.getLocation()); i++) {
                            CommandHologram.getArmorStand(player.getLocation()).remove();
                        }
                        player.sendMessage(ChatColor.GREEN + " * Holograma removido com sucesso.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes á &n/holograma&c:\n \n&7 - /holograma criar (texto)\n&7 - /holograma add (texto)\n&7 - /holograma remover\n \n"));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes á &n/holograma&c:\n \n&7 - /holograma criar (texto)\n&7 - /holograma add (texto)\n&7 - /holograma remover\n \n"));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
