package com.mixedup.commands;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandInvsee implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("invsee")) {
            if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor") || TagAPI.getTag(UUID).equals("Admin")) {
                if (args.length == 1) {
                    final Player target = Bukkit.getPlayerExact(args[0]);

                    if (target.isOnline()) {
                        player.openInventory(target.getInventory());
                        player.sendMessage(ChatColor.GREEN + " * Inventario de " + args[0] + " aberto com sucesso.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, este player não está online no momento.");
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/invsee (nick)");
                }
            }
        }
        return false;
    }
}
