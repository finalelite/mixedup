package com.mixedup.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerResourcePackStatusEvent;
import org.bukkit.event.player.PlayerResourcePackStatusEvent.Status;

public class CommandTexture implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player p = (Player) sender;

        if (p.getResourcePackStatus() != null) {

            if (!p.getResourcePackStatus().equals(Status.SUCCESSFULLY_LOADED)) {

                p.setResourcePack("https://www.dropbox.com/s/ljfh17bvjpgy0ac/mixedup_update.zip?dl=1", "626bf7f98b41f5b81ee7af0ad2b79f1b2b8d577d");

                if (p.getResourcePackStatus().equals(Status.SUCCESSFULLY_LOADED)) {

                    p.sendMessage("§e * Sua textura foi setada com sucesso.");

                } else {

                    if (p.getResourcePackStatus().equals(Status.DECLINED)) {

                        p.sendMessage(ChatColor.RED + " * Ops, o seu Minecraft está configurado para negar a textura do servidor, para modificar isso, vá na lista de servidores no seu Minecraft, selecione o nosso servidor, clique em editar e mude o \"Pacotes de recursos\" para Sim.");

                    } else {

                        p.sendMessage(ChatColor.RED + " * Um erro ocorreu ao tentar mudar a sua textura para a textura do servidor.");

                    }

                }

            } else {

                p.sendMessage(ChatColor.RED + " * Ops, Você já está usando a nossa textura.");

            }

        } else {


            p.setResourcePack("https://www.dropbox.com/s/ljfh17bvjpgy0ac/mixedup_update.zip?dl=1", "626bf7f98b41f5b81ee7af0ad2b79f1b2b8d577d");
            p.sendMessage(ChatColor.YELLOW + " * O seu Minecraft não enviou informações sobre a textura, tentamos setar a textura mesmo assim.");

        }

        return false;

    }
}
