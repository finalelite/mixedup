package com.mixedup.commands;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class CommandHead implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("head") || cmd.getName().equalsIgnoreCase("cabeça")) {
            if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor") || TagAPI.getTag(UUID).equals("Admin")) {
                if (args.length == 1) {
                    final ItemStack item = new ItemStack(Material.LEGACY_SKULL_ITEM, 1, (short) 3);
                    final SkullMeta meta = (SkullMeta) item.getItemMeta();
                    meta.setOwner(args[0]);
                    meta.setDisplayName(ChatColor.GREEN + "Cabeça de: " + args[0]);
                    item.setItemMeta(meta);

                    final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(item);
                    for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                        player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                    }

                    player.sendMessage(ChatColor.GREEN + " * Cabeça de " + args[0] + " pega com sucesso!");
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/head (nick) ou /cabeça (nick)");
                }
            }
        }
        return false;
    }
}
