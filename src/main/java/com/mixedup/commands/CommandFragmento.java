package com.mixedup.commands;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class CommandFragmento implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        // -/fragmento ceder (tipo) (quantia) (nível)
        if (cmd.getName().equalsIgnoreCase("fragmento")) {
            if (TagAPI.getTag(UUID).equalsIgnoreCase("Master") || TagAPI.getTag(UUID).equalsIgnoreCase("Supervisor")) {
                if (args.length == 4) {
                    if (args[0].equalsIgnoreCase("ceder")) {
                        if (args[1].equalsIgnoreCase("asa")) {
                            try {
                                final int quantia = Integer.valueOf(args[3]);
                                final int level = Integer.valueOf(args[2]);
                                final ItemStack frag = new ItemStack(Material.FEATHER, quantia);
                                final ItemMeta metaFrag = frag.getItemMeta();
                                metaFrag.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
                                final ArrayList<String> lore = new ArrayList<>();
                                lore.add(" ");
                                lore.add(ChatColor.GRAY + "Fragmento para asa, tipo: " + level);
                                lore.add(" ");
                                metaFrag.setLore(lore);
                                frag.setItemMeta(metaFrag);

                                final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(frag);
                                for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                    player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                                }
                                player.sendMessage(ChatColor.GREEN + " * Fragmento de asa cedido com sucesso.");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            } catch (final NumberFormatException e) {
                                player.sendMessage(ChatColor.RED + " * Ops, algum valor se encontra com erro.");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else if (args[1].equalsIgnoreCase("nivel")) {
                            try {
                                final int quantia = Integer.valueOf(args[3]);
                                final int level = Integer.valueOf(args[2]);
                                final ItemStack frag = new ItemStack(Material.PHANTOM_MEMBRANE, quantia);
                                final ItemMeta metaFrag = frag.getItemMeta();
                                metaFrag.setDisplayName(ChatColor.GREEN + "Fragmento de nível");
                                final ArrayList<String> lore = new ArrayList<>();
                                lore.add(" ");
                                lore.add(ChatColor.GRAY + "Fragmento de nível p/ asa, nível: " + level);
                                lore.add(" ");
                                metaFrag.setLore(lore);
                                frag.setItemMeta(metaFrag);

                                final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(frag);
                                for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                    player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                                }
                                player.sendMessage(ChatColor.GREEN + " * Fragmento de level cedido com sucesso.");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            } catch (final NumberFormatException e) {
                                player.sendMessage(ChatColor.RED + " * Ops, algum valor se encontra com erro.");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/fragmento ceder (tipo) (level) (quantia)");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/fragmento ceder (tipo) (level) (quantia)");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/fragmento ceder (tipo) (level) (quantia)");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
