package com.mixedup.commands;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class CommandMobspawn implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("mobspawner")) {
            if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                if (args.length == 3) {
                    if (args[0].equals("ceder")) {
                        final String[] mobs = {"Vaca", "Galinha", "Porco", "Ovelha", "Coelho", "Zumbi", "Esqueleto", "Aranha", "Creeper", "Enderman", "Golem_de_ferro", "Slime", "Blaze", "Homem_porco_zumbi", "Esqueleto_wither", "Bruxa", "Cubo_magma", "Ghast"};
                        for (int i = 1; i <= mobs.length; i++) {
                            if (args[1].equalsIgnoreCase(mobs[i - 1])) {
                                try {
                                    final int quantia = Integer.valueOf(args[2]);

                                    final ItemStack item = new ItemStack(Material.SPAWNER, quantia);
                                    final ItemMeta metaItem = item.getItemMeta();
                                    metaItem.setDisplayName(ChatColor.RED + "Spawner de " + mobs[i - 1].replaceAll("_", " "));
                                    item.setItemMeta(metaItem);

                                    final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(item);
                                    for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                        player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                                    }
                                    player.sendMessage(ChatColor.GREEN + " * Foram cedidos " + quantia + " Spawners de " + args[1] + ".");
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    i = mobs.length + 1;
                                    break;
                                } catch (final Exception e) {
                                    player.sendMessage(ChatColor.RED + "Ops, a quantidade informada está incorreta.");
                                }
                            } else {
                                if (i == mobs.length) {
                                    player.sendMessage(ChatColor.RED + "Ops, o mob informado não existe!");
                                }
                            }
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/mobspawn ceder (mob) (quantia)");
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/mobspawn ceder (mob) (quantia)");
                }
            } else {
                player.sendMessage("§c* Você não tem permissão para executar esse comando.");
            }
        }
        return false;
    }
}
