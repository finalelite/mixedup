package com.mixedup.commands;

import com.mixedup.apis.TagAPI;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkEffectMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class CommandBeacon implements CommandExecutor {

    private static ItemStack getItem(final String type) {
        if (type.equalsIgnoreCase("força")) {
            final ItemStack playerDrop = new ItemStack(Material.FIREWORK_STAR, 1);
            final ItemMeta meta = playerDrop.getItemMeta();
            meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
            meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
            meta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
            meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
            meta.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
            final ArrayList<String> lore = new ArrayList<>();
            lore.add(" ");
            lore.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Força");
            lore.add(" ");
            meta.setLore(lore);
            final FireworkEffectMeta metaFw = (FireworkEffectMeta) meta;
            final FireworkEffect aa = FireworkEffect.builder().withColor(Color.RED).build();
            metaFw.setEffect(aa);
            playerDrop.setItemMeta(metaFw);

            return playerDrop;
        } else if (type.equalsIgnoreCase("pressa")) {
            final ItemStack playerDrop1 = new ItemStack(Material.FIREWORK_STAR, 1);
            final ItemMeta meta1 = playerDrop1.getItemMeta();
            meta1.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
            meta1.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
            meta1.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            meta1.addItemFlags(ItemFlag.HIDE_DESTROYS);
            meta1.addItemFlags(ItemFlag.HIDE_PLACED_ON);
            meta1.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
            meta1.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
            final ArrayList<String> lore1 = new ArrayList<>();
            lore1.add(" ");
            lore1.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Pressa");
            lore1.add(" ");
            meta1.setLore(lore1);
            final FireworkEffectMeta metaFw1 = (FireworkEffectMeta) meta1;
            final FireworkEffect aa1 = FireworkEffect.builder().withColor(Color.NAVY).build();
            metaFw1.setEffect(aa1);
            playerDrop1.setItemMeta(metaFw1);

            return playerDrop1;
        } else if (type.equalsIgnoreCase("regeneração")) {
            final ItemStack playerDrop2 = new ItemStack(Material.FIREWORK_STAR, 1);
            final ItemMeta meta2 = playerDrop2.getItemMeta();
            meta2.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
            meta2.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
            meta2.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            meta2.addItemFlags(ItemFlag.HIDE_DESTROYS);
            meta2.addItemFlags(ItemFlag.HIDE_PLACED_ON);
            meta2.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
            meta2.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
            final ArrayList<String> lore2 = new ArrayList<>();
            lore2.add(" ");
            lore2.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Regeneração");
            lore2.add(" ");
            meta2.setLore(lore2);
            final FireworkEffectMeta metaFw2 = (FireworkEffectMeta) meta2;
            final FireworkEffect aa2 = FireworkEffect.builder().withColor(Color.FUCHSIA).build();
            metaFw2.setEffect(aa2);
            playerDrop2.setItemMeta(metaFw2);

            return playerDrop2;
        } else if (type.equalsIgnoreCase("super_pulo")) {
            final ItemStack playerDrop3 = new ItemStack(Material.FIREWORK_STAR, 1);
            final ItemMeta meta3 = playerDrop3.getItemMeta();
            meta3.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
            meta3.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
            meta3.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            meta3.addItemFlags(ItemFlag.HIDE_DESTROYS);
            meta3.addItemFlags(ItemFlag.HIDE_PLACED_ON);
            meta3.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
            meta3.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
            final ArrayList<String> lore3 = new ArrayList<>();
            lore3.add(" ");
            lore3.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Super pulo");
            lore3.add(" ");
            meta3.setLore(lore3);
            final FireworkEffectMeta metaFw3 = (FireworkEffectMeta) meta3;
            final FireworkEffect aa3 = FireworkEffect.builder().withColor(Color.LIME).build();
            metaFw3.setEffect(aa3);
            playerDrop3.setItemMeta(metaFw3);

            return playerDrop3;
        } else if (type.equalsIgnoreCase("velocidade")) {
            final ItemStack playerDrop4 = new ItemStack(Material.FIREWORK_STAR, 1);
            final ItemMeta meta4 = playerDrop4.getItemMeta();
            meta4.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
            meta4.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
            meta4.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
            meta4.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            meta4.addItemFlags(ItemFlag.HIDE_DESTROYS);
            meta4.addItemFlags(ItemFlag.HIDE_PLACED_ON);
            meta4.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
            final ArrayList<String> lore4 = new ArrayList<>();
            lore4.add(" ");
            lore4.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Velocidade");
            lore4.add(" ");
            meta4.setLore(lore4);
            final FireworkEffectMeta metaFw4 = (FireworkEffectMeta) meta4;
            final FireworkEffect aa4 = FireworkEffect.builder().withColor(Color.BLUE).build();
            metaFw4.setEffect(aa4);
            playerDrop4.setItemMeta(metaFw4);

            return playerDrop4;
        } else {
            return null;
        }
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = player.getUniqueId().toString();

        // -/beacon ceder (tipo) (quantia)
        if (cmd.getName().equalsIgnoreCase("beacon")) {
            if (TagAPI.getTag(UUID).equalsIgnoreCase("Master") || TagAPI.getTag(UUID).equalsIgnoreCase("Supervisor")) {
                if (args.length == 3) {
                    if (args[0].equalsIgnoreCase("ceder")) {
                        final String[] type = {"força", "pressa", "regeneração", "super_pulo", "velocidade"};

                        if (Arrays.asList(type).contains(args[1])) {
                            try {
                                final int amount = Integer.valueOf(args[2]);
                                final ItemStack item = CommandBeacon.getItem(args[1]);
                                item.setAmount(amount);

                                final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(item);
                                for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                    player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                                }
                                player.sendMessage(ChatColor.GREEN + " * Ativador de efeito, cedido com sucesso!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            } catch (final NumberFormatException e) {
                                player.sendMessage(ChatColor.RED + " * Ops, a quantia apresenta algum erro!");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, o efeito informado não existe!");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Comando correto: " + ChatColor.GRAY + "/beacon ceder (tipo) (quantia)");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + " * Comando correto: " + ChatColor.GRAY + "/beacon ceder (tipo) (quantia)");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage("§c* Você não tem permissão para executar esse comando.");
            }
        }

        return false;
    }
}
