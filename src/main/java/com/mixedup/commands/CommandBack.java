package com.mixedup.commands;

import com.mixedup.apis.ConfigAPI;
import com.mixedup.apis.LastPositionAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import com.mixedup.hashs.HasNoticed;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandBack implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("back") || cmd.getName().equalsIgnoreCase("voltar")) {

            if (args.length == 0) {
                final String[] loc = LastPositionAPI.getLastLocation(UUID).split(":");
                final Location location = new Location(Bukkit.getWorld(loc[0]), Double.valueOf(loc[1]), Double.valueOf(loc[2]), Double.valueOf(loc[3]));
                Location locationTeste;

                int noCotainsBlock = 0;
                boolean containsLava = false;
                for (int i = 1; i <= 4; i++) {
                    locationTeste = new Location(Bukkit.getWorld(loc[0]), Double.valueOf(loc[1]) - i, Double.valueOf(loc[2]), Double.valueOf(loc[3]));

                    if (locationTeste.getBlock().getType().equals(Material.AIR)) {
                        noCotainsBlock++;
                    }
                    if (locationTeste.getBlock().getType().equals(Material.LAVA) || locationTeste.getBlock().getType().equals(Material.LAVA)) {
                        containsLava = true;
                    }

                    if (HasNoticed.get(UUID) == null) {
                        if (noCotainsBlock >= 4 || containsLava == true) {
                            player.sendMessage(ChatColor.RED + "(!) ATENÇAO, ISTO PODE SER UMA TRAP! \nCaso deseje prosseguir até o local utilize novamente /back ou /voltar.");
                            new HasNoticed(UUID, true).insert();
                        } else {
                            if (i == 4) {
                                if (player.getAllowFlight() == true) {
                                    if (ConfigAPI.getFlyStatus(UUID) == true) {
                                        final String tag = TagAPI.getTag(player.getUniqueId().toString());
                                        if (tag != null && !(tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Moderador") || tag.equalsIgnoreCase("Admin"))) {
                                            player.setAllowFlight(false);
                                            ConfigAPI.updateFly(UUID, false);
                                        }
                                    }
                                }

                                player.teleport(location);
                                player.sendMessage(ChatColor.GREEN + " * Teletransportando para última localidade.");
                            }
                        }
                    } else {
                        if (noCotainsBlock >= 4 || containsLava == true) {
                            if (HasNoticed.get(UUID).getIsNoticed() == false) {
                                player.sendMessage(ChatColor.RED + "(!) ATENÇAO, ISTO PODE SER UMA TRAP! \nCaso deseje prosseguir até o local utilize novamente /back ou /voltar.");
                                HasNoticed.get(UUID).setIsNoticed(true);
                            } else {
                                if (i == 4) {
                                    if (player.getAllowFlight() == true) {
                                        if (ConfigAPI.getFlyStatus(UUID) == true) {
                                            final String tag = TagAPI.getTag(player.getUniqueId().toString());
                                            if (tag != null && !(tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Moderador") || tag.equalsIgnoreCase("Admin"))) {
                                                player.setAllowFlight(false);
                                                ConfigAPI.updateFly(UUID, false);
                                            }
                                        }
                                    }

                                    HasNoticed.get(UUID).setIsNoticed(false);
                                    player.teleport(location);
                                    player.sendMessage(ChatColor.GREEN + " * Teletransportando para última localidade.");
                                }
                            }
                        } else {
                            if (i == 4) {
                                if (player.getAllowFlight() == true) {
                                    player.setAllowFlight(false);
                                    if (player.getAllowFlight() == true) {
                                        if (ConfigAPI.getFlyStatus(UUID) == true) {
                                            final String tag = TagAPI.getTag(player.getUniqueId().toString());
                                            if (tag != null && !(tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Moderador") || tag.equalsIgnoreCase("Admin"))) {
                                                player.setAllowFlight(false);
                                                ConfigAPI.updateFly(UUID, false);
                                            }
                                        }
                                    }

                                }
                                player.teleport(location);
                                player.sendMessage(ChatColor.GREEN + " * Teletransportando para última localidade.");
                            }
                        }
                    }
                }
            } else {
                player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/back ou /voltar)");
            }
        }
        return false;
    }
}
