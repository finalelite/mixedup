package com.mixedup;

import org.bukkit.ChatColor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySqlHub {

    public static Connection con;

    public static void connect() {
        if (!MySqlHub.isConnected()) {
            try {
                MySqlHub.con = DriverManager.getConnection("jdbc:mysql://localhost/hubfinal?autoReconnect=true&useSSL=false", "root",
                        "9cjhUMZQBYYspLvrRbTYrgTrXudkbcSf");
                System.out.println(ChatColor.BLUE + "MySql Hub Conectado");
            } catch (final SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void disconnect() {
        if (MySqlHub.isConnected()) {
            try {
                MySqlHub.con.close();
                System.out.println(ChatColor.BLUE + "MySql Hub Desconectado");
            } catch (final SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean isConnected() {
        return (MySqlHub.con != null);
    }
}
