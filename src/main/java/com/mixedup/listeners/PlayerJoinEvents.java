package com.mixedup.listeners;

import br.com.finalelite.pauloo27.api.PauloAPI;
import br.com.finalelite.pauloo27.api.database.PlayerRole;
import com.mixedup.Main;
import com.mixedup.apis.*;
import com.mixedup.caixas.PlayerHasOpenCaixa;
import com.mixedup.economy.CoinsAPI;
import com.mixedup.hashs.GuildaSelectProv;
import com.mixedup.hashs.PlayerVanishHash;
import com.mixedup.mcmmo.McMMOApi;
import com.mixedup.mcmmo.McMMOCache;
import com.mixedup.mcmmo.McMMOlog;
import com.mixedup.mcmmo.listeners.CreateInfosSQL;
import com.mixedup.mcmmo.utils.FallHeight;
import com.mixedup.mcmmo.utils.FallHeightNoShift;
import com.mixedup.plots.utils.PlayerItIsUtil;
import com.mixedup.utils.CentralizeMsg;
import com.mixedup.utils.PlayerInArea;
import com.mixedup.utils.UUIDcache;
import com.mixedup.vipAutentication.Authentication;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.attribute.AttributeModifier.Operation;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;

public class PlayerJoinEvents implements Listener {

    @EventHandler
    public void onJoin(final PlayerJoinEvent event) {
        final Player player = event.getPlayer();
        if (UUIDcache.get(player.getName()) == null) {
            new UUIDcache(player.getName(), player.getUniqueId().toString()).insert();
        }
        final String UUID = player.getUniqueId().toString();

        player.setPlayerListHeader(ChatColor.translateAlternateColorCodes('&', " \n&6&lFINALELITE\n "));
        player.setPlayerListFooter(CentralizeMsg.sendCenteredMessage("\n                         " +
                "&6Twitter: &fwww.twitter.com/FinalElite                          " +
                "\n&6Discord: &fbit.ly/FinalEliteDiscord" +
                "\n &6Loja: &fwww.finalelite.com.br\n "));

        CreateInfosSQL.create(UUID);

        //Verificando o primeiro login do player e o jogando para o primeiro NPC
        if (!GuildaAPI.hasGuilda(UUID)) {
            if (RegionsSpawnAPI.getLocation("guildaselect") != null) {
                final String[] loc = RegionsSpawnAPI.getLocation("guildaselect").split(":");
                final Location location = new Location(Bukkit.getWorld("guildaselect"), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), Float.parseFloat(loc[4]), Float.parseFloat(loc[5]));

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    target.hidePlayer(Main.plugin, player);
                    player.hidePlayer(Main.plugin, target);
                }
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        player.teleport(location);
                    }
                }.runTaskLater(Main.getInstance(), 3 * 20);
            }
        } else {
            final String[] loc = RegionsSpawnAPI.getLocation("spawn").split(":");
            final Location location = new Location(Bukkit.getWorld(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), Float.parseFloat(loc[4]), Float.parseFloat(loc[5]));
            player.teleport(location);
        }

        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
            @Override
            public void run() {
                if (!GuildaAPI.hasGuilda(UUID)) {
                    if (RegionsSpawnAPI.getLocation("guildaselect") != null) {
                        final String[] loc = RegionsSpawnAPI.getLocation("guildaselect").split(":");
                        final Location location = new Location(Bukkit.getWorld("guildaselect"), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), Float.parseFloat(loc[4]), Float.parseFloat(loc[5]));
                        player.teleport(location);

                        for (final Player target : Bukkit.getOnlinePlayers()) {
                            target.hidePlayer(Main.plugin, player);
                            player.hidePlayer(Main.plugin, target);
                        }
                    }
                }
            }
        }, 100L);
        //----------------------------------------------------------------------

        java.util.Date date = new java.util.Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        McMMOlog.createLog(UUID + ":" + localDate.getDayOfMonth() + "/" + localDate.getMonthValue() + "/" + localDate.getYear(), player.getName());

        //----------------------------------------------------------------------

        //----------------------------------------------------------------------

        CoinsAPI.checkInfos(UUID);

        //----------------------------------------------------------------------


        //----------------------------------------------------------------------

        WarpAPI.addCache(UUID);

        //----------------------------------------------------------------------

        McMMOCache.saves.remove(UUID);

        //-----Setando scoreboard ao player entrar------------------------------
//        new Scoreboard(player).sendToPlayer(player);
//        for (Player target : Bukkit.getOnlinePlayers()) {
//            if (BackEnd.scoreboard.containsKey(target.getUniqueId())) {
//                BackEnd.scoreboard.get(target.getUniqueId()).updateScore(target);
//            }
//        }


        //----------------------------------------------------------------------

        // --- SCOREBOARD ---
        final val sb = Main.scoreboardManager;
        sb.createScoreboard(player);
        sb.setDefaultScoreboard(player);
        final val tblist = sb.getTablistManager();
        tblist.setPlayerGroup(player, tblist.getGroupIdByName(PauloAPI.getInstance().getAccountInfo(player).getRole().getDisplayName()));
        sb.setTabList(player);

        // atualizando para os outros
        sb.updateTabList(player);
        Bukkit.getOnlinePlayers().forEach(p -> {
            sb.updateTabList(player);
            if (sb.hasSquadScoreboard(player)) {
                sb.getSquadScoreboard().updateEntry(p, "squad_online");
            } else {
                sb.getDefaultScoreboard().updateEntry(p, "online");
            }
        });

        //-----Desabilitando e habilitando VIP----------------------------------
        Authentication.removeVip(UUID, player);
        Authentication.auth(UUID);

        Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
            Authentication.removeVip(UUID, player);
            Authentication.auth(UUID);
        }, 100L);
        //----------------------------------------------------------------------

        //----------------------------------------------------------------------
        if (player.getHealthScale() > 20) {
            player.setHealthScale(20);
        }
        //----------------------------------------------------------------------

        //-----PVP 1.8----------------------------------------------------------
        player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).addModifier(new AttributeModifier("", 9.9999999E7D, Operation.ADD_NUMBER));
        //----------------------------------------------------------------------


        //-----Removendo mensagem de join dos players---------------------------
        event.setJoinMessage(null);
        //----------------------------------------------------------------------


        //-----Removendo bug enchant slowfalling, nightvision-------------------
        if (player.hasPotionEffect(PotionEffectType.SLOW_FALLING) && player.hasPotionEffect(PotionEffectType.NIGHT_VISION)) {
            if (player.getPotionEffect(PotionEffectType.SLOW_FALLING).getDuration() >= 999999 && player.getPotionEffect(PotionEffectType.NIGHT_VISION).getDuration() >= 999999) {
                player.removePotionEffect(PotionEffectType.SLOW_FALLING);
                player.removePotionEffect(PotionEffectType.NIGHT_VISION);
            }
        }
        //----------------------------------------------------------------------


        //----------------------------------------------------------------------
        if (PoderAPI.getExist(UUID) == null) {
            PoderAPI.createInfo(UUID);
        }
        //----------------------------------------------------------------------


        //-----Setando info provisoria guilda-----------------------------------
        new GuildaSelectProv(UUID, false).insert();
        //----------------------------------------------------------------------


        //-----Setando primeira info lastlocation player------------------------
        if (LastPositionAPI.getLastLocation(UUID) == null) {
            final String loc = player.getLocation().getWorld().getName() + ":" + player.getLocation().getX() + ":" + player.getLocation().getY() + ":" + player.getLocation().getZ();
            LastPositionAPI.createFirstLocation(UUID, loc);
        }
        //----------------------------------------------------------------------


        //-----SETANDO SE PLAYER ESTÁ DENTRO DE ALGUM TERRENO-------------------
        if (PlayerItIsUtil.get(UUID) == null) {
            new PlayerItIsUtil(UUID, false, "NULL").insert();
        }
        //----------------------------------------------------------------------


        //-----CRIANDO INFO SE PLAYER ESTÁ ABRINDO CAIXA------------------------
        if (PlayerHasOpenCaixa.get(UUID) == null) {
            new PlayerHasOpenCaixa(UUID, false).insert();
        }
        //----------------------------------------------------------------------


        //-----CRIANDO INFO SE PLAYER ESTÁ NA AREA X----------------------------
        if (PlayerInArea.get(UUID) == null) {
            new PlayerInArea(UUID, 0, false).insert();
        }
        //----------------------------------------------------------------------


        //----------------------------------------------------------------------
        if (FallHeight.get(UUID) == null) {
            new FallHeight(UUID, 0).insert();
        }
        if (FallHeightNoShift.get(UUID) == null) {
            new FallHeightNoShift(UUID, 0).insert();
        }
        //----------------------------------------------------------------------
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void setupPermissions(final PlayerJoinEvent event) {
        Player p = event.getPlayer();

        PermissionAttachment perms = p.addAttachment(Main.getInstance());

        perms.setPermission("maxbans.broadcast", true);

        PlayerRole role = PauloAPI.getInstance().getAccountInfo(p).getRole();

        if(role.isStaff()) {

            perms.setPermission("maxbans.mute", true);
            perms.setPermission("maxbans.warn", true);
            perms.setPermission("maxbans.kick", true);
            perms.setPermission("maxbans.iplookup", true);
            perms.setPermission("maxbans.broadcast", true);
            perms.setPermission("maxbans.silent", true);
            perms.setPermission("maxbans.history", true);

            if(role.isMajorStaff()) {
                perms.setPermission("mv.bypass.gamemode.*", true);
                perms.setPermission("maxbans.ban", true);
                perms.setPermission("maxbans.ipban", true);
                perms.setPermission("maxbans.ipmute", true);

            } else {
                if(role == PlayerRole.MODERADOR) {
                    perms.setPermission("mv.bypass.gamemode.*", true);
                    perms.setPermission("maxbans.ban", true);
                }
            }

        }

    }


    @EventHandler(priority = EventPriority.LOWEST)
    public void hideVanishedPlayers(final PlayerJoinEvent event) {
        final Player player = event.getPlayer();
        PlayerVanishHash.getPlayers().forEach(p -> player.hidePlayer(Main.getInstance(), p));
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void fullServerJoin(final PlayerLoginEvent event) {
        final val player = event.getPlayer();
        final val tag = TagAPI.getTag(player.getUniqueId().toString());

        if (tag == null)
            return;

        if (!tag.equalsIgnoreCase("membro")) {
            if (event.getResult() == Result.KICK_FULL)
                event.allow();
        }

    }
}
