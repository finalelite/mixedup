package com.mixedup.listeners;

import com.mixedup.Main;
import com.mixedup.apis.BeaconAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.factions.FacAPI;
import org.bukkit.Bukkit;
import org.bukkit.block.Beacon;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class BeaconTaskFix {

    public static ArrayList<String> locations = new ArrayList<>();

    private static Thread thread;

    public static void stopThread() {

        thread.interrupt();

    }

    public static void setBeacon(String location, Beacon beacon, String UUID) {
        locations.add(location);
        new BeaconTaskFix(location, beacon, UUID).insert();
    }

    public static void removeBeacon(String location) {
        locations.remove(location);
        cache.remove(location);
    }

    public static void task() {
        thread = new Thread(() -> {
            while (true) {
                try {
                    Thread.currentThread().sleep(1000 * 1);
                } catch (final Exception e) {
                    e.printStackTrace();
                }

                ArrayList<String> remove = new ArrayList<>();
                for (String loc : locations) {
                    Bukkit.getScheduler().runTask(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            for (final LivingEntity entity : get(loc).getBeacon().getEntitiesInRange()) {
                                String UUID = get(loc).getUUID();

                                if (get(loc).getBeacon().getTier() != 4) {
                                    remove.add(loc);
                                }
                                if (entity.getType().equals(EntityType.PLAYER)) {
                                    if (BeaconAPI.getOscilar(loc) == true) {
                                        try {
                                            if (FacAPI.getAliados(FacAPI.getFacNome(UUID)) != null) {
                                                if (FacAPI.getAliados(FacAPI.getFacNome(UUID)).contains(":")) {
                                                    final String[] split = FacAPI.getAliados(FacAPI.getFacNome(UUID)).split(":");
                                                    final Player target = (Player) entity;
                                                    final String UUIDtarget = PlayerUUID.getUUID(target.getName());

                                                    for (int i = 1; i <= split.length; i++) {
                                                        if (FacAPI.getMembros(split[i - 1]).contains(":")) {
                                                            final String[] splitAliados = FacAPI.getMembros(split[i - 1]).split(":");

                                                            for (int x = 1; x <= splitAliados.length; x++) {
                                                                if (splitAliados[x - 1].equalsIgnoreCase(UUIDtarget)) {
                                                                    applyEffects(entity, loc);
                                                                }
                                                            }
                                                        } else {
                                                            if (!FacAPI.getMembros(split[i - 1]).equals("NULL")) {
                                                                if (FacAPI.getMembros(split[i - 1]).equalsIgnoreCase(UUIDtarget)) {
                                                                    applyEffects(entity, loc);
                                                                }
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    final Player target = (Player) entity;
                                                    final String UUIDtarget = PlayerUUID.getUUID(target.getName());

                                                    if (FacAPI.getMembros(FacAPI.getAliados(FacAPI.getFacNome(UUID))).contains(":")) {
                                                        final String[] splitAliados = FacAPI.getMembros(FacAPI.getAliados(FacAPI.getFacNome(UUID))).split(":");

                                                        for (int x = 1; x <= splitAliados.length; x++) {
                                                            if (splitAliados[x - 1].equalsIgnoreCase(UUIDtarget)) {
                                                                applyEffects(entity, loc);
                                                            }
                                                        }
                                                    } else {
                                                        if (!FacAPI.getMembros(FacAPI.getAliados(FacAPI.getFacNome(UUID))).equals("NULL")) {
                                                            if (FacAPI.getMembros(FacAPI.getAliados(FacAPI.getFacNome(UUID))).equalsIgnoreCase(UUIDtarget)) {
                                                                applyEffects(entity, loc);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if (FacAPI.getLideres(FacAPI.getFacNome(UUID)).contains(":")) {
                                                final String[] split = FacAPI.getLideres(FacAPI.getFacNome(UUID)).split(":");
                                                final Player target = (Player) entity;
                                                final String UUIDtarget = PlayerUUID.getUUID(target.getName());

                                                for (int i = 1; i <= split.length; i++) {
                                                    if (split[i - 1].equalsIgnoreCase(UUIDtarget)) {
                                                        applyEffects(entity, loc);
                                                    }
                                                }
                                            } else {
                                                if (!FacAPI.getLideres(FacAPI.getFacNome(UUID)).equals("NULL")) {
                                                    final Player target = (Player) entity;
                                                    final String UUIDtarget = PlayerUUID.getUUID(target.getName());

                                                    if (FacAPI.getLideres(FacAPI.getFacNome(UUID)).equalsIgnoreCase(UUIDtarget)) {
                                                        applyEffects(entity, loc);
                                                    }
                                                }
                                            }
                                            if (FacAPI.getCapitoes(FacAPI.getFacNome(UUID)).contains(":")) {
                                                final String[] split = FacAPI.getCapitoes(FacAPI.getFacNome(UUID)).split(":");
                                                final Player target = (Player) entity;
                                                final String UUIDtarget = PlayerUUID.getUUID(target.getName());

                                                for (int i = 1; i <= split.length; i++) {
                                                    if (split[i - 1].equalsIgnoreCase(UUIDtarget)) {
                                                        applyEffects(entity, loc);
                                                    }
                                                }
                                            } else {
                                                if (!FacAPI.getCapitoes(FacAPI.getFacNome(UUID)).equals("NULL")) {
                                                    final Player target = (Player) entity;
                                                    final String UUIDtarget = PlayerUUID.getUUID(target.getName());

                                                    if (FacAPI.getCapitoes(FacAPI.getFacNome(UUID)).equalsIgnoreCase(UUIDtarget)) {
                                                        applyEffects(entity, loc);
                                                    }
                                                }
                                            }
                                            if (FacAPI.getRecrutas(FacAPI.getFacNome(UUID)).contains(":")) {
                                                final String[] split = FacAPI.getRecrutas(FacAPI.getFacNome(UUID)).split(":");
                                                final Player target = (Player) entity;
                                                final String UUIDtarget = PlayerUUID.getUUID(target.getName());

                                                for (int i = 1; i <= split.length; i++) {
                                                    if (split[i - 1].equalsIgnoreCase(UUIDtarget)) {
                                                        applyEffects(entity, loc);
                                                    }
                                                }
                                            } else {
                                                if (!FacAPI.getRecrutas(FacAPI.getFacNome(UUID)).equals("NULL")) {
                                                    final Player target = (Player) entity;
                                                    final String UUIDtarget = PlayerUUID.getUUID(target.getName());

                                                    if (FacAPI.getRecrutas(FacAPI.getFacNome(UUID)).equalsIgnoreCase(UUIDtarget)) {
                                                        applyEffects(entity, loc);
                                                    }
                                                }
                                            }
                                            if (FacAPI.getMembros(FacAPI.getFacNome(UUID)).contains(":")) {
                                                final String[] split = FacAPI.getMembros(FacAPI.getFacNome(UUID)).split(":");
                                                final Player target = (Player) entity;
                                                final String UUIDtarget = PlayerUUID.getUUID(target.getName());

                                                for (int i = 1; i <= split.length; i++) {
                                                    if (split[i - 1].equalsIgnoreCase(UUIDtarget)) {
                                                        applyEffects(entity, loc);
                                                    }
                                                }
                                            } else {
                                                if (!FacAPI.getMembros(FacAPI.getFacNome(UUID)).equals("NULL")) {
                                                    final Player target = (Player) entity;
                                                    final String UUIDtarget = PlayerUUID.getUUID(target.getName());

                                                    if (FacAPI.getMembros(FacAPI.getFacNome(UUID)).equalsIgnoreCase(UUIDtarget)) {
                                                        applyEffects(entity, loc);
                                                    }
                                                }
                                            }
                                        } catch (Exception e) {
                                            remove.add(loc);
                                        }
                                    } else {
                                        applyEffects(entity, loc);
                                    }
                                }
                            }
                        }
                    });
                }

                for (String r : remove) {
                    removeBeacon(r);
                }
            }
        });
        thread.setName("beacon-thread");
        thread.start();
    }

    private static void applyEffects(final LivingEntity entity, final String loc) {
        Bukkit.getScheduler().runTask(Main.plugin, new Runnable() {
            @Override
            public void run() {
                if (BeaconAPI.getForcaActivated(loc) == true) {
                    final PotionEffect potion = new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 300, BeaconAPI.getForcaActivatedLevel(loc) - 1, false, false, true);
                    entity.addPotionEffect(potion);
                }
                if (BeaconAPI.getPressaActivated(loc) == true) {
                    final PotionEffect potion = new PotionEffect(PotionEffectType.FAST_DIGGING, 300, BeaconAPI.getPressaActivatedLevel(loc) - 1, false, false, true);
                    entity.addPotionEffect(potion);
                }
                if (BeaconAPI.getRegenActivated(loc) == true) {
                    final PotionEffect potion = new PotionEffect(PotionEffectType.REGENERATION, 300, BeaconAPI.getRegenActivatedLevel(loc) - 1, false, false, true);
                    entity.addPotionEffect(potion);
                }
                if (BeaconAPI.getJumpActivated(loc) == true) {
                    final PotionEffect potion = new PotionEffect(PotionEffectType.JUMP, 300, BeaconAPI.getJumpActivatedLevel(loc) - 1, false, false, true);
                    entity.addPotionEffect(potion);
                }
                if (BeaconAPI.getSpeedActivated(loc) == true) {
                    final PotionEffect potion = new PotionEffect(PotionEffectType.SPEED, 300, BeaconAPI.getSpeedActivatedLevel(loc) - 1, false, false, true);
                    entity.addPotionEffect(potion);
                }
            }
        });
    }


    //-----------------


    //String loc / Beacon beacon / String UUID
    private String location;
    private Beacon beacon;
    private String UUID;

    public static HashMap<String, BeaconTaskFix> cache = new HashMap<>();

    public BeaconTaskFix(String location, Beacon beacon, String UUID) {
        this.location = location;
        this.beacon = beacon;
        this.UUID = UUID;
    }

    public static BeaconTaskFix get(String location) {
        return cache.get(location);
    }

    public BeaconTaskFix insert() {
        cache.put(this.location, this);
        return this;
    }

    public Beacon getBeacon() {
        return this.beacon;
    }

    public void setBeacon(Beacon beacon) {
        this.beacon = beacon;
    }

    public String getUUID() {
        return this.UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }
}
