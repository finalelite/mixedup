package com.mixedup.listeners;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.mcmmo.ProbabilityApi;
import com.mixedup.utils.PlayerInArea;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Difficulty;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

public class MobsLevelsEvent implements Listener {

    public static boolean checkAllItems(final ItemStack... items) {
        return Arrays.stream(items).allMatch(MobsLevelsEvent::checkItem);
    }

    public static boolean checkItem(final ItemStack item) {
        if (item == null)
            return false;
        if (!item.hasItemMeta())
            return false;
        if (!item.getItemMeta().hasLore())
            return false;
        if (item.getItemMeta().getLore().size() <= 1)
            return false;
        return item.getItemMeta().getLore().get(1).equals(ChatColor.RED + "Invulnerável a radiação.");
    }

    @EventHandler
    public void onDeath(final PlayerDeathEvent event) {
        final Player player = event.getEntity();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (PlayerInArea.get(UUID) != null && PlayerInArea.get(UUID).getStatus() == true) {
            Bukkit.getScheduler().cancelTask(PlayerInArea.get(UUID).getRotacao());
        }
    }

    @EventHandler
    public void onMove(final PlayerMoveEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        //if (player.getWorld().getName().equalsIgnoreCase("Koi-3010.01")) {
        //            if (player.getWorld().getBiome(player.getLocation().getBlockX(), player.getLocation().getBlockZ()).equals(Biome.GIANT_TREE_TAIGA) || player.getWorld().getBiome(player.getLocation().getBlockX(), player.getLocation().getBlockZ()).equals(Biome.GIANT_SPRUCE_TAIGA) || player.getWorld().getBiome(player.getLocation().getBlockX(), player.getLocation().getBlockZ()).equals(Biome.GIANT_SPRUCE_TAIGA_HILLS) || player.getWorld().getBiome(player.getLocation().getBlockX(), player.getLocation().getBlockZ()).equals(Biome.GIANT_TREE_TAIGA_HILLS) || player.getWorld().getBiome(player.getLocation().getBlockX(), player.getLocation().getBlockZ()).equals(Biome.DARK_FOREST) || player.getWorld().getBiome(player.getLocation().getBlockX(), player.getLocation().getBlockZ()).equals(Biome.DARK_FOREST_HILLS) || player.getWorld().getBiome(player.getLocation().getBlockX(), player.getLocation().getBlockZ()).equals(Biome.MUSHROOM_FIELD_SHORE) || player.getWorld().getBiome(player.getLocation().getBlockX(), player.getLocation().getBlockZ()).equals(Biome.MUSHROOM_FIELDS)) {
        //                if (PlayerInArea.get(UUID).getStatus() == false) {
        //                    ItemStack helmet = player.getInventory().getHelmet();
        //                    ItemStack chestplate = player.getInventory().getChestplate();
        //                    ItemStack legs = player.getInventory().getLeggings();
        //                    ItemStack boots = player.getInventory().getBoots();
        //
        //                    if (checkAllItems(helmet, chestplate, legs, boots) == true) {
        //                        Bukkit.getScheduler().cancelTask(PlayerInArea.get(UUID).getRotacao());
        //                        if (PlayerInArea.get(UUID) != null) {
        //                            PlayerInArea.get(UUID).setStatus(false);
        //                        }
        //                        return;
        //                    }
        //                    PlayerInArea.get(UUID).setStatus(true);
        //                    int rotacao = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
        //                        @Override
        //                        public void run() {
        //                            ItemStack helmet = player.getInventory().getHelmet();
        //                            ItemStack chestplate = player.getInventory().getChestplate();
        //                            ItemStack legs = player.getInventory().getLeggings();
        //                            ItemStack boots = player.getInventory().getBoots();
        //
        //                            if (checkAllItems(helmet, chestplate, legs, boots) == true) {
        //                                Bukkit.getScheduler().cancelTask(PlayerInArea.get(UUID).getRotacao());
        //                                PlayerInArea.get(UUID).setStatus(false);
        //                                return;
        //                            }
        //                            player.damage(2);
        //                            player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_HURT, 1.0f, 1.0f);
        //                            if (player.getHealth() < 6) {
        //                                player.sendMessage(ChatColor.RED + " * Você está prestes a morrer de radiação, sai desta região!");
        //                            }
        //                        }
        //                    }, 0L, 30L);
        //                    PlayerInArea.get(UUID).setRotacao(rotacao);
        //                }
        //            } else if (PlayerInArea.get(UUID).getStatus() == true) {
        //                PlayerInArea.get(UUID).setStatus(false);
        //                Bukkit.getScheduler().cancelTask(PlayerInArea.get(UUID).getRotacao());
        //            }
        //        } else if (player.getWorld().getName().equalsIgnoreCase("HD-85512-b") || player.getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
        //            ItemStack helmet = player.getInventory().getHelmet();
        //            ItemStack chestplate = player.getInventory().getChestplate();
        //            ItemStack legs = player.getInventory().getLeggings();
        //            ItemStack boots = player.getInventory().getBoots();
        //
        //            if (checkAllItems(helmet, chestplate, legs, boots) == true) {
        //                Bukkit.getScheduler().cancelTask(PlayerInArea.get(UUID).getRotacao());
        //                if (PlayerInArea.get(UUID) != null) {
        //                    PlayerInArea.get(UUID).setStatus(false);
        //                }
        //                return;
        //            }
        //            if (PlayerInArea.get(UUID) != null && PlayerInArea.get(UUID).getStatus() == false) {
        //                PlayerInArea.get(UUID).setStatus(true);
        //                int rotacao = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
        //                    @Override
        //                    public void run() {
        //                        ItemStack helmet = player.getInventory().getHelmet();
        //                        ItemStack chestplate = player.getInventory().getChestplate();
        //                        ItemStack legs = player.getInventory().getLeggings();
        //                        ItemStack boots = player.getInventory().getBoots();
        //
        //                        if (checkAllItems(helmet, chestplate, legs, boots) == true) {
        //                            Bukkit.getScheduler().cancelTask(PlayerInArea.get(UUID).getRotacao());
        //                            PlayerInArea.get(UUID).setStatus(false);
        //                            return;
        //                        }
        //                        player.damage(2);
        //                        if (player.getHealth() == 4) {
        //                            player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_HURT, 1.0f, 1.0f);
        //                            if (player.getHealth() < 6) {
        //                                player.sendMessage(ChatColor.RED + " * Você está prestes a morrer de radiação, sai desta região!");
        //                            }
        //                        }
        //                    }
        //                }, 0L, 30L);
        //                PlayerInArea.get(UUID).setRotacao(rotacao);
        //            }
        //        } else {
        //            if (PlayerInArea.get(UUID) != null && PlayerInArea.get(UUID).getStatus() == true) {
        //                PlayerInArea.get(UUID).setStatus(false);
        //                Bukkit.getScheduler().cancelTask(PlayerInArea.get(UUID).getRotacao());
        //            }
        //        }
    }

    @EventHandler
    public void entitySpawn(final EntitySpawnEvent event) {
        if (event.getLocation().getWorld().getName().equalsIgnoreCase("Kepler-438b")) {
            if (!event.getLocation().getWorld().getDifficulty().equals(Difficulty.PEACEFUL)) {
                event.getLocation().getWorld().setDifficulty(Difficulty.PEACEFUL);
            }
        }
        if (event.getLocation().getWorld().getName().equalsIgnoreCase("Gliese-667-cc")) {
            if (!event.getLocation().getWorld().getDifficulty().equals(Difficulty.NORMAL)) {
                event.getLocation().getWorld().setDifficulty(Difficulty.NORMAL);
            }
        }
        if (event.getLocation().getWorld().getName().equalsIgnoreCase("Koi-3010.01")) {
            if (!event.getLocation().getWorld().getDifficulty().equals(Difficulty.HARD)) {
                event.getLocation().getWorld().setDifficulty(Difficulty.HARD);
            }
        }
        if (event.getLocation().getWorld().getName().equalsIgnoreCase("Hd-85512-b") || event.getLocation().getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
            if (!event.getLocation().getWorld().getDifficulty().equals(Difficulty.HARD)) {
                event.getLocation().getWorld().setDifficulty(Difficulty.HARD);
            }
        }
    }

    @EventHandler
    public void entityDamageEntity(final EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player) {
            final Player player = (Player) event.getEntity();
            if (player.getWorld().getName().equalsIgnoreCase("Koi-3010.01")) {
                final double damage = event.getDamage();
                event.setDamage(damage * 1.5);
            } else if (player.getWorld().getName().equalsIgnoreCase("Hd-85512-b") || player.getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
                final double damage = event.getDamage();
                event.setDamage(damage * 2);
            }
        } else if (event.getDamager() instanceof Player) {
            final Player player = (Player) event.getDamager();
            if (player.getWorld().getName().equalsIgnoreCase("Koi-3010.01")) {
                final double damage = event.getDamage();
                event.setDamage(damage / 1.5);

                final int probability = 100;
                if (ProbabilityApi.probab(probability)) {
                    event.getEntity().getWorld().spawnEntity(event.getEntity().getLocation(), event.getEntityType());
                    player.sendMessage(ChatColor.RED + " * Hmm, que azar o mob atacado se multiplicou!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VEX_CHARGE, 1.0f, 1.0f);
                }
            } else if (player.getWorld().getName().equalsIgnoreCase("Hd-85512-b") || player.getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
                final double damage = event.getDamage();
                event.setDamage(damage / 2);

                final int probability = 100;
                if (ProbabilityApi.probab(probability)) {
                    event.getEntity().getWorld().spawnEntity(event.getEntity().getLocation(), event.getEntityType());
                    player.sendMessage(ChatColor.RED + " * Hmm, que azar o mob atacado se multiplicou!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VEX_CHARGE, 1.0f, 1.0f);
                }
            }
        }
    }
}
