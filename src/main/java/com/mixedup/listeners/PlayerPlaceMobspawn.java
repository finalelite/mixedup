package com.mixedup.listeners;

import com.mixedup.apis.MobspawnAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.factions.FacAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class PlayerPlaceMobspawn implements Listener {

    public static void setSpawner(Block block, EntityType entity) {
        BlockState blockState = block.getState();
        CreatureSpawner spawner = (CreatureSpawner) blockState;
        spawner.setSpawnedType(entity);
        spawner.update();

        String location = block.getLocation().getWorld().getName() + ":" + block.getLocation().getBlockX() + ":" + block.getLocation().getBlockY() + ":" + block.getLocation().getBlockZ();
        MobspawnAPI.setInfo(location);
    }

    @EventHandler
    public void spawnChange(final PlayerInteractEvent event) {
        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && event.getPlayer().getItemInHand() != null && event.getPlayer().getItemInHand().getType().name().contains("EGG") && event.getClickedBlock().getType().equals(Material.SPAWNER)) {
            event.setCancelled(true);
        }
    }

    @EventHandler (priority = EventPriority.HIGH)
    public void onPlaceWorld(final BlockPlaceEvent event) {
        if (!event.getBlockPlaced().getWorld().getName().equalsIgnoreCase("Trappist-1b") && event.getBlockPlaced().getType().equals(Material.SPAWNER)) {
            if (event.getPlayer().getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.GREEN + "Caixa misteriosa"))
                return;
            if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spawner de Ovelha")) {
                setSpawner(event.getBlockPlaced(), EntityType.SHEEP);
                return;
            }
            if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spawner de Porco")) {
                setSpawner(event.getBlockPlaced(), EntityType.PIG);
                return;
            }
            if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spawner de Galinha")) {
                setSpawner(event.getBlockPlaced(), EntityType.CHICKEN);
                return;
            }
            if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spawner de Vaca")) {
                setSpawner(event.getBlockPlaced(), EntityType.COW);
                return;
            }
            if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spawner de Coelho")) {
                setSpawner(event.getBlockPlaced(), EntityType.RABBIT);
                return;
            }

            event.setCancelled(true);
            event.getPlayer().sendMessage(ChatColor.RED + " * Ops, você não pode colocar spawner aqui!");
            event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
        } else if (event.getBlockPlaced().getWorld().getName().equalsIgnoreCase("Trappist-1b") && event.getBlockPlaced().getType().equals(Material.SPAWNER)) {
            if (event.getBlockPlaced().getType().equals(Material.SPAWNER)) {
                if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spawner de Ghast")) {
                    setSpawner(event.getBlockPlaced(), EntityType.GHAST);
                }
                if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spawner de Aldeão zumbi")) {
                    setSpawner(event.getBlockPlaced(), EntityType.ZOMBIE_VILLAGER);
                }
                if(event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spawner de Golem de ferro")) {
                    setSpawner(event.getBlockPlaced(), EntityType.IRON_GOLEM);
                }
                if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spawner de Cubo magma")) {
                    setSpawner(event.getBlockPlaced(), EntityType.MAGMA_CUBE);
                }
                if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spawner de Bruxa")) {
                    setSpawner(event.getBlockPlaced(), EntityType.WITCH);
                }
                if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spawner de Esqueleto wither")) {
                    setSpawner(event.getBlockPlaced(), EntityType.WITHER_SKELETON);
                }
                if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spawner de Homem porco zumbi")) {
                    setSpawner(event.getBlockPlaced(), EntityType.PIG_ZOMBIE);
                }
                if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spawner de Blaze")) {
                    setSpawner(event.getBlockPlaced(), EntityType.BLAZE);
                }
                if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spawner de Slime")) {
                    setSpawner(event.getBlockPlaced(), EntityType.SLIME);
                }
                if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spawner de Aranha das cavernas")) {
                    setSpawner(event.getBlockPlaced(), EntityType.CAVE_SPIDER);
                }
                if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spawner de Enderman")) {
                    setSpawner(event.getBlockPlaced(), EntityType.ENDERMAN);
                }
                if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spawner de Creeper")) {
                    setSpawner(event.getBlockPlaced(), EntityType.CREEPER);
                }
                if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spawner de Aranha")) {
                    setSpawner(event.getBlockPlaced(), EntityType.SPIDER);
                }
                if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spawner de Esqueleto")) {
                    setSpawner(event.getBlockPlaced(), EntityType.SKELETON);
                }
                if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spawner de Zumbi")) {
                    setSpawner(event.getBlockPlaced(), EntityType.ZOMBIE);
                }
                if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spawner de Ovelha")) {
                    setSpawner(event.getBlockPlaced(), EntityType.SHEEP);
                }
                if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spawner de Porco")) {
                    setSpawner(event.getBlockPlaced(), EntityType.PIG);
                }
                if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spawner de Galinha")) {
                    setSpawner(event.getBlockPlaced(), EntityType.CHICKEN);
                }
                if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spawner de Vaca")) {
                    setSpawner(event.getBlockPlaced(), EntityType.COW);
                }
            }
        }
    }

    @EventHandler (priority = EventPriority.HIGH)
    public void onBreak(final BlockBreakEvent event) {
        if (event.isCancelled()) return;
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getPlayer().getItemInHand() != null) {
            if (event.getPlayer().getItemInHand().getType().equals(Material.DIAMOND_PICKAXE) || event.getPlayer().getItemInHand().getType().equals(Material.IRON_PICKAXE) || event.getPlayer().getItemInHand().getType().equals(Material.GOLDEN_PICKAXE)
                    || event.getPlayer().getItemInHand().getType().equals(Material.STONE_PICKAXE) || event.getPlayer().getItemInHand().getType().equals(Material.WOODEN_PICKAXE)) {
                String location = event.getBlock().getWorld().getName() + ":" + event.getBlock().getLocation().getBlockX() + ":" + event.getBlock().getLocation().getBlockY() + ":" + event.getBlock().getLocation().getBlockZ();

                if (event.getPlayer().getItemInHand().containsEnchantment(Enchantment.SILK_TOUCH)) {

                    if (event.getBlock().getType().equals(Material.SPAWNER) && event.getBlock().getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
                        if (FacAPI.getFacNome(UUID) != null) {
                            final String chunk = event.getBlock().getLocation().getChunk().getX() + ":" + event.getBlock().getLocation().getChunk().getZ();
                            if (FacAPI.getChunkOwn(chunk) != null && FacAPI.getChunkOwn(chunk).equalsIgnoreCase(FacAPI.getFacNome(UUID))) {
                                if (FacAPI.getPermSpawners(UUID) == false) {
                                    player.sendMessage(ChatColor.RED + " * Ops, você não contém permissão ou hierarquia para isto!");
                                    event.setCancelled(true);
                                    return;
                                } else {
                                    final BlockState blockState = event.getBlock().getState();
                                    final CreatureSpawner spawner = (CreatureSpawner) blockState;

                                    MobspawnAPI.createLog(location, spawner.getSpawnedType().name(), player.getName());
                                    if (MobspawnAPI.getExist(location) == null) {
                                        event.getBlock().setType(Material.AIR);
                                        event.setDropItems(false);
                                        return;
                                    }

                                    if (event.getBlock().getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
                                        if (FacAPI.getChunkOwn(chunk) != null) {
                                            if (FacAPI.getSobAttack(FacAPI.getChunkOwn(chunk)) == true) {
                                                if (FacAPI.getFacNome(UUID).equalsIgnoreCase(FacAPI.getChunkOwn(chunk))) {
                                                    event.setCancelled(true);
                                                    player.sendMessage(ChatColor.RED + " * Ops, o esquadrão está sob ataque. Não é permitido remover\n  os spawner da base, quando sob ataque!");
                                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                                    return;
                                                }
                                            }
                                        }
                                    }

                                    if (event.getBlock().getWorld().getName().equalsIgnoreCase("world_the_end") || event.getBlock().getWorld().getName().equalsIgnoreCase("world_nether")) {
                                        event.setCancelled(true);
                                        event.getBlock().setType(Material.AIR);
                                        return;
                                    }

                                    if (spawner.getSpawnedType().equals(EntityType.COW)) {
                                        final ItemStack item = new ItemStack(Material.SPAWNER);
                                        final ItemMeta metaItem = item.getItemMeta();
                                        metaItem.setDisplayName(ChatColor.RED + "Spawner de Vaca");
                                        item.setItemMeta(metaItem);
                                        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                                        event.getBlock().setType(Material.AIR);
                                        MobspawnAPI.removeInfo(location);
                                    }
                                    if (spawner.getSpawnedType().equals(EntityType.CHICKEN)) {
                                        final ItemStack item = new ItemStack(Material.SPAWNER);
                                        final ItemMeta metaItem = item.getItemMeta();
                                        metaItem.setDisplayName(ChatColor.RED + "Spawner de Galinha");
                                        item.setItemMeta(metaItem);
                                        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                                        event.getBlock().setType(Material.AIR);
                                        MobspawnAPI.removeInfo(location);
                                    }
                                    if (spawner.getSpawnedType().equals(EntityType.PIG)) {
                                        final ItemStack item = new ItemStack(Material.SPAWNER);
                                        final ItemMeta metaItem = item.getItemMeta();
                                        metaItem.setDisplayName(ChatColor.RED + "Spawner de Porco");
                                        item.setItemMeta(metaItem);
                                        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                                        event.getBlock().setType(Material.AIR);
                                        MobspawnAPI.removeInfo(location);
                                    }
                                    if (spawner.getSpawnedType().equals(EntityType.SHEEP)) {
                                        final ItemStack item = new ItemStack(Material.SPAWNER);
                                        final ItemMeta metaItem = item.getItemMeta();
                                        metaItem.setDisplayName(ChatColor.RED + "Spawner de Ovelha");
                                        item.setItemMeta(metaItem);
                                        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                                        event.getBlock().setType(Material.AIR);
                                        MobspawnAPI.removeInfo(location);
                                    }
                                    if (spawner.getSpawnedType().equals(EntityType.RABBIT)) {
                                        final ItemStack item = new ItemStack(Material.SPAWNER);
                                        final ItemMeta metaItem = item.getItemMeta();
                                        metaItem.setDisplayName(ChatColor.RED + "Spawner de Coelho");
                                        item.setItemMeta(metaItem);
                                        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                                        event.getBlock().setType(Material.AIR);
                                        MobspawnAPI.removeInfo(location);
                                    }
                                    if (spawner.getSpawnedType().equals(EntityType.ZOMBIE)) {
                                        final ItemStack item = new ItemStack(Material.SPAWNER);
                                        final ItemMeta metaItem = item.getItemMeta();
                                        metaItem.setDisplayName(ChatColor.RED + "Spawner de Zumbi");
                                        item.setItemMeta(metaItem);
                                        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                                        event.getBlock().setType(Material.AIR);
                                        MobspawnAPI.removeInfo(location);
                                    }
                                    if (spawner.getSpawnedType().equals(EntityType.SKELETON)) {
                                        final ItemStack item = new ItemStack(Material.SPAWNER);
                                        final ItemMeta metaItem = item.getItemMeta();
                                        metaItem.setDisplayName(ChatColor.RED + "Spawner de Esqueleto");
                                        item.setItemMeta(metaItem);
                                        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                                        event.getBlock().setType(Material.AIR);
                                        MobspawnAPI.removeInfo(location);
                                    }
                                    if (spawner.getSpawnedType().equals(EntityType.SPIDER)) {
                                        final ItemStack item = new ItemStack(Material.SPAWNER);
                                        final ItemMeta metaItem = item.getItemMeta();
                                        metaItem.setDisplayName(ChatColor.RED + "Spawner de Aranha");
                                        item.setItemMeta(metaItem);
                                        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                                        event.getBlock().setType(Material.AIR);
                                        MobspawnAPI.removeInfo(location);
                                    }
                                    if (spawner.getSpawnedType().equals(EntityType.CREEPER)) {
                                        final ItemStack item = new ItemStack(Material.SPAWNER);
                                        final ItemMeta metaItem = item.getItemMeta();
                                        metaItem.setDisplayName(ChatColor.RED + "Spawner de Creeper");
                                        item.setItemMeta(metaItem);
                                        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                                        event.getBlock().setType(Material.AIR);
                                        MobspawnAPI.removeInfo(location);
                                    }
                                    if (spawner.getSpawnedType().equals(EntityType.ENDERMAN)) {
                                        final ItemStack item = new ItemStack(Material.SPAWNER);
                                        final ItemMeta metaItem = item.getItemMeta();
                                        metaItem.setDisplayName(ChatColor.RED + "Spawner de Enderman");
                                        item.setItemMeta(metaItem);
                                        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                                        event.getBlock().setType(Material.AIR);
                                        MobspawnAPI.removeInfo(location);
                                    }
                                    if (spawner.getSpawnedType().equals(EntityType.CAVE_SPIDER)) {
                                        final ItemStack item = new ItemStack(Material.SPAWNER);
                                        final ItemMeta metaItem = item.getItemMeta();
                                        metaItem.setDisplayName(ChatColor.RED + "Spawner de Aranha das cavernas");
                                        item.setItemMeta(metaItem);
                                        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                                        event.getBlock().setType(Material.AIR);
                                        MobspawnAPI.removeInfo(location);
                                    }
                                    if (spawner.getSpawnedType().equals(EntityType.SLIME)) {
                                        final ItemStack item = new ItemStack(Material.SPAWNER);
                                        final ItemMeta metaItem = item.getItemMeta();
                                        metaItem.setDisplayName(ChatColor.RED + "Spawner de Slime");
                                        item.setItemMeta(metaItem);
                                        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                                        event.getBlock().setType(Material.AIR);
                                        MobspawnAPI.removeInfo(location);
                                    }
                                    if(spawner.getSpawnedType().equals(EntityType.IRON_GOLEM)) {
                                        final ItemStack item = new ItemStack(Material.SPAWNER);
                                        final ItemMeta metaItem = item.getItemMeta();
                                        metaItem.setDisplayName(ChatColor.RED + "Spawner de Golem de ferro");
                                        item.setItemMeta(metaItem);
                                        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                                        event.getBlock().setType(Material.AIR);
                                        MobspawnAPI.removeInfo(location);
                                    }
                                    if (spawner.getSpawnedType().equals(EntityType.BLAZE)) {
                                        final ItemStack item = new ItemStack(Material.SPAWNER);
                                        final ItemMeta metaItem = item.getItemMeta();
                                        metaItem.setDisplayName(ChatColor.RED + "Spawner de Blaze");
                                        item.setItemMeta(metaItem);
                                        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                                        event.getBlock().setType(Material.AIR);
                                        MobspawnAPI.removeInfo(location);
                                    }
                                    if (spawner.getSpawnedType().equals(EntityType.PIG_ZOMBIE)) {
                                        final ItemStack item = new ItemStack(Material.SPAWNER);
                                        final ItemMeta metaItem = item.getItemMeta();
                                        metaItem.setDisplayName(ChatColor.RED + "Spawner de Homem porco zumbi");
                                        item.setItemMeta(metaItem);
                                        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                                        event.getBlock().setType(Material.AIR);
                                        MobspawnAPI.removeInfo(location);
                                    }
                                    if (spawner.getSpawnedType().equals(EntityType.WITHER_SKELETON)) {
                                        final ItemStack item = new ItemStack(Material.SPAWNER);
                                        final ItemMeta metaItem = item.getItemMeta();
                                        metaItem.setDisplayName(ChatColor.RED + "Spawner de Esqueleto wither");
                                        item.setItemMeta(metaItem);
                                        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                                        event.getBlock().setType(Material.AIR);
                                        MobspawnAPI.removeInfo(location);
                                    }
                                    if (spawner.getSpawnedType().equals(EntityType.WITCH)) {
                                        final ItemStack item = new ItemStack(Material.SPAWNER);
                                        final ItemMeta metaItem = item.getItemMeta();
                                        metaItem.setDisplayName(ChatColor.RED + "Spawner de Bruxa");
                                        item.setItemMeta(metaItem);
                                        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                                        event.getBlock().setType(Material.AIR);
                                        MobspawnAPI.removeInfo(location);
                                    }
                                    if (spawner.getSpawnedType().equals(EntityType.MAGMA_CUBE)) {
                                        final ItemStack item = new ItemStack(Material.SPAWNER);
                                        final ItemMeta metaItem = item.getItemMeta();
                                        metaItem.setDisplayName(ChatColor.RED + "Spawner de Cubo magma");
                                        item.setItemMeta(metaItem);
                                        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                                        event.getBlock().setType(Material.AIR);
                                        MobspawnAPI.removeInfo(location);
                                    }
                                    if (spawner.getSpawnedType().equals(EntityType.ZOMBIE_VILLAGER)) {
                                        final ItemStack item = new ItemStack(Material.SPAWNER);
                                        final ItemMeta metaItem = item.getItemMeta();
                                        metaItem.setDisplayName(ChatColor.RED + "Spawner de Aldeão zumbi");
                                        item.setItemMeta(metaItem);
                                        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                                        event.getBlock().setType(Material.AIR);
                                        MobspawnAPI.removeInfo(location);
                                    }
                                    if (spawner.getSpawnedType().equals(EntityType.GHAST)) {
                                        final ItemStack item = new ItemStack(Material.SPAWNER);
                                        final ItemMeta metaItem = item.getItemMeta();
                                        metaItem.setDisplayName(ChatColor.RED + "Spawner de Ghast");
                                        item.setItemMeta(metaItem);
                                        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                                        event.getBlock().setType(Material.AIR);
                                        MobspawnAPI.removeInfo(location);
                                    }
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, esta zona contém proteção!");
                                event.setCancelled(true);
                                return;
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, esta zona contém proteção!");
                            event.setCancelled(true);
                            return;
                        }
                    }

                    if (event.getBlock().getType().equals(Material.SPAWNER)) {
                        final BlockState blockState = event.getBlock().getState();
                        final CreatureSpawner spawner = (CreatureSpawner) blockState;

                        MobspawnAPI.createLog(location, spawner.getSpawnedType().name(), player.getName());
                        if (MobspawnAPI.getExist(location) == null) {
                            event.getBlock().setType(Material.AIR);
                            event.setDropItems(false);
                            return;
                        }

                        if (event.getBlock().getWorld().getName().equalsIgnoreCase("world_the_end") || event.getBlock().getWorld().getName().equalsIgnoreCase("world_nether")) {
                            event.setCancelled(true);
                            event.getBlock().setType(Material.AIR);
                            return;
                        }

                        if (spawner.getSpawnedType().equals(EntityType.COW)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Vaca");
                            item.setItemMeta(metaItem);
                            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                            event.getBlock().setType(Material.AIR);
                            MobspawnAPI.removeInfo(location);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.CHICKEN)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Galinha");
                            item.setItemMeta(metaItem);
                            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                            event.getBlock().setType(Material.AIR);
                            MobspawnAPI.removeInfo(location);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.PIG)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Porco");
                            item.setItemMeta(metaItem);
                            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                            event.getBlock().setType(Material.AIR);
                            MobspawnAPI.removeInfo(location);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.SHEEP)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Ovelha");
                            item.setItemMeta(metaItem);
                            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                            event.getBlock().setType(Material.AIR);
                            MobspawnAPI.removeInfo(location);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.RABBIT)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Coelho");
                            item.setItemMeta(metaItem);
                            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                            event.getBlock().setType(Material.AIR);
                            MobspawnAPI.removeInfo(location);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.ZOMBIE)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Zumbi");
                            item.setItemMeta(metaItem);
                            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                            event.getBlock().setType(Material.AIR);
                            MobspawnAPI.removeInfo(location);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.SKELETON)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Esqueleto");
                            item.setItemMeta(metaItem);
                            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                            event.getBlock().setType(Material.AIR);
                            MobspawnAPI.removeInfo(location);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.SPIDER)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Aranha");
                            item.setItemMeta(metaItem);
                            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                            event.getBlock().setType(Material.AIR);
                            MobspawnAPI.removeInfo(location);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.CREEPER)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Creeper");
                            item.setItemMeta(metaItem);
                            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                            event.getBlock().setType(Material.AIR);
                            MobspawnAPI.removeInfo(location);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.ENDERMAN)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Enderman");
                            item.setItemMeta(metaItem);
                            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                            event.getBlock().setType(Material.AIR);
                            MobspawnAPI.removeInfo(location);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.CAVE_SPIDER)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Aranha das cavernas");
                            item.setItemMeta(metaItem);
                            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                            event.getBlock().setType(Material.AIR);
                            MobspawnAPI.removeInfo(location);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.SLIME)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Slime");
                            item.setItemMeta(metaItem);
                            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                            event.getBlock().setType(Material.AIR);
                            MobspawnAPI.removeInfo(location);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.BLAZE)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Blaze");
                            item.setItemMeta(metaItem);
                            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                            event.getBlock().setType(Material.AIR);
                            MobspawnAPI.removeInfo(location);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.PIG_ZOMBIE)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Homem porco zumbi");
                            item.setItemMeta(metaItem);
                            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                            event.getBlock().setType(Material.AIR);
                            MobspawnAPI.removeInfo(location);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.WITHER_SKELETON)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Esqueleto wither");
                            item.setItemMeta(metaItem);
                            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                            event.getBlock().setType(Material.AIR);
                            MobspawnAPI.removeInfo(location);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.WITCH)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Bruxa");
                            item.setItemMeta(metaItem);
                            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                            event.getBlock().setType(Material.AIR);
                            MobspawnAPI.removeInfo(location);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.MAGMA_CUBE)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Cubo magma");
                            item.setItemMeta(metaItem);
                            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                            event.getBlock().setType(Material.AIR);
                            MobspawnAPI.removeInfo(location);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.ZOMBIE_VILLAGER)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Aldeão zumbi");
                            item.setItemMeta(metaItem);
                            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                            event.getBlock().setType(Material.AIR);
                            MobspawnAPI.removeInfo(location);
                        }
                        if (spawner.getSpawnedType().equals(EntityType.GHAST)) {
                            final ItemStack item = new ItemStack(Material.SPAWNER);
                            final ItemMeta metaItem = item.getItemMeta();
                            metaItem.setDisplayName(ChatColor.RED + "Spawner de Ghast");
                            item.setItemMeta(metaItem);
                            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), item);
                            event.getBlock().setType(Material.AIR);
                            MobspawnAPI.removeInfo(location);
                        }
                    }
                } else {
                    MobspawnAPI.removeInfo(location);
                }
            }
        }
    }
}
