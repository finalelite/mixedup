package com.mixedup.listeners;

import com.mixedup.Main;
import com.mixedup.apis.ConfigAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.RegionsSpawnAPI;
import com.mixedup.apis.TagAPI;
import com.mixedup.hashs.JetpackHash;
import com.mixedup.hashs.PlayerInArenaHash;
import com.mixedup.hashs.PlayerInSpawnHash;
import org.bukkit.*;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Arrays;

public class SpawnEvents implements Listener {

    @EventHandler
    public void onChangeFoodLevel(final FoodLevelChangeEvent event) {
        if (event.getEntity().getWorld().getName().equalsIgnoreCase("spawn") || event.getEntity().getWorld().getName().equalsIgnoreCase("guildaselect")) {
            if (event.getEntity() instanceof Player) {
                final Player player = (Player) event.getEntity();
                if (player.getFoodLevel() > event.getFoodLevel()) {
                    if (playerInArenaVip(event.getEntity().getLocation()) == false) {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onInteract(final PlayerInteractEvent event) {
        if (event.getClickedBlock() != null && !event.getClickedBlock().getType().equals(Material.AIR)) {
            if (event.getClickedBlock().getWorld().getName().equalsIgnoreCase("spawn") || event.getClickedBlock().getWorld().getName().equalsIgnoreCase("guildaselect") || event.getClickedBlock().getWorld().getName().equalsIgnoreCase("aether")) {
                if (event.getClickedBlock().getType().equals(Material.ENDER_CHEST)) return;
                if (TagAPI.getTag(PlayerUUID.getUUID(event.getPlayer().getName())).equalsIgnoreCase("Master") || TagAPI.getTag(PlayerUUID.getUUID(event.getPlayer().getName())).equalsIgnoreCase("Supervisor"))
                    return;
                event.setCancelled(true);
            }
        }
    }

    @EventHandler (priority = EventPriority.MONITOR)
    public void onDrop(final EntityExplodeEvent event) {
        if (event.getLocation().getWorld().getName().equalsIgnoreCase("spawn") || event.getLocation().getWorld().getName().equalsIgnoreCase("guildaselect")) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onIgnite(final BlockIgniteEvent event) {
        if (event.getIgnitingEntity() != null && Arrays.asList(BlockIgniteEvent.IgniteCause.values()).contains(event.getCause())) {
            if (event.getIgnitingEntity().getWorld().getName().equalsIgnoreCase("spawn") || event.getIgnitingEntity().getWorld().getName().equalsIgnoreCase("guildaselect") || event.getIgnitingEntity().getWorld().getName().equalsIgnoreCase("aether")) {
                if (TagAPI.getTag(PlayerUUID.getUUID(event.getPlayer().getName())).equalsIgnoreCase("Master") || TagAPI.getTag(PlayerUUID.getUUID(event.getPlayer().getName())).equalsIgnoreCase("Supervisor"))
                    return;
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onBreak(final BlockBreakEvent event) {
        if (event.getBlock().getWorld().getName().equalsIgnoreCase("spawn") || event.getBlock().getWorld().getName().equalsIgnoreCase("guildaselect")) {
            if (TagAPI.getTag(PlayerUUID.getUUID(event.getPlayer().getName())).equalsIgnoreCase("Master") || TagAPI.getTag(PlayerUUID.getUUID(event.getPlayer().getName())).equalsIgnoreCase("Supervisor")) {
                event.setCancelled(false);
            } else {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPlace(final BlockPlaceEvent event) {
        if (event.getBlock().getWorld().getName().equalsIgnoreCase("spawn") || event.getBlock().getWorld().getName().equalsIgnoreCase("guildaselect")) {
            if (TagAPI.getTag(PlayerUUID.getUUID(event.getPlayer().getName())).equalsIgnoreCase("Master") || TagAPI.getTag(PlayerUUID.getUUID(event.getPlayer().getName())).equalsIgnoreCase("Supervisor")) {
                event.setCancelled(false);
            } else {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onProjectile(final ProjectileLaunchEvent event) {
        if (event.getEntity().getWorld().getName().equalsIgnoreCase("spawn") || event.getEntity().getWorld().getName().equalsIgnoreCase("guildaselect")) {
            if (ArenaEvents.playerInArea(((Player) event.getEntity().getShooter()).getLocation()) == false && playerInArenaVip(((Player) event.getEntity().getShooter()).getLocation()) == false) {
                event.setCancelled(true);
            } else {
                event.setCancelled(false);
            }
        }
    }

    @EventHandler
    public void onEat(final PlayerItemConsumeEvent event) {
        if (event.getItem().getType().equals(Material.CHORUS_FRUIT)) {
            if (event.getPlayer().getWorld().getName().equalsIgnoreCase("spawn") || event.getPlayer().getWorld().getName().equalsIgnoreCase("espaconave") || event.getPlayer().getWorld().getName().equalsIgnoreCase("ilhas-default") || event.getPlayer().getWorld().getName().equalsIgnoreCase("ilhas")) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onEntitySpawn(final EntitySpawnEvent event) {
        final String worldName = event.getLocation().getWorld().getName();
        if (worldName.equalsIgnoreCase("spawn") || worldName.equalsIgnoreCase("guildaselect")) {
            if (event.getEntityType().equals(EntityType.DROPPED_ITEM)) {
                if (!ArenaEvents.playerInArea(event.getLocation()) && playerInArenaVip(event.getLocation()) == false) {
                    event.setCancelled(true);
                }
            } else if (event.getEntityType() != EntityType.ARMOR_STAND && event.getEntityType() != EntityType.PLAYER) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onDamage(final EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            if (event.getEntity().getLocation().getWorld().getName().equalsIgnoreCase("spawn") || event.getEntity().getLocation().getWorld().getName().equalsIgnoreCase("guildaselect")) {
                if (event.getEntity().getName() != null && PlayerInArenaHash.get(event.getEntity().getUniqueId().toString()) != null && PlayerInArenaHash.get(event.getEntity().getUniqueId().toString()).getStatus() == false) {
                    event.setCancelled(true);
                } else if (PlayerInArenaHash.get(event.getEntity().getUniqueId().toString()) == null) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onPlaceEgg(final PlayerInteractEvent event) {
        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && event.getPlayer().getItemInHand() != null) {
            if (event.getPlayer().getWorld().getName().equalsIgnoreCase("spawn") || event.getPlayer().getWorld().getName().equalsIgnoreCase("guildaselect")) {
                if (event.getPlayer().getItemInHand().getType().equals(Material.CREEPER_SPAWN_EGG) || event.getPlayer().getItemInHand().getType().equals(Material.GHAST_SPAWN_EGG)) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onDrop(final PlayerDropItemEvent event) {
        if (event.getPlayer().getLocation().getWorld().getName().equalsIgnoreCase("spawn") || event.getPlayer().getLocation().getWorld().getName().equalsIgnoreCase("guildaselect")) {
            final String tag = TagAPI.getTag(event.getPlayer().getUniqueId().toString());
            if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Admin") || tag.equalsIgnoreCase("Moderador"))
                return;
            if (!ArenaEvents.playerInArea(event.getPlayer().getLocation()) && playerInArenaVip(event.getPlayer().getLocation()) == false) {
                event.getPlayer().sendMessage(ChatColor.RED + " * Proibido dropar itens aqui!");
                event.setCancelled(true);
            } else {
                event.setCancelled(false);
            }
        }
    }

    @EventHandler
    public void onMove(final PlayerMoveEvent event) {
        final Player player = event.getPlayer();
        final String UUID = player.getUniqueId().toString();

        if (playerInVoidArenaVip(player.getLocation()) == true) {
            if (player.isGliding() && player.getInventory().getChestplate() != null && player.getInventory().getChestplate().getType().equals(Material.ELYTRA)) {
                final ItemStack elytra = player.getInventory().getChestplate().clone();
                player.getInventory().setChestplate(new ItemStack(Material.AIR));

                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.getInventory().setChestplate(elytra);
                    }
                }, 5L);
            }
        }

        if (player.getWorld().getName().equalsIgnoreCase("guildaselect")) {
            if (player.getLocation().getY() < 0) {
                final String[] loc = RegionsSpawnAPI.getLocation("guildaselect").split(":");
                final Location location = new Location(Bukkit.getWorld("guildaselect"), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), Float.parseFloat(loc[4]), Float.parseFloat(loc[5]));
                player.teleport(location);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            }
        }

        if (player.getWorld().getName().equalsIgnoreCase("aether")) {
            if (player.getAllowFlight() == true) {
                if (!player.isOp()) {
                    final String tag = TagAPI.getTag(player.getUniqueId().toString());
                    if (tag != null && !(tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Moderador") || tag.equalsIgnoreCase("Admin"))) {
                        player.setAllowFlight(false);
                        ConfigAPI.updateFly(UUID, false);

                        if (JetpackHash.get(UUID) == null) {
                            new JetpackHash(UUID, false, false).insert();
                        } else {
                            JetpackHash.get(UUID).setStatus(false);
                        }
                    }
                }
            }
        }

        if (player.getWorld().getName().equalsIgnoreCase("spawn")) {
            if (PlayerInSpawnHash.get(UUID) == null) {
                new PlayerInSpawnHash(UUID, true).insert();
                final PotionEffect effectS = new PotionEffect(PotionEffectType.SLOW_FALLING, 999999, 1, true, false);
                player.addPotionEffect(effectS);
            } else {
                if (PlayerInSpawnHash.get(UUID).getStatus() == false) {
                    PlayerInSpawnHash.get(UUID).setStatus(true);
                    final PotionEffect effectS = new PotionEffect(PotionEffectType.SLOW_FALLING, 999999, 1, true, false);
                    player.addPotionEffect(effectS);
                }
            }
        } else if (PlayerInSpawnHash.get(UUID) != null && PlayerInSpawnHash.get(UUID).getStatus() == true) {
            PlayerInSpawnHash.get(UUID).setStatus(false);
            player.removePotionEffect(PotionEffectType.SLOW_FALLING);
        }
    }

    public static boolean playerInArenaVip(final Location loc) {
        final String[] pos1 = "spawn:228:0:88".split(":");
        final String[] pos2 = "spawn:387:151:248".split(":");

        if (!loc.getWorld().getName().equalsIgnoreCase(pos1[0])) return false;

        final int p1x = Integer.valueOf(pos1[1]);
        final int p1y = Integer.valueOf(pos1[2]);
        final int p1z = Integer.valueOf(pos1[3]);
        final int p2x = Integer.valueOf(pos2[1]);
        final int p2y = Integer.valueOf(pos2[2]);
        final int p2z = Integer.valueOf(pos2[3]);

        final int minX = p1x < p2x ? p1x : p2x;
        final int minY = p1y < p2y ? p1y : p2y;
        final int minZ = p1z < p2z ? p1z : p2z;

        final int maxX = p1x > p2x ? p1x : p2x;
        final int maxY = p1y > p2y ? p1y : p2y;
        final int maxZ = p1z > p2z ? p1z : p2z;

        return (loc.getBlockX() >= minX) && (loc.getBlockX() <= maxX) && (loc.getBlockY() >= minY)
                && (loc.getBlockY() <= maxY) && (loc.getBlockZ() >= minZ) && (loc.getBlockZ() <= maxZ);
    }

    public static boolean playerInVoidArenaVip(final Location loc) {
        final String[] pos1 = "spawn:228:-10000:88".split(":");
        final String[] pos2 = "spawn:387:151:248".split(":");

        if (!loc.getWorld().getName().equalsIgnoreCase(pos1[0])) return false;

        final int p1x = Integer.valueOf(pos1[1]);
        final int p1y = Integer.valueOf(pos1[2]);
        final int p1z = Integer.valueOf(pos1[3]);
        final int p2x = Integer.valueOf(pos2[1]);
        final int p2y = Integer.valueOf(pos2[2]);
        final int p2z = Integer.valueOf(pos2[3]);

        final int minX = p1x < p2x ? p1x : p2x;
        final int minY = p1y < p2y ? p1y : p2y;
        final int minZ = p1z < p2z ? p1z : p2z;

        final int maxX = p1x > p2x ? p1x : p2x;
        final int maxY = p1y > p2y ? p1y : p2y;
        final int maxZ = p1z > p2z ? p1z : p2z;

        return (loc.getBlockX() >= minX) && (loc.getBlockX() <= maxX) && (loc.getBlockY() >= minY)
                && (loc.getBlockY() <= maxY) && (loc.getBlockZ() >= minZ) && (loc.getBlockZ() <= maxZ);
    }
}
