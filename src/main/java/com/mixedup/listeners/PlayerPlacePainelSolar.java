package com.mixedup.listeners;

import com.mixedup.apis.PainelSolarAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.hashs.JetpackSaveHash;
import com.mixedup.plots.ilha.IlhaAPI;
import com.mixedup.plots.ilha.IlhaUtil;
import com.mixedup.plots.terrain.TerrainInfosAPI;
import com.mixedup.plots.terrain.TerrainsUtil;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class PlayerPlacePainelSolar implements Listener {

    @EventHandler
    public void onBreak(final BlockBreakEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        final ItemStack painel1 = new ItemStack(Material.DAYLIGHT_DETECTOR);
        final ItemMeta meta1 = painel1.getItemMeta();
        meta1.setDisplayName(ChatColor.YELLOW + "Painel solar");
        final List<String> lore1 = new ArrayList<>();
        lore1.add(" ");
        lore1.add(ChatColor.DARK_GRAY + "Modelo: " + ChatColor.RED + "100Wp");
        lore1.add(" ");
        meta1.setLore(lore1);
        painel1.setItemMeta(meta1);

        final ItemStack painel2 = new ItemStack(Material.DAYLIGHT_DETECTOR);
        final ItemMeta meta2 = painel2.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "Painel solar");
        final List<String> lore2 = new ArrayList<>();
        lore2.add(" ");
        lore2.add(ChatColor.DARK_GRAY + "Modelo: " + ChatColor.RED + "200Wp");
        lore2.add(" ");
        meta2.setLore(lore2);
        painel2.setItemMeta(meta2);

        final Block block = event.getBlock();
        if (block.getType().equals(Material.DAYLIGHT_DETECTOR)) {
            final String location = block.getWorld().getName() + ":" + block.getX() + ":" + block.getY() + ":" + block.getZ();
            if (PainelSolarAPI.getPainelExist(location) != null) {
                if (TerrainsUtil.playerInArea(event.getBlock().getLocation()) != null && !TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID().contains(UUID) && !TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).contains(UUID))
                    return;
                if (IlhaUtil.playerInArea(event.getBlock().getLocation()) != null && !IlhaUtil.playerInArea(event.getBlock().getLocation()).getRegionID().contains(UUID) && !IlhaAPI.getAmigos(IlhaUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).contains(UUID))
                    return;
                event.setCancelled(true);

                block.setType(Material.AIR);
                player.playSound(player.getLocation(), Sound.BLOCK_WOOD_BREAK, 1.0f, 1.0f);

                if (PainelSolarAPI.getForca(location).equals("100")) {
                    PainelSolarAPI.removePainel(location, UUID);
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.sendMessage(ChatColor.RED + " * Você acabou de remover e desativar um painel solar!");

                    final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(painel1);
                    for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                        player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                    }

                    if (JetpackSaveHash.get(location) != null) {
                        final HashMap<Integer, ItemStack> nope1 = player.getOpenInventory().getBottomInventory().addItem(JetpackSaveHash.get(location).getJetpack());
                        for (final Map.Entry<Integer, ItemStack> entry : nope1.entrySet()) {
                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                        }
                        JetpackLoaderEvent.removeJetpack(location);
                    }
                } else if (PainelSolarAPI.getForca(location).equals("200")) {
                    PainelSolarAPI.removePainel(location, UUID);
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.sendMessage(ChatColor.RED + " * Você acabou de remover e desativar um painel solar!");

                    final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(painel2);
                    for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                        player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                    }

                    if (JetpackSaveHash.get(location) != null) {
                        final HashMap<Integer, ItemStack> nope1 = player.getOpenInventory().getBottomInventory().addItem(JetpackSaveHash.get(location).getJetpack());
                        for (final Map.Entry<Integer, ItemStack> entry : nope1.entrySet()) {
                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                        }
                        JetpackLoaderEvent.removeJetpack(location);
                    }
                }
            } else {
                block.getDrops(new ItemStack(Material.DAYLIGHT_DETECTOR));
            }
        }
    }

    @EventHandler
    public void onPlace(final BlockPlaceEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        final ItemStack painel1 = new ItemStack(Material.DAYLIGHT_DETECTOR);
        final ItemMeta meta1 = painel1.getItemMeta();
        meta1.setDisplayName(ChatColor.YELLOW + "Painel solar");
        final List<String> lore1 = new ArrayList<>();
        lore1.add(" ");
        lore1.add(ChatColor.DARK_GRAY + "Modelo: " + ChatColor.RED + "100Wp");
        lore1.add(" ");
        meta1.setLore(lore1);
        painel1.setItemMeta(meta1);

        final ItemStack painel2 = new ItemStack(Material.DAYLIGHT_DETECTOR);
        final ItemMeta meta2 = painel2.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "Painel solar");
        final List<String> lore2 = new ArrayList<>();
        lore2.add(" ");
        lore2.add(ChatColor.DARK_GRAY + "Modelo: " + ChatColor.RED + "200Wp");
        lore2.add(" ");
        meta2.setLore(lore2);
        painel2.setItemMeta(meta2);

        final Block block = event.getBlock();
        final ItemStack item = event.getItemInHand();
        if (block.getType().equals(Material.DAYLIGHT_DETECTOR)) {
            if (item.getItemMeta().getLore() != null && item.getItemMeta().getDisplayName() != null && item.getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "Painel solar")) {
                if (item.getItemMeta().getLore().get(1).substring(item.getItemMeta().getLore().get(1).length() - 5).equalsIgnoreCase("100Wp")) {
                    if (PainelSolarAPI.getLastPainel(UUID) < 30) {
                        player.sendMessage(ChatColor.GREEN + " * Painel solar de 100Wp, acaba de ser ativado!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                        final String location = block.getWorld().getName() + ":" + block.getX() + ":" + block.getY() + ":" + block.getZ();
                        PainelSolarAPI.setPainel(UUID, location, "100", PainelSolarAPI.getLastPainel(UUID) + 1);
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, você atingiu o limite de paineis permitido por player!");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else if (item.getItemMeta().getLore().get(1).substring(item.getItemMeta().getLore().get(1).length() - 5).equalsIgnoreCase("200Wp")) {
                    if (PainelSolarAPI.getLastPainel(UUID) < 30) {
                        player.sendMessage(ChatColor.GREEN + " * Painel solar de 200Wp, acaba de ser ativado!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                        final String location = block.getWorld().getName() + ":" + block.getX() + ":" + block.getY() + ":" + block.getZ();
                        PainelSolarAPI.setPainel(UUID, location, "200", PainelSolarAPI.getLastPainel(UUID) + 1);
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, você atingiu o limite de paineis permitido por player!");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
            }
        }
    }
}
