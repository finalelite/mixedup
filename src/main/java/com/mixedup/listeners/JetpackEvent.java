package com.mixedup.listeners;

import com.mixedup.Main;
import com.mixedup.apis.HorarioAPI;
import com.mixedup.hashs.JetpackHash;
import net.minecraft.server.v1_13_R2.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerItemBreakEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.Date;

public class JetpackEvent implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        if (player.getInventory().getChestplate() != null) {
            org.bukkit.inventory.ItemStack chestplate = player.getInventory().getChestplate();

            if (chestplate.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "Jetpack") && JetpackHash.get(uuid) != null && JetpackHash.get(uuid).getStatus() == true) {
                if (event.getPlayer().getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
                    if (JetpackHash.get(uuid) != null && JetpackHash.get(uuid).getStatus() == true) {
                        if (!player.isOnGround()) {
                            PotionEffect effect = new PotionEffect(PotionEffectType.SLOW_FALLING, 600, 1, true, false);
                            player.addPotionEffect(effect);
                        }
                        JetpackHash.get(uuid).setStatus(false);
                        JetpackHash.get(uuid).setFix(true);
                        player.setAllowFlight(false);
                        player.setFlying(false);

                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                JetpackHash.get(uuid).setFix(false);
                            }
                        }, 20L);
                    }

                    player.sendMessage(ChatColor.RED + " * Ops, a elytra não funciona neste mundo!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }
                ItemStack nms = CraftItemStack.asNMSCopy(chestplate);
                NBTTagCompound compound = (nms.hasTag()) ? nms.getTag() : new NBTTagCompound();

                Date now = new Date();
                Date diff = new Date();
                diff.setTime((now.getTime() - compound.getLong("lastUse")));

                if (diff.getSeconds() >= 1) {
                    compound.set("energy", new NBTTagInt(compound.getInt("energy") - diff.getSeconds()));
                    compound.set("lastUse", new NBTTagLong(now.getTime()));
                    nms.setTag(compound);
                    chestplate = CraftItemStack.asBukkitCopy(nms);
                    player.getInventory().setChestplate(chestplate);
                } else return;

                if (compound.getInt("energy") <= 0) {
                    PotionEffect effect = new PotionEffect(PotionEffectType.SLOW_FALLING, 600, 1, true, false);
                    player.addPotionEffect(effect);
                    player.setAllowFlight(false);
                    player.setFlying(false);
                    JetpackHash.get(uuid).setStatus(false);

                    player.sendMessage(ChatColor.RED + " * SYSTEM SHUTDOWN... \nOps, a jetpack descarregou!");
                    player.playSound(player.getLocation(), Sound.ENTITY_ITEM_BREAK, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_FIREWORK_ROCKET_TWINKLE, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }

                player.getInventory().setChestplate(updateDurability(CraftItemStack.asBukkitCopy(nms), compound.getInt("energy")));
            }
        }
    }

    @EventHandler
    public void onSneak(PlayerToggleSneakEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        if (player.getInventory().getChestplate() != null) {
            org.bukkit.inventory.ItemStack chestplate = player.getInventory().getChestplate();

            if (chestplate.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "Jetpack")) {
                if (JetpackHash.get(uuid) == null) {
                    if (player.getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
                        player.sendMessage(ChatColor.RED + " * Ops, a elytra não funciona neste mundo!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        return;
                    }
                    new JetpackHash(uuid, true, true).insert();
                    player.setAllowFlight(true);
                    player.setFlying(true);
                    player.sendActionBar(ChatColor.GREEN + "JETPACK LIGADA!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_FIREWORK_ROCKET_TWINKLE, 1.0f, 1.0f);

                    ItemStack nms = CraftItemStack.asNMSCopy(chestplate);
                    NBTTagCompound compound = (nms.hasTag()) ? nms.getTag() : new NBTTagCompound();
                    compound.set("lastUse", new NBTTagFloat(new Date().getTime()));

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            JetpackHash.get(uuid).setFix(false);
                        }
                    }, 20L);
                } else if (JetpackHash.get(uuid) != null && JetpackHash.get(uuid).getStatus() == false && JetpackHash.get(uuid).getFix() == false) {
                    if (player.getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
                        player.sendMessage(ChatColor.RED + " * Ops, a elytra não funciona neste mundo!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        return;
                    }
                    JetpackHash.get(uuid).setStatus(true);
                    JetpackHash.get(uuid).setFix(true);
                    player.setAllowFlight(true);
                    player.setFlying(true);
                    player.sendActionBar(ChatColor.GREEN + "JETPACK LIGADA!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_FIREWORK_ROCKET_TWINKLE, 1.0f, 1.0f);

                    ItemStack nms = CraftItemStack.asNMSCopy(chestplate);
                    NBTTagCompound compound = (nms.hasTag()) ? nms.getTag() : new NBTTagCompound();
                    compound.set("lastUse", new NBTTagFloat(new Date().getTime()));

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            JetpackHash.get(uuid).setFix(false);
                        }
                    }, 20L);
                } else if (JetpackHash.get(uuid) != null && JetpackHash.get(uuid).getStatus() == true && JetpackHash.get(uuid).getFix() == false) {
                    if (player.isOnGround()) {
                        JetpackHash.get(uuid).setStatus(false);
                        JetpackHash.get(uuid).setFix(true);
                        player.setAllowFlight(false);
                        player.setFlying(false);
                        player.sendActionBar(ChatColor.RED + "JETPACK DESLIGADA!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_FIREWORK_ROCKET_TWINKLE, 1.0f, 1.0f);

                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                JetpackHash.get(uuid).setFix(false);
                            }
                        }, 20L);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onBreak(PlayerItemDamageEvent event) {
        if (event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "Jetpack")) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();
        String uuid = player.getUniqueId().toString();

        if (player.getInventory().getChestplate() != null && !player.getInventory().getChestplate().getType().equals(Material.AIR)) {
            if (JetpackHash.get(uuid) != null && JetpackHash.get(uuid).getStatus() == true) {
                JetpackHash.get(uuid).setStatus(false);
                JetpackHash.get(uuid).setFix(true);
                player.setAllowFlight(false);
                player.setFlying(false);

                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        JetpackHash.get(uuid).setFix(false);
                    }
                }, 20L);
            }
        }
    }

    @EventHandler
    public void onInteract(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        String uuid = player.getUniqueId().toString();

        if (event.getSlot() == 38 && event.getClickedInventory().getItem(38) != null && !event.getClickedInventory().getItem(38).getType().equals(Material.AIR)) {
            if (JetpackHash.get(uuid) != null && JetpackHash.get(uuid).getStatus() == true) {
                if (!player.isOnGround()) {
                    PotionEffect effect = new PotionEffect(PotionEffectType.SLOW_FALLING, 600, 1, true, false);
                    player.addPotionEffect(effect);
                }
                JetpackHash.get(uuid).setStatus(false);
                JetpackHash.get(uuid).setFix(true);
                player.setAllowFlight(false);
                player.setFlying(false);
                player.sendActionBar(ChatColor.RED + "JETPACK DESLIGADA!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_FIREWORK_ROCKET_TWINKLE, 1.0f, 1.0f);

                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        JetpackHash.get(uuid).setFix(false);
                    }
                }, 20L);
            }
        }
    }

    private static org.bukkit.inventory.ItemStack updateDurability(org.bukkit.inventory.ItemStack item, int activated) {
        short max = item.getType().getMaxDurability();
        short porcent = (short) (max / 100);
        int rest = 600 - activated;
        int a = 0;

        for (int i = 1; rest > 6; i++) {
            rest -= 6;
            a++;
        }

        ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.GRAY + "Para ativar/desativar aperte SHIFT,");
        lore.add(ChatColor.GRAY + "sobre o chão!");
        lore.add(" ");
        lore.add(ChatColor.WHITE + "Temporizador: " + ChatColor.GRAY + HorarioAPI.getTimeJetpack(activated));
        item.setLore(lore);

        short durability = item.getDurability();
        int b = 0;
        for (int i = 1; durability >= porcent; i++) {
            durability -= porcent;
            b++;
        }

        if (a > b) {
            item.setDurability((short) (item.getDurability() + (porcent * a)));
        }

        return item;
    }
}
