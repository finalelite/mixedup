package com.mixedup.listeners;

import com.mixedup.apis.HorarioAPI;
import com.mixedup.apis.PainelSolarAPI;
import com.mixedup.hashs.JetpackSaveHash;
import com.mixedup.hashs.PainelOpenHash;
import net.minecraft.server.v1_13_R2.NBTTagCompound;
import net.minecraft.server.v1_13_R2.NBTTagInt;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class JetpackLoaderEvent implements Listener {

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        Player player = (Player) event.getPlayer();
        String uuid = player.getUniqueId().toString();

        if (event.getInventory().getName().equalsIgnoreCase("Menu painel solar: ")) {
            if (!locations.contains(PainelOpenHash.get(uuid).getLocation())) {
                if (event.getInventory().getItem(11) != null && !event.getInventory().getItem(11).getType().equals(Material.AIR)) {
                    final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(event.getInventory().getItem(11));
                    for (final Map.Entry<Integer, ItemStack> entry : nope.entrySet()) {
                        player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                    }

                    if (JetpackSaveHash.get(PainelOpenHash.get(uuid).getLocation()) != null) {
                        JetpackSaveHash.cache.remove(PainelOpenHash.get(uuid).getLocation());
                    }
                }
            }
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && event.getClickedBlock() != null) {
            if (event.getClickedBlock().getType().equals(Material.DAYLIGHT_DETECTOR)) {
                String location = event.getClickedBlock().getWorld().getName() + ":" + event.getClickedBlock().getX() + ":" + event.getClickedBlock().getY() + ":" + event.getClickedBlock().getZ();
                if (PainelSolarAPI.getPainelExist(location) != null) {
                    if (PainelOpenHash.get(uuid) == null) {
                        new PainelOpenHash(uuid, location).insert();
                    } else {
                        PainelOpenHash.get(uuid).setLocation(location);
                    }

                    if (JetpackSaveHash.get(location) != null && !JetpackSaveHash.get(location).getPlayer().getName().equalsIgnoreCase(player.getName())) {
                        player.sendMessage(ChatColor.RED + " * Ops, este painel solar não pertence a você!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        return;
                    }

                    player.openInventory(inv(uuid));
                }
            }
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        String uuid = player.getUniqueId().toString();

        if (event.getClickedInventory() != null) {
            if (event.getClickedInventory().getName().equalsIgnoreCase("Menu painel solar: ")) {
                if (event.getSlot() == 11) {
                    if (event.getClickedInventory().getItem(11) != null && !event.getClickedInventory().getItem(11).getType().equals(Material.AIR)) {
                        if (JetpackSaveHash.get(PainelOpenHash.get(uuid).getLocation()) != null) {
                            removeJetpack(PainelOpenHash.get(uuid).getLocation());

                            player.sendMessage(ChatColor.RED + " \n * Jetpack removida!\n ");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.BLOCK_FIRE_EXTINGUISH, 1.0f, 1.0f);
                        }
                    }
                } else if (event.getSlot() == 15) {
                    event.setCancelled(true);
                    if (JetpackSaveHash.get(PainelOpenHash.get(uuid).getLocation()) == null) {
                        if (event.getClickedInventory().getItem(11) != null && !event.getClickedInventory().getItem(11).getType().equals(Material.AIR) &&
                                event.getClickedInventory().getItem(11).getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "Jetpack") && event.getClickedInventory().getItem(11).getType().equals(Material.GOLDEN_CHESTPLATE)) {
                            addJetpack(PainelOpenHash.get(uuid).getLocation(), event.getClickedInventory().getItem(11), player);

                            player.getOpenInventory().close();
                            player.sendMessage(ChatColor.YELLOW + " \nVossa jetpack iniciou o carregamento, dentro de\n5 minutos ela estará carregada!\n ");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.BLOCK_FIRE_EXTINGUISH, 1.0f, 1.0f);
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, adicione uma jetpack para proseguirmos!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, a jetpack já se encontra carregando!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else if (event.getSlot() == 16) {
                    event.setCancelled(true);
                    if (JetpackSaveHash.get(PainelOpenHash.get(uuid).getLocation()) == null) {
                        player.getOpenInventory().close();
                        player.sendMessage(ChatColor.RED + " * Ação cancelada!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        player.getOpenInventory().close();
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    event.setCancelled(true);
                }
            }
        }
    }

    public static ArrayList<String> locations = new ArrayList<>();

    public static void addJetpack(String location, ItemStack jetpack, Player player) {
        locations.add(location);
        new JetpackSaveHash(location, jetpack, player).insert();
    }

    public static void removeJetpack(String location) {
        JetpackSaveHash.cache.remove(location);
        if (locations.contains(location)) {
            locations.remove(location);
        }
    }

    public static void jetpackUpdater() {
        final Thread thread = new Thread(() -> {
            while (true) {
                try {
                    Thread.currentThread().sleep(1000 * 60);
                } catch (final Exception e) {
                    e.printStackTrace();
                }

                ArrayList<String> remove = new ArrayList<>();
                for (String location : locations) {
                    ItemStack jetpack = JetpackSaveHash.get(location).getJetpack();
                    net.minecraft.server.v1_13_R2.ItemStack nms = CraftItemStack.asNMSCopy(jetpack);
                    NBTTagCompound compound = (nms.hasTag()) ? nms.getTag() : new NBTTagCompound();
                    int energy = compound.getInt("energy");

                    if (energy < 600) {
                        if (energy <= 480) {
                            compound.set("energy", new NBTTagInt(compound.getInt("energy") + 120));
                            nms.setTag(compound);
                            JetpackSaveHash.get(location).setJetpack(updateDurability(CraftItemStack.asBukkitCopy(nms), compound.getInt("energy")));

                            if (compound.getInt("energy") == 600) {
                                remove.add(location);
                                if (JetpackSaveHash.get(location).getPlayer() != null && JetpackSaveHash.get(location).getPlayer().isOnline()) {
                                    JetpackSaveHash.get(location).getPlayer().sendMessage(ChatColor.YELLOW + " * Jetpack carregada!");
                                    JetpackSaveHash.get(location).getPlayer().playSound(JetpackSaveHash.get(location).getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                            }
                        } else {
                            compound.set("energy", new NBTTagInt(600));
                            nms.setTag(compound);
                            JetpackSaveHash.get(location).setJetpack(updateDurability(CraftItemStack.asBukkitCopy(nms), compound.getInt("energy")));

                            remove.add(location);
                            if (JetpackSaveHash.get(location).getPlayer() != null && JetpackSaveHash.get(location).getPlayer().isOnline()) {
                                JetpackSaveHash.get(location).getPlayer().sendMessage(ChatColor.YELLOW + " * Jetpack carregada!");
                                JetpackSaveHash.get(location).getPlayer().playSound(JetpackSaveHash.get(location).getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        }
                    }
                }

                for (String r : remove) {
                    locations.remove(r);
                }
                remove.clear();
            }
        });
        thread.setName("jetpack-thread");
        thread.start();
    }

    private static org.bukkit.inventory.ItemStack updateDurability(org.bukkit.inventory.ItemStack item, int activated) {
        short max = item.getType().getMaxDurability();
        short porcent = (short) (max / 100);
        int rest = 600 - activated;
        int a = 0;

        for (int i = 1; rest > 6; i++) {
            rest -= 6;
            a++;
        }

        ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.GRAY + "Para ativar/desativar aperte SHIFT,");
        lore.add(ChatColor.GRAY + "sobre o chão!");
        lore.add(" ");
        lore.add(ChatColor.WHITE + "Temporizador: " + ChatColor.GRAY + HorarioAPI.getTimeJetpack(activated));
        item.setLore(lore);

        short durability = item.getDurability();
        int b = 0;
        for (int i = 1; durability >= porcent; i++) {
            durability -= porcent;
            b++;
        }

        if (a > b) {
            item.setDurability((short) (item.getDurability() - (porcent * a)));
        }

        return item;
    }

    private static Inventory inv(String uuid) {
        Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Menu painel solar: ");

        ItemStack anything = new ItemStack(Material.GRAY_STAINED_GLASS_PANE);
        ItemMeta metaAnything = anything.getItemMeta();
        metaAnything.setDisplayName(" ");
        anything.setItemMeta(metaAnything);

        int[] glass = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26};
        for (int s : glass) {
            inventory.setItem(s, anything);
        }

        String location = PainelOpenHash.get(uuid).getLocation();
        if (JetpackSaveHash.get(location) != null) {
            inventory.setItem(11, JetpackSaveHash.get(location).getJetpack());
        }

        final ItemStack confirm = new ItemStack(Material.MUSIC_DISC_CHIRP);
        final ItemMeta metaConfirm = confirm.getItemMeta();
        metaConfirm.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        metaConfirm.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        metaConfirm.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        metaConfirm.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        metaConfirm.addItemFlags(ItemFlag.HIDE_DESTROYS);
        metaConfirm.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        metaConfirm.setDisplayName(ChatColor.GREEN + "Confirmar");
        ArrayList<String> loreConfirm = new ArrayList<>();
        loreConfirm.add(" ");
        loreConfirm.add(ChatColor.GRAY + "Ao confirmar a jetpack começara a carregar,");
        loreConfirm.add(ChatColor.GRAY + "tal processo levará em média 10 minutos!");
        loreConfirm.add(" ");
        metaConfirm.setLore(loreConfirm);
        confirm.setItemMeta(metaConfirm);
        inventory.setItem(15, confirm);

        final ItemStack cancel = new ItemStack(Material.MUSIC_DISC_MELLOHI);
        final ItemMeta metaCancel = cancel.getItemMeta();
        metaCancel.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        metaCancel.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        metaCancel.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        metaCancel.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        metaCancel.addItemFlags(ItemFlag.HIDE_DESTROYS);
        metaCancel.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        metaCancel.setDisplayName(ChatColor.RED + "Cancelar");
        ArrayList<String> loreCancel = new ArrayList<>();
        loreCancel.add(" ");
        loreCancel.add(ChatColor.GRAY + "Ao cancelar este inventario será fechado,");
        loreCancel.add(ChatColor.GRAY + "e a ação cancelada!");
        loreCancel.add(" ");
        metaCancel.setLore(loreCancel);
        cancel.setItemMeta(metaCancel);
        inventory.setItem(16, cancel);

        return inventory;
    }
}
