package com.mixedup.listeners;

import com.mixedup.factions.FacAPI;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityCombustEvent;

import java.util.Random;

public class MobsSpawnAtDay implements Listener {

    private static EntityType entityRandom() {
        EntityType entityType = null;

        final Random random = new Random();
        final int rd = random.nextInt(9);
        if (rd == 1 || rd == 0) {
            entityType = EntityType.ZOMBIE;
        }
        if (rd == 2) {
            entityType = EntityType.SKELETON;
        }
        if (rd == 3) {
            entityType = EntityType.CREEPER;
        }
        if (rd == 4) {
            entityType = EntityType.WITHER_SKELETON;
        }
        if (rd == 5) {
            entityType = EntityType.SPIDER;
        }
        if (rd == 6) {
            entityType = EntityType.WITHER_SKELETON;
        }
        if (rd == 7) {
            entityType = EntityType.SKELETON;
        }
        if (rd == 8) {
            entityType = EntityType.ZOMBIE;
        }
        if (rd == 9) {
            entityType = EntityType.ZOMBIE;
        }
        return entityType;
    }

    @EventHandler
    public void onBurnMob(final EntityCombustEvent event) {
        if (event.getEntityType().equals(EntityType.ZOMBIE) || event.getEntityType().equals(EntityType.SKELETON) || event.getEntityType().equals(EntityType.CREEPER) || event.getEntityType().equals(EntityType.WITHER_SKELETON) || event.getEntityType().equals(EntityType.SPIDER)
                || event.getEntityType().equals(EntityType.PHANTOM)) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void creatureSpawn(final CreatureSpawnEvent event) {
        if (event.getLocation().getWorld().getName().equals("Koi-3010.01") || event.getLocation().getWorld().getName().equals("Trappist-1b")) {
            final String chunk = event.getLocation().getChunk().getX() + ":" + event.getLocation().getChunk().getZ();
            if (event.getEntityType().equals(EntityType.PLAYER)) return;
            if (!event.getSpawnReason().equals(SpawnReason.SPAWNER) && !event.getSpawnReason().equals(SpawnReason.CUSTOM)) {
                if (FacAPI.getChunkOwn(chunk) != null) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onSpawn(final CreatureSpawnEvent event) {
        if (event.getSpawnReason().equals(SpawnReason.SPAWNER) || event.getSpawnReason().equals(SpawnReason.CUSTOM)) return;
        if (event.getLocation().getWorld().getName().equals("Koi-3010.01") || event.getLocation().getWorld().getName().equals("Trappist-1b")) {
            if (event.getEntityType().equals(EntityType.CHICKEN) || event.getEntityType().equals(EntityType.PIG) || event.getEntityType().equals(EntityType.RABBIT) || event.getEntityType().equals(EntityType.SHEEP) || event.getEntityType().equals(EntityType.COW)
                    || event.getEntityType().equals(EntityType.BAT) || event.getEntityType().equals(EntityType.LLAMA) || event.getEntityType().equals(EntityType.HORSE) || event.getEntityType().equals(EntityType.MULE) || event.getEntityType().equals(EntityType.OCELOT)
                    || event.getEntityType().equals(EntityType.WOLF) || event.getEntityType().equals(EntityType.POLAR_BEAR)) {
                final Location location = event.getLocation().clone();
                location.getWorld().spawnEntity(location, entityRandom());
                event.setCancelled(true);
            }
        }
    }
}
