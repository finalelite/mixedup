package com.mixedup.listeners;

import com.mixedup.Main;
import com.mixedup.apis.HorarioAPI;
import com.mixedup.apis.TagAPI;
import com.mixedup.hashs.DailyAwardsHash;
import com.mixedup.hashs.DailyHash;
import net.minecraft.server.v1_13_R2.NBTTagCompound;
import net.minecraft.server.v1_13_R2.NBTTagInt;
import org.bukkit.*;
import org.bukkit.block.Chest;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class DailyAwardsEvent implements Listener {

    @EventHandler
    public void onCloseInventory(final InventoryCloseEvent event) {
        if (event.getPlayer() instanceof Player) {
            Player player = (Player) event.getPlayer();

            if (event.getInventory() != null && event.getInventory().getName().equalsIgnoreCase("Premiação diária: ")) {
                for (int i = 1; i <= event.getInventory().getSize(); i++) {
                    if (event.getInventory().getItem(i - 1) != null && !event.getInventory().getItem(i - 1).equals(Material.AIR)) {
                        final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(event.getInventory().getItem(i - 1));
                        for (final Map.Entry<Integer, ItemStack> entry : nope.entrySet()) {
                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore.add(ChatColor.YELLOW + "50% sucesso");
        lore.add(" ");
        metaChave.setLore(lore);
        chave.setItemMeta(metaChave);

        final ItemStack caixa = new ItemStack(Material.CHEST);
        final ItemMeta metaCaixa = caixa.getItemMeta();
        metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Básica");
        lore1.add(" ");
        metaCaixa.setLore(lore1);
        caixa.setItemMeta(metaCaixa);

        final ItemStack caixaAva = new ItemStack(Material.ENDER_CHEST);
        final ItemMeta metaCaixaAva = caixaAva.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore3 = new ArrayList<String>();
        lore3.add(" ");
        lore3.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Avançada");
        lore3.add(" ");
        metaCaixaAva.setLore(lore3);
        caixaAva.setItemMeta(metaCaixaAva);

        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            String basic = "spawn:16:65:-20";
            String vip = "spawn:308:153:175";

            String loc = event.getClickedBlock().getLocation().getWorld().getName() + ":" + event.getClickedBlock().getLocation().getBlockX() + ":" + event.getClickedBlock().getLocation().getBlockY() + ":" + event.getClickedBlock().getLocation().getBlockZ();
            if (loc.equalsIgnoreCase(basic)) {
                Chest chest = (Chest) event.getClickedBlock().getState();

                boolean locked = true;
                if (DailyHash.get(uuid) != null) {
                    if ((DailyHash.get(uuid).getTime() - new Date().getTime()) <= 0) {
                        locked = false;

                        Date date = new Date();
                        date.setHours(date.getHours() + 24);
                        DailyHash.updateDaily(uuid, date.getTime());
                    }
                } else {
                    locked = false;

                    Date date = new Date();
                    date.setHours(date.getHours() + 24);
                    DailyHash.setDaily(uuid, date.getTime());
                }

                if (locked == false) {
                    Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Premiação diária: ");
                    inventory.setItem(12, caixa);
                    inventory.setItem(14, chave);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(inventory);
                            player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                            setParticle(player);
                        }
                    }, 5L);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + " * Ops, você já pegou sua recompensa diária!\n   Libera em: " + ChatColor.GRAY + HorarioAPI.getTime((DailyHash.get(uuid).getTime() - new Date().getTime()) / 1000));
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }

            if (loc.equalsIgnoreCase(vip)) {
                Chest chest = (Chest) event.getClickedBlock().getState();

                boolean firstOpen = false;
                boolean locked = true;
                if (DailyHash.get(uuid + "VIP") != null) {
                    if ((DailyHash.get(uuid + "VIP").getTime() - new Date().getTime()) <= 0) {
                        locked = false;

                        Date date = new Date();
                        date.setHours(date.getHours() + 24);
                        DailyHash.updateDaily(uuid + "VIP", date.getTime());
                    }
                } else {
                    firstOpen = true;
                    locked = false;

                    Date date = new Date();
                    date.setHours(date.getHours() + 24);
                    DailyHash.setDaily(uuid + "VIP", date.getTime());
                }

                if (locked == false) {

                    String tag = TagAPI.getTag(player.getUniqueId().toString());
                    if (tag.equals("Vip")) {
                        chave.setAmount(2);
                    } else if (tag.equals("Vip+")) {
                        caixaAva.setAmount(2);
                        chave.setAmount(4);
                    } else if (tag.equals("Vip++")) {
                        caixaAva.setAmount(3);
                        chave.setAmount(6);
                    } else if (tag.equals("Vip+++")) {
                        caixaAva.setAmount(4);
                        chave.setAmount(8);
                    }

                    ItemStack jetpack = new ItemStack(Material.GOLDEN_CHESTPLATE);
                    ItemMeta meta = jetpack.getItemMeta();
                    meta.setDisplayName(ChatColor.YELLOW + "Jetpack");
                    ArrayList<String> loreJet = new ArrayList<>();
                    loreJet.add(" ");
                    loreJet.add(ChatColor.GRAY + "Para ativar/desativar aperte SHIFT,");
                    loreJet.add(ChatColor.GRAY + "sobre o chão!");
                    loreJet.add(" ");
                    meta.setLore(loreJet);
                    jetpack.setItemMeta(meta);

                    net.minecraft.server.v1_13_R2.ItemStack nms = CraftItemStack.asNMSCopy(jetpack);
                    NBTTagCompound compound = (nms.hasTag()) ? nms.getTag() : new NBTTagCompound();
                    compound.set("energy", new NBTTagInt(600));
                    nms.setTag(compound);

                    Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Premiação diária: ");
                    inventory.setItem(12, caixaAva);
                    inventory.setItem(14, chave);
                    if (firstOpen == true) {
                        inventory.setItem(13, CraftItemStack.asBukkitCopy(nms));
                    }

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(inventory);
                            player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                            setParticle(player);
                        }
                    }, 5L);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + " * Ops, você já pegou sua recompensa diária!\n   Libera em: " + ChatColor.GRAY + HorarioAPI.getTime((DailyHash.get(uuid).getTime() - new Date().getTime()) / 1000));
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }
        }
    }

    private static void setParticle(Player player) {
        new BukkitRunnable() {
            final Location loc = player.getLocation();
            double t;
            final double r = 2;

            public void run() {
                this.t = this.t + Math.PI / 16;
                final double x = this.r * Math.cos(this.t);
                final double y = this.r * Math.sin(this.t);
                final double z = this.r * Math.sin(this.t);
                this.loc.add(x, y, z);
                player.spawnParticle(Particle.PORTAL, this.loc, 1);
                this.loc.subtract(x, y, z);
                if (this.t > Math.PI * 8) {
                    cancel();
                }
            }
        }.runTaskTimer(Main.getInstance(), 0, 1);

        new BukkitRunnable() {
            final Location loc = player.getLocation();
            double t = 1;
            final double r = 2;

            public void run() {
                this.t = this.t + Math.PI / 16;
                final double x = this.r * Math.sin(this.t);
                final double y = this.r * Math.sin(this.t);
                final double z = this.r * Math.cos(this.t);
                this.loc.add(x, y, z);
                player.spawnParticle(Particle.PORTAL, this.loc, 1);
                this.loc.subtract(x, y, z);
                if (this.t > Math.PI * 8) {
                    cancel();
                }
            }
        }.runTaskTimer(Main.getInstance(), 0, 1);
    }
}
