package com.mixedup.listeners;

import com.mixedup.Main;
import com.mixedup.apis.ConfigAPI;
import com.mixedup.apis.LastPositionAPI;
import com.mixedup.apis.TagAPI;
import com.mixedup.plots.ilha.IlhaAPI;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Biome;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;

public class PlayerTeleportEvents implements Listener {

    @EventHandler
    public void onTeleport(final PlayerTeleportEvent event) {
        final Player player = event.getPlayer();
        final String UUID = player.getUniqueId().toString();
        final String loc = event.getFrom().getWorld().getName() + ":" + event.getFrom().getX() + ":" + event.getFrom().getY() + ":" + event.getFrom().getZ();

        if (player.hasMetadata("NPC")) return;

        if (playerInAreaVip(event.getTo()) == true && TagAPI.getTag(UUID) != null && TagAPI.getTag(UUID).equalsIgnoreCase("Membro")) {
            event.setCancelled(true);
            player.sendMessage(ChatColor.RED + " * Ops, você não pode acessar este local!");
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            return;
        }

        if (event.getTo().getWorld().getName().equalsIgnoreCase("ilhas-default")) {
            if (!event.getTo().getBlock().getBiome().equals(Biome.PLAINS)) {
                final int id = IlhaAPI.getIdIlha(UUID + "1");
                final Location location = new Location(event.getTo().getWorld(), 1000 * id - 650, 60, 350);

                int x = 0;
                for (int z = 0; z <= 300; z++) {
                    location.getBlock().setBiome(Biome.PLAINS);

                    location.add(0, 0, 1);
                    if (z == 300 && x <= 300) {
                        z = 0;
                        x++;
                        location.add(1, 0, -300);
                    } else if (x > 300) {
                        break;
                    }
                }
            }
        }

        LastPositionAPI.updateLocation(UUID, loc);

        Bukkit.getScheduler().scheduleSyncDelayedTask(Main.plugin, () -> {
            final val from = event.getFrom();
            final val to = event.getTo();

            if (from.getWorld().getName().equalsIgnoreCase("Trappist-1b") && !to.getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
                Main.scoreboardManager.setDefaultScoreboard(player);
            } else if (to.getWorld().getName().equalsIgnoreCase("Trappist-1b") && !from.getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
                Main.scoreboardManager.setSquadScoreboard(player);
            } else if (to.getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
                Main.scoreboardManager.updateSquadScoreboardTitle(player);
            }
        }, 40L);

        final String tag = TagAPI.getTag(player.getUniqueId().toString());
        if (tag != null && (tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Moderador") || tag.equalsIgnoreCase("Admin")))
            return;

        if (event.getTo().getWorld().getName().equalsIgnoreCase("ilhas") || event.getTo().getWorld().getName().equalsIgnoreCase("ilhas-default") || event.getTo().getWorld().getName().equalsIgnoreCase("espaconave")) {
            if (player.getAllowFlight()) {
                if (ConfigAPI.getFlyStatus(UUID)) {
                    player.setAllowFlight(false);
                    ConfigAPI.updateFly(UUID, false);
                }
            }
        }

        try {
            if (ConfigAPI.getFlyStatus(UUID)) {
                if (event.getTo().getWorld().getName().equalsIgnoreCase("spawn")) {
                    player.setAllowFlight(false);
                    ConfigAPI.updateFly(UUID, false);
                }
            }
        } catch (final Exception e) {
            return;
        }
    }

    public static boolean playerInAreaVip(final Location loc) {
        final String[] pos1 = "spawn:228:0:88".split(":");
        final String[] pos2 = "spawn:387:256:248".split(":");

        if (!loc.getWorld().getName().equalsIgnoreCase(pos1[0])) return false;

        final int p1x = Integer.valueOf(pos1[1]);
        final int p1y = Integer.valueOf(pos1[2]);
        final int p1z = Integer.valueOf(pos1[3]);
        final int p2x = Integer.valueOf(pos2[1]);
        final int p2y = Integer.valueOf(pos2[2]);
        final int p2z = Integer.valueOf(pos2[3]);

        final int minX = p1x < p2x ? p1x : p2x;
        final int minY = p1y < p2y ? p1y : p2y;
        final int minZ = p1z < p2z ? p1z : p2z;

        final int maxX = p1x > p2x ? p1x : p2x;
        final int maxY = p1y > p2y ? p1y : p2y;
        final int maxZ = p1z > p2z ? p1z : p2z;

        return (loc.getBlockX() >= minX) && (loc.getBlockX() <= maxX) && (loc.getBlockY() >= minY)
                && (loc.getBlockY() <= maxY) && (loc.getBlockZ() >= minZ) && (loc.getBlockZ() <= maxZ);
    }
}
