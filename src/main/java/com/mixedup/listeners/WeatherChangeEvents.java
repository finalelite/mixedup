package com.mixedup.listeners;

import com.mixedup.hashs.WorldTimeHash;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;

public class WeatherChangeEvents implements Listener {

    @EventHandler
    public void onChange(final WeatherChangeEvent event) {
        if (WorldTimeHash.get(event.getWorld().getName()) != null) {
            if (WorldTimeHash.get(event.getWorld().getName()).getWeather() == true) {
                event.setCancelled(true);
            }
        }
    }
}
