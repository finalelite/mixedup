package com.mixedup.listeners;

import lombok.val;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;

public class FixBorderByPass implements Listener {

    private static final Map<String, Integer> borders = new HashMap<>();

    static {
        FixBorderByPass.setBorder("world_nether", 15000);
        FixBorderByPass.setBorder("trappist-1b", 20000);
        FixBorderByPass.setBorder("gliesse-667-Cc", 30000);
    }

    private static void setBorder(final String mapName, final int coord) {
        FixBorderByPass.borders.put(mapName.toLowerCase(), coord);
    }

    private static boolean isNotValid(final double pos, final String worldName) {
        final val worldBorder = FixBorderByPass.borders.get(worldName);
        return !(pos < worldBorder && pos > (worldBorder * -1));
    }

    @EventHandler
    public void onBorderBypassByTeleport(final PlayerTeleportEvent event) {
        if (!FixBorderByPass.borders.containsKey(event.getTo().getWorld().getName().toLowerCase())) return;

        final Location to = event.getTo();
        if (FixBorderByPass.isNotValid(to.getX(), to.getWorld().getName().toLowerCase()) || FixBorderByPass.isNotValid(to.getZ(), to.getWorld().getName().toLowerCase())) {
            event.getPlayer().sendMessage(ChatColor.RED + "Você não pode teleportar para fora da borda.");
            if (event.getCause() == TeleportCause.ENDER_PEARL) {
                event.getPlayer().getInventory().addItem(new ItemStack(Material.ENDER_PEARL));
            }
            event.setCancelled(true);
        }
    }

}
