package com.mixedup.listeners;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import com.mixedup.hashs.LastHitHash;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import java.util.Date;

public class CancelCommands implements Listener {

    @EventHandler
    public void playerSayCommand(final PlayerCommandPreprocessEvent event) {

        final Player player = event.getPlayer();
        final String uuid = PlayerUUID.getUUID(player.getName());

        if (!(TagAPI.getTag(uuid).equalsIgnoreCase("Master") || TagAPI.getTag(uuid).equalsIgnoreCase("Supervisor") || TagAPI.getTag(uuid).equalsIgnoreCase("Admin"))) {

            if (event.getMessage().startsWith("/")) {

                final String cmd = event.getMessage().split(" ")[0];

                if (cmd.contains(":")) {

                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + " * Ops, você não pode executar esta ação.");

                } else {

                    if (cmd.equalsIgnoreCase("/help") || cmd.equalsIgnoreCase("/plugins") || cmd.equalsIgnoreCase("/me") || cmd.equalsIgnoreCase("/pl") || cmd.equalsIgnoreCase("/seed") || cmd.equalsIgnoreCase("/icanhasbukkit")) {

                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, você não pode executar esta ação.");


                    }

                }

            }

        }

        if (LastHitHash.get(uuid) != null) {
            final Date date = LastHitHash.get(uuid).getDate();
            final int value = date.compareTo(new Date());

            if (value > 0) {

                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + " * Ops, você está em combate, não é permitido isto!");
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);

            }

        }


    }
}