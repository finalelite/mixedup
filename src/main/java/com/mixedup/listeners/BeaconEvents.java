package com.mixedup.listeners;

import com.mixedup.Main;
import com.mixedup.apis.BeaconAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.factions.FacAPI;
import com.mixedup.hashs.BeaconID;
import com.mixedup.hashs.InventoryLocation;
import com.mixedup.hashs.RightClickBeacon;
import com.mixedup.utils.RemoveItemInv;
import org.bukkit.*;
import org.bukkit.block.Beacon;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkEffectMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import java.util.ArrayList;

public class BeaconEvents implements Listener {

    private static void applyEffects(final LivingEntity entity, final String loc) {
        if (BeaconAPI.getForcaActivated(loc) == true) {
            final PotionEffect potion = new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 300, BeaconAPI.getForcaActivatedLevel(loc) - 1, false, false, true);
            entity.addPotionEffect(potion);
        }
        if (BeaconAPI.getPressaActivated(loc) == true) {
            final PotionEffect potion = new PotionEffect(PotionEffectType.FAST_DIGGING, 300, BeaconAPI.getPressaActivatedLevel(loc) - 1, false, false, true);
            entity.addPotionEffect(potion);
        }
        if (BeaconAPI.getRegenActivated(loc) == true) {
            final PotionEffect potion = new PotionEffect(PotionEffectType.REGENERATION, 300, BeaconAPI.getRegenActivatedLevel(loc) - 1, false, false, true);
            entity.addPotionEffect(potion);
        }
        if (BeaconAPI.getJumpActivated(loc) == true) {
            final PotionEffect potion = new PotionEffect(PotionEffectType.JUMP, 300, BeaconAPI.getJumpActivatedLevel(loc) - 1, false, false, true);
            entity.addPotionEffect(potion);
        }
        if (BeaconAPI.getSpeedActivated(loc) == true) {
            final PotionEffect potion = new PotionEffect(PotionEffectType.SPEED, 300, BeaconAPI.getSpeedActivatedLevel(loc) - 1, false, false, true);
            entity.addPotionEffect(potion);
        }
    }

    private static Inventory invLevelsSpeed(final String location) {
        final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Efeito velocidade: ");

        final ItemStack playerDrop4 = new ItemStack(Material.FIREWORK_STAR, 1);
        final ItemMeta meta4 = playerDrop4.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Velocidade");
        meta4.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta4.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        meta4.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        meta4.addItemFlags(ItemFlag.HIDE_DESTROYS);
        meta4.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        meta4.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        final FireworkEffectMeta metaFw4 = (FireworkEffectMeta) meta4;
        final FireworkEffect aa4 = FireworkEffect.builder().withColor(Color.BLUE).build();
        metaFw4.setEffect(aa4);
        playerDrop4.setItemMeta(metaFw4);
        inventory.setItem(13, playerDrop4);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta = voltar.getItemMeta();
        meta.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta);
        inventory.setItem(49, voltar);

        final ItemStack enchAtivo3 = new ItemStack(Material.LIME_STAINED_GLASS);
        final ItemMeta metaEnchA3 = enchAtivo3.getItemMeta();
        metaEnchA3.setDisplayName(ChatColor.GREEN + "Nível 3");
        final ArrayList<String> lore7 = new ArrayList<>();
        lore7.add(" ");
        lore7.add(ChatColor.GRAY + "Ativo!");
        lore7.add(" ");
        metaEnchA3.setLore(lore7);
        enchAtivo3.setItemMeta(metaEnchA3);

        final ItemStack enchAtivo2 = new ItemStack(Material.LIME_STAINED_GLASS);
        final ItemMeta metaEnchA2 = enchAtivo2.getItemMeta();
        metaEnchA2.setDisplayName(ChatColor.GREEN + "Nível 2");
        final ArrayList<String> lore2 = new ArrayList<>();
        lore2.add(" ");
        lore2.add(ChatColor.GRAY + "Ativo!");
        lore2.add(" ");
        metaEnchA2.setLore(lore2);
        enchAtivo2.setItemMeta(metaEnchA2);

        final ItemStack enchAtivo = new ItemStack(Material.LIME_STAINED_GLASS);
        final ItemMeta metaEnchA = enchAtivo.getItemMeta();
        metaEnchA.setDisplayName(ChatColor.GREEN + "Nível 1");
        final ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.GRAY + "Ativo!");
        lore.add(" ");
        metaEnchA.setLore(lore);
        enchAtivo.setItemMeta(metaEnchA);

        final ItemStack enchDeslig3 = new ItemStack(Material.RED_STAINED_GLASS);
        final ItemMeta metaEnchD3 = enchDeslig3.getItemMeta();
        metaEnchD3.setDisplayName(ChatColor.RED + "Nível 3");
        final ArrayList<String> lore8 = new ArrayList<>();
        lore8.add(" ");
        lore8.add(ChatColor.GRAY + "Desativado!");
        lore8.add(" ");
        metaEnchD3.setLore(lore8);
        enchDeslig3.setItemMeta(metaEnchD3);

        final ItemStack enchDeslig2 = new ItemStack(Material.RED_STAINED_GLASS);
        final ItemMeta metaEnchD2 = enchDeslig2.getItemMeta();
        metaEnchD2.setDisplayName(ChatColor.RED + "Nível 2");
        final ArrayList<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.GRAY + "Desativado!");
        lore4.add(" ");
        metaEnchD2.setLore(lore4);
        enchDeslig2.setItemMeta(metaEnchD2);

        final ItemStack enchDeslig = new ItemStack(Material.RED_STAINED_GLASS);
        final ItemMeta metaEnchD = enchDeslig.getItemMeta();
        metaEnchD.setDisplayName(ChatColor.RED + "Nível 1");
        final ArrayList<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Desativado!");
        lore3.add(" ");
        metaEnchD.setLore(lore3);
        enchDeslig.setItemMeta(metaEnchD);

        final ItemStack enchNeutro3 = new ItemStack(Material.GRAY_STAINED_GLASS);
        final ItemMeta metaEnchN3 = enchNeutro3.getItemMeta();
        metaEnchN3.setDisplayName(ChatColor.GRAY + "Nível 3");
        final ArrayList<String> lore9 = new ArrayList<>();
        lore9.add(" ");
        lore9.add(ChatColor.GRAY + "Inadquirido!");
        lore9.add(" ");
        metaEnchD.setLore(lore9);
        enchNeutro3.setItemMeta(metaEnchN3);

        final ItemStack enchNeutro2 = new ItemStack(Material.GRAY_STAINED_GLASS);
        final ItemMeta metaEnchN2 = enchNeutro2.getItemMeta();
        metaEnchN2.setDisplayName(ChatColor.GRAY + "Nível 2");
        final ArrayList<String> lore6 = new ArrayList<>();
        lore6.add(" ");
        lore6.add(ChatColor.GRAY + "Inadquirido!");
        lore6.add(" ");
        metaEnchD.setLore(lore6);
        enchNeutro2.setItemMeta(metaEnchN2);

        final ItemStack enchNeutro = new ItemStack(Material.GRAY_STAINED_GLASS);
        final ItemMeta metaEnchN = enchNeutro.getItemMeta();
        metaEnchN.setDisplayName(ChatColor.GRAY + "Nível 1");
        final ArrayList<String> lore5 = new ArrayList<>();
        lore5.add(" ");
        lore5.add(ChatColor.GRAY + "Inadquirido!");
        lore5.add(" ");
        metaEnchD.setLore(lore5);
        enchNeutro.setItemMeta(metaEnchN);

        if (BeaconAPI.getSpeed(location) == 0) {
            inventory.setItem(30, enchNeutro);
            inventory.setItem(31, enchNeutro2);
            inventory.setItem(32, enchNeutro3);
        } else if (BeaconAPI.getSpeed(location) == 1) {
            if (BeaconAPI.getSpeedActivatedLevel(location) == 1) {
                inventory.setItem(30, enchAtivo);
            } else {
                inventory.setItem(30, enchDeslig);
            }
            inventory.setItem(31, enchNeutro2);
            inventory.setItem(32, enchNeutro3);
        } else if (BeaconAPI.getSpeed(location) == 2) {
            if (BeaconAPI.getSpeedActivatedLevel(location) == 1) {
                inventory.setItem(30, enchAtivo);
            } else {
                inventory.setItem(30, enchDeslig);
            }
            if (BeaconAPI.getSpeedActivatedLevel(location) == 2) {
                inventory.setItem(31, enchAtivo2);
            } else {
                inventory.setItem(31, enchDeslig2);
            }
            inventory.setItem(32, enchNeutro3);
        } else if (BeaconAPI.getSpeed(location) == 3) {
            if (BeaconAPI.getSpeedActivatedLevel(location) == 1) {
                inventory.setItem(30, enchAtivo);
            } else {
                inventory.setItem(30, enchDeslig);
            }
            if (BeaconAPI.getSpeedActivatedLevel(location) == 2) {
                inventory.setItem(31, enchAtivo2);
            } else {
                inventory.setItem(31, enchDeslig2);
            }
            if (BeaconAPI.getSpeedActivatedLevel(location) == 3) {
                inventory.setItem(32, enchAtivo3);
            } else {
                inventory.setItem(32, enchDeslig3);
            }
        }

        return inventory;
    }

    private static Inventory invLevelsJump(final String location) {
        final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Efeito super pulo: ");

        final ItemStack playerDrop4 = new ItemStack(Material.FIREWORK_STAR, 1);
        final ItemMeta meta4 = playerDrop4.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Super pulo");
        meta4.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta4.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        meta4.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        meta4.addItemFlags(ItemFlag.HIDE_DESTROYS);
        meta4.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        meta4.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        final FireworkEffectMeta metaFw4 = (FireworkEffectMeta) meta4;
        final FireworkEffect aa4 = FireworkEffect.builder().withColor(Color.LIME).build();
        metaFw4.setEffect(aa4);
        playerDrop4.setItemMeta(metaFw4);
        inventory.setItem(13, playerDrop4);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta = voltar.getItemMeta();
        meta.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta);
        inventory.setItem(49, voltar);

        final ItemStack enchAtivo3 = new ItemStack(Material.LIME_STAINED_GLASS);
        final ItemMeta metaEnchA3 = enchAtivo3.getItemMeta();
        metaEnchA3.setDisplayName(ChatColor.GREEN + "Nível 3");
        final ArrayList<String> lore7 = new ArrayList<>();
        lore7.add(" ");
        lore7.add(ChatColor.GRAY + "Ativo!");
        lore7.add(" ");
        metaEnchA3.setLore(lore7);
        enchAtivo3.setItemMeta(metaEnchA3);

        final ItemStack enchAtivo2 = new ItemStack(Material.LIME_STAINED_GLASS);
        final ItemMeta metaEnchA2 = enchAtivo2.getItemMeta();
        metaEnchA2.setDisplayName(ChatColor.GREEN + "Nível 2");
        final ArrayList<String> lore2 = new ArrayList<>();
        lore2.add(" ");
        lore2.add(ChatColor.GRAY + "Ativo!");
        lore2.add(" ");
        metaEnchA2.setLore(lore2);
        enchAtivo2.setItemMeta(metaEnchA2);

        final ItemStack enchAtivo = new ItemStack(Material.LIME_STAINED_GLASS);
        final ItemMeta metaEnchA = enchAtivo.getItemMeta();
        metaEnchA.setDisplayName(ChatColor.GREEN + "Nível 1");
        final ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.GRAY + "Ativo!");
        lore.add(" ");
        metaEnchA.setLore(lore);
        enchAtivo.setItemMeta(metaEnchA);

        final ItemStack enchDeslig3 = new ItemStack(Material.RED_STAINED_GLASS);
        final ItemMeta metaEnchD3 = enchDeslig3.getItemMeta();
        metaEnchD3.setDisplayName(ChatColor.RED + "Nível 3");
        final ArrayList<String> lore8 = new ArrayList<>();
        lore8.add(" ");
        lore8.add(ChatColor.GRAY + "Desativado!");
        lore8.add(" ");
        metaEnchD3.setLore(lore8);
        enchDeslig3.setItemMeta(metaEnchD3);

        final ItemStack enchDeslig2 = new ItemStack(Material.RED_STAINED_GLASS);
        final ItemMeta metaEnchD2 = enchDeslig2.getItemMeta();
        metaEnchD2.setDisplayName(ChatColor.RED + "Nível 2");
        final ArrayList<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.GRAY + "Desativado!");
        lore4.add(" ");
        metaEnchD2.setLore(lore4);
        enchDeslig2.setItemMeta(metaEnchD2);

        final ItemStack enchDeslig = new ItemStack(Material.RED_STAINED_GLASS);
        final ItemMeta metaEnchD = enchDeslig.getItemMeta();
        metaEnchD.setDisplayName(ChatColor.RED + "Nível 1");
        final ArrayList<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Desativado!");
        lore3.add(" ");
        metaEnchD.setLore(lore3);
        enchDeslig.setItemMeta(metaEnchD);

        final ItemStack enchNeutro3 = new ItemStack(Material.GRAY_STAINED_GLASS);
        final ItemMeta metaEnchN3 = enchNeutro3.getItemMeta();
        metaEnchN3.setDisplayName(ChatColor.GRAY + "Nível 3");
        final ArrayList<String> lore9 = new ArrayList<>();
        lore9.add(" ");
        lore9.add(ChatColor.GRAY + "Inadquirido!");
        lore9.add(" ");
        metaEnchD.setLore(lore9);
        enchNeutro3.setItemMeta(metaEnchN3);

        final ItemStack enchNeutro2 = new ItemStack(Material.GRAY_STAINED_GLASS);
        final ItemMeta metaEnchN2 = enchNeutro2.getItemMeta();
        metaEnchN2.setDisplayName(ChatColor.GRAY + "Nível 2");
        final ArrayList<String> lore6 = new ArrayList<>();
        lore6.add(" ");
        lore6.add(ChatColor.GRAY + "Inadquirido!");
        lore6.add(" ");
        metaEnchD.setLore(lore6);
        enchNeutro2.setItemMeta(metaEnchN2);

        final ItemStack enchNeutro = new ItemStack(Material.GRAY_STAINED_GLASS);
        final ItemMeta metaEnchN = enchNeutro.getItemMeta();
        metaEnchN.setDisplayName(ChatColor.GRAY + "Nível 1");
        final ArrayList<String> lore5 = new ArrayList<>();
        lore5.add(" ");
        lore5.add(ChatColor.GRAY + "Inadquirido!");
        lore5.add(" ");
        metaEnchD.setLore(lore5);
        enchNeutro.setItemMeta(metaEnchN);

        if (BeaconAPI.getJump(location) == 0) {
            inventory.setItem(30, enchNeutro);
            inventory.setItem(31, enchNeutro2);
            inventory.setItem(32, enchNeutro3);
        } else if (BeaconAPI.getJump(location) == 1) {
            if (BeaconAPI.getJumpActivatedLevel(location) == 1) {
                inventory.setItem(30, enchAtivo);
            } else {
                inventory.setItem(30, enchDeslig);
            }
            inventory.setItem(31, enchNeutro2);
            inventory.setItem(32, enchNeutro3);
        } else if (BeaconAPI.getJump(location) == 2) {
            if (BeaconAPI.getJumpActivatedLevel(location) == 1) {
                inventory.setItem(30, enchAtivo);
            } else {
                inventory.setItem(30, enchDeslig);
            }
            if (BeaconAPI.getJumpActivatedLevel(location) == 2) {
                inventory.setItem(31, enchAtivo2);
            } else {
                inventory.setItem(31, enchDeslig2);
            }
            inventory.setItem(32, enchNeutro3);
        } else if (BeaconAPI.getJump(location) == 3) {
            if (BeaconAPI.getJumpActivatedLevel(location) == 1) {
                inventory.setItem(30, enchAtivo);
            } else {
                inventory.setItem(30, enchDeslig);
            }
            if (BeaconAPI.getJumpActivatedLevel(location) == 2) {
                inventory.setItem(31, enchAtivo2);
            } else {
                inventory.setItem(31, enchDeslig2);
            }
            if (BeaconAPI.getJumpActivatedLevel(location) == 3) {
                inventory.setItem(32, enchAtivo3);
            } else {
                inventory.setItem(32, enchDeslig3);
            }
        }

        return inventory;
    }

    private static Inventory invLevelsRegen(final String location) {
        final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Efeito regeneração: ");

        final ItemStack playerDrop4 = new ItemStack(Material.FIREWORK_STAR, 1);
        final ItemMeta meta4 = playerDrop4.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Regeneração");
        meta4.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta4.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        meta4.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        meta4.addItemFlags(ItemFlag.HIDE_DESTROYS);
        meta4.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        meta4.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        final FireworkEffectMeta metaFw4 = (FireworkEffectMeta) meta4;
        final FireworkEffect aa4 = FireworkEffect.builder().withColor(Color.FUCHSIA).build();
        metaFw4.setEffect(aa4);
        playerDrop4.setItemMeta(metaFw4);
        inventory.setItem(13, playerDrop4);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta = voltar.getItemMeta();
        meta.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta);
        inventory.setItem(49, voltar);

        final ItemStack enchAtivo2 = new ItemStack(Material.LIME_STAINED_GLASS);
        final ItemMeta metaEnchA2 = enchAtivo2.getItemMeta();
        metaEnchA2.setDisplayName(ChatColor.GREEN + "Nível 2");
        final ArrayList<String> lore2 = new ArrayList<>();
        lore2.add(" ");
        lore2.add(ChatColor.GRAY + "Ativo!");
        lore2.add(" ");
        metaEnchA2.setLore(lore2);
        enchAtivo2.setItemMeta(metaEnchA2);

        final ItemStack enchAtivo = new ItemStack(Material.LIME_STAINED_GLASS);
        final ItemMeta metaEnchA = enchAtivo.getItemMeta();
        metaEnchA.setDisplayName(ChatColor.GREEN + "Nível 1");
        final ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.GRAY + "Ativo!");
        lore.add(" ");
        metaEnchA.setLore(lore);
        enchAtivo.setItemMeta(metaEnchA);

        final ItemStack enchDeslig2 = new ItemStack(Material.RED_STAINED_GLASS);
        final ItemMeta metaEnchD2 = enchDeslig2.getItemMeta();
        metaEnchD2.setDisplayName(ChatColor.RED + "Nível 2");
        final ArrayList<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.GRAY + "Desativado!");
        lore4.add(" ");
        metaEnchD2.setLore(lore4);
        enchDeslig2.setItemMeta(metaEnchD2);

        final ItemStack enchDeslig = new ItemStack(Material.RED_STAINED_GLASS);
        final ItemMeta metaEnchD = enchDeslig.getItemMeta();
        metaEnchD.setDisplayName(ChatColor.RED + "Nível 1");
        final ArrayList<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Desativado!");
        lore3.add(" ");
        metaEnchD.setLore(lore3);
        enchDeslig.setItemMeta(metaEnchD);

        final ItemStack enchNeutro2 = new ItemStack(Material.GRAY_STAINED_GLASS);
        final ItemMeta metaEnchN2 = enchNeutro2.getItemMeta();
        metaEnchN2.setDisplayName(ChatColor.GRAY + "Nível 2");
        final ArrayList<String> lore6 = new ArrayList<>();
        lore6.add(" ");
        lore6.add(ChatColor.GRAY + "Inadquirido!");
        lore6.add(" ");
        metaEnchD.setLore(lore6);
        enchNeutro2.setItemMeta(metaEnchN2);

        final ItemStack enchNeutro = new ItemStack(Material.GRAY_STAINED_GLASS);
        final ItemMeta metaEnchN = enchNeutro.getItemMeta();
        metaEnchN.setDisplayName(ChatColor.GRAY + "Nível 1");
        final ArrayList<String> lore5 = new ArrayList<>();
        lore5.add(" ");
        lore5.add(ChatColor.GRAY + "Inadquirido!");
        lore5.add(" ");
        metaEnchD.setLore(lore5);
        enchNeutro.setItemMeta(metaEnchN);

        if (BeaconAPI.getRegen(location) == 0) {
            inventory.setItem(30, enchNeutro);
            inventory.setItem(31, enchNeutro2);
        } else if (BeaconAPI.getRegen(location) == 1) {
            if (BeaconAPI.getRegenActivatedLevel(location) == 1) {
                inventory.setItem(30, enchAtivo);
            } else {
                inventory.setItem(30, enchDeslig);
            }
            inventory.setItem(31, enchNeutro2);
        } else if (BeaconAPI.getRegen(location) == 2) {
            if (BeaconAPI.getRegenActivatedLevel(location) == 1) {
                inventory.setItem(30, enchAtivo);
            } else {
                inventory.setItem(30, enchDeslig);
            }
            if (BeaconAPI.getRegenActivatedLevel(location) == 2) {
                inventory.setItem(31, enchAtivo2);
            } else {
                inventory.setItem(31, enchDeslig2);
            }
        }

        return inventory;
    }

    private static Inventory invLevelsPressa(final String location) {
        final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Efeito pressa: ");

        final ItemStack playerDrop4 = new ItemStack(Material.FIREWORK_STAR, 1);
        final ItemMeta meta4 = playerDrop4.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Pressa");
        meta4.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta4.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        meta4.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        meta4.addItemFlags(ItemFlag.HIDE_DESTROYS);
        meta4.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        meta4.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        final FireworkEffectMeta metaFw4 = (FireworkEffectMeta) meta4;
        final FireworkEffect aa4 = FireworkEffect.builder().withColor(Color.NAVY).build();
        metaFw4.setEffect(aa4);
        playerDrop4.setItemMeta(metaFw4);
        inventory.setItem(13, playerDrop4);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta = voltar.getItemMeta();
        meta.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta);
        inventory.setItem(49, voltar);

        final ItemStack enchAtivo3 = new ItemStack(Material.LIME_STAINED_GLASS);
        final ItemMeta metaEnchA3 = enchAtivo3.getItemMeta();
        metaEnchA3.setDisplayName(ChatColor.GREEN + "Nível 3");
        final ArrayList<String> lore7 = new ArrayList<>();
        lore7.add(" ");
        lore7.add(ChatColor.GRAY + "Ativo!");
        lore7.add(" ");
        metaEnchA3.setLore(lore7);
        enchAtivo3.setItemMeta(metaEnchA3);

        final ItemStack enchAtivo2 = new ItemStack(Material.LIME_STAINED_GLASS);
        final ItemMeta metaEnchA2 = enchAtivo2.getItemMeta();
        metaEnchA2.setDisplayName(ChatColor.GREEN + "Nível 2");
        final ArrayList<String> lore2 = new ArrayList<>();
        lore2.add(" ");
        lore2.add(ChatColor.GRAY + "Ativo!");
        lore2.add(" ");
        metaEnchA2.setLore(lore2);
        enchAtivo2.setItemMeta(metaEnchA2);

        final ItemStack enchAtivo = new ItemStack(Material.LIME_STAINED_GLASS);
        final ItemMeta metaEnchA = enchAtivo.getItemMeta();
        metaEnchA.setDisplayName(ChatColor.GREEN + "Nível 1");
        final ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.GRAY + "Ativo!");
        lore.add(" ");
        metaEnchA.setLore(lore);
        enchAtivo.setItemMeta(metaEnchA);

        final ItemStack enchDeslig3 = new ItemStack(Material.RED_STAINED_GLASS);
        final ItemMeta metaEnchD3 = enchDeslig3.getItemMeta();
        metaEnchD3.setDisplayName(ChatColor.RED + "Nível 3");
        final ArrayList<String> lore8 = new ArrayList<>();
        lore8.add(" ");
        lore8.add(ChatColor.GRAY + "Desativado!");
        lore8.add(" ");
        metaEnchD3.setLore(lore8);
        enchDeslig3.setItemMeta(metaEnchD3);

        final ItemStack enchDeslig2 = new ItemStack(Material.RED_STAINED_GLASS);
        final ItemMeta metaEnchD2 = enchDeslig2.getItemMeta();
        metaEnchD2.setDisplayName(ChatColor.RED + "Nível 2");
        final ArrayList<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.GRAY + "Desativado!");
        lore4.add(" ");
        metaEnchD2.setLore(lore4);
        enchDeslig2.setItemMeta(metaEnchD2);

        final ItemStack enchDeslig = new ItemStack(Material.RED_STAINED_GLASS);
        final ItemMeta metaEnchD = enchDeslig.getItemMeta();
        metaEnchD.setDisplayName(ChatColor.RED + "Nível 1");
        final ArrayList<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Desativado!");
        lore3.add(" ");
        metaEnchD.setLore(lore3);
        enchDeslig.setItemMeta(metaEnchD);

        final ItemStack enchNeutro3 = new ItemStack(Material.GRAY_STAINED_GLASS);
        final ItemMeta metaEnchN3 = enchNeutro3.getItemMeta();
        metaEnchN3.setDisplayName(ChatColor.GRAY + "Nível 3");
        final ArrayList<String> lore9 = new ArrayList<>();
        lore9.add(" ");
        lore9.add(ChatColor.GRAY + "Inadquirido!");
        lore9.add(" ");
        metaEnchD.setLore(lore9);
        enchNeutro3.setItemMeta(metaEnchN3);

        final ItemStack enchNeutro2 = new ItemStack(Material.GRAY_STAINED_GLASS);
        final ItemMeta metaEnchN2 = enchNeutro2.getItemMeta();
        metaEnchN2.setDisplayName(ChatColor.GRAY + "Nível 2");
        final ArrayList<String> lore6 = new ArrayList<>();
        lore6.add(" ");
        lore6.add(ChatColor.GRAY + "Inadquirido!");
        lore6.add(" ");
        metaEnchD.setLore(lore6);
        enchNeutro2.setItemMeta(metaEnchN2);

        final ItemStack enchNeutro = new ItemStack(Material.GRAY_STAINED_GLASS);
        final ItemMeta metaEnchN = enchNeutro.getItemMeta();
        metaEnchN.setDisplayName(ChatColor.GRAY + "Nível 1");
        final ArrayList<String> lore5 = new ArrayList<>();
        lore5.add(" ");
        lore5.add(ChatColor.GRAY + "Inadquirido!");
        lore5.add(" ");
        metaEnchD.setLore(lore5);
        enchNeutro.setItemMeta(metaEnchN);

        if (BeaconAPI.getPressa(location) == 0) {
            inventory.setItem(30, enchNeutro);
            inventory.setItem(31, enchNeutro2);
            inventory.setItem(32, enchNeutro3);
        } else if (BeaconAPI.getPressa(location) == 1) {
            if (BeaconAPI.getPressaActivatedLevel(location) == 1) {
                inventory.setItem(30, enchAtivo);
            } else {
                inventory.setItem(30, enchDeslig);
            }
            inventory.setItem(31, enchNeutro2);
            inventory.setItem(32, enchNeutro3);
        } else if (BeaconAPI.getPressa(location) == 2) {
            if (BeaconAPI.getPressaActivatedLevel(location) == 1) {
                inventory.setItem(30, enchAtivo);
            } else {
                inventory.setItem(30, enchDeslig);
            }
            if (BeaconAPI.getPressaActivatedLevel(location) == 2) {
                inventory.setItem(31, enchAtivo2);
            } else {
                inventory.setItem(31, enchDeslig2);
            }
            inventory.setItem(32, enchNeutro3);
        } else if (BeaconAPI.getPressa(location) == 3) {
            if (BeaconAPI.getPressaActivatedLevel(location) == 1) {
                inventory.setItem(30, enchAtivo);
            } else {
                inventory.setItem(30, enchDeslig);
            }
            if (BeaconAPI.getPressaActivatedLevel(location) == 2) {
                inventory.setItem(31, enchAtivo2);
            } else {
                inventory.setItem(31, enchDeslig2);
            }
            if (BeaconAPI.getPressaActivatedLevel(location) == 3) {
                inventory.setItem(32, enchAtivo3);
            } else {
                inventory.setItem(32, enchDeslig3);
            }
        }

        return inventory;
    }

    private static Inventory invLevelsForca(final String location) {
        final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Efeito força: ");

        final ItemStack playerDrop4 = new ItemStack(Material.FIREWORK_STAR, 1);
        final ItemMeta meta4 = playerDrop4.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Força");
        meta4.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta4.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        meta4.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        meta4.addItemFlags(ItemFlag.HIDE_DESTROYS);
        meta4.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        meta4.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        final FireworkEffectMeta metaFw4 = (FireworkEffectMeta) meta4;
        final FireworkEffect aa4 = FireworkEffect.builder().withColor(Color.RED).build();
        metaFw4.setEffect(aa4);
        playerDrop4.setItemMeta(metaFw4);
        inventory.setItem(13, playerDrop4);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta = voltar.getItemMeta();
        meta.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta);
        inventory.setItem(49, voltar);

        final ItemStack enchAtivo2 = new ItemStack(Material.LIME_STAINED_GLASS);
        final ItemMeta metaEnchA2 = enchAtivo2.getItemMeta();
        metaEnchA2.setDisplayName(ChatColor.GREEN + "Nível 2");
        final ArrayList<String> lore2 = new ArrayList<>();
        lore2.add(" ");
        lore2.add(ChatColor.GRAY + "Ativo!");
        lore2.add(" ");
        metaEnchA2.setLore(lore2);
        enchAtivo2.setItemMeta(metaEnchA2);

        final ItemStack enchAtivo = new ItemStack(Material.LIME_STAINED_GLASS);
        final ItemMeta metaEnchA = enchAtivo.getItemMeta();
        metaEnchA.setDisplayName(ChatColor.GREEN + "Nível 1");
        final ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.GRAY + "Ativo!");
        lore.add(" ");
        metaEnchA.setLore(lore);
        enchAtivo.setItemMeta(metaEnchA);

        final ItemStack enchDeslig2 = new ItemStack(Material.RED_STAINED_GLASS);
        final ItemMeta metaEnchD2 = enchDeslig2.getItemMeta();
        metaEnchD2.setDisplayName(ChatColor.RED + "Nível 2");
        final ArrayList<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.GRAY + "Desativado!");
        lore4.add(" ");
        metaEnchD2.setLore(lore4);
        enchDeslig2.setItemMeta(metaEnchD2);

        final ItemStack enchDeslig = new ItemStack(Material.RED_STAINED_GLASS);
        final ItemMeta metaEnchD = enchDeslig.getItemMeta();
        metaEnchD.setDisplayName(ChatColor.RED + "Nível 1");
        final ArrayList<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Desativado!");
        lore3.add(" ");
        metaEnchD.setLore(lore3);
        enchDeslig.setItemMeta(metaEnchD);

        final ItemStack enchNeutro2 = new ItemStack(Material.GRAY_STAINED_GLASS);
        final ItemMeta metaEnchN2 = enchNeutro2.getItemMeta();
        metaEnchN2.setDisplayName(ChatColor.GRAY + "Nível 2");
        final ArrayList<String> lore6 = new ArrayList<>();
        lore6.add(" ");
        lore6.add(ChatColor.GRAY + "Inadquirido!");
        lore6.add(" ");
        metaEnchD.setLore(lore6);
        enchNeutro2.setItemMeta(metaEnchN2);

        final ItemStack enchNeutro = new ItemStack(Material.GRAY_STAINED_GLASS);
        final ItemMeta metaEnchN = enchNeutro.getItemMeta();
        metaEnchN.setDisplayName(ChatColor.GRAY + "Nível 1");
        final ArrayList<String> lore5 = new ArrayList<>();
        lore5.add(" ");
        lore5.add(ChatColor.GRAY + "Inadquirido!");
        lore5.add(" ");
        metaEnchD.setLore(lore5);
        enchNeutro.setItemMeta(metaEnchN);

        if (BeaconAPI.getForca(location) == 0) {
            inventory.setItem(30, enchNeutro);
            inventory.setItem(31, enchNeutro2);
        } else if (BeaconAPI.getForca(location) == 1) {
            if (BeaconAPI.getForcaActivatedLevel(location) == 1) {
                inventory.setItem(30, enchAtivo);
            } else {
                inventory.setItem(30, enchDeslig);
            }
            inventory.setItem(31, enchNeutro2);
        } else if (BeaconAPI.getForca(location) == 2) {
            if (BeaconAPI.getForcaActivatedLevel(location) == 1) {
                inventory.setItem(30, enchAtivo);
            } else {
                inventory.setItem(30, enchDeslig);
            }
            if (BeaconAPI.getForcaActivatedLevel(location) == 2) {
                inventory.setItem(31, enchAtivo2);
            } else {
                inventory.setItem(31, enchDeslig2);
            }
        }

        return inventory;
    }

    private static Inventory invOscilar(final String location) {
        final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Oscilar efeitos: ");

        final ItemStack oscilar = new ItemStack(Material.NETHER_STAR);
        final ItemMeta metaOsci = oscilar.getItemMeta();
        metaOsci.setDisplayName(ChatColor.YELLOW + "Oscilar efeitos");
        oscilar.setItemMeta(metaOsci);

        final ItemStack ativo = new ItemStack(Material.LIME_STAINED_GLASS);
        final ItemMeta metaAtivo = ativo.getItemMeta();
        metaAtivo.setDisplayName(ChatColor.GREEN + "ATIVO");
        ativo.setItemMeta(metaAtivo);

        final ItemStack desativado = new ItemStack(Material.RED_STAINED_GLASS);
        final ItemMeta metaDesa = desativado.getItemMeta();
        metaDesa.setDisplayName(ChatColor.RED + "DESATIVADO");
        desativado.setItemMeta(metaDesa);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta = voltar.getItemMeta();
        meta.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta);

        inventory.setItem(13, oscilar);
        inventory.setItem(49, voltar);

        if (BeaconAPI.getOscilar(location) == true) {
            inventory.setItem(31, ativo);
        } else {
            inventory.setItem(31, desativado);
        }

        return inventory;
    }

    private static Inventory invMultiplos(final String UUID) {
        final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Multiplos efeitos: ");

        final ItemStack multi = new ItemStack(Material.COMPARATOR);
        final ItemMeta metaMulti = multi.getItemMeta();
        metaMulti.setDisplayName(ChatColor.YELLOW + "Multiplos efeitos");
        final ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.DARK_GRAY + "Efeitos adquiridos: " + ChatColor.GRAY + BeaconAPI.getMultiplos(InventoryLocation.get(UUID).getLocation()));
        lore.add(" ");
        metaMulti.setLore(lore);
        multi.setItemMeta(metaMulti);
        inventory.setItem(13, multi);

        final ItemStack enchAtivo = new ItemStack(Material.LIME_STAINED_GLASS);
        final ItemMeta metaEnchA = enchAtivo.getItemMeta();
        metaEnchA.setDisplayName(ChatColor.GREEN + "Efeito ativo!");
        enchAtivo.setItemMeta(metaEnchA);

        final ItemStack enchDeslig = new ItemStack(Material.RED_STAINED_GLASS);
        final ItemMeta metaEnchD = enchDeslig.getItemMeta();
        metaEnchD.setDisplayName(ChatColor.RED + "Efeito desativado!");
        enchDeslig.setItemMeta(metaEnchD);

        final ItemStack enchNeutro = new ItemStack(Material.GRAY_STAINED_GLASS);
        final ItemMeta metaEnchN = enchNeutro.getItemMeta();
        metaEnchN.setDisplayName(ChatColor.GRAY + "Inadquirido.");
        enchNeutro.setItemMeta(metaEnchN);

        if (BeaconAPI.getForca(InventoryLocation.get(UUID).getLocation()) == 0) {
            inventory.setItem(29, enchNeutro);
        } else {
            if (BeaconAPI.getForcaActivated(InventoryLocation.get(UUID).getLocation()) == false) {
                inventory.setItem(29, enchDeslig);
            } else {
                inventory.setItem(29, enchAtivo);
            }
        }

        if (BeaconAPI.getPressa(InventoryLocation.get(UUID).getLocation()) == 0) {
            inventory.setItem(30, enchNeutro);
        } else {
            if (BeaconAPI.getPressaActivated(InventoryLocation.get(UUID).getLocation()) == false) {
                inventory.setItem(30, enchDeslig);
            } else {
                inventory.setItem(30, enchAtivo);
            }
        }

        if (BeaconAPI.getRegen(InventoryLocation.get(UUID).getLocation()) == 0) {
            inventory.setItem(31, enchNeutro);
        } else {
            if (BeaconAPI.getRegenActivated(InventoryLocation.get(UUID).getLocation()) == false) {
                inventory.setItem(31, enchDeslig);
            } else {
                inventory.setItem(31, enchAtivo);
            }
        }

        if (BeaconAPI.getJump(InventoryLocation.get(UUID).getLocation()) == 0) {
            inventory.setItem(32, enchNeutro);
        } else {
            if (BeaconAPI.getJumpActivated(InventoryLocation.get(UUID).getLocation()) == false) {
                inventory.setItem(32, enchDeslig);
            } else {
                inventory.setItem(32, enchAtivo);
            }
        }

        if (BeaconAPI.getSpeed(InventoryLocation.get(UUID).getLocation()) == 0) {
            inventory.setItem(33, enchNeutro);
        } else {
            if (BeaconAPI.getSpeedActivated(InventoryLocation.get(UUID).getLocation()) == false) {
                inventory.setItem(33, enchDeslig);
            } else {
                inventory.setItem(33, enchAtivo);
            }
        }

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta metaVoltar = voltar.getItemMeta();
        metaVoltar.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(metaVoltar);
        inventory.setItem(49, voltar);

        return inventory;
    }

    private static Inventory beacon(final String location) {
        final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Sinalizador: ");

        final ItemStack ativo = new ItemStack(Material.ENDER_PEARL);
        final ItemMeta metaAtivo = ativo.getItemMeta();
        metaAtivo.setDisplayName(ChatColor.GREEN + " (!)Ativado");
        ativo.setItemMeta(metaAtivo);

        final ItemStack desativo = new ItemStack(Material.FIREWORK_STAR);
        final ItemMeta metaDesa = desativo.getItemMeta();
        metaDesa.setDisplayName(ChatColor.RED + " (!)Desativado");
        desativo.setItemMeta(metaDesa);

        if (BeaconAPI.getActive(location) == true) {
            inventory.setItem(4, ativo);
        } else {
            inventory.setItem(4, desativo);
        }

        final ItemStack multi = new ItemStack(Material.COMPARATOR);
        final ItemMeta metaMulti = multi.getItemMeta();
        metaMulti.setDisplayName(ChatColor.YELLOW + "Múltiplos efeitos");
        multi.setItemMeta(metaMulti);
        inventory.setItem(21, multi);

        final ItemStack oscilar = new ItemStack(Material.NETHER_STAR);
        final ItemMeta metaOsci = oscilar.getItemMeta();
        metaOsci.setDisplayName(ChatColor.YELLOW + "Oscilar efeitos");
        oscilar.setItemMeta(metaOsci);
        inventory.setItem(23, oscilar);

        final ItemStack enchAtivo = new ItemStack(Material.LIME_STAINED_GLASS);
        final ItemMeta metaEnchA = enchAtivo.getItemMeta();
        metaEnchA.setDisplayName(ChatColor.GREEN + "Efeito ativo!");
        enchAtivo.setItemMeta(metaEnchA);

        final ItemStack enchDeslig = new ItemStack(Material.RED_STAINED_GLASS);
        final ItemMeta metaEnchD = enchDeslig.getItemMeta();
        metaEnchD.setDisplayName(ChatColor.RED + "Efeito desativado!");
        enchDeslig.setItemMeta(metaEnchD);

        final ItemStack enchNeutro = new ItemStack(Material.GRAY_STAINED_GLASS);
        final ItemMeta metaEnchN = enchNeutro.getItemMeta();
        metaEnchN.setDisplayName(ChatColor.GRAY + "Inadquirido.");
        enchNeutro.setItemMeta(metaEnchN);

        final Potion potion = new Potion(PotionType.STRENGTH);
        final ItemStack strength = potion.toItemStack(1);
        final ItemMeta metaStren = strength.getItemMeta();
        metaStren.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        metaStren.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        metaStren.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        metaStren.addItemFlags(ItemFlag.HIDE_DESTROYS);
        metaStren.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        metaStren.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        metaStren.setDisplayName(ChatColor.YELLOW + "Força");
        if (BeaconAPI.getForcaActivatedLevel(location) > 0) {
            final ArrayList<String> lore = new ArrayList<>();
            lore.add(" ");
            lore.add(ChatColor.GREEN + "Nível: " + ChatColor.GRAY + BeaconAPI.getForcaActivatedLevel(location) + " ativo!");
            lore.add(" ");
            metaStren.setLore(lore);
        }
        strength.setItemMeta(metaStren);
        inventory.setItem(38, strength);

        if (BeaconAPI.getForca(location) == 0) {
            inventory.setItem(47, enchNeutro);
        } else {
            if (BeaconAPI.getForcaActivated(location) == false) {
                inventory.setItem(47, enchDeslig);
            } else {
                inventory.setItem(47, enchAtivo);
            }
        }

        final Potion potion2 = new Potion(PotionType.SPEED);
        final ItemStack haste = potion2.toItemStack(1);
        final ItemMeta metaHast = haste.getItemMeta();
        metaHast.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        metaHast.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        metaHast.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        metaHast.addItemFlags(ItemFlag.HIDE_DESTROYS);
        metaHast.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        metaHast.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        metaHast.setDisplayName(ChatColor.YELLOW + "Pressa");
        if (BeaconAPI.getPressaActivatedLevel(location) > 0) {
            final ArrayList<String> lore = new ArrayList<>();
            lore.add(" ");
            lore.add(ChatColor.GREEN + "Nível: " + ChatColor.GRAY + BeaconAPI.getPressaActivatedLevel(location) + " ativo!");
            lore.add(" ");
            metaHast.setLore(lore);
        }
        haste.setItemMeta(metaHast);
        inventory.setItem(39, haste);

        if (BeaconAPI.getPressa(location) == 0) {
            inventory.setItem(48, enchNeutro);
        } else {
            if (BeaconAPI.getPressaActivated(location) == false) {
                inventory.setItem(48, enchDeslig);
            } else {
                inventory.setItem(48, enchAtivo);
            }
        }

        final Potion potion3 = new Potion(PotionType.REGEN);
        final ItemStack regenerator = potion3.toItemStack(1);
        final ItemMeta metaRegen = regenerator.getItemMeta();
        metaRegen.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        metaRegen.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        metaRegen.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        metaRegen.addItemFlags(ItemFlag.HIDE_DESTROYS);
        metaRegen.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        metaRegen.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        metaRegen.setDisplayName(ChatColor.YELLOW + "Regeneração");
        if (BeaconAPI.getRegenActivatedLevel(location) > 0) {
            final ArrayList<String> lore = new ArrayList<>();
            lore.add(" ");
            lore.add(ChatColor.GREEN + "Nível: " + ChatColor.GRAY + BeaconAPI.getRegenActivatedLevel(location) + " ativo!");
            lore.add(" ");
            metaRegen.setLore(lore);
        }
        regenerator.setItemMeta(metaRegen);
        inventory.setItem(40, regenerator);

        if (BeaconAPI.getRegen(location) == 0) {
            inventory.setItem(49, enchNeutro);
        } else {
            if (BeaconAPI.getRegenActivated(location) == false) {
                inventory.setItem(49, enchDeslig);
            } else {
                inventory.setItem(49, enchAtivo);
            }
        }

        final Potion potion4 = new Potion(PotionType.JUMP);
        final ItemStack jump = potion4.toItemStack(1);
        final ItemMeta metaJump = jump.getItemMeta();
        metaJump.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        metaJump.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        metaJump.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        metaJump.addItemFlags(ItemFlag.HIDE_DESTROYS);
        metaJump.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        metaJump.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        metaJump.setDisplayName(ChatColor.YELLOW + "Super pulo");
        if (BeaconAPI.getJumpActivatedLevel(location) > 0) {
            final ArrayList<String> lore = new ArrayList<>();
            lore.add(" ");
            lore.add(ChatColor.GREEN + "Nível: " + ChatColor.GRAY + BeaconAPI.getJumpActivatedLevel(location) + " ativo!");
            lore.add(" ");
            metaJump.setLore(lore);
        }
        jump.setItemMeta(metaJump);
        inventory.setItem(41, jump);

        if (BeaconAPI.getJump(location) == 0) {
            inventory.setItem(50, enchNeutro);
        } else {
            if (BeaconAPI.getJumpActivated(location) == false) {
                inventory.setItem(50, enchDeslig);
            } else {
                inventory.setItem(50, enchAtivo);
            }
        }

        final Potion potion5 = new Potion(PotionType.SPEED);
        final ItemStack speed = potion5.toItemStack(1);
        final ItemMeta metaSpeed = speed.getItemMeta();
        metaSpeed.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        metaSpeed.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        metaSpeed.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        metaSpeed.addItemFlags(ItemFlag.HIDE_DESTROYS);
        metaSpeed.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        metaSpeed.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        metaSpeed.setDisplayName(ChatColor.YELLOW + "Velocidade");
        if (BeaconAPI.getSpeedActivatedLevel(location) > 0) {
            final ArrayList<String> lore = new ArrayList<>();
            lore.add(" ");
            lore.add(ChatColor.GREEN + "Nível: " + ChatColor.GRAY + BeaconAPI.getSpeedActivatedLevel(location) + " ativo!");
            lore.add(" ");
            metaSpeed.setLore(lore);
        }
        speed.setItemMeta(metaSpeed);
        inventory.setItem(42, speed);

        if (BeaconAPI.getSpeed(location) == 0) {
            inventory.setItem(51, enchNeutro);
        } else {
            if (BeaconAPI.getSpeedActivated(location) == false) {
                inventory.setItem(51, enchDeslig);
            } else {
                inventory.setItem(51, enchAtivo);
            }
        }

        return inventory;
    }

    private static Inventory inativatedBeacon(final int tier) {
        final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Sinalizador: ");

        final ItemStack inativo = new ItemStack(Material.RED_STAINED_GLASS);
        final ItemMeta metaIna = inativo.getItemMeta();
        metaIna.setDisplayName(ChatColor.RED + "X");
        inativo.setItemMeta(metaIna);

        final ItemStack positivo = new ItemStack(Material.LIME_STAINED_GLASS);
        final ItemMeta metaPosi = positivo.getItemMeta();
        metaPosi.setDisplayName(ChatColor.GREEN + "✓");
        positivo.setItemMeta(metaPosi);

        final ItemStack beacon = new ItemStack(Material.BEACON);
        final ItemMeta metaBea = beacon.getItemMeta();
        metaBea.setDisplayName(ChatColor.GREEN + "Sinalizador");
        beacon.setItemMeta(metaBea);
        inventory.setItem(13, beacon);

        if (tier == 0) {
            final ItemStack papel = new ItemStack(Material.PAPER);
            final ItemMeta metaPap = papel.getItemMeta();
            metaPap.setDisplayName(ChatColor.RED + "Torre incompleta!");
            final ArrayList<String> lore = new ArrayList<>();
            lore.add(" ");
            lore.add(ChatColor.GRAY + "Faltam 4 andares da torre");
            lore.add(ChatColor.GRAY + "para finalizar.");
            lore.add(" ");
            metaPap.setLore(lore);
            papel.setItemMeta(metaPap);
            inventory.setItem(0, papel);

            inventory.setItem(21, inativo);
            inventory.setItem(22, inativo);
            inventory.setItem(23, inativo);
            inventory.setItem(29, inativo);
            inventory.setItem(30, inativo);
            inventory.setItem(31, inativo);
            inventory.setItem(32, inativo);
            inventory.setItem(33, inativo);
            inventory.setItem(37, inativo);
            inventory.setItem(38, inativo);
            inventory.setItem(39, inativo);
            inventory.setItem(40, inativo);
            inventory.setItem(41, inativo);
            inventory.setItem(42, inativo);
            inventory.setItem(43, inativo);
            inventory.setItem(45, inativo);
            inventory.setItem(46, inativo);
            inventory.setItem(47, inativo);
            inventory.setItem(48, inativo);
            inventory.setItem(49, inativo);
            inventory.setItem(50, inativo);
            inventory.setItem(51, inativo);
            inventory.setItem(52, inativo);
            inventory.setItem(53, inativo);
        } else if (tier == 1) {
            final ItemStack papel = new ItemStack(Material.PAPER);
            final ItemMeta metaPap = papel.getItemMeta();
            metaPap.setDisplayName(ChatColor.RED + "Torre incompleta!");
            final ArrayList<String> lore = new ArrayList<>();
            lore.add(" ");
            lore.add(ChatColor.GRAY + "Faltam 3 andares da torre");
            lore.add(ChatColor.GRAY + "para finalizar.");
            lore.add(" ");
            metaPap.setLore(lore);
            papel.setItemMeta(metaPap);
            inventory.setItem(0, papel);

            inventory.setItem(21, positivo);
            inventory.setItem(22, positivo);
            inventory.setItem(23, positivo);
            inventory.setItem(29, inativo);
            inventory.setItem(30, inativo);
            inventory.setItem(31, inativo);
            inventory.setItem(32, inativo);
            inventory.setItem(33, inativo);
            inventory.setItem(37, inativo);
            inventory.setItem(38, inativo);
            inventory.setItem(39, inativo);
            inventory.setItem(40, inativo);
            inventory.setItem(41, inativo);
            inventory.setItem(42, inativo);
            inventory.setItem(43, inativo);
            inventory.setItem(45, inativo);
            inventory.setItem(46, inativo);
            inventory.setItem(47, inativo);
            inventory.setItem(48, inativo);
            inventory.setItem(49, inativo);
            inventory.setItem(50, inativo);
            inventory.setItem(51, inativo);
            inventory.setItem(52, inativo);
            inventory.setItem(53, inativo);
        } else if (tier == 2) {
            final ItemStack papel = new ItemStack(Material.PAPER);
            final ItemMeta metaPap = papel.getItemMeta();
            metaPap.setDisplayName(ChatColor.RED + "Torre incompleta!");
            final ArrayList<String> lore = new ArrayList<>();
            lore.add(" ");
            lore.add(ChatColor.GRAY + "Faltam 2 andares da torre");
            lore.add(ChatColor.GRAY + "para finalizar.");
            lore.add(" ");
            metaPap.setLore(lore);
            papel.setItemMeta(metaPap);
            inventory.setItem(0, papel);

            inventory.setItem(21, positivo);
            inventory.setItem(22, positivo);
            inventory.setItem(23, positivo);
            inventory.setItem(29, positivo);
            inventory.setItem(30, positivo);
            inventory.setItem(31, positivo);
            inventory.setItem(32, positivo);
            inventory.setItem(33, positivo);
            inventory.setItem(37, inativo);
            inventory.setItem(38, inativo);
            inventory.setItem(39, inativo);
            inventory.setItem(40, inativo);
            inventory.setItem(41, inativo);
            inventory.setItem(42, inativo);
            inventory.setItem(43, inativo);
            inventory.setItem(45, inativo);
            inventory.setItem(46, inativo);
            inventory.setItem(47, inativo);
            inventory.setItem(48, inativo);
            inventory.setItem(49, inativo);
            inventory.setItem(50, inativo);
            inventory.setItem(51, inativo);
            inventory.setItem(52, inativo);
            inventory.setItem(53, inativo);
        } else if (tier == 3) {
            final ItemStack papel = new ItemStack(Material.PAPER);
            final ItemMeta metaPap = papel.getItemMeta();
            metaPap.setDisplayName(ChatColor.RED + "Torre incompleta!");
            final ArrayList<String> lore = new ArrayList<>();
            lore.add(" ");
            lore.add(ChatColor.GRAY + "Falta 1 andar da torre");
            lore.add(ChatColor.GRAY + "para finalizar.");
            lore.add(" ");
            metaPap.setLore(lore);
            papel.setItemMeta(metaPap);
            inventory.setItem(0, papel);

            inventory.setItem(21, positivo);
            inventory.setItem(22, positivo);
            inventory.setItem(23, positivo);
            inventory.setItem(29, positivo);
            inventory.setItem(30, positivo);
            inventory.setItem(31, positivo);
            inventory.setItem(32, positivo);
            inventory.setItem(33, positivo);
            inventory.setItem(37, positivo);
            inventory.setItem(38, positivo);
            inventory.setItem(39, positivo);
            inventory.setItem(40, positivo);
            inventory.setItem(41, positivo);
            inventory.setItem(42, positivo);
            inventory.setItem(43, positivo);
            inventory.setItem(45, inativo);
            inventory.setItem(46, inativo);
            inventory.setItem(47, inativo);
            inventory.setItem(48, inativo);
            inventory.setItem(49, inativo);
            inventory.setItem(50, inativo);
            inventory.setItem(51, inativo);
            inventory.setItem(52, inativo);
            inventory.setItem(53, inativo);
        }

        return inventory;
    }

    @EventHandler
    public void onBreak(final BlockBreakEvent event) {
        if (event.getBlock().getType().equals(Material.BEACON)) {
            String loc = event.getBlock().getWorld().getName() + ":" + event.getBlock().getLocation().getBlockX() + ":" + event.getBlock().getLocation().getBlockY() + ":" + event.getBlock().getLocation().getBlockZ();
            if (BeaconAPI.getExistBeacon(loc) != null) {
                int effect1 = BeaconAPI.getForca(loc);
                int effect2 = BeaconAPI.getJump(loc);
                int effect3 = BeaconAPI.getPressa(loc);
                int effect4 = BeaconAPI.getRegen(loc);
                int effect5 = BeaconAPI.getSpeed(loc);

                if (effect1 >= 1) {
                    final ItemStack playerDrop = new ItemStack(Material.FIREWORK_STAR, effect1);
                    final ItemMeta meta = playerDrop.getItemMeta();
                    meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                    meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                    meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                    meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
                    meta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
                    meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                    meta.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
                    final ArrayList<String> lore = new ArrayList<>();
                    lore.add(" ");
                    lore.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Força");
                    lore.add(" ");
                    meta.setLore(lore);
                    final FireworkEffectMeta metaFw = (FireworkEffectMeta) meta;
                    final FireworkEffect aa = FireworkEffect.builder().withColor(Color.RED).build();
                    metaFw.setEffect(aa);
                    playerDrop.setItemMeta(metaFw);

                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), playerDrop);
                }

                if (effect2 >= 1) {
                    final ItemStack playerDrop3 = new ItemStack(Material.FIREWORK_STAR, effect2);
                    final ItemMeta meta3 = playerDrop3.getItemMeta();
                    meta3.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                    meta3.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                    meta3.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                    meta3.addItemFlags(ItemFlag.HIDE_DESTROYS);
                    meta3.addItemFlags(ItemFlag.HIDE_PLACED_ON);
                    meta3.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                    meta3.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
                    final ArrayList<String> lore3 = new ArrayList<>();
                    lore3.add(" ");
                    lore3.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Super pulo");
                    lore3.add(" ");
                    meta3.setLore(lore3);
                    final FireworkEffectMeta metaFw3 = (FireworkEffectMeta) meta3;
                    final FireworkEffect aa3 = FireworkEffect.builder().withColor(Color.LIME).build();
                    metaFw3.setEffect(aa3);
                    playerDrop3.setItemMeta(metaFw3);

                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), playerDrop3);
                }

                if (effect3 >= 1) {
                    final ItemStack playerDrop1 = new ItemStack(Material.FIREWORK_STAR, effect3);
                    final ItemMeta meta1 = playerDrop1.getItemMeta();
                    meta1.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                    meta1.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                    meta1.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                    meta1.addItemFlags(ItemFlag.HIDE_DESTROYS);
                    meta1.addItemFlags(ItemFlag.HIDE_PLACED_ON);
                    meta1.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                    meta1.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
                    final ArrayList<String> lore1 = new ArrayList<>();
                    lore1.add(" ");
                    lore1.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Pressa");
                    lore1.add(" ");
                    meta1.setLore(lore1);
                    final FireworkEffectMeta metaFw1 = (FireworkEffectMeta) meta1;
                    final FireworkEffect aa1 = FireworkEffect.builder().withColor(Color.NAVY).build();
                    metaFw1.setEffect(aa1);
                    playerDrop1.setItemMeta(metaFw1);

                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), playerDrop1);
                }

                if (effect4 >= 1) {
                    final ItemStack playerDrop2 = new ItemStack(Material.FIREWORK_STAR, effect4);
                    final ItemMeta meta2 = playerDrop2.getItemMeta();
                    meta2.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                    meta2.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                    meta2.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                    meta2.addItemFlags(ItemFlag.HIDE_DESTROYS);
                    meta2.addItemFlags(ItemFlag.HIDE_PLACED_ON);
                    meta2.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                    meta2.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
                    final ArrayList<String> lore2 = new ArrayList<>();
                    lore2.add(" ");
                    lore2.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Regeneração");
                    lore2.add(" ");
                    meta2.setLore(lore2);
                    final FireworkEffectMeta metaFw2 = (FireworkEffectMeta) meta2;
                    final FireworkEffect aa2 = FireworkEffect.builder().withColor(Color.FUCHSIA).build();
                    metaFw2.setEffect(aa2);
                    playerDrop2.setItemMeta(metaFw2);

                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), playerDrop2);
                }

                if (effect5 >= 1) {
                    final ItemStack playerDrop4 = new ItemStack(Material.FIREWORK_STAR, effect5);
                    final ItemMeta meta4 = playerDrop4.getItemMeta();
                    meta4.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
                    meta4.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                    meta4.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                    meta4.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                    meta4.addItemFlags(ItemFlag.HIDE_DESTROYS);
                    meta4.addItemFlags(ItemFlag.HIDE_PLACED_ON);
                    meta4.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                    final ArrayList<String> lore4 = new ArrayList<>();
                    lore4.add(" ");
                    lore4.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Velocidade");
                    lore4.add(" ");
                    meta4.setLore(lore4);
                    final FireworkEffectMeta metaFw4 = (FireworkEffectMeta) meta4;
                    final FireworkEffect aa4 = FireworkEffect.builder().withColor(Color.BLUE).build();
                    metaFw4.setEffect(aa4);
                    playerDrop4.setItemMeta(metaFw4);

                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), playerDrop4);
                }

                BeaconAPI.deleteBeacon(loc);
            }
        }
    }

    @EventHandler
    public void onInteract(final InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getInventory().getName().equalsIgnoreCase("Efeito força: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 31) {
                if (BeaconAPI.getForca(InventoryLocation.get(UUID).getLocation()) >= 2) {
                    BeaconAPI.updateNivelAtivoForca(InventoryLocation.get(UUID).getLocation(), 2);
                    player.sendMessage(ChatColor.GREEN + " * Força nível 2 habilitado");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(invLevelsForca(InventoryLocation.get(UUID).getLocation()));
                        }
                    }, 5L);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, você ainda não contém este nível.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }

            if (event.getSlot() == 30) {
                if (BeaconAPI.getForca(InventoryLocation.get(UUID).getLocation()) >= 1) {
                    BeaconAPI.updateNivelAtivoForca(InventoryLocation.get(UUID).getLocation(), 1);
                    player.sendMessage(ChatColor.GREEN + " * Força nível 1 habilitado");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(invLevelsForca(InventoryLocation.get(UUID).getLocation()));
                        }
                    }, 5L);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, você ainda não contém este nível.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }

            if (event.getSlot() == 49) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(beacon(InventoryLocation.get(UUID).getLocation()));
                    }
                }, 5L);
            }
        }

        if (event.getInventory().getName().equalsIgnoreCase("Efeito pressa: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 32) {
                if (BeaconAPI.getPressa(InventoryLocation.get(UUID).getLocation()) >= 3) {
                    BeaconAPI.updateNivelAtivoPressa(InventoryLocation.get(UUID).getLocation(), 3);
                    player.sendMessage(ChatColor.GREEN + " * Pressa nível 3 habilitado");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(BeaconEvents.invLevelsPressa(InventoryLocation.get(UUID).getLocation()));
                        }
                    }, 5L);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, você ainda não contém este nível.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }

            if (event.getSlot() == 31) {
                if (BeaconAPI.getPressa(InventoryLocation.get(UUID).getLocation()) >= 2) {
                    BeaconAPI.updateNivelAtivoPressa(InventoryLocation.get(UUID).getLocation(), 2);
                    player.sendMessage(ChatColor.GREEN + " * Pressa nível 2 habilitado");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(BeaconEvents.invLevelsPressa(InventoryLocation.get(UUID).getLocation()));
                        }
                    }, 5L);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, você ainda não contém este nível.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }

            if (event.getSlot() == 30) {
                if (BeaconAPI.getPressa(InventoryLocation.get(UUID).getLocation()) >= 1) {
                    BeaconAPI.updateNivelAtivoPressa(InventoryLocation.get(UUID).getLocation(), 1);
                    player.sendMessage(ChatColor.GREEN + " * Pressa nível 1 habilitado");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(BeaconEvents.invLevelsPressa(InventoryLocation.get(UUID).getLocation()));
                        }
                    }, 5L);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, você ainda não contém este nível.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }

            if (event.getSlot() == 49) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(beacon(InventoryLocation.get(UUID).getLocation()));
                    }
                }, 5L);
            }
        }

        if (event.getInventory().getName().equalsIgnoreCase("Efeito regeneração: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 31) {
                if (BeaconAPI.getRegen(InventoryLocation.get(UUID).getLocation()) >= 2) {
                    BeaconAPI.updateNivelAtivoRegen(InventoryLocation.get(UUID).getLocation(), 2);
                    player.sendMessage(ChatColor.GREEN + " * Regeneração nível 2 habilitado");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(BeaconEvents.invLevelsRegen(InventoryLocation.get(UUID).getLocation()));
                        }
                    }, 5L);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, você ainda não contém este nível.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }

            if (event.getSlot() == 30) {
                if (BeaconAPI.getRegen(InventoryLocation.get(UUID).getLocation()) >= 1) {
                    BeaconAPI.updateNivelAtivoRegen(InventoryLocation.get(UUID).getLocation(), 1);
                    player.sendMessage(ChatColor.GREEN + " * Regeneração nível 1 habilitado");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(BeaconEvents.invLevelsRegen(InventoryLocation.get(UUID).getLocation()));
                        }
                    }, 5L);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, você ainda não contém este nível.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }

            if (event.getSlot() == 49) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(beacon(InventoryLocation.get(UUID).getLocation()));
                    }
                }, 5L);
            }
        }

        if (event.getInventory().getName().equalsIgnoreCase("Efeito super pulo: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 32) {
                if (BeaconAPI.getJump(InventoryLocation.get(UUID).getLocation()) >= 3) {
                    BeaconAPI.updateNivelAtivoJump(InventoryLocation.get(UUID).getLocation(), 3);
                    player.sendMessage(ChatColor.GREEN + " * Super pulo nível 3 habilitado");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(BeaconEvents.invLevelsJump(InventoryLocation.get(UUID).getLocation()));
                        }
                    }, 5L);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, você ainda não contém este nível.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }

            if (event.getSlot() == 31) {
                if (BeaconAPI.getJump(InventoryLocation.get(UUID).getLocation()) >= 2) {
                    BeaconAPI.updateNivelAtivoJump(InventoryLocation.get(UUID).getLocation(), 2);
                    player.sendMessage(ChatColor.GREEN + " * Super pulo nível 2 habilitado");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(BeaconEvents.invLevelsJump(InventoryLocation.get(UUID).getLocation()));
                        }
                    }, 5L);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, você ainda não contém este nível.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }

            if (event.getSlot() == 30) {
                if (BeaconAPI.getJump(InventoryLocation.get(UUID).getLocation()) >= 1) {
                    BeaconAPI.updateNivelAtivoJump(InventoryLocation.get(UUID).getLocation(), 1);
                    player.sendMessage(ChatColor.GREEN + " * Super pulo nível 1 habilitado");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(BeaconEvents.invLevelsJump(InventoryLocation.get(UUID).getLocation()));
                        }
                    }, 5L);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, você ainda não contém este nível.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }

            if (event.getSlot() == 49) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(beacon(InventoryLocation.get(UUID).getLocation()));
                    }
                }, 5L);
            }
        }

        if (event.getInventory().getName().equalsIgnoreCase("Efeito velocidade: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 32) {
                if (BeaconAPI.getSpeed(InventoryLocation.get(UUID).getLocation()) >= 3) {
                    BeaconAPI.updateNivelAtivoSpeed(InventoryLocation.get(UUID).getLocation(), 3);
                    player.sendMessage(ChatColor.GREEN + " * Velocidade nível 3 habilitado");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(BeaconEvents.invLevelsSpeed(InventoryLocation.get(UUID).getLocation()));
                        }
                    }, 5L);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, você ainda não contém este nível.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }

            if (event.getSlot() == 31) {
                if (BeaconAPI.getSpeed(InventoryLocation.get(UUID).getLocation()) >= 2) {
                    BeaconAPI.updateNivelAtivoSpeed(InventoryLocation.get(UUID).getLocation(), 2);
                    player.sendMessage(ChatColor.GREEN + " * Velocidade nível 2 habilitado");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(BeaconEvents.invLevelsSpeed(InventoryLocation.get(UUID).getLocation()));
                        }
                    }, 5L);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, você ainda não contém este nível.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }

            if (event.getSlot() == 30) {
                if (BeaconAPI.getSpeed(InventoryLocation.get(UUID).getLocation()) >= 1) {
                    BeaconAPI.updateNivelAtivoSpeed(InventoryLocation.get(UUID).getLocation(), 1);
                    player.sendMessage(ChatColor.GREEN + " * Velocidade nível 1 habilitado");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(BeaconEvents.invLevelsSpeed(InventoryLocation.get(UUID).getLocation()));
                        }
                    }, 5L);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, você ainda não contém este nível.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }

            if (event.getSlot() == 49) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(beacon(InventoryLocation.get(UUID).getLocation()));
                    }
                }, 5L);
            }
        }

        if (event.getInventory().getName().equalsIgnoreCase("Oscilar efeitos: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 31) {
                if (BeaconAPI.getOscilar(InventoryLocation.get(UUID).getLocation()) == true) {
                    BeaconAPI.updateOscilar(InventoryLocation.get(UUID).getLocation(), false);

                    player.sendMessage(ChatColor.GREEN + "\n * Oscilar desativado\n" + ChatColor.GRAY + "  Todos no raio do sinalizador passaram\n  a ter os efeitos do sinalizador.\n ");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    BeaconAPI.updateOscilar(InventoryLocation.get(UUID).getLocation(), true);

                    player.sendMessage(ChatColor.GREEN + "\n * Oscilar ativado\n" + ChatColor.GRAY + "  Apenas membros de vosso esquadrão ou alidos\n receberam os efeitos do sinalizador.\n ");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(invOscilar(InventoryLocation.get(UUID).getLocation()));
                    }
                }, 5L);
            }

            if (event.getSlot() == 49) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(beacon(InventoryLocation.get(UUID).getLocation()));
                    }
                }, 5L);
            }
        }

        if (event.getInventory().getName().equalsIgnoreCase("Multiplos efeitos: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 33) {
                if (BeaconAPI.getSpeedActivated(InventoryLocation.get(UUID).getLocation()) == false) {
                    BeaconAPI.updateActiveSpeed(InventoryLocation.get(UUID).getLocation(), true);
                    player.sendMessage(ChatColor.GREEN + " * Efeito-5 ativo com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(invMultiplos(UUID));
                        }
                    }, 5L);
                } else {
                    BeaconAPI.updateActiveSpeed(InventoryLocation.get(UUID).getLocation(), false);
                    player.sendMessage(ChatColor.GREEN + " * Efeito-5 desativado com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(invMultiplos(UUID));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 32) {
                if (BeaconAPI.getJumpActivated(InventoryLocation.get(UUID).getLocation()) == false) {
                    BeaconAPI.updateActiveJump(InventoryLocation.get(UUID).getLocation(), true);
                    player.sendMessage(ChatColor.GREEN + " * Efeito-4 ativo com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(invMultiplos(UUID));
                        }
                    }, 5L);
                } else {
                    BeaconAPI.updateActiveJump(InventoryLocation.get(UUID).getLocation(), false);
                    player.sendMessage(ChatColor.GREEN + " * Efeito-4 desativado com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(invMultiplos(UUID));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 31) {
                if (BeaconAPI.getRegenActivated(InventoryLocation.get(UUID).getLocation()) == false) {
                    BeaconAPI.updateActiveRegen(InventoryLocation.get(UUID).getLocation(), true);
                    player.sendMessage(ChatColor.GREEN + " * Efeito-3 ativo com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(invMultiplos(UUID));
                        }
                    }, 5L);
                } else {
                    BeaconAPI.updateActiveRegen(InventoryLocation.get(UUID).getLocation(), false);
                    player.sendMessage(ChatColor.GREEN + " * Efeito-3 desativado com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(invMultiplos(UUID));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 30) {
                if (BeaconAPI.getPressaActivated(InventoryLocation.get(UUID).getLocation()) == false) {
                    BeaconAPI.updateActivePressa(InventoryLocation.get(UUID).getLocation(), true);
                    player.sendMessage(ChatColor.GREEN + " * Efeito-2 ativo com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(invMultiplos(UUID));
                        }
                    }, 5L);
                } else {
                    BeaconAPI.updateActivePressa(InventoryLocation.get(UUID).getLocation(), false);
                    player.sendMessage(ChatColor.GREEN + " * Efeito-2 desativado com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(invMultiplos(UUID));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 29) {
                if (BeaconAPI.getForcaActivated(InventoryLocation.get(UUID).getLocation()) == false) {
                    BeaconAPI.updateActiveForca(InventoryLocation.get(UUID).getLocation(), true);
                    player.sendMessage(ChatColor.GREEN + " * Efeito-1 ativo com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(invMultiplos(UUID));
                        }
                    }, 5L);
                } else {
                    BeaconAPI.updateActiveForca(InventoryLocation.get(UUID).getLocation(), false);
                    player.sendMessage(ChatColor.GREEN + " * Efeito-1 desativado com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(invMultiplos(UUID));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 49) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(beacon(InventoryLocation.get(UUID).getLocation()));
                    }
                }, 5L);
            }
        }

        if (event.getInventory().getName().equalsIgnoreCase("Sinalizador: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 42) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(invLevelsSpeed(InventoryLocation.get(UUID).getLocation()));
                    }
                }, 5L);
            }

            if (event.getSlot() == 41) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(invLevelsJump(InventoryLocation.get(UUID).getLocation()));
                    }
                }, 5L);
            }

            if (event.getSlot() == 40) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(invLevelsRegen(InventoryLocation.get(UUID).getLocation()));
                    }
                }, 5L);
            }

            if (event.getSlot() == 39) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(invLevelsPressa(InventoryLocation.get(UUID).getLocation()));
                    }
                }, 5L);
            }

            if (event.getSlot() == 38) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(invLevelsForca(InventoryLocation.get(UUID).getLocation()));
                    }
                }, 5L);
            }

            if (event.getSlot() == 51) {
                if (BeaconAPI.getSpeed(InventoryLocation.get(UUID).getLocation()) == 0) {
                    player.sendMessage(ChatColor.RED + " * Ops você não contém este encantamento!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (BeaconAPI.getSpeedActivated(InventoryLocation.get(UUID).getLocation()) == false) {
                    BeaconAPI.updateActiveSpeed(InventoryLocation.get(UUID).getLocation(), true);
                    player.sendMessage(ChatColor.GREEN + " * Efeito-5 ativo com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(beacon(InventoryLocation.get(UUID).getLocation()));
                        }
                    }, 5L);
                } else {
                    BeaconAPI.updateActiveSpeed(InventoryLocation.get(UUID).getLocation(), false);
                    player.sendMessage(ChatColor.GREEN + " * Efeito-5 desativado com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(beacon(InventoryLocation.get(UUID).getLocation()));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 50) {
                if (BeaconAPI.getJump(InventoryLocation.get(UUID).getLocation()) == 0) {
                    player.sendMessage(ChatColor.RED + " * Ops você não contém este encantamento!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (BeaconAPI.getJumpActivated(InventoryLocation.get(UUID).getLocation()) == false) {
                    BeaconAPI.updateActiveJump(InventoryLocation.get(UUID).getLocation(), true);
                    player.sendMessage(ChatColor.GREEN + " * Efeito-4 ativo com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(beacon(InventoryLocation.get(UUID).getLocation()));
                        }
                    }, 5L);
                } else {
                    BeaconAPI.updateActiveJump(InventoryLocation.get(UUID).getLocation(), false);
                    player.sendMessage(ChatColor.GREEN + " * Efeito-4 desativado com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(beacon(InventoryLocation.get(UUID).getLocation()));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 49) {
                if (BeaconAPI.getRegen(InventoryLocation.get(UUID).getLocation()) == 0) {
                    player.sendMessage(ChatColor.RED + " * Ops você não contém este encantamento!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (BeaconAPI.getRegenActivated(InventoryLocation.get(UUID).getLocation()) == false) {
                    BeaconAPI.updateActiveRegen(InventoryLocation.get(UUID).getLocation(), true);
                    player.sendMessage(ChatColor.GREEN + " * Efeito-3 ativo com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(beacon(InventoryLocation.get(UUID).getLocation()));
                        }
                    }, 5L);
                } else {
                    BeaconAPI.updateActiveRegen(InventoryLocation.get(UUID).getLocation(), false);
                    player.sendMessage(ChatColor.GREEN + " * Efeito-3 desativado com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(beacon(InventoryLocation.get(UUID).getLocation()));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 48) {
                if (BeaconAPI.getPressa(InventoryLocation.get(UUID).getLocation()) == 0) {
                    player.sendMessage(ChatColor.RED + " * Ops você não contém este encantamento!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (BeaconAPI.getPressaActivated(InventoryLocation.get(UUID).getLocation()) == false) {
                    BeaconAPI.updateActivePressa(InventoryLocation.get(UUID).getLocation(), true);
                    player.sendMessage(ChatColor.GREEN + " * Efeito-2 ativo com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(beacon(InventoryLocation.get(UUID).getLocation()));
                        }
                    }, 5L);
                } else {
                    BeaconAPI.updateActivePressa(InventoryLocation.get(UUID).getLocation(), false);
                    player.sendMessage(ChatColor.GREEN + " * Efeito-2 desativado com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(beacon(InventoryLocation.get(UUID).getLocation()));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 47) {
                if (BeaconAPI.getForca(InventoryLocation.get(UUID).getLocation()) == 0) {
                    player.sendMessage(ChatColor.RED + " * Ops você não contém este encantamento!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (BeaconAPI.getForcaActivated(InventoryLocation.get(UUID).getLocation()) == false) {
                    BeaconAPI.updateActiveForca(InventoryLocation.get(UUID).getLocation(), true);
                    player.sendMessage(ChatColor.GREEN + " * Efeito-1 ativo com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(beacon(InventoryLocation.get(UUID).getLocation()));
                        }
                    }, 5L);
                } else {
                    BeaconAPI.updateActiveForca(InventoryLocation.get(UUID).getLocation(), false);
                    player.sendMessage(ChatColor.GREEN + " * Efeito-1 desativado com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(beacon(InventoryLocation.get(UUID).getLocation()));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 4) {
                if (BeaconAPI.getActive(InventoryLocation.get(UUID).getLocation()) == false) {
                    BeaconAPI.updateActive(InventoryLocation.get(UUID).getLocation(), true);

                    player.sendMessage(ChatColor.GREEN + " * Sinalizador ativado com sucesso!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.BLOCK_BEACON_ACTIVATE, 1.0f, 1.0f);

                    final String[] split = InventoryLocation.get(UUID).getLocation().split(":");
                    final Location location = new Location(Bukkit.getWorld(split[0]), Integer.valueOf(split[1]), Integer.valueOf(split[2]), Integer.valueOf(split[3]));
                    final Beacon beacon = (Beacon) location.getBlock().getState();

                    final String loc = InventoryLocation.get(UUID).getLocation();

                    BeaconTaskFix.setBeacon(loc, beacon, UUID);
                    //final int rotate = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                    //                        @Override
                    //                        public void run() {
                    //                            for (final LivingEntity entity : beacon.getEntitiesInRange()) {
                    //                                if (beacon.getTier() != 4) {
                    //                                    Bukkit.getScheduler().cancelTask(BeaconID.get(UUID).getId());
                    //                                }
                    //                                if (entity.getType().equals(EntityType.PLAYER)) {
                    //                                    //VERIFICAR SE OSCILAR ESTA ATIVO..
                    //                                    if (BeaconAPI.getOscilar(loc) == true) {
                    //                                        if (FacAPI.getAliados(FacAPI.getFacNome(UUID)) != null) {
                    //                                            if (FacAPI.getAliados(FacAPI.getFacNome(UUID)).contains(":")) {
                    //                                                final String[] split = FacAPI.getAliados(FacAPI.getFacNome(UUID)).split(":");
                    //                                                final Player target = (Player) entity;
                    //                                                final String UUIDtarget = PlayerUUID.getUUID(target.getName());
                    //
                    //                                                for (int i = 1; i <= split.length; i++) {
                    //                                                    if (FacAPI.getMembros(split[i - 1]).contains(":")) {
                    //                                                        final String[] splitAliados = FacAPI.getMembros(split[i - 1]).split(":");
                    //
                    //                                                        for (int x = 1; x <= splitAliados.length; x++) {
                    //                                                            if (splitAliados[x - 1].equalsIgnoreCase(UUIDtarget)) {
                    //                                                                applyEffects(entity, loc);
                    //                                                            }
                    //                                                        }
                    //                                                    } else {
                    //                                                        if (!FacAPI.getMembros(split[i - 1]).equals("NULL")) {
                    //                                                            if (FacAPI.getMembros(split[i - 1]).equalsIgnoreCase(UUIDtarget)) {
                    //                                                                applyEffects(entity, loc);
                    //                                                            }
                    //                                                        }
                    //                                                    }
                    //                                                }
                    //                                            } else {
                    //                                                final Player target = (Player) entity;
                    //                                                final String UUIDtarget = PlayerUUID.getUUID(target.getName());
                    //
                    //                                                if (FacAPI.getMembros(FacAPI.getAliados(FacAPI.getFacNome(UUID))).contains(":")) {
                    //                                                    final String[] splitAliados = FacAPI.getMembros(FacAPI.getAliados(FacAPI.getFacNome(UUID))).split(":");
                    //
                    //                                                    for (int x = 1; x <= splitAliados.length; x++) {
                    //                                                        if (splitAliados[x - 1].equalsIgnoreCase(UUIDtarget)) {
                    //                                                            applyEffects(entity, loc);
                    //                                                        }
                    //                                                    }
                    //                                                } else {
                    //                                                    if (!FacAPI.getMembros(FacAPI.getAliados(FacAPI.getFacNome(UUID))).equals("NULL")) {
                    //                                                        if (FacAPI.getMembros(FacAPI.getAliados(FacAPI.getFacNome(UUID))).equalsIgnoreCase(UUIDtarget)) {
                    //                                                            applyEffects(entity, loc);
                    //                                                        }
                    //                                                    }
                    //                                                }
                    //                                            }
                    //                                        }
                    //                                        if (FacAPI.getLideres(FacAPI.getFacNome(UUID)).contains(":")) {
                    //                                            final String[] split = FacAPI.getLideres(FacAPI.getFacNome(UUID)).split(":");
                    //                                            final Player target = (Player) entity;
                    //                                            final String UUIDtarget = PlayerUUID.getUUID(target.getName());
                    //
                    //                                            for (int i = 1; i <= split.length; i++) {
                    //                                                if (split[i - 1].equalsIgnoreCase(UUIDtarget)) {
                    //                                                    applyEffects(entity, loc);
                    //                                                }
                    //                                            }
                    //                                        } else {
                    //                                            if (!FacAPI.getLideres(FacAPI.getFacNome(UUID)).equals("NULL")) {
                    //                                                final Player target = (Player) entity;
                    //                                                final String UUIDtarget = PlayerUUID.getUUID(target.getName());
                    //
                    //                                                if (FacAPI.getLideres(FacAPI.getFacNome(UUID)).equalsIgnoreCase(UUIDtarget)) {
                    //                                                    applyEffects(entity, loc);
                    //                                                }
                    //                                            }
                    //                                        }
                    //                                        if (FacAPI.getCapitoes(FacAPI.getFacNome(UUID)).contains(":")) {
                    //                                            final String[] split = FacAPI.getCapitoes(FacAPI.getFacNome(UUID)).split(":");
                    //                                            final Player target = (Player) entity;
                    //                                            final String UUIDtarget = PlayerUUID.getUUID(target.getName());
                    //
                    //                                            for (int i = 1; i <= split.length; i++) {
                    //                                                if (split[i - 1].equalsIgnoreCase(UUIDtarget)) {
                    //                                                    applyEffects(entity, loc);
                    //                                                }
                    //                                            }
                    //                                        } else {
                    //                                            if (!FacAPI.getCapitoes(FacAPI.getFacNome(UUID)).equals("NULL")) {
                    //                                                final Player target = (Player) entity;
                    //                                                final String UUIDtarget = PlayerUUID.getUUID(target.getName());
                    //
                    //                                                if (FacAPI.getCapitoes(FacAPI.getFacNome(UUID)).equalsIgnoreCase(UUIDtarget)) {
                    //                                                    applyEffects(entity, loc);
                    //                                                }
                    //                                            }
                    //                                        }
                    //                                        if (FacAPI.getRecrutas(FacAPI.getFacNome(UUID)).contains(":")) {
                    //                                            final String[] split = FacAPI.getRecrutas(FacAPI.getFacNome(UUID)).split(":");
                    //                                            final Player target = (Player) entity;
                    //                                            final String UUIDtarget = PlayerUUID.getUUID(target.getName());
                    //
                    //                                            for (int i = 1; i <= split.length; i++) {
                    //                                                if (split[i - 1].equalsIgnoreCase(UUIDtarget)) {
                    //                                                    applyEffects(entity, loc);
                    //                                                }
                    //                                            }
                    //                                        } else {
                    //                                            if (!FacAPI.getRecrutas(FacAPI.getFacNome(UUID)).equals("NULL")) {
                    //                                                final Player target = (Player) entity;
                    //                                                final String UUIDtarget = PlayerUUID.getUUID(target.getName());
                    //
                    //                                                if (FacAPI.getRecrutas(FacAPI.getFacNome(UUID)).equalsIgnoreCase(UUIDtarget)) {
                    //                                                    applyEffects(entity, loc);
                    //                                                }
                    //                                            }
                    //                                        }
                    //                                        if (FacAPI.getMembros(FacAPI.getFacNome(UUID)).contains(":")) {
                    //                                            final String[] split = FacAPI.getMembros(FacAPI.getFacNome(UUID)).split(":");
                    //                                            final Player target = (Player) entity;
                    //                                            final String UUIDtarget = PlayerUUID.getUUID(target.getName());
                    //
                    //                                            for (int i = 1; i <= split.length; i++) {
                    //                                                if (split[i - 1].equalsIgnoreCase(UUIDtarget)) {
                    //                                                    applyEffects(entity, loc);
                    //                                                }
                    //                                            }
                    //                                        } else {
                    //                                            if (!FacAPI.getMembros(FacAPI.getFacNome(UUID)).equals("NULL")) {
                    //                                                final Player target = (Player) entity;
                    //                                                final String UUIDtarget = PlayerUUID.getUUID(target.getName());
                    //
                    //                                                if (FacAPI.getMembros(FacAPI.getFacNome(UUID)).equalsIgnoreCase(UUIDtarget)) {
                    //                                                    applyEffects(entity, loc);
                    //                                                }
                    //                                            }
                    //                                        }
                    //                                    } else {
                    //                                        applyEffects(entity, loc);
                    //                                    }
                    //                                }
                    //                            }
                    //                        }
                    //                    }, 0L, 40L);

                    //if (BeaconID.get(loc) == null) {
                    //                        new BeaconID(loc, rotate).insert();
                    //                    } else {
                    //                        BeaconID.get(loc).setId(rotate);
                    //                    }

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(beacon(InventoryLocation.get(UUID).getLocation()));
                        }
                    }, 5L);
                } else {
                    BeaconAPI.updateActive(InventoryLocation.get(UUID).getLocation(), false);

                    player.sendMessage(ChatColor.GREEN + " * Sinalizador desativado com sucesso!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.BLOCK_BEACON_ACTIVATE, 1.0f, 1.0f);

                    BeaconTaskFix.removeBeacon(InventoryLocation.get(UUID).getLocation());
                    //if (BeaconID.get(InventoryLocation.get(UUID).getLocation()) != null) {
                    //                        Bukkit.getScheduler().cancelTask(BeaconID.get(InventoryLocation.get(UUID).getLocation()).getId());
                    //                    }

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(beacon(InventoryLocation.get(UUID).getLocation()));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 23) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(invOscilar(InventoryLocation.get(UUID).getLocation()));
                    }
                }, 5L);
            }

            if (event.getSlot() == 21) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(invMultiplos(UUID));
                    }
                }, 5L);
            }
        }
    }

    @EventHandler
    public void onOpenBeacon(final PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && player.isSneaking()) {
            if (event.getClickedBlock().getType().equals(Material.BEACON)) {
                if (player.getItemInHand() != null && player.getItemInHand().getType().equals(Material.FIREWORK_STAR)) {
                    final String location = event.getClickedBlock().getWorld().getName() + ":" + event.getClickedBlock().getX() + ":" + event.getClickedBlock().getY() + ":" + event.getClickedBlock().getZ();

                    if (RightClickBeacon.get(UUID) != null && RightClickBeacon.get(UUID).getHasAdd() == true) return;

                    if (RightClickBeacon.get(UUID) == null) {
                        new RightClickBeacon(UUID, true).insert();
                    } else {
                        RightClickBeacon.get(UUID).setHasAdd(true);
                    }

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            RightClickBeacon.get(UUID).setHasAdd(false);
                        }
                    }, 5L);
                    if (player.getItemInHand().getItemMeta().getLore().get(1).substring(12).equals("Velocidade")) {
                        if (BeaconAPI.getSpeed(location) == 0) {
                            BeaconAPI.updateMultiplos(location, BeaconAPI.getMultiplos(location) + 1);
                            BeaconAPI.updateSpeed(location, 1);

                            RemoveItemInv.removeItemDifferentLore(player.getInventory(), player.getItemInHand(), 1);
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.BLOCK_BEACON_POWER_SELECT, 1.0f, 1.0f);
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.GREEN + "\n * Efeito aplicado, sinalizador atualizado.\n ");
                            return;
                        } else if (BeaconAPI.getSpeed(location) == 1) {
                            BeaconAPI.updateSpeed(location, 2);

                            RemoveItemInv.removeItemDifferentLore(player.getInventory(), player.getItemInHand(), 1);
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.BLOCK_BEACON_POWER_SELECT, 1.0f, 1.0f);
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.GREEN + "\n * Efeito aplicado, sinalizador atualizado.\n ");
                            return;
                        } else if (BeaconAPI.getSpeed(location) == 2) {
                            BeaconAPI.updateSpeed(location, 3);

                            RemoveItemInv.removeItemDifferentLore(player.getInventory(), player.getItemInHand(), 1);
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.BLOCK_BEACON_POWER_SELECT, 1.0f, 1.0f);
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.GREEN + "\n * Efeito aplicado, sinalizador atualizado.\n ");
                            return;
                        } else if (BeaconAPI.getSpeed(location) == 3) {
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.RED + " * Ops, o sinalizador já se encontra em seu nível máximo em velocidade.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    }

                    if (player.getItemInHand().getItemMeta().getLore().get(1).substring(12).equals("Super pulo")) {
                        if (BeaconAPI.getJump(location) == 0) {
                            BeaconAPI.updateMultiplos(location, BeaconAPI.getMultiplos(location) + 1);
                            BeaconAPI.updateJump(location, 1);

                            RemoveItemInv.removeItemDifferentLore(player.getInventory(), player.getItemInHand(), 1);
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.BLOCK_BEACON_POWER_SELECT, 1.0f, 1.0f);
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.GREEN + "\n * Efeito aplicado, sinalizador atualizado.\n ");
                            return;
                        } else if (BeaconAPI.getJump(location) == 1) {
                            BeaconAPI.updateJump(location, 2);

                            RemoveItemInv.removeItemDifferentLore(player.getInventory(), player.getItemInHand(), 1);
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.BLOCK_BEACON_POWER_SELECT, 1.0f, 1.0f);
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.GREEN + "\n * Efeito aplicado, sinalizador atualizado.\n ");
                            return;
                        } else if (BeaconAPI.getJump(location) == 2) {
                            BeaconAPI.updateJump(location, 3);

                            RemoveItemInv.removeItemDifferentLore(player.getInventory(), player.getItemInHand(), 1);
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.BLOCK_BEACON_POWER_SELECT, 1.0f, 1.0f);
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.GREEN + "\n * Efeito aplicado, sinalizador atualizado.\n ");
                            return;
                        } else if (BeaconAPI.getJump(location) == 3) {
                            player.sendMessage(ChatColor.RED + " * Ops, o sinalizador já se encontra em seu nível máximo em super pulo.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    }

                    if (player.getItemInHand().getItemMeta().getLore().get(1).substring(12).equals("Regeneração")) {
                        if (BeaconAPI.getRegen(location) == 0) {
                            BeaconAPI.updateMultiplos(location, BeaconAPI.getMultiplos(location) + 1);
                            BeaconAPI.updateRegeneracao(location, 1);

                            RemoveItemInv.removeItemDifferentLore(player.getInventory(), player.getItemInHand(), 1);
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.BLOCK_BEACON_POWER_SELECT, 1.0f, 1.0f);
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.GREEN + "\n * Efeito aplicado, sinalizador atualizado.\n ");
                            return;
                        } else if (BeaconAPI.getRegen(location) == 1) {
                            BeaconAPI.updateRegeneracao(location, 2);

                            RemoveItemInv.removeItemDifferentLore(player.getInventory(), player.getItemInHand(), 1);
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.BLOCK_BEACON_POWER_SELECT, 1.0f, 1.0f);
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.GREEN + "\n * Efeito aplicado, sinalizador atualizado.\n ");
                            return;
                        } else if (BeaconAPI.getRegen(location) == 2) {
                            player.sendMessage(ChatColor.RED + " * Ops, o sinalizador já se encontra em seu nível máximo em regeneração.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    }

                    if (player.getItemInHand().getItemMeta().getLore().get(1).substring(12).equals("Pressa")) {
                        if (BeaconAPI.getPressa(location) == 0) {
                            BeaconAPI.updateMultiplos(location, BeaconAPI.getMultiplos(location) + 1);
                            BeaconAPI.updatePressa(location, 1);

                            RemoveItemInv.removeItemDifferentLore(player.getInventory(), player.getItemInHand(), 1);
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.BLOCK_BEACON_POWER_SELECT, 1.0f, 1.0f);
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.GREEN + "\n * Efeito aplicado, sinalizador atualizado.\n ");
                            return;
                        } else if (BeaconAPI.getPressa(location) == 1) {
                            BeaconAPI.updatePressa(location, 2);

                            RemoveItemInv.removeItemDifferentLore(player.getInventory(), player.getItemInHand(), 1);
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.BLOCK_BEACON_POWER_SELECT, 1.0f, 1.0f);
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.GREEN + "\n * Efeito aplicado, sinalizador atualizado.\n ");
                            return;
                        } else if (BeaconAPI.getPressa(location) == 2) {
                            BeaconAPI.updatePressa(location, 3);

                            RemoveItemInv.removeItemDifferentLore(player.getInventory(), player.getItemInHand(), 1);
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.BLOCK_BEACON_POWER_SELECT, 1.0f, 1.0f);
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.GREEN + "\n * Efeito aplicado, sinalizador atualizado.\n ");
                            return;
                        } else if (BeaconAPI.getPressa(location) == 3) {
                            player.sendMessage(ChatColor.RED + " * Ops, o sinalizador já se encontra em seu nível máximo em pressa.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    }

                    if (player.getItemInHand().getItemMeta().getLore().get(1).substring(12).equals("Força")) {
                        if (BeaconAPI.getForca(location) == 0) {
                            BeaconAPI.updateMultiplos(location, BeaconAPI.getMultiplos(location) + 1);
                            BeaconAPI.updateForca(location, 1);

                            RemoveItemInv.removeItemDifferentLore(player.getInventory(), player.getItemInHand(), 1);
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.BLOCK_BEACON_POWER_SELECT, 1.0f, 1.0f);
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.GREEN + "\n * Efeito aplicado, sinalizador atualizado.\n ");
                            return;
                        } else if (BeaconAPI.getForca(location) == 1) {
                            BeaconAPI.updateForca(location, 2);

                            RemoveItemInv.removeItemDifferentLore(player.getInventory(), player.getItemInHand(), 1);
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.BLOCK_BEACON_POWER_SELECT, 1.0f, 1.0f);
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.GREEN + "\n * Efeito aplicado, sinalizador atualizado.\n ");
                            return;
                        } else if (BeaconAPI.getForca(location) == 2) {
                            player.sendMessage(ChatColor.RED + " * Ops, o sinalizador já se encontra em seu nível máximo em força.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    }
                } else {
                    event.setCancelled(true);
                    final Beacon beacon = (Beacon) event.getClickedBlock().getState();

                    if (beacon.getTier() == 0) {
                        player.openInventory(inativatedBeacon(0));
                    } else if (beacon.getTier() == 1) {
                        player.openInventory(inativatedBeacon(1));
                    } else if (beacon.getTier() == 2) {
                        player.openInventory(inativatedBeacon(2));
                    } else if (beacon.getTier() == 3) {
                        player.openInventory(inativatedBeacon(3));
                    } else if (beacon.getTier() == 4) {
                        final String location = event.getClickedBlock().getWorld().getName() + ":" + event.getClickedBlock().getX() + ":" + event.getClickedBlock().getY() + ":" + event.getClickedBlock().getZ();

                        if (InventoryLocation.get(UUID) == null) {
                            new InventoryLocation(UUID, location).insert();
                        } else {
                            InventoryLocation.get(UUID).setLocation(location);
                        }

                        if (BeaconAPI.getExistBeacon(location) == null) {
                            BeaconAPI.createBeacon(UUID, location);
                        }
                        player.openInventory(beacon(location));
                    }
                }
            }
        }

        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && !player.isSneaking()) {
            if (event.getClickedBlock().getType().equals(Material.BEACON)) {
                event.setCancelled(true);
                final Beacon beacon = (Beacon) event.getClickedBlock().getState();

                if (beacon.getTier() == 0) {
                    player.openInventory(inativatedBeacon(0));
                } else if (beacon.getTier() == 1) {
                    player.openInventory(inativatedBeacon(1));
                } else if (beacon.getTier() == 2) {
                    player.openInventory(inativatedBeacon(2));
                } else if (beacon.getTier() == 3) {
                    player.openInventory(inativatedBeacon(3));
                } else if (beacon.getTier() == 4) {
                    final String location = event.getClickedBlock().getWorld().getName() + ":" + event.getClickedBlock().getX() + ":" + event.getClickedBlock().getY() + ":" + event.getClickedBlock().getZ();

                    if (InventoryLocation.get(UUID) == null) {
                        new InventoryLocation(UUID, location).insert();
                    } else {
                        InventoryLocation.get(UUID).setLocation(location);
                    }

                    if (BeaconAPI.getExistBeacon(location) == null) {
                        BeaconAPI.createBeacon(UUID, location);
                    }
                    player.openInventory(beacon(location));
                }
            }
        }
    }
}
