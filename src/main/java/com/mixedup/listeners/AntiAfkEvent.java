package com.mixedup.listeners;

import com.mixedup.Main;
import com.mixedup.apis.TagAPI;
import com.mixedup.hashs.AntiAfkHash;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Arrays;

public class AntiAfkEvent implements Listener {

    public static void antiAfk() {
        final Thread thread = new Thread(() -> {
            while (true) {
                try {
                    Thread.currentThread().sleep(1000 * 60);
                } catch (final Exception e) {
                    e.printStackTrace();
                }

                for (Player target : Bukkit.getOnlinePlayers()) {
                    String tag = TagAPI.getTag(target.getUniqueId().toString());
                    String[] staff = {"Master", "Supervisor", "Admin", "Moderador", "Suporte"};

                    if (Arrays.asList(staff).contains(tag)) continue;

                    int vip = 1;
                    if (tag.equalsIgnoreCase("Vip") || tag.equalsIgnoreCase("Vip+") || tag.equalsIgnoreCase("Vip++") || tag.equalsIgnoreCase("Vip+++")) {
                        vip = 2;
                    }

                    if (AntiAfkHash.get(target.getUniqueId().toString()) == null) {
                        String view = target.getLocation().getYaw() + ":" + target.getLocation().getPitch();
                        new AntiAfkHash(target.getUniqueId().toString(), view, 1).insert();

                        continue;
                    } else {
                        String view = target.getLocation().getYaw() + ":" + target.getLocation().getPitch();
                        if (AntiAfkHash.get(target.getUniqueId().toString()).getView().equalsIgnoreCase(view)) {
                            AntiAfkHash.get(target.getUniqueId().toString()).setTimes(AntiAfkHash.get(target.getUniqueId().toString()).getTimes() + 1);

                            if (AntiAfkHash.get(target.getUniqueId().toString()).getTimes() == 9 * vip) {
                                target.sendMessage(ChatColor.RED + " * Dentro de 1 minuto você sera kickado por se encontrar AFK!");
                                target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }

                            if (AntiAfkHash.get(target.getUniqueId().toString()).getTimes() > 10 * vip) {
                                AntiAfkHash.cache.remove(target.getUniqueId().toString());

                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                    @Override
                                    public void run() {
                                        target.kickPlayer(ChatColor.RED + "Kickado por: anti-afk");
                                    }
                                }, 10L);
                            }
                        } else {
                            AntiAfkHash.get(target.getUniqueId().toString()).setView(view);
                            AntiAfkHash.get(target.getUniqueId().toString()).setTimes(1);
                        }
                    }
                }
            }
        });
        thread.setName("Anti-afk");
        thread.start();
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        if (AntiAfkHash.get(event.getPlayer().getUniqueId().toString()) != null) {
            AntiAfkHash.cache.remove(event.getPlayer().getUniqueId().toString());
        }
    }
}
