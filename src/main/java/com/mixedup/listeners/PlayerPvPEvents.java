package com.mixedup.listeners;

import com.mixedup.Main;
import com.mixedup.apis.*;
import com.mixedup.hashs.LastHitHash;
import com.mixedup.worldProtection.hashs.ProtectionUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Date;

public class PlayerPvPEvents implements Listener {

    @EventHandler(priority = EventPriority.LOW)
    public void onHited(final EntityDamageByEntityEvent event) {
        if (event.isCancelled()) return;
        if (event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
            if (event.getDamager().getWorld().getName().equals("Kepler-438b")) {
                event.setCancelled(true);
            }

            if (event.getDamager().getWorld().getName().equalsIgnoreCase("spawn") && ArenaEvents.playerInArea(event.getDamager().getLocation()) == false)
                return;
            if (event.getEntity().getWorld().getName().equalsIgnoreCase("spawn") && ArenaEvents.playerInArea(event.getEntity().getLocation()) == false)
                return;
            if (event.getDamager().getWorld().getName().equalsIgnoreCase("guildaselect")) return;
            if (event.getEntity().getWorld().getName().equalsIgnoreCase("guildaselect")) return;
            if (ProtectionUtil.playerInArea(event.getDamager().getLocation()) != null) return;
            if (ProtectionUtil.playerInArea(event.getEntity().getLocation()) != null) return;

            final Player player = (Player) event.getDamager();
            final String UUID = PlayerUUID.getUUID(player.getName());
            final Date date = new Date();
            date.setSeconds(date.getSeconds() + 15);
            if (LastHitHash.get(UUID) == null) {
                new LastHitHash(UUID, date).insert();
            } else {
                LastHitHash.get(UUID).setDate(date);
            }

            final Player target = (Player) event.getEntity();
            final String UUIDtarget = PlayerUUID.getUUID(target.getName());
            final Date date2 = new Date();
            date2.setSeconds(date.getSeconds() + 15);
            if (LastHitHash.get(UUIDtarget) == null) {
                new LastHitHash(UUIDtarget, date2).insert();
            } else {
                LastHitHash.get(UUIDtarget).setDate(date2);
            }
        }
    }

    @EventHandler
    public void onDeath(final EntityDeathEvent event) {
        if (event.getEntity() instanceof Player && event.getEntity().getKiller() instanceof Player) {
            final String killerID = PlayerUUID.getUUID(event.getEntity().getKiller().getName());
            if (KDRApi.getExistInfo(killerID) == null) {
                KDRApi.createKDR(killerID);
            } else {
                KDRApi.updateKills(killerID, KDRApi.getKills(killerID) + 1);
            }

            final String killedID = PlayerUUID.getUUID(event.getEntity().getName());
            if (KDRApi.getExistInfo(killedID) == null) {
                KDRApi.createKDR(killedID);
            } else {
                KDRApi.updateDeaths(killedID, KDRApi.getDeaths(killedID) + 1);
                if (PoderAPI.getPoder(killedID) > 0) {
                    PoderAPI.updatePoder(killedID, PoderAPI.getPoder(killedID) - 1);
                }
                LastHitHash.CACHE.remove(killedID);
            }
        }
    }

    @EventHandler
    public void onRespawn(final PlayerRespawnEvent event) {
        final String[] spawn = RegionsSpawnAPI.getLocation("spawn").split(":");
        final Location location = new Location(Bukkit.getWorld(spawn[0]), Double.parseDouble(spawn[1]), Double.parseDouble(spawn[2]), Double.parseDouble(spawn[3]), Float.parseFloat(spawn[4]), Float.parseFloat(spawn[5]));
        event.setRespawnLocation(location);
        new BukkitRunnable() {
            @Override
            public void run() {
                Main.scoreboardManager.setDefaultScoreboard(event.getPlayer());
            }
        }.runTaskLater(Main.getInstance(), 50L);
    }

    @EventHandler
    public void onDeath(final PlayerDeathEvent event) {
        final Player player = event.getEntity();
        final String UUID = PlayerUUID.getUUID(player.getName());
        final String loc = event.getEntity().getLocation().getWorld().getName() + ":" + player.getLocation().getX() + ":" + player.getLocation().getY() + ":" + player.getLocation().getZ();

        LastPositionAPI.updateLocation(UUID, loc);

        event.setDeathMessage(null);
    }
}
