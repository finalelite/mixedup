package com.mixedup.listeners;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.VillagerAcquireTradeEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.inventory.MerchantRecipe;

import java.util.ArrayList;
import java.util.List;

public class MerchantEvents implements Listener {

    private static List<MerchantRecipe> filterRecipes(final List<MerchantRecipe> recipes) {
        final ArrayList<MerchantRecipe> newRecipes = new ArrayList<>(recipes);
        newRecipes.removeIf(merchantRecipe -> merchantRecipe.getResult().getType() == Material.EMERALD);
        newRecipes.removeIf(merchantRecipe -> merchantRecipe.getResult().getType() == Material.ENCHANTED_BOOK);
        return newRecipes;
    }

    @EventHandler
    public void onOpenVillager(final PlayerInteractAtEntityEvent event) {
        if (event.getRightClicked().getType() != EntityType.VILLAGER)
            return;

        final Villager villager = (Villager) event.getRightClicked();
        villager.setRecipes(MerchantEvents.filterRecipes(villager.getRecipes()));
    }

    @EventHandler
    public void onAcquireTrade(final VillagerAcquireTradeEvent event) {
        final Villager villager = event.getEntity();
        villager.setRecipes(MerchantEvents.filterRecipes(villager.getRecipes()));
    }
}
