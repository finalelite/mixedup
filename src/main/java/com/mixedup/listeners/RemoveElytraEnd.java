package com.mixedup.listeners;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.hashs.LastChunkHash;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class RemoveElytraEnd implements Listener {

    @EventHandler
    public void onRemove(final PlayerMoveEvent event) {
        if (event.getPlayer().getLocation().getWorld().getName().equalsIgnoreCase("world_the_end")) {
            final Player player = event.getPlayer();
            final String UUID = PlayerUUID.getUUID(player.getName());

            final String chunk = player.getChunk().getX() + ":" + player.getChunk().getZ();
            if (LastChunkHash.get(UUID) != null) {
                if (!LastChunkHash.get(UUID).getChunk().equalsIgnoreCase(chunk)) {
                    LastChunkHash.get(UUID).setChunk(chunk);

                    for (final Entity entity : event.getPlayer().getChunk().getEntities()) {
                        if (entity.getType().equals(EntityType.ITEM_FRAME)) {
                            entity.remove();
                        }
                    }
                }
            } else {
                new LastChunkHash(UUID, chunk).insert();

                for (final Entity entity : event.getPlayer().getChunk().getEntities()) {
                    if (entity.getType().equals(EntityType.ITEM_FRAME)) {
                        entity.remove();
                    }
                }
            }
        }
    }
}
