package com.mixedup.listeners;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.economy.CoinsAPI;
import com.mixedup.utils.RemoveItemInv;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import java.text.DecimalFormat;

public class PlayerActiveCoins implements Listener {

    @EventHandler
    public void onInteractItem(final PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        final DecimalFormat formatter = new DecimalFormat("#,###.00");

        if (event.getAction().equals(Action.RIGHT_CLICK_AIR)) {
            if (event.getItem().getType() != null && event.getItem().getType().equals(Material.GOLD_NUGGET)) {
                if (event.getItem().getItemMeta().getDisplayName().contains("§e$") && event.getItem().getItemMeta().getDisplayName().contains("moedas")) {

                    final String msg = event.getItem().getItemMeta().getDisplayName();
                    final String a = msg.replace(",00 moedas", "");
                    final String b = a.replace(".", "");
                    final String c = b.replace("$", "");
                    final String d = c.replace("§e", "");

                    final int valor = Integer.valueOf(d);

                    CoinsAPI.updateCoins(UUID, CoinsAPI.getCoins(UUID) + valor);
                    RemoveItemInv.removeItems(player.getInventory(), event.getItem(), 1);
                    player.sendMessage(ChatColor.YELLOW + "\n * Foram adicionados a sua conta $" + formatter.format(valor) + " moedas\n ");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VEX_CHARGE, 1.0f, 1.0f);
//                    if (Main.scoreboardManager.hasSquadScoreboard(player)) {
//
//                    } else {
//                        Main.scoreboardManager.getDefaultScoreboard().updateEntry(player, "coins");
//                    }
                }
            } else if (event.getPlayer().getItemInHand() != null && event.getPlayer().getItemInHand().getType().equals(Material.GHAST_TEAR)) {
                if (event.getItem().getItemMeta().getDisplayName().contains(ChatColor.YELLOW + "$") && event.getItem().getItemMeta().getDisplayName().contains("moedas negra")) {

                    final String msg = event.getItem().getItemMeta().getDisplayName();
                    final String a = msg.replace(",00 moedas negra", "");
                    final String b = a.replace(".", "");
                    final String c = b.replace("$", "");
                    final String d = c.replace("§e", "");

                    final int valor = Integer.valueOf(d);

                    CoinsAPI.updateBlackCoins(UUID, CoinsAPI.getBlackCoins(UUID) + valor);
                    RemoveItemInv.removeItems(player.getInventory(), event.getItem(), 1);
                    player.sendMessage(ChatColor.YELLOW + "\n * Foram adicionados a sua conta $" + formatter.format(valor) + " moedas negras\n ");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VEX_CHARGE, 1.0f, 1.0f);
//                    if (Main.scoreboardManager.hasSquadScoreboard(player)) {
//
//                    } else {
//                        Main.scoreboardManager.getDefaultScoreboard().updateEntry(player, "blackcoins");
//                    }
                }
            }
        }
    }
}
