package com.mixedup.listeners;

import com.mixedup.Main;
import com.mixedup.utils.RemoveItemInv;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

public class LivroSortidoEvent implements Listener {

    private static ItemStack lastGlass;

    private static ItemStack randomBook() {
        final Enchantment[] list = Enchantment.values();
        final Random random = new Random();
        final Enchantment enchantRandom = list[random.nextInt(list.length)];

        int level = random.nextInt(enchantRandom.getMaxLevel());
        if (level == 0) level = 1;

        final ItemStack item = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta meta = (EnchantmentStorageMeta) item.getItemMeta();
        meta.addStoredEnchant(enchantRandom, level, true);
        final ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.GRAY + "Sorte: " + (50 + random.nextInt(20)) + "%");
        lore.add(" ");
        meta.setLore(lore);
        meta.setDisplayName(ChatColor.YELLOW + "Livro encantado");
        item.setItemMeta(meta);

        return item;
    }

    private static ItemStack getGlass() {
        final ItemStack[] list = {new ItemStack(Material.WHITE_STAINED_GLASS_PANE), new ItemStack(Material.BLACK_STAINED_GLASS_PANE), new ItemStack(Material.BLUE_STAINED_GLASS_PANE)
                , new ItemStack(Material.BROWN_STAINED_GLASS_PANE), new ItemStack(Material.CYAN_STAINED_GLASS_PANE), new ItemStack(Material.GRAY_STAINED_GLASS_PANE)
                , new ItemStack(Material.GREEN_STAINED_GLASS_PANE), new ItemStack(Material.LIGHT_BLUE_STAINED_GLASS_PANE), new ItemStack(Material.LIGHT_GRAY_STAINED_GLASS_PANE)
                , new ItemStack(Material.LIME_STAINED_GLASS_PANE), new ItemStack(Material.MAGENTA_STAINED_GLASS_PANE), new ItemStack(Material.ORANGE_STAINED_GLASS_PANE)
                , new ItemStack(Material.PINK_STAINED_GLASS_PANE), new ItemStack(Material.PURPLE_STAINED_GLASS_PANE), new ItemStack(Material.RED_STAINED_GLASS_PANE)};

        final Random random = new Random();
        ItemStack item = list[random.nextInt(14)];

        if (LivroSortidoEvent.lastGlass != null) {
            while (item.getType().name().equals(LivroSortidoEvent.lastGlass.getType().name())) {
                item = list[random.nextInt(14)];
            }
        }
        return list[random.nextInt(14)];
    }

    @EventHandler
    public void onClick(final InventoryClickEvent event) {
        if (event.getClickedInventory() != null && event.getClickedInventory().getName().equalsIgnoreCase("Livro sortido: ")) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInteractBook(final PlayerInteractEvent event) {
        final Player player = event.getPlayer();

        if (event.getAction().equals(Action.RIGHT_CLICK_AIR)) {
            if (event.getPlayer().getItemInHand() != null && event.getPlayer().getItemInHand().getType().equals(Material.ENCHANTED_BOOK)) {
                if (event.getPlayer().getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "Livro sortido")) {
                    RemoveItemInv.removeItems(player.getInventory(), player.getItemInHand(), 1);

                    final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Livro sortido: ");

                    final int rotate = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                        boolean open;

                        @Override
                        public void run() {
                            for (int i = 0; i <= 26; i++) {
                                if (i <= 9) {
                                    final ItemStack glass = LivroSortidoEvent.getGlass();
                                    LivroSortidoEvent.lastGlass = glass;
                                    inventory.setItem(i, glass);
                                } else if (i > 9 && i <= 16) {
                                    inventory.setItem(i, LivroSortidoEvent.randomBook());
                                } else if (i > 16) {
                                    final ItemStack glass = LivroSortidoEvent.getGlass();
                                    LivroSortidoEvent.lastGlass = glass;
                                    inventory.setItem(i, glass);
                                }
                            }
                            if (this.open == false) {
                                player.openInventory(inventory);
                                this.open = true;
                            }
                            player.playSound(player.getLocation(), Sound.BLOCK_WOODEN_DOOR_CLOSE, 1.0f, 1.0f);
                            if (player.getOpenInventory() != null && player.getOpenInventory().getTopInventory().getName().equalsIgnoreCase("Livro sortido: ")) {
                                player.openInventory(inventory);
                            }
                        }
                    }, 0L, 10L);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            Bukkit.getScheduler().cancelTask(rotate);

                            final ItemStack book = LivroSortidoEvent.randomBook();
                            for (int i = 0; i <= 26; i++) {
                                if (i <= 9) {
                                    final ItemStack glass = LivroSortidoEvent.getGlass();
                                    LivroSortidoEvent.lastGlass = glass;
                                    inventory.setItem(i, glass);
                                } else if (i > 9 && i <= 16) {
                                    inventory.setItem(i, book);
                                } else if (i > 16) {
                                    final ItemStack glass = LivroSortidoEvent.getGlass();
                                    LivroSortidoEvent.lastGlass = glass;
                                    inventory.setItem(i, glass);
                                }
                            }

                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.openInventory(inventory);

                            final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(book);
                            for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                            }
                        }
                    }, 120L);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            if (player.getOpenInventory() != null && player.getOpenInventory().getTopInventory().getName().equalsIgnoreCase("Livro sortido: ")) {
                                player.getOpenInventory().close();
                                player.playSound(player.getLocation(), Sound.BLOCK_CHEST_CLOSE, 1.0f, 1.0f);
                            }
                        }
                    }, 180L);
                }
            }
        }
    }
}
