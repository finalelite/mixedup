package com.mixedup.listeners;

import com.destroystokyo.paper.inventory.ItemStackRecipeChoice;
import com.mixedup.commands.CommandAmuleto;
import org.bukkit.*;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.RecipeChoice;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

import java.util.ArrayList;

public class PlayerCraftItens implements Listener {

    public void PlayerCraftItens() {
        final ItemStack frag1 = new ItemStack(Material.FIREWORK_STAR);
        final ItemMeta metaFrag1 = frag1.getItemMeta();
        metaFrag1.setDisplayName(ChatColor.GREEN + "Fragmento de amuleto");
        final ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.WHITE + "Compactado: " + ChatColor.GRAY + "Tipo 1");
        lore.add(" ");
        metaFrag1.setLore(lore);
        frag1.setItemMeta(metaFrag1);

        final ItemStack frag2 = new ItemStack(Material.PURPLE_DYE);
        final ItemMeta metaFrag2 = frag2.getItemMeta();
        metaFrag2.setDisplayName(ChatColor.GREEN + "Fragmento de amuleto");
        final ArrayList<String> lore2 = new ArrayList<>();
        lore2.add(" ");
        lore2.add(ChatColor.WHITE + "Compactado: " + ChatColor.GRAY + "Tipo 2");
        lore2.add(" ");
        metaFrag2.setLore(lore2);
        frag2.setItemMeta(metaFrag2);

        final ItemStack frag3 = new ItemStack(Material.ORANGE_DYE);
        final ItemMeta metaFrag3 = frag3.getItemMeta();
        metaFrag3.setDisplayName(ChatColor.GREEN + "Fragmento de amuleto");
        final ArrayList<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.WHITE + "Compactado: " + ChatColor.GRAY + "Tipo 3");
        lore3.add(" ");
        metaFrag3.setLore(lore3);
        frag3.setItemMeta(metaFrag3);

        final ItemStack amuleto = new ItemStack(Material.NAME_TAG);
        final ItemMeta metaAmulet = amuleto.getItemMeta();
        metaAmulet.setDisplayName(ChatColor.GOLD + "Amuleto wither");
        final ArrayList<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.GRAY + "Este amuleto serve como proteção e também");
        lore4.add(ChatColor.GRAY + "garante o sucesso ao spawnar um wither!");
        lore4.add(" ");
        metaAmulet.setLore(lore4);
        amuleto.setItemMeta(metaAmulet);


        //-

        ShapedRecipe recipe1 = new ShapedRecipe(CommandAmuleto.addGlow(frag2));

        recipe1.shape("###", "###", "###");
        recipe1.setIngredient('#', new MaterialData(frag1.getType(), frag1.getData().getData()));

        Bukkit.getServer().addRecipe(recipe1);

        //-

        ShapedRecipe recipe2 = new ShapedRecipe(CommandAmuleto.addGlow(frag3));

        recipe2.shape("###", "###", "###");
        recipe2.setIngredient('#', new MaterialData(frag2.getType(), frag2.getData().getData()));

        Bukkit.getServer().addRecipe(recipe2);

        //-

        ShapedRecipe recipe3 = new ShapedRecipe(CommandAmuleto.addGlow(amuleto));

        recipe3.shape("###", "###", "###");
        recipe3.setIngredient('#', new MaterialData(frag3.getType(), frag3.getData().getData()));

        Bukkit.getServer().addRecipe(recipe3);
    }

    @EventHandler
    public void onCraft(final PrepareItemCraftEvent event) {
        if (event.getRecipe() != null) {
            if (event.getRecipe().getResult().getType().equals(Material.EMERALD_BLOCK) || event.getRecipe().getResult().getType().equals(Material.ENCHANTING_TABLE) || event.getRecipe().getResult().getType().equals(Material.ELYTRA) || event.getRecipe().getResult().getType().equals(Material.TNT)) {
                event.getInventory().setResult(new ItemStack(Material.AIR));

                for (final HumanEntity entitys : event.getViewers()) {
                    if (entitys instanceof Player) {
                        entitys.sendMessage(ChatColor.RED + " * Ops, este crafting está bloqueado!");
                        ((Player) entitys).playSound(entitys.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
            }
        }
    }
}
