package com.mixedup.listeners;

import com.mixedup.apis.TagAPI;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.ShulkerBox;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class SkulkerBoxEvent implements Listener {

    @EventHandler
    public void onPlace(final BlockPlaceEvent event) {
        if (event.getBlock().getType().name().contains(Material.SHULKER_BOX.name())) {
            final Block block = event.getBlock();
            final ShulkerBox shulker = (ShulkerBox) block.getState();
            shulker.setCustomName(event.getPlayer().getName());
            shulker.update();
        }
    }

    @EventHandler
    public void onBreak(final BlockBreakEvent event) {
        final Player player = event.getPlayer();

        if (event.getBlock().getType().name().contains(Material.SHULKER_BOX.name())) {
            if (event.getPlayer() == null) {
                event.setCancelled(true);
                return;
            }
            final String tag = TagAPI.getTag(player.getUniqueId().toString());
            if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Admin") || tag.equalsIgnoreCase("Moderador")) {
                return;
            }

            final ShulkerBox shulker = (ShulkerBox) event.getBlock().getState();
            if (shulker.getCustomName() == null) return;
            if (!shulker.getCustomName().equalsIgnoreCase(event.getPlayer().getName())) {
                event.setCancelled(true);
                event.getPlayer().sendMessage(ChatColor.RED + " * Ops, está shulker não pertencem a você!");
                event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            }
        }
    }

    @EventHandler
    public void onOpenShulker(final PlayerInteractEvent event) {
        final Player player = event.getPlayer();

        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (event.getClickedBlock() != null && event.getClickedBlock().getType().name().contains(Material.SHULKER_BOX.name())) {
                final ShulkerBox shulker = (ShulkerBox) event.getClickedBlock().getState();

                if (shulker.getCustomName() == null) return;

                final String tag = TagAPI.getTag(player.getUniqueId().toString());
                if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Admin") || tag.equalsIgnoreCase("Moderador")) {
                    if (player.getItemInHand().getType().equals(Material.STICK)) {
                        player.sendMessage(ChatColor.GREEN + " * Dono: " + ChatColor.GRAY + shulker.getCustomName());
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                    return;
                }
                if (!shulker.getCustomName().equalsIgnoreCase(event.getPlayer().getName())) {
                    event.setCancelled(true);
                    event.getPlayer().sendMessage(ChatColor.RED + " * Ops, está shulker não pertencem a você!");
                    event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }
        }
    }

    @EventHandler
    public void onPistonBreakead(final BlockPistonExtendEvent event) {
        for (final Block blocks : event.getBlocks()) {
            if (blocks.getType().name().contains(Material.SHULKER_BOX.name())) {
                event.setCancelled(true);
            }
        }
    }
}
