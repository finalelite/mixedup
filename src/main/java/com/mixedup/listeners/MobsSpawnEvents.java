package com.mixedup.listeners;

import com.mixedup.Main;
import com.mixedup.apis.GuildaAPI;
import com.mixedup.mcmmo.ProbabilityApi;
import com.mixedup.mcmmo.utils.SkillKiller;
import com.mixedup.plots.espaconave.MobspawnAPI;
import com.mixedup.utils.RemoveItemInv;
import org.bukkit.*;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class MobsSpawnEvents implements Listener {

    public static void dropItens(final List<ItemStack> itens, final Location location, final int death) {

        boolean haveNautilus = false;
        List<ItemStack> removeItens = new ArrayList<>();
        for (ItemStack item : itens) {
            if (item.getType().name().contains(Material.NAUTILUS_SHELL.name())) {
                haveNautilus = true;
            }
            if (item.getType().name().contains("_CHESTPLATE") || item.getType().name().contains("_LEGGINGS") || item.getType().name().contains("_BOOTS") || item.getType().name().contains("_HELMET") || item.getType().name().contains("_SWORD")
                    || item.getType().name().contains(Material.CARROT_ON_A_STICK.name()) || item.getType().name().contains(Material.POTION.name()) || item.getType().name().contains(Material.LINGERING_POTION.name()) || item.getType().name().contains(Material.SPLASH_POTION.name())
                    || item.getType().name().contains(Material.NAUTILUS_SHELL.name())) {
                removeItens.add(item);
            }
        }
        for (ItemStack r : removeItens) {
            itens.remove(r);
        }


        final Random random = new Random();
        final int r = random.nextInt(8);
        for (int i = 1; i <= death + (r - 4); i++) {
            for (final ItemStack item : itens) {
                location.getWorld().dropItemNaturally(location, item);
            }
        }
        if (haveNautilus) {
            location.getWorld().dropItemNaturally(location, new ItemStack(Material.NAUTILUS_SHELL));
        }
    }

    public static int getQuantiaSpawners(final String location, final String mob) {
        int quantia = 0;

        if (MobspawnAPI.getSpawnersFromLoc(location).contains(":")) {
            final String[] split = MobspawnAPI.getSpawnersFromLoc(location).split(":");

            for (int i = 1; i <= split.length; i++) {
                final String[] split2 = split[i - 1].split("-");

                if (split2[0].contains(mob)) {
                    quantia += Integer.valueOf(split2[1]);
                }
            }
        } else {
            final String[] split = MobspawnAPI.getSpawnersFromLoc(location).split("-");

            if (split[0].contains(mob)) {
                quantia = Integer.valueOf(split[1]);
            }
        }

        return quantia;
    }

    private static boolean updateMetadata(final List<Entity> list, final int i, final SpawnReason spawnReason) {
        final Entity entity = list.get(i - 1);

        if (entity.hasMetadata("spawner")) {
            final int value = entity.getMetadata("spawner").get(0).asInt();
            if (value >= 2000) {
                return true;
            }
        } else if (entity.hasMetadata("natural")) {
            final int value = entity.getMetadata("natural").get(0).asInt();
            if (value >= 200) {
                return true;
            }
        }

        if (spawnReason.equals(SpawnReason.SPAWNER)) {
            if (entity.hasMetadata("spawner")) {
                final int value = entity.getMetadata("spawner").get(0).asInt();
                entity.removeMetadata("spawner", Main.getInstance());
                entity.setMetadata("spawner", new FixedMetadataValue(Main.getInstance(), value + 1));
            } else {
                entity.setMetadata("spawner", new FixedMetadataValue(Main.getInstance(), 1));
            }
        } else if (entity.hasMetadata("natural")) {
            final int value = entity.getMetadata("natural").get(0).asInt();
            entity.removeMetadata("natural", Main.getInstance());
            entity.setMetadata("natural", new FixedMetadataValue(Main.getInstance(), value + 1));
        } else {
            entity.setMetadata("natural", new FixedMetadataValue(Main.getInstance(), 1));
        }
        return false;
    }

    public static EntityType getEntityType(final String mob) {
        EntityType entityType = null;

        if (mob.equalsIgnoreCase("vaca")) {
            entityType = EntityType.COW;
        }
        if (mob.equalsIgnoreCase("galinha")) {
            entityType = EntityType.CHICKEN;
        }
        if (mob.equalsIgnoreCase("porco")) {
            entityType = EntityType.PIG;
        }
        if (mob.equalsIgnoreCase("ovelha")) {
            entityType = EntityType.SHEEP;
        }
        if (mob.equalsIgnoreCase("coelho")) {
            entityType = EntityType.RABBIT;
        }
        if (mob.equalsIgnoreCase("zumbi")) {
            entityType = EntityType.ZOMBIE;
        }
        if (mob.equalsIgnoreCase("esqueleto")) {
            entityType = EntityType.SKELETON;
        }
        if (mob.equalsIgnoreCase("aranha")) {
            entityType = EntityType.SPIDER;
        }
        if (mob.equalsIgnoreCase("creeper")) {
            entityType = EntityType.CREEPER;
        }
        if (mob.equalsIgnoreCase("enderman")) {
            entityType = EntityType.ENDERMAN;
        }
        if (mob.equalsIgnoreCase("aranha das cavernas")) {
            entityType = EntityType.CAVE_SPIDER;
        }
        if (mob.equalsIgnoreCase("slime")) {
            entityType = EntityType.SLIME;
        }
        if (mob.equalsIgnoreCase("blaze")) {
            entityType = EntityType.BLAZE;
        }
        if (mob.equalsIgnoreCase("homem porco zumbi")) {
            entityType = EntityType.PIG_ZOMBIE;
        }
        if (mob.equalsIgnoreCase("esqueleto wither")) {
            entityType = EntityType.WITHER_SKELETON;
        }
        if (mob.equalsIgnoreCase("bruxa")) {
            entityType = EntityType.WITCH;
        }
        if (mob.equalsIgnoreCase("cubo magma")) {
            entityType = EntityType.MAGMA_CUBE;
        }
        if (mob.equalsIgnoreCase("aldeão zumbi")) {
            entityType = EntityType.ZOMBIE_VILLAGER;
        }
        if (mob.equalsIgnoreCase("ghast")) {
            entityType = EntityType.GHAST;
        }
        //ADD +2 MOBS 1.13!

        return entityType;
    }

    @EventHandler
    public void onDeath(final EntityDeathEvent event) {
        if (event.getEntityType().equals(EntityType.WITHER_SKELETON)) {
            if (event.getEntity().getKiller() == null) return;
            final int level = GuildaAPI.getGuildaRank(event.getEntity().getKiller().getUniqueId().toString());

            final int probability = 750 * level;
            if (ProbabilityApi.probab(probability) == true) {
                final ItemStack frag1 = new ItemStack(Material.FIREWORK_STAR);
                final ItemMeta metaFrag1 = frag1.getItemMeta();
                metaFrag1.setDisplayName(ChatColor.GREEN + "Fragmento de amuleto");
                final ArrayList<String> lore = new ArrayList<>();
                lore.add(" ");
                lore.add(ChatColor.WHITE + "Compactado: " + ChatColor.GRAY + "Tipo 1");
                lore.add(" ");
                metaFrag1.setLore(lore);
                frag1.setItemMeta(metaFrag1);

                event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), frag1);
            }
        }

        if (event.getEntityType().equals(EntityType.WITCH)) {
            if (event.getEntity().getKiller() == null) return;
            final int level = GuildaAPI.getGuildaRank(event.getEntity().getKiller().getUniqueId().toString());

            final int probability = 750 * level;
            if (ProbabilityApi.probab(probability) == true) {
                final Random random = new Random();
                final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK);
                final ItemMeta metaChave = chave.getItemMeta();
                metaChave.setDisplayName(ChatColor.GREEN + "Chave");
                final List<String> lore = new ArrayList<>();
                lore.add(" ");
                lore.add(ChatColor.GRAY + "Utilizado para abrir caixas");
                lore.add(ChatColor.YELLOW + String.valueOf(random.nextInt(10) + 10) + "% sucesso");
                lore.add(" ");
                metaChave.setLore(lore);
                chave.setItemMeta(metaChave);

                event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), chave);
            }

            final int probability2 = 750 * level;
            if (ProbabilityApi.probab(probability2) == true) {
                final ItemStack papel = new ItemStack(Material.PAPER);
                final ItemMeta meta = papel.getItemMeta();
                meta.setDisplayName(ChatColor.YELLOW + "Papiro com escritos");
                final List<String> lore = new ArrayList<>();
                lore.add(" ");
                lore.add(ChatColor.YELLOW + "Escritos: " + ChatColor.DARK_GRAY + "Comum");
                lore.add(" ");
                meta.setLore(lore);
                papel.setItemMeta(meta);

                event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), papel);
            }

            final int probability3 = 500 * level;
            if (ProbabilityApi.probab(probability3) == true) {
                final ItemStack papel1 = new ItemStack(Material.PAPER);
                final ItemMeta meta1 = papel1.getItemMeta();
                meta1.setDisplayName(ChatColor.YELLOW + "Papiro com escritos");
                final List<String> lore1 = new ArrayList<>();
                lore1.add(" ");
                lore1.add(ChatColor.YELLOW + "Escritos: " + ChatColor.DARK_GRAY + "Raro");
                lore1.add(" ");
                meta1.setLore(lore1);
                papel1.setItemMeta(meta1);

                event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), papel1);
            }

            final int probability4 = 250 * level;
            if (ProbabilityApi.probab(probability4) == true) {
                final ItemStack papel2 = new ItemStack(Material.PAPER);
                final ItemMeta meta2 = papel2.getItemMeta();
                meta2.setDisplayName(ChatColor.YELLOW + "Papiro com escritos");
                final List<String> lore2 = new ArrayList<>();
                lore2.add(" ");
                lore2.add(ChatColor.YELLOW + "Escritos: " + ChatColor.DARK_GRAY + "Super raro");
                lore2.add(" ");
                meta2.setLore(lore2);
                papel2.setItemMeta(meta2);

                event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), papel2);
            }
        }
    }

    @EventHandler
    public void pickupItem(final EntityPickupItemEvent event) {
        if (!event.getEntityType().equals(EntityType.PLAYER)) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onSpawnWither(final CreatureSpawnEvent event) {
        if (event.getSpawnReason().equals(SpawnReason.BUILD_WITHER)) {
            final List<Entity> next = event.getEntity().getNearbyEntities(20, 20, 20);

            final List<Player> players = new ArrayList<>();
            for (final Entity entity : next) {
                if (entity instanceof Player) {
                    players.add((Player) entity);
                }
            }

            boolean have = false;
            Player target = null;
            for (final Player pls : players) {
                for (int i = 1; i <= pls.getInventory().getSize(); i++) {
                    final ItemStack item = pls.getInventory().getItem(i - 1);
                    if (item != null) {
                        if (item.getType().equals(Material.NAME_TAG)) {
                            if (item.getItemMeta() != null && item.getItemMeta().getDisplayName() != null) {
                                if (item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "Amuleto wither")) {
                                    have = true;
                                    target = pls;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            if (have == true) {
                final ItemStack amuleto = new ItemStack(Material.NAME_TAG);
                final ItemMeta metaAmulet = amuleto.getItemMeta();
                metaAmulet.setDisplayName(ChatColor.GOLD + "Amuleto wither");
                final ArrayList<String> lore = new ArrayList<>();
                lore.add(" ");
                lore.add(ChatColor.GRAY + "Este amuleto serve como proteção e também");
                lore.add(ChatColor.GRAY + "garante o sucesso ao spawnar um wither!");
                lore.add(" ");
                metaAmulet.setLore(lore);
                amuleto.setItemMeta(metaAmulet);

                RemoveItemInv.removeItems(target.getInventory(), amuleto, 1);
                for (final Player player : players) {
                    player.playSound(player.getLocation(), Sound.ENTITY_ITEM_BREAK, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                    player.sendMessage(ChatColor.GOLD + "\n * O Amuleto correu-se ao wither spawnar!\n ");
                }
            } else {
                for (final Player player : players) {
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    player.sendMessage(ChatColor.RED + " * Ops, para spawnar o wither é necessario conter um amuleto mágico.");
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void cancelDropReprod(final EntityDeathEvent event) {
        if (event.getEntity().isCustomNameVisible()) {
            if (event.getEntity().getCustomName().equalsIgnoreCase(ChatColor.GREEN + "Reprodutor")) {
                event.getDrops().clear();
            }
        }
    }

    @EventHandler
    public void onEntityKill(final EntityDeathEvent event) {

        if (event.getEntity().getCustomName() != null) {
            if (event.getEntity().getCustomName().contains("x")) {
                final String[] split = event.getEntity().getCustomName().split("x");
                final int quantia = Integer.valueOf(split[0].substring(2));

                if (quantia == 1) {
                    final EntityType[] entitys = {EntityType.COW, EntityType.SHEEP, EntityType.CHICKEN, EntityType.PIG};

                    final List<Entity> list = event.getEntity().getNearbyEntities(30, 30, 30);
                    if (Arrays.asList(entitys).contains(event.getEntity().getType())) {
                        for (final Entity entity : list) {
                            if (entity.getType().equals(event.getEntity().getType()) && entity.getCustomName() != null) {
                                if (entity.getCustomName().equalsIgnoreCase(ChatColor.GREEN + "Reprodutor")) {
                                    entity.remove();
                                }
                            }
                        }
                    }
                    return;
                }

                if (quantia > 1) {
                    int value = 0;
                    if (event.getEntity().hasMetadata("spawner")) {
                        value = event.getEntity().getMetadata("spawner").get(0).asInt();
                    } else if (event.getEntity().hasMetadata("natural")) {
                        value = event.getEntity().getMetadata("natural").get(0).asInt();
                    }
                    final int valor = value;

                    event.getEntity().remove();
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            final Entity entity = event.getEntity().getWorld().spawnEntity(event.getEntity().getLocation(), event.getEntityType());
                            if (event.getEntity().getKiller() != null && SkillKiller.get(event.getEntity().getKiller().getUniqueId().toString()) != null && SkillKiller.get(event.getEntity().getKiller().getUniqueId().toString()).getAble() == true) {
                                if (quantia - 10 >= 1) {
                                    MobsSpawnEvents.dropItens(event.getDrops(), event.getEntity().getLocation(), 10);
                                    entity.setCustomName(ChatColor.GREEN + String.valueOf(quantia - 10) + "x" + split[1]);

                                    final Entity entityXP = entity.getWorld().spawn(entity.getLocation(), ExperienceOrb.class);
                                    final ExperienceOrb xp = (ExperienceOrb) entityXP;
                                    xp.setExperience(event.getDroppedExp() * 8);
                                } else {
                                    MobsSpawnEvents.dropItens(event.getDrops(), event.getEntity().getLocation(), quantia);

                                    final Entity entityXP = entity.getWorld().spawn(entity.getLocation(), ExperienceOrb.class);
                                    final ExperienceOrb xp = (ExperienceOrb) entityXP;
                                    xp.setExperience(event.getDroppedExp() * (quantia - 1));

                                    entity.remove();
                                    return;
                                }
                            } else {
                                entity.setCustomName(ChatColor.GREEN + String.valueOf(quantia - 1) + "x" + split[1]);

                                final Entity entityXP = entity.getWorld().spawn(entity.getLocation(), ExperienceOrb.class);
                                final ExperienceOrb xp = (ExperienceOrb) entityXP;
                                xp.setExperience(event.getDroppedExp());
                            }
                            entity.setCustomNameVisible(true);
                            entity.setMetadata("killed-stacker", new FixedMetadataValue(Main.getInstance(), true));

                            if (event.getEntity().getKiller() != null && SkillKiller.get(event.getEntity().getKiller().getUniqueId().toString()) != null && SkillKiller.get(event.getEntity().getKiller().getUniqueId().toString()).getAble() == true) {
                                entity.setMetadata("spawner", new FixedMetadataValue(Main.getInstance(), valor - 10));
                            } else {
                                entity.setMetadata("spawner", new FixedMetadataValue(Main.getInstance(), valor - 1));
                            }
                        }
                    }, 3L);

                    //
                }
            }
        }
    }

    @EventHandler
    public void onSpawn(final EntitySpawnEvent event) {
        if (event.getEntityType().equals(EntityType.PHANTOM) && !event.getLocation().getWorld().getName().equalsIgnoreCase("aether")) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onInteractEntity(final PlayerInteractAtEntityEvent event) {
        final Player player = event.getPlayer();
        final EntityType[] entitys = {EntityType.COW, EntityType.SHEEP, EntityType.CHICKEN, EntityType.PIG};

        final List<Entity> list = event.getRightClicked().getNearbyEntities(20, 20, 20);
        if (Arrays.asList(entitys).contains(event.getRightClicked().getType()) && player.getItemInHand().getType().equals(Material.AIR)) {
            int quantia = 1;
            for (final Entity entity : list) {
                if (entity.getType().equals(event.getRightClicked().getType())) {
                    quantia++;
                }
            }

            if (quantia >= 2) {
                if (!event.isCancelled()) {
                    player.sendMessage(ChatColor.RED + " * Ops, você não pode dividir está entidade novamente!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    event.setCancelled(true);
                }
            } else if (quantia <= 1) {
                final Entity entity = event.getRightClicked().getWorld().spawnEntity(event.getRightClicked().getLocation(), event.getRightClicked().getType());
                entity.setCustomName(ChatColor.GREEN + "Reprodutor");
                entity.setCustomNameVisible(true);
                player.sendMessage(ChatColor.GREEN + " * Mob dividido!");
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void spawnerSpawnEntity(final CreatureSpawnEvent event) {
        if (event.getEntityType() == EntityType.DROPPED_ITEM)
            return;

        if (event.getSpawnReason().equals(SpawnReason.CUSTOM)) return;

        if (event.getEntity().hasMetadata("killed-stacker")) {
            event.getEntity().removeMetadata("killed-stacker", Main.getInstance());
            return;
        }

        final EntityType[] entitys = {EntityType.COW, EntityType.SHEEP, EntityType.CHICKEN, EntityType.PIG};
        if (Arrays.asList(entitys).contains(event.getEntityType())) {
            if (event.getSpawnReason().equals(SpawnReason.CUSTOM)) return;
            if (event.getEntity().getCustomName() != null) {
                if (event.getEntity().getCustomName().equalsIgnoreCase(ChatColor.GREEN + "Reprodutor")) {
                    return;
                }
            }
        }

        //if (event.getSpawnReason().equals(CreatureSpawnEvent.SpawnReason.BREEDING))

        final List<Entity> list = event.getEntity().getNearbyEntities(30, 30, 30);

        if (list.size() != 0) {
            for (int i = 1; i <= list.size(); i++) {
                if (list.get(i - 1).getType().equals(event.getEntityType())) {
                    event.getEntity().remove();
                    if (list.get(i - 1).getCustomName() != null) {
                        if (event.getEntityType().equals(EntityType.GHAST)) {
                            final String[] split = list.get(i - 1).getCustomName().split("x");

                            if (event.getLocation().getWorld().getName().equalsIgnoreCase("espaconave")) {
                                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                                if (MobsSpawnEvents.getQuantiaSpawners(location, "Ghast") != 0) {
                                    if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    final int quantiaMobspawners = MobsSpawnEvents.getQuantiaSpawners(location, "Ghast");
                                    final int quantia = Integer.valueOf(split[0].substring(2)) + 1 * quantiaMobspawners;
                                    list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Ghasts");
                                    list.get(i - 1).setCustomNameVisible(true);
                                    break;
                                } else {
                                    event.setCancelled(true);
                                }
                            } else {
                                if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                    event.setCancelled(true);
                                    return;
                                }

                                final int quantia = Integer.valueOf(split[0].substring(2)) + 1;
                                list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Ghasts");
                                list.get(i - 1).setCustomNameVisible(true);
                                break;
                            }
                        }
                        if (event.getEntityType().equals(EntityType.ZOMBIE_VILLAGER)) {
                            final String[] split = list.get(i - 1).getCustomName().split("x");

                            if (event.getLocation().getWorld().getName().equalsIgnoreCase("espaconave")) {
                                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                                if (MobsSpawnEvents.getQuantiaSpawners(location, "Aldeão zumbi") != 0) {
                                    if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    final int quantiaMobspawners = MobsSpawnEvents.getQuantiaSpawners(location, "Aldeão zumbi");
                                    final int quantia = Integer.valueOf(split[0].substring(2)) + 1 * quantiaMobspawners;
                                    list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Aldeões zumbi");
                                    list.get(i - 1).setCustomNameVisible(true);
                                    break;
                                } else {
                                    event.setCancelled(true);
                                }
                            } else {
                                if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                    event.setCancelled(true);
                                    return;
                                }

                                final int quantia = Integer.valueOf(split[0].substring(2)) + 1;
                                list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Aldeões zumbi");
                                list.get(i - 1).setCustomNameVisible(true);
                                break;
                            }
                        }
                        if (event.getEntityType().equals(EntityType.MAGMA_CUBE)) {
                            final String[] split = list.get(i - 1).getCustomName().split("x");

                            if (event.getLocation().getWorld().getName().equalsIgnoreCase("espaconave")) {
                                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                                if (MobsSpawnEvents.getQuantiaSpawners(location, "Cubo magma") != 0) {
                                    if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    final int quantiaMobspawners = MobsSpawnEvents.getQuantiaSpawners(location, "Cubo magma");
                                    final int quantia = Integer.valueOf(split[0].substring(2)) + 1 * quantiaMobspawners;
                                    list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Cubos magmas");
                                    list.get(i - 1).setCustomNameVisible(true);
                                    break;
                                } else {
                                    event.setCancelled(true);
                                }
                            } else {
                                if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                    event.setCancelled(true);
                                    return;
                                }

                                final int quantia = Integer.valueOf(split[0].substring(2)) + 1;
                                list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Cubos magmas");
                                list.get(i - 1).setCustomNameVisible(true);
                                break;
                            }
                        }
                        if (event.getEntityType().equals(EntityType.WITCH)) {
                            final String[] split = list.get(i - 1).getCustomName().split("x");

                            if (event.getLocation().getWorld().getName().equalsIgnoreCase("espaconave")) {
                                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                                if (MobsSpawnEvents.getQuantiaSpawners(location, "Bruxa") != 0) {
                                    if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    final int quantiaMobspawners = MobsSpawnEvents.getQuantiaSpawners(location, "Bruxa");
                                    final int quantia = Integer.valueOf(split[0].substring(2)) + 1 * quantiaMobspawners;
                                    list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Bruxas");
                                    list.get(i - 1).setCustomNameVisible(true);
                                    break;
                                } else {
                                    event.setCancelled(true);
                                }
                            } else {
                                if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                    event.setCancelled(true);
                                    return;
                                }

                                final int quantia = Integer.valueOf(split[0].substring(2)) + 1;
                                list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Bruxas");
                                list.get(i - 1).setCustomNameVisible(true);
                                break;
                            }
                        }
                        if (event.getEntityType().equals(EntityType.WITHER_SKELETON)) {
                            final String[] split = list.get(i - 1).getCustomName().split("x");

                            if (event.getLocation().getWorld().getName().equalsIgnoreCase("espaconave")) {
                                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                                if (MobsSpawnEvents.getQuantiaSpawners(location, "Esqueleto wither") != 0) {
                                    if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    final int quantiaMobspawners = MobsSpawnEvents.getQuantiaSpawners(location, "Esqueleto wither");
                                    final int quantia = Integer.valueOf(split[0].substring(2)) + 1 * quantiaMobspawners;
                                    list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Esqueletos wither");
                                    list.get(i - 1).setCustomNameVisible(true);
                                    break;
                                } else {
                                    event.setCancelled(true);
                                }
                            } else {
                                if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                    event.setCancelled(true);
                                    return;
                                }

                                final int quantia = Integer.valueOf(split[0].substring(2)) + 1;
                                list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Esqueletos wither");
                                list.get(i - 1).setCustomNameVisible(true);
                                break;
                            }
                        }
                        if (event.getEntityType().equals(EntityType.PIG_ZOMBIE)) {
                            final String[] split = list.get(i - 1).getCustomName().split("x");

                            if (event.getLocation().getWorld().getName().equalsIgnoreCase("espaconave")) {
                                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                                if (MobsSpawnEvents.getQuantiaSpawners(location, "Homem porco zumbi") != 0) {
                                    if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    final int quantiaMobspawners = MobsSpawnEvents.getQuantiaSpawners(location, "Homem porco zumbi");
                                    final int quantia = Integer.valueOf(split[0].substring(2)) + 1 * quantiaMobspawners;
                                    list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Homens porco zumbi");
                                    list.get(i - 1).setCustomNameVisible(true);
                                    break;
                                } else {
                                    event.setCancelled(true);
                                }
                            } else {
                                if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                    event.setCancelled(true);
                                    return;
                                }

                                final int quantia = Integer.valueOf(split[0].substring(2)) + 1;
                                list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Homens porco zumbi");
                                list.get(i - 1).setCustomNameVisible(true);
                                break;
                            }
                        }
                        if (event.getEntityType().equals(EntityType.BLAZE)) {
                            final String[] split = list.get(i - 1).getCustomName().split("x");

                            if (event.getLocation().getWorld().getName().equalsIgnoreCase("espaconave")) {
                                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                                if (MobsSpawnEvents.getQuantiaSpawners(location, "Blaze") != 0) {
                                    if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    final int quantiaMobspawners = MobsSpawnEvents.getQuantiaSpawners(location, "Blaze");
                                    final int quantia = Integer.valueOf(split[0].substring(2)) + 1 * quantiaMobspawners;
                                    list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Blazes");
                                    list.get(i - 1).setCustomNameVisible(true);
                                    break;
                                } else {
                                    event.setCancelled(true);
                                }
                            } else {
                                if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                    event.setCancelled(true);
                                    return;
                                }

                                final int quantia = Integer.valueOf(split[0].substring(2)) + 1;
                                list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Blazes");
                                list.get(i - 1).setCustomNameVisible(true);
                                break;
                            }
                        }
                        if (event.getEntityType().equals(EntityType.SLIME)) {

                            final String[] split = list.get(i - 1).getCustomName().split("x");

                            if (event.getLocation().getWorld().getName().equalsIgnoreCase("espaconave")) {
                                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                                if (MobsSpawnEvents.getQuantiaSpawners(location, "Slime") != 0) {
                                    if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    final int quantiaMobspawners = MobsSpawnEvents.getQuantiaSpawners(location, "Slime");
                                    final int quantia = Integer.valueOf(split[0].substring(2)) + 1 * quantiaMobspawners;
                                    list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Slimes");
                                    list.get(i - 1).setCustomNameVisible(true);
                                    break;
                                } else {
                                    event.setCancelled(true);
                                }
                            } else {
                                if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                    event.setCancelled(true);
                                    return;
                                }

                                final int quantia = Integer.valueOf(split[0].substring(2)) + 1;
                                list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Slimes");
                                list.get(i - 1).setCustomNameVisible(true);
                                break;
                            }
                        }
                        if (event.getEntityType().equals(EntityType.CAVE_SPIDER)) {
                            final String[] split = list.get(i - 1).getCustomName().split("x");

                            if (event.getLocation().getWorld().getName().equalsIgnoreCase("espaconave")) {
                                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                                if (MobsSpawnEvents.getQuantiaSpawners(location, "Aranha das cavernas") != 0) {
                                    if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    final int quantiaMobspawners = MobsSpawnEvents.getQuantiaSpawners(location, "Aranha das cavernas");
                                    final int quantia = Integer.valueOf(split[0].substring(2)) + 1 * quantiaMobspawners;
                                    list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Aranhas das cavernas");
                                    list.get(i - 1).setCustomNameVisible(true);
                                    break;
                                } else {
                                    event.setCancelled(true);
                                }
                            } else {
                                if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                    event.setCancelled(true);
                                    return;
                                }

                                final int quantia = Integer.valueOf(split[0].substring(2)) + 1;
                                list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Aranhas das cavernas");
                                list.get(i - 1).setCustomNameVisible(true);
                                break;
                            }
                        }
                        if (event.getEntityType().equals(EntityType.ENDERMAN)) {
                            final String[] split = list.get(i - 1).getCustomName().split("x");

                            if (event.getLocation().getWorld().getName().equalsIgnoreCase("espaconave")) {
                                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                                if (MobsSpawnEvents.getQuantiaSpawners(location, "Enderman") != 0) {
                                    if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    final int quantiaMobspawners = MobsSpawnEvents.getQuantiaSpawners(location, "Enderman");
                                    final int quantia = Integer.valueOf(split[0].substring(2)) + 1 * quantiaMobspawners;
                                    list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Endermans");
                                    list.get(i - 1).setCustomNameVisible(true);
                                    break;
                                } else {
                                    event.setCancelled(true);
                                }
                            } else {
                                if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                    event.setCancelled(true);
                                    return;
                                }

                                final int quantia = Integer.valueOf(split[0].substring(2)) + 1;
                                list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Endermans");
                                list.get(i - 1).setCustomNameVisible(true);
                                break;
                            }
                        }
                        if (event.getEntityType().equals(EntityType.CREEPER)) {
                            final String[] split = list.get(i - 1).getCustomName().split("x");

                            if (event.getLocation().getWorld().getName().equalsIgnoreCase("espaconave")) {
                                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                                if (MobsSpawnEvents.getQuantiaSpawners(location, "Creeper") != 0) {
                                    if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    final int quantiaMobspawners = MobsSpawnEvents.getQuantiaSpawners(location, "Creeper");
                                    final int quantia = Integer.valueOf(split[0].substring(2)) + 1 * quantiaMobspawners;
                                    list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Creepers");
                                    list.get(i - 1).setCustomNameVisible(true);
                                    break;
                                } else {
                                    event.setCancelled(true);
                                }
                            } else {
                                if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                    event.setCancelled(true);
                                    return;
                                }

                                final int quantia = Integer.valueOf(split[0].substring(2)) + 1;
                                list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Creepers");
                                list.get(i - 1).setCustomNameVisible(true);
                                break;
                            }
                        }
                        if (event.getEntityType().equals(EntityType.SPIDER)) {
                            final String[] split = list.get(i - 1).getCustomName().split("x");

                            if (event.getLocation().getWorld().getName().equalsIgnoreCase("espaconave")) {
                                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                                if (MobsSpawnEvents.getQuantiaSpawners(location, "Aranha") != 0) {
                                    if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    final int quantiaMobspawners = MobsSpawnEvents.getQuantiaSpawners(location, "Aranha");
                                    final int quantia = Integer.valueOf(split[0].substring(2)) + 1 * quantiaMobspawners;
                                    list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Aranhas");
                                    list.get(i - 1).setCustomNameVisible(true);
                                    break;
                                } else {
                                    event.setCancelled(true);
                                }
                            } else {
                                if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                    event.setCancelled(true);
                                    return;
                                }

                                final int quantia = Integer.valueOf(split[0].substring(2)) + 1;
                                list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Aranhas");
                                list.get(i - 1).setCustomNameVisible(true);
                                break;
                            }
                        }
                        if (event.getEntityType().equals(EntityType.SKELETON)) {
                            final String[] split = list.get(i - 1).getCustomName().split("x");

                            if (event.getLocation().getWorld().getName().equalsIgnoreCase("espaconave")) {
                                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                                if (MobsSpawnEvents.getQuantiaSpawners(location, "Esqueleto") != 0) {
                                    if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    final int quantiaMobspawners = MobsSpawnEvents.getQuantiaSpawners(location, "Esqueleto");
                                    final int quantia = Integer.valueOf(split[0].substring(2)) + 1 * quantiaMobspawners;
                                    list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Esqueletos");
                                    list.get(i - 1).setCustomNameVisible(true);
                                    break;
                                } else {
                                    event.setCancelled(true);
                                }
                            } else {
                                if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                    event.setCancelled(true);
                                    return;
                                }

                                final int quantia = Integer.valueOf(split[0].substring(2)) + 1;
                                list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Esqueletos");
                                list.get(i - 1).setCustomNameVisible(true);
                                break;
                            }
                        }
                        if (event.getEntityType().equals(EntityType.ZOMBIE)) {
                            final String[] split = list.get(i - 1).getCustomName().split("x");

                            if (event.getLocation().getWorld().getName().equalsIgnoreCase("espaconave")) {
                                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                                if (MobsSpawnEvents.getQuantiaSpawners(location, "Zumbi") != 0) {
                                    if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    final int quantiaMobspawners = MobsSpawnEvents.getQuantiaSpawners(location, "Zumbi");
                                    final int quantia = Integer.valueOf(split[0].substring(2)) + quantiaMobspawners;
                                    list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Zumbis");
                                    list.get(i - 1).setCustomNameVisible(true);
                                    break;
                                } else {
                                    event.setCancelled(true);
                                }
                            } else {
                                if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                    event.setCancelled(true);
                                    return;
                                }

                                final int quantia = Integer.valueOf(split[0].substring(2)) + 1;
                                list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Zumbis");
                                list.get(i - 1).setCustomNameVisible(true);
                                break;
                            }
                        }
                        if (event.getEntityType().equals(EntityType.RABBIT)) {
                            final String[] split = list.get(i - 1).getCustomName().split("x");

                            if (event.getLocation().getWorld().getName().equalsIgnoreCase("espaconave")) {
                                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                                if (MobsSpawnEvents.getQuantiaSpawners(location, "Coelho") != 0) {
                                    if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    final int quantiaMobspawners = MobsSpawnEvents.getQuantiaSpawners(location, "Coelho");
                                    final int quantia = Integer.valueOf(split[0].substring(2)) + 1 * quantiaMobspawners;
                                    list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Coelhos");
                                    list.get(i - 1).setCustomNameVisible(true);
                                    break;
                                } else {
                                    event.setCancelled(true);
                                }
                            } else {
                                if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                    event.setCancelled(true);
                                    return;
                                }

                                final int quantia = Integer.valueOf(split[0].substring(2)) + 1;
                                list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Coelhos");
                                list.get(i - 1).setCustomNameVisible(true);
                                break;
                            }
                        }
                        if (event.getEntityType().equals(EntityType.SHEEP)) {
                            final String[] split = list.get(i - 1).getCustomName().split("x");

                            if (event.getLocation().getWorld().getName().equalsIgnoreCase("espaconave")) {
                                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                                if (MobsSpawnEvents.getQuantiaSpawners(location, "Ovelha") != 0) {
                                    if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    final int quantiaMobspawners = MobsSpawnEvents.getQuantiaSpawners(location, "Ovelha");
                                    final int quantia = Integer.valueOf(split[0].substring(2)) + 1 * quantiaMobspawners;
                                    list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Ovelhas");
                                    list.get(i - 1).setCustomNameVisible(true);
                                    break;
                                } else {
                                    event.setCancelled(true);
                                }
                            } else {
                                if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                    event.setCancelled(true);
                                    return;
                                }

                                final int quantia = Integer.valueOf(split[0].substring(2)) + 1;
                                list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Ovelhas");
                                list.get(i - 1).setCustomNameVisible(true);
                                break;
                            }
                        }
                        if (event.getEntityType().equals(EntityType.PIG)) {
                            final String[] split = list.get(i - 1).getCustomName().split("x");

                            if (event.getLocation().getWorld().getName().equalsIgnoreCase("espaconave")) {
                                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                                if (MobsSpawnEvents.getQuantiaSpawners(location, "Porco") != 0) {
                                    if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    final int quantiaMobspawners = MobsSpawnEvents.getQuantiaSpawners(location, "Porco");
                                    final int quantia = Integer.valueOf(split[0].substring(2)) + 1 * quantiaMobspawners;
                                    list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Porcos");
                                    list.get(i - 1).setCustomNameVisible(true);
                                    break;
                                } else {
                                    event.setCancelled(true);
                                }
                            } else {
                                if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                    event.setCancelled(true);
                                    return;
                                }

                                final int quantia = Integer.valueOf(split[0].substring(2)) + 1;
                                list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Porcos");
                                list.get(i - 1).setCustomNameVisible(true);
                                break;
                            }
                        }
                        if (event.getEntityType().equals(EntityType.CHICKEN)) {
                            final String[] split = list.get(i - 1).getCustomName().split("x");

                            if (event.getLocation().getWorld().getName().equalsIgnoreCase("espaconave")) {
                                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                                if (MobsSpawnEvents.getQuantiaSpawners(location, "Galinha") != 0) {
                                    if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    final int quantiaMobspawners = MobsSpawnEvents.getQuantiaSpawners(location, "Galinha");
                                    final int quantia = Integer.valueOf(split[0].substring(2)) + 1 * quantiaMobspawners;
                                    list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Galinhas");
                                    list.get(i - 1).setCustomNameVisible(true);
                                    break;
                                } else {
                                    event.setCancelled(true);
                                }
                            } else {
                                if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                    event.setCancelled(true);
                                    return;
                                }

                                final int quantia = Integer.valueOf(split[0].substring(2)) + 1;
                                list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Galinhas");
                                list.get(i - 1).setCustomNameVisible(true);
                                break;
                            }
                        }
                        if (event.getEntityType().equals(EntityType.COW)) {
                            final String[] split = list.get(i - 1).getCustomName().split("x");

                            if (event.getLocation().getWorld().getName().equalsIgnoreCase("espaconave")) {
                                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                                if (MobsSpawnEvents.getQuantiaSpawners(location, "Vaca") != 0) {
                                    if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                        event.setCancelled(true);
                                        return;
                                    }
                                    final int quantiaMobspawners = MobsSpawnEvents.getQuantiaSpawners(location, "Vaca");
                                    final int quantia = Integer.valueOf(split[0].substring(2)) + 1 * quantiaMobspawners;
                                    list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Vacas");
                                    list.get(i - 1).setCustomNameVisible(true);
                                    break;
                                } else {
                                    event.setCancelled(true);
                                }
                            } else {
                                if (MobsSpawnEvents.updateMetadata(list, i, event.getSpawnReason()) == true) {
                                    event.setCancelled(true);
                                    return;
                                }

                                final int quantia = Integer.valueOf(split[0].substring(2)) + 1;
                                list.get(i - 1).setCustomName(ChatColor.GREEN + String.valueOf(quantia) + "x Vacas");
                                list.get(i - 1).setCustomNameVisible(true);
                                break;
                            }
                        }
                    }
                }
                if (i == list.size()) {
                    if (event.getEntityType().equals(EntityType.GHAST)) {
                        event.getEntity().setCustomName(ChatColor.GREEN + "1x Ghast");
                        event.getEntity().setCustomNameVisible(true);
                    }
                    if (event.getEntityType().equals(EntityType.ZOMBIE_VILLAGER)) {
                        event.getEntity().setCustomName(ChatColor.GREEN + "1x Aldeão zumbi");
                        event.getEntity().setCustomNameVisible(true);
                    }
                    if (event.getEntityType().equals(EntityType.MAGMA_CUBE)) {
                        event.getEntity().setCustomName(ChatColor.GREEN + "1x Cubo magma");
                        event.getEntity().setCustomNameVisible(true);
                    }
                    if (event.getEntityType().equals(EntityType.WITCH)) {
                        event.getEntity().setCustomName(ChatColor.GREEN + "1x Bruxa");
                        event.getEntity().setCustomNameVisible(true);
                    }
                    if (event.getEntityType().equals(EntityType.WITHER_SKELETON)) {
                        event.getEntity().setCustomName(ChatColor.GREEN + "1x Esqueleto wither");
                        event.getEntity().setCustomNameVisible(true);
                    }
                    if (event.getEntityType().equals(EntityType.PIG_ZOMBIE)) {
                        event.getEntity().setCustomName(ChatColor.GREEN + "1x Homem porco zumbi");
                        event.getEntity().setCustomNameVisible(true);
                    }
                    if (event.getEntityType().equals(EntityType.BLAZE)) {
                        event.getEntity().setCustomName(ChatColor.GREEN + "1x Blaze");
                        event.getEntity().setCustomNameVisible(true);
                    }
                    if (event.getEntityType().equals(EntityType.SLIME)) {
                        final Slime slime = (Slime) event.getEntity();
                        slime.setSize(1);
                        event.getEntity().setCustomName(ChatColor.GREEN + "1x Slime");
                        event.getEntity().setCustomNameVisible(true);
                    }
                    if (event.getEntityType().equals(EntityType.CAVE_SPIDER)) {
                        event.getEntity().setCustomName(ChatColor.GREEN + "1x Aranha das cavernas");
                        event.getEntity().setCustomNameVisible(true);
                    }
                    if (event.getEntityType().equals(EntityType.ENDERMAN)) {
                        event.getEntity().setCustomName(ChatColor.GREEN + "1x Enderman");
                        event.getEntity().setCustomNameVisible(true);
                    }
                    if (event.getEntityType().equals(EntityType.CREEPER)) {
                        event.getEntity().setCustomName(ChatColor.GREEN + "1x Creeper");
                        event.getEntity().setCustomNameVisible(true);
                    }
                    if (event.getEntityType().equals(EntityType.SPIDER)) {
                        event.getEntity().setCustomName(ChatColor.GREEN + "1x Aranha");
                        event.getEntity().setCustomNameVisible(true);
                    }
                    if (event.getEntityType().equals(EntityType.SKELETON)) {
                        event.getEntity().setCustomName(ChatColor.GREEN + "1x Esqueleto");
                        event.getEntity().setCustomNameVisible(true);
                    }
                    if (event.getEntityType().equals(EntityType.ZOMBIE)) {
                        event.getEntity().setCustomName(ChatColor.GREEN + "1x Zumbi");
                        event.getEntity().setCustomNameVisible(true);
                    }
                    if (event.getEntityType().equals(EntityType.RABBIT)) {
                        event.getEntity().setCustomName(ChatColor.GREEN + "1x Coelho");
                        event.getEntity().setCustomNameVisible(true);
                    }
                    if (event.getEntityType().equals(EntityType.SHEEP)) {
                        event.getEntity().setCustomName(ChatColor.GREEN + "1x Ovelha");
                        event.getEntity().setCustomNameVisible(true);
                    }
                    if (event.getEntityType().equals(EntityType.PIG)) {
                        event.getEntity().setCustomName(ChatColor.GREEN + "1x Porco");
                        event.getEntity().setCustomNameVisible(true);
                    }
                    if (event.getEntityType().equals(EntityType.CHICKEN)) {
                        event.getEntity().setCustomName(ChatColor.GREEN + "1x Galinha");
                        event.getEntity().setCustomNameVisible(true);
                    }
                    if (event.getEntityType().equals(EntityType.COW)) {
                        event.getEntity().setCustomName(ChatColor.GREEN + "1x Vaca");
                        event.getEntity().setCustomNameVisible(true);
                    }
                }
            }
        } else {
            if (event.getEntityType().equals(EntityType.GHAST)) {
                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                    event.setCancelled(true);
                    return;
                }
                event.getEntity().setCustomName(ChatColor.GREEN + "1x Ghast");
                event.getEntity().setCustomNameVisible(true);
            }
            if (event.getEntityType().equals(EntityType.ZOMBIE_VILLAGER)) {
                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                    event.setCancelled(true);
                    return;
                }
                event.getEntity().setCustomName(ChatColor.GREEN + "1x Aldeão zumbi");
                event.getEntity().setCustomNameVisible(true);
            }
            if (event.getEntityType().equals(EntityType.MAGMA_CUBE)) {
                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                    event.setCancelled(true);
                    return;
                }
                event.getEntity().setCustomName(ChatColor.GREEN + "1x Cubo magma");
                event.getEntity().setCustomNameVisible(true);
            }
            if (event.getEntityType().equals(EntityType.WITCH)) {
                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                    event.setCancelled(true);
                    return;
                }
                event.getEntity().setCustomName(ChatColor.GREEN + "1x Bruxa");
                event.getEntity().setCustomNameVisible(true);
            }
            if (event.getEntityType().equals(EntityType.WITHER_SKELETON)) {
                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                    event.setCancelled(true);
                    return;
                }
                event.getEntity().setCustomName(ChatColor.GREEN + "1x Esqueleto wither");
                event.getEntity().setCustomNameVisible(true);
            }
            if (event.getEntityType().equals(EntityType.PIG_ZOMBIE)) {
                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                    event.setCancelled(true);
                    return;
                }
                event.getEntity().setCustomName(ChatColor.GREEN + "1x Homem porco zumbi");
                event.getEntity().setCustomNameVisible(true);
            }
            if (event.getEntityType().equals(EntityType.BLAZE)) {
                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                    event.setCancelled(true);
                    return;
                }
                event.getEntity().setCustomName(ChatColor.GREEN + "1x Blaze");
                event.getEntity().setCustomNameVisible(true);
            }
            if (event.getEntityType().equals(EntityType.SLIME)) {
                final Slime slime = (Slime) event.getEntity();
                slime.setSize(1);
                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                    event.setCancelled(true);
                    return;
                }
                event.getEntity().setCustomName(ChatColor.GREEN + "1x Slime");
                event.getEntity().setCustomNameVisible(true);
            }
            if (event.getEntityType().equals(EntityType.CAVE_SPIDER)) {
                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                    event.setCancelled(true);
                    return;
                }
                event.getEntity().setCustomName(ChatColor.GREEN + "1x Aranha das cavernas");
                event.getEntity().setCustomNameVisible(true);
            }
            if (event.getEntityType().equals(EntityType.ENDERMAN)) {
                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                    event.setCancelled(true);
                    return;
                }
                event.getEntity().setCustomName(ChatColor.GREEN + "1x Enderman");
                event.getEntity().setCustomNameVisible(true);
            }
            if (event.getEntityType().equals(EntityType.CREEPER)) {
                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                    event.setCancelled(true);
                    return;
                }
                event.getEntity().setCustomName(ChatColor.GREEN + "1x Creeper");
                event.getEntity().setCustomNameVisible(true);
            }
            if (event.getEntityType().equals(EntityType.SPIDER)) {
                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                    event.setCancelled(true);
                    return;
                }
                event.getEntity().setCustomName(ChatColor.GREEN + "1x Aranha");
                event.getEntity().setCustomNameVisible(true);
            }
            if (event.getEntityType().equals(EntityType.SKELETON)) {
                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                    event.setCancelled(true);
                    return;
                }
                event.getEntity().setCustomName(ChatColor.GREEN + "1x Esqueleto");
                event.getEntity().setCustomNameVisible(true);
            }
            if (event.getEntityType().equals(EntityType.ZOMBIE)) {
                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                    event.setCancelled(true);
                    return;
                }
                event.getEntity().setCustomName(ChatColor.GREEN + "1x Zumbi");
                event.getEntity().setCustomNameVisible(true);
            }
            if (event.getEntityType().equals(EntityType.RABBIT)) {
                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                    event.setCancelled(true);
                    return;
                }
                event.getEntity().setCustomName(ChatColor.GREEN + "1x Coelho");
                event.getEntity().setCustomNameVisible(true);
            }
            if (event.getEntityType().equals(EntityType.SHEEP)) {
                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                    event.setCancelled(true);
                    return;
                }
                event.getEntity().setCustomName(ChatColor.GREEN + "1x Ovelha");
                event.getEntity().setCustomNameVisible(true);
            }
            if (event.getEntityType().equals(EntityType.PIG)) {
                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                    event.setCancelled(true);
                    return;
                }
                event.getEntity().setCustomName(ChatColor.GREEN + "1x Porco");
                event.getEntity().setCustomNameVisible(true);
            }
            if (event.getEntityType().equals(EntityType.CHICKEN)) {
                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                    event.setCancelled(true);
                    return;
                }
                event.getEntity().setCustomName(ChatColor.GREEN + "1x Galinha");
                event.getEntity().setCustomNameVisible(true);
            }
            if (event.getEntityType().equals(EntityType.COW)) {
                final String location = event.getLocation().getBlockX() + ":" + event.getLocation().getBlockY() + ":" + event.getLocation().getBlockZ();
                if (MobspawnAPI.getSpawnAtivoFromLoc(location) == false) {
                    event.setCancelled(true);
                    return;
                }
                event.getEntity().setCustomName(ChatColor.GREEN + "1x Vaca");
                event.getEntity().setCustomNameVisible(true);
            }
        }
    }
}
