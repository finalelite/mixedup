package com.mixedup.listeners;

import com.mixedup.Main;
import com.mixedup.apis.LastPositionAPI;
import com.mixedup.apis.PoderAPI;
import com.mixedup.apis.WarpAPI;
import com.mixedup.chat.TellCommand;
import com.mixedup.commands.CommandVip;
import com.mixedup.economy.CoinsAPI;
import com.mixedup.hashs.PlayerVanishHash;
import com.mixedup.mcmmo.McMMOApi;
import com.mixedup.plots.CasaAPI;
import com.mixedup.utils.UUIDcache;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class PlayerLeftEvents implements Listener {

    @EventHandler
    public void onLeft(final PlayerQuitEvent event) {
        final Player player = event.getPlayer();

        if (PlayerVanishHash.isVanished(player)) {
            PlayerVanishHash.toggleVanish(player);
        }
        TellCommand.remove(player);
        WarpAPI.removeCache(player.getUniqueId().toString());
        CasaAPI.getCache().remove(player.getUniqueId().toString());
        UUIDcache.CACHE.remove(player.getUniqueId().toString());
        PoderAPI.poderCache.remove(player.getUniqueId().toString());
        McMMOApi.getCache().setXPInSQL(player.getUniqueId().toString(), true);
        CommandVip.getCache().remove(player.getUniqueId().toString());
        CoinsAPI.economyManager.removeFromCache(player);
        if (LastPositionAPI.get(player.getUniqueId().toString()) != null)
            LastPositionAPI.CACHE.remove(player.getUniqueId().toString());

        new BukkitRunnable() {
            @Override
            public void run() {
                final val sb = Main.scoreboardManager;
                Bukkit.getOnlinePlayers().forEach(target -> {
                    if (sb.hasSquadScoreboard(target)) {
                        sb.getSquadScoreboard().updateEntry(target, "squad_online");
                    } else {
                        sb.getDefaultScoreboard().updateEntry(target, "online");
                    }
                });
            }
        }.runTaskLater(Main.getInstance(), 25);
    }
}
