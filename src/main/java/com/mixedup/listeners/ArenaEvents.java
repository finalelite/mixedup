package com.mixedup.listeners;

import com.mixedup.apis.ConfigAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import com.mixedup.hashs.PlayerInArenaHash;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class ArenaEvents implements Listener {

    public static boolean playerInArea(final Location loc) {
        final String[] pos1 = "spawn:140:50:106".split(":");
        final String[] pos2 = "spawn:78:31:44".split(":");

        if (!loc.getWorld().getName().equalsIgnoreCase(pos1[0])) return false;

        final int p1x = Integer.valueOf(pos1[1]);
        final int p1y = Integer.valueOf(pos1[2]);
        final int p1z = Integer.valueOf(pos1[3]);
        final int p2x = Integer.valueOf(pos2[1]);
        final int p2y = Integer.valueOf(pos2[2]);
        final int p2z = Integer.valueOf(pos2[3]);

        final int minX = p1x < p2x ? p1x : p2x;
        final int minY = p1y < p2y ? p1y : p2y;
        final int minZ = p1z < p2z ? p1z : p2z;

        final int maxX = p1x > p2x ? p1x : p2x;
        final int maxY = p1y > p2y ? p1y : p2y;
        final int maxZ = p1z > p2z ? p1z : p2z;

        return (loc.getBlockX() >= minX) && (loc.getBlockX() <= maxX) && (loc.getBlockY() >= minY)
                && (loc.getBlockY() <= maxY) && (loc.getBlockZ() >= minZ) && (loc.getBlockZ() <= maxZ);
    }

    @EventHandler
    public void onMove(final PlayerMoveEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (playerInArea(player.getLocation()) == true || playerInArenaVip(player.getLocation()) == true) {
            if (PlayerInArenaHash.get(UUID) == null) {
                new PlayerInArenaHash(UUID, true).insert();
                player.sendTitle(ChatColor.RED + "Zona de PVP", ChatColor.RED + "(!)", 20, 60, 20);
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                if (player.getAllowFlight() == true) {
                    final String tag = TagAPI.getTag(player.getUniqueId().toString());
                    if (tag != null && !(tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Moderador") || tag.equalsIgnoreCase("Admin"))) {
                        player.setAllowFlight(false);
                        ConfigAPI.updateFly(UUID, false);
                    }
                }

                if (player.hasPotionEffect(PotionEffectType.SLOW_FALLING)) {
                    player.removePotionEffect(PotionEffectType.SLOW_FALLING);
                }
            } else if (PlayerInArenaHash.get(UUID).getStatus() == false) {
                PlayerInArenaHash.get(UUID).setStatus(true);
                player.sendTitle(ChatColor.RED + "Zona de PVP", ChatColor.RED + "(!)", 20, 60, 20);
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                if (player.getAllowFlight() == true) {
                    final String tag = TagAPI.getTag(player.getUniqueId().toString());
                    if (tag != null && !(tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Moderador") || tag.equalsIgnoreCase("Admin"))) {
                        player.setAllowFlight(false);
                        ConfigAPI.updateFly(UUID, false);
                    }
                }
                if (player.hasPotionEffect(PotionEffectType.SLOW_FALLING)) {
                    player.removePotionEffect(PotionEffectType.SLOW_FALLING);
                }
            }
        } else {
            if (PlayerInArenaHash.get(UUID) != null && PlayerInArenaHash.get(UUID).getStatus() == true) {
                PlayerInArenaHash.get(UUID).setStatus(false);
                player.sendTitle(ChatColor.GREEN + "Zona segura", ChatColor.GREEN + "(!)", 20, 60, 20);
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                if (!player.hasPotionEffect(PotionEffectType.SLOW_FALLING)) {
                    final PotionEffect effectS = new PotionEffect(PotionEffectType.SLOW_FALLING, 999999, 1, true, false);
                    player.addPotionEffect(effectS);
                }
            }
        }
    }

    @EventHandler
    public void teleportOutArena(final PlayerTeleportEvent event) {
        final Location from = event.getFrom();
        if (playerInArea(from) && (event.getCause() == TeleportCause.ENDER_PEARL)) {
            event.getPlayer().sendMessage(ChatColor.RED + "Você não pode teleportar da arena.");
            event.getPlayer().getInventory().addItem(new ItemStack(Material.ENDER_PEARL));
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onDamage(final EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player) {
            if (playerInArea(event.getEntity().getLocation()) == true) {
                if (event.getDamager().getType().equals(EntityType.ARROW) || event.getDamager().getType().equals(EntityType.SPECTRAL_ARROW) || event.getDamager().getType().equals(EntityType.TIPPED_ARROW)) {
                    final Arrow arrow = (Arrow) event.getDamager();
                    final Entity shooter = (Entity) arrow.getShooter();

                    if (shooter instanceof Player) {
                        if (playerInArea(shooter.getLocation()) == false) {
                            event.setCancelled(true);
                        }
                    }
                }
            }
        }
        if (event.getEntity() instanceof Player && event.getDamager() instanceof Player && event.getEntity().getWorld().getName().equalsIgnoreCase("spawn")) {
            if (playerInArea(event.getEntity().getLocation()) == false) {
                event.setCancelled(true);
            }
        }
    }

    public static boolean playerInArenaVip(final Location loc) {
        final String[] pos1 = "spawn:228:0:88".split(":");
        final String[] pos2 = "spawn:387:151:248".split(":");

        if (!loc.getWorld().getName().equalsIgnoreCase(pos1[0])) return false;

        final int p1x = Integer.valueOf(pos1[1]);
        final int p1y = Integer.valueOf(pos1[2]);
        final int p1z = Integer.valueOf(pos1[3]);
        final int p2x = Integer.valueOf(pos2[1]);
        final int p2y = Integer.valueOf(pos2[2]);
        final int p2z = Integer.valueOf(pos2[3]);

        final int minX = p1x < p2x ? p1x : p2x;
        final int minY = p1y < p2y ? p1y : p2y;
        final int minZ = p1z < p2z ? p1z : p2z;

        final int maxX = p1x > p2x ? p1x : p2x;
        final int maxY = p1y > p2y ? p1y : p2y;
        final int maxZ = p1z > p2z ? p1z : p2z;

        return (loc.getBlockX() >= minX) && (loc.getBlockX() <= maxX) && (loc.getBlockY() >= minY)
                && (loc.getBlockY() <= maxY) && (loc.getBlockZ() >= minZ) && (loc.getBlockZ() <= maxZ);
    }
}
