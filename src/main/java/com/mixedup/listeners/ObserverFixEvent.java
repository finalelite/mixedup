package com.mixedup.listeners;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.ItemStack;

public class ObserverFixEvent implements Listener {

    @EventHandler
    public void onUpdate(final BlockPhysicsEvent event) {
        if (event.getSourceBlock().getType().equals(Material.OBSERVER)) {
            event.getSourceBlock().setType(Material.AIR);
        }
    }

    @EventHandler
    public void onPlace(final BlockPlaceEvent event) {
        if (event.getBlockPlaced().getType().equals(Material.OBSERVER)) {
            event.getPlayer().sendMessage(ChatColor.RED + " * Ops, este bloco está bloqueado para qualquer uso!");
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onCraft(final PrepareItemCraftEvent event) {
        if (event.getRecipe() != null) {
            if (event.getRecipe().getResult().getType().equals(Material.OBSERVER)) {
                event.getInventory().setResult(new ItemStack(Material.AIR));

                for (final HumanEntity entitys : event.getViewers()) {
                    if (entitys instanceof Player) {
                        entitys.sendMessage(ChatColor.RED + " * Ops, este crafting está bloqueado!");
                        ((Player) entitys).playSound(entitys.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
            }
        }
    }
}
