package com.mixedup.utils;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;

public class Hologram {

    private static final double distance = 0.25D;

    public static ArmorStand createHologramReturnStandNoNegative(Location location, String text) {
        final ArmorStand stand = (ArmorStand) location.getWorld().spawnEntity(location, EntityType.ARMOR_STAND);
        stand.setArms(false);
        stand.setGravity(false);
        stand.setVisible(false);
        stand.setCustomName(text);
        stand.setCustomNameVisible(true);

        return stand;
    }

    public static ArmorStand createHologramReturnStand(Location location, final String[] text) {
        ArmorStand armorStand = null;
        Material block1 = null;
        Material block2 = null;
        Location loc1 = null;
        Location loc2 = null;

        location = location.add(0, -0.2, 0);
        if (!location.clone().add(0, +2, 0).getBlock().getType().equals(Material.AIR) || !location.clone().add(0, +1, 0).getBlock().getType().equals(Material.AIR)) {
            block1 = location.clone().add(0, +1, 0).getBlock().getType();
            loc1 = location.clone().add(0, +1, 0);
            block2 = location.clone().add(0, +2, 0).getBlock().getType();
            loc2 = location.clone().add(0, +2, 0);
            location.clone().add(0, +1, 0).getBlock().setType(Material.AIR);
            location.clone().add(0, +2, 0).getBlock().setType(Material.AIR);

            for (final String texts : text) {
                final ArmorStand stand = (ArmorStand) location.getWorld().spawnEntity(location, EntityType.ARMOR_STAND);
                stand.setArms(false);
                stand.setGravity(false);
                stand.setVisible(false);
                stand.setCustomName(texts);
                stand.setCustomNameVisible(true);
                location.subtract(0, Hologram.distance, 0);
                armorStand = stand;
            }

            if (block1 != null) {
                loc1.getBlock().setType(block1);
            }
            if (block2 != null) {
                loc2.getBlock().setType(block2);
            }
            block1 = null;
            block2 = null;
            loc1 = null;
            loc2 = null;
        } else {
            for (final String texts : text) {
                final ArmorStand stand = (ArmorStand) location.getWorld().spawnEntity(location, EntityType.ARMOR_STAND);
                stand.setArms(false);
                stand.setGravity(false);
                stand.setVisible(false);
                stand.setCustomName(texts);
                stand.setCustomNameVisible(true);
                location.subtract(0, Hologram.distance, 0);
                armorStand = stand;
            }
        }

        return armorStand;
    }

    public static void createHologram(Location location, final String[] text) {
        Material block1 = null;
        Material block2 = null;
        Location loc1 = null;
        Location loc2 = null;

        location = location.add(0, -0.2, 0);
        if (!location.clone().add(0, +2, 0).getBlock().getType().equals(Material.AIR) || !location.clone().add(0, +1, 0).getBlock().getType().equals(Material.AIR)) {
            block1 = location.clone().add(0, +1, 0).getBlock().getType();
            loc1 = location.clone().add(0, +1, 0);
            block2 = location.clone().add(0, +2, 0).getBlock().getType();
            loc2 = location.clone().add(0, +2, 0);
            location.clone().add(0, +1, 0).getBlock().setType(Material.AIR);
            location.clone().add(0, +2, 0).getBlock().setType(Material.AIR);

            for (final String texts : text) {
                final ArmorStand stand = (ArmorStand) location.getWorld().spawnEntity(location, EntityType.ARMOR_STAND);
                stand.setArms(false);
                stand.setGravity(false);
                stand.setVisible(false);
                stand.setCustomName(texts);
                stand.setCustomNameVisible(true);
                location.subtract(0, Hologram.distance, 0);
            }

            if (block1 != null) {
                loc1.getBlock().setType(block1);
            }
            if (block2 != null) {
                loc2.getBlock().setType(block2);
            }
            block1 = null;
            block2 = null;
            loc1 = null;
            loc2 = null;
        } else {
            for (final String texts : text) {
                final ArmorStand stand = (ArmorStand) location.getWorld().spawnEntity(location, EntityType.ARMOR_STAND);
                stand.setArms(false);
                stand.setGravity(false);
                stand.setVisible(false);
                stand.setCustomName(texts);
                stand.setCustomNameVisible(true);
                location.subtract(0, Hologram.distance, 0);
            }
        }
    }
}
