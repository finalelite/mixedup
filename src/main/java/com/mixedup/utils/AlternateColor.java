package com.mixedup.utils;

import org.bukkit.ChatColor;

public class AlternateColor {

    public static String alternate(String text) {
        text = text.replace("&k", "").replace("&K", "").replace("&l", "").replace("&L", "").replace("&n", "").replace("&N", "").replace("&m", "").replace("&M", "");
        return ChatColor.translateAlternateColorCodes('&', text);
    }

    public static String alternate2(final String text) {
        final String newtext = text.replaceAll("&", "§");

        return newtext;
    }
}
