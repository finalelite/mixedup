package com.mixedup.utils;

import com.sk89q.worldedit.*;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.extent.clipboard.BlockArrayClipboard;
import com.sk89q.worldedit.extent.clipboard.io.*;
import com.sk89q.worldedit.function.operation.ForwardExtentCopy;
import com.sk89q.worldedit.function.operation.Operation;
import com.sk89q.worldedit.function.operation.Operations;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldedit.regions.Region;
import com.sk89q.worldedit.session.ClipboardHolder;
import com.sk89q.worldedit.world.block.BlockTypes;
import org.bukkit.Location;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class WorldEditUtil {

    public static void paste(final BlockArrayClipboard clipboard, final boolean ignoreAir, final Location locPaste) throws WorldEditException {
        final com.sk89q.worldedit.world.World weWorld = new BukkitWorld(locPaste.getWorld());
        final EditSession editSession = WorldEdit.getInstance().getEditSessionFactory().getEditSession(weWorld, -1);
        editSession.enableQueue();

        final Vector vector = new Vector(locPaste.getBlockX(), locPaste.getBlockY(), locPaste.getBlockZ());
        final Operation operation = new ClipboardHolder(clipboard).createPaste(editSession).to(vector).ignoreAirBlocks(ignoreAir).build();

        Operations.complete(operation);
        editSession.flushSession();
    }

    public static BlockArrayClipboard load(final String namespace) throws IOException {
        final File file = new File(namespace + WorldEditUtil.getFileSuffix());

        if (!file.exists()) {
            return null;
        }

        BlockArrayClipboard copy = null;

        final ClipboardFormat format = ClipboardFormats.findByFile(file);
        final ClipboardReader reader = format.getReader(new FileInputStream(file));
        copy = (BlockArrayClipboard) reader.read();


        return copy;
    }

    public static void save(final String namespace, final BlockArrayClipboard clipboard) throws IOException {
        final File file = new File(namespace + WorldEditUtil.getFileSuffix());
        try (final ClipboardWriter writer = BuiltInClipboardFormat.SPONGE_SCHEMATIC.getWriter(new FileOutputStream(file))) {
            writer.write(clipboard);
        }
    }

    public static BlockArrayClipboard copy(final Region region) throws WorldEditException {
        final BlockArrayClipboard copy = new BlockArrayClipboard(region);

        final EditSession editSession = WorldEdit.getInstance().getEditSessionFactory().getEditSession(region.getWorld(), -1);

        final ForwardExtentCopy forwardExtentCopy = new ForwardExtentCopy(editSession, region, copy, region.getMinimumPoint());
        forwardExtentCopy.setCopyingEntities(true);
        Operations.complete(forwardExtentCopy);
        editSession.flushSession();

        return copy;
    }

    public static void clear(final BlockArrayClipboard clipboard) {
        try {
            final EditSession editSession = WorldEdit.getInstance().getEditSessionFactory().getEditSession(clipboard.getRegion().getWorld(), -1);
            editSession.enableQueue();
            editSession.setBlocks(clipboard.getRegion(), BlockTypes.AIR.getDefaultState());
            editSession.flushSession();
        } catch (final MaxChangedBlocksException e) {
            //-
        }
    }

    public static Region getRegion(final Location l1, final Location l2) {
        final Vector min = new Vector(l1.getBlockX(), l1.getBlockY(), l1.getBlockZ());
        final Vector max = new Vector(l2.getBlockX(), l2.getBlockY(), l2.getBlockZ());

        final CuboidRegion region = new CuboidRegion(new BukkitWorld(l1.getWorld()), min, max);
        return region;
    }

    private static String getFileSuffix() {
        return '.' + BuiltInClipboardFormat.SPONGE_SCHEMATIC.getPrimaryFileExtension();
    }
}
