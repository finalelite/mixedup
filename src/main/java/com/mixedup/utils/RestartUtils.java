package com.mixedup.utils;

import br.com.finalelite.pauloo27.api.message.MessageUtils;
import com.destroystokyo.paper.Title;
import com.mixedup.Main;
import com.mixedup.apis.ServerAPI;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.concurrent.atomic.AtomicInteger;

public class RestartUtils {

    @Setter
    @Getter
    private boolean autoRestart = true;

    private BukkitTask task;

    public static void movePlayersToHub1() {
        if (Bukkit.getOnlinePlayers().size() > 0) {
            for (final Player target : Bukkit.getOnlinePlayers()) {
                try {
                    final ByteArrayOutputStream b = new ByteArrayOutputStream();
                    final DataOutputStream out = new DataOutputStream(b);

                    out.writeUTF("Connect");
                    out.writeUTF("Hub1");

                    target.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                } catch (final Exception e) {
                    target.kickPlayer("...");
                    e.printStackTrace();
                }
            }
        }
    }

    public void startTask() {
        new BukkitRunnable() {
            @Override
            public void run() {
                if (!RestartUtils.this.autoRestart)
                    return;

                if (RestartUtils.this.isRestarting()) {
                    return;
                }

                final val now = Instant.now();
                final val uptime = now.getEpochSecond() - Main.startedDate.getEpochSecond();
                final val ldt = LocalDateTime.ofInstant(now, ZoneId.systemDefault());


                // if the uptime is at least 2 hours and it's 3:05 or 12:05, then restart

                if (uptime >= 60 * 60 * 2 &&
                        ((ldt.getHour() == 3 && ldt.getMinute() >= 5)
                                ||
                                (ldt.getHour() == 12 && ldt.getMinute() >= 5)))

                    RestartUtils.this.restart();

            }
        }.runTaskTimerAsynchronously(Main.getInstance(), 60 * 20, 20 * 30);
    }

    public void restart() {
        if (this.isRestarting())
            return;

        final val cycle = new AtomicInteger();
        this.task = new BukkitRunnable() {
            @Override
            public void run() {
                final val currentCycle = cycle.getAndIncrement();
                final val reamingTime = (5 * 60) - currentCycle * 10;
                // 4.59 minutes
                if (currentCycle >= 30) {
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            Bukkit.getOnlinePlayers().forEach(p -> p.sendTitle(Title.builder()
                                    .fadeIn(30)
                                    .fadeOut(30)
                                    .stay(50)
                                    .title("")
                                    .subtitle(MessageUtils
                                            .colourMessage("&4* Servidor reiniciando *"))
                                    .build()));

                            RestartUtils.movePlayersToHub1();
                        }

                    }.runTask(Main.getInstance());

                    try {
                        Thread.sleep(5000);
                    } catch (final InterruptedException e) {
                        e.printStackTrace();
                    }

                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            Bukkit.getOnlinePlayers().forEach(p -> p.kickPlayer(ChatColor.RED + "Reiniciando..."));
                            Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "save-all");
                            Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "restart");
                        }

                    }.runTask(Main.getInstance());
                    this.cancel();
                }

                // 30 seconds
                if (currentCycle == 27) {
                    ServerAPI.setRestarting(true);
                }

                if (reamingTime != 60 && reamingTime > 30 && currentCycle % 6 == 0) {
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            Bukkit.getOnlinePlayers().forEach(p -> {
                                p.sendMessage(MessageUtils
                                        .colourMessage(String.format("\n&4 * O servidor irá"
                                                        + " reiniciar em %s.\n",
                                                MessageUtils.pluralize(reamingTime / 60,
                                                        "minuto", "minutos"))));
                            });
                        }

                    }.runTask(Main.getInstance());
                } else if (reamingTime == 60) {
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            Bukkit.getOnlinePlayers().forEach(p -> {
                                p.sendMessage(MessageUtils
                                        .colourMessage("&4&l * O servidor irá "
                                                + "reiniciar em 1 minuto."));

                                p.sendTitle(Title.builder()
                                        .fadeIn(30)
                                        .fadeOut(30)
                                        .stay(50)
                                        .title("")
                                        .subtitle(MessageUtils
                                                .colourMessage("&4&l * O servidor irá reiniciar em 1 minuto.")).build());
                            });
                        }
                    }.runTask(Main.getInstance());
                } else if (reamingTime <= 30) {
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            Bukkit.getOnlinePlayers().forEach(p -> {
                                p.sendMessage(MessageUtils
                                        .colourMessage(String.format("&4&l * O servidor irá reiniciar em %s.",
                                                MessageUtils.pluralize(reamingTime,
                                                        "segundo", "segundos"))));

                                p.sendTitle(Title.builder()
                                        .fadeIn(30)
                                        .fadeOut(30)
                                        .stay(50)
                                        .title("")
                                        .subtitle(MessageUtils
                                                .colourMessage(String.format("&4&l * O servidor irá reiniciar em %s.",
                                                        MessageUtils.pluralize(reamingTime,
                                                                "segundo", "segundos")))).build());
                            });
                        }
                    }.runTask(Main.getInstance());
                }
            }
        }.runTaskTimerAsynchronously(Main.getInstance(), 20, 20 * 10);
    }

    public void cancel() {
        if (!this.isRestarting())
            return;

        this.task.cancel();

        Bukkit.getOnlinePlayers().forEach(p -> {
            p.sendMessage(MessageUtils
                    .colourMessage("&4&l * O reinicio foi cancelado!"));

            p.sendTitle(Title.builder()
                    .fadeIn(30)
                    .fadeOut(30)
                    .stay(50)
                    .title("")
                    .subtitle(MessageUtils
                            .colourMessage("&4&l * O servidor NÃO irá mais reiniciar.")).build());
        });

        this.task = null;
    }

    public boolean isRestarting() {
        return this.task != null;
    }

}
