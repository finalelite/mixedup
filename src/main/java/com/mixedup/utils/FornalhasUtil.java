package com.mixedup.utils;

import org.bukkit.inventory.Inventory;

import java.util.HashMap;

public class FornalhasUtil {

    private static final HashMap<String, FornalhasUtil> CACHE = new HashMap<String, FornalhasUtil>();
    private final String UUID;
    private Inventory fornalha1;
    private Inventory fornalha2;
    private Inventory fornalha3;
    private Inventory fornalha4;

    public FornalhasUtil(final String UUID, final Inventory fornalha1, final Inventory fornalha2, final Inventory fornalha3, final Inventory fornalha4) {
        this.UUID = UUID;
        this.fornalha1 = fornalha1;
        this.fornalha2 = fornalha2;
        this.fornalha3 = fornalha3;
        this.fornalha4 = fornalha4;
    }

    public static FornalhasUtil get(final String UUID) {
        return FornalhasUtil.CACHE.get(String.valueOf(UUID));
    }

    public FornalhasUtil insert() {
        FornalhasUtil.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public Inventory getFornalha1() {
        return fornalha1;
    }

    public void setFornalha1(final Inventory fornalha1) {
        this.fornalha1 = fornalha1;
    }

    public Inventory getFornalha2() {
        return fornalha2;
    }

    public void setFornalha2(final Inventory fornalha2) {
        this.fornalha2 = fornalha2;
    }

    public Inventory getFornalha3() {
        return fornalha3;
    }

    public void setFornalha3(final Inventory fornalha3) {
        this.fornalha3 = fornalha3;
    }

    public Inventory getFornalha4() {
        return fornalha4;
    }

    public void setFornalha4(final Inventory fornalha4) {
        this.fornalha4 = fornalha4;
    }

    public String getUUID() {
        return UUID;
    }
}
