package com.mixedup.utils;

public class Scoreboard {

/*
    public org.bukkit.scoreboard.Scoreboard sb;

    public Scoreboard(Player player) {
        sb = Bukkit.getScoreboardManager().getNewScoreboard();

        if (player.getLocation().getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
            Objective obj = sb.registerNewObjective("FinalElite", "stats");

            obj.setDisplaySlot(DisplaySlot.SIDEBAR);

            if (FacAPI.getFacNome(player.getUniqueId().toString()) != null) {
                Score spacer7 = obj.getScore("§1 ");
                spacer7.setScore(14);

                Score score9 = obj.getScore(ChatColor.GRAY + " Grupo: ");
                score9.setScore(13);

                Score score2 = obj.getScore(ChatColor.GRAY + " McMMO: ");
                score2.setScore(12);

                Score spacer6 = obj.getScore(ChatColor.GRAY + " Poder: ");
                spacer6.setScore(11);

                Score score7 = obj.getScore("§2 ");
                score7.setScore(10);

                Score score6 = obj.getScore(ChatColor.GRAY + "  ");
                score6.setScore(9);

                Score spacer4 = obj.getScore(ChatColor.GRAY + "    Online: ");
                spacer4.setScore(8);

                Score score8 = obj.getScore(ChatColor.GRAY + "    Poder: ");
                score8.setScore(7);

                Score spacer3 = obj.getScore(ChatColor.GRAY + "    Terras: ");
                spacer3.setScore(6);

                Score score5 = obj.getScore("§6 ");
                score5.setScore(5);

                Score spacer2 = obj.getScore(ChatColor.GRAY + " Moedas: ");
                spacer2.setScore(4);

                Score score4 = obj.getScore(ChatColor.GRAY + " Moedas negras: ");
                score4.setScore(3);

                Score spacer1 = obj.getScore("§7 ");
                spacer1.setScore(2);

                Score score1 = obj.getScore("§5 www.finalelite.com.br");
                score1.setScore(1);
            } else {
                Score spacer7 = obj.getScore("§1 ");
                spacer7.setScore(14);

                Score score9 = obj.getScore(ChatColor.GRAY + " Grupo: ");
                score9.setScore(13);

                Score score2 = obj.getScore(ChatColor.GRAY + " McMMO: ");
                score2.setScore(12);

                Score spacer6 = obj.getScore(ChatColor.GRAY + " Poder: ");
                spacer6.setScore(11);

                Score score7 = obj.getScore("§2 ");
                score7.setScore(10);

                Score score6 = obj.getScore(ChatColor.GRAY + " Sem esquadrão");
                score6.setScore(9);

                Score score5 = obj.getScore("§6 ");
                score5.setScore(5);

                Score spacer2 = obj.getScore(ChatColor.GRAY + " Moedas: ");
                spacer2.setScore(4);

                Score score4 = obj.getScore(ChatColor.GRAY + " Moedas negras: ");
                score4.setScore(3);

                Score spacer1 = obj.getScore("§7 ");
                spacer1.setScore(2);

                Score score1 = obj.getScore("§5 www.finalelite.com.br");
                score1.setScore(1);
            }

            A.clear();
            A.add("1");

            String chunk = String.valueOf(player.getChunk().getX()) + ":" + String.valueOf(player.getChunk().getZ());
            if (FacAPI.getSobAttack(FacAPI.getFacNome(player.getUniqueId().toString())) == true) {
                obj.setDisplayName(AlternateColor.alternate("&4&lSob ataque"));
            } else if (FacAPI.getChunkOwn(chunk) == null) {
                obj.setDisplayName(AlternateColor.alternate("&2&lZona livre"));
            } else {
                obj.setDisplayName(AlternateColor.alternate("&6&lZona protegida"));
            }

            Team Grupo = sb.registerNewTeam("grupo");
            Team mcmmo = sb.registerNewTeam("mcmmo");
            Team poderplayer = sb.registerNewTeam("poderplayer");
            Team esquadrao = sb.registerNewTeam("esquadrao");
            Team online = sb.registerNewTeam("online");
            Team poder = sb.registerNewTeam("poder");
            Team terras = sb.registerNewTeam("terras");
            Team moedas = sb.registerNewTeam("moeda");
            Team moedasnegras = sb.registerNewTeam("moedanegra");

            Grupo.addEntry(ChatColor.GRAY + " Grupo: ");
            mcmmo.addEntry(ChatColor.GRAY + " McMMO: ");
            poderplayer.addEntry(ChatColor.GRAY + " Poder: ");
            esquadrao.addEntry(ChatColor.GRAY + "  ");
            online.addEntry(ChatColor.GRAY + "    Online: ");
            poder.addEntry(ChatColor.GRAY + "    Poder: ");
            terras.addEntry(ChatColor.GRAY + "    Terras: ");
            moedas.addEntry(ChatColor.GRAY + " Moedas: ");
            moedasnegras.addEntry(ChatColor.GRAY + " Moedas negras: ");

            Team MASTER = sb.registerNewTeam("00000master");
            Team SUPERVISOR = sb.registerNewTeam("00001supervisor");
            Team ADMIN = sb.registerNewTeam("00002admin");
            Team YOUTUBER = sb.registerNewTeam("00003youtuber");
            Team MODERADOR = sb.registerNewTeam("00004moderador");
            Team SUPORTE = sb.registerNewTeam("00005suporte");
            Team VIP = sb.registerNewTeam("00006titan");
            Team VIPP = sb.registerNewTeam("00007duque");
            Team VIPPP = sb.registerNewTeam("00008lord");
            Team VIPPPP = sb.registerNewTeam("00009conde");
            Team DEFAULT = sb.registerNewTeam("00010membro");

            MASTER.setPrefix(ChatColor.GOLD + "[GM] ");
            SUPERVISOR.setPrefix(ChatColor.DARK_RED + "[Supervisor] ");
            ADMIN.setPrefix(ChatColor.RED + "[Admin] ");
            YOUTUBER.setPrefix(ChatColor.RED + "[Youtuber] ");
            MODERADOR.setPrefix(ChatColor.DARK_GREEN + "[Moderador] ");
            SUPORTE.setPrefix(ChatColor.GREEN + "[Suporte] ");
            VIP.setPrefix(ChatColor.DARK_PURPLE + "[Titan] ");
            VIPP.setPrefix(ChatColor.DARK_RED + "[Duque] ");
            VIPPP.setPrefix(ChatColor.AQUA + "[Lord] ");
            VIPPPP.setPrefix(ChatColor.YELLOW + "[Conde] ");
            DEFAULT.setPrefix("§7");
        } else {
            Objective obj = sb.registerNewObjective("FinalElite", "stats");

            obj.setDisplaySlot(DisplaySlot.SIDEBAR);

            Score spacer7 = obj.getScore("§1 ");
            spacer7.setScore(15);

            Score score9 = obj.getScore(ChatColor.GRAY + " Grupo: ");
            score9.setScore(14);

            Score score2 = obj.getScore(ChatColor.GRAY + " Online: ");
            score2.setScore(13);

            Score spacer6 = obj.getScore("§2 ");
            spacer6.setScore(12);

            Score score7 = obj.getScore(ChatColor.GRAY + " Moeda: ");
            score7.setScore(11);

            Score score6 = obj.getScore(ChatColor.GRAY + " Moeda negra: ");
            score6.setScore(10);

            Score spacer4 = obj.getScore("§4 ");
            spacer4.setScore(9);

            Score score8 = obj.getScore(ChatColor.GRAY + " Energia: ");
            score8.setScore(8);

            Score spacer3 = obj.getScore("§5 ");
            spacer3.setScore(7);

            Score score5 = obj.getScore(ChatColor.GRAY + " Esquadrão: ");
            score5.setScore(6);

            Score spacer2 = obj.getScore("§6 ");
            spacer2.setScore(5);

            Score score4 = obj.getScore(ChatColor.GRAY + " Guilda: ");
            score4.setScore(4);

            Score score3 = obj.getScore(ChatColor.GRAY + " Guilda rank: ");
            score3.setScore(3);

            Score spacer1 = obj.getScore("§7 ");
            spacer1.setScore(2);

            Score score1 = obj.getScore("§5 www.finalelite.com.br");
            score1.setScore(1);

            A.clear();
            A.add("1");

            obj.setDisplayName(AlternateColor.alternate("&6&lFinalElite"));

            Team Grupo = sb.registerNewTeam("grupo");
            Team energia = sb.registerNewTeam("energia");
            Team moeda = sb.registerNewTeam("moeda");
            Team moedanegra = sb.registerNewTeam("moedanegra");
            Team esquadrao = sb.registerNewTeam("esquadrao");
            Team guilda = sb.registerNewTeam("guilda");
            Team guildarank = sb.registerNewTeam("guildarank");
            Team online = sb.registerNewTeam("online");

            Grupo.addEntry(ChatColor.GRAY + " Grupo: ");
            online.addEntry(ChatColor.GRAY + " Online: ");
            energia.addEntry(ChatColor.GRAY + " Energia: ");
            moeda.addEntry(ChatColor.GRAY + " Moeda: ");
            moedanegra.addEntry(ChatColor.GRAY + " Moeda negra: ");
            esquadrao.addEntry(ChatColor.GRAY + " Esquadrão: ");
            guilda.addEntry(ChatColor.GRAY + " Guilda: ");
            guildarank.addEntry(ChatColor.GRAY + " Guilda rank: ");

            Team MASTER = sb.registerNewTeam("00000master");
            Team SUPERVISOR = sb.registerNewTeam("00001supervisor");
            Team ADMIN = sb.registerNewTeam("00002admin");
            Team YOUTUBER = sb.registerNewTeam("00003youtuber");
            Team MODERADOR = sb.registerNewTeam("00004moderador");
            Team SUPORTE = sb.registerNewTeam("00005suporte");
            Team VIP = sb.registerNewTeam("00006titan");
            Team VIPP = sb.registerNewTeam("00007duque");
            Team VIPPP = sb.registerNewTeam("00008lord");
            Team VIPPPP = sb.registerNewTeam("00009conde");
            Team DEFAULT = sb.registerNewTeam("00010membro");

            MASTER.setPrefix(ChatColor.GOLD + "[GM] ");
            SUPERVISOR.setPrefix(ChatColor.DARK_RED + "[Supervisor] ");
            ADMIN.setPrefix(ChatColor.RED + "[Admin] ");
            YOUTUBER.setPrefix(ChatColor.RED + "[Youtuber] ");
            MODERADOR.setPrefix(ChatColor.DARK_GREEN + "[Moderador] ");
            SUPORTE.setPrefix(ChatColor.GREEN + "[Suporte] ");
            VIP.setPrefix(ChatColor.DARK_PURPLE + "[Titan] ");
            VIPP.setPrefix(ChatColor.DARK_RED + "[Duque] ");
            VIPPP.setPrefix(ChatColor.AQUA + "[Lord] ");
            VIPPPP.setPrefix(ChatColor.YELLOW + "[Conde] ");
            DEFAULT.setPrefix("§7");
        }
    }

    public void sendToPlayer(Player player) {
        String UUID = player.getUniqueId().toString();
        String tag = TagAPI.getTag(UUID);

        if (player.getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
            if (tag.equals("Master")) {
                sb.getTeam("grupo").setSuffix(ChatColor.GOLD + "GameMaster");
            } else {
                if (tag.equals("Admin")) {
                    sb.getTeam("grupo").setSuffix(ChatColor.RED + "Admin");
                } else {
                    if (tag.equals("Supervisor")) {
                        sb.getTeam("grupo").setSuffix(ChatColor.DARK_RED + "Supervisor");
                    } else {
                        if (tag.equals("Youtuber")) {
                            sb.getTeam("grupo").setSuffix(ChatColor.RED + "Youtuber");
                        } else {
                            if (tag.equals("Moderador")) {
                                sb.getTeam("grupo").setSuffix(ChatColor.DARK_GREEN + "Moderador");
                            } else {
                                if (tag.equals("Suporte")) {
                                    sb.getTeam("grupo").setSuffix(ChatColor.GREEN + "Suporte");
                                } else {
                                    if (tag.equals("Vip+++")) {
                                        sb.getTeam("grupo").setSuffix(ChatColor.DARK_PURPLE + "Titan");
                                    } else {
                                        if (tag.equals("Vip++")) {
                                            sb.getTeam("grupo").setSuffix(ChatColor.DARK_RED + "Duque");
                                        } else {
                                            if (tag.equals("Vip+")) {
                                                sb.getTeam("grupo").setSuffix(ChatColor.AQUA + "Lord");
                                            } else {
                                                if (tag.equals("Vip")) {
                                                    sb.getTeam("grupo").setSuffix(ChatColor.YELLOW + "Conde");
                                                } else {
                                                    if (tag.equals("Membro")) {
                                                        sb.getTeam("grupo").setSuffix(ChatColor.GRAY + "Membro");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            DecimalFormat formatter = new DecimalFormat("#,###.00");
            DecimalFormat formatterEnergia = new DecimalFormat("#####.#####");

            int valor = McMMOApi.getNivel(UUID + "Acrobacias") + McMMOApi.getNivel(UUID + "Alquimia") + McMMOApi.getNivel(UUID + "Arqueiro") + McMMOApi.getNivel(UUID + "Encantar") + McMMOApi.getNivel(UUID + "Escavacao") + McMMOApi.getNivel(UUID + "Espada") + McMMOApi.getNivel(UUID + "Herbalismo") + McMMOApi.getNivel(UUID + "Machado") + McMMOApi.getNivel(UUID + "Mineracao") + McMMOApi.getNivel(UUID + "Reparacao");
            sb.getTeam("mcmmo").setSuffix(ChatColor.GREEN + String.valueOf(valor));
            sb.getTeam("poderplayer").setSuffix(ChatColor.GREEN + String.valueOf(PoderAPI.getPoder(UUID)) + "/15");

            if (CoinsAPI.getCoins(UUID) != 0.0) {
                sb.getTeam("moeda").setSuffix(ChatColor.GREEN + formatter.format(CoinsAPI.getCoins(UUID)));
            } else {
                sb.getTeam("moeda").setSuffix(ChatColor.RED + "0.0");
            }

            if (CoinsAPI.getBlackCoins(UUID) != 0.0) {
                sb.getTeam("moedanegra").setSuffix(ChatColor.GREEN + formatterEnergia.format(CoinsAPI.getBlackCoins(UUID)));
            } else {
                sb.getTeam("moedanegra").setSuffix(ChatColor.RED + "0.0");
            }

            if (FacAPI.getFacNome(UUID) != null) {
                sb.getTeam("esquadrao").setSuffix(ChatColor.GREEN + FacAPI.getFacNome(UUID));
                sb.getTeam("online").setSuffix(ChatColor.GREEN + String.valueOf(getFacOnline(UUID)));
                sb.getTeam("poder").setSuffix(ChatColor.GREEN + String.valueOf(getPoderFac(UUID)));
                sb.getTeam("terras").setSuffix(ChatColor.GREEN + String.valueOf(FacAPI.getTerras(FacAPI.getFacNome(UUID))));
            }

            for (Player target : Bukkit.getOnlinePlayers()) {

                String UUIDtarget = PlayerUUID.getUUID(target.getName());
                String tagTarget = TagAPI.getTag(UUIDtarget);

                if (tagTarget.equals("Master")) {
                    if (target.getName().equals("Mistter_")) {
                        sb.getTeam("00000master").addEntry(target.getName());
                        sb.getTeam("00000master").setColor(ChatColor.GOLD);
                        sb.getTeam("00000master").setSuffix(ChatColor.GRAY + " [CEO]");
                    } else {
                        sb.getTeam("00000master").addEntry(target.getName());
                        sb.getTeam("00000master").setColor(ChatColor.GOLD);
                    }
                } else {
                    if (tagTarget.equals("Admin")) {
                        sb.getTeam("00002admin").addEntry(target.getName());
                        sb.getTeam("00002admin").setColor(ChatColor.RED);
                    } else {
                        if (tagTarget.equals("Supervisor")) {
                            sb.getTeam("00001supervisor").addEntry(target.getName());
                            sb.getTeam("00001supervisor").setColor(ChatColor.DARK_RED);
                        } else {
                            if (tagTarget.equals("Moderador")) {
                                sb.getTeam("00004moderador").addEntry(target.getName());
                                sb.getTeam("00004moderador").setColor(ChatColor.DARK_GREEN);
                            } else {
                                if (tagTarget.equals("Suporte")) {
                                    sb.getTeam("00005suporte").addEntry(target.getName());
                                    sb.getTeam("00005suporte").setColor(ChatColor.GREEN);
                                } else {
                                    if (tagTarget.equals("Youtuber")) {
                                        sb.getTeam("00003youtuber").addEntry(target.getName());
                                        sb.getTeam("00003youtuber").setColor(ChatColor.RED);
                                    } else {
                                        if (tagTarget.equals("Vip+++")) {
                                            sb.getTeam("00006titan").addEntry(target.getName());
                                            sb.getTeam("00006titan").setColor(ChatColor.DARK_PURPLE);
                                        } else {
                                            if (tagTarget.equals("Vip++")) {
                                                sb.getTeam("00007duque").addEntry(target.getName());
                                                sb.getTeam("00007duque").setColor(ChatColor.DARK_RED);
                                            } else {
                                                if (tagTarget.equals("Vip+")) {
                                                    sb.getTeam("00008lord").addEntry(target.getName());
                                                    sb.getTeam("00008lord").setColor(ChatColor.AQUA);
                                                } else {
                                                    if (tagTarget.equals("Vip")) {
                                                        sb.getTeam("00009conde").addEntry(target.getName());
                                                        sb.getTeam("00009conde").setColor(ChatColor.YELLOW);
                                                    } else {
                                                        if (tagTarget.equals("Membro")) {
                                                            sb.getTeam("00010membro").addEntry(target.getName());
                                                            sb.getTeam("00010membro").setColor(ChatColor.GRAY);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            player.setScoreboard(sb);
            BackEnd.scoreboard.put(player.getUniqueId(), this);
        } else {
            //----------------------------------------------------------------------------------------------------------

            String tagP = TagAPI.getTag(UUID);
            if (tagP.equals("Master")) {
                sb.getTeam("grupo").setSuffix(ChatColor.GOLD + "GameMaster");
            } else {
                if (tagP.equals("Admin")) {
                    sb.getTeam("grupo").setSuffix(ChatColor.RED + "Admin");
                } else {
                    if (tagP.equals("Supervisor")) {
                        sb.getTeam("grupo").setSuffix(ChatColor.DARK_RED + "Supervisor");
                    } else {
                        if (tagP.equals("Youtuber")) {
                            sb.getTeam("grupo").setSuffix(ChatColor.RED + "Youtuber");
                        } else {
                            if (tagP.equals("Moderador")) {
                                sb.getTeam("grupo").setSuffix(ChatColor.DARK_GREEN + "Moderador");
                            } else {
                                if (tagP.equals("Suporte")) {
                                    sb.getTeam("grupo").setSuffix(ChatColor.GREEN + "Suporte");
                                } else {
                                    if (tagP.equals("Vip+++")) {
                                        sb.getTeam("grupo").setSuffix(ChatColor.DARK_PURPLE + "Titan");
                                    } else {
                                        if (tagP.equals("Vip++")) {
                                            sb.getTeam("grupo").setSuffix(ChatColor.DARK_RED + "Duque");
                                        } else {
                                            if (tagP.equals("Vip+")) {
                                                sb.getTeam("grupo").setSuffix(ChatColor.AQUA + "Lord");
                                            } else {
                                                if (tagP.equals("Vip")) {
                                                    sb.getTeam("grupo").setSuffix(ChatColor.YELLOW + "Conde");
                                                } else {
                                                    if (tagP.equals("Membro")) {
                                                        sb.getTeam("grupo").setSuffix(ChatColor.GRAY + "Membro");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            DecimalFormat formatter = new DecimalFormat("#,###.00");
            DecimalFormat formatterEnergia = new DecimalFormat("#####.#####");

            sb.getTeam("online").setSuffix(ChatColor.GREEN + String.valueOf(Bukkit.getOnlinePlayers().size()));

            if (CoinsAPI.getCoins(UUID) != 0.0) {
                sb.getTeam("moeda").setSuffix(ChatColor.GREEN + formatter.format(CoinsAPI.getCoins(UUID)));
            } else {
                sb.getTeam("moeda").setSuffix(ChatColor.RED + "0.0");
            }

            if (CoinsAPI.getBlackCoins(UUID) != 0.0) {
                sb.getTeam("moedanegra").setSuffix(ChatColor.GREEN + formatter.format(CoinsAPI.getBlackCoins(UUID)));
            } else {
                sb.getTeam("moedanegra").setSuffix(ChatColor.RED + "0.0");
            }

            if (EnergiaAPI.getEnergia(UUID) != 0.0) {
                sb.getTeam("energia").setSuffix(ChatColor.GREEN + formatterEnergia.format(EnergiaAPI.getEnergia(UUID)) + " kW");
            } else {
                sb.getTeam("energia").setSuffix(ChatColor.RED + "0 kW");
            }

            if (FacAPI.getFacNome(UUID) == null) {
                sb.getTeam("esquadrao").setSuffix(AlternateColor.alternate("&cNENHUM"));
            } else {
                sb.getTeam("esquadrao").setSuffix(ChatColor.YELLOW + "[" + FacAPI.getTagWithNome(FacAPI.getFacNome(UUID)) + "]");
            }

            if (GuildaAPI.getGuilda(player.getUniqueId().toString()) != null) {
                if (GuildaAPI.getGuilda(player.getUniqueId().toString()).equals("sanguinaria")) {
                    sb.getTeam("guilda").setSuffix(ChatColor.RED + "Sanguinaria");
                }
                if (GuildaAPI.getGuilda(player.getUniqueId().toString()).equals("ancia")) {
                    sb.getTeam("guilda").setSuffix(ChatColor.BLUE + "Anciã");
                }
                if (GuildaAPI.getGuilda(player.getUniqueId().toString()).equals("nobre")) {
                    sb.getTeam("guilda").setSuffix(ChatColor.DARK_GREEN + "Nobre");
                }
                sb.getTeam("guildarank").setSuffix(ChatColor.GREEN + String.valueOf(GuildaAPI.getGuildaRank(UUID)));
            } else {
                sb.getTeam("guilda").setSuffix(AlternateColor.alternate("&cNENHUM"));
                sb.getTeam("guildarank").setSuffix(AlternateColor.alternate("&cNENHUM"));
            }


            for (Player target : Bukkit.getOnlinePlayers()) {

                String UUIDtarget = PlayerUUID.getUUID(target.getName());
                String tagTarget = TagAPI.getTag(UUIDtarget);

                if (tagTarget.equals("Master")) {
                    if (target.getName().equals("Mistter_")) {
                        sb.getTeam("00000master").addEntry(target.getName());
                        sb.getTeam("00000master").setColor(ChatColor.GOLD);
                        sb.getTeam("00000master").setSuffix(ChatColor.GRAY + " [CEO]");
                    } else {
                        sb.getTeam("00000master").addEntry(target.getName());
                        sb.getTeam("00000master").setColor(ChatColor.GOLD);
                    }
                } else {
                    if (tagTarget.equals("Admin")) {
                        sb.getTeam("00002admin").addEntry(target.getName());
                        sb.getTeam("00002admin").setColor(ChatColor.RED);
                    } else {
                        if (tagTarget.equals("Supervisor")) {
                            sb.getTeam("00001supervisor").addEntry(target.getName());
                            sb.getTeam("00001supervisor").setColor(ChatColor.DARK_RED);
                        } else {
                            if (tagTarget.equals("Moderador")) {
                                sb.getTeam("00004moderador").addEntry(target.getName());
                                sb.getTeam("00004moderador").setColor(ChatColor.DARK_GREEN);
                            } else {
                                if (tagTarget.equals("Suporte")) {
                                    sb.getTeam("00005suporte").addEntry(target.getName());
                                    sb.getTeam("00005suporte").setColor(ChatColor.GREEN);
                                } else {
                                    if (tagTarget.equals("Youtuber")) {
                                        sb.getTeam("00003youtuber").addEntry(target.getName());
                                        sb.getTeam("00003youtuber").setColor(ChatColor.RED);
                                    } else {
                                        if (tagTarget.equals("Vip+++")) {
                                            sb.getTeam("00006titan").addEntry(target.getName());
                                            sb.getTeam("00006titan").setColor(ChatColor.DARK_PURPLE);
                                        } else {
                                            if (tagTarget.equals("Vip++")) {
                                                sb.getTeam("00007duque").addEntry(target.getName());
                                                sb.getTeam("00007duque").setColor(ChatColor.DARK_RED);
                                            } else {
                                                if (tagTarget.equals("Vip+")) {
                                                    sb.getTeam("00008lord").addEntry(target.getName());
                                                    sb.getTeam("00008lord").setColor(ChatColor.AQUA);
                                                } else {
                                                    if (tagTarget.equals("Vip")) {
                                                        sb.getTeam("00009conde").addEntry(target.getName());
                                                        sb.getTeam("00009conde").setColor(ChatColor.YELLOW);
                                                    } else {
                                                        if (tagTarget.equals("Membro")) {
                                                            sb.getTeam("00010membro").addEntry(target.getName());
                                                            sb.getTeam("00010membro").setColor(ChatColor.GRAY);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            player.setScoreboard(sb);
            BackEnd.scoreboard.put(player.getUniqueId(), this);
        }
    }

    public void updateScore(Player player) {
        String UUID = player.getUniqueId().toString();
        String tag = TagAPI.getTag(UUID);

        if (player.getWorld().getName().equalsIgnoreCase("Trappist-1b")) {
            if (tag.equals("Master")) {
                sb.getTeam("grupo").setSuffix(ChatColor.GOLD + "GameMaster");
            } else {
                if (tag.equals("Admin")) {
                    sb.getTeam("grupo").setSuffix(ChatColor.RED + "Admin");
                } else {
                    if (tag.equals("Supervisor")) {
                        sb.getTeam("grupo").setSuffix(ChatColor.DARK_RED + "Supervisor");
                    } else {
                        if (tag.equals("Youtuber")) {
                            sb.getTeam("grupo").setSuffix(ChatColor.RED + "Youtuber");
                        } else {
                            if (tag.equals("Moderador")) {
                                sb.getTeam("grupo").setSuffix(ChatColor.DARK_GREEN + "Moderador");
                            } else {
                                if (tag.equals("Suporte")) {
                                    sb.getTeam("grupo").setSuffix(ChatColor.GREEN + "Suporte");
                                } else {
                                    if (tag.equals("Vip+++")) {
                                        sb.getTeam("grupo").setSuffix(ChatColor.DARK_PURPLE + "Titan");
                                    } else {
                                        if (tag.equals("Vip++")) {
                                            sb.getTeam("grupo").setSuffix(ChatColor.DARK_RED + "Duque");
                                        } else {
                                            if (tag.equals("Vip+")) {
                                                sb.getTeam("grupo").setSuffix(ChatColor.AQUA + "Lord");
                                            } else {
                                                if (tag.equals("Vip")) {
                                                    sb.getTeam("grupo").setSuffix(ChatColor.YELLOW + "Conde");
                                                } else {
                                                    if (tag.equals("Membro")) {
                                                        sb.getTeam("grupo").setSuffix(ChatColor.GRAY + "Membro");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            DecimalFormat formatter = new DecimalFormat("#,###.00");
            DecimalFormat formatterEnergia = new DecimalFormat("#####.#####");

            try {
                val level = McMMOApi.getTotalLevel(UUID);
                sb.getTeam("mcmmo").setSuffix(ChatColor.GREEN + String.valueOf(level));
            } catch (Exception e) {
                sb.getTeam("mcmmo").setSuffix(ChatColor.GREEN + "0");
            }
            sb.getTeam("poderplayer").setSuffix(ChatColor.GREEN + String.valueOf(PoderAPI.getPoder(UUID)) + "/15");

            int moeda = CoinsAPI.getCoins(UUID);
            if (moeda != 0.0) {
                sb.getTeam("moeda").setSuffix(ChatColor.GREEN + formatter.format(moeda));
            } else {
                sb.getTeam("moeda").setSuffix(ChatColor.RED + "0.0");
            }

            int blackCoin = CoinsAPI.getBlackCoins(UUID);
            if (blackCoin != 0.0) {
                sb.getTeam("moedanegra").setSuffix(ChatColor.GREEN + formatterEnergia.format(blackCoin));
            } else {
                sb.getTeam("moedanegra").setSuffix(ChatColor.RED + "0.0");
            }

            String facName = FacAPI.getFacNome(UUID);
            if (facName != null) {
                sb.getTeam("esquadrao").setSuffix(ChatColor.GREEN + facName);
                sb.getTeam("online").setSuffix(ChatColor.GREEN + String.valueOf(getFacOnline(UUID)));
                sb.getTeam("poder").setSuffix(ChatColor.GREEN + String.valueOf(getPoderFac(UUID)));
                sb.getTeam("terras").setSuffix(ChatColor.GREEN + String.valueOf(FacAPI.getTerras(facName)));
            }

            for (Player target : Bukkit.getOnlinePlayers()) {

                String UUIDtarget = PlayerUUID.getUUID(target.getName());
                String tagTarget = TagAPI.getTag(UUIDtarget);

                if (tagTarget.equals("Master")) {
                    if (target.getName().equals("Mistter_")) {
                        sb.getTeam("00000master").addEntry(target.getName());
                        sb.getTeam("00000master").setColor(ChatColor.GOLD);
                        sb.getTeam("00000master").setSuffix(ChatColor.GRAY + " [CEO]");
                    } else {
                        sb.getTeam("00000master").addEntry(target.getName());
                        sb.getTeam("00000master").setColor(ChatColor.GOLD);
                    }
                } else {
                    if (tagTarget.equals("Admin")) {
                        sb.getTeam("00002admin").addEntry(target.getName());
                        sb.getTeam("00002admin").setColor(ChatColor.RED);
                    } else {
                        if (tagTarget.equals("Supervisor")) {
                            sb.getTeam("00001supervisor").addEntry(target.getName());
                            sb.getTeam("00001supervisor").setColor(ChatColor.DARK_RED);
                        } else {
                            if (tagTarget.equals("Moderador")) {
                                sb.getTeam("00004moderador").addEntry(target.getName());
                                sb.getTeam("00004moderador").setColor(ChatColor.DARK_GREEN);
                            } else {
                                if (tagTarget.equals("Suporte")) {
                                    sb.getTeam("00005suporte").addEntry(target.getName());
                                    sb.getTeam("00005suporte").setColor(ChatColor.GREEN);
                                } else {
                                    if (tagTarget.equals("Youtuber")) {
                                        sb.getTeam("00003youtuber").addEntry(target.getName());
                                        sb.getTeam("00003youtuber").setColor(ChatColor.RED);
                                    } else {
                                        if (tagTarget.equals("Vip+++")) {
                                            sb.getTeam("00006titan").addEntry(target.getName());
                                            sb.getTeam("00006titan").setColor(ChatColor.DARK_PURPLE);
                                        } else {
                                            if (tagTarget.equals("Vip++")) {
                                                sb.getTeam("00007duque").addEntry(target.getName());
                                                sb.getTeam("00007duque").setColor(ChatColor.DARK_RED);
                                            } else {
                                                if (tagTarget.equals("Vip+")) {
                                                    sb.getTeam("00008lord").addEntry(target.getName());
                                                    sb.getTeam("00008lord").setColor(ChatColor.AQUA);
                                                } else {
                                                    if (tagTarget.equals("Vip")) {
                                                        sb.getTeam("00009conde").addEntry(target.getName());
                                                        sb.getTeam("00009conde").setColor(ChatColor.YELLOW);
                                                    } else {
                                                        if (tagTarget.equals("Membro")) {
                                                            sb.getTeam("00010membro").addEntry(target.getName());
                                                            sb.getTeam("00010membro").setColor(ChatColor.GRAY);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            //----------------------------------------------------------------------------------------------------------

            String tagP = TagAPI.getTag(UUID);
            if (tagP.equals("Master")) {
                sb.getTeam("grupo").setSuffix(ChatColor.GOLD + "GameMaster");
            } else {
                if (tagP.equals("Admin")) {
                    sb.getTeam("grupo").setSuffix(ChatColor.RED + "Admin");
                } else {
                    if (tagP.equals("Supervisor")) {
                        sb.getTeam("grupo").setSuffix(ChatColor.DARK_RED + "Supervisor");
                    } else {
                        if (tagP.equals("Youtuber")) {
                            sb.getTeam("grupo").setSuffix(ChatColor.RED + "Youtuber");
                        } else {
                            if (tagP.equals("Moderador")) {
                                sb.getTeam("grupo").setSuffix(ChatColor.DARK_GREEN + "Moderador");
                            } else {
                                if (tagP.equals("Suporte")) {
                                    sb.getTeam("grupo").setSuffix(ChatColor.GREEN + "Suporte");
                                } else {
                                    if (tagP.equals("Vip+++")) {
                                        sb.getTeam("grupo").setSuffix(ChatColor.DARK_PURPLE + "Titan");
                                    } else {
                                        if (tagP.equals("Vip++")) {
                                            sb.getTeam("grupo").setSuffix(ChatColor.DARK_RED + "Duque");
                                        } else {
                                            if (tagP.equals("Vip+")) {
                                                sb.getTeam("grupo").setSuffix(ChatColor.AQUA + "Lord");
                                            } else {
                                                if (tagP.equals("Vip")) {
                                                    sb.getTeam("grupo").setSuffix(ChatColor.YELLOW + "Conde");
                                                } else {
                                                    if (tagP.equals("Membro")) {
                                                        sb.getTeam("grupo").setSuffix(ChatColor.GRAY + "Membro");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            DecimalFormat formatter = new DecimalFormat("#,###.00");
            DecimalFormat formatterEnergia = new DecimalFormat("#####.#####");

            sb.getTeam("online").setSuffix(ChatColor.GREEN + String.valueOf(Bukkit.getOnlinePlayers().size()));

            int coins = CoinsAPI.getCoins(UUID);
            if (coins != 0.0) {
                sb.getTeam("moeda").setSuffix(ChatColor.GREEN + formatter.format(coins));
            } else {
                sb.getTeam("moeda").setSuffix(ChatColor.RED + "0.0");
            }

            int blackCoins = CoinsAPI.getBlackCoins(UUID);
            if (blackCoins != 0.0) {
                sb.getTeam("moedanegra").setSuffix(ChatColor.GREEN + formatter.format(blackCoins));
            } else {
                sb.getTeam("moedanegra").setSuffix(ChatColor.RED + "0.0");
            }

            double energia = EnergiaAPI.getEnergia(UUID);
            if (energia != 0.0) {
                sb.getTeam("energia").setSuffix(ChatColor.GREEN + formatterEnergia.format(energia) + " kW");
            } else {
                // https://media.discordapp.net/attachments/512681338013220864/534572522079911946/unknown.png
                // idk why, but okay
                if (sb.getTeam("energia") != null)
                    sb.getTeam("energia").setSuffix(ChatColor.RED + "0 kW");
            }

            String facNome = FacAPI.getFacNome(UUID);
            if (FacAPI.getFacNome(UUID) == null) {
                sb.getTeam("esquadrao").setSuffix(AlternateColor.alternate("&cNENHUM"));
            } else {
                sb.getTeam("esquadrao").setSuffix(ChatColor.YELLOW + "[" + FacAPI.getTagWithNome(facNome) + "]");
            }

            String guilda = GuildaAPI.getGuilda(player.getUniqueId().toString());
            if (guilda != null) {
                if (guilda.equals("sanguinaria")) {
                    sb.getTeam("guilda").setSuffix(ChatColor.RED + "Sanguinaria");
                }
                if (guilda.equals("ancia")) {
                    sb.getTeam("guilda").setSuffix(ChatColor.BLUE + "Anciã");
                }
                if (guilda.equals("nobre")) {
                    sb.getTeam("guilda").setSuffix(ChatColor.DARK_GREEN + "Nobre");
                }
                sb.getTeam("guildarank").setSuffix(ChatColor.GREEN + String.valueOf(GuildaAPI.getGuildaRank(UUID)));
            } else {
                sb.getTeam("guilda").setSuffix(AlternateColor.alternate("&cNENHUM"));
                sb.getTeam("guildarank").setSuffix(AlternateColor.alternate("&cNENHUM"));
            }


            for (Player target : Bukkit.getOnlinePlayers()) {

                String UUIDtarget = PlayerUUID.getUUID(target.getName());
                String tagTarget = TagAPI.getTag(UUIDtarget);

                if (tagTarget.equals("Master")) {
                    if (target.getName().equals("Mistter_")) {
                        sb.getTeam("00000master").addEntry(target.getName());
                        sb.getTeam("00000master").setColor(ChatColor.GOLD);
                        sb.getTeam("00000master").setSuffix(ChatColor.GRAY + " [CEO]");
                    } else {
                        sb.getTeam("00000master").addEntry(target.getName());
                        sb.getTeam("00000master").setColor(ChatColor.GOLD);
                    }
                } else {
                    if (tagTarget.equals("Admin")) {
                        sb.getTeam("00002admin").addEntry(target.getName());
                        sb.getTeam("00002admin").setColor(ChatColor.RED);
                    } else {
                        if (tagTarget.equals("Supervisor")) {
                            sb.getTeam("00001supervisor").addEntry(target.getName());
                            sb.getTeam("00001supervisor").setColor(ChatColor.DARK_RED);
                        } else {
                            if (tagTarget.equals("Moderador")) {
                                sb.getTeam("00004moderador").addEntry(target.getName());
                                sb.getTeam("00004moderador").setColor(ChatColor.DARK_GREEN);
                            } else {
                                if (tagTarget.equals("Suporte")) {
                                    sb.getTeam("00005suporte").addEntry(target.getName());
                                    sb.getTeam("00005suporte").setColor(ChatColor.GREEN);
                                } else {
                                    if (tagTarget.equals("Youtuber")) {
                                        sb.getTeam("00003youtuber").addEntry(target.getName());
                                        sb.getTeam("00003youtuber").setColor(ChatColor.RED);
                                    } else {
                                        if (tagTarget.equals("Vip+++")) {
                                            sb.getTeam("00006titan").addEntry(target.getName());
                                            sb.getTeam("00006titan").setColor(ChatColor.DARK_PURPLE);
                                        } else {
                                            if (tagTarget.equals("Vip++")) {
                                                sb.getTeam("00007duque").addEntry(target.getName());
                                                sb.getTeam("00007duque").setColor(ChatColor.DARK_RED);
                                            } else {
                                                if (tagTarget.equals("Vip+")) {
                                                    sb.getTeam("00008lord").addEntry(target.getName());
                                                    sb.getTeam("00008lord").setColor(ChatColor.AQUA);
                                                } else {
                                                    if (tagTarget.equals("Vip")) {
                                                        sb.getTeam("00009conde").addEntry(target.getName());
                                                        sb.getTeam("00009conde").setColor(ChatColor.YELLOW);
                                                    } else {
                                                        if (tagTarget.equals("Membro")) {
                                                            sb.getTeam("00010membro").addEntry(target.getName());
                                                            sb.getTeam("00010membro").setColor(ChatColor.GRAY);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private static int getPoderFac(String UUID) {
        int poder = 0;
        val facNome = FacAPI.getFacNome(UUID);
        String recrutas = FacAPI.getRecrutas(facNome);
        if (!recrutas.equals("NULL")) {
            if (recrutas.contains(":")) {
                String[] split = recrutas.split(":");
                for (int i = 1; i <= split.length; i++) {
                    poder += PoderAPI.getPoder(split[i - 1]);
                }
            } else {
                poder += PoderAPI.getPoder(recrutas);
            }
        }

        String membros = FacAPI.getMembros(facNome);
        if (!membros.equals("NULL")) {
            if (membros.contains(":")) {
                String[] split = membros.split(":");
                for (int i = 1; i <= split.length; i++) {
                    poder += PoderAPI.getPoder(split[i - 1]);
                }
            } else {
                poder += PoderAPI.getPoder(membros);
            }
        }

        String capitoes = FacAPI.getCapitoes(facNome);
        if (!capitoes.equals("NULL")) {
            if (capitoes.contains(":")) {
                String[] split = capitoes.split(":");
                for (int i = 1; i <= split.length; i++) {
                    poder += PoderAPI.getPoder(split[i - 1]);
                }
            } else {
                poder += PoderAPI.getPoder(capitoes);
            }
        }

        String lideres = FacAPI.getLideres(facNome);
        if (!lideres.equals("NULL")) {
            if (lideres.contains(":")) {
                String[] split = lideres.split(":");
                for (int i = 1; i <= split.length; i++) {
                    poder += PoderAPI.getPoder(split[i - 1]);
                }
            } else {
                poder += PoderAPI.getPoder(lideres);
            }
        }
        return poder;
    }

    private static int getFacOnline(String UUID) {
        int online = 0;
        val facNome = FacAPI.getFacNome(UUID);
        String recrutas = FacAPI.getRecrutas(facNome);
        if (!recrutas.equals("NULL")) {
            if (recrutas.contains(":")) {
                String[] split = recrutas.split(":");
                for (int i = 1; i <= split.length; i++) {
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        if (PlayerUUID.getUUID(target.getName()).equalsIgnoreCase(split[i - 1])) {
                            online++;
                        }
                    }
                }
            } else {
                for (Player target : Bukkit.getOnlinePlayers()) {
                    if (PlayerUUID.getUUID(target.getName()).equalsIgnoreCase(recrutas)) {
                        online++;
                    }
                }
            }
        }

        String membros = FacAPI.getMembros(facNome);
        if (!membros.equals("NULL")) {
            if (membros.contains(":")) {
                String[] split = membros.split(":");
                for (int i = 1; i <= split.length; i++) {
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        if (PlayerUUID.getUUID(target.getName()).equalsIgnoreCase(split[i - 1])) {
                            online++;
                        }
                    }
                }
            } else {
                for (Player target : Bukkit.getOnlinePlayers()) {
                    if (PlayerUUID.getUUID(target.getName()).equalsIgnoreCase(membros)) {
                        online++;
                    }
                }
            }
        }

        String capitoes = FacAPI.getCapitoes(facNome);
        if (!capitoes.equals("NULL")) {
            if (capitoes.contains(":")) {
                String[] split = capitoes.split(":");
                for (int i = 1; i <= split.length; i++) {
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        if (PlayerUUID.getUUID(target.getName()).equalsIgnoreCase(split[i - 1])) {
                            online++;
                        }
                    }
                }
            } else {
                for (Player target : Bukkit.getOnlinePlayers()) {
                    if (PlayerUUID.getUUID(target.getName()).equalsIgnoreCase(capitoes)) {
                        online++;
                    }
                }
            }
        }

        String lideres = FacAPI.getLideres(facNome);
        if (!lideres.equals("NULL")) {
            if (lideres.contains(":")) {
                String[] split = lideres.split(":");
                for (int i = 1; i <= split.length; i++) {
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        if (PlayerUUID.getUUID(target.getName()).equalsIgnoreCase(split[i - 1])) {
                            online++;
                        }
                    }
                }
            } else {
                for (Player target : Bukkit.getOnlinePlayers()) {
                    if (PlayerUUID.getUUID(target.getName()).equalsIgnoreCase(lideres)) {
                        online++;
                    }
                }
            }
        }
        return online;
    }
     */
}
