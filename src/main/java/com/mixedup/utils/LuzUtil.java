package com.mixedup.utils;

import java.util.HashMap;

public class LuzUtil {

    private static final HashMap<String, LuzUtil> CACHE = new HashMap<String, LuzUtil>();
    private final String UUID;
    private boolean status;

    public LuzUtil(final String UUID, final boolean status) {
        this.UUID = UUID;
        this.status = status;
    }

    public static LuzUtil get(final String UUID) {
        return LuzUtil.CACHE.get(String.valueOf(UUID));
    }

    public LuzUtil insert() {
        LuzUtil.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getstatus() {
        return status;
    }

    public void setstatus(final boolean status) {
        this.status = status;
    }

    public String getUUID() {
        return UUID;
    }
}
