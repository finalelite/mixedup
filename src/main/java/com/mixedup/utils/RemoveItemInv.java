package com.mixedup.utils;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class RemoveItemInv {

    public static void removeItems(final Inventory inventory, final ItemStack item, int amount) {
        if (amount <= 0) {
            return;
        }
        final int size = inventory.getSize();
        for (int slot = 0; slot < size; slot++) {
            final ItemStack is = inventory.getItem(slot);
            if (is == null) {
                continue;
            }
            if (item.getItemMeta().getDisplayName().equals(is.getItemMeta().getDisplayName())) {
                final int newAmount = is.getAmount() - amount;
                if (newAmount > 0) {
                    is.setAmount(newAmount);
                    break;
                } else {
                    inventory.clear(slot);
                    amount = -newAmount;
                    if (amount == 0) {
                        break;
                    }
                }
            }
        }
    }

    public static void removeItemsFromType(final Inventory inventory, final ItemStack item, int amount) {
        if (amount <= 0) {
            return;
        }
        final int size = inventory.getSize();
        for (int slot = 0; slot < size; slot++) {
            final ItemStack is = inventory.getItem(slot);
            if (is == null) {
                continue;
            }
            if (item.getType().equals(is.getType())) {
                final int newAmount = is.getAmount() - amount;
                if (newAmount > 0) {
                    is.setAmount(newAmount);
                    break;
                } else {
                    inventory.clear(slot);
                    amount = -newAmount;
                    if (amount == 0) {
                        break;
                    }
                }
            }
        }
    }

    public static void removeItemDifferentLore(final Inventory inventory, final ItemStack item, int amount) {
        if (amount <= 0) {
            return;
        }

        for (int slot = 0; slot < inventory.getSize(); slot++) {
            final ItemStack is = inventory.getItem(slot);
            if (is == null) {
                continue;
            }
            if (item.getItemMeta().getLore().equals(is.getItemMeta().getLore())) {
                final int newAmount = is.getAmount() - amount;
                if (newAmount > 0) {
                    is.setAmount(newAmount);
                    break;
                } else {
                    inventory.clear(slot);
                    amount = -newAmount;
                    if (amount == 0) {
                        break;
                    }
                }
            }
        }
    }

    public static boolean removeItemDifferentLoreBoolean(final Inventory inventory, final ItemStack item, int amount) {
        if (amount <= 0) {
            return false;
        }

        for (int slot = 0; slot < inventory.getSize(); slot++) {
            final ItemStack is = inventory.getItem(slot);
            if (is == null) {
                continue;
            }
            if (item.getItemMeta().getLore().equals(is.getItemMeta().getLore())) {
                final int newAmount = is.getAmount() - amount;
                if (newAmount > 0) {
                    is.setAmount(newAmount);
                    return true;
                } else {
                    inventory.clear(slot);
                    amount = -newAmount;
                    if (amount == 0) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
