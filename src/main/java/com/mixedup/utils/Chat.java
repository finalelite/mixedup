package com.mixedup.utils;

public class Chat {

    public static boolean chatStatus;

    public static boolean getStatus() {
        return Chat.chatStatus;
    }

    public static void setStatus(final boolean status) {
        Chat.chatStatus = status;
    }
}
