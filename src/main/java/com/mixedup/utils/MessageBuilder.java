package com.mixedup.utils;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.HoverEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;
import net.minecraft.server.v1_13_R2.NBTTagCompound;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Willian Gois / zMathi
 */

public class MessageBuilder {

    private final List<BaseComponent> components = new ArrayList<>();
    private TextComponent current;

    public MessageBuilder(final String text) {
        current = new TextComponent(text);
    }

    public static MessageBuilder of(final String text) {
        return new MessageBuilder(text);
    }

    public MessageBuilder append(final String text) {
        this.components.add(this.current);
        this.current = new TextComponent(text);
        return this;
    }

    public MessageBuilder space() {
        return this.append(" ");
    }

    public MessageBuilder color(final org.bukkit.ChatColor color) {
        return this.color(ChatColor.valueOf(color.name()));
    }

    public MessageBuilder color(final ChatColor color) {
        this.current.setColor(color);
        return this;
    }

    public MessageBuilder bold() {
        return this.bold(true);
    }

    public MessageBuilder italic() {
        return this.italic(true);
    }

    public MessageBuilder obfuscated() {
        return this.obfuscated(true);
    }

    public MessageBuilder strikethrough() {
        return this.strikethrough(true);
    }

    public MessageBuilder obfuscated(final boolean obfuscated) {
        this.current.setObfuscated(obfuscated);
        return this;
    }

    public MessageBuilder italic(final boolean italic) {
        this.current.setItalic(italic);
        return this;
    }

    public MessageBuilder strikethrough(final boolean strikethrough) {
        this.current.setStrikethrough(strikethrough);
        return this;
    }

    public MessageBuilder bold(final boolean bold) {
        this.current.setBold(bold);
        return this;
    }

    public MessageBuilder hover(final String... text) {
        final BaseComponent[] texts = new BaseComponent[text.length];
        for (int i = 0; i != text.length; i++) {
            texts[i] = new TextComponent(text[i] + (i == text.length - 1 ? "" : "\n"));
        }
        this.current.setHoverEvent(new HoverEvent(Action.SHOW_TEXT, texts));
        return this;
    }

    public MessageBuilder command(final String command) {
        this.current.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, command));
        return this;
    }

    public MessageBuilder url(final String url) {
        this.current.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, url));
        return this;
    }

    public MessageBuilder item(final ItemStack item) {
        final TextComponent component = new TextComponent();
        component.setHoverEvent(new HoverEvent(Action.SHOW_ITEM, new BaseComponent[]{new TextComponent(CraftItemStack.asNMSCopy(item).save(new NBTTagCompound()).toString())}));

        this.current.addExtra(component);
        return this;
    }

    public MessageBuilder extras(final BaseComponent... extras) {
        this.current.getExtra().addAll(Arrays.asList(extras));
        return this;
    }

    public TextComponent build() {
        this.components.add(this.current);
        return new TextComponent(this.components.stream().toArray(BaseComponent[]::new));
    }

    public void send(final Player player) {
        player.spigot().sendMessage(this.build());
    }
}