package com.mixedup.utils;

import com.mixedup.Main;
import com.mixedup.apis.ServerAPI;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class SocketUtils {

    public static void online() {
        try {
            final Socket s = new Socket();
            s.connect(new InetSocketAddress(Main.ip, ServerAPI.getPort("Hub1")));
            s.close();

            ServerAPI.updateOnlineStatus("Hub1", true);
        } catch (final UnknownHostException e) {
            ServerAPI.updateOnlineStatus("Hub1", false);
        } catch (final IOException e) {
            ServerAPI.updateOnlineStatus("Hub1", false);
        }

        try {
            final Socket s = new Socket();
            s.connect(new InetSocketAddress(Main.ip, ServerAPI.getPort("Hub2")));
            s.close();

            ServerAPI.updateOnlineStatus("Hub2", true);
        } catch (final UnknownHostException e) {
            ServerAPI.updateOnlineStatus("Hub2", false);
        } catch (final IOException e) {
            ServerAPI.updateOnlineStatus("Hub2", false);
        }

        try {
            final Socket s = new Socket();
            s.connect(new InetSocketAddress(Main.ip, ServerAPI.getPort("Hub3")));
            s.close();

            ServerAPI.updateOnlineStatus("Hub3", true);
        } catch (final UnknownHostException e) {
            ServerAPI.updateOnlineStatus("Hub3", false);
        } catch (final IOException e) {
            ServerAPI.updateOnlineStatus("Hub3", false);
        }

        try {
            final Socket s = new Socket();
            s.connect(new InetSocketAddress(Main.ip, ServerAPI.getPort("Factions")));
            s.close();

            ServerAPI.updateOnlineStatus("Factions", true);
        } catch (final UnknownHostException e) {
            ServerAPI.updateOnlineStatus("Factions", false);
        } catch (final IOException e) {
            ServerAPI.updateOnlineStatus("Factions", false);
        }

        try {
            final Socket s = new Socket();
            s.connect(new InetSocketAddress(Main.ip, ServerAPI.getPort("Construir")));
            s.close();

            ServerAPI.updateOnlineStatus("Construir", true);
        } catch (final UnknownHostException e) {
            ServerAPI.updateOnlineStatus("Construir", false);
        } catch (final IOException e) {
            ServerAPI.updateOnlineStatus("Construir", false);
        }
    }
}
