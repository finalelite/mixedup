package com.mixedup.utils;

import java.util.HashMap;

public class UUIDcache {

    public static final HashMap<String, UUIDcache> CACHE = new HashMap<String, UUIDcache>();
    private String nick;
    private String UUID;

    public UUIDcache(final String nick, final String UUID) {
        this.nick = nick;
        this.UUID = UUID;
    }

    public static UUIDcache get(final String nick) {
        return UUIDcache.CACHE.get(String.valueOf(nick));
    }

    public UUIDcache insert() {
        UUIDcache.CACHE.put(String.valueOf(nick), this);

        return this;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(final String UUID) {
        this.UUID = UUID;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(final String nick) {
        this.nick = nick;
    }
}
