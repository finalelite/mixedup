package com.mixedup.utils;

import org.bukkit.Bukkit;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;
import java.util.logging.Level;

public class Skin {

    private static String uuid;
    private static String name;
    private static String value;
    private static String signatur;

    public Skin(final String uuid) {
        Skin.uuid = uuid;
        Skin.load();
    }

    private static String readURL(final String url) {
        try {
            final HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();

            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "SkinsRestorer");
            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);
            con.setDoOutput(true);

            final StringBuilder output = new StringBuilder();
            final BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                output.append(line);
            }
            in.close();

            return output.toString();
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void load() {
        try {
            // Get the name from SwordPVP
            final URL url = new URL("https://sessionserver.mojang.com/session/minecraft/profile/" + Skin.uuid + "?unsigned=false");
            final URLConnection uc = url.openConnection();
            uc.setUseCaches(false);
            uc.setDefaultUseCaches(false);
            uc.addRequestProperty("User-Agent", "Mozilla/5.0");
            uc.addRequestProperty("Cache-Control", "no-cache, no-store, must-revalidate");
            uc.addRequestProperty("Pragma", "no-cache");

            // Parse it
            final String json = new Scanner(uc.getInputStream(), "UTF-8").useDelimiter("\\A").next();
            final JSONParser parser = new JSONParser();
            final Object obj = parser.parse(json);
            final JSONArray properties = (JSONArray) ((JSONObject) obj).get("properties");
            for (int i = 0; i < properties.size(); i++) {
                try {
                    final JSONObject property = (JSONObject) properties.get(i);
                    final String name = (String) property.get("name");
                    final String value = (String) property.get("value");
                    final String signature = property.containsKey("signature") ? (String) property.get("signature") : null;

                    Skin.name = name;
                    Skin.value = value;
                    signatur = signature;
                } catch (final Exception e) {
                    Bukkit.getLogger().log(Level.WARNING, "Failed to apply auth property", e);
                }
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    public String getSkinValue() {
        return Skin.value;
    }

    public String getSkinName() {
        return Skin.name;
    }

    public String getSkinSignatur() {
        return Skin.signatur;
    }
}
