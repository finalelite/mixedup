package com.mixedup.utils;

import br.com.finalelite.pauloo27.api.PauloAPI;
import br.com.finalelite.pauloo27.api.database.PlayerRole;
import br.com.finalelite.pauloo27.api.message.MessageUtils;
import br.com.finalelite.pauloo27.api.scoreboard.ScoreboardBuilder;
import br.com.finalelite.pauloo27.api.scoreboard.ScoreboardBuilder.ScoreboardEntry;
import br.com.finalelite.pauloo27.api.scoreboard.TabListManager;
import br.com.finalelite.pauloo27.api.scoreboard.TabListManager.TabGroup;
import com.mixedup.apis.EnergiaAPI;
import com.mixedup.apis.GuildaAPI;
import com.mixedup.apis.PoderAPI;
import com.mixedup.factions.FacAPI;
import com.mixedup.hashs.PlayerVanishHash;
import com.mixedup.mcmmo.McMMOApi;
import lombok.Getter;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class ScoreboardManager {

    @Getter
    private final ScoreboardBuilder defaultScoreboard;
    @Getter
    private final ScoreboardBuilder squadScoreboard;
    @Getter
    private final TabListManager tablistManager;

    public ScoreboardManager() {
        this.defaultScoreboard = new ScoreboardBuilder("DefaultSB", "&6&lFinalElite");

        this.defaultScoreboard.addWhiteSpace();

        this.defaultScoreboard
                .addEntry(new ScoreboardEntry("group",
                        " &7Grupo: ",
                        player -> {
                            final val role = PauloAPI.getInstance().getAccountInfo(player).getRole();
                            return role.getColor() + role.getDisplayName();
                        }));

        this.defaultScoreboard
                .addEntry(new ScoreboardEntry("online",
                        " &7Online: ",
                        player -> "&a"
                                + (
                                PauloAPI.getInstance().getAccountInfo(player).getRole().isMajorStaff() ?
                                        (PlayerVanishHash.getPlayers().size() != 0 ?
                                                Bukkit.getOnlinePlayers().size() - PlayerVanishHash.getPlayers().size() + " (" + Bukkit.getOnlinePlayers().size() + ")" :
                                                Bukkit.getOnlinePlayers().size()) :
                                        Bukkit.getOnlinePlayers().size() - PlayerVanishHash.getPlayers().size()

                        )));
        this.defaultScoreboard.addWhiteSpace();

        this.defaultScoreboard
                .addEntry(new ScoreboardEntry("coins",
                        " &7Moeda: ",
                        player -> "&f$&a" +
                                MessageUtils.formatDecimalToEuropeanLocale(PauloAPI.getInstance().getEconomyManager().getPlayerEconomy(player).getCoins())));

        this.defaultScoreboard
                .addEntry(new ScoreboardEntry("blackcoins",
                        " &7Moeda negra: ",
                        player -> "&f$&a" +
                                MessageUtils.formatDecimalToEuropeanLocale(PauloAPI.getInstance().getEconomyManager().getPlayerEconomy(player).getBlackCoins())));

        this.defaultScoreboard.addWhiteSpace();

        this.defaultScoreboard
                .addEntry(new ScoreboardEntry("power",
                        " &7Energia: ",
                        player -> "&c" +
                                MessageUtils.formatDecimalToEuropeanLocale(EnergiaAPI.getEnergia(player.getUniqueId().toString())) + " kW"
                ));

        this.defaultScoreboard.addWhiteSpace();

        this.defaultScoreboard
                .addEntry(new ScoreboardEntry("squad",
                        " &7Esquadrão: ",
                        player -> {
                            if (FacAPI.getFacNome(player.getUniqueId().toString()) == null) {
                                return "&8Sem Esquadrão";
                            } else {
                                return "&e[" + FacAPI.getTagWithNome(FacAPI.getFacNome(player.getUniqueId().toString())) + "]";
                            }
                        }
                ));

        this.defaultScoreboard.addWhiteSpace();

        this.defaultScoreboard
                .addEntry(new ScoreboardEntry("guild",
                        " &7Guilda: ",
                        player -> {
                            if (GuildaAPI.hasGuilda(player.getUniqueId().toString())) {
                                return "&a" + PauloAPI.getInstance().getAccountInfo(player).getGuild().getDisplayName();
                            } else {
                                return "&8Sem Guilda";
                            }
                        }
                ));

        this.defaultScoreboard
                .addEntry(new ScoreboardEntry("guild_rank",
                        " &7Guilda Rank: ",
                        player -> {
                            if (GuildaAPI.hasGuilda(player.getUniqueId().toString())) {
                                return "&a" + GuildaAPI.getGuildaRank(player.getUniqueId().toString());
                            } else {
                                return "&80";
                            }
                        }
                ));

        this.defaultScoreboard.addWhiteSpace();
        this.defaultScoreboard
                .addEntry(new ScoreboardEntry("site",
                        "&5   www.finalelite.com.br",
                        player -> ""
                ));

        this.squadScoreboard = new ScoreboardBuilder("TrappistSB", "&2Zona Livre");

        this.squadScoreboard.addWhiteSpace();

        this.squadScoreboard
                .addEntry(new ScoreboardEntry("group",
                        " &7Grupo: ",
                        player -> {
                            final val role = PauloAPI.getInstance().getAccountInfo(player).getRole();
                            return role.getColor() + role.getDisplayName();
                        }));

        this.squadScoreboard
                .addEntry(new ScoreboardEntry("mcmmo",
                        " &7McMMO: ",
                        player -> "&a" + McMMOApi.getTotalLevel(player.getUniqueId().toString())));

        this.squadScoreboard
                .addEntry(new ScoreboardEntry("user_power",
                        " &7Poder: ",
                        player -> "&a" + PoderAPI.getPoder(player.getUniqueId().toString()) + "/15"));

        this.squadScoreboard.addWhiteSpace();

        this.squadScoreboard
                .addEntry(new ScoreboardEntry("squad_name",
                        "  ",
                        player -> {
                            final val facName = FacAPI.getFacNome(player.getUniqueId().toString());
                            return facName == null ? "&8Sem Esquadrão." : "&a" + facName;
                        }));

        this.squadScoreboard
                .addEntry(new ScoreboardEntry("squad_online",
                        "   &7Online: ",
                        player -> {
                            final val facName = FacAPI.getFacNome(player.getUniqueId().toString());
                            return facName == null ? "&8&o--" : "&a" + FacAPI.getFacOnline(player.getUniqueId().toString());
                        }));

        this.squadScoreboard
                .addEntry(new ScoreboardEntry("squad_power",
                        "   &7Poder: ",
                        player -> {
                            final val facName = FacAPI.getFacNome(player.getUniqueId().toString());
                            return facName == null ? "&8&o--" : "&a" + PoderAPI.getTotalPoder(player.getUniqueId().toString());
                        }));

        this.squadScoreboard
                .addEntry(new ScoreboardEntry("squad_lands",
                        "   &7Terras: ",
                        player -> {
                            final val facName = FacAPI.getFacNome(player.getUniqueId().toString());
                            return facName == null ? "&8&o--" : "&a" + FacAPI.getTerras(facName);
                        }));

        this.squadScoreboard.addWhiteSpace();

        this.squadScoreboard
                .addEntry(new ScoreboardEntry("coins",
                        " &7Moeda: ",
                        player -> "&f$&a" +
                                MessageUtils.formatDecimalToEuropeanLocale(PauloAPI.getInstance().getEconomyManager().getPlayerEconomy(player).getCoins())));

        this.squadScoreboard
                .addEntry(new ScoreboardEntry("blackcoins",
                        " &7Moeda negra: ",
                        player -> "&f$&a" +
                                MessageUtils.formatDecimalToEuropeanLocale(PauloAPI.getInstance().getEconomyManager().getPlayerEconomy(player).getBlackCoins())));

        this.squadScoreboard.addWhiteSpace();

        this.squadScoreboard.addEntry(new ScoreboardEntry("site",
                "&5   www.finalelite.com.br",
                player -> ""
        ));

        this.tablistManager = new TabListManager();
        Arrays.stream(PlayerRole.values()).forEach(role -> {
            final val color = ChatColor.getByChar(role.getColor().replace("&", "").charAt(0));
            this.tablistManager
                    .addGroup(new TabGroup(color,
                            (role == PlayerRole.MEMBRO ? color.toString() : String.format("%s[%s] ", color, role.getDisplayName())),
                            role.getDisplayName(),
                            null));
        });
    }

    public void createScoreboard(final Player player) {
        final val scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        player.setScoreboard(scoreboard);
    }

    public void setDefaultScoreboard(final Player player) {
        defaultScoreboard.setScoreboard(player);
    }

    public void setTabList(final Player player) {
        tablistManager.setTabListToPlayer(player);
    }

    public void updateTabList(final Player player) {
        Bukkit.getOnlinePlayers().forEach(target -> this.tablistManager.updatePlayerGroup(player, target));
    }

    public void setSquadScoreboard(final Player player) {
        squadScoreboard.setScoreboard(player);
    }

    public boolean hasSquadScoreboard(final Player player) {
        return player.getWorld().getName().equalsIgnoreCase("Trappist-1b");
    }

    public void updateSquadScoreboardTitle(final Player player) {
        if (!this.hasSquadScoreboard(player) || !this.squadScoreboard.isScoreboardEnabled(player))
            return;

        final val objective = player.getScoreboard().getObjective("TrappistSB");
        final String chunk = player.getChunk().getX() + ":" + player.getChunk().getZ();
        if (FacAPI.getFacNome(player.getUniqueId().toString()) != null && FacAPI.getSobAttack(FacAPI.getFacNome(player.getUniqueId().toString())) && objective != null) {
            objective.setDisplayName(AlternateColor.alternate("&4&lSob ataque"));
        } else if (FacAPI.getChunkOwn(chunk) == null && objective != null) {
            objective.setDisplayName(AlternateColor.alternate("&2&lZona livre"));
        } else {
            if (objective != null) {
                objective.setDisplayName(AlternateColor.alternate("&6&lZona protegida"));
            }
        }
    }

    public boolean toggleScoreboardToPlayer(final Player player) {
        final val result = this.defaultScoreboard.toggleScoreboardToPlayer(player);
        this.squadScoreboard.toggleScoreboardToPlayer(player);

        if (this.hasSquadScoreboard(player))
            this.squadScoreboard.setScoreboard(player);
        else
            this.defaultScoreboard.setScoreboard(player);

        return result;
    }


}
