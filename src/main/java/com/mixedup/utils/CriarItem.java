package com.mixedup.utils;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CriarItem {

    public static ItemStack add(final Material m) {
        return new ItemStack(m);
    }

    public static ItemStack add(final Material m, final int quantidade) {
        return new ItemStack(m, quantidade);
    }

    public static ItemStack add(final Material m, final int quantidade, final int durabilidade) {
        final ItemStack item = new ItemStack(m, quantidade);
        item.setDurability((short) durabilidade);
        final ItemMeta meta = item.getItemMeta();
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack add(final Material m, final String nome) {
        final ItemStack item = new ItemStack(m);
        final ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(nome);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack add1(final Material m, final String nome, final int quantidade) {
        final ItemStack item = new ItemStack(m, quantidade);
        final ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(nome);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack add1(final Material m, final String nome, final int quantidade, final int durabilidade) {
        final ItemStack item = new ItemStack(m, quantidade);
        item.setDurability((short) durabilidade);
        final ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(nome);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack add(final Material m, final String nome, final Enchantment ench, final int level) {
        final ItemStack item = new ItemStack(m);
        item.addUnsafeEnchantment(ench, level);
        final ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(nome);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack add(final Material m, final String nome, final Enchantment ench, final int level, final int quantidade) {
        final ItemStack item = new ItemStack(m, quantidade);
        item.addUnsafeEnchantment(ench, level);
        final ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(nome);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack add(final Material m, final String nome, final int durability) {
        final ItemStack item = new ItemStack(m);
        item.setDurability((short) durability);
        final ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(nome);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack add(final Material m, final String nome, final int durability, final String[] lore) {
        final ItemStack item = new ItemStack(m);
        item.setDurability((short) durability);
        final ItemMeta meta = item.getItemMeta();
        meta.setLore(Arrays.asList(lore));
        meta.setDisplayName(nome);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack add(final Material m, final String nome, final int quantidade, final String nada) {
        final ItemStack item = new ItemStack(m);
        item.setAmount(quantidade);
        final ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(nome);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack add(final Material m, final String nome, final String[] lore, final int durability) {
        final ItemStack item = new ItemStack(m);
        item.setDurability((short) durability);
        final ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(nome);
        meta.setLore(Arrays.asList(lore));
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack add(final Material m, final String nome, final String[] lore) {
        final ItemStack item = new ItemStack(m);
        final ItemMeta meta = item.getItemMeta();
        meta.setLore(Arrays.asList(lore));
        meta.setDisplayName(nome);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack add(final Material m, final String nome, final String[] lore, final Enchantment enchant, final int lvl) {
        final ItemStack item = new ItemStack(m);
        item.addUnsafeEnchantment(enchant, lvl);
        final ItemMeta meta = item.getItemMeta();
        meta.setLore(Arrays.asList(lore));
        meta.setDisplayName(nome);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack add(final Material m, final String nome, final String[] lore, final Enchantment enchant1, final int lvl1,
                                final Enchantment enchant2, final int lvl2) {
        final ItemStack item = new ItemStack(m);
        item.addUnsafeEnchantment(enchant1, lvl1);
        item.addUnsafeEnchantment(enchant2, lvl2);
        final ItemMeta meta = item.getItemMeta();
        meta.setLore(Arrays.asList(lore));
        meta.setDisplayName(nome);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack add(final Material m, final String nome, final Enchantment enchant1, final int lvl1, final Enchantment enchant2,
                                final int lvl2) {
        final ItemStack item = new ItemStack(m);
        item.addUnsafeEnchantment(enchant1, lvl1);
        item.addUnsafeEnchantment(enchant2, lvl2);
        final ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(nome);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack add(final Material m, final String nome, final Enchantment enchant1, final int lvl1, final Enchantment enchant2,
                                final int lvl2, final Enchantment enchant3, final int lvl3) {
        final ItemStack item = new ItemStack(m);
        item.addUnsafeEnchantment(enchant1, lvl1);
        item.addUnsafeEnchantment(enchant2, lvl2);
        item.addUnsafeEnchantment(enchant3, lvl3);
        final ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(nome);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack add(final Material m, final String nome, final Enchantment enchant1, final int lvl1, final Enchantment enchant2,
                                final int lvl2, final Enchantment enchant3, final int lvl3, final Enchantment enchant4, final int lvl4) {
        final ItemStack item = new ItemStack(m);
        item.addUnsafeEnchantment(enchant1, lvl1);
        item.addUnsafeEnchantment(enchant2, lvl2);
        item.addUnsafeEnchantment(enchant3, lvl3);
        item.addUnsafeEnchantment(enchant4, lvl4);
        final ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(nome);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack add(final Material m, final String nome, final String[] lore, final Enchantment enchant1, final int lvl1,
                                final Enchantment enchant2, final int lvl2, final Enchantment enchant3, final int lvl3) {
        final ItemStack item = new ItemStack(m);
        item.addUnsafeEnchantment(enchant1, lvl1);
        item.addUnsafeEnchantment(enchant2, lvl2);
        item.addUnsafeEnchantment(enchant3, lvl3);
        final ItemMeta meta = item.getItemMeta();
        meta.setLore(Arrays.asList(lore));
        meta.setDisplayName(nome);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack potion(final PotionEffectType efeito, final String nome, final String[] lore) {
        final ItemStack item = new ItemStack(Material.POTION);
        final PotionMeta eta = (PotionMeta) item.getItemMeta();
        eta.addCustomEffect(new PotionEffect(PotionEffectType.SPEED, 3600, 9), true);
        eta.setLore(Arrays.asList(lore));
        eta.setDisplayName(nome);
        item.setItemMeta(eta);
        return item;
    }

    public static ItemStack couro() {
        final ItemStack item = new ItemStack(Material.LEATHER_CHESTPLATE);
        item.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
        final LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
        meta.setColor(Color.BLUE);
        meta.setDisplayName("ï¿½bArmadura de Couro");
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack couro(final Color cor, final String nome) {
        final ItemStack item = new ItemStack(Material.LEATHER_CHESTPLATE);
        item.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
        final LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
        meta.setColor(cor);
        meta.setDisplayName(nome);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack outro(final Material m, final String nome, final Enchantment enchant, final int lvl) {
        final ItemStack item = new ItemStack(m);
        item.addUnsafeEnchantment(enchant, lvl);
        final ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(nome);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack outro(final Material m, final String nome, final Enchantment enchant1, final int lvl1, final Enchantment enchant2,
                                  final int lvl2) {
        final ItemStack item = new ItemStack(m);
        item.addUnsafeEnchantment(enchant1, lvl1);
        item.addUnsafeEnchantment(enchant2, lvl2);
        final ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(nome);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack outro(final Material m, final String nome, final Enchantment enchant1, final int lvl1, final Enchantment enchant2,
                                  final int lvl2, final Enchantment enchant3, final int lvl3) {
        final ItemStack item = new ItemStack(m);
        item.addUnsafeEnchantment(enchant1, lvl1);
        item.addUnsafeEnchantment(enchant2, lvl2);
        item.addUnsafeEnchantment(enchant3, lvl3);
        final ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(nome);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack getSpawner(final String nome, final int amount, final EntityType type) {
        final ItemStack item = new ItemStack(Material.SPAWNER, amount);
        final List<String> lore = new ArrayList<String>();

        String loreString = type.toString();
        loreString = loreString.substring(0, 1).toUpperCase() + loreString.substring(1).toLowerCase();
        loreString = loreString + " Spawner";
        lore.add(loreString);

        final ItemMeta meta = item.getItemMeta();
        meta.setLore(lore);
        meta.setDisplayName(nome);
        item.setItemMeta(meta);

        return item;
    }

    public static ItemStack cap(final Material m, final int durabilidade) {
        final ItemStack item = new ItemStack(m);
        item.setDurability((short) durabilidade);
        return item;
    }
}
