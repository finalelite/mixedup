package com.mixedup.utils;

import java.util.HashMap;

public class PlayerInArea {

    private static final HashMap<String, PlayerInArea> CACHE = new HashMap<String, PlayerInArea>();
    private String UUID;
    private int rotacao;
    private boolean status;

    public PlayerInArea(final String UUID, final int rotacao, final boolean status) {
        this.UUID = UUID;
        this.rotacao = rotacao;
        this.status = status;
    }

    public static PlayerInArea get(final String UUID) {
        return PlayerInArea.CACHE.get(String.valueOf(UUID));
    }

    public PlayerInArea insert() {
        PlayerInArea.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(final String UUID) {
        this.UUID = UUID;

    }

    public int getRotacao() {
        return rotacao;
    }

    public void setRotacao(final int rotacao) {
        this.rotacao = rotacao;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(final boolean status) {
        this.status = status;
    }
}
