package com.mixedup.rpg;

import com.mixedup.Main;
import com.mixedup.apis.GuildaAPI;
import com.mixedup.mcmmo.ProbabilityApi;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Random;

public class MobdropMoedas implements Listener {

    public static void removeItem(final Inventory inventory, final int i) {
        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
            @Override
            public void run() {
                for (int x = 0; x <= inventory.getSize(); x++) {
                    if (i != x && inventory.getItem(x).getType().equals(Material.CARROT_ON_A_STICK)) {
                        inventory.getItem(x).setType(Material.AIR);
                        inventory.getItem(x).setAmount(0);
                        break;
                    }
                }
            }
        }, 2L);
    }

    @EventHandler
    public void onDeath(final EntityDeathEvent event) {
        if (event.getEntityType().equals(EntityType.ZOMBIE) || event.getEntityType().equals(EntityType.SKELETON)) {
            if (event.getEntity().getKiller() instanceof Player) {
                final Player player = event.getEntity().getKiller();
                final String UUID = player.getUniqueId().toString();

                if (ProbabilityApi.probab(5000 + (GuildaAPI.getGuildaRank(UUID) * 2000)) == true) {
                    final Random random = new Random();
                    int r = random.nextInt(2);
                    if (r == 0) {
                        r = 1;
                    }
                    final ItemStack moeda = new ItemStack(Material.CARROT_ON_A_STICK);
                    final ItemMeta meta = moeda.getItemMeta();
                    meta.setDisplayName(ChatColor.YELLOW + "Saco de moedas");
                    final ArrayList<String> lore = new ArrayList<>();
                    lore.add(" ");
                    lore.add(ChatColor.DARK_GRAY + "Moeda(s): " + ChatColor.GRAY + r);
                    lore.add(" ");
                    meta.setLore(lore);
                    moeda.setItemMeta(meta);
                    event.getEntity().getLocation().getWorld().dropItemNaturally(event.getEntity().getLocation(), moeda);
                }
            }
        }
    }

    @EventHandler
    public void onGetItemOnTheFloor(final PlayerPickupItemEvent event) {
        final Player player = event.getPlayer();
        final Inventory inventory = player.getInventory();

        if (event.getItem().getItemStack().getType().equals(Material.CARROT_ON_A_STICK)) {
            for (int i = 0; i <= inventory.getSize(); i++) {
                if (inventory.getItem(i) != null) {
                    if (inventory.getItem(i).getType().equals(Material.CARROT_ON_A_STICK)) {
                        if (inventory.getItem(i).getItemMeta().getLore().get(1).contains("Moeda(s): ") && event.getItem().getItemStack().getItemMeta().getLore().get(1).contains("Moeda(s): ")) {
                            event.setCancelled(true);
                            try {
                                final int quantia = Integer.valueOf(inventory.getItem(i).getItemMeta().getLore().get(1).substring(14));
                                final int quantiaAdd = Integer.valueOf(event.getItem().getItemStack().getItemMeta().getLore().get(1).substring(14));
                                final ArrayList<String> lore = new ArrayList<>();
                                lore.add(" ");
                                lore.add(ChatColor.DARK_GRAY + "Moeda(s): " + ChatColor.GRAY + (quantia + quantiaAdd));
                                lore.add(" ");
                                final ItemMeta meta = inventory.getItem(i).getItemMeta();
                                meta.setLore(lore);
                                inventory.getItem(i).setItemMeta(meta);
                                event.getItem().remove();
                                //removeItem(inventory, i);
                                break;
                            } catch (final NumberFormatException e) {
                                //-
                            }
                        }
                    }
                }
            }
        }
    }
}
