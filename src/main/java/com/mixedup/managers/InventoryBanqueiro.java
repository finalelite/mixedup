package com.mixedup.managers;

import com.mixedup.economy.CoinsAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class InventoryBanqueiro {

    private static final DecimalFormat formatter = new DecimalFormat("#,###.00");

    public static Inventory getInventory(final String UUID) {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Banqueiro: ");

        final ItemStack vazio = new ItemStack(Material.GRAY_STAINED_GLASS_PANE);
        final ItemMeta meta = vazio.getItemMeta();
        meta.setDisplayName(" ");
        vazio.setItemMeta(meta);

        for (int i = 0; i <= 18; i++) {
            if (i <= 9) {
                inventory.setItem(i, vazio);
            } else if (i >= 10) {
                inventory.setItem(i + 8, vazio);
            }
        }
        inventory.setItem(11, vazio);
        inventory.setItem(13, vazio);
        inventory.setItem(17, vazio);

        final ItemStack values = new ItemStack(Material.MUSIC_DISC_FAR);
        final ItemMeta metaValues = values.getItemMeta();
        metaValues.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        metaValues.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        metaValues.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        metaValues.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        metaValues.addItemFlags(ItemFlag.HIDE_DESTROYS);
        metaValues.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        metaValues.setDisplayName(ChatColor.YELLOW + "Saldo atual");
        final ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        final int blackCoins = CoinsAPI.getBlackCoins(UUID);
        if (blackCoins == 0) {
            lore.add(ChatColor.GRAY + " $0");
        } else {
            lore.add(ChatColor.GRAY + " $" + InventoryBanqueiro.formatter.format(CoinsAPI.getBlackCoins(UUID)));
        }
        lore.add(" ");
        metaValues.setLore(lore);
        values.setItemMeta(metaValues);
        inventory.setItem(10, values);

        final ItemStack negativar = new ItemStack(Material.MUSIC_DISC_MALL);
        final ItemMeta metaNega = negativar.getItemMeta();
        metaNega.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        metaNega.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        metaNega.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        metaNega.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        metaNega.addItemFlags(ItemFlag.HIDE_DESTROYS);
        metaNega.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        metaNega.setDisplayName(ChatColor.RED + "Sacar");
        negativar.setItemMeta(metaNega);
        inventory.setItem(14, negativar);

        final ItemStack depositar = new ItemStack(Material.MUSIC_DISC_CHIRP);
        final ItemMeta metaPosi = depositar.getItemMeta();
        metaPosi.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        metaPosi.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        metaPosi.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        metaPosi.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        metaPosi.addItemFlags(ItemFlag.HIDE_DESTROYS);
        metaPosi.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        metaPosi.setDisplayName(ChatColor.GREEN + "Depositar");
        depositar.setItemMeta(metaPosi);
        inventory.setItem(15, depositar);

        final ItemStack cancelar = new ItemStack(Material.MUSIC_DISC_MELLOHI);
        final ItemMeta metaCancel = cancelar.getItemMeta();
        metaCancel.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        metaCancel.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        metaCancel.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        metaCancel.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        metaCancel.addItemFlags(ItemFlag.HIDE_DESTROYS);
        metaCancel.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        metaCancel.setDisplayName(ChatColor.RED + "Cancelar");
        cancelar.setItemMeta(metaCancel);
        inventory.setItem(16, cancelar);

        return inventory;
    }
}
