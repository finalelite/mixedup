package com.mixedup.managers;

import com.mixedup.apis.GuildaAPI;
import net.minecraft.server.v1_13_R2.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class InventoryElytra {

    public static Inventory firstInv() {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Ferreiro de asas: ");

        final ItemStack elytra = new ItemStack(Material.ELYTRA);
        final ItemMeta metaElytra = elytra.getItemMeta();
        metaElytra.setDisplayName(ChatColor.YELLOW + "Criar elytra");
        elytra.setItemMeta(metaElytra);

        final ItemStack upgrade = new ItemStack(Material.PHANTOM_MEMBRANE);
        final ItemMeta metaUp = upgrade.getItemMeta();
        metaUp.setDisplayName(ChatColor.YELLOW + "Upar elytra");
        upgrade.setItemMeta(metaUp);

        inventory.setItem(12, elytra);
        inventory.setItem(14, upgrade);

        return inventory;
    }

    public static Inventory buyElytra(final String UUID) {
        final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Criar asa: ");

        final ItemStack vazio = new ItemStack(Material.GRAY_STAINED_GLASS_PANE);
        final ItemMeta meta = vazio.getItemMeta();
        meta.setDisplayName(" ");
        vazio.setItemMeta(meta);

        for (int i = 1; i <= 54; i++) {
            if (i - 1 == 10 || i - 1 == 11 || i - 1 == 12 || i - 1 == 13 || i - 1 == 14 || i - 1 == 19 || i - 1 == 20 || i - 1 == 21 || i - 1 == 22 || i - 1 == 23 || i - 1 == 28 || i - 1 == 29 || i - 1 == 30 || i - 1 == 31 || i - 1 == 32 || i - 1 == 37 || i - 1 == 38 || i - 1 == 39 || i - 1 == 40 || i - 1 == 41 || i - 1 == 16 || i - 1 == 34 || i - 1 == 43) {
                continue;
            } else {
                inventory.setItem(i - 1, vazio);
            }
        }

        ItemStack elytra = new ItemStack(Material.ELYTRA);
        final ItemMeta metaElytra = elytra.getItemMeta();
        if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("sanguinaria")) {
            metaElytra.setDisplayName("Asa Sanguinaria [Nivel 1]");
            metaElytra.addEnchant(Enchantment.PROTECTION_FIRE, 1, true);
        } else if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("ancia")) {
            metaElytra.setDisplayName("Asa Ancia [Nivel 1]");
            metaElytra.addEnchant(Enchantment.PROTECTION_PROJECTILE, 1, true);
        } else if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("nobre")) {
            metaElytra.setDisplayName("Asa Nobre [Nivel 1]");
            metaElytra.addEnchant(Enchantment.PROTECTION_EXPLOSIONS, 1, true);
        }
        metaElytra.addEnchant(Enchantment.PROTECTION_FALL, 1, true);
        metaElytra.addEnchant(Enchantment.DURABILITY, 1, true);
        metaElytra.addEnchant(Enchantment.THORNS, 1, true);
        metaElytra.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
        elytra.setItemMeta(metaElytra);

        final net.minecraft.server.v1_13_R2.ItemStack nmsStack = CraftItemStack.asNMSCopy(elytra);
        final NBTTagCompound compound = (nmsStack.hasTag()) ? nmsStack.getTag() : new NBTTagCompound();
        final NBTTagList modifiers = new NBTTagList();
        final NBTTagCompound speed = new NBTTagCompound();
        final NBTTagCompound maxHealth = new NBTTagCompound();
        final NBTTagCompound attackDamage = new NBTTagCompound();
        final NBTTagCompound armor = new NBTTagCompound();

        speed.set("AttributeName", new NBTTagString("generic.movementSpeed"));
        speed.set("Name", new NBTTagString("generic.movementSpeed"));
        speed.set("Amount", new NBTTagDouble(0.1));
        speed.set("Operation", new NBTTagInt(1));
        speed.set("UUIDLeast", new NBTTagInt(894654));
        speed.set("UUIDMost", new NBTTagInt(2872));
        speed.set("Slot", new NBTTagString("chest"));

        maxHealth.set("AttributeName", new NBTTagString("generic.maxHealth"));
        maxHealth.set("Name", new NBTTagString("generic.maxHealth"));
        maxHealth.set("Amount", new NBTTagDouble(0.1));
        maxHealth.set("Operation", new NBTTagInt(1));
        maxHealth.set("UUIDLeast", new NBTTagInt(894654));
        maxHealth.set("UUIDMost", new NBTTagInt(2872));
        maxHealth.set("Slot", new NBTTagString("chest"));

        attackDamage.set("AttributeName", new NBTTagString("generic.attackDamage"));
        attackDamage.set("Name", new NBTTagString("generic.attackDamage"));
        attackDamage.set("Amount", new NBTTagDouble(0.1));
        attackDamage.set("Operation", new NBTTagInt(1));
        attackDamage.set("UUIDLeast", new NBTTagInt(894654));
        attackDamage.set("UUIDMost", new NBTTagInt(2872));
        attackDamage.set("Slot", new NBTTagString("chest"));

        armor.set("AttributeName", new NBTTagString("generic.armor"));
        armor.set("Name", new NBTTagString("generic.armor"));
        armor.set("Amount", new NBTTagDouble(0.1));
        armor.set("Operation", new NBTTagInt(1));
        armor.set("UUIDLeast", new NBTTagInt(894654));
        armor.set("UUIDMost", new NBTTagInt(2872));
        armor.set("Slot", new NBTTagString("chest"));

        modifiers.add(speed);
        modifiers.add(maxHealth);
        modifiers.add(attackDamage);
        modifiers.add(armor);
        compound.set("AttributeModifiers", modifiers);
        nmsStack.setTag(compound);
        elytra = CraftItemStack.asBukkitCopy(nmsStack);

        inventory.setItem(16, elytra);

        final ItemStack cancel = new ItemStack(Material.MUSIC_DISC_MELLOHI);
        final ItemMeta metaCancel = cancel.getItemMeta();
        metaCancel.setDisplayName(ChatColor.RED + "Cancelar");
        metaCancel.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        metaCancel.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        metaCancel.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        metaCancel.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        metaCancel.addItemFlags(ItemFlag.HIDE_DESTROYS);
        metaCancel.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        cancel.setItemMeta(metaCancel);
        inventory.setItem(34, cancel);

        final ItemStack confirm = new ItemStack(Material.MUSIC_DISC_CHIRP);
        final ItemMeta metaConfirm = confirm.getItemMeta();
        metaConfirm.setDisplayName(ChatColor.GREEN + "Confirmar");
        metaConfirm.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        metaConfirm.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        metaConfirm.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        metaConfirm.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        metaConfirm.addItemFlags(ItemFlag.HIDE_DESTROYS);
        metaConfirm.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        confirm.setItemMeta(metaConfirm);
        inventory.setItem(43, confirm);

        final ItemStack frag = new ItemStack(Material.FEATHER);
        final ItemMeta metaFrag = frag.getItemMeta();
        metaFrag.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
        final ArrayList<String> lore1 = new ArrayList<>();
        lore1.add(" ");
        lore1.add(ChatColor.GRAY + "Fragmento para asa, tipo: 1");
        lore1.add(" ");
        metaFrag.setLore(lore1);
        frag.setItemMeta(metaFrag);

        final ItemStack frag2 = new ItemStack(Material.FEATHER);
        final ItemMeta metaFrag2 = frag2.getItemMeta();
        metaFrag2.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
        final ArrayList<String> lore2 = new ArrayList<>();
        lore2.add(" ");
        lore2.add(ChatColor.GRAY + "Fragmento para asa, tipo: 2");
        lore2.add(" ");
        metaFrag2.setLore(lore2);
        frag2.setItemMeta(metaFrag2);

        final ItemStack fragN = new ItemStack(Material.PHANTOM_MEMBRANE);
        final ItemMeta metaFragN = fragN.getItemMeta();
        metaFragN.setDisplayName(ChatColor.GREEN + "Fragmento de nível");
        final ArrayList<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Fragmento de nível p/ asa, nível: 1");
        lore3.add(" ");
        metaFragN.setLore(lore3);
        fragN.setItemMeta(metaFragN);

        final ItemStack paper = new ItemStack(Material.PAPER);
        final ItemMeta metaPaper = paper.getItemMeta();
        metaPaper.setDisplayName(ChatColor.GREEN + "Faltam");
        final ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");

        if (100 - InventoryElytra.getQuantityFragAsa(frag, frag2, inventory) <= 0) {
            lore.add(ChatColor.WHITE + "Fragmento de asa: " + ChatColor.GRAY + "0");
        } else {
            lore.add(ChatColor.WHITE + "Fragmento de asa: " + ChatColor.GRAY + (100 - InventoryElytra.getQuantityFragAsa(frag, frag2, inventory)));
        }
        if (15 - InventoryElytra.getQuantity(fragN, inventory) <= 0) {
            lore.add(ChatColor.WHITE + "Fragmento de nível: " + ChatColor.GRAY + "0");
        } else {
            lore.add(ChatColor.WHITE + "Fragmento de nível: " + ChatColor.GRAY + (15 - InventoryElytra.getQuantity(fragN, inventory)));
        }
        lore.add(" ");
        metaPaper.setLore(lore);
        paper.setItemMeta(metaPaper);
        inventory.setItem(48, paper);

        return inventory;
    }

    public static Inventory upgradeElytra(final String UUID) {
        final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Upar asa: ");

        final ItemStack vazio = new ItemStack(Material.GRAY_STAINED_GLASS_PANE);
        final ItemMeta meta = vazio.getItemMeta();
        meta.setDisplayName(" ");
        vazio.setItemMeta(meta);

        for (int i = 1; i <= 54; i++) {
            if (i - 1 == 10 || i - 1 == 11 || i - 1 == 12 || i - 1 == 13 || i - 1 == 14 || i - 1 == 19 || i - 1 == 20 || i - 1 == 21 || i - 1 == 22 || i - 1 == 23 || i - 1 == 28 || i - 1 == 29 || i - 1 == 30 || i - 1 == 31 || i - 1 == 32 || i - 1 == 37 || i - 1 == 38 || i - 1 == 39 || i - 1 == 40 || i - 1 == 41 || i - 1 == 16 || i - 1 == 34 || i - 1 == 43) {
                continue;
            } else {
                inventory.setItem(i - 1, vazio);
            }
        }

        final ItemStack elytra = new ItemStack(Material.ELYTRA);
        final ItemMeta metaElytra = elytra.getItemMeta();
        if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("sanguinaria")) {
            metaElytra.setDisplayName("Asa Sanguinaria [Nivel ?]");
        } else if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("ancia")) {
            metaElytra.setDisplayName("Asa Ancia [Nivel ?]");
        } else if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("nobre")) {
            metaElytra.setDisplayName("Asa Nobre [Nivel ?]");
        }
        elytra.setItemMeta(metaElytra);
        inventory.setItem(16, elytra);

        final ItemStack cancel = new ItemStack(Material.MUSIC_DISC_MELLOHI);
        final ItemMeta metaCancel = cancel.getItemMeta();
        metaCancel.setDisplayName(ChatColor.RED + "Cancelar");
        metaCancel.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        metaCancel.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        metaCancel.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        metaCancel.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        metaCancel.addItemFlags(ItemFlag.HIDE_DESTROYS);
        metaCancel.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        cancel.setItemMeta(metaCancel);
        inventory.setItem(34, cancel);

        final ItemStack confirm = new ItemStack(Material.MUSIC_DISC_CHIRP);
        final ItemMeta metaConfirm = confirm.getItemMeta();
        metaConfirm.setDisplayName(ChatColor.GREEN + "Confirmar");
        metaConfirm.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        metaConfirm.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        metaConfirm.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        metaConfirm.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        metaConfirm.addItemFlags(ItemFlag.HIDE_DESTROYS);
        metaConfirm.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        confirm.setItemMeta(metaConfirm);
        inventory.setItem(43, confirm);

        final ItemStack frag = new ItemStack(Material.FEATHER);
        final ItemMeta metaFrag = frag.getItemMeta();
        metaFrag.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
        final ArrayList<String> lore1 = new ArrayList<>();
        lore1.add(" ");
        lore1.add(ChatColor.GRAY + "Fragmento para asa, tipo: 1");
        lore1.add(" ");
        metaFrag.setLore(lore1);
        frag.setItemMeta(metaFrag);

        final ItemStack frag2 = new ItemStack(Material.FEATHER);
        final ItemMeta metaFrag2 = frag2.getItemMeta();
        metaFrag2.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
        final ArrayList<String> lore2 = new ArrayList<>();
        lore2.add(" ");
        lore2.add(ChatColor.GRAY + "Fragmento para asa, tipo: 2");
        lore2.add(" ");
        metaFrag2.setLore(lore2);
        frag2.setItemMeta(metaFrag2);

        final ItemStack fragN = new ItemStack(Material.PHANTOM_MEMBRANE);
        final ItemMeta metaFragN = fragN.getItemMeta();
        metaFragN.setDisplayName(ChatColor.GREEN + "Fragmento de nível");
        final ArrayList<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Fragmento de nível p/ asa, nível: 1");
        lore3.add(" ");
        metaFragN.setLore(lore3);
        fragN.setItemMeta(metaFragN);

        final ItemStack paper = new ItemStack(Material.PAPER);
        final ItemMeta metaPaper = paper.getItemMeta();
        metaPaper.setDisplayName(ChatColor.GREEN + "Faltam");
        final ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.WHITE + "Asa: " + ChatColor.GRAY + "Nenhuma");
        lore.add(ChatColor.WHITE + "Fragmento de asa: " + ChatColor.GRAY + "0");
        lore.add(ChatColor.WHITE + "Fragmento de nível: " + ChatColor.GRAY + "0");
        lore.add(" ");
        metaPaper.setLore(lore);
        paper.setItemMeta(metaPaper);
        inventory.setItem(48, paper);

        return inventory;
    }

    public static ItemStack getElytra(final String UUID, final int nivel) {
        ItemStack elytra = new ItemStack(Material.ELYTRA);
        final ItemMeta metaElytra = elytra.getItemMeta();
        if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("sanguinaria")) {
            metaElytra.setDisplayName("Asa Sanguinaria [Nivel " + nivel + "]");
            metaElytra.addEnchant(Enchantment.PROTECTION_FIRE, 1, true);
        } else if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("ancia")) {
            metaElytra.setDisplayName("Asa Ancia [Nivel " + nivel + "]");
            metaElytra.addEnchant(Enchantment.PROTECTION_PROJECTILE, 1, true);
        } else if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("nobre")) {
            metaElytra.setDisplayName("Asa Nobre [Nivel " + nivel + "]");
            metaElytra.addEnchant(Enchantment.PROTECTION_EXPLOSIONS, 1, true);
        }
        metaElytra.addEnchant(Enchantment.PROTECTION_FALL, 1, true);
        metaElytra.addEnchant(Enchantment.DURABILITY, 1, true);
        metaElytra.addEnchant(Enchantment.THORNS, 1, true);
        metaElytra.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
        elytra.setItemMeta(metaElytra);

        final net.minecraft.server.v1_13_R2.ItemStack nmsStack = CraftItemStack.asNMSCopy(elytra);
        final NBTTagCompound compound = (nmsStack.hasTag()) ? nmsStack.getTag() : new NBTTagCompound();
        final NBTTagList modifiers = new NBTTagList();
        final NBTTagCompound speed = new NBTTagCompound();
        final NBTTagCompound maxHealth = new NBTTagCompound();
        final NBTTagCompound attackDamage = new NBTTagCompound();
        final NBTTagCompound armor = new NBTTagCompound();

        speed.set("AttributeName", new NBTTagString("generic.movementSpeed"));
        speed.set("Name", new NBTTagString("generic.movementSpeed"));
        speed.set("Amount", new NBTTagDouble(0.1 * nivel));
        speed.set("Operation", new NBTTagInt(1));
        speed.set("UUIDLeast", new NBTTagInt(894654));
        speed.set("UUIDMost", new NBTTagInt(2872));
        speed.set("Slot", new NBTTagString("chest"));

        maxHealth.set("AttributeName", new NBTTagString("generic.maxHealth"));
        maxHealth.set("Name", new NBTTagString("generic.maxHealth"));
        maxHealth.set("Amount", new NBTTagDouble(0.1 * nivel));
        maxHealth.set("Operation", new NBTTagInt(1));
        maxHealth.set("UUIDLeast", new NBTTagInt(894654));
        maxHealth.set("UUIDMost", new NBTTagInt(2872));
        maxHealth.set("Slot", new NBTTagString("chest"));

        attackDamage.set("AttributeName", new NBTTagString("generic.attackDamage"));
        attackDamage.set("Name", new NBTTagString("generic.attackDamage"));
        attackDamage.set("Amount", new NBTTagDouble(0.1 * nivel));
        attackDamage.set("Operation", new NBTTagInt(1));
        attackDamage.set("UUIDLeast", new NBTTagInt(894654));
        attackDamage.set("UUIDMost", new NBTTagInt(2872));
        attackDamage.set("Slot", new NBTTagString("chest"));

        armor.set("AttributeName", new NBTTagString("generic.armor"));
        armor.set("Name", new NBTTagString("generic.armor"));
        armor.set("Amount", new NBTTagDouble(0.1 * nivel));
        armor.set("Operation", new NBTTagInt(1));
        armor.set("UUIDLeast", new NBTTagInt(894654));
        armor.set("UUIDMost", new NBTTagInt(2872));
        armor.set("Slot", new NBTTagString("chest"));

        modifiers.add(speed);
        modifiers.add(maxHealth);
        modifiers.add(attackDamage);
        modifiers.add(armor);
        compound.set("AttributeModifiers", modifiers);
        nmsStack.setTag(compound);
        elytra = CraftItemStack.asBukkitCopy(nmsStack);

        return elytra;
    }

    public static ArrayList<String> updatePaperUpgradeLv(final Inventory inventory, final String UUID) {
        final ItemStack frag = new ItemStack(Material.FEATHER);
        final ItemMeta metaFrag = frag.getItemMeta();
        metaFrag.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
        final ArrayList<String> lore1 = new ArrayList<>();
        lore1.add(" ");
        lore1.add(ChatColor.GRAY + "Fragmento para asa, tipo: 1");
        lore1.add(" ");
        metaFrag.setLore(lore1);
        frag.setItemMeta(metaFrag);

        final ItemStack frag2 = new ItemStack(Material.FEATHER);
        final ItemMeta metaFrag2 = frag2.getItemMeta();
        metaFrag2.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
        final ArrayList<String> lore2 = new ArrayList<>();
        lore2.add(" ");
        lore2.add(ChatColor.GRAY + "Fragmento para asa, tipo: 2");
        lore2.add(" ");
        metaFrag2.setLore(lore2);
        frag2.setItemMeta(metaFrag2);

        final ItemStack fragN = new ItemStack(Material.PHANTOM_MEMBRANE);
        final ItemMeta metaFragN = fragN.getItemMeta();
        metaFragN.setDisplayName(ChatColor.GREEN + "Fragmento de nível");
        final ArrayList<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Fragmento de nível p/ asa, nível: " + (InventoryElytra.containsElytra(inventory) + 1));
        lore3.add(" ");
        metaFragN.setLore(lore3);
        fragN.setItemMeta(metaFragN);

        final ItemStack paper = new ItemStack(Material.PAPER);
        final ItemMeta metaPaper = paper.getItemMeta();
        metaPaper.setDisplayName(ChatColor.GREEN + "Faltam");
        final ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");

        final ItemStack elytra = new ItemStack(Material.ELYTRA);
        final ItemMeta metaElytra = elytra.getItemMeta();
        if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("sanguinaria")) {
            metaElytra.setDisplayName("Asa Sanguinaria [Nivel ?]");
        } else if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("ancia")) {
            metaElytra.setDisplayName("Asa Ancia [Nivel ?]");
        } else if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("nobre")) {
            metaElytra.setDisplayName("Asa Nobre [Nivel ?]");
        }
        elytra.setItemMeta(metaElytra);
        inventory.setItem(16, elytra);

        if (InventoryElytra.containsElytra(inventory) == 0) {
            lore.add(ChatColor.WHITE + "Asa: " + ChatColor.GRAY + "Nenhuma");
            inventory.setItem(16, elytra);
        } else {
            lore.add(ChatColor.WHITE + "Asa: " + ChatColor.GRAY + "Aplicado");
            inventory.setItem(16, getElytra(UUID, InventoryElytra.containsElytra(inventory) + 1));
        }
        int level = 0;
        if (InventoryElytra.containsElytra(inventory) == 1) {
            level = 200;
        } else if (InventoryElytra.containsElytra(inventory) == 2) {
            level = 300;
        }
        if (level - InventoryElytra.getQuantityFragAsa(frag, frag2, inventory) <= 0) {
            lore.add(ChatColor.WHITE + "Fragmento de asa: " + ChatColor.GRAY + "0");
        } else {
            lore.add(ChatColor.WHITE + "Fragmento de asa: " + ChatColor.GRAY + (level - InventoryElytra.getQuantityFragAsa(frag, frag2, inventory)));
        }
        int level2 = 0;
        if (InventoryElytra.containsElytra(inventory) == 1) {
            level2 = 30;
        } else if (InventoryElytra.containsElytra(inventory) == 2) {
            level2 = 60;
        }
        if (level2 - InventoryElytra.getQuantity(fragN, inventory) <= 0) {
            lore.add(ChatColor.WHITE + "Fragmento de nível: " + ChatColor.GRAY + "0");
        } else {
            lore.add(ChatColor.WHITE + "Fragmento de nível: " + ChatColor.GRAY + (level2 - InventoryElytra.getQuantity(fragN, inventory)));
        }
        lore.add(" ");
        metaPaper.setLore(lore);
        paper.setItemMeta(metaPaper);
        inventory.setItem(48, paper);

        return lore;
    }

    public static ArrayList<String> updatePaper(final Inventory inventory) {
        final ItemStack frag = new ItemStack(Material.FEATHER);
        final ItemMeta metaFrag = frag.getItemMeta();
        metaFrag.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
        final ArrayList<String> lore1 = new ArrayList<>();
        lore1.add(" ");
        lore1.add(ChatColor.GRAY + "Fragmento para asa, tipo: 1");
        lore1.add(" ");
        metaFrag.setLore(lore1);
        frag.setItemMeta(metaFrag);

        final ItemStack frag2 = new ItemStack(Material.FEATHER);
        final ItemMeta metaFrag2 = frag2.getItemMeta();
        metaFrag2.setDisplayName(ChatColor.GREEN + "Fragmento de asa");
        final ArrayList<String> lore2 = new ArrayList<>();
        lore2.add(" ");
        lore2.add(ChatColor.GRAY + "Fragmento para asa, tipo: 2");
        lore2.add(" ");
        metaFrag2.setLore(lore2);
        frag2.setItemMeta(metaFrag2);

        final ItemStack fragN = new ItemStack(Material.PHANTOM_MEMBRANE);
        final ItemMeta metaFragN = fragN.getItemMeta();
        metaFragN.setDisplayName(ChatColor.GREEN + "Fragmento de nível");
        final ArrayList<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Fragmento de nível p/ asa, nível: 1");
        lore3.add(" ");
        metaFragN.setLore(lore3);
        fragN.setItemMeta(metaFragN);

        final ItemStack paper = new ItemStack(Material.PAPER);
        final ItemMeta metaPaper = paper.getItemMeta();
        metaPaper.setDisplayName(ChatColor.GREEN + "Faltam");
        final ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");

        if (100 - InventoryElytra.getQuantityFragAsa(frag, frag2, inventory) <= 0) {
            lore.add(ChatColor.WHITE + "Fragmento de asa: " + ChatColor.GRAY + "0");
        } else {
            lore.add(ChatColor.WHITE + "Fragmento de asa: " + ChatColor.GRAY + (100 - InventoryElytra.getQuantityFragAsa(frag, frag2, inventory)));
        }
        if (15 - InventoryElytra.getQuantity(fragN, inventory) <= 0) {
            lore.add(ChatColor.WHITE + "Fragmento de nível: " + ChatColor.GRAY + "0");
        } else {
            lore.add(ChatColor.WHITE + "Fragmento de nível: " + ChatColor.GRAY + (100 - InventoryElytra.getQuantity(fragN, inventory)));
        }
        lore.add(" ");
        metaPaper.setLore(lore);
        paper.setItemMeta(metaPaper);
        inventory.setItem(48, paper);

        return lore;
    }

    private static int getQuantityFragAsa(final ItemStack type1, final ItemStack type2, final Inventory inventory) {
        final int quantity = InventoryElytra.getQuantity(type1, inventory) + (InventoryElytra.getQuantity(type2, inventory) * 2);

        return quantity;
    }

    private static int getQuantity(final ItemStack item, final Inventory inventory) {
        int quantity = 0;
        for (int i = 1; i <= inventory.getSize(); i++) {
            if (inventory.getItem(i - 1) != null && !inventory.getItem(i - 1).getType().equals(Material.AIR) && inventory.getItem(i - 1).getItemMeta().getLore() != null && inventory.getItem(i - 1).getItemMeta().getLore().get(1).equalsIgnoreCase(item.getItemMeta().getLore().get(1))) {
                quantity += inventory.getItem(i - 1).getAmount();
            }
        }
        return quantity;
    }

    private static int containsElytra(final Inventory inventory) {
        int have = 0;
        for (int i = 1; i <= inventory.getSize(); i++) {
            if (i - 1 == 10 || i - 1 == 11 || i - 1 == 12 || i - 1 == 13 || i - 1 == 14 || i - 1 == 19 || i - 1 == 20 || i - 1 == 21 || i - 1 == 22 || i - 1 == 23
                    || i - 1 == 28 || i - 1 == 29 || i - 1 == 30 || i - 1 == 31 || i - 1 == 32 || i - 1 == 37 || i - 1 == 38 || i - 1 == 39 || i - 1 == 40 || i - 1 == 41) {
                if (inventory.getItem(i - 1) != null && !inventory.getItem(i - 1).getType().equals(Material.AIR)) {
                    if (inventory.getItem(i - 1).getType().equals(Material.ELYTRA)) {
                        if (inventory.getItem(i - 1).getItemMeta().getDisplayName().contains("[Nivel 1]") || inventory.getItem(i - 1).getItemMeta().getDisplayName().contains("[Nivel 2]")) {
                            if (inventory.getItem(i - 1).getItemMeta().getAttributeModifiers().size() > 1) {
                                if (inventory.getItem(i - 1).getItemMeta().getDisplayName().contains("[Nivel 1]")) {
                                    have = 1;
                                } else if (inventory.getItem(i - 1).getItemMeta().getDisplayName().contains("[Nivel 2]")) {
                                    have = 2;
                                }
                            }
                        }
                    }
                }
            }
        }

        return have;
    }

    public static boolean containsElytraBoolean(final Inventory inventory) {
        boolean have = false;
        for (int i = 1; i <= inventory.getSize(); i++) {
            if (i - 1 == 10 || i - 1 == 11 || i - 1 == 12 || i - 1 == 13 || i - 1 == 14 || i - 1 == 19 || i - 1 == 20 || i - 1 == 21 || i - 1 == 22 || i - 1 == 23
                    || i - 1 == 28 || i - 1 == 29 || i - 1 == 30 || i - 1 == 31 || i - 1 == 32 || i - 1 == 37 || i - 1 == 38 || i - 1 == 39 || i - 1 == 40 || i - 1 == 41) {
                if (inventory.getItem(i - 1) != null && !inventory.getItem(i - 1).getType().equals(Material.AIR)) {
                    if (inventory.getItem(i - 1).getType().equals(Material.ELYTRA)) {
                        if (inventory.getItem(i - 1).getItemMeta().getDisplayName().contains("[Nivel 1]") || inventory.getItem(i - 1).getItemMeta().getDisplayName().contains("[Nivel 2]")) {
                            if (inventory.getItem(i - 1).getItemMeta().getAttributeModifiers().size() > 1) {
                                have = true;
                            }
                        }
                    }
                }
            }
        }
        return have;
    }
}
