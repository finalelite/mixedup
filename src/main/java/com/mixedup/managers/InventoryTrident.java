package com.mixedup.managers;

import com.mixedup.economy.LojaAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class InventoryTrident {

    public static DecimalFormat formatter = new DecimalFormat("#,###.00");

    public static Inventory inv(final String type) {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Tridentes: ");

        final ItemStack item13 = new ItemStack(Material.TRIDENT);
        final ItemMeta metaItem13 = item13.getItemMeta();
        metaItem13.setDisplayName(ChatColor.RED + "Tridente Elétrico");
        final ArrayList<String> lore13 = new ArrayList<String>();
        if (LojaAPI.valorVenda("tridente_eletrico", type) != 0 && LojaAPI.valorCompra("tridente_eletrico", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryTrident.formatter.format(LojaAPI.valorCompra("tridente_eletrico", type)));
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryTrident.formatter.format(LojaAPI.valorVenda("tridente_eletrico", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("tridente_eletrico", type));
            lore13.add(" ");
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaItem13.setLore(lore13);
        } else if (LojaAPI.valorCompra("tridente_eletrico", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[COMPRA]: §a$" + InventoryTrident.formatter.format(LojaAPI.valorCompra("tridente_eletrico", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("tridente_eletrico", type));
            lore13.add(" ");
            lore13.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore13.add(" ");
            metaItem13.setLore(lore13);
        } else if (LojaAPI.valorVenda("tridente_eletrico", type) != 0) {
            lore13.add(" ");
            lore13.add("§7Preço §c[VENDA]: §a$" + InventoryTrident.formatter.format(LojaAPI.valorVenda("tridente_eletrico", type)));
            lore13.add("§7Quantia: §a" + LojaAPI.itemQuantia("tridente_eletrico", type));
            lore13.add(" ");
            lore13.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore13.add(" ");
            metaItem13.setLore(lore13);
        }
        item13.setItemMeta(metaItem13);
        inventory.setItem(15, item13);

        final ItemStack item14 = new ItemStack(Material.TRIDENT);
        final ItemMeta metaItem14 = item14.getItemMeta();
        metaItem14.setDisplayName(ChatColor.RED + "Tridente da Lentidão");
        final ArrayList<String> lore14 = new ArrayList<String>();
        if (LojaAPI.valorVenda("tridente_da_lentidao", type) != 0 && LojaAPI.valorCompra("tridente_da_lentidao", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryTrident.formatter.format(LojaAPI.valorCompra("tridente_da_lentidao", type)));
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryTrident.formatter.format(LojaAPI.valorVenda("tridente_da_lentidao", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("tridente_da_lentidao", type));
            lore14.add(" ");
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaItem14.setLore(lore14);
        } else if (LojaAPI.valorCompra("tridente_da_lentidao", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[COMPRA]: §a$" + InventoryTrident.formatter.format(LojaAPI.valorCompra("tridente_da_lentidao", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("tridente_da_lentidao", type));
            lore14.add(" ");
            lore14.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore14.add(" ");
            metaItem14.setLore(lore14);
        } else if (LojaAPI.valorVenda("tridente_da_lentidao", type) != 0) {
            lore14.add(" ");
            lore14.add("§7Preço §c[VENDA]: §a$" + InventoryTrident.formatter.format(LojaAPI.valorVenda("tridente_da_lentidao", type)));
            lore14.add("§7Quantia: §a" + LojaAPI.itemQuantia("tridente_da_lentidao", type));
            lore14.add(" ");
            lore14.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore14.add(" ");
            metaItem14.setLore(lore14);
        }
        item14.setItemMeta(metaItem14);
        inventory.setItem(14, item14);

        final ItemStack item15 = new ItemStack(Material.TRIDENT);
        final ItemMeta metaItem15 = item15.getItemMeta();
        metaItem15.setDisplayName(ChatColor.RED + "Tridente Envenenado");
        final ArrayList<String> lore15 = new ArrayList<String>();
        if (LojaAPI.valorVenda("tridente_envenenado", type) != 0 && LojaAPI.valorCompra("tridente_envenenado", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryTrident.formatter.format(LojaAPI.valorCompra("tridente_envenenado", type)));
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryTrident.formatter.format(LojaAPI.valorVenda("tridente_envenenado", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("tridente_envenenado", type));
            lore15.add(" ");
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaItem15.setLore(lore15);
        } else if (LojaAPI.valorCompra("tridente_envenenado", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[COMPRA]: §a$" + InventoryTrident.formatter.format(LojaAPI.valorCompra("tridente_envenenado", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("tridente_envenenado", type));
            lore15.add(" ");
            lore15.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore15.add(" ");
            metaItem15.setLore(lore15);
        } else if (LojaAPI.valorVenda("tridente_envenenado", type) != 0) {
            lore15.add(" ");
            lore15.add("§7Preço §c[VENDA]: §a$" + InventoryTrident.formatter.format(LojaAPI.valorVenda("tridente_envenenado", type)));
            lore15.add("§7Quantia: §a" + LojaAPI.itemQuantia("tridente_envenenado", type));
            lore15.add(" ");
            lore15.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore15.add(" ");
            metaItem15.setLore(lore15);
        }
        item15.setItemMeta(metaItem15);
        inventory.setItem(13, item15);

        final ItemStack item16 = new ItemStack(Material.TRIDENT);
        final ItemMeta metaItem16 = item16.getItemMeta();
        metaItem16.setDisplayName(ChatColor.RED + "Tridente Flamejante");
        final ArrayList<String> lore16 = new ArrayList<String>();
        if (LojaAPI.valorVenda("tridente_flamejante", type) != 0 && LojaAPI.valorCompra("tridente_flamejante", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryTrident.formatter.format(LojaAPI.valorCompra("tridente_flamejante", type)));
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryTrident.formatter.format(LojaAPI.valorVenda("tridente_flamejante", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("tridente_flamejante", type));
            lore16.add(" ");
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaItem16.setLore(lore16);
        } else if (LojaAPI.valorCompra("tridente_flamejante", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[COMPRA]: §a$" + InventoryTrident.formatter.format(LojaAPI.valorCompra("tridente_flamejante", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("tridente_flamejante", type));
            lore16.add(" ");
            lore16.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore16.add(" ");
            metaItem16.setLore(lore16);
        } else if (LojaAPI.valorVenda("tridente_flamejante", type) != 0) {
            lore16.add(" ");
            lore16.add("§7Preço §c[VENDA]: §a$" + InventoryTrident.formatter.format(LojaAPI.valorVenda("tridente_flamejante", type)));
            lore16.add("§7Quantia: §a" + LojaAPI.itemQuantia("tridente_flamejante", type));
            lore16.add(" ");
            lore16.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore16.add(" ");
            metaItem16.setLore(lore16);
        }
        item16.setItemMeta(metaItem16);
        inventory.setItem(12, item16);

        final ItemStack item17 = new ItemStack(Material.TRIDENT);
        final ItemMeta metaItem17 = item17.getItemMeta();
        metaItem17.setDisplayName(ChatColor.RED + "Tridente Sequestrador");
        final ArrayList<String> lore17 = new ArrayList<String>();
        if (LojaAPI.valorVenda("tridente_sequestrador", type) != 0 && LojaAPI.valorCompra("tridente_sequestrador", type) != 0) {
            lore17.add(" ");
            lore17.add("§7Preço §c[COMPRA]: §a$" + InventoryTrident.formatter.format(LojaAPI.valorCompra("tridente_sequestrador", type)));
            lore17.add("§7Preço §c[VENDA]: §a$" + InventoryTrident.formatter.format(LojaAPI.valorVenda("tridente_sequestrador", type)));
            lore17.add("§7Quantia: §a" + LojaAPI.itemQuantia("tridente_sequestrador", type));
            lore17.add(" ");
            lore17.add(" ");
            lore17.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore17.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            metaItem17.setLore(lore17);
        } else if (LojaAPI.valorCompra("tridente_sequestrador", type) != 0) {
            lore17.add(" ");
            lore17.add("§7Preço §c[COMPRA]: §a$" + InventoryTrident.formatter.format(LojaAPI.valorCompra("tridente_sequestrador", type)));
            lore17.add("§7Quantia: §a" + LojaAPI.itemQuantia("tridente_sequestrador", type));
            lore17.add(" ");
            lore17.add("§c[PARA COMPRAR CLIQUE COM BOTÃO DIREITO]");
            lore17.add(" ");
            metaItem17.setLore(lore17);
        } else if (LojaAPI.valorVenda("tridente_sequestrador", type) != 0) {
            lore17.add(" ");
            lore17.add("§7Preço §c[VENDA]: §a$" + InventoryTrident.formatter.format(LojaAPI.valorVenda("tridente_sequestrador", type)));
            lore17.add("§7Quantia: §a" + LojaAPI.itemQuantia("tridente_sequestrador", type));
            lore17.add(" ");
            lore17.add("§c[PARA VENDER CLIQUE COM BOTÃO ESQUERDO]");
            lore17.add(" ");
            metaItem17.setLore(lore17);
        }
        item17.setItemMeta(metaItem17);
        inventory.setItem(11, item17);

        return inventory;
    }
}
