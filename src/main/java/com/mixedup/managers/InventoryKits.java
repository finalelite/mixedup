package com.mixedup.managers;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class InventoryKits {

    public static Inventory firstInvKits() {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Seleção classe: ");

        final ItemStack basico = new ItemStack(Material.WOODEN_PICKAXE);
        final ItemMeta metaBasico = basico.getItemMeta();
        metaBasico.setDisplayName(ChatColor.YELLOW + "Kits básicos");
        basico.setItemMeta(metaBasico);

        final ItemStack vips = new ItemStack(Material.DIAMOND_BLOCK);
        final ItemMeta metaVips = vips.getItemMeta();
        metaVips.setDisplayName(ChatColor.YELLOW + "Kits VIPS");
        final List<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.GRAY + "Adquira " + ChatColor.AQUA + "VIP" + ChatColor.GRAY + " e contenha vantagens");
        lore.add(ChatColor.GRAY + "como está!");
        lore.add(" ");
        metaVips.setLore(lore);
        vips.setItemMeta(metaVips);

        inventory.setItem(12, basico);
        inventory.setItem(14, vips);

        return inventory;
    }

    public static Inventory inventoryKitsVips() {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Kits VIPS: ");

        final ItemStack conde = new ItemStack(Material.COAL_BLOCK);
        final ItemMeta meta1 = conde.getItemMeta();
        meta1.setDisplayName(ChatColor.YELLOW + "Kits VIP (Conde)");
        final List<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.GRAY + "Adquira vantagens como está em: ");
        lore.add(ChatColor.AQUA + "loja.finalelite.com.br");
        lore.add(" ");
        meta1.setLore(lore);
        conde.setItemMeta(meta1);

        final ItemStack lord = new ItemStack(Material.GOLD_BLOCK);
        final ItemMeta meta2 = lord.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "Kits VIP " + ChatColor.AQUA + "(Lord)");
        final List<String> lore2 = new ArrayList<>();
        lore2.add(" ");
        lore2.add(ChatColor.GRAY + "Adquira vantagens como está em: ");
        lore2.add(ChatColor.AQUA + "loja.finalelite.com.br");
        lore2.add(" ");
        meta2.setLore(lore2);
        lord.setItemMeta(meta2);

        final ItemStack titan = new ItemStack(Material.IRON_BLOCK);
        final ItemMeta meta3 = titan.getItemMeta();
        meta3.setDisplayName(ChatColor.YELLOW + "Kits VIP " + ChatColor.DARK_PURPLE + "(Titan)");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Adquira vantagens como está em: ");
        lore3.add(ChatColor.AQUA + "loja.finalelite.com.br");
        lore3.add(" ");
        meta3.setLore(lore3);
        titan.setItemMeta(meta3);

        final ItemStack duque = new ItemStack(Material.DIAMOND_BLOCK);
        final ItemMeta meta4 = duque.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Kits VIP " + ChatColor.DARK_RED + "(Duque)");
        final List<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.GRAY + "Adquira vantagens como está em: ");
        lore4.add(ChatColor.AQUA + "loja.finalelite.com.br");
        lore4.add(" ");
        meta4.setLore(lore4);
        duque.setItemMeta(meta4);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta10 = voltar.getItemMeta();
        meta10.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta10);

        inventory.setItem(10, conde);
        inventory.setItem(12, lord);
        inventory.setItem(14, titan);
        inventory.setItem(16, duque);
        inventory.setItem(18, voltar);

        return inventory;
    }

    public static Inventory inventoryKitsBasics() {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Kits básicos: ");

        final ItemStack basico = new ItemStack(Material.WOODEN_PICKAXE);
        final ItemMeta metaBasico = basico.getItemMeta();
        metaBasico.setDisplayName(ChatColor.YELLOW + "Kit básico");
        final List<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão direito para");
        lore.add(ChatColor.GRAY + " verificar os itens que este kit");
        lore.add(ChatColor.GRAY + " disponibiliza.");
        lore.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão esquerdo para");
        lore.add(ChatColor.GRAY + " pegar o kit.");
        lore.add(" ");
        metaBasico.setLore(lore);
        basico.setItemMeta(metaBasico);

        final ItemStack horas = new ItemStack(Material.STONE_PICKAXE);
        final ItemMeta metaHoras = horas.getItemMeta();
        metaHoras.setDisplayName(ChatColor.YELLOW + "Kit, 6 horas jogadas");
        final List<String> lore1 = new ArrayList<>();
        lore1.add(" ");
        lore1.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão direito para");
        lore1.add(ChatColor.GRAY + " verificar os itens que este kit");
        lore1.add(ChatColor.GRAY + " disponibiliza.");
        lore1.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão esquerdo para");
        lore1.add(ChatColor.GRAY + " pegar o kit.");
        lore1.add(" ");
        metaHoras.setLore(lore1);
        horas.setItemMeta(metaHoras);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta10 = voltar.getItemMeta();
        meta10.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta10);

        inventory.setItem(12, basico);
        inventory.setItem(14, horas);
        inventory.setItem(18, voltar);

        return inventory;
    }

    //-
    public static void giveKit6HorasDuque(final Player player) {
        final ItemStack caixaBasica = new ItemStack(Material.CHEST, 6);
        final ItemMeta metaCaixaAva = caixaBasica.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Básica");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaBasica.setItemMeta(metaCaixaAva);
        final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(caixaBasica);
        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 6);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);
        final HashMap<Integer, ItemStack> nope1 = player.getInventory().addItem(chave);
        for (final Entry<Integer, ItemStack> entry : nope1.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta meta2 = moeda.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "$7.500,00 moedas");
        moeda.setItemMeta(meta2);
        final HashMap<Integer, ItemStack> nope2 = player.getInventory().addItem(moeda);
        for (final Entry<Integer, ItemStack> entry : nope2.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack moedaN = new ItemStack(Material.GHAST_TEAR);
        final ItemMeta metaN2 = moedaN.getItemMeta();
        metaN2.setDisplayName(ChatColor.YELLOW + "$150,00 moedas");
        moedaN.setItemMeta(metaN2);
        final HashMap<Integer, ItemStack> nope5 = player.getInventory().addItem(moedaN);
        for (final Entry<Integer, ItemStack> entry : nope5.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack goldenCarrot = new ItemStack(Material.GOLDEN_CARROT, 30);
        final HashMap<Integer, ItemStack> nope3 = player.getInventory().addItem(goldenCarrot);
        for (final Entry<Integer, ItemStack> entry : nope3.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack pao = new ItemStack(Material.BREAD, 64);
        final HashMap<Integer, ItemStack> nope4 = player.getInventory().addItem(pao);
        for (final Entry<Integer, ItemStack> entry : nope4.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack APPLE = new ItemStack(Material.ENCHANTED_GOLDEN_APPLE);
        final HashMap<Integer, ItemStack> nope6 = player.getInventory().addItem(APPLE);
        for (final Entry<Integer, ItemStack> entry : nope6.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
    }

    public static Inventory inventoryKit6HorasDuque() {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Kit 6 horas jogadas (Duque): ");

        final ItemStack caixaBasica = new ItemStack(Material.CHEST, 6);
        final ItemMeta metaCaixaAva = caixaBasica.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Básica");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaBasica.setItemMeta(metaCaixaAva);

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 6);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta meta2 = moeda.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "$7.500,00 moedas");
        moeda.setItemMeta(meta2);

        final ItemStack moedaN = new ItemStack(Material.GHAST_TEAR);
        final ItemMeta metaN2 = moedaN.getItemMeta();
        metaN2.setDisplayName(ChatColor.YELLOW + "$150,00 moedas");
        moedaN.setItemMeta(metaN2);

        final ItemStack goldenCarrot = new ItemStack(Material.GOLDEN_CARROT, 30);
        final ItemStack pao = new ItemStack(Material.BREAD, 64);
        final ItemStack goldenAp = new ItemStack(Material.ENCHANTED_GOLDEN_APPLE);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta10 = voltar.getItemMeta();
        meta10.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta10);

        inventory.setItem(10, caixaBasica);
        inventory.setItem(11, chave);
        inventory.setItem(12, moeda);
        inventory.setItem(13, moedaN);
        inventory.setItem(14, goldenCarrot);
        inventory.setItem(15, pao);
        inventory.setItem(16, goldenAp);
        inventory.setItem(18, voltar);

        return inventory;
    }
    //-

    public static void giveKit6HorasTitan(final Player player) {
        final ItemStack caixaBasica = new ItemStack(Material.CHEST, 4);
        final ItemMeta metaCaixaAva = caixaBasica.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Básica");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaBasica.setItemMeta(metaCaixaAva);
        final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(caixaBasica);
        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 4);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);
        final HashMap<Integer, ItemStack> nope1 = player.getInventory().addItem(chave);
        for (final Entry<Integer, ItemStack> entry : nope1.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta meta2 = moeda.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "$5.000,00 moedas");
        moeda.setItemMeta(meta2);
        final HashMap<Integer, ItemStack> nope2 = player.getInventory().addItem(moeda);
        for (final Entry<Integer, ItemStack> entry : nope2.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack goldenCarrot = new ItemStack(Material.GOLDEN_CARROT, 20);
        final HashMap<Integer, ItemStack> nope3 = player.getInventory().addItem(goldenCarrot);
        for (final Entry<Integer, ItemStack> entry : nope3.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack pao = new ItemStack(Material.BREAD, 48);
        final HashMap<Integer, ItemStack> nope4 = player.getInventory().addItem(pao);
        for (final Entry<Integer, ItemStack> entry : nope4.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
    }

    public static Inventory inventoryKit6HorasTitan() {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Kit 6 horas jogadas (Titan): ");

        final ItemStack caixaBasica = new ItemStack(Material.CHEST, 4);
        final ItemMeta metaCaixaAva = caixaBasica.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Básica");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaBasica.setItemMeta(metaCaixaAva);

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 4);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta meta2 = moeda.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "$5.000,00 moedas");
        moeda.setItemMeta(meta2);

        final ItemStack goldenCarrot = new ItemStack(Material.GOLDEN_CARROT, 20);
        final ItemStack pao = new ItemStack(Material.BREAD, 48);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta10 = voltar.getItemMeta();
        meta10.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta10);

        inventory.setItem(11, caixaBasica);
        inventory.setItem(12, chave);
        inventory.setItem(13, moeda);
        inventory.setItem(14, goldenCarrot);
        inventory.setItem(15, pao);
        inventory.setItem(18, voltar);

        return inventory;
    }

    public static void giveKit6HorasLord(final Player player) {
        final ItemStack caixaBasica = new ItemStack(Material.CHEST, 4);
        final ItemMeta metaCaixaAva = caixaBasica.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Básica");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaBasica.setItemMeta(metaCaixaAva);
        final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(caixaBasica);
        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 2);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);
        final HashMap<Integer, ItemStack> nope1 = player.getInventory().addItem(chave);
        for (final Entry<Integer, ItemStack> entry : nope1.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta meta2 = moeda.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "$5.000,00 moedas");
        moeda.setItemMeta(meta2);
        final HashMap<Integer, ItemStack> nope2 = player.getInventory().addItem(moeda);
        for (final Entry<Integer, ItemStack> entry : nope2.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack goldenCarrot = new ItemStack(Material.GOLDEN_CARROT, 15);
        final HashMap<Integer, ItemStack> nope3 = player.getInventory().addItem(goldenCarrot);
        for (final Entry<Integer, ItemStack> entry : nope3.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack pao = new ItemStack(Material.BREAD, 32);
        final HashMap<Integer, ItemStack> nope4 = player.getInventory().addItem(pao);
        for (final Entry<Integer, ItemStack> entry : nope4.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
    }

    public static Inventory inventoryKit6HorasLord() {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Kit 6 horas jogadas (Lord): ");

        final ItemStack caixaBasica = new ItemStack(Material.CHEST, 4);
        final ItemMeta metaCaixaAva = caixaBasica.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Básica");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaBasica.setItemMeta(metaCaixaAva);

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 2);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta meta2 = moeda.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "$5.000,00 moedas");
        moeda.setItemMeta(meta2);

        final ItemStack goldenCarrot = new ItemStack(Material.GOLDEN_CARROT, 15);
        final ItemStack pao = new ItemStack(Material.BREAD, 32);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta10 = voltar.getItemMeta();
        meta10.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta10);

        inventory.setItem(11, caixaBasica);
        inventory.setItem(12, chave);
        inventory.setItem(13, moeda);
        inventory.setItem(14, goldenCarrot);
        inventory.setItem(15, pao);
        inventory.setItem(18, voltar);

        return inventory;
    }

    public static void giveKit6HorasConde(final Player player) {
        final ItemStack caixaBasica = new ItemStack(Material.CHEST, 2);
        final ItemMeta metaCaixaAva = caixaBasica.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Básica");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaBasica.setItemMeta(metaCaixaAva);
        final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(caixaBasica);
        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 1);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);
        final HashMap<Integer, ItemStack> nope1 = player.getInventory().addItem(chave);
        for (final Entry<Integer, ItemStack> entry : nope1.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta meta2 = moeda.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "$5.000,00 moedas");
        moeda.setItemMeta(meta2);
        final HashMap<Integer, ItemStack> nope2 = player.getInventory().addItem(moeda);
        for (final Entry<Integer, ItemStack> entry : nope2.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack goldenCarrot = new ItemStack(Material.GOLDEN_CARROT, 10);
        final HashMap<Integer, ItemStack> nope3 = player.getInventory().addItem(goldenCarrot);
        for (final Entry<Integer, ItemStack> entry : nope3.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack pao = new ItemStack(Material.BREAD, 16);
        final HashMap<Integer, ItemStack> nope4 = player.getInventory().addItem(pao);
        for (final Entry<Integer, ItemStack> entry : nope4.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
    }

    public static Inventory inventoryKit6HorasConde() {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Kit 6 horas jogadas (Conde): ");

        final ItemStack caixaBasica = new ItemStack(Material.CHEST, 2);
        final ItemMeta metaCaixaAva = caixaBasica.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Básica");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaBasica.setItemMeta(metaCaixaAva);

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 1);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta meta2 = moeda.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "$5.000,00 moedas");
        moeda.setItemMeta(meta2);

        final ItemStack goldenCarrot = new ItemStack(Material.GOLDEN_CARROT, 10);
        final ItemStack pao = new ItemStack(Material.BREAD, 16);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta10 = voltar.getItemMeta();
        meta10.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta10);

        inventory.setItem(11, caixaBasica);
        inventory.setItem(12, chave);
        inventory.setItem(13, moeda);
        inventory.setItem(14, goldenCarrot);
        inventory.setItem(15, pao);
        inventory.setItem(18, voltar);

        return inventory;
    }

    //-
    public static void giveKitDiarioDuque(final Player player) {
        final ItemStack picareta = new ItemStack(Material.IRON_PICKAXE);
        picareta.addEnchantment(Enchantment.DURABILITY, 1);
        final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(picareta);
        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack machado = new ItemStack(Material.IRON_AXE);
        machado.addEnchantment(Enchantment.DURABILITY, 1);
        final HashMap<Integer, ItemStack> nope1 = player.getInventory().addItem(machado);
        for (final Entry<Integer, ItemStack> entry : nope1.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack pa = new ItemStack(Material.IRON_SHOVEL);
        pa.addEnchantment(Enchantment.DURABILITY, 1);
        final HashMap<Integer, ItemStack> nope2 = player.getInventory().addItem(pa);
        for (final Entry<Integer, ItemStack> entry : nope2.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack carne = new ItemStack(Material.COOKED_BEEF, 48);
        final HashMap<Integer, ItemStack> nope3 = player.getInventory().addItem(carne);
        for (final Entry<Integer, ItemStack> entry : nope3.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack appleGolden = new ItemStack(Material.GOLDEN_APPLE, 25);
        final HashMap<Integer, ItemStack> nope4 = player.getInventory().addItem(appleGolden);
        for (final Entry<Integer, ItemStack> entry : nope4.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack wood = new ItemStack(Material.OAK_LOG, 64);
        final HashMap<Integer, ItemStack> nope5 = player.getInventory().addItem(wood);
        for (final Entry<Integer, ItemStack> entry : nope5.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta meta2 = moeda.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "$4.000,00 moedas");
        moeda.setItemMeta(meta2);
        final HashMap<Integer, ItemStack> nope6 = player.getInventory().addItem(moeda);
        for (final Entry<Integer, ItemStack> entry : nope6.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack moedaNe = new ItemStack(Material.GHAST_TEAR);
        final ItemMeta meta3 = moedaNe.getItemMeta();
        meta3.setDisplayName(ChatColor.YELLOW + "$150,00 moedas negra");
        moedaNe.setItemMeta(meta3);
        final HashMap<Integer, ItemStack> nope7 = player.getInventory().addItem(moedaNe);
        for (final Entry<Integer, ItemStack> entry : nope7.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack random = new ItemStack(Material.ENCHANTED_BOOK);
        final ItemMeta meta4 = random.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Livro sortido");
        random.setItemMeta(meta4);
        final HashMap<Integer, ItemStack> nope8 = player.getInventory().addItem(random);
        for (final Entry<Integer, ItemStack> entry : nope8.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope18 = player.getInventory().addItem(random);
        for (final Entry<Integer, ItemStack> entry : nope18.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack bottle = new ItemStack(Material.EXPERIENCE_BOTTLE, 64);
        final HashMap<Integer, ItemStack> nope9 = player.getInventory().addItem(bottle);
        for (final Entry<Integer, ItemStack> entry : nope9.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack caixaBasica = new ItemStack(Material.CHEST, 5);
        final ItemMeta metaCaixaAva = caixaBasica.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Básica");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaBasica.setItemMeta(metaCaixaAva);
        final HashMap<Integer, ItemStack> nope10 = player.getInventory().addItem(caixaBasica);
        for (final Entry<Integer, ItemStack> entry : nope10.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 5);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);
        final HashMap<Integer, ItemStack> nope11 = player.getInventory().addItem(chave);
        for (final Entry<Integer, ItemStack> entry : nope11.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
    }

    public static Inventory inventoryKitDiarioDuque() {
        final Inventory inventory = Bukkit.createInventory(null, 4 * 9, "Kit diário (Duque): ");

        final ItemStack picareta = new ItemStack(Material.IRON_PICKAXE);
        picareta.addEnchantment(Enchantment.DURABILITY, 1);
        final ItemStack machado = new ItemStack(Material.IRON_AXE);
        machado.addEnchantment(Enchantment.DURABILITY, 1);
        final ItemStack pa = new ItemStack(Material.IRON_SHOVEL);
        pa.addEnchantment(Enchantment.DURABILITY, 1);
        final ItemStack carne = new ItemStack(Material.COOKED_BEEF, 48);
        final ItemStack appleGolden = new ItemStack(Material.GOLDEN_APPLE, 25);
        final ItemStack wood = new ItemStack(Material.OAK_LOG, 64);

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta meta2 = moeda.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "$4.000,00 moedas");
        moeda.setItemMeta(meta2);

        final ItemStack moedaNe = new ItemStack(Material.GHAST_TEAR);
        final ItemMeta meta3 = moedaNe.getItemMeta();
        meta3.setDisplayName(ChatColor.YELLOW + "$150,00 moedas negra");
        moedaNe.setItemMeta(meta3);

        final ItemStack random = new ItemStack(Material.ENCHANTED_BOOK);
        final ItemMeta meta4 = random.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Livro sortido");
        random.setItemMeta(meta4);

        final ItemStack bottle = new ItemStack(Material.EXPERIENCE_BOTTLE, 64);

        final ItemStack caixaBasica = new ItemStack(Material.CHEST, 5);
        final ItemMeta metaCaixaAva = caixaBasica.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Básica");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaBasica.setItemMeta(metaCaixaAva);

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 5);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta10 = voltar.getItemMeta();
        meta10.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta10);

        inventory.setItem(10, picareta);
        inventory.setItem(11, machado);
        inventory.setItem(12, pa);
        inventory.setItem(13, carne);
        inventory.setItem(14, appleGolden);
        inventory.setItem(15, wood);
        inventory.setItem(16, moeda);
        inventory.setItem(19, moedaNe);
        inventory.setItem(20, random);
        inventory.setItem(21, random);
        inventory.setItem(22, bottle);
        inventory.setItem(23, caixaBasica);
        inventory.setItem(24, chave);
        inventory.setItem(27, voltar);

        return inventory;
    }
    //-

    public static void giveKitDiarioTitan(final Player player) {
        final ItemStack picareta = new ItemStack(Material.IRON_PICKAXE);
        final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(picareta);
        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack machado = new ItemStack(Material.IRON_AXE);
        final HashMap<Integer, ItemStack> nope1 = player.getInventory().addItem(machado);
        for (final Entry<Integer, ItemStack> entry : nope1.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack pa = new ItemStack(Material.IRON_SHOVEL);
        final HashMap<Integer, ItemStack> nope2 = player.getInventory().addItem(pa);
        for (final Entry<Integer, ItemStack> entry : nope2.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack carne = new ItemStack(Material.COOKED_BEEF, 32);
        final HashMap<Integer, ItemStack> nope3 = player.getInventory().addItem(carne);
        for (final Entry<Integer, ItemStack> entry : nope3.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack appleGolden = new ItemStack(Material.GOLDEN_APPLE, 20);
        final HashMap<Integer, ItemStack> nope4 = player.getInventory().addItem(appleGolden);
        for (final Entry<Integer, ItemStack> entry : nope4.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack wood = new ItemStack(Material.OAK_LOG, 64);
        final HashMap<Integer, ItemStack> nope5 = player.getInventory().addItem(wood);
        for (final Entry<Integer, ItemStack> entry : nope5.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta meta2 = moeda.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "$2.000,00 moedas");
        moeda.setItemMeta(meta2);
        final HashMap<Integer, ItemStack> nope6 = player.getInventory().addItem(moeda);
        for (final Entry<Integer, ItemStack> entry : nope6.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack moedaNe = new ItemStack(Material.GHAST_TEAR);
        final ItemMeta meta3 = moedaNe.getItemMeta();
        meta3.setDisplayName(ChatColor.YELLOW + "$100,00 moedas negra");
        moedaNe.setItemMeta(meta3);
        final HashMap<Integer, ItemStack> nope7 = player.getInventory().addItem(moedaNe);
        for (final Entry<Integer, ItemStack> entry : nope7.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack random = new ItemStack(Material.ENCHANTED_BOOK);
        final ItemMeta meta4 = random.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Livro sortido");
        random.setItemMeta(meta4);
        final HashMap<Integer, ItemStack> nope8 = player.getInventory().addItem(random);
        for (final Entry<Integer, ItemStack> entry : nope8.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack bottle = new ItemStack(Material.EXPERIENCE_BOTTLE, 64);
        final HashMap<Integer, ItemStack> nope9 = player.getInventory().addItem(bottle);
        for (final Entry<Integer, ItemStack> entry : nope9.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack caixaBasica = new ItemStack(Material.CHEST, 2);
        final ItemMeta metaCaixaAva = caixaBasica.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Básica");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaBasica.setItemMeta(metaCaixaAva);
        final HashMap<Integer, ItemStack> nope10 = player.getInventory().addItem(caixaBasica);
        for (final Entry<Integer, ItemStack> entry : nope10.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 2);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);
        final HashMap<Integer, ItemStack> nope11 = player.getInventory().addItem(chave);
        for (final Entry<Integer, ItemStack> entry : nope11.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
    }

    public static Inventory inventoryKitDiarioTitan() {
        final Inventory inventory = Bukkit.createInventory(null, 4 * 9, "Kit diário (Titan): ");

        final ItemStack picareta = new ItemStack(Material.IRON_PICKAXE);
        final ItemStack machado = new ItemStack(Material.IRON_AXE);
        final ItemStack pa = new ItemStack(Material.IRON_SHOVEL);
        final ItemStack carne = new ItemStack(Material.COOKED_BEEF, 32);
        final ItemStack appleGolden = new ItemStack(Material.GOLDEN_APPLE, 20);
        final ItemStack wood = new ItemStack(Material.OAK_LOG, 64);

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta meta2 = moeda.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "$2.000,00 moedas");
        moeda.setItemMeta(meta2);

        final ItemStack moedaNe = new ItemStack(Material.GHAST_TEAR);
        final ItemMeta meta3 = moedaNe.getItemMeta();
        meta3.setDisplayName(ChatColor.YELLOW + "$100,00 moedas negra");
        moedaNe.setItemMeta(meta3);

        final ItemStack random = new ItemStack(Material.ENCHANTED_BOOK);
        final ItemMeta meta4 = random.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Livro sortido");
        random.setItemMeta(meta4);

        final ItemStack bottle = new ItemStack(Material.EXPERIENCE_BOTTLE, 64);

        final ItemStack caixaBasica = new ItemStack(Material.CHEST, 2);
        final ItemMeta metaCaixaAva = caixaBasica.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Básica");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaBasica.setItemMeta(metaCaixaAva);

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 2);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta10 = voltar.getItemMeta();
        meta10.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta10);

        inventory.setItem(10, picareta);
        inventory.setItem(11, machado);
        inventory.setItem(12, pa);
        inventory.setItem(13, carne);
        inventory.setItem(14, appleGolden);
        inventory.setItem(15, wood);
        inventory.setItem(16, moeda);
        inventory.setItem(20, moedaNe);
        inventory.setItem(21, random);
        inventory.setItem(22, bottle);
        inventory.setItem(23, caixaBasica);
        inventory.setItem(24, chave);
        inventory.setItem(27, voltar);

        return inventory;
    }

    public static void giveKitDiarioLord(final Player player) {
        final ItemStack picareta = new ItemStack(Material.IRON_PICKAXE);
        final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(picareta);
        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack machado = new ItemStack(Material.IRON_AXE);
        final HashMap<Integer, ItemStack> nope1 = player.getInventory().addItem(machado);
        for (final Entry<Integer, ItemStack> entry : nope1.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack pa = new ItemStack(Material.IRON_SHOVEL);
        final HashMap<Integer, ItemStack> nope2 = player.getInventory().addItem(pa);
        for (final Entry<Integer, ItemStack> entry : nope2.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack carne = new ItemStack(Material.COOKED_BEEF, 24);
        final HashMap<Integer, ItemStack> nope3 = player.getInventory().addItem(carne);
        for (final Entry<Integer, ItemStack> entry : nope3.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack appleGolden = new ItemStack(Material.GOLDEN_APPLE, 15);
        final HashMap<Integer, ItemStack> nope4 = player.getInventory().addItem(appleGolden);
        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack wood = new ItemStack(Material.OAK_LOG, 48);
        final HashMap<Integer, ItemStack> nope5 = player.getInventory().addItem(wood);
        for (final Entry<Integer, ItemStack> entry : nope5.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta meta2 = moeda.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "$2.000,00 moedas");
        moeda.setItemMeta(meta2);
        final HashMap<Integer, ItemStack> nope6 = player.getInventory().addItem(moeda);
        for (final Entry<Integer, ItemStack> entry : nope6.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack moedaNe = new ItemStack(Material.GHAST_TEAR);
        final ItemMeta meta3 = moedaNe.getItemMeta();
        meta3.setDisplayName(ChatColor.YELLOW + "$100,00 moedas negra");
        moedaNe.setItemMeta(meta3);
        final HashMap<Integer, ItemStack> nope7 = player.getInventory().addItem(moedaNe);
        for (final Entry<Integer, ItemStack> entry : nope7.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack random = new ItemStack(Material.ENCHANTED_BOOK);
        final ItemMeta meta4 = random.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Livro sortido");
        random.setItemMeta(meta4);
        final HashMap<Integer, ItemStack> nope8 = player.getInventory().addItem(random);
        for (final Entry<Integer, ItemStack> entry : nope8.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack bottle = new ItemStack(Material.EXPERIENCE_BOTTLE, 48);
        final HashMap<Integer, ItemStack> nope9 = player.getInventory().addItem(bottle);
        for (final Entry<Integer, ItemStack> entry : nope9.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack caixaBasica = new ItemStack(Material.CHEST, 2);
        final ItemMeta metaCaixaAva = caixaBasica.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Básica");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaBasica.setItemMeta(metaCaixaAva);
        final HashMap<Integer, ItemStack> nope10 = player.getInventory().addItem(caixaBasica);
        for (final Entry<Integer, ItemStack> entry : nope10.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 1);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);
        final HashMap<Integer, ItemStack> nope11 = player.getInventory().addItem(chave);
        for (final Entry<Integer, ItemStack> entry : nope11.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
    }

    public static Inventory inventoryKitDiarioLord() {
        final Inventory inventory = Bukkit.createInventory(null, 4 * 9, "Kit diário (Lord): ");

        final ItemStack picareta = new ItemStack(Material.IRON_PICKAXE);
        final ItemStack machado = new ItemStack(Material.IRON_AXE);
        final ItemStack pa = new ItemStack(Material.IRON_SHOVEL);
        final ItemStack carne = new ItemStack(Material.COOKED_BEEF, 24);
        final ItemStack appleGolden = new ItemStack(Material.GOLDEN_APPLE, 15);
        final ItemStack wood = new ItemStack(Material.OAK_LOG, 48);

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta meta2 = moeda.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "$2.000,00 moedas");
        moeda.setItemMeta(meta2);

        final ItemStack moedaNe = new ItemStack(Material.GHAST_TEAR);
        final ItemMeta meta3 = moedaNe.getItemMeta();
        meta3.setDisplayName(ChatColor.YELLOW + "$100,00 moedas negra");
        moedaNe.setItemMeta(meta3);

        final ItemStack random = new ItemStack(Material.ENCHANTED_BOOK);
        final ItemMeta meta4 = random.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Livro sortido");
        random.setItemMeta(meta4);

        final ItemStack bottle = new ItemStack(Material.EXPERIENCE_BOTTLE, 48);

        final ItemStack caixaBasica = new ItemStack(Material.CHEST, 2);
        final ItemMeta metaCaixaAva = caixaBasica.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Básica");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaBasica.setItemMeta(metaCaixaAva);

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 1);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta10 = voltar.getItemMeta();
        meta10.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta10);

        inventory.setItem(10, picareta);
        inventory.setItem(11, machado);
        inventory.setItem(12, pa);
        inventory.setItem(13, carne);
        inventory.setItem(14, appleGolden);
        inventory.setItem(15, wood);
        inventory.setItem(16, moeda);
        inventory.setItem(20, moedaNe);
        inventory.setItem(21, random);
        inventory.setItem(22, bottle);
        inventory.setItem(23, caixaBasica);
        inventory.setItem(24, chave);
        inventory.setItem(27, voltar);

        return inventory;
    }

    public static void giveKitDiarioConde(final Player player) {
        final ItemStack picareta = new ItemStack(Material.IRON_PICKAXE);
        final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(picareta);
        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack machado = new ItemStack(Material.IRON_AXE);
        final HashMap<Integer, ItemStack> nope1 = player.getInventory().addItem(machado);
        for (final Entry<Integer, ItemStack> entry : nope1.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack pa = new ItemStack(Material.IRON_SHOVEL);
        final HashMap<Integer, ItemStack> nope2 = player.getInventory().addItem(pa);
        for (final Entry<Integer, ItemStack> entry : nope2.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack carne = new ItemStack(Material.COOKED_BEEF, 16);
        final HashMap<Integer, ItemStack> nope3 = player.getInventory().addItem(carne);
        for (final Entry<Integer, ItemStack> entry : nope3.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack appleGolden = new ItemStack(Material.GOLDEN_APPLE, 10);
        final HashMap<Integer, ItemStack> nope4 = player.getInventory().addItem(appleGolden);
        for (final Entry<Integer, ItemStack> entry : nope4.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack wood = new ItemStack(Material.OAK_LOG, 32);
        final HashMap<Integer, ItemStack> nope5 = player.getInventory().addItem(wood);
        for (final Entry<Integer, ItemStack> entry : nope5.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta meta2 = moeda.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "$2.000,00 moedas");
        moeda.setItemMeta(meta2);
        final HashMap<Integer, ItemStack> nope6 = player.getInventory().addItem(moeda);
        for (final Entry<Integer, ItemStack> entry : nope6.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack moedaNe = new ItemStack(Material.GHAST_TEAR);
        final ItemMeta meta3 = moedaNe.getItemMeta();
        meta3.setDisplayName(ChatColor.YELLOW + "$100,00 moedas negra");
        moedaNe.setItemMeta(meta3);
        final HashMap<Integer, ItemStack> nope7 = player.getInventory().addItem(moedaNe);
        for (final Entry<Integer, ItemStack> entry : nope7.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack random = new ItemStack(Material.ENCHANTED_BOOK);
        final ItemMeta meta4 = random.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Livro sortido");
        random.setItemMeta(meta4);
        final HashMap<Integer, ItemStack> nope8 = player.getInventory().addItem(random);
        for (final Entry<Integer, ItemStack> entry : nope8.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack bottle = new ItemStack(Material.EXPERIENCE_BOTTLE, 32);
        final HashMap<Integer, ItemStack> nope9 = player.getInventory().addItem(bottle);
        for (final Entry<Integer, ItemStack> entry : nope9.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack caixaBasica = new ItemStack(Material.CHEST, 1);
        final ItemMeta metaCaixaAva = caixaBasica.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Básica");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaBasica.setItemMeta(metaCaixaAva);
        final HashMap<Integer, ItemStack> nope10 = player.getInventory().addItem(caixaBasica);
        for (final Entry<Integer, ItemStack> entry : nope10.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 1);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);
        final HashMap<Integer, ItemStack> nope11 = player.getInventory().addItem(chave);
        for (final Entry<Integer, ItemStack> entry : nope11.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
    }

    public static Inventory inventoryKitDiarioConde() {
        final Inventory inventory = Bukkit.createInventory(null, 4 * 9, "Kit diário (Conde): ");

        final ItemStack picareta = new ItemStack(Material.IRON_PICKAXE);
        final ItemStack machado = new ItemStack(Material.IRON_AXE);
        final ItemStack pa = new ItemStack(Material.IRON_SHOVEL);
        final ItemStack carne = new ItemStack(Material.COOKED_BEEF, 16);
        final ItemStack appleGolden = new ItemStack(Material.GOLDEN_APPLE, 10);
        final ItemStack wood = new ItemStack(Material.OAK_LOG, 32);

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta meta2 = moeda.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "$2.000,00 moedas");
        moeda.setItemMeta(meta2);

        final ItemStack moedaNe = new ItemStack(Material.GHAST_TEAR);
        final ItemMeta meta3 = moedaNe.getItemMeta();
        meta3.setDisplayName(ChatColor.YELLOW + "$100,00 moedas negra");
        moedaNe.setItemMeta(meta3);

        final ItemStack random = new ItemStack(Material.ENCHANTED_BOOK);
        final ItemMeta meta4 = random.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Livro sortido");
        random.setItemMeta(meta4);

        final ItemStack bottle = new ItemStack(Material.EXPERIENCE_BOTTLE, 32);

        final ItemStack caixaBasica = new ItemStack(Material.CHEST, 1);
        final ItemMeta metaCaixaAva = caixaBasica.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Básica");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaBasica.setItemMeta(metaCaixaAva);

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 1);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta10 = voltar.getItemMeta();
        meta10.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta10);

        inventory.setItem(10, picareta);
        inventory.setItem(11, machado);
        inventory.setItem(12, pa);
        inventory.setItem(13, carne);
        inventory.setItem(14, appleGolden);
        inventory.setItem(15, wood);
        inventory.setItem(16, moeda);
        inventory.setItem(20, moedaNe);
        inventory.setItem(21, random);
        inventory.setItem(22, bottle);
        inventory.setItem(23, caixaBasica);
        inventory.setItem(24, chave);
        inventory.setItem(27, voltar);

        return inventory;
    }

    //-
    public static void giveKitSemanalDuque(final Player player) {
        final ItemStack espada = new ItemStack(Material.IRON_SWORD);
        espada.addEnchantment(Enchantment.DAMAGE_ALL, 2);
        espada.addEnchantment(Enchantment.DURABILITY, 2);
        final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(espada);
        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack capacete = new ItemStack(Material.IRON_HELMET);
        capacete.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        capacete.addEnchantment(Enchantment.DURABILITY, 2);
        final HashMap<Integer, ItemStack> nope1 = player.getInventory().addItem(capacete);
        for (final Entry<Integer, ItemStack> entry : nope1.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack peitoral = new ItemStack(Material.IRON_CHESTPLATE);
        peitoral.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        peitoral.addEnchantment(Enchantment.DURABILITY, 2);
        final HashMap<Integer, ItemStack> nope2 = player.getInventory().addItem(peitoral);
        for (final Entry<Integer, ItemStack> entry : nope2.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack calca = new ItemStack(Material.IRON_LEGGINGS);
        calca.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        calca.addEnchantment(Enchantment.DURABILITY, 2);
        final HashMap<Integer, ItemStack> nope3 = player.getInventory().addItem(calca);
        for (final Entry<Integer, ItemStack> entry : nope3.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack botas = new ItemStack(Material.IRON_BOOTS);
        botas.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        botas.addEnchantment(Enchantment.DURABILITY, 2);
        final HashMap<Integer, ItemStack> nope4 = player.getInventory().addItem(botas);
        for (final Entry<Integer, ItemStack> entry : nope4.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack caixaAvancada = new ItemStack(Material.ENDER_CHEST, 6);
        final ItemMeta metaCaixaAva = caixaAvancada.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Avançada");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaAvancada.setItemMeta(metaCaixaAva);
        final HashMap<Integer, ItemStack> nope5 = player.getInventory().addItem(caixaAvancada);
        for (final Entry<Integer, ItemStack> entry : nope5.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 6);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);
        final HashMap<Integer, ItemStack> nope6 = player.getInventory().addItem(chave);
        for (final Entry<Integer, ItemStack> entry : nope6.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack ghasts = new ItemStack(Material.GHAST_SPAWN_EGG, 8);
        final HashMap<Integer, ItemStack> nope7 = player.getInventory().addItem(ghasts);
        for (final Entry<Integer, ItemStack> entry : nope7.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack regen = new ItemStack(Material.POTION);
        final PotionMeta meta2 = (PotionMeta) regen.getItemMeta();
        final PotionEffect reg = new PotionEffect(PotionEffectType.REGENERATION, 1000, 1);
        meta2.addCustomEffect(reg, true);
        regen.setItemMeta(meta2);
        final HashMap<Integer, ItemStack> nope8 = player.getInventory().addItem(regen);
        for (final Entry<Integer, ItemStack> entry : nope8.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope18 = player.getInventory().addItem(regen);
        for (final Entry<Integer, ItemStack> entry : nope18.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack forca = new ItemStack(Material.POTION);
        final PotionMeta meta3 = (PotionMeta) forca.getItemMeta();
        final PotionEffect strengh = new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 1000, 1);
        meta3.addCustomEffect(strengh, true);
        forca.setItemMeta(meta3);
        final HashMap<Integer, ItemStack> nope9 = player.getInventory().addItem(forca);
        for (final Entry<Integer, ItemStack> entry : nope9.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack torre = new ItemStack(Material.STONE, 6);
        final ItemMeta meta4 = torre.getItemMeta();
        meta4.setDisplayName(ChatColor.GREEN + "Torre de fortificação");
        final List<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.GRAY + "Aumenta a defesa em: " + ChatColor.GREEN + "2 pontos");
        lore4.add(ChatColor.GRAY + "Dano médio a combatentes: " + ChatColor.GREEN + "2.5");
        lore4.add(ChatColor.GRAY + "Em ataque spawna: " + ChatColor.GREEN + "1 mob á cd/ 15s");
        lore4.add(" ");
        meta4.setLore(lore4);
        torre.setItemMeta(meta4);
        final HashMap<Integer, ItemStack> nope10 = player.getInventory().addItem(torre);
        for (final Entry<Integer, ItemStack> entry : nope10.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
    }

    public static Inventory inventoryKitSemanalDuque() {
        final Inventory inventory = Bukkit.createInventory(null, 4 * 9, "Kit semanal (Duque): ");

        final ItemStack espada = new ItemStack(Material.IRON_SWORD);
        espada.addEnchantment(Enchantment.DAMAGE_ALL, 2);
        espada.addEnchantment(Enchantment.DURABILITY, 2);

        final ItemStack capacete = new ItemStack(Material.IRON_HELMET);
        capacete.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        capacete.addEnchantment(Enchantment.DURABILITY, 2);

        final ItemStack peitoral = new ItemStack(Material.IRON_CHESTPLATE);
        peitoral.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        peitoral.addEnchantment(Enchantment.DURABILITY, 2);

        final ItemStack calca = new ItemStack(Material.IRON_LEGGINGS);
        calca.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        calca.addEnchantment(Enchantment.DURABILITY, 2);

        final ItemStack botas = new ItemStack(Material.IRON_BOOTS);
        botas.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        botas.addEnchantment(Enchantment.DURABILITY, 2);

        final ItemStack caixaAvancada = new ItemStack(Material.ENDER_CHEST, 6);
        final ItemMeta metaCaixaAva = caixaAvancada.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Avançada");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaAvancada.setItemMeta(metaCaixaAva);

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 6);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);

        final ItemStack ghasts = new ItemStack(Material.GHAST_SPAWN_EGG, 8);

        final ItemStack regen = new ItemStack(Material.POTION);
        final PotionMeta meta2 = (PotionMeta) regen.getItemMeta();
        final PotionEffect reg = new PotionEffect(PotionEffectType.REGENERATION, 1000, 1);
        meta2.addCustomEffect(reg, true);
        regen.setItemMeta(meta2);

        final ItemStack forca = new ItemStack(Material.POTION);
        final PotionMeta meta3 = (PotionMeta) forca.getItemMeta();
        final PotionEffect strengh = new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 1000, 1);
        meta3.addCustomEffect(strengh, true);
        forca.setItemMeta(meta3);

        final ItemStack torre = new ItemStack(Material.STONE, 6);
        final ItemMeta meta4 = torre.getItemMeta();
        meta4.setDisplayName(ChatColor.GREEN + "Torre de fortificação");
        final List<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.GRAY + "Aumenta a defesa em: " + ChatColor.GREEN + "2 pontos");
        lore4.add(ChatColor.GRAY + "Dano médio a combatentes: " + ChatColor.GREEN + "2.5");
        lore4.add(ChatColor.GRAY + "Em ataque spawna: " + ChatColor.GREEN + "1 mob á cd/ 15s");
        lore4.add(" ");
        meta4.setLore(lore4);
        torre.setItemMeta(meta4);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta10 = voltar.getItemMeta();
        meta10.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta10);

        inventory.setItem(10, espada);
        inventory.setItem(11, capacete);
        inventory.setItem(12, peitoral);
        inventory.setItem(13, calca);
        inventory.setItem(14, botas);
        inventory.setItem(15, caixaAvancada);
        inventory.setItem(16, chave);
        inventory.setItem(20, ghasts);
        inventory.setItem(21, regen);
        inventory.setItem(22, regen);
        inventory.setItem(23, forca);
        inventory.setItem(24, torre);
        inventory.setItem(27, voltar);

        return inventory;
    }
    //-

    public static void giveKitSemanalTitan(final Player player) {
        final ItemStack espada = new ItemStack(Material.IRON_SWORD);
        espada.addEnchantment(Enchantment.DAMAGE_ALL, 2);
        espada.addEnchantment(Enchantment.DURABILITY, 1);
        final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(espada);
        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack capacete = new ItemStack(Material.IRON_HELMET);
        capacete.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        capacete.addEnchantment(Enchantment.DURABILITY, 1);
        final HashMap<Integer, ItemStack> nope1 = player.getInventory().addItem(capacete);
        for (final Entry<Integer, ItemStack> entry : nope1.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack peitoral = new ItemStack(Material.IRON_CHESTPLATE);
        peitoral.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        peitoral.addEnchantment(Enchantment.DURABILITY, 1);
        final HashMap<Integer, ItemStack> nope2 = player.getInventory().addItem(peitoral);
        for (final Entry<Integer, ItemStack> entry : nope2.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack calca = new ItemStack(Material.IRON_LEGGINGS);
        calca.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        calca.addEnchantment(Enchantment.DURABILITY, 1);
        final HashMap<Integer, ItemStack> nope3 = player.getInventory().addItem(calca);
        for (final Entry<Integer, ItemStack> entry : nope3.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack botas = new ItemStack(Material.IRON_BOOTS);
        botas.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        botas.addEnchantment(Enchantment.DURABILITY, 1);
        final HashMap<Integer, ItemStack> nope4 = player.getInventory().addItem(botas);
        for (final Entry<Integer, ItemStack> entry : nope4.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack caixaAvancada = new ItemStack(Material.ENDER_CHEST, 4);
        final ItemMeta metaCaixaAva = caixaAvancada.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Avançada");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaAvancada.setItemMeta(metaCaixaAva);
        final HashMap<Integer, ItemStack> nope5 = player.getInventory().addItem(caixaAvancada);
        for (final Entry<Integer, ItemStack> entry : nope5.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 4);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);
        final HashMap<Integer, ItemStack> nope6 = player.getInventory().addItem(chave);
        for (final Entry<Integer, ItemStack> entry : nope6.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack ghasts = new ItemStack(Material.GHAST_SPAWN_EGG, 4);
        final HashMap<Integer, ItemStack> nope7 = player.getInventory().addItem(ghasts);
        for (final Entry<Integer, ItemStack> entry : nope7.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack regen = new ItemStack(Material.POTION);
        final PotionMeta meta2 = (PotionMeta) regen.getItemMeta();
        final PotionEffect reg = new PotionEffect(PotionEffectType.REGENERATION, 1000, 1);
        meta2.addCustomEffect(reg, true);
        regen.setItemMeta(meta2);
        final HashMap<Integer, ItemStack> nope8 = player.getInventory().addItem(regen);
        for (final Entry<Integer, ItemStack> entry : nope8.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack forca = new ItemStack(Material.POTION);
        final PotionMeta meta3 = (PotionMeta) forca.getItemMeta();
        final PotionEffect strengh = new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 1000, 1);
        meta3.addCustomEffect(strengh, true);
        forca.setItemMeta(meta3);
        final HashMap<Integer, ItemStack> nope9 = player.getInventory().addItem(forca);
        for (final Entry<Integer, ItemStack> entry : nope9.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack torre = new ItemStack(Material.STONE, 4);
        final ItemMeta meta4 = torre.getItemMeta();
        meta4.setDisplayName(ChatColor.GREEN + "Torre de fortificação");
        final List<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.GRAY + "Aumenta a defesa em: " + ChatColor.GREEN + "2 pontos");
        lore4.add(ChatColor.GRAY + "Dano médio a combatentes: " + ChatColor.GREEN + "2.5");
        lore4.add(ChatColor.GRAY + "Em ataque spawna: " + ChatColor.GREEN + "1 mob á cd/ 15s");
        lore4.add(" ");
        meta4.setLore(lore4);
        torre.setItemMeta(meta4);
        final HashMap<Integer, ItemStack> nope10 = player.getInventory().addItem(torre);
        for (final Entry<Integer, ItemStack> entry : nope10.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
    }

    public static Inventory inventoryKitSemanalTitan() {
        final Inventory inventory = Bukkit.createInventory(null, 4 * 9, "Kit semanal (Titan): ");

        final ItemStack espada = new ItemStack(Material.IRON_SWORD);
        espada.addEnchantment(Enchantment.DAMAGE_ALL, 2);
        espada.addEnchantment(Enchantment.DURABILITY, 1);

        final ItemStack capacete = new ItemStack(Material.IRON_HELMET);
        capacete.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        capacete.addEnchantment(Enchantment.DURABILITY, 1);

        final ItemStack peitoral = new ItemStack(Material.IRON_CHESTPLATE);
        peitoral.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        peitoral.addEnchantment(Enchantment.DURABILITY, 1);

        final ItemStack calca = new ItemStack(Material.IRON_LEGGINGS);
        calca.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        calca.addEnchantment(Enchantment.DURABILITY, 1);

        final ItemStack botas = new ItemStack(Material.IRON_BOOTS);
        botas.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        botas.addEnchantment(Enchantment.DURABILITY, 1);

        final ItemStack caixaAvancada = new ItemStack(Material.ENDER_CHEST, 4);
        final ItemMeta metaCaixaAva = caixaAvancada.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Avançada");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaAvancada.setItemMeta(metaCaixaAva);

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 4);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);

        final ItemStack ghasts = new ItemStack(Material.GHAST_SPAWN_EGG, 4);

        final ItemStack regen = new ItemStack(Material.POTION);
        final PotionMeta meta2 = (PotionMeta) regen.getItemMeta();
        final PotionEffect reg = new PotionEffect(PotionEffectType.REGENERATION, 1000, 1);
        meta2.addCustomEffect(reg, true);
        regen.setItemMeta(meta2);

        final ItemStack forca = new ItemStack(Material.POTION);
        final PotionMeta meta3 = (PotionMeta) forca.getItemMeta();
        final PotionEffect strengh = new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 1000, 1);
        meta3.addCustomEffect(strengh, true);
        forca.setItemMeta(meta3);

        final ItemStack torre = new ItemStack(Material.STONE, 4);
        final ItemMeta meta4 = torre.getItemMeta();
        meta4.setDisplayName(ChatColor.GREEN + "Torre de fortificação");
        final List<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.GRAY + "Aumenta a defesa em: " + ChatColor.GREEN + "2 pontos");
        lore4.add(ChatColor.GRAY + "Dano médio a combatentes: " + ChatColor.GREEN + "2.5");
        lore4.add(ChatColor.GRAY + "Em ataque spawna: " + ChatColor.GREEN + "1 mob á cd/ 15s");
        lore4.add(" ");
        meta4.setLore(lore4);
        torre.setItemMeta(meta4);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta10 = voltar.getItemMeta();
        meta10.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta10);

        inventory.setItem(10, espada);
        inventory.setItem(11, capacete);
        inventory.setItem(12, peitoral);
        inventory.setItem(13, calca);
        inventory.setItem(14, botas);
        inventory.setItem(15, caixaAvancada);
        inventory.setItem(16, chave);
        inventory.setItem(20, ghasts);
        inventory.setItem(21, regen);
        inventory.setItem(22, forca);
        inventory.setItem(23, forca);
        inventory.setItem(24, torre);
        inventory.setItem(27, voltar);

        return inventory;
    }

    public static void giveKitSemanalLord(final Player player) {
        final ItemStack espada = new ItemStack(Material.IRON_SWORD);
        espada.addEnchantment(Enchantment.DAMAGE_ALL, 1);
        espada.addEnchantment(Enchantment.DURABILITY, 1);
        final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(espada);
        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack capacete = new ItemStack(Material.IRON_HELMET);
        capacete.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        capacete.addEnchantment(Enchantment.DURABILITY, 1);
        final HashMap<Integer, ItemStack> nope1 = player.getInventory().addItem(capacete);
        for (final Entry<Integer, ItemStack> entry : nope1.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack peitoral = new ItemStack(Material.IRON_CHESTPLATE);
        peitoral.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        peitoral.addEnchantment(Enchantment.DURABILITY, 1);
        final HashMap<Integer, ItemStack> nope2 = player.getInventory().addItem(peitoral);
        for (final Entry<Integer, ItemStack> entry : nope2.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack calca = new ItemStack(Material.IRON_LEGGINGS);
        calca.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        calca.addEnchantment(Enchantment.DURABILITY, 1);
        final HashMap<Integer, ItemStack> nope3 = player.getInventory().addItem(calca);
        for (final Entry<Integer, ItemStack> entry : nope3.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack botas = new ItemStack(Material.IRON_BOOTS);
        botas.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        botas.addEnchantment(Enchantment.DURABILITY, 1);
        final HashMap<Integer, ItemStack> nope4 = player.getInventory().addItem(botas);
        for (final Entry<Integer, ItemStack> entry : nope4.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack caixaAvancada = new ItemStack(Material.ENDER_CHEST, 4);
        final ItemMeta metaCaixaAva = caixaAvancada.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Avançada");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaAvancada.setItemMeta(metaCaixaAva);
        final HashMap<Integer, ItemStack> nope5 = player.getInventory().addItem(caixaAvancada);
        for (final Entry<Integer, ItemStack> entry : nope5.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 2);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);
        final HashMap<Integer, ItemStack> nope6 = player.getInventory().addItem(chave);
        for (final Entry<Integer, ItemStack> entry : nope6.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack ghasts = new ItemStack(Material.GHAST_SPAWN_EGG, 4);
        final HashMap<Integer, ItemStack> nope7 = player.getInventory().addItem(ghasts);
        for (final Entry<Integer, ItemStack> entry : nope7.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack regen = new ItemStack(Material.POTION);
        final PotionMeta meta2 = (PotionMeta) regen.getItemMeta();
        final PotionEffect reg = new PotionEffect(PotionEffectType.REGENERATION, 1000, 1);
        meta2.addCustomEffect(reg, true);
        regen.setItemMeta(meta2);
        final HashMap<Integer, ItemStack> nope8 = player.getInventory().addItem(regen);
        for (final Entry<Integer, ItemStack> entry : nope8.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack forca = new ItemStack(Material.POTION);
        final PotionMeta meta3 = (PotionMeta) forca.getItemMeta();
        final PotionEffect strengh = new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 1000, 1);
        meta3.addCustomEffect(strengh, true);
        forca.setItemMeta(meta3);
        final HashMap<Integer, ItemStack> nope9 = player.getInventory().addItem(forca);
        for (final Entry<Integer, ItemStack> entry : nope9.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack torre = new ItemStack(Material.STONE, 2);
        final ItemMeta meta4 = torre.getItemMeta();
        meta4.setDisplayName(ChatColor.GREEN + "Torre de fortificação");
        final List<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.GRAY + "Aumenta a defesa em: " + ChatColor.GREEN + "2 pontos");
        lore4.add(ChatColor.GRAY + "Dano médio a combatentes: " + ChatColor.GREEN + "2.5");
        lore4.add(ChatColor.GRAY + "Em ataque spawna: " + ChatColor.GREEN + "1 mob á cd/ 15s");
        lore4.add(" ");
        meta4.setLore(lore4);
        torre.setItemMeta(meta4);
        final HashMap<Integer, ItemStack> nope10 = player.getInventory().addItem(torre);
        for (final Entry<Integer, ItemStack> entry : nope10.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
    }

    public static Inventory inventoryKitSemanalLord() {
        final Inventory inventory = Bukkit.createInventory(null, 4 * 9, "Kit semanal (Lord): ");

        final ItemStack espada = new ItemStack(Material.IRON_SWORD);
        espada.addEnchantment(Enchantment.DAMAGE_ALL, 1);
        espada.addEnchantment(Enchantment.DURABILITY, 1);

        final ItemStack capacete = new ItemStack(Material.IRON_HELMET);
        capacete.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        capacete.addEnchantment(Enchantment.DURABILITY, 1);

        final ItemStack peitoral = new ItemStack(Material.IRON_CHESTPLATE);
        peitoral.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        peitoral.addEnchantment(Enchantment.DURABILITY, 1);

        final ItemStack calca = new ItemStack(Material.IRON_LEGGINGS);
        calca.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        calca.addEnchantment(Enchantment.DURABILITY, 1);

        final ItemStack botas = new ItemStack(Material.IRON_BOOTS);
        botas.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        botas.addEnchantment(Enchantment.DURABILITY, 1);

        final ItemStack caixaAvancada = new ItemStack(Material.ENDER_CHEST, 4);
        final ItemMeta metaCaixaAva = caixaAvancada.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Avançada");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaAvancada.setItemMeta(metaCaixaAva);

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 2);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);

        final ItemStack ghasts = new ItemStack(Material.GHAST_SPAWN_EGG, 4);

        final ItemStack regen = new ItemStack(Material.POTION);
        final PotionMeta meta2 = (PotionMeta) regen.getItemMeta();
        final PotionEffect reg = new PotionEffect(PotionEffectType.REGENERATION, 1000, 1);
        meta2.addCustomEffect(reg, true);
        regen.setItemMeta(meta2);

        final ItemStack forca = new ItemStack(Material.POTION);
        final PotionMeta meta3 = (PotionMeta) forca.getItemMeta();
        final PotionEffect strengh = new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 1000, 1);
        meta3.addCustomEffect(strengh, true);
        forca.setItemMeta(meta3);

        final ItemStack torre = new ItemStack(Material.STONE, 2);
        final ItemMeta meta4 = torre.getItemMeta();
        meta4.setDisplayName(ChatColor.GREEN + "Torre de fortificação");
        final List<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.GRAY + "Aumenta a defesa em: " + ChatColor.GREEN + "2 pontos");
        lore4.add(ChatColor.GRAY + "Dano médio a combatentes: " + ChatColor.GREEN + "2.5");
        lore4.add(ChatColor.GRAY + "Em ataque spawna: " + ChatColor.GREEN + "1 mob á cd/ 15s");
        lore4.add(" ");
        meta4.setLore(lore4);
        torre.setItemMeta(meta4);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta10 = voltar.getItemMeta();
        meta10.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta10);

        inventory.setItem(10, espada);
        inventory.setItem(11, capacete);
        inventory.setItem(12, peitoral);
        inventory.setItem(13, calca);
        inventory.setItem(14, botas);
        inventory.setItem(15, caixaAvancada);
        inventory.setItem(16, chave);
        inventory.setItem(20, ghasts);
        inventory.setItem(21, regen);
        inventory.setItem(22, forca);
        inventory.setItem(23, forca);
        inventory.setItem(24, torre);
        inventory.setItem(27, voltar);

        return inventory;
    }

    public static void giveKitSemanalConde(final Player player) {
        final ItemStack espada = new ItemStack(Material.IRON_SWORD);
        espada.addEnchantment(Enchantment.DAMAGE_ALL, 1);
        final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(espada);
        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack capacete = new ItemStack(Material.IRON_HELMET);
        capacete.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        final HashMap<Integer, ItemStack> nope1 = player.getInventory().addItem(capacete);
        for (final Entry<Integer, ItemStack> entry : nope1.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack peitoral = new ItemStack(Material.IRON_CHESTPLATE);
        peitoral.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        final HashMap<Integer, ItemStack> nope2 = player.getInventory().addItem(peitoral);
        for (final Entry<Integer, ItemStack> entry : nope2.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack calca = new ItemStack(Material.IRON_LEGGINGS);
        calca.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        final HashMap<Integer, ItemStack> nope3 = player.getInventory().addItem(calca);
        for (final Entry<Integer, ItemStack> entry : nope3.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack botas = new ItemStack(Material.IRON_BOOTS);
        botas.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        final HashMap<Integer, ItemStack> nope4 = player.getInventory().addItem(botas);
        for (final Entry<Integer, ItemStack> entry : nope4.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack caixaAvancada = new ItemStack(Material.ENDER_CHEST, 2);
        final ItemMeta metaCaixaAva = caixaAvancada.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Avançada");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaAvancada.setItemMeta(metaCaixaAva);
        final HashMap<Integer, ItemStack> nope5 = player.getInventory().addItem(caixaAvancada);
        for (final Entry<Integer, ItemStack> entry : nope5.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 1);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);
        final HashMap<Integer, ItemStack> nope6 = player.getInventory().addItem(chave);
        for (final Entry<Integer, ItemStack> entry : nope6.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack ghasts = new ItemStack(Material.GHAST_SPAWN_EGG, 4);
        final HashMap<Integer, ItemStack> nope7 = player.getInventory().addItem(espada);
        for (final Entry<Integer, ItemStack> entry : nope7.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack regen = new ItemStack(Material.POTION);
        final PotionMeta meta2 = (PotionMeta) regen.getItemMeta();
        final PotionEffect reg = new PotionEffect(PotionEffectType.REGENERATION, 1000, 1);
        meta2.addCustomEffect(reg, true);
        regen.setItemMeta(meta2);
        final HashMap<Integer, ItemStack> nope8 = player.getInventory().addItem(regen);
        for (final Entry<Integer, ItemStack> entry : nope8.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack forca = new ItemStack(Material.POTION);
        final PotionMeta meta3 = (PotionMeta) forca.getItemMeta();
        final PotionEffect strengh = new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 1000, 1);
        meta3.addCustomEffect(strengh, true);
        forca.setItemMeta(meta3);
        final HashMap<Integer, ItemStack> nope9 = player.getInventory().addItem(forca);
        for (final Entry<Integer, ItemStack> entry : nope9.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack torre = new ItemStack(Material.STONE);
        final ItemMeta meta4 = torre.getItemMeta();
        meta4.setDisplayName(ChatColor.GREEN + "Torre de fortificação");
        final List<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.GRAY + "Aumenta a defesa em: " + ChatColor.GREEN + "2 pontos");
        lore4.add(ChatColor.GRAY + "Dano médio a combatentes: " + ChatColor.GREEN + "2.5");
        lore4.add(ChatColor.GRAY + "Em ataque spawna: " + ChatColor.GREEN + "1 mob á cd/ 15s");
        lore4.add(" ");
        meta4.setLore(lore4);
        torre.setItemMeta(meta4);
        final HashMap<Integer, ItemStack> nope10 = player.getInventory().addItem(torre);
        for (final Entry<Integer, ItemStack> entry : nope10.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
    }

    public static Inventory inventoryKitSemanalConde() {
        final Inventory inventory = Bukkit.createInventory(null, 4 * 9, "Kit semanal (Conde): ");

        final ItemStack espada = new ItemStack(Material.IRON_SWORD);
        espada.addEnchantment(Enchantment.DAMAGE_ALL, 1);

        final ItemStack capacete = new ItemStack(Material.IRON_HELMET);
        capacete.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);

        final ItemStack peitoral = new ItemStack(Material.IRON_CHESTPLATE);
        peitoral.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);

        final ItemStack calca = new ItemStack(Material.IRON_LEGGINGS);
        calca.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);

        final ItemStack botas = new ItemStack(Material.IRON_BOOTS);
        botas.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);

        final ItemStack caixaAvancada = new ItemStack(Material.ENDER_CHEST, 2);
        final ItemMeta metaCaixaAva = caixaAvancada.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Avançada");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaAvancada.setItemMeta(metaCaixaAva);

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 1);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);

        final ItemStack ghasts = new ItemStack(Material.GHAST_SPAWN_EGG, 4);

        final ItemStack regen = new ItemStack(Material.POTION);
        final PotionMeta meta2 = (PotionMeta) regen.getItemMeta();
        final PotionEffect reg = new PotionEffect(PotionEffectType.REGENERATION, 1000, 1);
        meta2.addCustomEffect(reg, true);
        regen.setItemMeta(meta2);

        final ItemStack forca = new ItemStack(Material.POTION);
        final PotionMeta meta3 = (PotionMeta) forca.getItemMeta();
        final PotionEffect strengh = new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 1000, 1);
        meta3.addCustomEffect(strengh, true);
        forca.setItemMeta(meta3);

        final ItemStack torre = new ItemStack(Material.STONE);
        final ItemMeta meta4 = torre.getItemMeta();
        meta4.setDisplayName(ChatColor.GREEN + "Torre de fortificação");
        final List<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.GRAY + "Aumenta a defesa em: " + ChatColor.GREEN + "2 pontos");
        lore4.add(ChatColor.GRAY + "Dano médio a combatentes: " + ChatColor.GREEN + "2.5");
        lore4.add(ChatColor.GRAY + "Em ataque spawna: " + ChatColor.GREEN + "1 mob á cd/ 15s");
        lore4.add(" ");
        meta4.setLore(lore4);
        torre.setItemMeta(meta4);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta10 = voltar.getItemMeta();
        meta10.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta10);

        inventory.setItem(10, espada);
        inventory.setItem(11, capacete);
        inventory.setItem(12, peitoral);
        inventory.setItem(13, calca);
        inventory.setItem(14, botas);
        inventory.setItem(15, caixaAvancada);
        inventory.setItem(16, chave);
        inventory.setItem(20, ghasts);
        inventory.setItem(21, regen);
        inventory.setItem(22, forca);
        inventory.setItem(23, forca);
        inventory.setItem(24, torre);
        inventory.setItem(27, voltar);

        return inventory;
    }

    public static void giveKitMensalTitan(final Player player) {
        final ItemStack espada = new ItemStack(Material.DIAMOND_SWORD);
        espada.addEnchantment(Enchantment.DURABILITY, 3);
        espada.addEnchantment(Enchantment.DAMAGE_ALL, 4);
        final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(espada);
        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack capacete = new ItemStack(Material.DIAMOND_HELMET);
        capacete.addEnchantment(Enchantment.DURABILITY, 3);
        capacete.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
        final HashMap<Integer, ItemStack> nope1 = player.getInventory().addItem(capacete);
        for (final Entry<Integer, ItemStack> entry : nope1.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack peitoral = new ItemStack(Material.DIAMOND_CHESTPLATE);
        peitoral.addEnchantment(Enchantment.DURABILITY, 3);
        peitoral.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
        final HashMap<Integer, ItemStack> nope2 = player.getInventory().addItem(peitoral);
        for (final Entry<Integer, ItemStack> entry : nope2.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack calca = new ItemStack(Material.DIAMOND_LEGGINGS);
        calca.addEnchantment(Enchantment.DURABILITY, 3);
        calca.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
        final HashMap<Integer, ItemStack> nope3 = player.getInventory().addItem(calca);
        for (final Entry<Integer, ItemStack> entry : nope3.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack botas = new ItemStack(Material.DIAMOND_BOOTS);
        botas.addEnchantment(Enchantment.DURABILITY, 3);
        botas.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
        final HashMap<Integer, ItemStack> nope4 = player.getInventory().addItem(botas);
        for (final Entry<Integer, ItemStack> entry : nope4.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack caixaSpawners = new ItemStack(Material.SPAWNER, 5);
        final ItemMeta metaCaixa = caixaSpawners.getItemMeta();
        metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore = new ArrayList<String>();
        lore.add(" ");
        lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Spawners");
        lore.add(" ");
        metaCaixa.setLore(lore);
        caixaSpawners.setItemMeta(metaCaixa);
        final HashMap<Integer, ItemStack> nope5 = player.getInventory().addItem(caixaSpawners);
        for (final Entry<Integer, ItemStack> entry : nope5.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack caixaArmas = new ItemStack(Material.ENDER_CHEST, 5);
        final ItemMeta metaCaixaAr = caixaArmas.getItemMeta();
        metaCaixaAr.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore2 = new ArrayList<String>();
        lore2.add(" ");
        lore2.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Armas");
        lore2.add(" ");
        metaCaixaAr.setLore(lore2);
        caixaArmas.setItemMeta(metaCaixaAr);
        final HashMap<Integer, ItemStack> nope6 = player.getInventory().addItem(caixaArmas);
        for (final Entry<Integer, ItemStack> entry : nope6.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack caixaAvancada = new ItemStack(Material.ENDER_CHEST, 5);
        final ItemMeta metaCaixaAva = caixaAvancada.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Avançada");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaAvancada.setItemMeta(metaCaixaAva);
        final HashMap<Integer, ItemStack> nope7 = player.getInventory().addItem(caixaAvancada);
        for (final Entry<Integer, ItemStack> entry : nope7.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 15);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);
        final HashMap<Integer, ItemStack> nope8 = player.getInventory().addItem(chave);
        for (final Entry<Integer, ItemStack> entry : nope8.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack ghasts = new ItemStack(Material.GHAST_SPAWN_EGG, 20);
        final HashMap<Integer, ItemStack> nope9 = player.getInventory().addItem(ghasts);
        for (final Entry<Integer, ItemStack> entry : nope9.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack totem = new ItemStack(Material.TOTEM_OF_UNDYING);
        final HashMap<Integer, ItemStack> nope10 = player.getInventory().addItem(totem);
        for (final Entry<Integer, ItemStack> entry : nope10.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope11 = player.getInventory().addItem(totem);
        for (final Entry<Integer, ItemStack> entry : nope11.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack escudo = new ItemStack(Material.SHIELD);
        final HashMap<Integer, ItemStack> nope12 = player.getInventory().addItem(escudo);
        for (final Entry<Integer, ItemStack> entry : nope12.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack elytra = new ItemStack(Material.ELYTRA);
        final HashMap<Integer, ItemStack> nope13 = player.getInventory().addItem(elytra);
        for (final Entry<Integer, ItemStack> entry : nope13.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack box = new ItemStack(Material.PURPLE_SHULKER_BOX);
        final HashMap<Integer, ItemStack> nope14 = player.getInventory().addItem(box);
        for (final Entry<Integer, ItemStack> entry : nope14.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope15 = player.getInventory().addItem(box);
        for (final Entry<Integer, ItemStack> entry : nope15.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope16 = player.getInventory().addItem(box);
        for (final Entry<Integer, ItemStack> entry : nope16.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta meta2 = moeda.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "$10.000,00 moedas");
        moeda.setItemMeta(meta2);
        final HashMap<Integer, ItemStack> nope17 = player.getInventory().addItem(moeda);
        for (final Entry<Integer, ItemStack> entry : nope17.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack moedaNe = new ItemStack(Material.GHAST_TEAR);
        final ItemMeta meta3 = moedaNe.getItemMeta();
        meta3.setDisplayName(ChatColor.YELLOW + "$500,00 moedas negra negra");
        moedaNe.setItemMeta(meta3);
        final HashMap<Integer, ItemStack> nope18 = player.getInventory().addItem(moedaNe);
        for (final Entry<Integer, ItemStack> entry : nope18.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack antiRadiacao = new ItemStack(Material.ENCHANTED_BOOK);
        final ItemMeta meta4 = antiRadiacao.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Proteção radiação");
        antiRadiacao.setItemMeta(meta4);
        final HashMap<Integer, ItemStack> nope19 = player.getInventory().addItem(antiRadiacao);
        for (final Entry<Integer, ItemStack> entry : nope19.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope20 = player.getInventory().addItem(antiRadiacao);
        for (final Entry<Integer, ItemStack> entry : nope20.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack tntComum = new ItemStack(Material.TNT, 48);
        final ItemMeta meta5 = tntComum.getItemMeta();
        meta5.setDisplayName(ChatColor.RED + "Explosivo comum");
        final List<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.GRAY + "Dano: 1/5");
        lore4.add(" ");
        meta5.setLore(lore4);
        tntComum.setItemMeta(meta5);
        final HashMap<Integer, ItemStack> nope21 = player.getInventory().addItem(tntComum);
        for (final Entry<Integer, ItemStack> entry : nope21.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack goldenApple = new ItemStack(Material.GOLDEN_APPLE, 32);
        final HashMap<Integer, ItemStack> nope22 = player.getInventory().addItem(goldenApple);
        for (final Entry<Integer, ItemStack> entry : nope22.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack end = new ItemStack(Material.END_STONE, 64);
        final ItemMeta meta6 = end.getItemMeta();
        meta6.setDisplayName(ChatColor.GREEN + "Bloco de fortificação");
        final List<String> lore5 = new ArrayList<>();
        lore5.add(" ");
        lore5.add(ChatColor.GRAY + "Defesa: " + ChatColor.RED + "4/10");
        lore5.add(" ");
        meta6.setLore(lore5);
        end.setItemMeta(meta6);
        final HashMap<Integer, ItemStack> nope23 = player.getInventory().addItem(end);
        for (final Entry<Integer, ItemStack> entry : nope23.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope24 = player.getInventory().addItem(end);
        for (final Entry<Integer, ItemStack> entry : nope24.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope25 = player.getInventory().addItem(end);
        for (final Entry<Integer, ItemStack> entry : nope25.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack bedrock = new ItemStack(Material.SMOOTH_STONE, 64);
        final ItemMeta meta7 = bedrock.getItemMeta();
        meta7.setDisplayName(ChatColor.GREEN + "Bloco de fortificação");
        final List<String> lore7 = new ArrayList<>();
        lore7.add(" ");
        lore7.add(ChatColor.GRAY + "Defesa: " + ChatColor.GREEN + "10/10");
        lore7.add(" ");
        meta7.setLore(lore7);
        bedrock.setItemMeta(meta7);
        final HashMap<Integer, ItemStack> nope26 = player.getInventory().addItem(bedrock);
        for (final Entry<Integer, ItemStack> entry : nope26.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope27 = player.getInventory().addItem(bedrock);
        for (final Entry<Integer, ItemStack> entry : nope27.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
    }

    public static Inventory inventoryKitMensalTitan() {
        final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Kit mensal (Titan): ");

        final ItemStack espada = new ItemStack(Material.DIAMOND_SWORD);
        espada.addEnchantment(Enchantment.DURABILITY, 3);
        espada.addEnchantment(Enchantment.DAMAGE_ALL, 4);

        final ItemStack capacete = new ItemStack(Material.DIAMOND_HELMET);
        capacete.addEnchantment(Enchantment.DURABILITY, 3);
        capacete.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);

        final ItemStack peitoral = new ItemStack(Material.DIAMOND_CHESTPLATE);
        peitoral.addEnchantment(Enchantment.DURABILITY, 3);
        peitoral.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);

        final ItemStack calca = new ItemStack(Material.DIAMOND_LEGGINGS);
        calca.addEnchantment(Enchantment.DURABILITY, 3);
        calca.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);

        final ItemStack botas = new ItemStack(Material.DIAMOND_BOOTS);
        botas.addEnchantment(Enchantment.DURABILITY, 3);
        botas.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);

        final ItemStack caixaSpawners = new ItemStack(Material.SPAWNER, 5);
        final ItemMeta metaCaixa = caixaSpawners.getItemMeta();
        metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore = new ArrayList<String>();
        lore.add(" ");
        lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Spawners");
        lore.add(" ");
        metaCaixa.setLore(lore);
        caixaSpawners.setItemMeta(metaCaixa);

        final ItemStack caixaArmas = new ItemStack(Material.ENDER_CHEST, 5);
        final ItemMeta metaCaixaAr = caixaArmas.getItemMeta();
        metaCaixaAr.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore2 = new ArrayList<String>();
        lore2.add(" ");
        lore2.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Armas");
        lore2.add(" ");
        metaCaixaAr.setLore(lore2);
        caixaArmas.setItemMeta(metaCaixaAr);

        final ItemStack caixaAvancada = new ItemStack(Material.ENDER_CHEST, 5);
        final ItemMeta metaCaixaAva = caixaAvancada.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Avançada");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaAvancada.setItemMeta(metaCaixaAva);

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 15);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);

        final ItemStack ghasts = new ItemStack(Material.GHAST_SPAWN_EGG, 20);
        final ItemStack totem = new ItemStack(Material.TOTEM_OF_UNDYING);
        final ItemStack escudo = new ItemStack(Material.SHIELD);
        final ItemStack elytra = new ItemStack(Material.ELYTRA);
        final ItemStack box = new ItemStack(Material.PURPLE_SHULKER_BOX);

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta meta2 = moeda.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "$10.000,00 moedas");
        moeda.setItemMeta(meta2);

        final ItemStack moedaNe = new ItemStack(Material.GHAST_TEAR);
        final ItemMeta meta3 = moedaNe.getItemMeta();
        meta3.setDisplayName(ChatColor.YELLOW + "$500,00 moedas negra");
        moedaNe.setItemMeta(meta3);

        final ItemStack antiRadiacao = new ItemStack(Material.ENCHANTED_BOOK);
        final ItemMeta meta4 = antiRadiacao.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Proteção radiação");
        antiRadiacao.setItemMeta(meta4);

        final ItemStack tntComum = new ItemStack(Material.TNT, 48);
        final ItemMeta meta5 = tntComum.getItemMeta();
        meta5.setDisplayName(ChatColor.RED + "Explosivo comum");
        final List<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.GRAY + "Dano: 1/5");
        lore4.add(" ");
        meta5.setLore(lore4);
        tntComum.setItemMeta(meta5);

        final ItemStack goldenApple = new ItemStack(Material.GOLDEN_APPLE, 32);

        final ItemStack end = new ItemStack(Material.END_STONE, 64);
        final ItemMeta meta6 = end.getItemMeta();
        meta6.setDisplayName(ChatColor.GREEN + "Bloco de fortificação");
        final List<String> lore5 = new ArrayList<>();
        lore5.add(" ");
        lore5.add(ChatColor.GRAY + "Defesa: " + ChatColor.RED + "4/10");
        lore5.add(" ");
        meta6.setLore(lore5);
        end.setItemMeta(meta6);

        final ItemStack bedrock = new ItemStack(Material.SMOOTH_STONE, 64);
        final ItemMeta meta7 = bedrock.getItemMeta();
        meta7.setDisplayName(ChatColor.GREEN + "Bloco de fortificação");
        final List<String> lore7 = new ArrayList<>();
        lore7.add(" ");
        lore7.add(ChatColor.GRAY + "Defesa: " + ChatColor.GREEN + "10/10");
        lore7.add(" ");
        meta7.setLore(lore7);
        bedrock.setItemMeta(meta7);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta10 = voltar.getItemMeta();
        meta10.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta10);

        inventory.setItem(10, espada);
        inventory.setItem(11, capacete);
        inventory.setItem(12, peitoral);
        inventory.setItem(13, calca);
        inventory.setItem(14, botas);
        inventory.setItem(15, caixaSpawners);
        inventory.setItem(16, caixaArmas);
        inventory.setItem(19, caixaAvancada);
        inventory.setItem(20, chave);
        inventory.setItem(21, ghasts);
        inventory.setItem(22, totem);
        inventory.setItem(23, totem);
        inventory.setItem(24, escudo);
        inventory.setItem(25, elytra);
        inventory.setItem(28, box);
        inventory.setItem(29, box);
        inventory.setItem(30, box);
        inventory.setItem(31, moeda);
        inventory.setItem(32, moedaNe);
        inventory.setItem(33, antiRadiacao);
        inventory.setItem(34, antiRadiacao);
        inventory.setItem(37, tntComum);
        inventory.setItem(38, goldenApple);
        inventory.setItem(39, end);
        inventory.setItem(40, end);
        inventory.setItem(41, end);
        inventory.setItem(42, bedrock);
        inventory.setItem(43, bedrock);
        inventory.setItem(45, voltar);

        return inventory;
    }

    //-
    public static Inventory inventoryKitMensalDuque() {
        final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Kit mensal (Duque): ");

        final ItemStack espada = new ItemStack(Material.DIAMOND_SWORD);
        espada.addEnchantment(Enchantment.DURABILITY, 3);
        espada.addEnchantment(Enchantment.DAMAGE_ALL, 5);

        final ItemStack capacete = new ItemStack(Material.DIAMOND_HELMET);
        capacete.addEnchantment(Enchantment.DURABILITY, 3);
        capacete.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
        capacete.addEnchantment(Enchantment.PROTECTION_FIRE, 1);
        capacete.addEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 1);

        final ItemStack peitoral = new ItemStack(Material.DIAMOND_CHESTPLATE);
        peitoral.addEnchantment(Enchantment.DURABILITY, 3);
        peitoral.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
        peitoral.addEnchantment(Enchantment.PROTECTION_FIRE, 1);
        peitoral.addEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 1);

        final ItemStack calca = new ItemStack(Material.DIAMOND_LEGGINGS);
        calca.addEnchantment(Enchantment.DURABILITY, 3);
        calca.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
        calca.addEnchantment(Enchantment.PROTECTION_FIRE, 1);
        calca.addEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 1);

        final ItemStack botas = new ItemStack(Material.DIAMOND_BOOTS);
        botas.addEnchantment(Enchantment.DURABILITY, 3);
        botas.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
        botas.addEnchantment(Enchantment.PROTECTION_FIRE, 1);
        botas.addEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 1);

        final ItemStack caixaSpawners = new ItemStack(Material.SPAWNER, 8);
        final ItemMeta metaCaixa = caixaSpawners.getItemMeta();
        metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore = new ArrayList<String>();
        lore.add(" ");
        lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Spawners");
        lore.add(" ");
        metaCaixa.setLore(lore);
        caixaSpawners.setItemMeta(metaCaixa);

        final ItemStack caixaArmas = new ItemStack(Material.ENDER_CHEST, 8);
        final ItemMeta metaCaixaAr = caixaArmas.getItemMeta();
        metaCaixaAr.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore2 = new ArrayList<String>();
        lore2.add(" ");
        lore2.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Armas");
        lore2.add(" ");
        metaCaixaAr.setLore(lore2);
        caixaArmas.setItemMeta(metaCaixaAr);

        final ItemStack caixaAvancada = new ItemStack(Material.ENDER_CHEST, 8);
        final ItemMeta metaCaixaAva = caixaAvancada.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Avançada");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaAvancada.setItemMeta(metaCaixaAva);

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 15);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);

        final ItemStack ghasts = new ItemStack(Material.GHAST_SPAWN_EGG, 30);
        final ItemStack totem = new ItemStack(Material.TOTEM_OF_UNDYING);
        final ItemStack escudo = new ItemStack(Material.SHIELD);
        final ItemStack elytra = new ItemStack(Material.ELYTRA);
        final ItemStack box = new ItemStack(Material.PURPLE_SHULKER_BOX);

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta meta2 = moeda.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "$15.000,00 moedas");
        moeda.setItemMeta(meta2);

        final ItemStack moedaNe = new ItemStack(Material.GHAST_TEAR);
        final ItemMeta meta3 = moedaNe.getItemMeta();
        meta3.setDisplayName(ChatColor.YELLOW + "$750,00 moedas negra");
        moedaNe.setItemMeta(meta3);

        final ItemStack antiRadiacao = new ItemStack(Material.ENCHANTED_BOOK);
        final ItemMeta meta4 = antiRadiacao.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Proteção radiação");
        antiRadiacao.setItemMeta(meta4);

        final ItemStack tntComum = new ItemStack(Material.TNT, 48);
        final ItemMeta meta5 = tntComum.getItemMeta();
        meta5.setDisplayName(ChatColor.RED + "Explosivo comum");
        final List<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.GRAY + "Dano: 1/5");
        lore4.add(" ");
        meta5.setLore(lore4);
        tntComum.setItemMeta(meta5);

        final ItemStack goldenApple = new ItemStack(Material.GOLDEN_APPLE, 48);
        final ItemStack goldenAppleEnch = new ItemStack(Material.ENCHANTED_GOLDEN_APPLE, 4);

        final ItemStack end = new ItemStack(Material.END_STONE, 64);
        final ItemMeta meta6 = end.getItemMeta();
        meta6.setDisplayName(ChatColor.GREEN + "Bloco de fortificação");
        final List<String> lore5 = new ArrayList<>();
        lore5.add(" ");
        lore5.add(ChatColor.GRAY + "Defesa: " + ChatColor.RED + "4/10");
        lore5.add(" ");
        meta6.setLore(lore5);
        end.setItemMeta(meta6);

        final ItemStack bedrock = new ItemStack(Material.SMOOTH_STONE, 64);
        final ItemMeta meta7 = bedrock.getItemMeta();
        meta7.setDisplayName(ChatColor.GREEN + "Bloco de fortificação");
        final List<String> lore7 = new ArrayList<>();
        lore7.add(" ");
        lore7.add(ChatColor.GRAY + "Defesa: " + ChatColor.GREEN + "10/10");
        lore7.add(" ");
        meta7.setLore(lore7);
        bedrock.setItemMeta(meta7);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta10 = voltar.getItemMeta();
        meta10.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta10);

        final ItemStack proxima = new ItemStack(Material.ARROW);
        final ItemMeta meta11 = proxima.getItemMeta();
        meta11.setDisplayName(ChatColor.RED + "Próxima");
        proxima.setItemMeta(meta11);

        inventory.setItem(10, espada);
        inventory.setItem(11, capacete);
        inventory.setItem(12, peitoral);
        inventory.setItem(13, calca);
        inventory.setItem(14, botas);
        inventory.setItem(15, caixaSpawners);
        inventory.setItem(16, caixaArmas);
        inventory.setItem(19, caixaAvancada);
        inventory.setItem(20, chave);
        inventory.setItem(21, ghasts);
        inventory.setItem(22, totem);
        inventory.setItem(23, totem);
        inventory.setItem(24, totem);
        inventory.setItem(25, escudo);
        inventory.setItem(28, elytra);
        inventory.setItem(29, elytra);
        inventory.setItem(30, box);
        inventory.setItem(31, box);
        inventory.setItem(32, box);
        inventory.setItem(33, moeda);
        inventory.setItem(34, moedaNe);
        inventory.setItem(37, antiRadiacao);
        inventory.setItem(38, antiRadiacao);
        inventory.setItem(39, tntComum);
        inventory.setItem(40, goldenApple);
        inventory.setItem(41, goldenAppleEnch);
        inventory.setItem(42, end);
        inventory.setItem(43, end);
        inventory.setItem(45, voltar);
        inventory.setItem(53, proxima);

        return inventory;
    }

    public static Inventory inventoryKitMensalDuque2() {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Kit mensal (Duque): ");

        final ItemStack end = new ItemStack(Material.END_STONE, 64);
        final ItemMeta meta6 = end.getItemMeta();
        meta6.setDisplayName(ChatColor.GREEN + "Bloco de fortificação");
        final List<String> lore5 = new ArrayList<>();
        lore5.add(" ");
        lore5.add(ChatColor.GRAY + "Defesa: " + ChatColor.RED + "4/10");
        lore5.add(" ");
        meta6.setLore(lore5);
        end.setItemMeta(meta6);

        final ItemStack bedrock = new ItemStack(Material.SMOOTH_STONE, 64);
        final ItemMeta meta7 = bedrock.getItemMeta();
        meta7.setDisplayName(ChatColor.GREEN + "Bloco de fortificação");
        final List<String> lore7 = new ArrayList<>();
        lore7.add(" ");
        lore7.add(ChatColor.GRAY + "Defesa: " + ChatColor.GREEN + "10/10");
        lore7.add(" ");
        meta7.setLore(lore7);
        bedrock.setItemMeta(meta7);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta10 = voltar.getItemMeta();
        meta10.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta10);

        inventory.setItem(10, end);
        inventory.setItem(11, end);
        inventory.setItem(12, end);
        inventory.setItem(13, bedrock);
        inventory.setItem(14, bedrock);
        inventory.setItem(15, bedrock);

        inventory.setItem(18, voltar);

        return inventory;
    }

    public static void giveKitMensalDuque(final Player player) {
        final ItemStack espada = new ItemStack(Material.DIAMOND_SWORD);
        espada.addEnchantment(Enchantment.DURABILITY, 3);
        espada.addEnchantment(Enchantment.DAMAGE_ALL, 5);
        final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(espada);
        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack capacete = new ItemStack(Material.DIAMOND_HELMET);
        capacete.addEnchantment(Enchantment.DURABILITY, 3);
        capacete.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
        capacete.addEnchantment(Enchantment.PROTECTION_FIRE, 1);
        capacete.addEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 1);
        final HashMap<Integer, ItemStack> nope1 = player.getInventory().addItem(capacete);
        for (final Entry<Integer, ItemStack> entry : nope1.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack peitoral = new ItemStack(Material.DIAMOND_CHESTPLATE);
        peitoral.addEnchantment(Enchantment.DURABILITY, 3);
        peitoral.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
        peitoral.addEnchantment(Enchantment.PROTECTION_FIRE, 1);
        peitoral.addEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 1);
        final HashMap<Integer, ItemStack> nope2 = player.getInventory().addItem(peitoral);
        for (final Entry<Integer, ItemStack> entry : nope2.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack calca = new ItemStack(Material.DIAMOND_LEGGINGS);
        calca.addEnchantment(Enchantment.DURABILITY, 3);
        calca.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
        calca.addEnchantment(Enchantment.PROTECTION_FIRE, 1);
        calca.addEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 1);
        final HashMap<Integer, ItemStack> nope3 = player.getInventory().addItem(calca);
        for (final Entry<Integer, ItemStack> entry : nope3.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack botas = new ItemStack(Material.DIAMOND_BOOTS);
        botas.addEnchantment(Enchantment.DURABILITY, 3);
        botas.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
        botas.addEnchantment(Enchantment.PROTECTION_FIRE, 1);
        botas.addEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 1);
        final HashMap<Integer, ItemStack> nope4 = player.getInventory().addItem(botas);
        for (final Entry<Integer, ItemStack> entry : nope4.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack caixaSpawners = new ItemStack(Material.SPAWNER, 8);
        final ItemMeta metaCaixa = caixaSpawners.getItemMeta();
        metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore = new ArrayList<String>();
        lore.add(" ");
        lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Spawners");
        lore.add(" ");
        metaCaixa.setLore(lore);
        caixaSpawners.setItemMeta(metaCaixa);
        final HashMap<Integer, ItemStack> nope5 = player.getInventory().addItem(caixaSpawners);
        for (final Entry<Integer, ItemStack> entry : nope5.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack caixaArmas = new ItemStack(Material.ENDER_CHEST, 8);
        final ItemMeta metaCaixaAr = caixaArmas.getItemMeta();
        metaCaixaAr.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore2 = new ArrayList<String>();
        lore2.add(" ");
        lore2.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Armas");
        lore2.add(" ");
        metaCaixaAr.setLore(lore2);
        caixaArmas.setItemMeta(metaCaixaAr);
        final HashMap<Integer, ItemStack> nope6 = player.getInventory().addItem(caixaArmas);
        for (final Entry<Integer, ItemStack> entry : nope6.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack caixaAvancada = new ItemStack(Material.ENDER_CHEST, 8);
        final ItemMeta metaCaixaAva = caixaAvancada.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Avançada");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaAvancada.setItemMeta(metaCaixaAva);
        final HashMap<Integer, ItemStack> nope7 = player.getInventory().addItem(caixaAvancada);
        for (final Entry<Integer, ItemStack> entry : nope7.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 15);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);
        final HashMap<Integer, ItemStack> nope8 = player.getInventory().addItem(chave);
        for (final Entry<Integer, ItemStack> entry : nope8.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack ghasts = new ItemStack(Material.GHAST_SPAWN_EGG, 30);
        final HashMap<Integer, ItemStack> nope9 = player.getInventory().addItem(ghasts);
        for (final Entry<Integer, ItemStack> entry : nope9.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack totem = new ItemStack(Material.TOTEM_OF_UNDYING);
        final HashMap<Integer, ItemStack> nope10 = player.getInventory().addItem(totem);
        for (final Entry<Integer, ItemStack> entry : nope10.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope11 = player.getInventory().addItem(totem);
        for (final Entry<Integer, ItemStack> entry : nope11.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope111 = player.getInventory().addItem(totem);
        for (final Entry<Integer, ItemStack> entry : nope111.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack escudo = new ItemStack(Material.SHIELD);
        final HashMap<Integer, ItemStack> nope12 = player.getInventory().addItem(escudo);
        for (final Entry<Integer, ItemStack> entry : nope12.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack elytra = new ItemStack(Material.ELYTRA);
        final HashMap<Integer, ItemStack> nope13 = player.getInventory().addItem(elytra);
        for (final Entry<Integer, ItemStack> entry : nope13.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope113 = player.getInventory().addItem(elytra);
        for (final Entry<Integer, ItemStack> entry : nope113.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack box = new ItemStack(Material.PURPLE_SHULKER_BOX);
        final HashMap<Integer, ItemStack> nope14 = player.getInventory().addItem(box);
        for (final Entry<Integer, ItemStack> entry : nope14.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope15 = player.getInventory().addItem(box);
        for (final Entry<Integer, ItemStack> entry : nope15.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope16 = player.getInventory().addItem(box);
        for (final Entry<Integer, ItemStack> entry : nope16.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta meta2 = moeda.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "$15.000,00 moedas");
        moeda.setItemMeta(meta2);
        final HashMap<Integer, ItemStack> nope17 = player.getInventory().addItem(moeda);
        for (final Entry<Integer, ItemStack> entry : nope17.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack moedaNe = new ItemStack(Material.GHAST_TEAR);
        final ItemMeta meta3 = moedaNe.getItemMeta();
        meta3.setDisplayName(ChatColor.YELLOW + "$750,00 moedas negra negra");
        moedaNe.setItemMeta(meta3);
        final HashMap<Integer, ItemStack> nope18 = player.getInventory().addItem(moedaNe);
        for (final Entry<Integer, ItemStack> entry : nope18.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack antiRadiacao = new ItemStack(Material.ENCHANTED_BOOK);
        final ItemMeta meta4 = antiRadiacao.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Proteção radiação");
        antiRadiacao.setItemMeta(meta4);
        final HashMap<Integer, ItemStack> nope19 = player.getInventory().addItem(antiRadiacao);
        for (final Entry<Integer, ItemStack> entry : nope19.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope20 = player.getInventory().addItem(antiRadiacao);
        for (final Entry<Integer, ItemStack> entry : nope20.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack tntComum = new ItemStack(Material.TNT, 64);
        final ItemMeta meta5 = tntComum.getItemMeta();
        meta5.setDisplayName(ChatColor.RED + "Explosivo comum");
        final List<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.GRAY + "Dano: 1/5");
        lore4.add(" ");
        meta5.setLore(lore4);
        tntComum.setItemMeta(meta5);
        final HashMap<Integer, ItemStack> nope21 = player.getInventory().addItem(tntComum);
        for (final Entry<Integer, ItemStack> entry : nope21.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack goldenApple = new ItemStack(Material.GOLDEN_APPLE, 48);
        final HashMap<Integer, ItemStack> nope22 = player.getInventory().addItem(goldenApple);
        for (final Entry<Integer, ItemStack> entry : nope22.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack goldenAppleEnch = new ItemStack(Material.ENCHANTED_GOLDEN_APPLE, 4);
        final HashMap<Integer, ItemStack> nope122 = player.getInventory().addItem(goldenAppleEnch);
        for (final Entry<Integer, ItemStack> entry : nope122.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack end = new ItemStack(Material.END_STONE, 64);
        final ItemMeta meta6 = end.getItemMeta();
        meta6.setDisplayName(ChatColor.GREEN + "Bloco de fortificação");
        final List<String> lore5 = new ArrayList<>();
        lore5.add(" ");
        lore5.add(ChatColor.GRAY + "Defesa: " + ChatColor.RED + "4/10");
        lore5.add(" ");
        meta6.setLore(lore5);
        end.setItemMeta(meta6);
        final HashMap<Integer, ItemStack> nope23 = player.getInventory().addItem(end);
        for (final Entry<Integer, ItemStack> entry : nope23.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope24 = player.getInventory().addItem(end);
        for (final Entry<Integer, ItemStack> entry : nope24.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope25 = player.getInventory().addItem(end);
        for (final Entry<Integer, ItemStack> entry : nope25.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope125 = player.getInventory().addItem(end);
        for (final Entry<Integer, ItemStack> entry : nope125.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope1125 = player.getInventory().addItem(end);
        for (final Entry<Integer, ItemStack> entry : nope1125.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack bedrock = new ItemStack(Material.SMOOTH_STONE, 64);
        final ItemMeta meta7 = bedrock.getItemMeta();
        meta7.setDisplayName(ChatColor.GREEN + "Bloco de fortificação");
        final List<String> lore7 = new ArrayList<>();
        lore7.add(" ");
        lore7.add(ChatColor.GRAY + "Defesa: " + ChatColor.GREEN + "10/10");
        lore7.add(" ");
        meta7.setLore(lore7);
        bedrock.setItemMeta(meta7);
        final HashMap<Integer, ItemStack> nope26 = player.getInventory().addItem(bedrock);
        for (final Entry<Integer, ItemStack> entry : nope26.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope27 = player.getInventory().addItem(bedrock);
        for (final Entry<Integer, ItemStack> entry : nope27.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope127 = player.getInventory().addItem(bedrock);
        for (final Entry<Integer, ItemStack> entry : nope127.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
    }
    //-

    public static void giveKitMensalLord(final Player player) {
        final ItemStack espada = new ItemStack(Material.DIAMOND_SWORD);
        espada.addEnchantment(Enchantment.DURABILITY, 3);
        espada.addEnchantment(Enchantment.DAMAGE_ALL, 3);
        final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(espada);
        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack capacete = new ItemStack(Material.DIAMOND_HELMET);
        capacete.addEnchantment(Enchantment.DURABILITY, 3);
        capacete.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
        final HashMap<Integer, ItemStack> nope1 = player.getInventory().addItem(capacete);
        for (final Entry<Integer, ItemStack> entry : nope1.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack peitoral = new ItemStack(Material.DIAMOND_CHESTPLATE);
        peitoral.addEnchantment(Enchantment.DURABILITY, 3);
        peitoral.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
        final HashMap<Integer, ItemStack> nope2 = player.getInventory().addItem(peitoral);
        for (final Entry<Integer, ItemStack> entry : nope2.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack calca = new ItemStack(Material.DIAMOND_LEGGINGS);
        calca.addEnchantment(Enchantment.DURABILITY, 3);
        calca.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
        final HashMap<Integer, ItemStack> nope3 = player.getInventory().addItem(calca);
        for (final Entry<Integer, ItemStack> entry : nope3.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack botas = new ItemStack(Material.DIAMOND_BOOTS);
        botas.addEnchantment(Enchantment.DURABILITY, 3);
        botas.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
        final HashMap<Integer, ItemStack> nope4 = player.getInventory().addItem(botas);
        for (final Entry<Integer, ItemStack> entry : nope4.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack caixaSpawners = new ItemStack(Material.SPAWNER, 5);
        final ItemMeta metaCaixa = caixaSpawners.getItemMeta();
        metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore = new ArrayList<String>();
        lore.add(" ");
        lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Spawners");
        lore.add(" ");
        metaCaixa.setLore(lore);
        caixaSpawners.setItemMeta(metaCaixa);
        final HashMap<Integer, ItemStack> nope5 = player.getInventory().addItem(caixaSpawners);
        for (final Entry<Integer, ItemStack> entry : nope5.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack caixaArmas = new ItemStack(Material.ENDER_CHEST, 5);
        final ItemMeta metaCaixaAr = caixaArmas.getItemMeta();
        metaCaixaAr.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore2 = new ArrayList<String>();
        lore2.add(" ");
        lore2.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Armas");
        lore2.add(" ");
        metaCaixaAr.setLore(lore2);
        caixaArmas.setItemMeta(metaCaixaAr);
        final HashMap<Integer, ItemStack> nope6 = player.getInventory().addItem(caixaArmas);
        for (final Entry<Integer, ItemStack> entry : nope6.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack caixaAvancada = new ItemStack(Material.ENDER_CHEST, 5);
        final ItemMeta metaCaixaAva = caixaAvancada.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Avançada");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaAvancada.setItemMeta(metaCaixaAva);
        final HashMap<Integer, ItemStack> nope7 = player.getInventory().addItem(caixaAvancada);
        for (final Entry<Integer, ItemStack> entry : nope7.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 10);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);
        final HashMap<Integer, ItemStack> nope8 = player.getInventory().addItem(chave);
        for (final Entry<Integer, ItemStack> entry : nope8.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack ghasts = new ItemStack(Material.GHAST_SPAWN_EGG, 15);
        final HashMap<Integer, ItemStack> nope9 = player.getInventory().addItem(ghasts);
        for (final Entry<Integer, ItemStack> entry : nope9.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack totem = new ItemStack(Material.TOTEM_OF_UNDYING);
        final HashMap<Integer, ItemStack> nope10 = player.getInventory().addItem(totem);
        for (final Entry<Integer, ItemStack> entry : nope10.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack escudo = new ItemStack(Material.SHIELD);
        final HashMap<Integer, ItemStack> nope11 = player.getInventory().addItem(escudo);
        for (final Entry<Integer, ItemStack> entry : nope11.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack elytra = new ItemStack(Material.ELYTRA);
        final HashMap<Integer, ItemStack> nope12 = player.getInventory().addItem(elytra);
        for (final Entry<Integer, ItemStack> entry : nope12.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack box = new ItemStack(Material.PURPLE_SHULKER_BOX);
        final HashMap<Integer, ItemStack> nope13 = player.getInventory().addItem(box);
        for (final Entry<Integer, ItemStack> entry : nope13.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope14 = player.getInventory().addItem(box);
        for (final Entry<Integer, ItemStack> entry : nope14.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta meta2 = moeda.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "$10.000,00 moedas");
        moeda.setItemMeta(meta2);
        final HashMap<Integer, ItemStack> nope15 = player.getInventory().addItem(moeda);
        for (final Entry<Integer, ItemStack> entry : nope15.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack moedaNe = new ItemStack(Material.GHAST_TEAR);
        final ItemMeta meta3 = moedaNe.getItemMeta();
        meta3.setDisplayName(ChatColor.YELLOW + "$500,00 moedas negra");
        moedaNe.setItemMeta(meta3);
        final HashMap<Integer, ItemStack> nope16 = player.getInventory().addItem(moedaNe);
        for (final Entry<Integer, ItemStack> entry : nope16.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack antiRadiacao = new ItemStack(Material.ENCHANTED_BOOK);
        final ItemMeta meta4 = antiRadiacao.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Proteção radiação");
        antiRadiacao.setItemMeta(meta4);
        final HashMap<Integer, ItemStack> nope17 = player.getInventory().addItem(antiRadiacao);
        for (final Entry<Integer, ItemStack> entry : nope17.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope18 = player.getInventory().addItem(espada);
        for (final Entry<Integer, ItemStack> entry : nope18.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack tntComum = new ItemStack(Material.TNT, 48);
        final ItemMeta meta5 = tntComum.getItemMeta();
        meta5.setDisplayName(ChatColor.RED + "Explosivo comum");
        final List<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.GRAY + "Dano: 1/5");
        lore4.add(" ");
        meta5.setLore(lore4);
        tntComum.setItemMeta(meta5);
        final HashMap<Integer, ItemStack> nope19 = player.getInventory().addItem(tntComum);
        for (final Entry<Integer, ItemStack> entry : nope19.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack goldenApple = new ItemStack(Material.GOLDEN_APPLE, 32);
        final HashMap<Integer, ItemStack> nope20 = player.getInventory().addItem(goldenApple);
        for (final Entry<Integer, ItemStack> entry : nope20.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack end = new ItemStack(Material.END_STONE, 64);
        final ItemMeta meta6 = end.getItemMeta();
        meta6.setDisplayName(ChatColor.GREEN + "Bloco de fortificação");
        final List<String> lore5 = new ArrayList<>();
        lore5.add(" ");
        lore5.add(ChatColor.GRAY + "Defesa: " + ChatColor.RED + "4/10");
        lore5.add(" ");
        meta6.setLore(lore5);
        end.setItemMeta(meta6);
        final HashMap<Integer, ItemStack> nope21 = player.getInventory().addItem(end);
        for (final Entry<Integer, ItemStack> entry : nope21.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope22 = player.getInventory().addItem(end);
        for (final Entry<Integer, ItemStack> entry : nope22.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope23 = player.getInventory().addItem(end);
        for (final Entry<Integer, ItemStack> entry : nope23.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack bedrock = new ItemStack(Material.SMOOTH_STONE, 64);
        final ItemMeta meta7 = bedrock.getItemMeta();
        meta7.setDisplayName(ChatColor.GREEN + "Bloco de fortificação");
        final List<String> lore7 = new ArrayList<>();
        lore7.add(" ");
        lore7.add(ChatColor.GRAY + "Defesa: " + ChatColor.GREEN + "10/10");
        lore7.add(" ");
        meta7.setLore(lore7);
        bedrock.setItemMeta(meta7);
        final HashMap<Integer, ItemStack> nope24 = player.getInventory().addItem(bedrock);
        for (final Entry<Integer, ItemStack> entry : nope24.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope25 = player.getInventory().addItem(bedrock);
        for (final Entry<Integer, ItemStack> entry : nope25.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
    }

    public static Inventory inventoryKitMensalLord() {
        final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Kit mensal (Lord): ");

        final ItemStack espada = new ItemStack(Material.DIAMOND_SWORD);
        espada.addEnchantment(Enchantment.DURABILITY, 3);
        espada.addEnchantment(Enchantment.DAMAGE_ALL, 3);

        final ItemStack capacete = new ItemStack(Material.DIAMOND_HELMET);
        capacete.addEnchantment(Enchantment.DURABILITY, 3);
        capacete.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);

        final ItemStack peitoral = new ItemStack(Material.DIAMOND_CHESTPLATE);
        peitoral.addEnchantment(Enchantment.DURABILITY, 3);
        peitoral.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);

        final ItemStack calca = new ItemStack(Material.DIAMOND_LEGGINGS);
        calca.addEnchantment(Enchantment.DURABILITY, 3);
        calca.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);

        final ItemStack botas = new ItemStack(Material.DIAMOND_BOOTS);
        botas.addEnchantment(Enchantment.DURABILITY, 3);
        botas.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);

        final ItemStack caixaSpawners = new ItemStack(Material.SPAWNER, 5);
        final ItemMeta metaCaixa = caixaSpawners.getItemMeta();
        metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore = new ArrayList<String>();
        lore.add(" ");
        lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Spawners");
        lore.add(" ");
        metaCaixa.setLore(lore);
        caixaSpawners.setItemMeta(metaCaixa);

        final ItemStack caixaArmas = new ItemStack(Material.ENDER_CHEST, 5);
        final ItemMeta metaCaixaAr = caixaArmas.getItemMeta();
        metaCaixaAr.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore2 = new ArrayList<String>();
        lore2.add(" ");
        lore2.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Armas");
        lore2.add(" ");
        metaCaixaAr.setLore(lore2);
        caixaArmas.setItemMeta(metaCaixaAr);

        final ItemStack caixaAvancada = new ItemStack(Material.ENDER_CHEST, 5);
        final ItemMeta metaCaixaAva = caixaAvancada.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Avançada");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaAvancada.setItemMeta(metaCaixaAva);

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 10);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);

        final ItemStack ghasts = new ItemStack(Material.GHAST_SPAWN_EGG, 15);
        final ItemStack totem = new ItemStack(Material.TOTEM_OF_UNDYING);
        final ItemStack escudo = new ItemStack(Material.SHIELD);
        final ItemStack elytra = new ItemStack(Material.ELYTRA);
        final ItemStack box = new ItemStack(Material.PURPLE_SHULKER_BOX);

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta meta2 = moeda.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "$10.000,00 moedas");
        moeda.setItemMeta(meta2);

        final ItemStack moedaNe = new ItemStack(Material.GHAST_TEAR);
        final ItemMeta meta3 = moedaNe.getItemMeta();
        meta3.setDisplayName(ChatColor.YELLOW + "$500,00 moedas negra");
        moedaNe.setItemMeta(meta3);

        final ItemStack antiRadiacao = new ItemStack(Material.ENCHANTED_BOOK);
        final ItemMeta meta4 = antiRadiacao.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Proteção radiação");
        antiRadiacao.setItemMeta(meta4);

        final ItemStack tntComum = new ItemStack(Material.TNT, 48);
        final ItemMeta meta5 = tntComum.getItemMeta();
        meta5.setDisplayName(ChatColor.RED + "Explosivo comum");
        final List<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.GRAY + "Dano: 1/5");
        lore4.add(" ");
        meta5.setLore(lore4);
        tntComum.setItemMeta(meta5);

        final ItemStack goldenApple = new ItemStack(Material.GOLDEN_APPLE, 32);

        final ItemStack end = new ItemStack(Material.END_STONE, 64);
        final ItemMeta meta6 = end.getItemMeta();
        meta6.setDisplayName(ChatColor.GREEN + "Bloco de fortificação");
        final List<String> lore5 = new ArrayList<>();
        lore5.add(" ");
        lore5.add(ChatColor.GRAY + "Defesa: " + ChatColor.RED + "4/10");
        lore5.add(" ");
        meta6.setLore(lore5);
        end.setItemMeta(meta6);

        final ItemStack bedrock = new ItemStack(Material.SMOOTH_STONE, 64);
        final ItemMeta meta7 = bedrock.getItemMeta();
        meta7.setDisplayName(ChatColor.GREEN + "Bloco de fortificação");
        final List<String> lore7 = new ArrayList<>();
        lore7.add(" ");
        lore7.add(ChatColor.GRAY + "Defesa: " + ChatColor.GREEN + "10/10");
        lore7.add(" ");
        meta7.setLore(lore7);
        bedrock.setItemMeta(meta7);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta10 = voltar.getItemMeta();
        meta10.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta10);

        inventory.setItem(10, espada);
        inventory.setItem(11, capacete);
        inventory.setItem(12, peitoral);
        inventory.setItem(13, calca);
        inventory.setItem(14, botas);
        inventory.setItem(15, caixaSpawners);
        inventory.setItem(16, caixaArmas);
        inventory.setItem(19, caixaAvancada);
        inventory.setItem(20, chave);
        inventory.setItem(21, ghasts);
        inventory.setItem(22, totem);
        inventory.setItem(23, escudo);
        inventory.setItem(24, elytra);
        inventory.setItem(25, box);
        inventory.setItem(28, box);
        inventory.setItem(29, moeda);
        inventory.setItem(30, moedaNe);
        inventory.setItem(31, antiRadiacao);
        inventory.setItem(32, antiRadiacao);
        inventory.setItem(33, tntComum);
        inventory.setItem(34, goldenApple);
        inventory.setItem(38, end);
        inventory.setItem(39, end);
        inventory.setItem(40, end);
        inventory.setItem(41, bedrock);
        inventory.setItem(42, bedrock);
        inventory.setItem(45, voltar);

        return inventory;
    }

    public static void giveKitMensalConde(final Player player) {
        final ItemStack espada = new ItemStack(Material.DIAMOND_SWORD);
        espada.addEnchantment(Enchantment.DURABILITY, 2);
        espada.addEnchantment(Enchantment.DAMAGE_ALL, 3);
        final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(espada);
        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack capacete = new ItemStack(Material.DIAMOND_HELMET);
        capacete.addEnchantment(Enchantment.DURABILITY, 2);
        capacete.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
        final HashMap<Integer, ItemStack> nope1 = player.getInventory().addItem(capacete);
        for (final Entry<Integer, ItemStack> entry : nope1.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack peitoral = new ItemStack(Material.DIAMOND_CHESTPLATE);
        peitoral.addEnchantment(Enchantment.DURABILITY, 2);
        peitoral.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
        final HashMap<Integer, ItemStack> nope2 = player.getInventory().addItem(peitoral);
        for (final Entry<Integer, ItemStack> entry : nope2.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack calca = new ItemStack(Material.DIAMOND_LEGGINGS);
        calca.addEnchantment(Enchantment.DURABILITY, 2);
        calca.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
        final HashMap<Integer, ItemStack> nope3 = player.getInventory().addItem(calca);
        for (final Entry<Integer, ItemStack> entry : nope3.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack botas = new ItemStack(Material.DIAMOND_BOOTS);
        botas.addEnchantment(Enchantment.DURABILITY, 2);
        botas.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
        final HashMap<Integer, ItemStack> nope4 = player.getInventory().addItem(botas);
        for (final Entry<Integer, ItemStack> entry : nope4.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack caixaSpawners = new ItemStack(Material.SPAWNER, 5);
        final ItemMeta metaCaixa = caixaSpawners.getItemMeta();
        metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore = new ArrayList<String>();
        lore.add(" ");
        lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Spawners");
        lore.add(" ");
        metaCaixa.setLore(lore);
        caixaSpawners.setItemMeta(metaCaixa);
        final HashMap<Integer, ItemStack> nope5 = player.getInventory().addItem(caixaSpawners);
        for (final Entry<Integer, ItemStack> entry : nope5.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack caixaArmas = new ItemStack(Material.ENDER_CHEST, 5);
        final ItemMeta metaCaixaAr = caixaArmas.getItemMeta();
        metaCaixaAr.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore2 = new ArrayList<String>();
        lore2.add(" ");
        lore2.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Armas");
        lore2.add(" ");
        metaCaixaAr.setLore(lore2);
        caixaArmas.setItemMeta(metaCaixaAr);
        final HashMap<Integer, ItemStack> nope6 = player.getInventory().addItem(caixaArmas);
        for (final Entry<Integer, ItemStack> entry : nope6.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack caixaAvancada = new ItemStack(Material.ENDER_CHEST, 5);
        final ItemMeta metaCaixaAva = caixaAvancada.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Avançada");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaAvancada.setItemMeta(metaCaixaAva);
        final HashMap<Integer, ItemStack> nope7 = player.getInventory().addItem(caixaAvancada);
        for (final Entry<Integer, ItemStack> entry : nope7.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 8);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);
        final HashMap<Integer, ItemStack> nope8 = player.getInventory().addItem(chave);
        for (final Entry<Integer, ItemStack> entry : nope8.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack ghasts = new ItemStack(Material.GHAST_SPAWN_EGG, 10);
        final HashMap<Integer, ItemStack> nope9 = player.getInventory().addItem(ghasts);
        for (final Entry<Integer, ItemStack> entry : nope9.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack totem = new ItemStack(Material.TOTEM_OF_UNDYING);
        final HashMap<Integer, ItemStack> nope10 = player.getInventory().addItem(totem);
        for (final Entry<Integer, ItemStack> entry : nope10.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack escudo = new ItemStack(Material.SHIELD);
        final HashMap<Integer, ItemStack> nope11 = player.getInventory().addItem(escudo);
        for (final Entry<Integer, ItemStack> entry : nope11.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack elytra = new ItemStack(Material.ELYTRA);
        final HashMap<Integer, ItemStack> nope12 = player.getInventory().addItem(elytra);
        for (final Entry<Integer, ItemStack> entry : nope12.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final ItemStack box = new ItemStack(Material.PURPLE_SHULKER_BOX);
        final HashMap<Integer, ItemStack> nope13 = player.getInventory().addItem(box);
        for (final Entry<Integer, ItemStack> entry : nope13.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta meta2 = moeda.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "$10.000,00 moedas");
        moeda.setItemMeta(meta2);
        final HashMap<Integer, ItemStack> nope14 = player.getInventory().addItem(moeda);
        for (final Entry<Integer, ItemStack> entry : nope14.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack moedaNe = new ItemStack(Material.GHAST_TEAR);
        final ItemMeta meta3 = moedaNe.getItemMeta();
        meta3.setDisplayName(ChatColor.YELLOW + "$500,00 moedas negra");
        moedaNe.setItemMeta(meta3);
        final HashMap<Integer, ItemStack> nope15 = player.getInventory().addItem(moedaNe);
        for (final Entry<Integer, ItemStack> entry : nope15.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack antiRadiacao = new ItemStack(Material.ENCHANTED_BOOK);
        final ItemMeta meta4 = antiRadiacao.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Proteção radiação");
        antiRadiacao.setItemMeta(meta4);
        final HashMap<Integer, ItemStack> nope16 = player.getInventory().addItem(antiRadiacao);
        for (final Entry<Integer, ItemStack> entry : nope16.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope21 = player.getInventory().addItem(antiRadiacao);
        for (final Entry<Integer, ItemStack> entry : nope21.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack tntComum = new ItemStack(Material.TNT, 32);
        final ItemMeta meta5 = tntComum.getItemMeta();
        meta5.setDisplayName(ChatColor.RED + "Explosivo comum");
        final List<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.GRAY + "Dano: 1/5");
        lore4.add(" ");
        meta5.setLore(lore4);
        tntComum.setItemMeta(meta5);
        final HashMap<Integer, ItemStack> nope17 = player.getInventory().addItem(tntComum);
        for (final Entry<Integer, ItemStack> entry : nope17.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack goldenApple = new ItemStack(Material.GOLDEN_APPLE, 16);
        final HashMap<Integer, ItemStack> nope18 = player.getInventory().addItem(goldenApple);
        for (final Entry<Integer, ItemStack> entry : nope18.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack end = new ItemStack(Material.END_STONE, 64);
        final ItemMeta meta6 = end.getItemMeta();
        meta6.setDisplayName(ChatColor.GREEN + "Bloco de fortificação");
        final List<String> lore5 = new ArrayList<>();
        lore5.add(" ");
        lore5.add(ChatColor.GRAY + "Defesa: " + ChatColor.RED + "4/10");
        lore5.add(" ");
        meta6.setLore(lore5);
        end.setItemMeta(meta6);
        final HashMap<Integer, ItemStack> nope19 = player.getInventory().addItem(end);
        for (final Entry<Integer, ItemStack> entry : nope19.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        final HashMap<Integer, ItemStack> nope22 = player.getInventory().addItem(end);
        for (final Entry<Integer, ItemStack> entry : nope22.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }

        final ItemStack bedrock = new ItemStack(Material.SMOOTH_STONE, 64);
        final ItemMeta meta7 = bedrock.getItemMeta();
        meta7.setDisplayName(ChatColor.GREEN + "Bloco de fortificação");
        final List<String> lore7 = new ArrayList<>();
        lore7.add(" ");
        lore7.add(ChatColor.GRAY + "Defesa: " + ChatColor.GREEN + "10/10");
        lore7.add(" ");
        meta7.setLore(lore7);
        bedrock.setItemMeta(meta7);
        final HashMap<Integer, ItemStack> nope20 = player.getInventory().addItem(bedrock);
        for (final Entry<Integer, ItemStack> entry : nope20.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
    }

    public static Inventory inventoryKitMensalConde() {
        final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Kit mensal (Conde): ");

        final ItemStack espada = new ItemStack(Material.DIAMOND_SWORD);
        espada.addEnchantment(Enchantment.DURABILITY, 2);
        espada.addEnchantment(Enchantment.DAMAGE_ALL, 3);

        final ItemStack capacete = new ItemStack(Material.DIAMOND_HELMET);
        capacete.addEnchantment(Enchantment.DURABILITY, 2);
        capacete.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);

        final ItemStack peitoral = new ItemStack(Material.DIAMOND_CHESTPLATE);
        peitoral.addEnchantment(Enchantment.DURABILITY, 2);
        peitoral.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);

        final ItemStack calca = new ItemStack(Material.DIAMOND_LEGGINGS);
        calca.addEnchantment(Enchantment.DURABILITY, 2);
        calca.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);

        final ItemStack botas = new ItemStack(Material.DIAMOND_BOOTS);
        botas.addEnchantment(Enchantment.DURABILITY, 2);
        botas.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);

        final ItemStack caixaSpawners = new ItemStack(Material.SPAWNER, 5);
        final ItemMeta metaCaixa = caixaSpawners.getItemMeta();
        metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore = new ArrayList<String>();
        lore.add(" ");
        lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Spawners");
        lore.add(" ");
        metaCaixa.setLore(lore);
        caixaSpawners.setItemMeta(metaCaixa);

        final ItemStack caixaArmas = new ItemStack(Material.ENDER_CHEST, 5);
        final ItemMeta metaCaixaAr = caixaArmas.getItemMeta();
        metaCaixaAr.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore2 = new ArrayList<String>();
        lore2.add(" ");
        lore2.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Armas");
        lore2.add(" ");
        metaCaixaAr.setLore(lore2);
        caixaArmas.setItemMeta(metaCaixaAr);

        final ItemStack caixaAvancada = new ItemStack(Material.ENDER_CHEST, 5);
        final ItemMeta metaCaixaAva = caixaAvancada.getItemMeta();
        metaCaixaAva.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Avançada");
        lore1.add(" ");
        metaCaixaAva.setLore(lore1);
        caixaAvancada.setItemMeta(metaCaixaAva);

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 8);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore3.add(ChatColor.YELLOW + "50% sucesso");
        lore3.add(" ");
        metaChave.setLore(lore3);
        chave.setItemMeta(metaChave);

        final ItemStack ghasts = new ItemStack(Material.GHAST_SPAWN_EGG, 10);

        final ItemStack totem = new ItemStack(Material.TOTEM_OF_UNDYING);
        final ItemStack escudo = new ItemStack(Material.SHIELD);
        final ItemStack elytra = new ItemStack(Material.ELYTRA);
        final ItemStack box = new ItemStack(Material.PURPLE_SHULKER_BOX);

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta meta2 = moeda.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "$10.000,00 moedas");
        moeda.setItemMeta(meta2);

        final ItemStack moedaNe = new ItemStack(Material.GHAST_TEAR);
        final ItemMeta meta3 = moedaNe.getItemMeta();
        meta3.setDisplayName(ChatColor.YELLOW + "$500,00 moedas negra");
        moedaNe.setItemMeta(meta3);

        final ItemStack antiRadiacao = new ItemStack(Material.ENCHANTED_BOOK);
        final ItemMeta meta4 = antiRadiacao.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Proteção radiação");
        antiRadiacao.setItemMeta(meta4);

        final ItemStack tntComum = new ItemStack(Material.TNT, 32);
        final ItemMeta meta5 = tntComum.getItemMeta();
        meta5.setDisplayName(ChatColor.RED + "Explosivo comum");
        final List<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.GRAY + "Dano: 1/5");
        lore4.add(" ");
        meta5.setLore(lore4);
        tntComum.setItemMeta(meta5);

        final ItemStack goldenApple = new ItemStack(Material.GOLDEN_APPLE, 16);

        final ItemStack end = new ItemStack(Material.END_STONE, 64);
        final ItemMeta meta6 = end.getItemMeta();
        meta6.setDisplayName(ChatColor.GREEN + "Bloco de fortificação");
        final List<String> lore5 = new ArrayList<>();
        lore5.add(" ");
        lore5.add(ChatColor.GRAY + "Defesa: " + ChatColor.RED + "4/10");
        lore5.add(" ");
        meta6.setLore(lore5);
        end.setItemMeta(meta6);

        final ItemStack bedrock = new ItemStack(Material.SMOOTH_STONE, 64);
        final ItemMeta meta7 = bedrock.getItemMeta();
        meta7.setDisplayName(ChatColor.GREEN + "Bloco de fortificação");
        final List<String> lore7 = new ArrayList<>();
        lore7.add(" ");
        lore7.add(ChatColor.GRAY + "Defesa: " + ChatColor.GREEN + "10/10");
        lore7.add(" ");
        meta7.setLore(lore7);
        bedrock.setItemMeta(meta7);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta10 = voltar.getItemMeta();
        meta10.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta10);

        inventory.setItem(10, espada);
        inventory.setItem(11, capacete);
        inventory.setItem(12, peitoral);
        inventory.setItem(13, calca);
        inventory.setItem(14, botas);
        inventory.setItem(15, caixaSpawners);
        inventory.setItem(16, caixaArmas);
        inventory.setItem(19, caixaAvancada);
        inventory.setItem(20, chave);
        inventory.setItem(21, ghasts);
        inventory.setItem(22, totem);
        inventory.setItem(23, escudo);
        inventory.setItem(24, elytra);
        inventory.setItem(25, box);
        inventory.setItem(28, moeda);
        inventory.setItem(29, moedaNe);
        inventory.setItem(30, antiRadiacao);
        inventory.setItem(31, antiRadiacao);
        inventory.setItem(32, tntComum);
        inventory.setItem(33, goldenApple);
        inventory.setItem(34, end);
        inventory.setItem(39, end);
        inventory.setItem(40, bedrock);
        inventory.setItem(45, voltar);

        return inventory;
    }

    public static Inventory inventoryKitBasico6Horas() {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Kit básico, 6 horas jogadas: ");

        final ItemStack fragmento = new ItemStack(Material.TRIPWIRE_HOOK);
        final ItemMeta meta1 = fragmento.getItemMeta();
        meta1.setDisplayName(ChatColor.GREEN + "Fragmento VIP");
        fragmento.setItemMeta(meta1);

        final ItemStack caixa = new ItemStack(Material.CHEST);
        final ItemMeta metaCaixa = caixa.getItemMeta();
        metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore = new ArrayList<String>();
        lore.add(" ");
        lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Básica");
        lore.add(" ");
        metaCaixa.setLore(lore);
        caixa.setItemMeta(metaCaixa);

        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK);
        final ItemMeta metaChave = chave.getItemMeta();
        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
        final List<String> lore1 = new ArrayList<>();
        lore1.add(" ");
        lore1.add(ChatColor.GRAY + "Utilizado para abrir caixas");
        lore1.add(ChatColor.YELLOW + "50% sucesso");
        lore1.add(" ");
        metaChave.setLore(lore1);
        chave.setItemMeta(metaChave);

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta meta2 = moeda.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "$5.000,00 moedas");
        moeda.setItemMeta(meta2);

        final ItemStack end = new ItemStack(Material.END_STONE, 32);
        final ItemMeta meta3 = end.getItemMeta();
        meta3.setDisplayName(ChatColor.GREEN + "Bloco de fortificação");
        final List<String> lore2 = new ArrayList<>();
        lore2.add(" ");
        lore2.add(ChatColor.GRAY + "Defesa: " + ChatColor.RED + "4/10");
        lore2.add(" ");
        meta3.setLore(lore2);
        end.setItemMeta(meta3);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta = voltar.getItemMeta();
        meta.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta);

        inventory.setItem(11, fragmento);
        inventory.setItem(12, caixa);
        inventory.setItem(13, chave);
        inventory.setItem(14, moeda);
        inventory.setItem(15, end);
        inventory.setItem(18, voltar);

        return inventory;
    }

    public static void giveKitBasico(final Player player) {
        final ItemStack picareta = new ItemStack(Material.STONE_PICKAXE);
        final ItemStack machado = new ItemStack(Material.STONE_AXE);
        final ItemStack pa = new ItemStack(Material.STONE_SHOVEL);
        final ItemStack tocha = new ItemStack(Material.TORCH, 16);
        final ItemStack madeira = new ItemStack(Material.OAK_WOOD, 16);
        final ItemStack workbench = new ItemStack(Material.LEGACY_WORKBENCH);
        final ItemStack pao = new ItemStack(Material.BREAD);
        final ItemStack espada = new ItemStack(Material.STONE_SWORD);
        final ItemStack capacete = new ItemStack(Material.LEATHER_HELMET);
        final ItemStack peitoral = new ItemStack(Material.LEATHER_CHESTPLATE);
        final ItemStack calca = new ItemStack(Material.LEATHER_LEGGINGS);
        final ItemStack bota = new ItemStack(Material.LEATHER_BOOTS);
        final ItemStack arco = new ItemStack(Material.BOW);
        final ItemStack flechas = new ItemStack(Material.ARROW, 15);
    }

    public static Inventory inventoryKitBasico() {
        final Inventory inventory = Bukkit.createInventory(null, 4 * 9, "Kit básico: ");

        final ItemStack picareta = new ItemStack(Material.STONE_PICKAXE);
        final ItemStack machado = new ItemStack(Material.STONE_AXE);
        final ItemStack pa = new ItemStack(Material.STONE_SHOVEL);
        final ItemStack tocha = new ItemStack(Material.TORCH, 16);
        final ItemStack madeira = new ItemStack(Material.OAK_WOOD, 16);
        final ItemStack workbench = new ItemStack(Material.LEGACY_WORKBENCH);
        final ItemStack pao = new ItemStack(Material.BREAD);
        final ItemStack espada = new ItemStack(Material.STONE_SWORD);
        final ItemStack capacete = new ItemStack(Material.LEATHER_HELMET);
        final ItemStack peitoral = new ItemStack(Material.LEATHER_CHESTPLATE);
        final ItemStack calca = new ItemStack(Material.LEATHER_LEGGINGS);
        final ItemStack bota = new ItemStack(Material.LEATHER_BOOTS);
        final ItemStack arco = new ItemStack(Material.BOW);
        final ItemStack flechas = new ItemStack(Material.ARROW, 15);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta = voltar.getItemMeta();
        meta.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta);

        inventory.setItem(10, picareta);
        inventory.setItem(11, machado);
        inventory.setItem(12, pa);
        inventory.setItem(13, tocha);
        inventory.setItem(14, madeira);
        inventory.setItem(15, workbench);
        inventory.setItem(16, pao);
        inventory.setItem(19, espada);
        inventory.setItem(20, capacete);
        inventory.setItem(21, peitoral);
        inventory.setItem(22, calca);
        inventory.setItem(23, bota);
        inventory.setItem(24, arco);
        inventory.setItem(25, flechas);
        inventory.setItem(27, voltar);

        return inventory;
    }

    //-
    public static Inventory inventoryKitsVipDuque() {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Kits VIP (Duque): ");

        final ItemStack VIP2MES = new ItemStack(Material.DIAMOND_BLOCK);
        final ItemMeta meta5 = VIP2MES.getItemMeta();
        meta5.setDisplayName(ChatColor.YELLOW + " * Kit mensal " + ChatColor.AQUA + "(Duque)");
        final List<String> lore5 = new ArrayList<>();
        lore5.add(" ");
        lore5.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão direito para");
        lore5.add(ChatColor.GRAY + " verificar os itens que este kit");
        lore5.add(ChatColor.GRAY + " disponibiliza.");
        lore5.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão esquerdo para");
        lore5.add(ChatColor.GRAY + " pegar o kit.");
        lore5.add(" ");
        meta5.setLore(lore5);
        VIP2MES.setItemMeta(meta5);

        final ItemStack VIP2SEMANAL = new ItemStack(Material.DIAMOND_ORE);
        final ItemMeta meta6 = VIP2SEMANAL.getItemMeta();
        meta6.setDisplayName(ChatColor.YELLOW + " * Kit semanal " + ChatColor.AQUA + "(Duque)");
        final List<String> lore6 = new ArrayList<>();
        lore6.add(" ");
        lore6.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão direito para");
        lore6.add(ChatColor.GRAY + " verificar os itens que este kit");
        lore6.add(ChatColor.GRAY + " disponibiliza.");
        lore6.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão esquerdo para");
        lore6.add(ChatColor.GRAY + " pegar o kit.");
        lore6.add(" ");
        meta6.setLore(lore6);
        VIP2SEMANAL.setItemMeta(meta6);

        final ItemStack VIP2HORAS = new ItemStack(Material.DIAMOND_PICKAXE);
        final ItemMeta meta7 = VIP2HORAS.getItemMeta();
        meta7.setDisplayName(ChatColor.YELLOW + " * Kit, 6 horas jogadas " + ChatColor.AQUA + "(Duque)");
        final List<String> lore7 = new ArrayList<>();
        lore7.add(" ");
        lore7.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão direito para");
        lore7.add(ChatColor.GRAY + " verificar os itens que este kit");
        lore7.add(ChatColor.GRAY + " disponibiliza.");
        lore7.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão esquerdo para");
        lore7.add(ChatColor.GRAY + " pegar o kit.");
        lore7.add(" ");
        meta7.setLore(lore7);
        VIP2HORAS.setItemMeta(meta7);

        final ItemStack VIP2DIARIO = new ItemStack(Material.DIAMOND);
        final ItemMeta meta8 = VIP2DIARIO.getItemMeta();
        meta8.setDisplayName(ChatColor.YELLOW + " * Kit diário " + ChatColor.AQUA + "(Duque)");
        final List<String> lore8 = new ArrayList<>();
        lore8.add(" ");
        lore8.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão direito para");
        lore8.add(ChatColor.GRAY + " verificar os itens que este kit");
        lore8.add(ChatColor.GRAY + " disponibiliza.");
        lore8.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão esquerdo para");
        lore8.add(ChatColor.GRAY + " pegar o kit.");
        lore8.add(" ");
        meta8.setLore(lore8);
        VIP2DIARIO.setItemMeta(meta8);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta = voltar.getItemMeta();
        meta.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta);

        inventory.setItem(10, VIP2MES);
        inventory.setItem(12, VIP2SEMANAL);
        inventory.setItem(14, VIP2HORAS);
        inventory.setItem(16, VIP2DIARIO);
        inventory.setItem(18, voltar);

        return inventory;
    }
    //-

    public static Inventory inventoryKitsVipTitan() {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Kits VIP (Titan): ");

        final ItemStack VIP2MES = new ItemStack(Material.IRON_BLOCK);
        final ItemMeta meta5 = VIP2MES.getItemMeta();
        meta5.setDisplayName(ChatColor.YELLOW + " * Kit mensal " + ChatColor.AQUA + "(Titan)");
        final List<String> lore5 = new ArrayList<>();
        lore5.add(" ");
        lore5.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão direito para");
        lore5.add(ChatColor.GRAY + " verificar os itens que este kit");
        lore5.add(ChatColor.GRAY + " disponibiliza.");
        lore5.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão esquerdo para");
        lore5.add(ChatColor.GRAY + " pegar o kit.");
        lore5.add(" ");
        meta5.setLore(lore5);
        VIP2MES.setItemMeta(meta5);

        final ItemStack VIP2SEMANAL = new ItemStack(Material.IRON_ORE);
        final ItemMeta meta6 = VIP2SEMANAL.getItemMeta();
        meta6.setDisplayName(ChatColor.YELLOW + " * Kit semanal " + ChatColor.AQUA + "(Titan)");
        final List<String> lore6 = new ArrayList<>();
        lore6.add(" ");
        lore6.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão direito para");
        lore6.add(ChatColor.GRAY + " verificar os itens que este kit");
        lore6.add(ChatColor.GRAY + " disponibiliza.");
        lore6.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão esquerdo para");
        lore6.add(ChatColor.GRAY + " pegar o kit.");
        lore6.add(" ");
        meta6.setLore(lore6);
        VIP2SEMANAL.setItemMeta(meta6);

        final ItemStack VIP2HORAS = new ItemStack(Material.IRON_PICKAXE);
        final ItemMeta meta7 = VIP2HORAS.getItemMeta();
        meta7.setDisplayName(ChatColor.YELLOW + " * Kit, 6 horas jogadas " + ChatColor.AQUA + "(Titan)");
        final List<String> lore7 = new ArrayList<>();
        lore7.add(" ");
        lore7.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão direito para");
        lore7.add(ChatColor.GRAY + " verificar os itens que este kit");
        lore7.add(ChatColor.GRAY + " disponibiliza.");
        lore7.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão esquerdo para");
        lore7.add(ChatColor.GRAY + " pegar o kit.");
        lore7.add(" ");
        meta7.setLore(lore7);
        VIP2HORAS.setItemMeta(meta7);

        final ItemStack VIP2DIARIO = new ItemStack(Material.IRON_INGOT);
        final ItemMeta meta8 = VIP2DIARIO.getItemMeta();
        meta8.setDisplayName(ChatColor.YELLOW + " * Kit diário " + ChatColor.AQUA + "(Titan)");
        final List<String> lore8 = new ArrayList<>();
        lore8.add(" ");
        lore8.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão direito para");
        lore8.add(ChatColor.GRAY + " verificar os itens que este kit");
        lore8.add(ChatColor.GRAY + " disponibiliza.");
        lore8.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão esquerdo para");
        lore8.add(ChatColor.GRAY + " pegar o kit.");
        lore8.add(" ");
        meta8.setLore(lore8);
        VIP2DIARIO.setItemMeta(meta8);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta = voltar.getItemMeta();
        meta.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta);

        inventory.setItem(10, VIP2MES);
        inventory.setItem(12, VIP2SEMANAL);
        inventory.setItem(14, VIP2HORAS);
        inventory.setItem(16, VIP2DIARIO);
        inventory.setItem(18, voltar);

        return inventory;
    }

    public static Inventory inventoryKitsVipLord() {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Kits VIP (Lord): ");

        final ItemStack VIP2MES = new ItemStack(Material.GOLD_BLOCK);
        final ItemMeta meta5 = VIP2MES.getItemMeta();
        meta5.setDisplayName(ChatColor.YELLOW + " * Kit mensal " + ChatColor.AQUA + "(Lord)");
        final List<String> lore5 = new ArrayList<>();
        lore5.add(" ");
        lore5.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão direito para");
        lore5.add(ChatColor.GRAY + " verificar os itens que este kit");
        lore5.add(ChatColor.GRAY + " disponibiliza.");
        lore5.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão esquerdo para");
        lore5.add(ChatColor.GRAY + " pegar o kit.");
        lore5.add(" ");
        meta5.setLore(lore5);
        VIP2MES.setItemMeta(meta5);

        final ItemStack VIP2SEMANAL = new ItemStack(Material.GOLD_ORE);
        final ItemMeta meta6 = VIP2SEMANAL.getItemMeta();
        meta6.setDisplayName(ChatColor.YELLOW + " * Kit semanal " + ChatColor.AQUA + "(Lord)");
        final List<String> lore6 = new ArrayList<>();
        lore6.add(" ");
        lore6.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão direito para");
        lore6.add(ChatColor.GRAY + " verificar os itens que este kit");
        lore6.add(ChatColor.GRAY + " disponibiliza.");
        lore6.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão esquerdo para");
        lore6.add(ChatColor.GRAY + " pegar o kit.");
        lore6.add(" ");
        meta6.setLore(lore6);
        VIP2SEMANAL.setItemMeta(meta6);

        final ItemStack VIP2HORAS = new ItemStack(Material.GOLDEN_PICKAXE);
        final ItemMeta meta7 = VIP2HORAS.getItemMeta();
        meta7.setDisplayName(ChatColor.YELLOW + " * Kit, 6 horas jogadas " + ChatColor.AQUA + "(Lord)");
        final List<String> lore7 = new ArrayList<>();
        lore7.add(" ");
        lore7.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão direito para");
        lore7.add(ChatColor.GRAY + " verificar os itens que este kit");
        lore7.add(ChatColor.GRAY + " disponibiliza.");
        lore7.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão esquerdo para");
        lore7.add(ChatColor.GRAY + " pegar o kit.");
        lore7.add(" ");
        meta7.setLore(lore7);
        VIP2HORAS.setItemMeta(meta7);

        final ItemStack VIP2DIARIO = new ItemStack(Material.GOLD_INGOT);
        final ItemMeta meta8 = VIP2DIARIO.getItemMeta();
        meta8.setDisplayName(ChatColor.YELLOW + " * Kit diário " + ChatColor.AQUA + "(Lord)");
        final List<String> lore8 = new ArrayList<>();
        lore8.add(" ");
        lore8.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão direito para");
        lore8.add(ChatColor.GRAY + " verificar os itens que este kit");
        lore8.add(ChatColor.GRAY + " disponibiliza.");
        lore8.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão esquerdo para");
        lore8.add(ChatColor.GRAY + " pegar o kit.");
        lore8.add(" ");
        meta8.setLore(lore8);
        VIP2DIARIO.setItemMeta(meta8);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta = voltar.getItemMeta();
        meta.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta);

        inventory.setItem(10, VIP2MES);
        inventory.setItem(12, VIP2SEMANAL);
        inventory.setItem(14, VIP2HORAS);
        inventory.setItem(16, VIP2DIARIO);
        inventory.setItem(18, voltar);

        return inventory;
    }

    public static Inventory inventoryKitsVipConde() {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Kits VIP (Conde): ");

        final ItemStack VIP1MES = new ItemStack(Material.COAL_BLOCK);
        final ItemMeta meta1 = VIP1MES.getItemMeta();
        meta1.setDisplayName(ChatColor.YELLOW + " * Kit mensal (Conde)");
        final List<String> lore1 = new ArrayList<>();
        lore1.add(" ");
        lore1.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão direito para");
        lore1.add(ChatColor.GRAY + " verificar os itens que este kit");
        lore1.add(ChatColor.GRAY + " disponibiliza.");
        lore1.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão esquerdo para");
        lore1.add(ChatColor.GRAY + " pegar o kit.");
        lore1.add(" ");
        meta1.setLore(lore1);
        VIP1MES.setItemMeta(meta1);

        final ItemStack VIP1SEMANAL = new ItemStack(Material.COAL_ORE);
        final ItemMeta meta2 = VIP1SEMANAL.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + " * Kit semanal (Conde)");
        final List<String> lore2 = new ArrayList<>();
        lore2.add(" ");
        lore2.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão direito para");
        lore2.add(ChatColor.GRAY + " verificar os itens que este kit");
        lore2.add(ChatColor.GRAY + " disponibiliza.");
        lore2.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão esquerdo para");
        lore2.add(ChatColor.GRAY + " pegar o kit.");
        lore2.add(" ");
        meta2.setLore(lore2);
        VIP1SEMANAL.setItemMeta(meta2);

        final ItemStack VIP1HORAS = new ItemStack(Material.STONE_PICKAXE);
        final ItemMeta meta3 = VIP1HORAS.getItemMeta();
        meta3.setDisplayName(ChatColor.YELLOW + " * Kit, 6 horas jogadas (Conde)");
        final List<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão direito para");
        lore3.add(ChatColor.GRAY + " verificar os itens que este kit");
        lore3.add(ChatColor.GRAY + " disponibiliza.");
        lore3.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão esquerdo para");
        lore3.add(ChatColor.GRAY + " pegar o kit.");
        lore3.add(" ");
        meta3.setLore(lore3);
        VIP1HORAS.setItemMeta(meta3);

        final ItemStack VIP1DIARIO = new ItemStack(Material.COAL);
        final ItemMeta meta4 = VIP1DIARIO.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + " * Kit diário (Conde)");
        final List<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão direito para");
        lore4.add(ChatColor.GRAY + " verificar os itens que este kit");
        lore4.add(ChatColor.GRAY + " disponibiliza.");
        lore4.add(ChatColor.RED + " (!) " + ChatColor.GRAY + "Clique com o botão esquerdo para");
        lore4.add(ChatColor.GRAY + " pegar o kit.");
        lore4.add(" ");
        meta4.setLore(lore4);
        VIP1DIARIO.setItemMeta(meta4);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta = voltar.getItemMeta();
        meta.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta);

        inventory.setItem(10, VIP1MES);
        inventory.setItem(12, VIP1SEMANAL);
        inventory.setItem(14, VIP1HORAS);
        inventory.setItem(16, VIP1DIARIO);
        inventory.setItem(18, voltar);

        return inventory;
    }
}
