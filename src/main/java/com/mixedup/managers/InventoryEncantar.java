package com.mixedup.managers;

import com.mixedup.mcmmo.McMMOApi;
import com.mixedup.utils.RemoveItemInv;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

public class InventoryEncantar {

    public static void giveBook(final ItemStack book, final int nivel, final Player player) {
        final Random random = new Random();
        int sorte = 0;
        int soma = 0;
        if (nivel == 1) {
            sorte = random.nextInt(9);
            soma = 71;
        } else if (nivel == 2) {
            sorte = random.nextInt(9);
            soma = 61;
        } else if (nivel == 3) {
            sorte = random.nextInt(9);
            soma = 51;
        } else if (nivel == 4) {
            sorte = random.nextInt(9);
            soma = 41;
        } else if (nivel == 5) {
            sorte = random.nextInt(10);
            soma = 30;
        }

        final EnchantmentStorageMeta meta = (EnchantmentStorageMeta) book.getItemMeta();
        final ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.GRAY + "Sorte: " + (soma + sorte) + "%");
        lore.add(" ");
        meta.setLore(lore);
        book.setItemMeta(meta);

        RemoveItemInv.removeItemDifferentLore(player.getInventory(), player.getItemInHand(), 1);

        final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(book);
        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
        }
        player.getOpenInventory().close();
        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        player.playSound(player.getLocation(), Sound.ENTITY_VEX_CHARGE, 1.0f, 1.0f);
        player.sendMessage(ChatColor.GREEN + " * Livro pego com sucesso.");
    }

    public static Inventory invEnchant5() {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Encantamentos nível 5: ");

        final ItemStack power = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta powerMeta = (EnchantmentStorageMeta) power.getItemMeta();
        powerMeta.addStoredEnchant(Enchantment.ARROW_DAMAGE, 5, true);
        power.setItemMeta(powerMeta);
        inventory.setItem(15, power);

        final ItemStack efficiency = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta effMeta = (EnchantmentStorageMeta) efficiency.getItemMeta();
        effMeta.addStoredEnchant(Enchantment.DIG_SPEED, 5, true);
        efficiency.setItemMeta(effMeta);
        inventory.setItem(14, efficiency);

        final ItemStack arthropods = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta arthMeta = (EnchantmentStorageMeta) arthropods.getItemMeta();
        arthMeta.addStoredEnchant(Enchantment.DAMAGE_ARTHROPODS, 5, true);
        arthropods.setItemMeta(arthMeta);
        inventory.setItem(13, arthropods);

        final ItemStack smite = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta smiteMeta = (EnchantmentStorageMeta) smite.getItemMeta();
        smiteMeta.addStoredEnchant(Enchantment.DAMAGE_UNDEAD, 5, true);
        smite.setItemMeta(smiteMeta);
        inventory.setItem(12, smite);

        final ItemStack sharpness = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta sharpMeta = (EnchantmentStorageMeta) sharpness.getItemMeta();
        sharpMeta.addStoredEnchant(Enchantment.DAMAGE_ALL, 5, true);
        sharpness.setItemMeta(sharpMeta);
        inventory.setItem(11, sharpness);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta = voltar.getItemMeta();
        meta.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta);
        inventory.setItem(18, voltar);

        return inventory;
    }

    public static Inventory invEnchant4() {
        final Inventory inventory = Bukkit.createInventory(null, 4 * 9, "Encantamentos nível 4: ");

        final ItemStack power = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta powerMeta = (EnchantmentStorageMeta) power.getItemMeta();
        powerMeta.addStoredEnchant(Enchantment.ARROW_DAMAGE, 4, true);
        power.setItemMeta(powerMeta);
        inventory.setItem(23, power);

        final ItemStack efficiency = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta effMeta = (EnchantmentStorageMeta) efficiency.getItemMeta();
        effMeta.addStoredEnchant(Enchantment.DIG_SPEED, 4, true);
        efficiency.setItemMeta(effMeta);
        inventory.setItem(22, efficiency);

        final ItemStack arthropods = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta arthMeta = (EnchantmentStorageMeta) arthropods.getItemMeta();
        arthMeta.addStoredEnchant(Enchantment.DAMAGE_ARTHROPODS, 4, true);
        arthropods.setItemMeta(arthMeta);
        inventory.setItem(21, arthropods);

        final ItemStack smite = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta smiteMeta = (EnchantmentStorageMeta) smite.getItemMeta();
        smiteMeta.addStoredEnchant(Enchantment.DAMAGE_UNDEAD, 4, true);
        smite.setItemMeta(smiteMeta);
        inventory.setItem(16, smite);

        final ItemStack sharpness = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta sharpMeta = (EnchantmentStorageMeta) sharpness.getItemMeta();
        sharpMeta.addStoredEnchant(Enchantment.DAMAGE_ALL, 4, true);
        sharpness.setItemMeta(sharpMeta);
        inventory.setItem(15, sharpness);

        final ItemStack pexprotection = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta pexprocMeta = (EnchantmentStorageMeta) pexprotection.getItemMeta();
        pexprocMeta.addStoredEnchant(Enchantment.PROTECTION_EXPLOSIONS, 4, true);
        pexprotection.setItemMeta(pexprocMeta);
        inventory.setItem(14, pexprotection);

        final ItemStack pprotection = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta pprocMeta = (EnchantmentStorageMeta) pprotection.getItemMeta();
        pprocMeta.addStoredEnchant(Enchantment.PROTECTION_PROJECTILE, 4, true);
        pprotection.setItemMeta(pprocMeta);
        inventory.setItem(13, pprotection);

        final ItemStack feather = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta feaMeta = (EnchantmentStorageMeta) feather.getItemMeta();
        feaMeta.addStoredEnchant(Enchantment.PROTECTION_FALL, 4, true);
        feather.setItemMeta(feaMeta);
        inventory.setItem(12, feather);

        final ItemStack fprotection = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta fprocMeta = (EnchantmentStorageMeta) fprotection.getItemMeta();
        fprocMeta.addStoredEnchant(Enchantment.PROTECTION_FIRE, 4, true);
        fprotection.setItemMeta(fprocMeta);
        inventory.setItem(11, fprotection);

        final ItemStack protection = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta procMeta = (EnchantmentStorageMeta) protection.getItemMeta();
        procMeta.addStoredEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4, true);
        protection.setItemMeta(procMeta);
        inventory.setItem(10, protection);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta = voltar.getItemMeta();
        meta.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta);
        inventory.setItem(27, voltar);

        return inventory;
    }

    public static Inventory invEnchant3() {
        final Inventory inventory = Bukkit.createInventory(null, 5 * 9, "Encantamentos nível 3: ");

        final ItemStack lure = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta lureMeta = (EnchantmentStorageMeta) lure.getItemMeta();
        lureMeta.addStoredEnchant(Enchantment.LURE, 3, true);
        lure.setItemMeta(lureMeta);
        inventory.setItem(31, lure);

        final ItemStack lucksea = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta lucksMeta = (EnchantmentStorageMeta) lucksea.getItemMeta();
        lucksMeta.addStoredEnchant(Enchantment.LUCK, 3, true);
        lucksea.setItemMeta(lucksMeta);
        inventory.setItem(30, lucksea);

        final ItemStack power = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta powerMeta = (EnchantmentStorageMeta) power.getItemMeta();
        powerMeta.addStoredEnchant(Enchantment.ARROW_DAMAGE, 3, true);
        power.setItemMeta(powerMeta);
        inventory.setItem(29, power);

        final ItemStack fortune = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta fortMeta = (EnchantmentStorageMeta) fortune.getItemMeta();
        fortMeta.addStoredEnchant(Enchantment.LOOT_BONUS_BLOCKS, 3, true);
        fortune.setItemMeta(fortMeta);
        inventory.setItem(28, fortune);

        final ItemStack unbreaking = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta unbreMeta = (EnchantmentStorageMeta) unbreaking.getItemMeta();
        unbreMeta.addStoredEnchant(Enchantment.DURABILITY, 3, true);
        unbreaking.setItemMeta(unbreMeta);
        inventory.setItem(25, unbreaking);

        final ItemStack efficiency = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta effMeta = (EnchantmentStorageMeta) efficiency.getItemMeta();
        effMeta.addStoredEnchant(Enchantment.DIG_SPEED, 3, true);
        efficiency.setItemMeta(effMeta);
        inventory.setItem(24, efficiency);

        final ItemStack looting = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta looMeta = (EnchantmentStorageMeta) looting.getItemMeta();
        looMeta.addStoredEnchant(Enchantment.LOOT_BONUS_MOBS, 3, true);
        looting.setItemMeta(looMeta);
        inventory.setItem(23, looting);

        final ItemStack arthropods = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta arthMeta = (EnchantmentStorageMeta) arthropods.getItemMeta();
        arthMeta.addStoredEnchant(Enchantment.DAMAGE_ARTHROPODS, 3, true);
        arthropods.setItemMeta(arthMeta);
        inventory.setItem(22, arthropods);

        final ItemStack smite = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta smiteMeta = (EnchantmentStorageMeta) smite.getItemMeta();
        smiteMeta.addStoredEnchant(Enchantment.DAMAGE_UNDEAD, 3, true);
        smite.setItemMeta(smiteMeta);
        inventory.setItem(21, smite);

        final ItemStack sharpness = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta sharpMeta = (EnchantmentStorageMeta) sharpness.getItemMeta();
        sharpMeta.addStoredEnchant(Enchantment.DAMAGE_ALL, 3, true);
        sharpness.setItemMeta(sharpMeta);
        inventory.setItem(20, sharpness);

        final ItemStack depth = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta depthMeta = (EnchantmentStorageMeta) depth.getItemMeta();
        depthMeta.addStoredEnchant(Enchantment.DEPTH_STRIDER, 3, true);
        depth.setItemMeta(depthMeta);
        inventory.setItem(19, depth);

        final ItemStack thorns = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta thornsMeta = (EnchantmentStorageMeta) thorns.getItemMeta();
        thornsMeta.addStoredEnchant(Enchantment.THORNS, 3, true);
        thorns.setItemMeta(thornsMeta);
        inventory.setItem(16, thorns);

        final ItemStack respprotection = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta resprocMeta = (EnchantmentStorageMeta) respprotection.getItemMeta();
        resprocMeta.addStoredEnchant(Enchantment.OXYGEN, 3, true);
        respprotection.setItemMeta(resprocMeta);
        inventory.setItem(15, respprotection);

        final ItemStack pexprotection = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta pexprocMeta = (EnchantmentStorageMeta) pexprotection.getItemMeta();
        pexprocMeta.addStoredEnchant(Enchantment.PROTECTION_EXPLOSIONS, 3, true);
        pexprotection.setItemMeta(pexprocMeta);
        inventory.setItem(14, pexprotection);

        final ItemStack pprotection = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta pprocMeta = (EnchantmentStorageMeta) pprotection.getItemMeta();
        pprocMeta.addStoredEnchant(Enchantment.PROTECTION_PROJECTILE, 3, true);
        pprotection.setItemMeta(pprocMeta);
        inventory.setItem(13, pprotection);

        final ItemStack feather = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta feaMeta = (EnchantmentStorageMeta) feather.getItemMeta();
        feaMeta.addStoredEnchant(Enchantment.PROTECTION_FALL, 3, true);
        feather.setItemMeta(feaMeta);
        inventory.setItem(12, feather);

        final ItemStack fprotection = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta fprocMeta = (EnchantmentStorageMeta) fprotection.getItemMeta();
        fprocMeta.addStoredEnchant(Enchantment.PROTECTION_FIRE, 3, true);
        fprotection.setItemMeta(fprocMeta);
        inventory.setItem(11, fprotection);

        final ItemStack protection = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta procMeta = (EnchantmentStorageMeta) protection.getItemMeta();
        procMeta.addStoredEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true);
        protection.setItemMeta(procMeta);
        inventory.setItem(10, protection);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta = voltar.getItemMeta();
        meta.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta);
        inventory.setItem(36, voltar);

        return inventory;
    }

    public static Inventory invEnchant2() {
        final Inventory inventory = Bukkit.createInventory(null, 5 * 9, "Encantamentos nível 2: ");

        final ItemStack lure = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta lureMeta = (EnchantmentStorageMeta) lure.getItemMeta();
        lureMeta.addStoredEnchant(Enchantment.LURE, 2, true);
        lure.setItemMeta(lureMeta);
        inventory.setItem(34, lure);

        final ItemStack lucksea = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta lucksMeta = (EnchantmentStorageMeta) lucksea.getItemMeta();
        lucksMeta.addStoredEnchant(Enchantment.LUCK, 2, true);
        lucksea.setItemMeta(lucksMeta);
        inventory.setItem(33, lucksea);

        final ItemStack punch = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta punchMeta = (EnchantmentStorageMeta) punch.getItemMeta();
        punchMeta.addStoredEnchant(Enchantment.ARROW_KNOCKBACK, 2, true);
        punch.setItemMeta(punchMeta);
        inventory.setItem(32, punch);

        final ItemStack power = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta powerMeta = (EnchantmentStorageMeta) power.getItemMeta();
        powerMeta.addStoredEnchant(Enchantment.ARROW_DAMAGE, 2, true);
        power.setItemMeta(powerMeta);
        inventory.setItem(31, power);

        final ItemStack fortune = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta fortMeta = (EnchantmentStorageMeta) fortune.getItemMeta();
        fortMeta.addStoredEnchant(Enchantment.LOOT_BONUS_BLOCKS, 2, true);
        fortune.setItemMeta(fortMeta);
        inventory.setItem(30, fortune);

        final ItemStack unbreaking = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta unbreMeta = (EnchantmentStorageMeta) unbreaking.getItemMeta();
        unbreMeta.addStoredEnchant(Enchantment.DURABILITY, 2, true);
        unbreaking.setItemMeta(unbreMeta);
        inventory.setItem(29, unbreaking);

        final ItemStack efficiency = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta effMeta = (EnchantmentStorageMeta) efficiency.getItemMeta();
        effMeta.addStoredEnchant(Enchantment.DIG_SPEED, 2, true);
        efficiency.setItemMeta(effMeta);
        inventory.setItem(28, efficiency);

        final ItemStack looting = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta looMeta = (EnchantmentStorageMeta) looting.getItemMeta();
        looMeta.addStoredEnchant(Enchantment.LOOT_BONUS_MOBS, 2, true);
        looting.setItemMeta(looMeta);
        inventory.setItem(25, looting);

        final ItemStack fireaspect = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta fireaMeta = (EnchantmentStorageMeta) fireaspect.getItemMeta();
        fireaMeta.addStoredEnchant(Enchantment.FIRE_ASPECT, 2, true);
        fireaspect.setItemMeta(fireaMeta);
        inventory.setItem(24, fireaspect);

        final ItemStack knockback = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta knockMeta = (EnchantmentStorageMeta) knockback.getItemMeta();
        knockMeta.addStoredEnchant(Enchantment.KNOCKBACK, 2, true);
        knockback.setItemMeta(knockMeta);
        inventory.setItem(23, knockback);

        final ItemStack arthropods = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta arthMeta = (EnchantmentStorageMeta) arthropods.getItemMeta();
        arthMeta.addStoredEnchant(Enchantment.DAMAGE_ARTHROPODS, 2, true);
        arthropods.setItemMeta(arthMeta);
        inventory.setItem(22, arthropods);

        final ItemStack smite = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta smiteMeta = (EnchantmentStorageMeta) smite.getItemMeta();
        smiteMeta.addStoredEnchant(Enchantment.DAMAGE_UNDEAD, 2, true);
        smite.setItemMeta(smiteMeta);
        inventory.setItem(21, smite);

        final ItemStack sharpness = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta sharpMeta = (EnchantmentStorageMeta) sharpness.getItemMeta();
        sharpMeta.addStoredEnchant(Enchantment.DAMAGE_ALL, 2, true);
        sharpness.setItemMeta(sharpMeta);
        inventory.setItem(20, sharpness);

        final ItemStack depth = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta depthMeta = (EnchantmentStorageMeta) depth.getItemMeta();
        depthMeta.addStoredEnchant(Enchantment.DEPTH_STRIDER, 2, true);
        depth.setItemMeta(depthMeta);
        inventory.setItem(19, depth);

        final ItemStack thorns = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta thornsMeta = (EnchantmentStorageMeta) thorns.getItemMeta();
        thornsMeta.addStoredEnchant(Enchantment.THORNS, 2, true);
        thorns.setItemMeta(thornsMeta);
        inventory.setItem(16, thorns);

        final ItemStack respprotection = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta resprocMeta = (EnchantmentStorageMeta) respprotection.getItemMeta();
        resprocMeta.addStoredEnchant(Enchantment.OXYGEN, 2, true);
        respprotection.setItemMeta(resprocMeta);
        inventory.setItem(15, respprotection);

        final ItemStack pexprotection = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta pexprocMeta = (EnchantmentStorageMeta) pexprotection.getItemMeta();
        pexprocMeta.addStoredEnchant(Enchantment.PROTECTION_EXPLOSIONS, 2, true);
        pexprotection.setItemMeta(pexprocMeta);
        inventory.setItem(14, pexprotection);

        final ItemStack pprotection = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta pprocMeta = (EnchantmentStorageMeta) pprotection.getItemMeta();
        pprocMeta.addStoredEnchant(Enchantment.PROTECTION_PROJECTILE, 2, true);
        pprotection.setItemMeta(pprocMeta);
        inventory.setItem(13, pprotection);

        final ItemStack feather = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta feaMeta = (EnchantmentStorageMeta) feather.getItemMeta();
        feaMeta.addStoredEnchant(Enchantment.PROTECTION_FALL, 2, true);
        feather.setItemMeta(feaMeta);
        inventory.setItem(12, feather);

        final ItemStack fprotection = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta fprocMeta = (EnchantmentStorageMeta) fprotection.getItemMeta();
        fprocMeta.addStoredEnchant(Enchantment.PROTECTION_FIRE, 2, true);
        fprotection.setItemMeta(fprocMeta);
        inventory.setItem(11, fprotection);

        final ItemStack protection = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta procMeta = (EnchantmentStorageMeta) protection.getItemMeta();
        procMeta.addStoredEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2, true);
        protection.setItemMeta(procMeta);
        inventory.setItem(10, protection);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta = voltar.getItemMeta();
        meta.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta);
        inventory.setItem(36, voltar);

        return inventory;
    }

    public static Inventory invEnchant1() {
        final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Encantamentos nível 1: ");

        final ItemStack lure = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta lureMeta = (EnchantmentStorageMeta) lure.getItemMeta();
        lureMeta.addStoredEnchant(Enchantment.LURE, 1, true);
        lure.setItemMeta(lureMeta);
        inventory.setItem(41, lure);

        final ItemStack lucksea = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta lucksMeta = (EnchantmentStorageMeta) lucksea.getItemMeta();
        lucksMeta.addStoredEnchant(Enchantment.LUCK, 1, true);
        lucksea.setItemMeta(lucksMeta);
        inventory.setItem(40, lucksea);

        final ItemStack mending = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta mendingiMeta = (EnchantmentStorageMeta) mending.getItemMeta();
        mendingiMeta.addStoredEnchant(Enchantment.MENDING, 1, true);
        mending.setItemMeta(mendingiMeta);
        inventory.setItem(39, mending);

        final ItemStack infinity = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta infiMeta = (EnchantmentStorageMeta) infinity.getItemMeta();
        infiMeta.addStoredEnchant(Enchantment.ARROW_INFINITE, 1, true);
        infinity.setItemMeta(infiMeta);
        inventory.setItem(38, infinity);

        final ItemStack flame = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta flameMeta = (EnchantmentStorageMeta) flame.getItemMeta();
        flameMeta.addStoredEnchant(Enchantment.ARROW_FIRE, 1, true);
        flame.setItemMeta(flameMeta);
        inventory.setItem(37, flame);

        final ItemStack punch = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta punchMeta = (EnchantmentStorageMeta) punch.getItemMeta();
        punchMeta.addStoredEnchant(Enchantment.ARROW_KNOCKBACK, 1, true);
        punch.setItemMeta(punchMeta);
        inventory.setItem(34, punch);

        final ItemStack power = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta powerMeta = (EnchantmentStorageMeta) power.getItemMeta();
        powerMeta.addStoredEnchant(Enchantment.ARROW_DAMAGE, 1, true);
        power.setItemMeta(powerMeta);
        inventory.setItem(33, power);

        final ItemStack fortune = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta fortMeta = (EnchantmentStorageMeta) fortune.getItemMeta();
        fortMeta.addStoredEnchant(Enchantment.LOOT_BONUS_BLOCKS, 1, true);
        fortune.setItemMeta(fortMeta);
        inventory.setItem(32, fortune);

        final ItemStack unbreaking = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta unbreMeta = (EnchantmentStorageMeta) unbreaking.getItemMeta();
        unbreMeta.addStoredEnchant(Enchantment.DURABILITY, 1, true);
        unbreaking.setItemMeta(unbreMeta);
        inventory.setItem(31, unbreaking);

        final ItemStack silktouch = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta silkMeta = (EnchantmentStorageMeta) silktouch.getItemMeta();
        silkMeta.addStoredEnchant(Enchantment.SILK_TOUCH, 1, true);
        silktouch.setItemMeta(silkMeta);
        inventory.setItem(30, silktouch);

        final ItemStack efficiency = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta effMeta = (EnchantmentStorageMeta) efficiency.getItemMeta();
        effMeta.addStoredEnchant(Enchantment.DIG_SPEED, 1, true);
        efficiency.setItemMeta(effMeta);
        inventory.setItem(29, efficiency);

        final ItemStack looting = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta looMeta = (EnchantmentStorageMeta) looting.getItemMeta();
        looMeta.addStoredEnchant(Enchantment.LOOT_BONUS_MOBS, 1, true);
        looting.setItemMeta(looMeta);
        inventory.setItem(28, looting);

        final ItemStack fireaspect = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta fireaMeta = (EnchantmentStorageMeta) fireaspect.getItemMeta();
        fireaMeta.addStoredEnchant(Enchantment.FIRE_ASPECT, 1, true);
        fireaspect.setItemMeta(fireaMeta);
        inventory.setItem(25, fireaspect);

        final ItemStack knockback = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta knockMeta = (EnchantmentStorageMeta) knockback.getItemMeta();
        knockMeta.addStoredEnchant(Enchantment.KNOCKBACK, 1, true);
        knockback.setItemMeta(knockMeta);
        inventory.setItem(24, knockback);

        final ItemStack arthropods = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta arthMeta = (EnchantmentStorageMeta) arthropods.getItemMeta();
        arthMeta.addStoredEnchant(Enchantment.DAMAGE_ARTHROPODS, 1, true);
        arthropods.setItemMeta(arthMeta);
        inventory.setItem(23, arthropods);

        final ItemStack smite = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta smiteMeta = (EnchantmentStorageMeta) smite.getItemMeta();
        smiteMeta.addStoredEnchant(Enchantment.DAMAGE_UNDEAD, 1, true);
        smite.setItemMeta(smiteMeta);
        inventory.setItem(22, smite);

        final ItemStack sharpness = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta sharpMeta = (EnchantmentStorageMeta) sharpness.getItemMeta();
        sharpMeta.addStoredEnchant(Enchantment.DAMAGE_ALL, 1, true);
        sharpness.setItemMeta(sharpMeta);
        inventory.setItem(21, sharpness);

        final ItemStack depth = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta depthMeta = (EnchantmentStorageMeta) depth.getItemMeta();
        depthMeta.addStoredEnchant(Enchantment.DEPTH_STRIDER, 1, true);
        depth.setItemMeta(depthMeta);
        inventory.setItem(20, depth);

        final ItemStack thorns = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta thornsMeta = (EnchantmentStorageMeta) thorns.getItemMeta();
        thornsMeta.addStoredEnchant(Enchantment.THORNS, 1, true);
        thorns.setItemMeta(thornsMeta);
        inventory.setItem(19, thorns);

        final ItemStack aquaprotection = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta aquaMeta = (EnchantmentStorageMeta) aquaprotection.getItemMeta();
        aquaMeta.addStoredEnchant(Enchantment.WATER_WORKER, 1, true);
        aquaprotection.setItemMeta(aquaMeta);
        inventory.setItem(16, aquaprotection);

        final ItemStack respprotection = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta resprocMeta = (EnchantmentStorageMeta) respprotection.getItemMeta();
        resprocMeta.addStoredEnchant(Enchantment.OXYGEN, 1, true);
        respprotection.setItemMeta(resprocMeta);
        inventory.setItem(15, respprotection);

        final ItemStack pexprotection = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta pexprocMeta = (EnchantmentStorageMeta) pexprotection.getItemMeta();
        pexprocMeta.addStoredEnchant(Enchantment.PROTECTION_EXPLOSIONS, 1, true);
        pexprotection.setItemMeta(pexprocMeta);
        inventory.setItem(14, pexprotection);

        final ItemStack pprotection = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta pprocMeta = (EnchantmentStorageMeta) pprotection.getItemMeta();
        pprocMeta.addStoredEnchant(Enchantment.PROTECTION_PROJECTILE, 1, true);
        pprotection.setItemMeta(pprocMeta);
        inventory.setItem(13, pprotection);

        final ItemStack feather = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta feaMeta = (EnchantmentStorageMeta) feather.getItemMeta();
        feaMeta.addStoredEnchant(Enchantment.PROTECTION_FALL, 1, true);
        feather.setItemMeta(feaMeta);
        inventory.setItem(12, feather);

        final ItemStack fprotection = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta fprocMeta = (EnchantmentStorageMeta) fprotection.getItemMeta();
        fprocMeta.addStoredEnchant(Enchantment.PROTECTION_FIRE, 1, true);
        fprotection.setItemMeta(fprocMeta);
        inventory.setItem(11, fprotection);

        final ItemStack protection = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta procMeta = (EnchantmentStorageMeta) protection.getItemMeta();
        procMeta.addStoredEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
        protection.setItemMeta(procMeta);
        inventory.setItem(10, protection);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta = voltar.getItemMeta();
        meta.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta);
        inventory.setItem(45, voltar);

        return inventory;
    }

    public static Inventory invNivel(final String UUID, final boolean nivel1a2, final boolean nivel1a4, final boolean nivel1a5, final Player player) {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Encantamentos níveis: ");

        final ItemStack nivel1 = new ItemStack(Material.BOOK);
        final ItemMeta meta1 = nivel1.getItemMeta();
        meta1.setDisplayName(ChatColor.YELLOW + "Encantamentos");
        final ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.GRAY + " Nível 1");
        lore.add(" ");
        meta1.setLore(lore);
        nivel1.setItemMeta(meta1);

        final ItemStack nivel2 = new ItemStack(Material.BOOK);
        final ItemMeta meta2 = nivel2.getItemMeta();
        meta2.setDisplayName(ChatColor.YELLOW + "Encantamentos");
        final ArrayList<String> lore1 = new ArrayList<>();
        lore1.add(" ");
        lore1.add(ChatColor.GRAY + " Nível 2");
        lore1.add(" ");
        meta2.setLore(lore1);
        nivel2.setItemMeta(meta2);

        final ItemStack nivel3 = new ItemStack(Material.BOOK);
        final ItemMeta meta3 = nivel3.getItemMeta();
        meta3.setDisplayName(ChatColor.YELLOW + "Encantamentos");
        final ArrayList<String> lore2 = new ArrayList<>();
        lore2.add(" ");
        lore2.add(ChatColor.GRAY + " Nível 3");
        lore2.add(" ");
        meta3.setLore(lore2);
        nivel3.setItemMeta(meta3);

        final ItemStack nivel4 = new ItemStack(Material.BOOK);
        final ItemMeta meta4 = nivel4.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Encantamentos");
        final ArrayList<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.GRAY + " Nível 4");
        lore3.add(" ");
        meta4.setLore(lore3);
        nivel4.setItemMeta(meta4);

        final ItemStack nivel5 = new ItemStack(Material.BOOK);
        final ItemMeta meta5 = nivel5.getItemMeta();
        meta5.setDisplayName(ChatColor.YELLOW + "Encantamentos");
        final ArrayList<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.GRAY + " Nível 5");
        lore4.add(" ");
        meta5.setLore(lore4);
        nivel5.setItemMeta(meta5);

        if (McMMOApi.getNivel(UUID + "Encantar") < 200 || nivel1a2 == true) {
            if (nivel1a4 == true || nivel1a5 == true) {
                player.sendMessage(ChatColor.RED + " * Ops, você não contém nível para pegar encantamentos altos.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
            inventory.setItem(12, nivel1);
            inventory.setItem(14, nivel2);
            return inventory;
        }
        if (McMMOApi.getNivel(UUID + "Encantar") >= 200 && McMMOApi.getNivel(UUID + "Encantar") < 400 || nivel1a4 == true) {
            if (nivel1a5 == true) {
                player.sendMessage(ChatColor.RED + " * Ops, você não contém nível para pegar encantamentos altos.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
            inventory.setItem(10, nivel1);
            inventory.setItem(12, nivel2);
            inventory.setItem(14, nivel3);
            inventory.setItem(16, nivel4);
            return inventory;
        }
        if (McMMOApi.getNivel(UUID + "Encantar") >= 400 || nivel1a5 == true) {
            inventory.setItem(11, nivel1);
            inventory.setItem(12, nivel2);
            inventory.setItem(13, nivel3);
            inventory.setItem(14, nivel4);
            inventory.setItem(15, nivel5);
            return inventory;
        }

        return inventory;
    }
}
