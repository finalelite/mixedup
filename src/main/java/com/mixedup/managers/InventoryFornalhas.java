package com.mixedup.managers;

import com.mixedup.apis.FurnaceAPI;
import com.mixedup.apis.TagAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class InventoryFornalhas {

    public static Inventory inventoryConfirm() {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Confirmar compra fornalha: ");

        final ItemStack confirm = new ItemStack(Material.LIME_DYE);
        final ItemMeta metaConfirm = confirm.getItemMeta();
        metaConfirm.setDisplayName(ChatColor.GREEN + "CONFIRMAR");
        confirm.setItemMeta(metaConfirm);

        final ItemStack negar = new ItemStack(Material.ROSE_RED);
        final ItemMeta metaNegar = negar.getItemMeta();
        metaNegar.setDisplayName(ChatColor.RED + "NEGAR");
        negar.setItemMeta(metaNegar);

        inventory.setItem(12, confirm);
        inventory.setItem(14, negar);

        return inventory;
    }

    public static Inventory inventory(final String UUID) {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Fornalha virtual: ");

        final ItemStack furnace = new ItemStack(Material.FURNACE);
        final ItemMeta meta = furnace.getItemMeta();
        meta.setDisplayName(ChatColor.YELLOW + "Fornalha");
        final ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.WHITE + "Comprar: " + ChatColor.GRAY + "$30.000,00 moedas");
        lore.add(" ");
        if (FurnaceAPI.has(UUID) == false) {
            String tag = TagAPI.getTag(UUID);
            if (!tag.equalsIgnoreCase("Vip") && !tag.equalsIgnoreCase("Vip+") && !tag.equalsIgnoreCase("Vip++") && !tag.equalsIgnoreCase("Vip+++")) {
                meta.setLore(lore);
            }
        }
        furnace.setItemMeta(meta);

        inventory.setItem(13, furnace);
        return inventory;
    }

    public static Inventory furnace(final ItemStack result) {
        final Inventory inventory = Bukkit.createInventory(null, InventoryType.FURNACE, "Fornalha privada");

        if (result != null) {
            inventory.setItem(2, result);
        }
        return inventory;
    }
}
