package com.mixedup.managers;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class InventoryIr {

    public static Inventory INVENTORY_IR = Bukkit.createInventory(null, 5 * 9, "Ir: ");

    public static void build() {
        InventoryIr.buildInvIr();
    }

    private static void buildInvIr() {
        final ItemStack areavip = new ItemStack(Material.NETHER_STAR);
        final ItemMeta metaAreavip = areavip.getItemMeta();
        metaAreavip.setDisplayName(ChatColor.AQUA + " * Área vip");
        final ArrayList<String> lore7 = new ArrayList<String>();
        lore7.add(" ");
        lore7.add(ChatColor.GRAY + "  Adquira " + ChatColor.GREEN + "VIP" + ChatColor.GRAY + " e obtenha vantagens como esta. ");
        lore7.add(ChatColor.RED + "  www.finalelite.com.br");
        lore7.add(" ");
        metaAreavip.setLore(lore7);
        areavip.setItemMeta(metaAreavip);

        final ItemStack spawn = new ItemStack(Material.BEACON);
        final ItemMeta metaSpawn = spawn.getItemMeta();
        metaSpawn.setDisplayName(ChatColor.AQUA + " * Spawn ");
        spawn.setItemMeta(metaSpawn);

        final ItemStack loja = new ItemStack(Material.SUNFLOWER);
        final ItemMeta metaLoja = loja.getItemMeta();
        metaLoja.setDisplayName(ChatColor.YELLOW + " * Loja ");
        loja.setItemMeta(metaLoja);

        final ItemStack mundof = new ItemStack(Material.MAGMA_BLOCK);
        final ItemMeta metaMundof = mundof.getItemMeta();
        metaMundof.setDisplayName(ChatColor.YELLOW + " * Planeta Trappist-1b");
        final ArrayList<String> lore6 = new ArrayList<String>();
        lore6.add(" ");
        lore6.add(ChatColor.GRAY + "  Mundo dos esquadrões, local destinado as ");
        lore6.add(ChatColor.GRAY + "  bases dos esquadrões!");
        lore6.add(" ");
        lore6.add(ChatColor.GRAY + "  Mobs: " + ChatColor.GREEN + "Ativos!");
        lore6.add(ChatColor.GRAY + "  Pvp: " + ChatColor.RED + "Ativo!");
        lore6.add(" ");
        metaMundof.setLore(lore6);
        mundof.setItemMeta(metaMundof);

        final ItemStack nether = new ItemStack(Material.NETHERRACK);
        final ItemMeta metaNether = nether.getItemMeta();
        metaNether.setDisplayName(ChatColor.YELLOW + " * Planeta Vênus (Nether)");
        final ArrayList<String> lore4 = new ArrayList<String>();
        lore4.add(" ");
        lore4.add(ChatColor.GRAY + "  Mobs: " + ChatColor.GREEN + "Ativos!");
        lore4.add(ChatColor.GRAY + "  Pvp: " + ChatColor.RED + "Ativo!");
        lore4.add(" ");
        metaNether.setLore(lore4);
        nether.setItemMeta(metaNether);

        final ItemStack mundo3 = new ItemStack(Material.COARSE_DIRT);
        final ItemMeta metaMundo3 = mundo3.getItemMeta();
        metaMundo3.setDisplayName(ChatColor.YELLOW + " * Planeta Kepler-438b");
        final ArrayList<String> lore2 = new ArrayList<String>();
        lore2.add(" ");
        lore2.add(ChatColor.GRAY + "  Os nobres reinam dentro deste planeta!");
        lore2.add(ChatColor.GRAY + "  Tenham cuidado, não ser um nobre e visitar");
        lore2.add(ChatColor.GRAY + "  este planeta pode ser perigoso!");
        lore2.add(" ");
        lore2.add(ChatColor.GRAY + "  Mobs: " + ChatColor.GREEN + "Ativos!");
        lore2.add(ChatColor.GRAY + "  Pvp: " + ChatColor.RED + "Ativo!");
        lore2.add(" ");
        metaMundo3.setLore(lore2);
        mundo3.setItemMeta(metaMundo3);

        final ItemStack elytra = new ItemStack(Material.ELYTRA);
        final ItemMeta metaElytra = elytra.getItemMeta();
        metaElytra.setDisplayName(ChatColor.YELLOW + " * Elytras");
        final ArrayList<String> lore10 = new ArrayList<>();
        lore10.add(" ");
        lore10.add(ChatColor.GRAY + "  Alcance as dungeons mais distantes, ");
        lore10.add(ChatColor.GRAY + "  evolua e conquiste este mundo.");
        lore10.add(" ");
        metaElytra.setLore(lore10);
        elytra.setItemMeta(metaElytra);

        final ItemStack guilda = new ItemStack(Material.HEART_OF_THE_SEA);
        final ItemMeta metaGuilda = guilda.getItemMeta();
        metaGuilda.setDisplayName(ChatColor.YELLOW + " * Guildas");
        final ArrayList<String> lore11 = new ArrayList<>();
        lore11.add(" ");
        lore11.add(ChatColor.GRAY + "  Evolua sua guilda e seja o mais forte! ");
        lore11.add(" ");
        lore2.add(ChatColor.GRAY + "  Pvp: " + ChatColor.RED + "Ativo de noite!");
        lore2.add(" ");
        metaGuilda.setLore(lore11);
        guilda.setItemMeta(metaGuilda);

        final ItemStack arena = new ItemStack(Material.DIAMOND_SWORD);
        final ItemMeta metaArena = arena.getItemMeta();
        metaArena.setDisplayName(ChatColor.YELLOW + " * Arena");
        arena.setItemMeta(metaArena);

        final ItemStack bank = new ItemStack(Material.CARROT_ON_A_STICK);
        final ItemMeta metaBank = bank.getItemMeta();
        metaBank.setDisplayName(ChatColor.YELLOW + " * Banqueiro ");
        bank.setItemMeta(metaBank);

        final ItemStack skygrid = new ItemStack(Material.GRASS_BLOCK);
        final ItemMeta metaSky = skygrid.getItemMeta();
        metaSky.setDisplayName(ChatColor.YELLOW + " * Skygrid");
        final ArrayList<String> lore12 = new ArrayList<>();
        lore12.add(" ");
        lore12.add(ChatColor.GRAY + "  Os sanguinarios reinam sobre este cinturão de");
        lore12.add(ChatColor.GRAY + "  asteróides, um mundo obsoleto com o qual apenas");
        lore12.add(ChatColor.GRAY + "  os mais resistentes e fortes resistentem a tal");
        lore12.add(ChatColor.GRAY + "  desafio, tenham cuidado pois as vezes os asteróides");
        lore12.add(ChatColor.GRAY + "  são o menor desafio!");
        lore12.add(" ");
        lore12.add(ChatColor.GRAY + "  Morte por queda: " + ChatColor.RED + "Ativo!");
        lore12.add(ChatColor.GRAY + "  Mobs: " + ChatColor.GREEN + "Ativos!");
        lore12.add(ChatColor.GRAY + "  Pvp: " + ChatColor.RED + "Ativo!");
        lore12.add(" ");
        metaSky.setLore(lore12);
        skygrid.setItemMeta(metaSky);

        final ItemStack blacks = new ItemStack(Material.HOPPER_MINECART);
        final ItemMeta metaB = blacks.getItemMeta();
        metaB.setDisplayName(ChatColor.YELLOW + " * Mercado negro ");
        final ArrayList<String> loreB = new ArrayList<>();
        loreB.add(" ");
        loreB.add(ChatColor.RED + "  Em breve.");
        loreB.add(" ");
        metaB.setLore(loreB);
        blacks.setItemMeta(metaB);

        InventoryIr.INVENTORY_IR.setItem(10, spawn);
        InventoryIr.INVENTORY_IR.setItem(12, skygrid);
        InventoryIr.INVENTORY_IR.setItem(13, mundo3);
        InventoryIr.INVENTORY_IR.setItem(14, mundof);
        InventoryIr.INVENTORY_IR.setItem(15, nether);
        InventoryIr.INVENTORY_IR.setItem(16, elytra);
        InventoryIr.INVENTORY_IR.setItem(28, arena);
        InventoryIr.INVENTORY_IR.setItem(29, guilda);
        InventoryIr.INVENTORY_IR.setItem(30, blacks);
        InventoryIr.INVENTORY_IR.setItem(32, bank);
        InventoryIr.INVENTORY_IR.setItem(33, areavip);
        InventoryIr.INVENTORY_IR.setItem(34, loja);
    }
}
