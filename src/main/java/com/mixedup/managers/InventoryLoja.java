package com.mixedup.managers;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class InventoryLoja {

    public static Inventory inv() {
        final Inventory inventory = Bukkit.createInventory(null, 5 * 9, "Loja virtual: ");

        final ItemStack blocos = new ItemStack(Material.COBBLESTONE);
        final ItemMeta metaBlocos = blocos.getItemMeta();
        metaBlocos.setDisplayName(ChatColor.YELLOW + "Blocos");
        blocos.setItemMeta(metaBlocos);
        inventory.setItem(10, blocos);

        final ItemStack decoracoes = new ItemStack(Material.FLOWER_POT);
        final ItemMeta metaDecoracoes = decoracoes.getItemMeta();
        metaDecoracoes.setDisplayName(ChatColor.YELLOW + "Decorações");
        decoracoes.setItemMeta(metaDecoracoes);
        inventory.setItem(11, decoracoes);

        final ItemStack armas = new ItemStack(Material.TNT);
        final ItemMeta metaArmas = armas.getItemMeta();
        metaArmas.setDisplayName(ChatColor.YELLOW + "Armamentos");
        armas.setItemMeta(metaArmas);
        inventory.setItem(12, armas);

        final ItemStack materiais = new ItemStack(Material.NETHER_WART);
        final ItemMeta metaMateriais = materiais.getItemMeta();
        metaMateriais.setDisplayName(ChatColor.YELLOW + "Materiais");
        materiais.setItemMeta(metaMateriais);
        inventory.setItem(13, materiais);

        final ItemStack comidas = new ItemStack(Material.BREAD);
        final ItemMeta metaComidas = comidas.getItemMeta();
        metaComidas.setDisplayName(ChatColor.YELLOW + "Comidas");
        comidas.setItemMeta(metaComidas);
        inventory.setItem(14, comidas);

        final ItemStack redstone = new ItemStack(Material.REDSTONE);
        final ItemMeta metaRedstone = redstone.getItemMeta();
        metaRedstone.setDisplayName(ChatColor.YELLOW + "Redstone");
        redstone.setItemMeta(metaRedstone);
        inventory.setItem(15, redstone);

        final ItemStack alquimia = new ItemStack(Material.BREWING_STAND);
        final ItemMeta metaAlquimia = alquimia.getItemMeta();
        metaAlquimia.setDisplayName(ChatColor.YELLOW + "Alquimia");
        alquimia.setItemMeta(metaAlquimia);
        inventory.setItem(16, alquimia);

        final ItemStack coloridos = new ItemStack(Material.CYAN_DYE);
        final ItemMeta metaColoridos = coloridos.getItemMeta();
        metaColoridos.setDisplayName(ChatColor.YELLOW + "Coloridos");
        coloridos.setItemMeta(metaColoridos);
        inventory.setItem(20, coloridos);

        final ItemStack elytra = new ItemStack(Material.TRIDENT);
        final ItemMeta metaElytra = elytra.getItemMeta();
        metaElytra.setDisplayName(ChatColor.YELLOW + "Tridentes");
        elytra.setItemMeta(metaElytra);
        inventory.setItem(21, elytra);

        final ItemStack spawners = new ItemStack(Material.SPAWNER);
        final ItemMeta metaSpawners = spawners.getItemMeta();
        metaSpawners.setDisplayName(ChatColor.YELLOW + "Mobspawners");
        spawners.setItemMeta(metaSpawners);
        inventory.setItem(22, spawners);

        final ItemStack caixas = new ItemStack(Material.ENDER_CHEST);
        final ItemMeta metaCaixas = caixas.getItemMeta();
        metaCaixas.setDisplayName(ChatColor.YELLOW + "Caixas");
        caixas.setItemMeta(metaCaixas);
        inventory.setItem(23, caixas);

        final ItemStack painel = new ItemStack(Material.DAYLIGHT_DETECTOR);
        final ItemMeta metaPainel = painel.getItemMeta();
        metaPainel.setDisplayName(ChatColor.YELLOW + "Paineis solares");
        painel.setItemMeta(metaPainel);
        inventory.setItem(24, painel);

        final ItemStack sair = new ItemStack(Material.BARRIER);
        final ItemMeta metaSair = sair.getItemMeta();
        metaSair.setDisplayName(ChatColor.RED + "Sair");
        sair.setItemMeta(metaSair);
        inventory.setItem(31, sair);

        return inventory;
    }
}
