package com.mixedup.managers;

import com.mixedup.Main;
import com.mixedup.apis.ConfigAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.UUID;

public class InventoryConfig {

    public static Inventory buildInvConfig(final String uuid) {

        final Inventory INVENTORY_CONFIGS = Bukkit.createInventory(null, 5 * 9, "Configurações: ");

        final ItemStack depositosOn = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta metaDepositosOn = depositosOn.getItemMeta();
        metaDepositosOn.setDisplayName(ChatColor.GREEN + " * Depósitos");
        final ArrayList<String> lore11 = new ArrayList<String>();
        lore11.add(" ");
        lore11.add(ChatColor.DARK_GRAY + " * Habilitado!");
        lore11.add(" ");
        metaDepositosOn.setLore(lore11);
        depositosOn.setItemMeta(metaDepositosOn);

        final ItemStack depositosOff = new ItemStack(Material.BARRIER);
        final ItemMeta metaDepositosOff = depositosOff.getItemMeta();
        metaDepositosOff.setDisplayName(ChatColor.RED + " * Depósitos");
        final ArrayList<String> lore10 = new ArrayList<String>();
        lore10.add(" ");
        lore10.add(ChatColor.DARK_GRAY + " * Desabilitado!");
        lore10.add(" ");
        metaDepositosOff.setLore(lore10);
        depositosOff.setItemMeta(metaDepositosOff);

        if (ConfigAPI.getDepositosStatus(uuid) == true) {
            INVENTORY_CONFIGS.setItem(31, depositosOn);
        } else {
            INVENTORY_CONFIGS.setItem(31, depositosOff);
        }

        final ItemStack tpaOn = new ItemStack(Material.FEATHER);
        final ItemMeta metaTpaOn = tpaOn.getItemMeta();
        metaTpaOn.setDisplayName(ChatColor.GREEN + " * Tpa");
        final ArrayList<String> lore9 = new ArrayList<String>();
        lore9.add(" ");
        lore9.add(ChatColor.DARK_GRAY + " * Habilitado!");
        lore9.add(" ");
        metaTpaOn.setLore(lore9);
        tpaOn.setItemMeta(metaTpaOn);

        final ItemStack tpaOff = new ItemStack(Material.MAGMA_CREAM);
        final ItemMeta metaTpaOff = tpaOff.getItemMeta();
        metaTpaOff.setDisplayName(ChatColor.RED + " * Tpa");
        final ArrayList<String> lore8 = new ArrayList<String>();
        lore8.add(" ");
        lore8.add(ChatColor.DARK_GRAY + " * Desabilitado!");
        lore8.add(" ");
        metaTpaOff.setLore(lore8);
        tpaOff.setItemMeta(metaTpaOff);

        if (ConfigAPI.getTpaStatus(uuid) == true) {
            INVENTORY_CONFIGS.setItem(29, tpaOn);
        } else {
            INVENTORY_CONFIGS.setItem(29, tpaOff);
        }

        final ItemStack voarOn = new ItemStack(Material.FEATHER);
        final ItemMeta metaVoarOn = voarOn.getItemMeta();
        metaVoarOn.setDisplayName(ChatColor.GREEN + " * Voar " + ChatColor.GOLD + "[VIP]");
        final ArrayList<String> lore7 = new ArrayList<String>();
        lore7.add(" ");
        lore7.add(ChatColor.DARK_GRAY + " * Habilitado!");
        lore7.add(" ");
        metaVoarOn.setLore(lore7);
        voarOn.setItemMeta(metaVoarOn);

        final ItemStack voarOff = new ItemStack(Material.LEATHER_BOOTS);
        final ItemMeta metaVoarOff = voarOff.getItemMeta();
        metaVoarOff.setDisplayName(ChatColor.RED + " * Voar " + ChatColor.GOLD + "[VIP]");
        final ArrayList<String> lore6 = new ArrayList<String>();
        lore6.add(" ");
        lore6.add(ChatColor.DARK_GRAY + " * Desabilitado!");
        lore6.add(" ");
        metaVoarOff.setLore(lore6);
        voarOff.setItemMeta(metaVoarOff);

        if (ConfigAPI.getFlyStatus(uuid) == true) {
            INVENTORY_CONFIGS.setItem(16, voarOn);
        } else {
            INVENTORY_CONFIGS.setItem(16, voarOff);
        }

        final ItemStack tellOn = new ItemStack(Material.SIGN);
        final ItemMeta metaTellOn = tellOn.getItemMeta();
        metaTellOn.setDisplayName(ChatColor.GREEN + " * Mensagens privadas");
        final ArrayList<String> lore5 = new ArrayList<String>();
        lore5.add(" ");
        lore5.add(ChatColor.DARK_GRAY + " * Habilitado!");
        lore5.add(" ");
        metaTellOn.setLore(lore5);
        tellOn.setItemMeta(metaTellOn);

        final ItemStack tellOff = new ItemStack(Material.BARRIER);
        final ItemMeta metaTellOff = tellOff.getItemMeta();
        metaTellOff.setDisplayName(ChatColor.RED + " * Mensagens privadas");
        final ArrayList<String> lore4 = new ArrayList<String>();
        lore4.add(" ");
        lore4.add(ChatColor.DARK_GRAY + " * Desabilitado!");
        lore4.add(" ");
        metaTellOff.setLore(lore4);
        tellOff.setItemMeta(metaTellOff);

        if (ConfigAPI.getTellStatus(uuid) == true) {
            INVENTORY_CONFIGS.setItem(14, tellOn);
        } else {
            INVENTORY_CONFIGS.setItem(14, tellOff);
        }

        final ItemStack chatGlobalOff = new ItemStack(Material.WRITTEN_BOOK);
        final ItemMeta metaChatGlobalOff = chatGlobalOff.getItemMeta();
        metaChatGlobalOff.setDisplayName(ChatColor.RED + " * Chat global");
        final ArrayList<String> lore3 = new ArrayList<String>();
        lore3.add(" ");
        lore3.add(ChatColor.DARK_GRAY + " * Desabilitado!");
        lore3.add(" ");
        metaChatGlobalOff.setLore(lore3);
        chatGlobalOff.setItemMeta(metaChatGlobalOff);

        final ItemStack chatGlobalOn = new ItemStack(Material.BOOK);
        final ItemMeta metaChatGlobalOn = chatGlobalOn.getItemMeta();
        metaChatGlobalOn.setDisplayName(ChatColor.GREEN + " * Chat global");
        final ArrayList<String> lore2 = new ArrayList<String>();
        lore2.add(" ");
        lore2.add(ChatColor.DARK_GRAY + " * Habilitado!");
        lore2.add(" ");
        metaChatGlobalOn.setLore(lore2);
        chatGlobalOn.setItemMeta(metaChatGlobalOn);

        if (ConfigAPI.getChatGlobalStatus(uuid) == true) {
            INVENTORY_CONFIGS.setItem(12, chatGlobalOn);
        } else {
            INVENTORY_CONFIGS.setItem(12, chatGlobalOff);
        }

        final ItemStack chatLocalOff = new ItemStack(Material.MAP);
        final ItemMeta metaChatLocalOff = chatLocalOff.getItemMeta();
        metaChatLocalOff.setDisplayName(ChatColor.RED + " * Chat local");
        final ArrayList<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.DARK_GRAY + " * Desabilitado!");
        lore1.add(" ");
        metaChatLocalOff.setLore(lore1);
        chatLocalOff.setItemMeta(metaChatLocalOff);

        final ItemStack chatLocalOn = new ItemStack(Material.PAPER);
        final ItemMeta metaChatLocalOn = chatLocalOn.getItemMeta();
        metaChatLocalOn.setDisplayName(ChatColor.GREEN + " * Chat local");
        final ArrayList<String> lore = new ArrayList<String>();
        lore.add(" ");
        lore.add(ChatColor.DARK_GRAY + " * Habilitado!");
        lore.add(" ");
        metaChatLocalOn.setLore(lore);
        chatLocalOn.setItemMeta(metaChatLocalOn);

        if (ConfigAPI.getChatLocalStatus(uuid) == true) {
            INVENTORY_CONFIGS.setItem(10, chatLocalOn);
        } else {
            INVENTORY_CONFIGS.setItem(10, chatLocalOff);
        }

        final ItemStack scoreboardOff = new ItemStack(Material.PAINTING);
        final ItemMeta metaScoreboardOff = scoreboardOff.getItemMeta();
        metaScoreboardOff.setDisplayName(ChatColor.RED + " * Scoreboard");
        final ArrayList<String> loreSb = new ArrayList<String>();
        loreSb.add(" ");
        loreSb.add(ChatColor.DARK_GRAY + " * Desabilitado!");
        loreSb.add(" ");
        metaScoreboardOff.setLore(loreSb);
        scoreboardOff.setItemMeta(metaScoreboardOff);

        final ItemStack scoreabordOn = new ItemStack(Material.ITEM_FRAME);
        final ItemMeta metaScoreabordOn = scoreabordOn.getItemMeta();
        metaScoreabordOn.setDisplayName(ChatColor.GREEN + " * Scoreboard");
        final ArrayList<String> loreSbOn = new ArrayList<String>();
        loreSbOn.add(" ");
        loreSbOn.add(ChatColor.DARK_GRAY + " * Habilitado!");
        loreSbOn.add(" ");
        metaScoreabordOn.setLore(loreSbOn);
        scoreabordOn.setItemMeta(metaScoreabordOn);

        if (Main.scoreboardManager.getDefaultScoreboard().isScoreboardEnabled(Bukkit.getPlayer(UUID.fromString(uuid)))) {
            INVENTORY_CONFIGS.setItem(33, scoreabordOn);
        } else {
            INVENTORY_CONFIGS.setItem(33, scoreboardOff);
        }

        return INVENTORY_CONFIGS;
    }
}
