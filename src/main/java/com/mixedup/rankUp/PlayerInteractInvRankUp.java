package com.mixedup.rankUp;

import com.mixedup.apis.GuildaAPI;
import com.mixedup.apis.KDRApi;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.economy.CoinsAPI;
import org.bukkit.*;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.meta.FireworkMeta;

public class PlayerInteractInvRankUp implements Listener {

    @EventHandler
    public void onDamage(final EntityDamageEvent event) {
        if (event.getEntity().getWorld().getName().equalsIgnoreCase("guildaselect")) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInteract(final InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getClickedInventory() != null && event.getClickedInventory().getName().equalsIgnoreCase("Rank UP: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 13) {
                int moedas = 0;
                int kills = 0;
                if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("sanguinaria")) {
                    moedas = 10000 + (GuildaAPI.getGuildaRank(UUID) * 5000);
                    kills = GuildaAPI.getGuildaRank(UUID) * 600;

                    if (CoinsAPI.getBlackCoins(UUID) >= moedas && KDRApi.getKills(UUID) >= kills) {
                        GuildaAPI.updateRank(UUID);
                        CoinsAPI.updateBlackCoins(UUID, CoinsAPI.getBlackCoins(UUID) - moedas);
                        KDRApi.updateKills(UUID, KDRApi.getKills(UUID) - kills);
                        player.getOpenInventory().close();
                        player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                        player.sendMessage(ChatColor.GREEN + "\n * Novo nível, novas responsabilidades!\n ");
                        player.getWorld().strikeLightningEffect(player.getLocation());

                        final Firework fw = (Firework) player.getWorld().spawnEntity(player.getLocation(), EntityType.FIREWORK);
                        final FireworkMeta fwm = fw.getFireworkMeta();

                        fwm.setPower(3);
                        fwm.addEffect(FireworkEffect.builder().withColor(Color.ORANGE).flicker(true).build());

                        fw.setFireworkMeta(fwm);

                        fw.setPassenger(player);

                        for (final Player target : Bukkit.getOnlinePlayers()) {
                            target.sendTitle(ChatColor.RED + player.getName(), ChatColor.GRAY + "Acaba de upar de nível!", 20, 60, 20);
                            target.playSound(target.getLocation(), Sound.ENTITY_WITHER_SPAWN, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, você ainda não pode upar de nível!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("ancia")) {
                    moedas = 5000 + (GuildaAPI.getGuildaRank(UUID) * 5000);
                    kills = GuildaAPI.getGuildaRank(UUID) * 300;

                    if (CoinsAPI.getBlackCoins(UUID) >= moedas && KDRApi.getKills(UUID) >= kills) {
                        GuildaAPI.updateRank(UUID);
                        CoinsAPI.updateBlackCoins(UUID, CoinsAPI.getBlackCoins(UUID) - moedas);
                        KDRApi.updateKills(UUID, KDRApi.getKills(UUID) - kills);
                        player.getOpenInventory().close();
                        player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                        player.sendMessage(ChatColor.GREEN + "\n * Novo nível, novas responsabilidades!\n ");
                        player.getWorld().strikeLightningEffect(player.getLocation());

                        final Firework fw = (Firework) player.getWorld().spawnEntity(player.getLocation(), EntityType.FIREWORK);
                        final FireworkMeta fwm = fw.getFireworkMeta();

                        fwm.setPower(3);
                        fwm.addEffect(FireworkEffect.builder().withColor(Color.ORANGE).flicker(true).build());

                        fw.setFireworkMeta(fwm);

                        fw.setPassenger(player);

                        for (final Player target : Bukkit.getOnlinePlayers()) {
                            target.sendTitle(ChatColor.RED + player.getName(), ChatColor.GRAY + "Acaba de upar de nível!", 20, 60, 20);
                            target.playSound(target.getLocation(), Sound.ENTITY_WITHER_SPAWN, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, você ainda não pode upar de nível!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("nobre")) {
                    moedas = GuildaAPI.getGuildaRank(UUID) * 5000;
                    kills = GuildaAPI.getGuildaRank(UUID) * 150;

                    if (CoinsAPI.getBlackCoins(UUID) >= moedas && KDRApi.getKills(UUID) >= kills) {
                        GuildaAPI.updateRank(UUID);
                        CoinsAPI.updateBlackCoins(UUID, CoinsAPI.getBlackCoins(UUID) - moedas);
                        KDRApi.updateKills(UUID, KDRApi.getKills(UUID) - kills);
                        player.getOpenInventory().close();
                        player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                        player.sendMessage(ChatColor.GREEN + "\n * Novo nível, novas responsabilidades!\n ");
                        player.getWorld().strikeLightningEffect(player.getLocation());

                        final Firework fw = (Firework) player.getWorld().spawnEntity(player.getLocation(), EntityType.FIREWORK);
                        final FireworkMeta fwm = fw.getFireworkMeta();

                        fwm.setPower(3);
                        fwm.addEffect(FireworkEffect.builder().withColor(Color.ORANGE).flicker(true).build());

                        fw.setFireworkMeta(fwm);

                        fw.setPassenger(player);

                        for (final Player target : Bukkit.getOnlinePlayers()) {
                            target.sendTitle(ChatColor.RED + player.getName(), ChatColor.GRAY + "Acaba de upar de nível!", 20, 60, 20);
                            target.playSound(target.getLocation(), Sound.ENTITY_WITHER_SPAWN, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, você ainda não pode upar de nível!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                }
            }
        }
    }
}
