package com.mixedup.rankUp;

import com.mixedup.apis.GuildaAPI;
import com.mixedup.apis.KDRApi;
import com.mixedup.economy.CoinsAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class InventoryRankUp {

    public static Inventory getInventory(final String UUID) {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Rank UP: ");

        final DecimalFormat formatter = new DecimalFormat("#,###.00");
        int moedas = 0;
        int kills = 0;
        if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("sanguinaria")) {
            moedas = 10000 + (GuildaAPI.getGuildaRank(UUID) * 5000);
            kills = GuildaAPI.getGuildaRank(UUID) * 600;
        } else if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("ancia")) {
            moedas = 5000 + (GuildaAPI.getGuildaRank(UUID) * 5000);
            kills = GuildaAPI.getGuildaRank(UUID) * 300;
        } else if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("nobre")) {
            moedas = GuildaAPI.getGuildaRank(UUID) * 5000;
            kills = GuildaAPI.getGuildaRank(UUID) * 150;
        }

        final ItemStack nivel1 = new ItemStack(Material.STONE_SWORD);
        final ItemMeta metaNivel1 = nivel1.getItemMeta();
        if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("sanguinaria")) {
            if (GuildaAPI.getGuildaRank(UUID) == 1) {
                metaNivel1.setDisplayName(ChatColor.RED + " * Sanguinária (Nível " + (GuildaAPI.getGuildaRank(UUID) + 1) + ")");
            } else if (GuildaAPI.getGuildaRank(UUID) == 2) {
                metaNivel1.setDisplayName(ChatColor.GOLD + " * Sanguinária (Nível " + (GuildaAPI.getGuildaRank(UUID) + 1) + ")");
            } else if (GuildaAPI.getGuildaRank(UUID) == 3) {
                metaNivel1.setDisplayName(ChatColor.YELLOW + " * Sanguinária (Nível " + (GuildaAPI.getGuildaRank(UUID) + 1) + ")");
            } else if (GuildaAPI.getGuildaRank(UUID) == 4) {
                metaNivel1.setDisplayName(ChatColor.GREEN + " * Sanguinária (Nível " + (GuildaAPI.getGuildaRank(UUID) + 1) + ")");
            }
        } else if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("ancia")) {
            if (GuildaAPI.getGuildaRank(UUID) == 1) {
                metaNivel1.setDisplayName(ChatColor.RED + " * Anciã (Nível " + (GuildaAPI.getGuildaRank(UUID) + 1) + ")");
            } else if (GuildaAPI.getGuildaRank(UUID) == 2) {
                metaNivel1.setDisplayName(ChatColor.GOLD + " * Anciã (Nível " + (GuildaAPI.getGuildaRank(UUID) + 1) + ")");
            } else if (GuildaAPI.getGuildaRank(UUID) == 3) {
                metaNivel1.setDisplayName(ChatColor.YELLOW + " * Anciã (Nível " + (GuildaAPI.getGuildaRank(UUID) + 1) + ")");
            } else if (GuildaAPI.getGuildaRank(UUID) == 4) {
                metaNivel1.setDisplayName(ChatColor.GREEN + " * Anciã (Nível " + (GuildaAPI.getGuildaRank(UUID) + 1) + ")");
            }
        } else if (GuildaAPI.getGuilda(UUID).equalsIgnoreCase("nobre")) {
            if (GuildaAPI.getGuildaRank(UUID) == 1) {
                metaNivel1.setDisplayName(ChatColor.RED + " * Nobre (Nível " + (GuildaAPI.getGuildaRank(UUID) + 1) + ")");
            } else if (GuildaAPI.getGuildaRank(UUID) == 2) {
                metaNivel1.setDisplayName(ChatColor.GOLD + " * Nobre (Nível " + (GuildaAPI.getGuildaRank(UUID) + 1) + ")");
            } else if (GuildaAPI.getGuildaRank(UUID) == 3) {
                metaNivel1.setDisplayName(ChatColor.YELLOW + " * Nobre (Nível " + (GuildaAPI.getGuildaRank(UUID) + 1) + ")");
            } else if (GuildaAPI.getGuildaRank(UUID) == 4) {
                metaNivel1.setDisplayName(ChatColor.GREEN + " * Nobre (Nível " + (GuildaAPI.getGuildaRank(UUID) + 1) + ")");
            }
        }
        final ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.WHITE + "  Requisitos: ");
        if (CoinsAPI.getBlackCoins(UUID) >= moedas) {
            lore.add(ChatColor.GREEN + "  " + formatter.format(CoinsAPI.getBlackCoins(UUID)) + ChatColor.GREEN + "/" + formatter.format(moedas) + ChatColor.GRAY + " moedas negras");
        } else {
            lore.add(ChatColor.RED + "  " + formatter.format(CoinsAPI.getBlackCoins(UUID)) + ChatColor.YELLOW + "/" + formatter.format(moedas) + ChatColor.GRAY + " moedas negras");
        }
        if (KDRApi.getKills(UUID) >= kills) {
            lore.add(ChatColor.GREEN + "  " + KDRApi.getKills(UUID) + ChatColor.GREEN + "/" + kills + ChatColor.GRAY + " vítimas");
        } else {
            lore.add(ChatColor.RED + "  " + KDRApi.getKills(UUID) + ChatColor.YELLOW + "/" + kills + ChatColor.GRAY + " vítimas");
        }
        lore.add(" ");
        lore.add(" ");
        lore.add(ChatColor.WHITE + "  Vantagens: ");
        lore.add(ChatColor.GRAY + "  Aumento de " + (5 + (GuildaAPI.getGuildaRank(UUID) * 1)) + "% para " + (5 + (GuildaAPI.getGuildaRank(UUID) * 1) + 1) + "% de dropar moedas negras.");
        lore.add(ChatColor.GRAY + "  Acesso ao kit nível " + (GuildaAPI.getGuildaRank(UUID) + 1) + ".");
        lore.add(" ");
        metaNivel1.setLore(lore);
        nivel1.setItemMeta(metaNivel1);

        inventory.setItem(13, nivel1);

        return inventory;
    }
}
