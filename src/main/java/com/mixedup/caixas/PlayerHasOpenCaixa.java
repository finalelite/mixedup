package com.mixedup.caixas;

import java.util.HashMap;

public class PlayerHasOpenCaixa {

    private static final HashMap<String, PlayerHasOpenCaixa> CACHE = new HashMap<String, PlayerHasOpenCaixa>();
    private final String UUID;
    private boolean status;

    public PlayerHasOpenCaixa(final String UUID, final boolean status) {
        this.UUID = UUID;
        this.status = status;
    }

    public static PlayerHasOpenCaixa get(final String UUID) {
        return PlayerHasOpenCaixa.CACHE.get(String.valueOf(UUID));
    }

    public PlayerHasOpenCaixa insert() {
        PlayerHasOpenCaixa.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(final boolean status) {
        this.status = status;
    }

    public String getUUID() {
        return UUID;
    }
}
