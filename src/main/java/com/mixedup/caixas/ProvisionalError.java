package com.mixedup.caixas;

import java.util.HashMap;

public class ProvisionalError {

    private static final HashMap<String, ProvisionalError> CACHE = new HashMap<String, ProvisionalError>();
    private final String UUID;
    private boolean status;

    public ProvisionalError(final String UUID, final boolean status) {
        this.UUID = UUID;
        this.status = status;
    }

    public static ProvisionalError get(final String UUID) {
        return ProvisionalError.CACHE.get(String.valueOf(UUID));
    }

    public ProvisionalError insert() {
        ProvisionalError.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(final boolean status) {
        this.status = status;
    }

    public String getUUID() {
        return UUID;
    }
}
