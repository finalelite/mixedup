package com.mixedup.caixas;

import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class ItemGanhoUtil {

    private static final HashMap<String, ItemGanhoUtil> CACHE = new HashMap<String, ItemGanhoUtil>();
    private final String UUID;
    private int itemID;
    private ItemStack item;

    public ItemGanhoUtil(final String UUID, final int itemID, final ItemStack item) {
        this.UUID = UUID;
        this.itemID = itemID;
        this.item = item;
    }

    public static ItemGanhoUtil get(final String UUID) {
        return ItemGanhoUtil.CACHE.get(String.valueOf(UUID));
    }

    public ItemGanhoUtil insert() {
        ItemGanhoUtil.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public int getItemID() {
        return itemID;
    }

    public void setItemID(final int itemID) {
        this.itemID = itemID;
    }

    public ItemStack getItem() {
        return item;
    }

    public void setItem(final ItemStack item) {
        this.item = item;
    }

    public String getUUID() {
        return UUID;
    }
}
