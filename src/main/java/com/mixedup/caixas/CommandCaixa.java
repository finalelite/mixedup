package com.mixedup.caixas;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;
import java.util.Map.Entry;

public class CommandCaixa implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        //caixaall (type) (quantia)
        if (cmd.getName().equalsIgnoreCase("caixaall")) {
            final String tag = TagAPI.getTag(UUID);

            if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Supervisor")) {
                if (args.length == 2) {
                    final String[] caixas = {"Básica", "Avançada", "Suprema", "Spawners", "Chave"};

                    if (Arrays.asList(caixas).contains(args[0])) {
                        try {
                            final int quantia = Integer.valueOf(args[1]);

                            if (args[0].equalsIgnoreCase("Chave")) {
                                final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, quantia);
                                final ItemMeta metaChave = chave.getItemMeta();
                                metaChave.setDisplayName(ChatColor.GREEN + "Chave");
                                final List<String> lore = new ArrayList<>();
                                lore.add(" ");
                                lore.add(ChatColor.GRAY + "Utilizado para abrir caixas");
                                lore.add(ChatColor.YELLOW + "50% sucesso");
                                lore.add(" ");
                                metaChave.setLore(lore);
                                chave.setItemMeta(metaChave);

                                for (final Player target : Bukkit.getOnlinePlayers()) {
                                    if (target.getName().equalsIgnoreCase(player.getName())) continue;
                                    final HashMap<Integer, ItemStack> nope = target.getInventory().addItem(chave);
                                    for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                        target.getWorld().dropItemNaturally(target.getLocation(), entry.getValue());
                                    }
                                    target.sendMessage(ChatColor.GREEN + " * Foram cedidos " + quantia + " chave(s) a você.\n  " + ChatColor.RED + "Por: " + ChatColor.GRAY + player.getName());
                                    target.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    target.playSound(player.getLocation(), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                }
                                return false;
                            }

                            if (args[0].equalsIgnoreCase("básica")) {
                                final ItemStack caixa = new ItemStack(Material.CHEST, quantia);
                                final ItemMeta metaCaixa = caixa.getItemMeta();
                                metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
                                final List<String> lore = new ArrayList<String>();
                                lore.add(" ");
                                lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Básica");
                                lore.add(" ");
                                metaCaixa.setLore(lore);
                                caixa.setItemMeta(metaCaixa);

                                for (final Player target : Bukkit.getOnlinePlayers()) {
                                    if (target.getName().equalsIgnoreCase(player.getName())) continue;
                                    final HashMap<Integer, ItemStack> nope = target.getInventory().addItem(caixa);
                                    for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                        target.getWorld().dropItemNaturally(target.getLocation(), entry.getValue());
                                    }
                                    target.sendMessage(ChatColor.GREEN + " * Foram cedidos " + quantia + " caixa(s) básica a você.\n  " + ChatColor.RED + "Por: " + ChatColor.GRAY + player.getName());
                                    target.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    target.playSound(player.getLocation(), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                }
                            } else if (args[0].equalsIgnoreCase("spawners")) {
                                final ItemStack caixa = new ItemStack(Material.SPAWNER, quantia);
                                final ItemMeta metaCaixa = caixa.getItemMeta();
                                metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
                                final List<String> lore = new ArrayList<String>();
                                lore.add(" ");
                                lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Spawners");
                                lore.add(" ");
                                metaCaixa.setLore(lore);
                                caixa.setItemMeta(metaCaixa);

                                for (final Player target : Bukkit.getOnlinePlayers()) {
                                    if (target.getName().equalsIgnoreCase(player.getName())) continue;
                                    final HashMap<Integer, ItemStack> nope = target.getInventory().addItem(caixa);
                                    for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                        target.getWorld().dropItemNaturally(target.getLocation(), entry.getValue());
                                    }
                                    target.sendMessage(ChatColor.GREEN + " * Foram cedidos " + quantia + " caixa(s) spawners a você.\n  " + ChatColor.RED + "Por: " + ChatColor.GRAY + player.getName());
                                    target.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    target.playSound(player.getLocation(), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                }
                            } else if (args[0].equalsIgnoreCase("suprema")) {
                                final ItemStack caixa = new ItemStack(Material.END_PORTAL_FRAME, quantia);
                                final ItemMeta metaCaixa = caixa.getItemMeta();
                                metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
                                final List<String> lore = new ArrayList<String>();
                                lore.add(" ");
                                lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Suprema");
                                lore.add(" ");
                                metaCaixa.setLore(lore);
                                caixa.setItemMeta(metaCaixa);

                                for (final Player target : Bukkit.getOnlinePlayers()) {
                                    if (target.getName().equalsIgnoreCase(player.getName())) continue;
                                    final HashMap<Integer, ItemStack> nope = target.getInventory().addItem(caixa);
                                    for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                        target.getWorld().dropItemNaturally(target.getLocation(), entry.getValue());
                                    }
                                    target.sendMessage(ChatColor.GREEN + " * Foram cedidos " + quantia + " caixa(s) suprema a você.\n  " + ChatColor.RED + "Por: " + ChatColor.GRAY + player.getName());
                                    target.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    target.playSound(player.getLocation(), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                }
                            } else {
                                final ItemStack caixa = new ItemStack(Material.ENDER_CHEST, quantia);
                                final ItemMeta metaCaixa = caixa.getItemMeta();
                                metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
                                final List<String> lore = new ArrayList<String>();
                                lore.add(" ");
                                lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Avançada");
                                lore.add(" ");
                                metaCaixa.setLore(lore);
                                caixa.setItemMeta(metaCaixa);

                                for (final Player target : Bukkit.getOnlinePlayers()) {
                                    if (target.getName().equalsIgnoreCase(player.getName())) continue;
                                    final HashMap<Integer, ItemStack> nope = target.getInventory().addItem(caixa);
                                    for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                        target.getWorld().dropItemNaturally(target.getLocation(), entry.getValue());
                                    }
                                    target.sendMessage(ChatColor.GREEN + " * Foram cedidos " + quantia + " caixa(s) avançada a você.\n  " + ChatColor.RED + "Por: " + ChatColor.GRAY + player.getName());
                                    target.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    target.playSound(player.getLocation(), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                                }
                            }
                        } catch (final NumberFormatException e) {
                            player.sendMessage(ChatColor.RED + "Ops, a quantidade informada está incorreta.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, a caixa informada não existe!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/caixaall (nome-da-caixa) (quantia)");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }

        if (cmd.getName().equalsIgnoreCase("caixamisteriosa")) {
            final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Caixas misteriosas: ");

            final ItemStack basica = new ItemStack(Material.CHEST);
            final ItemMeta metaBasica = basica.getItemMeta();
            metaBasica.setDisplayName(ChatColor.GOLD + "Caixa misteriosa");
            final ArrayList<String> loreB = new ArrayList<>();
            loreB.add(" ");
            loreB.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Básica");
            loreB.add(" ");
            metaBasica.setLore(loreB);
            basica.setItemMeta(metaBasica);

            final ItemStack avançada = new ItemStack(Material.ENDER_CHEST);
            final ItemMeta metaAvançada = avançada.getItemMeta();
            metaAvançada.setDisplayName(ChatColor.GOLD + "Caixa misteriosa");
            final ArrayList<String> loreA = new ArrayList<>();
            loreA.add(" ");
            loreA.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Avançada");
            loreA.add(" ");
            metaAvançada.setLore(loreA);
            avançada.setItemMeta(metaAvançada);

            final ItemStack suprema = new ItemStack(Material.END_PORTAL_FRAME);
            final ItemMeta metaSuprema = suprema.getItemMeta();
            metaSuprema.setDisplayName(ChatColor.GOLD + "Caixa misteriosa");
            final ArrayList<String> loreS = new ArrayList<>();
            loreS.add(" ");
            loreS.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Suprema");
            loreS.add(" ");
            metaSuprema.setLore(loreS);
            suprema.setItemMeta(metaSuprema);

            final ItemStack spawners = new ItemStack(Material.SPAWNER);
            final ItemMeta metaSpawners = spawners.getItemMeta();
            metaSpawners.setDisplayName(ChatColor.GOLD + "Caixa misteriosa");
            final ArrayList<String> loreSp = new ArrayList<>();
            loreSp.add(" ");
            loreSp.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Spawners");
            loreSp.add(" ");
            metaSpawners.setLore(loreSp);
            spawners.setItemMeta(metaSpawners);

            final ItemStack caixa = new ItemStack(Material.BARRIER);
            final ItemMeta metaCaixa = caixa.getItemMeta();
            metaCaixa.setDisplayName(ChatColor.RED + "Caixa de evento");
            final ArrayList<String> lore = new ArrayList<>();
            lore.add(ChatColor.GRAY + "Nenhum evento está ocorrendo");
            lore.add(ChatColor.GRAY + "no momento.");
            metaCaixa.setLore(lore);
            caixa.setItemMeta(metaCaixa);

            final ItemStack breve = new ItemStack(Material.CONDUIT);
            final ItemMeta metaBreve = caixa.getItemMeta();
            metaBreve.setDisplayName(ChatColor.RED + "Em breve");
            breve.setItemMeta(metaBreve);

            inventory.setItem(10, basica);
            inventory.setItem(11, avançada);
            inventory.setItem(12, suprema);
            inventory.setItem(13, spawners);
            inventory.setItem(14, breve);
            inventory.setItem(16, caixa);

            player.openInventory(inventory);
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }

        if (cmd.getName().equalsIgnoreCase("caixa")) {
            if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                if (args.length == 3) {
                    if (args[0].equals("ceder")) {
                        final String[] caixas = {"Básica", "Avançada", "Suprema", "Spawners", "Chave"};
                        for (int i = 1; i <= caixas.length; i++) {
                            if (args[1].equalsIgnoreCase(caixas[i - 1])) {
                                try {
                                    final int quantia = Integer.valueOf(args[2]);

                                    if (args[1].equalsIgnoreCase("Chave")) {
                                        final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, quantia);
                                        final ItemMeta metaChave = chave.getItemMeta();
                                        metaChave.setDisplayName(ChatColor.GREEN + "Chave");
                                        final List<String> lore = new ArrayList<>();
                                        lore.add(" ");
                                        lore.add(ChatColor.GRAY + "Utilizado para abrir caixas");
                                        lore.add(ChatColor.YELLOW + "50% sucesso");
                                        lore.add(" ");
                                        metaChave.setLore(lore);
                                        chave.setItemMeta(metaChave);

                                        final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(chave);
                                        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                                        }
                                        return false;
                                    }

                                    if (args[1].equalsIgnoreCase("básica")) {
                                        final ItemStack caixa = new ItemStack(Material.CHEST, quantia);
                                        final ItemMeta metaCaixa = caixa.getItemMeta();
                                        metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
                                        final List<String> lore = new ArrayList<String>();
                                        lore.add(" ");
                                        lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + caixas[i - 1]);
                                        lore.add(" ");
                                        metaCaixa.setLore(lore);
                                        caixa.setItemMeta(metaCaixa);

                                        final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(caixa);
                                        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                                        }
                                    } else if (args[1].equalsIgnoreCase("spawners")) {
                                        final ItemStack caixa = new ItemStack(Material.SPAWNER, quantia);
                                        final ItemMeta metaCaixa = caixa.getItemMeta();
                                        metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
                                        final List<String> lore = new ArrayList<String>();
                                        lore.add(" ");
                                        lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + caixas[i - 1]);
                                        lore.add(" ");
                                        metaCaixa.setLore(lore);
                                        caixa.setItemMeta(metaCaixa);

                                        final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(caixa);
                                        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                                        }
                                    } else if (args[1].equalsIgnoreCase("suprema")) {
                                        final ItemStack caixa = new ItemStack(Material.END_PORTAL_FRAME, quantia);
                                        final ItemMeta metaCaixa = caixa.getItemMeta();
                                        metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
                                        final List<String> lore = new ArrayList<String>();
                                        lore.add(" ");
                                        lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + caixas[i - 1]);
                                        lore.add(" ");
                                        metaCaixa.setLore(lore);
                                        caixa.setItemMeta(metaCaixa);

                                        final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(caixa);
                                        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                                        }
                                    } else {
                                        final ItemStack caixa = new ItemStack(Material.ENDER_CHEST, quantia);
                                        final ItemMeta metaCaixa = caixa.getItemMeta();
                                        metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
                                        final List<String> lore = new ArrayList<String>();
                                        lore.add(" ");
                                        lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + caixas[i - 1]);
                                        lore.add(" ");
                                        metaCaixa.setLore(lore);
                                        caixa.setItemMeta(metaCaixa);

                                        final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(caixa);
                                        for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                            player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                                        }
                                    }

                                    player.sendMessage(ChatColor.GREEN + " * Foram cedidos " + quantia + " Caixas misteriosas " + caixas[i - 1] + ".");
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    break;
                                } catch (final Exception e) {
                                    player.sendMessage(ChatColor.RED + "Ops, a quantidade informada está incorreta.");
                                    break;
                                }
                            } else if (i == caixas.length) {
                                player.sendMessage(ChatColor.RED + "Ops, a caixa informada não existe!");
                                break;
                            }
                        }
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/caixa ceder (nome-da-caixa) (quantia)");
                }
            }
        }
        return false;
    }
}
