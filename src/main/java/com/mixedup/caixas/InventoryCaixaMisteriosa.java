package com.mixedup.caixas;

import com.mixedup.Main;
import com.mixedup.utils.CriarItem;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkEffectMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class InventoryCaixaMisteriosa implements Listener {

    @EventHandler
    public void onClick(final InventoryClickEvent event) {
        if (event.getClickedInventory() != null) {
            final Player player = (Player) event.getWhoClicked();

            if (event.getClickedInventory().getName().equalsIgnoreCase("Caixa básica: ")) {
                event.setCancelled(true);

                if (event.getSlot() == 45) {
                    final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Caixas misteriosas: ");

                    final ItemStack basica = new ItemStack(Material.CHEST);
                    final ItemMeta metaBasica = basica.getItemMeta();
                    metaBasica.setDisplayName(ChatColor.GOLD + "Caixa misteriosa");
                    final ArrayList<String> loreB = new ArrayList<>();
                    loreB.add(" ");
                    loreB.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Básica");
                    loreB.add(" ");
                    metaBasica.setLore(loreB);
                    basica.setItemMeta(metaBasica);

                    final ItemStack avançada = new ItemStack(Material.ENDER_CHEST);
                    final ItemMeta metaAvançada = avançada.getItemMeta();
                    metaAvançada.setDisplayName(ChatColor.GOLD + "Caixa misteriosa");
                    final ArrayList<String> loreA = new ArrayList<>();
                    loreA.add(" ");
                    loreA.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Avançada");
                    loreA.add(" ");
                    metaAvançada.setLore(loreA);
                    avançada.setItemMeta(metaAvançada);

                    final ItemStack suprema = new ItemStack(Material.END_PORTAL_FRAME);
                    final ItemMeta metaSuprema = suprema.getItemMeta();
                    metaSuprema.setDisplayName(ChatColor.GOLD + "Caixa misteriosa");
                    final ArrayList<String> loreS = new ArrayList<>();
                    loreS.add(" ");
                    loreS.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Suprema");
                    loreS.add(" ");
                    metaSuprema.setLore(loreS);
                    suprema.setItemMeta(metaSuprema);

                    final ItemStack spawners = new ItemStack(Material.SPAWNER);
                    final ItemMeta metaSpawners = spawners.getItemMeta();
                    metaSpawners.setDisplayName(ChatColor.GOLD + "Caixa misteriosa");
                    final ArrayList<String> loreSp = new ArrayList<>();
                    loreSp.add(" ");
                    loreSp.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Spawners");
                    loreSp.add(" ");
                    metaSpawners.setLore(loreSp);
                    spawners.setItemMeta(metaSpawners);

                    final ItemStack caixa = new ItemStack(Material.BARRIER);
                    final ItemMeta metaCaixa = caixa.getItemMeta();
                    metaCaixa.setDisplayName(ChatColor.RED + "Caixa de evento");
                    final ArrayList<String> lore = new ArrayList<>();
                    lore.add(ChatColor.GRAY + "Nenhum evento está ocorrendo");
                    lore.add(ChatColor.GRAY + "no momento.");
                    metaCaixa.setLore(lore);
                    caixa.setItemMeta(metaCaixa);

                    inventory.setItem(10, basica);
                    inventory.setItem(11, avançada);
                    inventory.setItem(12, suprema);
                    inventory.setItem(13, spawners);
                    inventory.setItem(16, caixa);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(inventory);
                        }
                    }, 5L);
                }
            }

            if (event.getClickedInventory().getName().equalsIgnoreCase("Caixa avançada:  ")) {
                event.setCancelled(true);

                if (event.getSlot() == 45) {
                    final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Caixa avançada: ");

                    inventory.setItem(10, CriarItem.add(Material.DIAMOND_SWORD, "Espada de diamante", Enchantment.FIRE_ASPECT, 2, Enchantment.DURABILITY, 3, Enchantment.DAMAGE_ALL, 5, Enchantment.LOOT_BONUS_MOBS, 3));//1
                    inventory.setItem(11, CriarItem.add(Material.DIAMOND_HELMET, "Capacete de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 4, Enchantment.DURABILITY, 3));//1
                    inventory.setItem(12, CriarItem.add(Material.DIAMOND_CHESTPLATE, "Peitoral de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 4, Enchantment.DURABILITY, 3));//1
                    inventory.setItem(13, CriarItem.add(Material.DIAMOND_LEGGINGS, "Calças de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 4, Enchantment.DURABILITY, 3));//1
                    inventory.setItem(14, CriarItem.add(Material.DIAMOND_BOOTS, "Botas de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 4, Enchantment.DURABILITY, 3));//1
                    inventory.setItem(15, CriarItem.add(Material.BOW, "Arco", Enchantment.DURABILITY, 3, Enchantment.ARROW_FIRE, 2, Enchantment.ARROW_INFINITE, 1));//3
                    inventory.setItem(16, CriarItem.add(Material.DIAMOND_PICKAXE, "Picareta de diamante", Enchantment.DURABILITY, 3, Enchantment.LOOT_BONUS_BLOCKS, 3, Enchantment.DIG_SPEED, 5));//1
                    inventory.setItem(19, CriarItem.add(Material.DIAMOND_SWORD, "Espada de diamante", Enchantment.DURABILITY, 3, Enchantment.DAMAGE_ALL, 5, Enchantment.FIRE_ASPECT, 2));//3]
                    inventory.setItem(20, CriarItem.add(Material.DIAMOND_HELMET, "Capacete de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 3, Enchantment.DURABILITY, 3));//3
                    inventory.setItem(21, CriarItem.add(Material.DIAMOND_CHESTPLATE, "Peitoral de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 3, Enchantment.DURABILITY, 3));//3
                    inventory.setItem(22, CriarItem.add(Material.DIAMOND_LEGGINGS, "Calças de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 3, Enchantment.DURABILITY, 3));//3
                    inventory.setItem(23, CriarItem.add(Material.DIAMOND_BOOTS, "Botas de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 3, Enchantment.DURABILITY, 3));//3
                    inventory.setItem(24, CriarItem.add(Material.BOW, "Arco", Enchantment.DURABILITY, 3, Enchantment.ARROW_FIRE, 2));//5
                    inventory.setItem(25, CriarItem.add(Material.DIAMOND_PICKAXE, "Picareta de diamante", Enchantment.DURABILITY, 2, Enchantment.SILK_TOUCH, 1, Enchantment.DIG_SPEED, 5));//3
                    inventory.setItem(29, CriarItem.add(Material.DIAMOND_AXE, "Machado de diamante", Enchantment.DURABILITY, 3, Enchantment.LOOT_BONUS_BLOCKS, 3, Enchantment.DIG_SPEED, 5));//2
                    inventory.setItem(28, CriarItem.add(Material.DIAMOND_AXE, "Machado de diamante", Enchantment.DIG_SPEED, 5, Enchantment.DURABILITY, 3));//4
                    inventory.setItem(30, CriarItem.add(Material.NETHER_STAR, ChatColor.GREEN + "Poder máximo", 1, ""));//2
                    inventory.setItem(31, CriarItem.add(Material.NETHER_STAR, ChatColor.GREEN + "Poder máximo", 2, ""));//1
                    inventory.setItem(32, CriarItem.add(Material.ENCHANTED_GOLDEN_APPLE, 16));//2
                    inventory.setItem(33, CriarItem.add(Material.CREEPER_SPAWN_EGG, 5));//2
                    inventory.setItem(34, CriarItem.add(Material.GHAST_SPAWN_EGG, 5));//2
                    inventory.setItem(37, CriarItem.add(Material.IRON_BLOCK, 15));//8
                    inventory.setItem(38, CriarItem.add(Material.GOLD_BLOCK, 15));//8
                    inventory.setItem(39, CriarItem.add(Material.DIAMOND_BLOCK, 15));//8
                    inventory.setItem(40, CriarItem.add(Material.LAPIS_BLOCK, 15));//8

                    final ItemStack caixaSup = new ItemStack(Material.END_PORTAL_FRAME);//2
                    final ItemMeta metaCaixaSup = caixaSup.getItemMeta();
                    metaCaixaSup.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
                    final List<String> loreSup = new ArrayList<String>();
                    loreSup.add(" ");
                    loreSup.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Suprema");
                    loreSup.add(" ");
                    metaCaixaSup.setLore(loreSup);
                    caixaSup.setItemMeta(metaCaixaSup);
                    inventory.setItem(41, caixaSup);

                    final ItemStack booster = new ItemStack(Material.BLAZE_POWDER);//4
                    final ItemMeta meta = booster.getItemMeta();
                    meta.setDisplayName(ChatColor.GREEN + "Booster mcmmo");
                    final ArrayList<String> lore = new ArrayList<>();
                    lore.add(" ");
                    lore.add(ChatColor.WHITE + "Tipo: " + ChatColor.GRAY + "Membro");
                    lore.add(ChatColor.WHITE + "Tempo: " + ChatColor.GRAY + "15 minutos");
                    lore.add(" ");
                    meta.setLore(lore);
                    booster.setItemMeta(meta);
                    inventory.setItem(42, booster);

                    final ItemStack playerDrop = new ItemStack(Material.FIREWORK_STAR, 1);//3
                    final ItemMeta meta100 = playerDrop.getItemMeta();
                    meta100.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                    meta100.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                    meta100.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                    meta100.addItemFlags(ItemFlag.HIDE_DESTROYS);
                    meta100.addItemFlags(ItemFlag.HIDE_PLACED_ON);
                    meta100.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                    meta100.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
                    final ArrayList<String> lore100 = new ArrayList<>();
                    lore100.add(" ");
                    lore100.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Força");
                    lore100.add(" ");
                    meta100.setLore(lore100);
                    final FireworkEffectMeta metaFw = (FireworkEffectMeta) meta100;
                    final FireworkEffect aa = FireworkEffect.builder().withColor(Color.RED).build();
                    metaFw.setEffect(aa);
                    playerDrop.setItemMeta(metaFw);
                    inventory.setItem(43, playerDrop);

                    final ItemStack voltar = new ItemStack(Material.ARROW);
                    final ItemMeta metaV = voltar.getItemMeta();
                    metaV.setDisplayName(ChatColor.RED + "Voltar");
                    voltar.setItemMeta(metaV);
                    inventory.setItem(45, voltar);

                    final ItemStack proxima = new ItemStack(Material.ARROW);
                    final ItemMeta metaP = proxima.getItemMeta();
                    metaP.setDisplayName(ChatColor.RED + "Próxima página");
                    proxima.setItemMeta(metaP);
                    inventory.setItem(53, proxima);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(inventory);
                        }
                    }, 5L);
                }
            }

            if (event.getClickedInventory().getName().equalsIgnoreCase("Caixa avançada: ")) {
                event.setCancelled(true);

                if (event.getSlot() == 45) {
                    final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Caixas misteriosas: ");

                    final ItemStack basica = new ItemStack(Material.CHEST);
                    final ItemMeta metaBasica = basica.getItemMeta();
                    metaBasica.setDisplayName(ChatColor.GOLD + "Caixa misteriosa");
                    final ArrayList<String> loreB = new ArrayList<>();
                    loreB.add(" ");
                    loreB.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Básica");
                    loreB.add(" ");
                    metaBasica.setLore(loreB);
                    basica.setItemMeta(metaBasica);

                    final ItemStack avançada = new ItemStack(Material.ENDER_CHEST);
                    final ItemMeta metaAvançada = avançada.getItemMeta();
                    metaAvançada.setDisplayName(ChatColor.GOLD + "Caixa misteriosa");
                    final ArrayList<String> loreA = new ArrayList<>();
                    loreA.add(" ");
                    loreA.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Avançada");
                    loreA.add(" ");
                    metaAvançada.setLore(loreA);
                    avançada.setItemMeta(metaAvançada);

                    final ItemStack suprema = new ItemStack(Material.END_PORTAL_FRAME);
                    final ItemMeta metaSuprema = suprema.getItemMeta();
                    metaSuprema.setDisplayName(ChatColor.GOLD + "Caixa misteriosa");
                    final ArrayList<String> loreS = new ArrayList<>();
                    loreS.add(" ");
                    loreS.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Suprema");
                    loreS.add(" ");
                    metaSuprema.setLore(loreS);
                    suprema.setItemMeta(metaSuprema);

                    final ItemStack spawners = new ItemStack(Material.SPAWNER);
                    final ItemMeta metaSpawners = spawners.getItemMeta();
                    metaSpawners.setDisplayName(ChatColor.GOLD + "Caixa misteriosa");
                    final ArrayList<String> loreSp = new ArrayList<>();
                    loreSp.add(" ");
                    loreSp.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Spawners");
                    loreSp.add(" ");
                    metaSpawners.setLore(loreSp);
                    spawners.setItemMeta(metaSpawners);

                    final ItemStack caixa = new ItemStack(Material.BARRIER);
                    final ItemMeta metaCaixa = caixa.getItemMeta();
                    metaCaixa.setDisplayName(ChatColor.RED + "Caixa de evento");
                    final ArrayList<String> lore = new ArrayList<>();
                    lore.add(ChatColor.GRAY + "Nenhum evento está ocorrendo");
                    lore.add(ChatColor.GRAY + "no momento.");
                    metaCaixa.setLore(lore);
                    caixa.setItemMeta(metaCaixa);

                    inventory.setItem(10, basica);
                    inventory.setItem(11, avançada);
                    inventory.setItem(12, suprema);
                    inventory.setItem(13, spawners);
                    inventory.setItem(16, caixa);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(inventory);
                        }
                    }, 5L);
                }

                if (event.getSlot() == 53) {
                    final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Caixa avançada:  ");

                    final ItemStack playerDrop1 = new ItemStack(Material.FIREWORK_STAR, 1);//3
                    final ItemMeta meta1 = playerDrop1.getItemMeta();
                    meta1.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                    meta1.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                    meta1.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                    meta1.addItemFlags(ItemFlag.HIDE_DESTROYS);
                    meta1.addItemFlags(ItemFlag.HIDE_PLACED_ON);
                    meta1.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                    meta1.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
                    final ArrayList<String> lore1 = new ArrayList<>();
                    lore1.add(" ");
                    lore1.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Pressa");
                    lore1.add(" ");
                    meta1.setLore(lore1);
                    final FireworkEffectMeta metaFw1 = (FireworkEffectMeta) meta1;
                    final FireworkEffect aa1 = FireworkEffect.builder().withColor(Color.NAVY).build();
                    metaFw1.setEffect(aa1);
                    playerDrop1.setItemMeta(metaFw1);
                    inventory.setItem(10, playerDrop1);

                    final ItemStack playerDrop2 = new ItemStack(Material.FIREWORK_STAR, 1);//3
                    final ItemMeta meta2 = playerDrop2.getItemMeta();
                    meta2.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                    meta2.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                    meta2.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                    meta2.addItemFlags(ItemFlag.HIDE_DESTROYS);
                    meta2.addItemFlags(ItemFlag.HIDE_PLACED_ON);
                    meta2.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                    meta2.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
                    final ArrayList<String> lore2 = new ArrayList<>();
                    lore2.add(" ");
                    lore2.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Regeneração");
                    lore2.add(" ");
                    meta2.setLore(lore2);
                    final FireworkEffectMeta metaFw2 = (FireworkEffectMeta) meta2;
                    final FireworkEffect aa2 = FireworkEffect.builder().withColor(Color.FUCHSIA).build();
                    metaFw2.setEffect(aa2);
                    playerDrop2.setItemMeta(metaFw2);
                    inventory.setItem(11, playerDrop2);

                    final ItemStack playerDrop3 = new ItemStack(Material.FIREWORK_STAR, 1);//3
                    final ItemMeta meta3 = playerDrop3.getItemMeta();
                    meta3.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                    meta3.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                    meta3.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                    meta3.addItemFlags(ItemFlag.HIDE_DESTROYS);
                    meta3.addItemFlags(ItemFlag.HIDE_PLACED_ON);
                    meta3.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                    meta3.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
                    final ArrayList<String> lore3 = new ArrayList<>();
                    lore3.add(" ");
                    lore3.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Super pulo");
                    lore3.add(" ");
                    meta3.setLore(lore3);
                    final FireworkEffectMeta metaFw3 = (FireworkEffectMeta) meta3;
                    final FireworkEffect aa3 = FireworkEffect.builder().withColor(Color.LIME).build();
                    metaFw3.setEffect(aa3);
                    playerDrop3.setItemMeta(metaFw3);
                    inventory.setItem(12, playerDrop3);

                    final ItemStack playerDrop4 = new ItemStack(Material.FIREWORK_STAR, 1);//3
                    final ItemMeta meta4 = playerDrop4.getItemMeta();
                    meta4.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
                    meta4.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                    meta4.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                    meta4.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                    meta4.addItemFlags(ItemFlag.HIDE_DESTROYS);
                    meta4.addItemFlags(ItemFlag.HIDE_PLACED_ON);
                    meta4.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                    final ArrayList<String> lore4 = new ArrayList<>();
                    lore4.add(" ");
                    lore4.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Velocidade");
                    lore4.add(" ");
                    meta4.setLore(lore4);
                    final FireworkEffectMeta metaFw4 = (FireworkEffectMeta) meta4;
                    final FireworkEffect aa4 = FireworkEffect.builder().withColor(Color.BLUE).build();
                    metaFw4.setEffect(aa4);
                    playerDrop4.setItemMeta(metaFw4);
                    inventory.setItem(13, playerDrop4);

                    final ItemStack voltar = new ItemStack(Material.ARROW);
                    final ItemMeta metaV = voltar.getItemMeta();
                    metaV.setDisplayName(ChatColor.RED + "Voltar");
                    voltar.setItemMeta(metaV);
                    inventory.setItem(45, voltar);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(inventory);
                        }
                    }, 5L);
                }
            }

            if (event.getClickedInventory().getName().equalsIgnoreCase("Caixa suprema: ")) {
                event.setCancelled(true);

                if (event.getSlot() == 45) {
                    final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Caixas misteriosas: ");

                    final ItemStack basica = new ItemStack(Material.CHEST);
                    final ItemMeta metaBasica = basica.getItemMeta();
                    metaBasica.setDisplayName(ChatColor.GOLD + "Caixa misteriosa");
                    final ArrayList<String> loreB = new ArrayList<>();
                    loreB.add(" ");
                    loreB.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Básica");
                    loreB.add(" ");
                    metaBasica.setLore(loreB);
                    basica.setItemMeta(metaBasica);

                    final ItemStack avançada = new ItemStack(Material.ENDER_CHEST);
                    final ItemMeta metaAvançada = avançada.getItemMeta();
                    metaAvançada.setDisplayName(ChatColor.GOLD + "Caixa misteriosa");
                    final ArrayList<String> loreA = new ArrayList<>();
                    loreA.add(" ");
                    loreA.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Avançada");
                    loreA.add(" ");
                    metaAvançada.setLore(loreA);
                    avançada.setItemMeta(metaAvançada);

                    final ItemStack suprema = new ItemStack(Material.END_PORTAL_FRAME);
                    final ItemMeta metaSuprema = suprema.getItemMeta();
                    metaSuprema.setDisplayName(ChatColor.GOLD + "Caixa misteriosa");
                    final ArrayList<String> loreS = new ArrayList<>();
                    loreS.add(" ");
                    loreS.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Suprema");
                    loreS.add(" ");
                    metaSuprema.setLore(loreS);
                    suprema.setItemMeta(metaSuprema);

                    final ItemStack spawners = new ItemStack(Material.SPAWNER);
                    final ItemMeta metaSpawners = spawners.getItemMeta();
                    metaSpawners.setDisplayName(ChatColor.GOLD + "Caixa misteriosa");
                    final ArrayList<String> loreSp = new ArrayList<>();
                    loreSp.add(" ");
                    loreSp.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Spawners");
                    loreSp.add(" ");
                    metaSpawners.setLore(loreSp);
                    spawners.setItemMeta(metaSpawners);

                    final ItemStack caixa = new ItemStack(Material.BARRIER);
                    final ItemMeta metaCaixa = caixa.getItemMeta();
                    metaCaixa.setDisplayName(ChatColor.RED + "Caixa de evento");
                    final ArrayList<String> lore = new ArrayList<>();
                    lore.add(ChatColor.GRAY + "Nenhum evento está ocorrendo");
                    lore.add(ChatColor.GRAY + "no momento.");
                    metaCaixa.setLore(lore);
                    caixa.setItemMeta(metaCaixa);

                    inventory.setItem(10, basica);
                    inventory.setItem(11, avançada);
                    inventory.setItem(12, suprema);
                    inventory.setItem(13, spawners);
                    inventory.setItem(16, caixa);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(inventory);
                        }
                    }, 5L);
                }

                if (event.getSlot() == 53) {
                    //-
                }
            }

            if (event.getClickedInventory().getName().equalsIgnoreCase("Caixa spawners: ")) {
                event.setCancelled(true);

                if (event.getSlot() == 45) {
                    final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Caixas misteriosas: ");

                    final ItemStack basica = new ItemStack(Material.CHEST);
                    final ItemMeta metaBasica = basica.getItemMeta();
                    metaBasica.setDisplayName(ChatColor.GOLD + "Caixa misteriosa");
                    final ArrayList<String> loreB = new ArrayList<>();
                    loreB.add(" ");
                    loreB.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Básica");
                    loreB.add(" ");
                    metaBasica.setLore(loreB);
                    basica.setItemMeta(metaBasica);

                    final ItemStack avançada = new ItemStack(Material.ENDER_CHEST);
                    final ItemMeta metaAvançada = avançada.getItemMeta();
                    metaAvançada.setDisplayName(ChatColor.GOLD + "Caixa misteriosa");
                    final ArrayList<String> loreA = new ArrayList<>();
                    loreA.add(" ");
                    loreA.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Avançada");
                    loreA.add(" ");
                    metaAvançada.setLore(loreA);
                    avançada.setItemMeta(metaAvançada);

                    final ItemStack suprema = new ItemStack(Material.END_PORTAL_FRAME);
                    final ItemMeta metaSuprema = suprema.getItemMeta();
                    metaSuprema.setDisplayName(ChatColor.GOLD + "Caixa misteriosa");
                    final ArrayList<String> loreS = new ArrayList<>();
                    loreS.add(" ");
                    loreS.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Suprema");
                    loreS.add(" ");
                    metaSuprema.setLore(loreS);
                    suprema.setItemMeta(metaSuprema);

                    final ItemStack spawners = new ItemStack(Material.SPAWNER);
                    final ItemMeta metaSpawners = spawners.getItemMeta();
                    metaSpawners.setDisplayName(ChatColor.GOLD + "Caixa misteriosa");
                    final ArrayList<String> loreSp = new ArrayList<>();
                    loreSp.add(" ");
                    loreSp.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Spawners");
                    loreSp.add(" ");
                    metaSpawners.setLore(loreSp);
                    spawners.setItemMeta(metaSpawners);

                    final ItemStack caixa = new ItemStack(Material.BARRIER);
                    final ItemMeta metaCaixa = caixa.getItemMeta();
                    metaCaixa.setDisplayName(ChatColor.RED + "Caixa de evento");
                    final ArrayList<String> lore = new ArrayList<>();
                    lore.add(ChatColor.GRAY + "Nenhum evento está ocorrendo");
                    lore.add(ChatColor.GRAY + "no momento.");
                    metaCaixa.setLore(lore);
                    caixa.setItemMeta(metaCaixa);

                    inventory.setItem(10, basica);
                    inventory.setItem(11, avançada);
                    inventory.setItem(12, suprema);
                    inventory.setItem(13, spawners);
                    inventory.setItem(16, caixa);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(inventory);
                        }
                    }, 5L);
                }
            }

            if (event.getClickedInventory().getName().equalsIgnoreCase("Caixas misteriosas: ")) {
                event.setCancelled(true);

                if (event.getSlot() == 13) {
                    final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Caixa spawners: ");

                    final ArrayList<ItemStack> caixaSpawners = new ArrayList<>();
                    final String[] mobs = {"Vaca", "Galinha", "Porco", "Ovelha", "Coelho", "Zumbi", "Esqueleto", "Aranha", "Creeper", "Enderman", "Golem_de_ferro", "Slime", "Blaze", "Homem_porco_zumbi", "Esqueleto_wither", "Bruxa", "Cubo_magma", "Ghast"};
                    for (int i = 1; i <= mobs.length; i++) {
                        final ItemStack item = new ItemStack(Material.SPAWNER, 1);
                        final ItemMeta metaItem = item.getItemMeta();
                        metaItem.setDisplayName(ChatColor.RED + "Spawner de " + mobs[i - 1].replaceAll("_", " "));
                        item.setItemMeta(metaItem);

                        caixaSpawners.add(item);
                    }

                    int i = 0;
                    for (final ItemStack item : caixaSpawners) {
                        i++;
                        if (i <= 7) {
                            inventory.setItem(9 + i, item);
                        } else if (i > 7 && i <= 14) {
                            inventory.setItem(11 + i, item);
                        } else if (i > 14 && i <= 21) {
                            inventory.setItem(13 + i, item);
                        } else if (i > 21) {
                            inventory.setItem(15 + i, item);
                        }
                    }

                    final ItemStack voltar = new ItemStack(Material.ARROW);
                    final ItemMeta metaV = voltar.getItemMeta();
                    metaV.setDisplayName(ChatColor.RED + "Voltar");
                    voltar.setItemMeta(metaV);
                    inventory.setItem(45, voltar);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(inventory);
                        }
                    }, 5L);
                }

                if (event.getSlot() == 12) {
                    final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Caixa suprema: ");

                    inventory.setItem(10, CriarItem.add(Material.DIAMOND_SWORD, "Espada de diamante", Enchantment.FIRE_ASPECT, 2, Enchantment.DURABILITY, 5, Enchantment.DAMAGE_ALL, 5, Enchantment.LOOT_BONUS_MOBS, 3));//2
                    inventory.setItem(11, CriarItem.add(Material.DIAMOND_HELMET, "Capacete de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 5, Enchantment.DURABILITY, 5, Enchantment.FIRE_ASPECT, 1));//2
                    inventory.setItem(12, CriarItem.add(Material.DIAMOND_CHESTPLATE, "Peitoral de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 5, Enchantment.DURABILITY, 5, Enchantment.FIRE_ASPECT, 1));//2
                    inventory.setItem(13, CriarItem.add(Material.DIAMOND_LEGGINGS, "Calças de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 5, Enchantment.DURABILITY, 5, Enchantment.FIRE_ASPECT, 1));//2
                    inventory.setItem(14, CriarItem.add(Material.DIAMOND_BOOTS, "Botas de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 5, Enchantment.DURABILITY, 5, Enchantment.FIRE_ASPECT, 1));//2
                    inventory.setItem(15, CriarItem.add(Material.BOW, "Arco", Enchantment.DURABILITY, 3, Enchantment.ARROW_FIRE, 2, Enchantment.ARROW_INFINITE, 1, Enchantment.ARROW_DAMAGE, 5));//5
                    inventory.setItem(16, CriarItem.add(Material.DIAMOND_PICKAXE, "Picareta de diamante", Enchantment.DURABILITY, 5, Enchantment.LOOT_BONUS_BLOCKS, 3, Enchantment.DIG_SPEED, 5));//3
                    inventory.setItem(19, CriarItem.add(Material.DIAMOND_SWORD, "Espada de diamante", Enchantment.DURABILITY, 3, Enchantment.DAMAGE_ALL, 4, Enchantment.FIRE_ASPECT, 2));//4
                    inventory.setItem(20, CriarItem.add(Material.DIAMOND_HELMET, "Capacete de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 4, Enchantment.DURABILITY, 4));//4
                    inventory.setItem(21, CriarItem.add(Material.DIAMOND_CHESTPLATE, "Peitoral de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 4, Enchantment.DURABILITY, 4));//4
                    inventory.setItem(22, CriarItem.add(Material.DIAMOND_LEGGINGS, "Calças de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 4, Enchantment.DURABILITY, 4));//4
                    inventory.setItem(23, CriarItem.add(Material.DIAMOND_BOOTS, "Botas de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 4, Enchantment.DURABILITY, 4));//4
                    inventory.setItem(24, CriarItem.add(Material.BOW, "Arco", Enchantment.DURABILITY, 3, Enchantment.ARROW_FIRE, 2, Enchantment.ARROW_DAMAGE, 3));//6
                    inventory.setItem(25, CriarItem.add(Material.DIAMOND_PICKAXE, "Picareta de diamante", Enchantment.DURABILITY, 4, Enchantment.LOOT_BONUS_BLOCKS, 3, Enchantment.DIG_SPEED, 4));//5
                    inventory.setItem(28, CriarItem.add(Material.DIAMOND_AXE, "Machado de diamante", Enchantment.DURABILITY, 5, Enchantment.LOOT_BONUS_BLOCKS, 3, Enchantment.DIG_SPEED, 5));//4
                    inventory.setItem(29, CriarItem.add(Material.DIAMOND_AXE, "Machado de diamante", Enchantment.DIG_SPEED, 5, Enchantment.DURABILITY, 4));//5
                    inventory.setItem(30, CriarItem.add(Material.IRON_BLOCK, 32));//8
                    inventory.setItem(31, CriarItem.add(Material.GOLD_BLOCK, 32));//8
                    inventory.setItem(32, CriarItem.add(Material.DIAMOND_BLOCK, 32));//7
                    inventory.setItem(33, CriarItem.add(Material.CREEPER_SPAWN_EGG, 8));//4
                    inventory.setItem(34, CriarItem.add(Material.GHAST_SPAWN_EGG, 6));//4
                    inventory.setItem(37, CriarItem.add(Material.ENCHANTED_GOLDEN_APPLE, 32));//2

                    final ArrayList<String> loreSupr = new ArrayList<>();
                    loreSupr.add(" ");
                    loreSupr.add(ChatColor.GOLD + "Item supremo");
                    loreSupr.add(" ");

                    final ItemStack espada = CriarItem.add(Material.DIAMOND_SWORD, "Espada de diamante", Enchantment.DURABILITY, 5, Enchantment.DAMAGE_ALL, 5, Enchantment.FIRE_ASPECT, 2, Enchantment.LOOT_BONUS_MOBS, 3);
                    final ItemMeta metaEspa = espada.getItemMeta();
                    metaEspa.setLore(loreSupr);
                    espada.setItemMeta(metaEspa);
                    inventory.setItem(38, espada);//1

                    final ItemStack pick1 = CriarItem.add(Material.DIAMOND_PICKAXE, "Picareta de diamante", Enchantment.DURABILITY, 5, Enchantment.LOOT_BONUS_BLOCKS, 3, Enchantment.DIG_SPEED, 5);
                    final ItemMeta metaPick1 = pick1.getItemMeta();
                    metaPick1.setLore(loreSupr);
                    pick1.setItemMeta(metaPick1);
                    inventory.setItem(39, pick1);//1

                    final ItemStack pick2 = CriarItem.add(Material.DIAMOND_PICKAXE, "Picareta de diamante", Enchantment.DURABILITY, 4, Enchantment.LOOT_BONUS_BLOCKS, 3, Enchantment.DIG_SPEED, 4);
                    final ItemMeta metaPick2 = pick2.getItemMeta();
                    metaPick2.setLore(loreSupr);
                    pick2.setItemMeta(metaPick2);
                    inventory.setItem(40, pick2);//1

                    inventory.setItem(41, new ItemStack(Material.BEACON, 1));//2
                    inventory.setItem(42, new ItemStack(Material.BEACON, 2));//1

                    ItemStack jetpack = new ItemStack(Material.GOLDEN_CHESTPLATE);
                    ItemMeta meta = jetpack.getItemMeta();
                    meta.setDisplayName(ChatColor.YELLOW + "Jetpack");
                    ArrayList<String> lore = new ArrayList<>();
                    lore.add(" ");
                    lore.add(ChatColor.GRAY + "Para ativar/desativar aperte SHIFT,");
                    lore.add(ChatColor.GRAY + "sobre o chão!");
                    lore.add(" ");
                    meta.setLore(lore);
                    jetpack.setItemMeta(meta);
                    inventory.setItem(43, jetpack);

                    final ItemStack voltar = new ItemStack(Material.ARROW);
                    final ItemMeta metaV = voltar.getItemMeta();
                    metaV.setDisplayName(ChatColor.RED + "Voltar");
                    voltar.setItemMeta(metaV);
                    inventory.setItem(45, voltar);

                    final ItemStack proxima = new ItemStack(Material.ARROW);
                    final ItemMeta metaP = proxima.getItemMeta();
                    metaP.setDisplayName(ChatColor.RED + "Próxima página");
                    proxima.setItemMeta(metaP);
                    inventory.setItem(53, proxima);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(inventory);
                        }
                    }, 5L);
                }

                if (event.getSlot() == 11) {
                    final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Caixa avançada: ");

                    inventory.setItem(10, CriarItem.add(Material.DIAMOND_SWORD, "Espada de diamante", Enchantment.FIRE_ASPECT, 2, Enchantment.DURABILITY, 3, Enchantment.DAMAGE_ALL, 5, Enchantment.LOOT_BONUS_MOBS, 3));//1
                    inventory.setItem(11, CriarItem.add(Material.DIAMOND_HELMET, "Capacete de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 4, Enchantment.DURABILITY, 3));//1
                    inventory.setItem(12, CriarItem.add(Material.DIAMOND_CHESTPLATE, "Peitoral de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 4, Enchantment.DURABILITY, 3));//1
                    inventory.setItem(13, CriarItem.add(Material.DIAMOND_LEGGINGS, "Calças de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 4, Enchantment.DURABILITY, 3));//1
                    inventory.setItem(14, CriarItem.add(Material.DIAMOND_BOOTS, "Botas de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 4, Enchantment.DURABILITY, 3));//1
                    inventory.setItem(15, CriarItem.add(Material.BOW, "Arco", Enchantment.DURABILITY, 3, Enchantment.ARROW_FIRE, 2, Enchantment.ARROW_INFINITE, 1));//3
                    inventory.setItem(16, CriarItem.add(Material.DIAMOND_PICKAXE, "Picareta de diamante", Enchantment.DURABILITY, 3, Enchantment.LOOT_BONUS_BLOCKS, 3, Enchantment.DIG_SPEED, 5));//1
                    inventory.setItem(19, CriarItem.add(Material.DIAMOND_SWORD, "Espada de diamante", Enchantment.DURABILITY, 3, Enchantment.DAMAGE_ALL, 5, Enchantment.FIRE_ASPECT, 2));//3]
                    inventory.setItem(20, CriarItem.add(Material.DIAMOND_HELMET, "Capacete de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 3, Enchantment.DURABILITY, 3));//3
                    inventory.setItem(21, CriarItem.add(Material.DIAMOND_CHESTPLATE, "Peitoral de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 3, Enchantment.DURABILITY, 3));//3
                    inventory.setItem(22, CriarItem.add(Material.DIAMOND_LEGGINGS, "Calças de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 3, Enchantment.DURABILITY, 3));//3
                    inventory.setItem(23, CriarItem.add(Material.DIAMOND_BOOTS, "Botas de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 3, Enchantment.DURABILITY, 3));//3
                    inventory.setItem(24, CriarItem.add(Material.BOW, "Arco", Enchantment.DURABILITY, 3, Enchantment.ARROW_FIRE, 2));//5
                    inventory.setItem(25, CriarItem.add(Material.DIAMOND_PICKAXE, "Picareta de diamante", Enchantment.DURABILITY, 2, Enchantment.SILK_TOUCH, 1, Enchantment.DIG_SPEED, 5));//3
                    inventory.setItem(29, CriarItem.add(Material.DIAMOND_AXE, "Machado de diamante", Enchantment.DURABILITY, 3, Enchantment.LOOT_BONUS_BLOCKS, 3, Enchantment.DIG_SPEED, 5));//2
                    inventory.setItem(28, CriarItem.add(Material.DIAMOND_AXE, "Machado de diamante", Enchantment.DIG_SPEED, 5, Enchantment.DURABILITY, 3));//4
                    inventory.setItem(30, CriarItem.add(Material.NETHER_STAR, ChatColor.GREEN + "Poder máximo", 1, ""));//2
                    inventory.setItem(31, CriarItem.add(Material.NETHER_STAR, ChatColor.GREEN + "Poder máximo", 2, ""));//1
                    inventory.setItem(32, CriarItem.add(Material.ENCHANTED_GOLDEN_APPLE, 16));//2
                    inventory.setItem(33, CriarItem.add(Material.CREEPER_SPAWN_EGG, 5));//2
                    inventory.setItem(34, CriarItem.add(Material.GHAST_SPAWN_EGG, 5));//2
                    inventory.setItem(37, CriarItem.add(Material.IRON_BLOCK, 15));//8
                    inventory.setItem(38, CriarItem.add(Material.GOLD_BLOCK, 15));//8
                    inventory.setItem(39, CriarItem.add(Material.DIAMOND_BLOCK, 15));//8
                    inventory.setItem(40, CriarItem.add(Material.LAPIS_BLOCK, 15));//8

                    final ItemStack caixaSup = new ItemStack(Material.END_PORTAL_FRAME);//2
                    final ItemMeta metaCaixaSup = caixaSup.getItemMeta();
                    metaCaixaSup.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
                    final List<String> loreSup = new ArrayList<String>();
                    loreSup.add(" ");
                    loreSup.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Suprema");
                    loreSup.add(" ");
                    metaCaixaSup.setLore(loreSup);
                    caixaSup.setItemMeta(metaCaixaSup);
                    inventory.setItem(41, caixaSup);

                    final ItemStack booster = new ItemStack(Material.BLAZE_POWDER);//4
                    final ItemMeta meta = booster.getItemMeta();
                    meta.setDisplayName(ChatColor.GREEN + "Booster mcmmo");
                    final ArrayList<String> lore = new ArrayList<>();
                    lore.add(" ");
                    lore.add(ChatColor.WHITE + "Tipo: " + ChatColor.GRAY + "Membro");
                    lore.add(ChatColor.WHITE + "Tempo: " + ChatColor.GRAY + "15 minutos");
                    lore.add(" ");
                    meta.setLore(lore);
                    booster.setItemMeta(meta);
                    inventory.setItem(42, booster);

                    final ItemStack playerDrop = new ItemStack(Material.FIREWORK_STAR, 1);//3
                    final ItemMeta meta100 = playerDrop.getItemMeta();
                    meta100.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                    meta100.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                    meta100.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                    meta100.addItemFlags(ItemFlag.HIDE_DESTROYS);
                    meta100.addItemFlags(ItemFlag.HIDE_PLACED_ON);
                    meta100.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                    meta100.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
                    final ArrayList<String> lore100 = new ArrayList<>();
                    lore100.add(" ");
                    lore100.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Força");
                    lore100.add(" ");
                    meta100.setLore(lore100);
                    final FireworkEffectMeta metaFw = (FireworkEffectMeta) meta100;
                    final FireworkEffect aa = FireworkEffect.builder().withColor(Color.RED).build();
                    metaFw.setEffect(aa);
                    playerDrop.setItemMeta(metaFw);
                    inventory.setItem(43, playerDrop);

                    final ItemStack voltar = new ItemStack(Material.ARROW);
                    final ItemMeta metaV = voltar.getItemMeta();
                    metaV.setDisplayName(ChatColor.RED + "Voltar");
                    voltar.setItemMeta(metaV);
                    inventory.setItem(45, voltar);

                    final ItemStack proxima = new ItemStack(Material.ARROW);
                    final ItemMeta metaP = proxima.getItemMeta();
                    metaP.setDisplayName(ChatColor.RED + "Próxima página");
                    proxima.setItemMeta(metaP);
                    inventory.setItem(53, proxima);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(inventory);
                        }
                    }, 5L);
                }

                if (event.getSlot() == 10) {
                    final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Caixa básica: ");

                    inventory.setItem(10, CriarItem.add(Material.DIAMOND_SWORD, "Espada de diamante", Enchantment.FIRE_ASPECT, 2, Enchantment.DURABILITY, 3, Enchantment.DAMAGE_ALL, 2));
                    inventory.setItem(11, CriarItem.add(Material.DIAMOND_HELMET, "Capacete de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 3, Enchantment.DURABILITY, 3));
                    inventory.setItem(12, CriarItem.add(Material.DIAMOND_CHESTPLATE, "Peitoral de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 3, Enchantment.DURABILITY, 3));
                    inventory.setItem(13, CriarItem.add(Material.DIAMOND_LEGGINGS, "Calças de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 3, Enchantment.DURABILITY, 3));
                    inventory.setItem(14, CriarItem.add(Material.DIAMOND_BOOTS, "Botas de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 3, Enchantment.DURABILITY, 3));
                    inventory.setItem(15, CriarItem.add(Material.BOW, "Arco", Enchantment.DURABILITY, 3, Enchantment.ARROW_FIRE, 1, Enchantment.ARROW_INFINITE, 1));
                    inventory.setItem(16, CriarItem.add(Material.DIAMOND_PICKAXE, "Picareta de diamante", Enchantment.DURABILITY, 3, Enchantment.LOOT_BONUS_BLOCKS, 2));
                    inventory.setItem(19, CriarItem.add(Material.DIAMOND_SWORD, "Espada de diamante", Enchantment.DURABILITY, 3, Enchantment.DAMAGE_ALL, 2));
                    inventory.setItem(20, CriarItem.add(Material.DIAMOND_HELMET, "Capacete de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 2, Enchantment.DURABILITY, 2));
                    inventory.setItem(21, CriarItem.add(Material.DIAMOND_CHESTPLATE, "Peitoral de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 2, Enchantment.DURABILITY, 2));
                    inventory.setItem(22, CriarItem.add(Material.DIAMOND_LEGGINGS, "Calças de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 2, Enchantment.DURABILITY, 2));
                    inventory.setItem(23, CriarItem.add(Material.DIAMOND_BOOTS, "Botas de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 2, Enchantment.DURABILITY, 2));
                    inventory.setItem(24, CriarItem.add(Material.BOW, "Arco", Enchantment.DURABILITY, 3, Enchantment.ARROW_INFINITE, 1));
                    inventory.setItem(25, CriarItem.add(Material.DIAMOND_PICKAXE, "Picareta de diamante", Enchantment.DURABILITY, 2, Enchantment.SILK_TOUCH, 1));
                    inventory.setItem(28, CriarItem.add(Material.DIAMOND_AXE, "Machado de diamante", Enchantment.DURABILITY, 3, Enchantment.LOOT_BONUS_BLOCKS, 2));
                    inventory.setItem(29, CriarItem.add(Material.DIAMOND_AXE, "Machado de diamante", Enchantment.DIG_SPEED, 4, Enchantment.DURABILITY, 3));
                    inventory.setItem(30, CriarItem.add(Material.ENCHANTED_GOLDEN_APPLE, 16));
                    inventory.setItem(31, CriarItem.add(Material.CREEPER_SPAWN_EGG, 2));
                    inventory.setItem(32, CriarItem.add(Material.GHAST_SPAWN_EGG, 2));
                    inventory.setItem(33, CriarItem.add(Material.IRON_BLOCK, 5));
                    inventory.setItem(34, CriarItem.add(Material.GOLD_BLOCK, 5));
                    inventory.setItem(37, CriarItem.add(Material.DIAMOND_BLOCK, 5));
                    inventory.setItem(38, CriarItem.add(Material.LAPIS_BLOCK, 5));

                    final ItemStack voltar = new ItemStack(Material.ARROW);
                    final ItemMeta meta = voltar.getItemMeta();
                    meta.setDisplayName(ChatColor.RED + "Voltar");
                    voltar.setItemMeta(meta);
                    inventory.setItem(45, voltar);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(inventory);
                        }
                    }, 5L);
                }
            }
        }
    }
}
