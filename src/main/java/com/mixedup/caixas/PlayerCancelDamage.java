package com.mixedup.caixas;

import java.util.HashMap;

public class PlayerCancelDamage {

    private static final HashMap<String, PlayerCancelDamage> CACHE = new HashMap<String, PlayerCancelDamage>();
    private final String UUID;
    private boolean status;

    public PlayerCancelDamage(final String UUID, final boolean status) {
        this.UUID = UUID;
        this.status = status;
    }

    public static PlayerCancelDamage get(final String UUID) {
        return PlayerCancelDamage.CACHE.get(String.valueOf(UUID));
    }

    public PlayerCancelDamage insert() {
        PlayerCancelDamage.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(final boolean status) {
        this.status = status;
    }

    public String getUUID() {
        return UUID;
    }
}
