package com.mixedup.caixas;

import com.mixedup.Main;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.utils.AlternateColor;
import com.mixedup.utils.CriarItem;
import com.mixedup.utils.MessageBuilder;
import com.mixedup.utils.RemoveItemInv;
import org.bukkit.*;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkEffectMeta;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;
import java.util.Map.Entry;

public class EventPlayerOpenCaixa implements Listener {

    private static int lastID;
    int corAnterior;
    int itemAnterior;
    int slotCores;
    int slotItens;

    private static List<ItemStack> getListCaixa(final String caixa) {
        final List<ItemStack> caixaBasica = new ArrayList<ItemStack>();
        final List<ItemStack> caixaAvancada = new ArrayList<ItemStack>();
        final List<ItemStack> caixaSpawners = new ArrayList<ItemStack>();
        final List<ItemStack> caixaEspecial = new ArrayList<ItemStack>();
        final List<ItemStack> caixaSuprema = new ArrayList<ItemStack>();
        List<ItemStack> list = new ArrayList<ItemStack>();

        //CRIAR LISTA DE ITENS
        caixaSuprema.add(CriarItem.add(Material.DIAMOND_HELMET, "Capacete de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 4, Enchantment.DURABILITY, 4));//4
        caixaSuprema.add(CriarItem.add(Material.DIAMOND_CHESTPLATE, "Peitoral de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 4, Enchantment.DURABILITY, 4));//4
        caixaSuprema.add(CriarItem.add(Material.DIAMOND_LEGGINGS, "Calças de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 4, Enchantment.DURABILITY, 4));//4
        caixaSuprema.add(CriarItem.add(Material.DIAMOND_BOOTS, "Botas de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 4, Enchantment.DURABILITY, 4));//4
        caixaSuprema.add(CriarItem.add(Material.DIAMOND_HELMET, "Capacete de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 5, Enchantment.DURABILITY, 5, Enchantment.FIRE_ASPECT, 1));//2
        caixaSuprema.add(CriarItem.add(Material.DIAMOND_CHESTPLATE, "Peitoral de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 5, Enchantment.DURABILITY, 5, Enchantment.FIRE_ASPECT, 1));//2
        caixaSuprema.add(CriarItem.add(Material.DIAMOND_LEGGINGS, "Calças de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 5, Enchantment.DURABILITY, 5, Enchantment.FIRE_ASPECT, 1));//2
        caixaSuprema.add(CriarItem.add(Material.DIAMOND_BOOTS, "Botas de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 5, Enchantment.DURABILITY, 5, Enchantment.FIRE_ASPECT, 1));//2
        caixaSuprema.add(CriarItem.add(Material.DIAMOND_SWORD, "Espada de diamante", Enchantment.FIRE_ASPECT, 2, Enchantment.DURABILITY, 5, Enchantment.DAMAGE_ALL, 5, Enchantment.LOOT_BONUS_MOBS, 3));//2
        caixaSuprema.add(CriarItem.add(Material.DIAMOND_SWORD, "Espada de diamante", Enchantment.DURABILITY, 3, Enchantment.DAMAGE_ALL, 4, Enchantment.FIRE_ASPECT, 2));//4
        caixaSuprema.add(CriarItem.add(Material.DIAMOND_PICKAXE, "Picareta de diamante", Enchantment.DURABILITY, 5, Enchantment.LOOT_BONUS_BLOCKS, 3, Enchantment.DIG_SPEED, 5));//3
        caixaSuprema.add(CriarItem.add(Material.DIAMOND_PICKAXE, "Picareta de diamante", Enchantment.DURABILITY, 4, Enchantment.LOOT_BONUS_BLOCKS, 3, Enchantment.DIG_SPEED, 4));//5
        caixaSuprema.add(CriarItem.add(Material.DIAMOND_AXE, "Machado de diamante", Enchantment.DURABILITY, 5, Enchantment.LOOT_BONUS_BLOCKS, 3, Enchantment.DIG_SPEED, 5));//4
        caixaSuprema.add(CriarItem.add(Material.DIAMOND_AXE, "Machado de diamante", Enchantment.DIG_SPEED, 5, Enchantment.DURABILITY, 4));//5
        caixaSuprema.add(CriarItem.add(Material.BOW, "Arco", Enchantment.DURABILITY, 3, Enchantment.ARROW_FIRE, 2, Enchantment.ARROW_INFINITE, 1, Enchantment.ARROW_DAMAGE, 5));//5
        caixaSuprema.add(CriarItem.add(Material.BOW, "Arco", Enchantment.DURABILITY, 3, Enchantment.ARROW_FIRE, 2, Enchantment.ARROW_DAMAGE, 3));//6
        caixaSuprema.add(CriarItem.add(Material.IRON_BLOCK, 32));//8
        caixaSuprema.add(CriarItem.add(Material.GOLD_BLOCK, 32));//8
        caixaSuprema.add(CriarItem.add(Material.DIAMOND_BLOCK, 32));//7
        caixaSuprema.add(CriarItem.add(Material.CREEPER_SPAWN_EGG, 8));//4
        caixaSuprema.add(CriarItem.add(Material.GHAST_SPAWN_EGG, 6));//4
        caixaSuprema.add(CriarItem.add(Material.ENCHANTED_GOLDEN_APPLE, 32));//2

        ItemStack jetpack = new ItemStack(Material.GOLDEN_CHESTPLATE);
        ItemMeta meta = jetpack.getItemMeta();
        meta.setDisplayName(ChatColor.YELLOW + "Jetpack");
        ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.GRAY + "Para ativar/desativar aperte SHIFT,");
        lore.add(ChatColor.GRAY + "sobre o chão!");
        lore.add(" ");
        meta.setLore(lore);
        jetpack.setItemMeta(meta);
        caixaSuprema.add(jetpack); //1

        final ArrayList<String> loreSupr = new ArrayList<>();
        loreSupr.add(" ");
        loreSupr.add(ChatColor.GOLD + "Item supremo");
        loreSupr.add(" ");

        final ItemStack espada = CriarItem.add(Material.DIAMOND_SWORD, "Espada de diamante", Enchantment.DURABILITY, 5, Enchantment.DAMAGE_ALL, 5, Enchantment.FIRE_ASPECT, 2, Enchantment.LOOT_BONUS_MOBS, 3);
        final ItemMeta metaEspa = espada.getItemMeta();
        metaEspa.setLore(loreSupr);
        espada.setItemMeta(metaEspa);
        caixaSuprema.add(espada);//1

        final ItemStack pick1 = CriarItem.add(Material.DIAMOND_PICKAXE, "Picareta de diamante", Enchantment.DURABILITY, 5, Enchantment.LOOT_BONUS_BLOCKS, 3, Enchantment.DIG_SPEED, 5);
        final ItemMeta metaPick1 = pick1.getItemMeta();
        metaPick1.setLore(loreSupr);
        pick1.setItemMeta(metaPick1);
        caixaSuprema.add(pick1);//1

        final ItemStack pick2 = CriarItem.add(Material.DIAMOND_PICKAXE, "Picareta de diamante", Enchantment.DURABILITY, 4, Enchantment.LOOT_BONUS_BLOCKS, 3, Enchantment.DIG_SPEED, 4);
        final ItemMeta metaPick2 = pick2.getItemMeta();
        metaPick2.setLore(loreSupr);
        pick2.setItemMeta(metaPick2);
        caixaSuprema.add(pick2);//1

        caixaSuprema.add(new ItemStack(Material.BEACON, 1));//2
        caixaSuprema.add(new ItemStack(Material.BEACON, 2));//1

        final ItemStack caixaSup2 = new ItemStack(Material.END_PORTAL_FRAME, 2);//2
        final ItemMeta metaCaixaSup2 = caixaSup2.getItemMeta();
        metaCaixaSup2.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> loreSup2 = new ArrayList<String>();
        loreSup2.add(" ");
        loreSup2.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Suprema");
        loreSup2.add(" ");
        metaCaixaSup2.setLore(loreSup2);
        caixaSup2.setItemMeta(metaCaixaSup2);
        caixaSuprema.add(caixaSup2);


        final String[] mobs = {"Vaca", "Galinha", "Porco", "Ovelha", "Coelho", "Zumbi", "Esqueleto", "Aranha", "Creeper", "Enderman", "Golem_de_ferro", "Slime", "Blaze", "Homem_porco_zumbi", "Esqueleto_wither", "Bruxa", "Cubo_magma", "Ghast"};
        for (int i = 1; i <= mobs.length; i++) {
            final ItemStack item = new ItemStack(Material.SPAWNER, 1);
            final ItemMeta metaItem = item.getItemMeta();
            metaItem.setDisplayName(ChatColor.RED + "Spawner de " + mobs[i - 1].replaceAll("_", " "));
            item.setItemMeta(metaItem);

            caixaSpawners.add(item);
        }

        final ItemStack caixaA = new ItemStack(Material.ENDER_CHEST, 2);
        final ItemMeta metaCaixa = caixaA.getItemMeta();
        metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> lore103 = new ArrayList<String>();
        lore103.add(" ");
        lore103.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Avançada");
        lore103.add(" ");
        metaCaixa.setLore(lore103);
        caixaA.setItemMeta(metaCaixa);
        caixaSpawners.add(caixaA);
        caixaSpawners.add(caixaA);

        caixaBasica.add(CriarItem.add(Material.DIAMOND_HELMET, "Capacete de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 2, Enchantment.DURABILITY, 2));//2
        caixaBasica.add(CriarItem.add(Material.DIAMOND_CHESTPLATE, "Peitoral de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 2, Enchantment.DURABILITY, 2));//2
        caixaBasica.add(CriarItem.add(Material.DIAMOND_LEGGINGS, "Calças de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 2, Enchantment.DURABILITY, 2));//2
        caixaBasica.add(CriarItem.add(Material.DIAMOND_BOOTS, "Botas de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 2, Enchantment.DURABILITY, 2));//2
        caixaBasica.add(CriarItem.add(Material.DIAMOND_HELMET, "Capacete de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 3, Enchantment.DURABILITY, 3)); //1
        caixaBasica.add(CriarItem.add(Material.DIAMOND_CHESTPLATE, "Peitoral de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 3, Enchantment.DURABILITY, 3)); //1
        caixaBasica.add(CriarItem.add(Material.DIAMOND_LEGGINGS, "Calças de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 3, Enchantment.DURABILITY, 3)); //1
        caixaBasica.add(CriarItem.add(Material.DIAMOND_BOOTS, "Botas de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 3, Enchantment.DURABILITY, 3)); //1
        caixaBasica.add(CriarItem.add(Material.DIAMOND_SWORD, "Espada de diamante", Enchantment.FIRE_ASPECT, 2, Enchantment.DURABILITY, 3, Enchantment.DAMAGE_ALL, 2));//4
        caixaBasica.add(CriarItem.add(Material.DIAMOND_SWORD, "Espada de diamante", Enchantment.DURABILITY, 3, Enchantment.DAMAGE_ALL, 2));//8
        caixaBasica.add(CriarItem.add(Material.DIAMOND_PICKAXE, "Picareta de diamante", Enchantment.DURABILITY, 3, Enchantment.LOOT_BONUS_BLOCKS, 2));//5
        caixaBasica.add(CriarItem.add(Material.DIAMOND_PICKAXE, "Picareta de diamante", Enchantment.DURABILITY, 2, Enchantment.SILK_TOUCH, 1));//5
        caixaBasica.add(CriarItem.add(Material.DIAMOND_AXE, "Machado de diamante", Enchantment.DURABILITY, 3, Enchantment.LOOT_BONUS_BLOCKS, 2));//5
        caixaBasica.add(CriarItem.add(Material.DIAMOND_AXE, "Machado de diamante", Enchantment.DIG_SPEED, 4, Enchantment.DURABILITY, 3));//5
        caixaBasica.add(CriarItem.add(Material.BOW, "Arco", Enchantment.DURABILITY, 3, Enchantment.ARROW_FIRE, 1, Enchantment.ARROW_INFINITE, 1));//5
        caixaBasica.add(CriarItem.add(Material.BOW, "Arco", Enchantment.DURABILITY, 3, Enchantment.ARROW_INFINITE, 1));//8
        caixaBasica.add(CriarItem.add(Material.ENCHANTED_GOLDEN_APPLE, 16));//1
        caixaBasica.add(CriarItem.add(Material.CREEPER_SPAWN_EGG, 2));//1
        caixaBasica.add(CriarItem.add(Material.GHAST_SPAWN_EGG, 2));//1
        caixaBasica.add(CriarItem.add(Material.IRON_BLOCK, 5));//10
        caixaBasica.add(CriarItem.add(Material.GOLD_BLOCK, 5));//10
        caixaBasica.add(CriarItem.add(Material.DIAMOND_BLOCK, 5));//10
        caixaBasica.add(CriarItem.add(Material.LAPIS_BLOCK, 5));//10

        caixaAvancada.add(CriarItem.add(Material.DIAMOND_HELMET, "Capacete de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 3, Enchantment.DURABILITY, 3));//3
        caixaAvancada.add(CriarItem.add(Material.DIAMOND_CHESTPLATE, "Peitoral de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 3, Enchantment.DURABILITY, 3));//3
        caixaAvancada.add(CriarItem.add(Material.DIAMOND_LEGGINGS, "Calças de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 3, Enchantment.DURABILITY, 3));//3
        caixaAvancada.add(CriarItem.add(Material.DIAMOND_BOOTS, "Botas de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 3, Enchantment.DURABILITY, 3));//3
        caixaAvancada.add(CriarItem.add(Material.DIAMOND_HELMET, "Capacete de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 4, Enchantment.DURABILITY, 3));//1
        caixaAvancada.add(CriarItem.add(Material.DIAMOND_CHESTPLATE, "Peitoral de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 4, Enchantment.DURABILITY, 3));//1
        caixaAvancada.add(CriarItem.add(Material.DIAMOND_LEGGINGS, "Calças de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 4, Enchantment.DURABILITY, 3));//1
        caixaAvancada.add(CriarItem.add(Material.DIAMOND_BOOTS, "Botas de diamante", Enchantment.PROTECTION_ENVIRONMENTAL, 4, Enchantment.DURABILITY, 3));//1
        caixaAvancada.add(CriarItem.add(Material.DIAMOND_SWORD, "Espada de diamante", Enchantment.FIRE_ASPECT, 2, Enchantment.DURABILITY, 3, Enchantment.DAMAGE_ALL, 5, Enchantment.LOOT_BONUS_MOBS, 3));//1
        caixaAvancada.add(CriarItem.add(Material.DIAMOND_SWORD, "Espada de diamante", Enchantment.DURABILITY, 3, Enchantment.DAMAGE_ALL, 5, Enchantment.FIRE_ASPECT, 2));//3
        caixaAvancada.add(CriarItem.add(Material.DIAMOND_PICKAXE, "Picareta de diamante", Enchantment.DURABILITY, 3, Enchantment.LOOT_BONUS_BLOCKS, 3, Enchantment.DIG_SPEED, 5));//1
        caixaAvancada.add(CriarItem.add(Material.DIAMOND_PICKAXE, "Picareta de diamante", Enchantment.DURABILITY, 2, Enchantment.SILK_TOUCH, 1, Enchantment.DIG_SPEED, 5));//3
        caixaAvancada.add(CriarItem.add(Material.DIAMOND_AXE, "Machado de diamante", Enchantment.DURABILITY, 3, Enchantment.LOOT_BONUS_BLOCKS, 3, Enchantment.DIG_SPEED, 5));//2
        caixaAvancada.add(CriarItem.add(Material.DIAMOND_AXE, "Machado de diamante", Enchantment.DIG_SPEED, 5, Enchantment.DURABILITY, 3));//4
        caixaAvancada.add(CriarItem.add(Material.BOW, "Arco", Enchantment.DURABILITY, 3, Enchantment.ARROW_FIRE, 2, Enchantment.ARROW_INFINITE, 1));//3
        caixaAvancada.add(CriarItem.add(Material.BOW, "Arco", Enchantment.DURABILITY, 3, Enchantment.ARROW_FIRE, 2));//5
        caixaAvancada.add(CriarItem.add(Material.NETHER_STAR, ChatColor.GREEN + "Poder máximo", 1, ""));//2
        caixaAvancada.add(CriarItem.add(Material.NETHER_STAR, ChatColor.GREEN + "Poder máximo", 2, ""));//1
        caixaAvancada.add(CriarItem.add(Material.ENCHANTED_GOLDEN_APPLE, 16));//2
        caixaAvancada.add(CriarItem.add(Material.CREEPER_SPAWN_EGG, 5));//2
        caixaAvancada.add(CriarItem.add(Material.GHAST_SPAWN_EGG, 5));//2
        caixaAvancada.add(CriarItem.add(Material.IRON_BLOCK, 15));//8
        caixaAvancada.add(CriarItem.add(Material.GOLD_BLOCK, 15));//8
        caixaAvancada.add(CriarItem.add(Material.DIAMOND_BLOCK, 15));//8
        caixaAvancada.add(CriarItem.add(Material.LAPIS_BLOCK, 15));//8

        final ItemStack caixaSup = new ItemStack(Material.END_PORTAL_FRAME);//2
        final ItemMeta metaCaixaSup = caixaSup.getItemMeta();
        metaCaixaSup.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
        final List<String> loreSup = new ArrayList<String>();
        loreSup.add(" ");
        loreSup.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Suprema");
        loreSup.add(" ");
        metaCaixaSup.setLore(loreSup);
        caixaSup.setItemMeta(metaCaixaSup);
        caixaAvancada.add(caixaSup);

        final ItemStack booster = new ItemStack(Material.BLAZE_POWDER);//4
        final ItemMeta meta50 = booster.getItemMeta();
        meta50.setDisplayName(ChatColor.GREEN + "Booster mcmmo");
        final ArrayList<String> lore50 = new ArrayList<>();
        lore50.add(" ");
        lore50.add(ChatColor.WHITE + "Tipo: " + ChatColor.GRAY + "Membro");
        lore50.add(ChatColor.WHITE + "Tempo: " + ChatColor.GRAY + "15 minutos");
        lore50.add(" ");
        meta50.setLore(lore50);
        booster.setItemMeta(meta50);
        caixaAvancada.add(booster);

        final ItemStack playerDrop = new ItemStack(Material.FIREWORK_STAR, 1);//3
        final ItemMeta meta100 = playerDrop.getItemMeta();
        meta100.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta100.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        meta100.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        meta100.addItemFlags(ItemFlag.HIDE_DESTROYS);
        meta100.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        meta100.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        meta100.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
        final ArrayList<String> lore100 = new ArrayList<>();
        lore100.add(" ");
        lore100.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Força");
        lore100.add(" ");
        meta100.setLore(lore100);
        final FireworkEffectMeta metaFw = (FireworkEffectMeta) meta100;
        final FireworkEffect aa = FireworkEffect.builder().withColor(Color.RED).build();
        metaFw.setEffect(aa);
        playerDrop.setItemMeta(metaFw);
        caixaAvancada.add(playerDrop);

        final ItemStack playerDrop1 = new ItemStack(Material.FIREWORK_STAR, 1);//3
        final ItemMeta meta1 = playerDrop1.getItemMeta();
        meta1.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta1.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        meta1.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        meta1.addItemFlags(ItemFlag.HIDE_DESTROYS);
        meta1.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        meta1.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        meta1.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
        final ArrayList<String> lore1 = new ArrayList<>();
        lore1.add(" ");
        lore1.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Pressa");
        lore1.add(" ");
        meta1.setLore(lore1);
        final FireworkEffectMeta metaFw1 = (FireworkEffectMeta) meta1;
        final FireworkEffect aa1 = FireworkEffect.builder().withColor(Color.NAVY).build();
        metaFw1.setEffect(aa1);
        playerDrop1.setItemMeta(metaFw1);
        caixaAvancada.add(playerDrop1);

        final ItemStack playerDrop2 = new ItemStack(Material.FIREWORK_STAR, 1);//3
        final ItemMeta meta2 = playerDrop2.getItemMeta();
        meta2.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta2.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        meta2.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        meta2.addItemFlags(ItemFlag.HIDE_DESTROYS);
        meta2.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        meta2.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        meta2.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
        final ArrayList<String> lore2 = new ArrayList<>();
        lore2.add(" ");
        lore2.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Regeneração");
        lore2.add(" ");
        meta2.setLore(lore2);
        final FireworkEffectMeta metaFw2 = (FireworkEffectMeta) meta2;
        final FireworkEffect aa2 = FireworkEffect.builder().withColor(Color.FUCHSIA).build();
        metaFw2.setEffect(aa2);
        playerDrop2.setItemMeta(metaFw2);
        caixaAvancada.add(playerDrop2);

        final ItemStack playerDrop3 = new ItemStack(Material.FIREWORK_STAR, 1);//3
        final ItemMeta meta3 = playerDrop3.getItemMeta();
        meta3.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta3.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        meta3.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        meta3.addItemFlags(ItemFlag.HIDE_DESTROYS);
        meta3.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        meta3.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        meta3.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
        final ArrayList<String> lore3 = new ArrayList<>();
        lore3.add(" ");
        lore3.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Super pulo");
        lore3.add(" ");
        meta3.setLore(lore3);
        final FireworkEffectMeta metaFw3 = (FireworkEffectMeta) meta3;
        final FireworkEffect aa3 = FireworkEffect.builder().withColor(Color.LIME).build();
        metaFw3.setEffect(aa3);
        playerDrop3.setItemMeta(metaFw3);
        caixaAvancada.add(playerDrop3);

        final ItemStack playerDrop4 = new ItemStack(Material.FIREWORK_STAR, 1);//3
        final ItemMeta meta4 = playerDrop4.getItemMeta();
        meta4.setDisplayName(ChatColor.YELLOW + "Ativador de efeitos");
        meta4.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta4.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        meta4.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        meta4.addItemFlags(ItemFlag.HIDE_DESTROYS);
        meta4.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        meta4.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        final ArrayList<String> lore4 = new ArrayList<>();
        lore4.add(" ");
        lore4.add(ChatColor.DARK_GRAY + "Efeito: " + ChatColor.GRAY + "Velocidade");
        lore4.add(" ");
        meta4.setLore(lore4);
        final FireworkEffectMeta metaFw4 = (FireworkEffectMeta) meta4;
        final FireworkEffect aa4 = FireworkEffect.builder().withColor(Color.BLUE).build();
        metaFw4.setEffect(aa4);
        playerDrop4.setItemMeta(metaFw4);
        caixaAvancada.add(playerDrop4);

        if (caixa.equals("básica")) {
            list = caixaBasica;
        } else if (caixa.equals("avançada")) {
            list = caixaAvancada;
        } else if (caixa.equals("spawners")) {
            list = caixaSpawners;
        } else if (caixa.equals("especial")) {
            list = caixaEspecial;
        } else if (caixa.equals("suprema")) {
            list = caixaSuprema;
        }
        return list;
    }

    private static List<Integer> getChance(final String caixa) {
        final List<Integer> listChanceCaixaBasica = new ArrayList<Integer>();
        final List<Integer> listChanceCaixaAvancada = new ArrayList<Integer>();
        final List<Integer> listChanceCaixaSpawners = new ArrayList<Integer>();
        final List<Integer> listChanceCaixaEspacial = new ArrayList<Integer>();
        final List<Integer> listChanceCaixaSuprema = new ArrayList<Integer>();
        List<Integer> list = new ArrayList<Integer>();

        //PRIMEIRO ID DO ITEM DEPOIS CHANCE
        listChanceCaixaSuprema.add(4);
        listChanceCaixaSuprema.add(4);
        listChanceCaixaSuprema.add(4);
        listChanceCaixaSuprema.add(4);
        listChanceCaixaSuprema.add(2);
        listChanceCaixaSuprema.add(2);
        listChanceCaixaSuprema.add(2);
        listChanceCaixaSuprema.add(2);
        listChanceCaixaSuprema.add(2);
        listChanceCaixaSuprema.add(4);
        listChanceCaixaSuprema.add(3);
        listChanceCaixaSuprema.add(5);
        listChanceCaixaSuprema.add(4);
        listChanceCaixaSuprema.add(5);
        listChanceCaixaSuprema.add(5);
        listChanceCaixaSuprema.add(6);
        listChanceCaixaSuprema.add(8);
        listChanceCaixaSuprema.add(8);
        listChanceCaixaSuprema.add(7);
        listChanceCaixaSuprema.add(4);
        listChanceCaixaSuprema.add(4);
        listChanceCaixaSuprema.add(2);
        listChanceCaixaSuprema.add(2);
        listChanceCaixaSuprema.add(1);
        listChanceCaixaSuprema.add(1);
        listChanceCaixaSuprema.add(1);
        listChanceCaixaSuprema.add(2);
        listChanceCaixaSuprema.add(1);
        listChanceCaixaSuprema.add(1);

        listChanceCaixaSpawners.add(5);
        listChanceCaixaSpawners.add(10);
        listChanceCaixaSpawners.add(15);
        listChanceCaixaSpawners.add(13);
        listChanceCaixaSpawners.add(9);
        listChanceCaixaSpawners.add(2);
        listChanceCaixaSpawners.add(4);
        listChanceCaixaSpawners.add(5);
        listChanceCaixaSpawners.add(5);
        listChanceCaixaSpawners.add(5);
        listChanceCaixaSpawners.add(1);
        listChanceCaixaSpawners.add(1);
        listChanceCaixaSpawners.add(1);
        listChanceCaixaSpawners.add(4);
        listChanceCaixaSpawners.add(2);
        listChanceCaixaSpawners.add(2);
        listChanceCaixaSpawners.add(1);
        listChanceCaixaSpawners.add(1);
        listChanceCaixaSpawners.add(7);
        listChanceCaixaSpawners.add(7);

        listChanceCaixaBasica.add(2);
        listChanceCaixaBasica.add(2);
        listChanceCaixaBasica.add(2);
        listChanceCaixaBasica.add(2);
        listChanceCaixaBasica.add(1);
        listChanceCaixaBasica.add(1);
        listChanceCaixaBasica.add(1);
        listChanceCaixaBasica.add(1);
        listChanceCaixaBasica.add(4);
        listChanceCaixaBasica.add(8);
        listChanceCaixaBasica.add(5);
        listChanceCaixaBasica.add(5);
        listChanceCaixaBasica.add(5);
        listChanceCaixaBasica.add(5);
        listChanceCaixaBasica.add(5);
        listChanceCaixaBasica.add(8);
        listChanceCaixaBasica.add(1);
        listChanceCaixaBasica.add(1);
        listChanceCaixaBasica.add(1);
        listChanceCaixaBasica.add(10);
        listChanceCaixaBasica.add(10);
        listChanceCaixaBasica.add(10);
        listChanceCaixaBasica.add(10);

        listChanceCaixaAvancada.add(3);
        listChanceCaixaAvancada.add(3);
        listChanceCaixaAvancada.add(3);
        listChanceCaixaAvancada.add(3);
        listChanceCaixaAvancada.add(1);
        listChanceCaixaAvancada.add(1);
        listChanceCaixaAvancada.add(1);
        listChanceCaixaAvancada.add(1);
        listChanceCaixaAvancada.add(1);
        listChanceCaixaAvancada.add(3);
        listChanceCaixaAvancada.add(1);
        listChanceCaixaAvancada.add(3);
        listChanceCaixaAvancada.add(2);
        listChanceCaixaAvancada.add(4);
        listChanceCaixaAvancada.add(3);
        listChanceCaixaAvancada.add(5);
        listChanceCaixaAvancada.add(2);
        listChanceCaixaAvancada.add(1);
        listChanceCaixaAvancada.add(2);
        listChanceCaixaAvancada.add(2);
        listChanceCaixaAvancada.add(2);
        listChanceCaixaAvancada.add(8);
        listChanceCaixaAvancada.add(8);
        listChanceCaixaAvancada.add(8);
        listChanceCaixaAvancada.add(8);
        listChanceCaixaAvancada.add(2);
        listChanceCaixaAvancada.add(4);
        listChanceCaixaAvancada.add(3);
        listChanceCaixaAvancada.add(3);
        listChanceCaixaAvancada.add(3);
        listChanceCaixaAvancada.add(3);
        listChanceCaixaAvancada.add(3);

        if (caixa.equalsIgnoreCase("básica")) {
            list = listChanceCaixaBasica;
        } else if (caixa.equalsIgnoreCase("avançada")) {
            list = listChanceCaixaAvancada;
        } else if (caixa.equalsIgnoreCase("spawners")) {
            list = listChanceCaixaSpawners;
        } else if (caixa.equalsIgnoreCase("especial")) {
            list = listChanceCaixaEspacial;
        } else if (caixa.equalsIgnoreCase("suprema")) {
            list = listChanceCaixaSuprema;
        }
        return list;
    }

    private static int getItemRandom(final String caixa) {
        final Random random = new Random();
        final int valorRandom = random.nextInt(100);
        int item = 1;

        for (int i = 1; i <= EventPlayerOpenCaixa.getChance(caixa).size(); i++) {
            for (int x = 1; x <= EventPlayerOpenCaixa.getChance(caixa).get(i - 1); x++) {
                if (valorRandom == x + EventPlayerOpenCaixa.lastID) {
                    item = i - 1;
                    i = EventPlayerOpenCaixa.getChance(caixa).size() + 1;
                    break;
                }
                if (x == EventPlayerOpenCaixa.getChance(caixa).get(i - 1)) {
                    EventPlayerOpenCaixa.lastID = EventPlayerOpenCaixa.lastID + x;
                }
            }
        }
        EventPlayerOpenCaixa.lastID = 0;
        return item;
    }

    private static boolean getContainsKey(final Inventory inventory) {
        boolean has = false;
        final int size = inventory.getSize();
        final int amount = 1;
        for (int slot = 0; slot < size; slot++) {
            final ItemStack is = inventory.getItem(slot);
            if (is == null) {
                continue;
            }
            final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 1);
            final ItemMeta metaChave = chave.getItemMeta();
            metaChave.setDisplayName(ChatColor.GREEN + "Chave");
            final List<String> lore = new ArrayList<>();
            lore.add(" ");
            lore.add(ChatColor.DARK_GRAY + "Utilizado para abrir caixas");
            lore.add(ChatColor.YELLOW + "50% sucesso");
            lore.add(" ");
            metaChave.setLore(lore);
            chave.setItemMeta(metaChave);

            if (chave.getItemMeta().getDisplayName().equals(is.getItemMeta().getDisplayName())) {
                if (is.getAmount() > 0) {
                    has = true;
                    break;
                }
            }
        }

        return has;
    }

    private static boolean tryThis(final Inventory inventory) {
        boolean has = false;
        final int size = inventory.getSize();
        int amount = 1;
        for (int slot = 0; slot < size; slot++) {
            final ItemStack is = inventory.getItem(slot);
            if (is == null) {
                continue;
            }
            final ItemStack chave = new ItemStack(Material.TRIPWIRE_HOOK, 1);
            final ItemMeta metaChave = chave.getItemMeta();
            metaChave.setDisplayName(ChatColor.GREEN + "Chave");
            final List<String> lore = new ArrayList<>();
            lore.add(" ");
            lore.add(ChatColor.DARK_GRAY + "Utilizado para abrir caixas");
            lore.add(ChatColor.YELLOW + "50% sucesso");
            lore.add(" ");
            metaChave.setLore(lore);
            chave.setItemMeta(metaChave);

            final Random random = new Random();
            if (chave.getItemMeta().getDisplayName().equals(is.getItemMeta().getDisplayName())) {
                final int newAmount = is.getAmount() - amount;
                if (newAmount > 0) {
                    if (random.nextBoolean() == true) {
                        is.setAmount(newAmount);
                        has = true;
                        break;
                    } else {
                        is.setAmount(newAmount);
                        has = false;
                        break;
                    }
                } else {
                    if (random.nextBoolean() == true) {
                        inventory.clear(slot);
                        amount = -newAmount;
                        has = true;
                        if (amount == 0) {
                            break;
                        }
                    } else {
                        inventory.clear(slot);
                        amount = -newAmount;
                        has = false;
                        if (amount == 0) {
                            break;
                        }
                    }
                }
            }
        }

        return has;
    }

    @EventHandler
    public void onClickInventory(final InventoryInteractEvent event) {
        if (event.getInventory().getName().equalsIgnoreCase("Caixa misteriosa")) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onClickCancel(final InventoryClickEvent event) {
        if (event.getClickedInventory() != null && event.getClickedInventory().getName() != null) {
            if (event.getClickedInventory().getName().equalsIgnoreCase("Caixa misteriosa")) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPlace(final BlockPlaceEvent event) {
        if (event.getBlock().getType().equals(Material.TRIPWIRE_HOOK)) {
            if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.GREEN + "Chave")) {
                event.setCancelled(true);
            }
        }
        if (event.getBlock().getType().equals(Material.CHEST) || event.getBlock().getType().equals(Material.ENDER_CHEST) || event.getBlock().getType().equals(Material.SPAWNER) || event.getBlock().getType().equals(Material.END_PORTAL_FRAME)) {
            if (event.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.GREEN + "Caixa misteriosa")) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onOpen(final PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (event.getItem() != null && !event.getItem().getType().equals(Material.AIR)) {
                if (event.getItem().getType().equals(Material.CHEST) || event.getItem().getType().equals(Material.ENDER_CHEST) || event.getItem().getType().equals(Material.SPAWNER) || event.getItem().getType().equals(Material.END_PORTAL_FRAME)) {
                    if (event.getItem().getItemMeta().getDisplayName().equals(ChatColor.GREEN + "Caixa misteriosa")) {

                        if (event.getItem().getItemMeta().getLore().get(1).substring(10).equals("Armas")) {
                            player.sendMessage(ChatColor.RED + " * Temporariamente desabilitado!");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            return;
                        }

                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                if (PlayerHasOpenCaixa.get(UUID) == null) {
                                    new PlayerHasOpenCaixa(UUID, false).insert();
                                } else {
                                    PlayerHasOpenCaixa.get(UUID).setStatus(false);
                                }
                            }
                        }, 200L);

                        if (PlayerHasOpenCaixa.get(UUID) != null && PlayerHasOpenCaixa.get(UUID).getStatus() == false) {
                            if (EventPlayerOpenCaixa.getContainsKey(player.getInventory()) == true) {
                                if (EventPlayerOpenCaixa.tryThis(player.getInventory())) {
                                    PlayerHasOpenCaixa.get(UUID).setStatus(true);

                                    if (ProvisionalError.get(UUID) == null) {
                                        new ProvisionalError(UUID, true).insert();
                                    } else {
                                        ProvisionalError.get(UUID).setStatus(true);
                                    }

                                    final int rotacaoParticles = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                                        double t;
                                        final double r = 2;

                                        @Override
                                        public void run() {
                                            final Location loc = player.getLocation();
                                            this.t = this.t + Math.PI / 16;
                                            final double x = this.r * Math.cos(this.t);
                                            final double y = 0.15 * this.t;
                                            final double z = this.r * Math.sin(this.t);
                                            loc.add(x, y, z);
                                            player.spawnParticle(Particle.PORTAL, loc, 1);

                                            for (final Player target : Bukkit.getOnlinePlayers()) {
                                                if (!player.getName().equals(target.getName()) && player.getWorld().equals(target.getLocation().getWorld()) && target.getLocation().distance(player.getLocation()) <= 50 && Bukkit.getOnlinePlayers().size() > 1) {
                                                    player.spawnParticle(Particle.PORTAL, loc, 10);
                                                }
                                            }

                                            loc.subtract(x, y, z);
                                        }
                                    }, 0L, 1L);

                                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                        @Override
                                        public void run() {
                                            Bukkit.getScheduler().cancelTask(rotacaoParticles);
                                        }
                                    }, 120L);

                                    if (event.getItem().getItemMeta().getLore().get(1).substring(10).equals("Básica")) {
                                        final ItemStack caixa = new ItemStack(Material.CHEST);
                                        final ItemMeta metaCaixa = caixa.getItemMeta();
                                        metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
                                        final List<String> lore = new ArrayList<String>();
                                        lore.add(" ");
                                        lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Básica");
                                        lore.add(" ");
                                        metaCaixa.setLore(lore);
                                        caixa.setItemMeta(metaCaixa);
                                        if (RemoveItemInv.removeItemDifferentLoreBoolean(player.getInventory(), caixa, 1) == false) {
                                            final ItemStack caixa2 = new ItemStack(Material.CHEST);
                                            final ItemMeta metaCaixa2 = caixa2.getItemMeta();
                                            metaCaixa2.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
                                            final List<String> lore2 = new ArrayList<String>();
                                            lore2.add(" ");
                                            lore2.add(ChatColor.YELLOW + "Tipo: " + ChatColor.DARK_GRAY + "Básica");
                                            lore2.add(" ");
                                            metaCaixa2.setLore(lore2);
                                            caixa2.setItemMeta(metaCaixa2);
                                            RemoveItemInv.removeItemDifferentLore(player.getInventory(), caixa2, 1);
                                        }

                                        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Caixa misteriosa");
                                        final Inventory inventoryfinish = Bukkit.createInventory(null, 3 * 9, "Caixa misteriosa");

                                        final Random random = new Random();

                                        final int rotacao = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                //CORES
                                                for (int i = 1; i <= 20; i++) {
                                                    if (i == 1) {
                                                        final int numeroRandom = random.nextInt(15);
                                                        final ItemStack vidro = new ItemStack(Material.LEGACY_STAINED_GLASS_PANE, 1, (short) numeroRandom);
                                                        final ItemMeta metaVidro = vidro.getItemMeta();
                                                        metaVidro.setDisplayName(" ");
                                                        vidro.setItemMeta(metaVidro);

                                                        inventory.setItem(0, vidro);
                                                        EventPlayerOpenCaixa.this.corAnterior = numeroRandom;
                                                        EventPlayerOpenCaixa.this.slotCores++;
                                                    } else {
                                                        int numeroRandom = random.nextInt(15);
                                                        while (numeroRandom == EventPlayerOpenCaixa.this.corAnterior) {
                                                            numeroRandom = random.nextInt(15);
                                                        }
                                                        final ItemStack vidro = new ItemStack(Material.LEGACY_STAINED_GLASS_PANE, 1, (short) numeroRandom);
                                                        final ItemMeta metaVidro = vidro.getItemMeta();
                                                        metaVidro.setDisplayName(" ");
                                                        vidro.setItemMeta(metaVidro);

                                                        if (EventPlayerOpenCaixa.this.slotCores <= 9) {
                                                            inventory.setItem(EventPlayerOpenCaixa.this.slotCores, vidro);
                                                        } else if (EventPlayerOpenCaixa.this.slotCores >= 9) {
                                                            inventory.setItem(EventPlayerOpenCaixa.this.slotCores + 7, vidro);
                                                        }
                                                        EventPlayerOpenCaixa.this.corAnterior = numeroRandom;
                                                        EventPlayerOpenCaixa.this.slotCores++;
                                                    }
                                                }
                                                // -------
                                                EventPlayerOpenCaixa.this.slotCores = 0;
                                                EventPlayerOpenCaixa.this.corAnterior = 0;
                                                //ITENS
                                                for (int i = 1; i <= 7; i++) {
                                                    if (i == 1) {
                                                        final int numeroRandom = random.nextInt(EventPlayerOpenCaixa.getListCaixa("básica").size());
                                                        final ItemStack item = EventPlayerOpenCaixa.getListCaixa("básica").get(numeroRandom);

                                                        inventory.setItem(10, item);
                                                        EventPlayerOpenCaixa.this.itemAnterior = numeroRandom;
                                                        EventPlayerOpenCaixa.this.slotItens++;
                                                    } else {
                                                        int numeroRandom = random.nextInt(EventPlayerOpenCaixa.getListCaixa("básica").size());
                                                        while (numeroRandom == EventPlayerOpenCaixa.this.itemAnterior) {
                                                            numeroRandom = random.nextInt(EventPlayerOpenCaixa.getListCaixa("básica").size());
                                                        }
                                                        final ItemStack item = EventPlayerOpenCaixa.getListCaixa("básica").get(numeroRandom);

                                                        inventory.setItem(EventPlayerOpenCaixa.this.slotItens + 10, item);
                                                        EventPlayerOpenCaixa.this.itemAnterior = numeroRandom;
                                                        EventPlayerOpenCaixa.this.slotItens++;
                                                    }
                                                }
                                                //-------
                                                EventPlayerOpenCaixa.this.slotItens = 0;
                                                EventPlayerOpenCaixa.this.itemAnterior = 0;

                                                if (ProvisionalError.get(UUID).getStatus() == true) {
                                                    player.openInventory(inventory);
                                                    ProvisionalError.get(UUID).setStatus(false);
                                                }
                                                if (player.getOpenInventory() != null && player.getOpenInventory().getTopInventory().getName().equalsIgnoreCase("Caixa misteriosa")) {
                                                    player.openInventory(inventory);
                                                }
                                                player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1F, 1F);
                                            }
                                        }, 0L, 10L);

                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                Bukkit.getScheduler().cancelTask(rotacao);

                                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1F, 1F);

                                                //ITEM SORTIDO
                                                final int itemID = EventPlayerOpenCaixa.getItemRandom("básica");
                                                final ItemStack itemGanho = EventPlayerOpenCaixa.getListCaixa("básica").get(itemID);
                                                if (ItemGanhoUtil.get(UUID) == null) {
                                                    new ItemGanhoUtil(UUID, itemID, itemGanho).insert();
                                                } else {
                                                    ItemGanhoUtil.get(UUID).setItemID(itemID);
                                                    ItemGanhoUtil.get(UUID).setItem(itemGanho);
                                                }

                                                for (int i = 1; i <= 7; i++) {
                                                    inventoryfinish.setItem(9 + i, itemGanho);
                                                }
                                                // -------

                                                if (player.getOpenInventory() != null && player.getOpenInventory().getTopInventory().getName().equalsIgnoreCase("Caixa misteriosa")) {
                                                    player.openInventory(inventoryfinish);
                                                }
                                            }
                                        }, 135L);

                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                PlayerHasOpenCaixa.get(UUID).setStatus(false);
                                                if (player.getOpenInventory() != null && player.getOpenInventory().getTopInventory().getName().equalsIgnoreCase("Caixa misteriosa")) {
                                                    player.getOpenInventory().close();
                                                }
                                                final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(ItemGanhoUtil.get(UUID).getItem());
                                                for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                                    player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                                                }

                                                if (EventPlayerOpenCaixa.getChance("básica").get(ItemGanhoUtil.get(UUID).getItemID()) <= 2) {
                                                    player.sendTitle(ChatColor.GOLD + "ULTRA RARO", ChatColor.DARK_GRAY + "Parabéns!", 20, 20, 20);
                                                    player.sendMessage(AlternateColor.alternate("\n \n&a * Você acabou de ganhar um item &6&nULTRA RARO!&a\n \n"));
                                                    player.getWorld().strikeLightningEffect(player.getLocation());
                                                    final Firework f = player.getWorld().spawn(player.getLocation(), Firework.class);
                                                    final FireworkMeta fm = f.getFireworkMeta();
                                                    fm.addEffect(FireworkEffect.builder()
                                                            .flicker(true)
                                                            .trail(true)
                                                            .with(Type.BALL_LARGE)
                                                            .withColor(Color.ORANGE)
                                                            .withFade(Color.YELLOW)
                                                            .build());
                                                    fm.setPower(2);
                                                    f.setFireworkMeta(fm);
                                                    final Firework f2 = player.getWorld().spawn(player.getLocation(), Firework.class);
                                                    final FireworkMeta fm2 = f2.getFireworkMeta();
                                                    fm2.addEffect(FireworkEffect.builder()
                                                            .flicker(true)
                                                            .trail(true)
                                                            .with(Type.BALL)
                                                            .withColor(Color.ORANGE)
                                                            .withFade(Color.YELLOW)
                                                            .build());
                                                    fm2.setPower(1);
                                                    f2.setFireworkMeta(fm2);

                                                    for (final Player target : Bukkit.getOnlinePlayers()) {
                                                        if (!target.getName().equals(player.getName())) {
                                                            //mb.send(target);

                                                            target.sendMessage(AlternateColor.alternate("\n \n&a * " + player.getName() + " acabou de ganhar um item &6&nULTRA RARO!&a\n \n"));
                                                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                                        }
                                                    }
                                                }
                                                if (EventPlayerOpenCaixa.getChance("básica").get(ItemGanhoUtil.get(UUID).getItemID()) > 2 && EventPlayerOpenCaixa.getChance("básica").get(ItemGanhoUtil.get(UUID).getItemID()) <= 4) {
                                                    player.sendMessage(AlternateColor.alternate("\n \n&a * Você acabou de ganhar um item &6&nRARO!&a\n \n"));

                                                    player.getWorld().strikeLightningEffect(player.getLocation());
                                                }
                                                if (EventPlayerOpenCaixa.getChance("básica").get(ItemGanhoUtil.get(UUID).getItemID()) > 4 && EventPlayerOpenCaixa.getChance("básica").get(ItemGanhoUtil.get(UUID).getItemID()) < 8) {
                                                    player.sendMessage(AlternateColor.alternate("\n \n&a * Você acabou de ganhar um item &5&nLENDARIO!&a\n \n"));
                                                    player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                                }
                                                if (EventPlayerOpenCaixa.getChance("básica").get(ItemGanhoUtil.get(UUID).getItemID()) >= 8) {
                                                    player.sendMessage(AlternateColor.alternate("\n \n&a * Você acabou de ganhar um item &8&nCOMUM!&a\n \n"));
                                                    player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                                }

                                                ItemGanhoUtil.get(UUID).setItem(null);
                                                ItemGanhoUtil.get(UUID).setItemID(0);
                                            }
                                        }, 180L);
                                    } else if (event.getItem().getItemMeta().getLore().get(1).substring(10).equals("Avançada")) {
                                        final ItemStack caixa = new ItemStack(Material.ENDER_CHEST);
                                        final ItemMeta metaCaixa = caixa.getItemMeta();
                                        metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
                                        final List<String> lore = new ArrayList<String>();
                                        lore.add(" ");
                                        lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Avançada");
                                        lore.add(" ");
                                        metaCaixa.setLore(lore);
                                        caixa.setItemMeta(metaCaixa);
                                        if (RemoveItemInv.removeItemDifferentLoreBoolean(player.getInventory(), caixa, 1) == false) {
                                            final ItemStack caixa2 = new ItemStack(Material.ENDER_CHEST);
                                            final ItemMeta metaCaixa2 = caixa2.getItemMeta();
                                            metaCaixa2.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
                                            final List<String> lore2 = new ArrayList<String>();
                                            lore2.add(" ");
                                            lore2.add(ChatColor.YELLOW + "Tipo: " + ChatColor.DARK_GRAY + "Avançada");
                                            lore2.add(" ");
                                            metaCaixa2.setLore(lore2);
                                            caixa2.setItemMeta(metaCaixa2);
                                            RemoveItemInv.removeItemDifferentLore(player.getInventory(), caixa2, 1);
                                        }

                                        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Caixa misteriosa");
                                        final Inventory inventoryfinish = Bukkit.createInventory(null, 3 * 9, "Caixa misteriosa");

                                        final Random random = new Random();

                                        final int rotacao = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                //CORES
                                                for (int i = 1; i <= 20; i++) {
                                                    if (i == 1) {
                                                        final int numeroRandom = random.nextInt(15);
                                                        final ItemStack vidro = new ItemStack(Material.LEGACY_STAINED_GLASS_PANE, 1, (short) numeroRandom);
                                                        final ItemMeta metaVidro = vidro.getItemMeta();
                                                        metaVidro.setDisplayName(" ");
                                                        vidro.setItemMeta(metaVidro);

                                                        inventory.setItem(0, vidro);
                                                        EventPlayerOpenCaixa.this.corAnterior = numeroRandom;
                                                        EventPlayerOpenCaixa.this.slotCores++;
                                                    } else {
                                                        int numeroRandom = random.nextInt(15);
                                                        while (numeroRandom == EventPlayerOpenCaixa.this.corAnterior) {
                                                            numeroRandom = random.nextInt(15);
                                                        }
                                                        final ItemStack vidro = new ItemStack(Material.LEGACY_STAINED_GLASS_PANE, 1, (short) numeroRandom);
                                                        final ItemMeta metaVidro = vidro.getItemMeta();
                                                        metaVidro.setDisplayName(" ");
                                                        vidro.setItemMeta(metaVidro);

                                                        if (EventPlayerOpenCaixa.this.slotCores <= 9) {
                                                            inventory.setItem(EventPlayerOpenCaixa.this.slotCores, vidro);
                                                        } else if (EventPlayerOpenCaixa.this.slotCores >= 9) {
                                                            inventory.setItem(EventPlayerOpenCaixa.this.slotCores + 7, vidro);
                                                        }
                                                        EventPlayerOpenCaixa.this.corAnterior = numeroRandom;
                                                        EventPlayerOpenCaixa.this.slotCores++;
                                                    }
                                                }
                                                // -------
                                                EventPlayerOpenCaixa.this.slotCores = 0;
                                                EventPlayerOpenCaixa.this.corAnterior = 0;
                                                //ITENS
                                                for (int i = 1; i <= 7; i++) {
                                                    if (i == 1) {
                                                        final int numeroRandom = random.nextInt(EventPlayerOpenCaixa.getListCaixa("avançada").size());
                                                        final ItemStack item = EventPlayerOpenCaixa.getListCaixa("avançada").get(numeroRandom);

                                                        inventory.setItem(10, item);
                                                        EventPlayerOpenCaixa.this.itemAnterior = numeroRandom;
                                                        EventPlayerOpenCaixa.this.slotItens++;
                                                    } else {
                                                        int numeroRandom = random.nextInt(EventPlayerOpenCaixa.getListCaixa("avançada").size());
                                                        while (numeroRandom == EventPlayerOpenCaixa.this.itemAnterior) {
                                                            numeroRandom = random.nextInt(EventPlayerOpenCaixa.getListCaixa("avançada").size());
                                                        }
                                                        final ItemStack item = EventPlayerOpenCaixa.getListCaixa("avançada").get(numeroRandom);

                                                        inventory.setItem(EventPlayerOpenCaixa.this.slotItens + 10, item);
                                                        EventPlayerOpenCaixa.this.itemAnterior = numeroRandom;
                                                        EventPlayerOpenCaixa.this.slotItens++;
                                                    }
                                                }
                                                //-------
                                                EventPlayerOpenCaixa.this.slotItens = 0;
                                                EventPlayerOpenCaixa.this.itemAnterior = 0;

                                                if (ProvisionalError.get(UUID).getStatus() == true) {
                                                    player.openInventory(inventory);
                                                    ProvisionalError.get(UUID).setStatus(false);
                                                }
                                                if (player.getOpenInventory() != null && player.getOpenInventory().getTopInventory().getName().equalsIgnoreCase("Caixa misteriosa")) {
                                                    player.openInventory(inventory);
                                                }
                                                player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1F, 1F);
                                            }
                                        }, 0L, 10L);

                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                Bukkit.getScheduler().cancelTask(rotacao);

                                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1F, 1F);

                                                //ITEM SORTIDO
                                                final int itemID = EventPlayerOpenCaixa.getItemRandom("avançada");
                                                final ItemStack itemGanho = EventPlayerOpenCaixa.getListCaixa("avançada").get(itemID);
                                                if (ItemGanhoUtil.get(UUID) == null) {
                                                    new ItemGanhoUtil(UUID, itemID, itemGanho).insert();
                                                } else {
                                                    ItemGanhoUtil.get(UUID).setItemID(itemID);
                                                    ItemGanhoUtil.get(UUID).setItem(itemGanho);
                                                }

                                                for (int i = 1; i <= 7; i++) {
                                                    inventoryfinish.setItem(9 + i, itemGanho);
                                                }
                                                // -------

                                                if (player.getOpenInventory() != null && player.getOpenInventory().getTopInventory().getName().equalsIgnoreCase("Caixa misteriosa")) {
                                                    player.openInventory(inventoryfinish);
                                                }
                                            }
                                        }, 135L);

                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                PlayerHasOpenCaixa.get(UUID).setStatus(false);
                                                if (player.getOpenInventory() != null && player.getOpenInventory().getTopInventory().getName().equalsIgnoreCase("Caixa misteriosa")) {
                                                    player.getOpenInventory().close();
                                                }
                                                final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(ItemGanhoUtil.get(UUID).getItem());
                                                for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                                    player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                                                }

                                                if (EventPlayerOpenCaixa.getChance("avançada").get(ItemGanhoUtil.get(UUID).getItemID()) <= 2) {
                                                    player.sendTitle(ChatColor.GOLD + "ULTRA RARO", ChatColor.DARK_GRAY + "Parabéns!", 20, 20, 20);
                                                    player.sendMessage(AlternateColor.alternate("\n \n&a * Você acabou de ganhar um item &6&nULTRA RARO!&a\n \n"));
                                                    player.getWorld().strikeLightningEffect(player.getLocation());
                                                    final Firework f = player.getWorld().spawn(player.getLocation(), Firework.class);
                                                    final FireworkMeta fm = f.getFireworkMeta();
                                                    fm.addEffect(FireworkEffect.builder()
                                                            .flicker(true)
                                                            .trail(true)
                                                            .with(Type.BALL_LARGE)
                                                            .withColor(Color.ORANGE)
                                                            .withFade(Color.YELLOW)
                                                            .build());
                                                    fm.setPower(2);
                                                    f.setFireworkMeta(fm);
                                                    final Firework f2 = player.getWorld().spawn(player.getLocation(), Firework.class);
                                                    final FireworkMeta fm2 = f2.getFireworkMeta();
                                                    fm2.addEffect(FireworkEffect.builder()
                                                            .flicker(true)
                                                            .trail(true)
                                                            .with(Type.BALL)
                                                            .withColor(Color.ORANGE)
                                                            .withFade(Color.YELLOW)
                                                            .build());
                                                    fm2.setPower(1);
                                                    f2.setFireworkMeta(fm2);

                                                    final MessageBuilder mb = new MessageBuilder("\n \n§a * " + player.getName() + " acabou de ganhar um item ").color(ChatColor.GOLD).append("ULTRA RARO!").item(ItemGanhoUtil.get(UUID).getItem());

                                                    for (final Player target : Bukkit.getOnlinePlayers()) {
                                                        if (!target.getName().equals(player.getName())) {
                                                            //mb.send(target);

                                                            target.sendMessage(AlternateColor.alternate("\n \n&a * " + player.getName() + " acabou de ganhar um item &6&nULTRA RARO!&a\n \n"));
                                                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                                        }
                                                    }
                                                }
                                                if (EventPlayerOpenCaixa.getChance("avançada").get(ItemGanhoUtil.get(UUID).getItemID()) > 2 && EventPlayerOpenCaixa.getChance("avançada").get(ItemGanhoUtil.get(UUID).getItemID()) <= 4) {
                                                    player.sendMessage(AlternateColor.alternate("\n \n&a * Você acabou de ganhar um item &6&nRARO!&a\n \n"));
                                                    player.getWorld().strikeLightningEffect(player.getLocation());
                                                }
                                                if (EventPlayerOpenCaixa.getChance("avançada").get(ItemGanhoUtil.get(UUID).getItemID()) > 4 && EventPlayerOpenCaixa.getChance("avançada").get(ItemGanhoUtil.get(UUID).getItemID()) < 8) {
                                                    player.sendMessage(AlternateColor.alternate("\n \n&a * Você acabou de ganhar um item &5&nLENDARIO!&a\n \n"));
                                                    player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                                }
                                                if (EventPlayerOpenCaixa.getChance("avançada").get(ItemGanhoUtil.get(UUID).getItemID()) >= 8) {
                                                    player.sendMessage(AlternateColor.alternate("\n \n&a * Você acabou de ganhar um item &8&nCOMUM!&a\n \n"));
                                                    player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                                }

                                                ItemGanhoUtil.get(UUID).setItem(null);
                                                ItemGanhoUtil.get(UUID).setItemID(0);
                                            }
                                        }, 180L);
                                    } else if (event.getItem().getItemMeta().getLore().get(1).substring(10).equals("Spawners")) {
                                        final ItemStack caixa = new ItemStack(Material.SPAWNER);
                                        final ItemMeta metaCaixa = caixa.getItemMeta();
                                        metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
                                        final List<String> lore = new ArrayList<String>();
                                        lore.add(" ");
                                        lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Spawners");
                                        lore.add(" ");
                                        metaCaixa.setLore(lore);
                                        caixa.setItemMeta(metaCaixa);
                                        if (RemoveItemInv.removeItemDifferentLoreBoolean(player.getInventory(), caixa, 1) == false) {
                                            final ItemStack caixa2 = new ItemStack(Material.SPAWNER);
                                            final ItemMeta metaCaixa2 = caixa2.getItemMeta();
                                            metaCaixa2.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
                                            final List<String> lore2 = new ArrayList<String>();
                                            lore2.add(" ");
                                            lore2.add(ChatColor.YELLOW + "Tipo: " + ChatColor.DARK_GRAY + "Spawners");
                                            lore2.add(" ");
                                            metaCaixa2.setLore(lore2);
                                            caixa2.setItemMeta(metaCaixa2);
                                            RemoveItemInv.removeItemDifferentLore(player.getInventory(), caixa2, 1);
                                        }

                                        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Caixa misteriosa");
                                        final Inventory inventoryfinish = Bukkit.createInventory(null, 3 * 9, "Caixa misteriosa");

                                        final Random random = new Random();

                                        final int rotacao = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                //CORES
                                                for (int i = 1; i <= 20; i++) {
                                                    if (i == 1) {
                                                        final int numeroRandom = random.nextInt(15);
                                                        final ItemStack vidro = new ItemStack(Material.LEGACY_STAINED_GLASS_PANE, 1, (short) numeroRandom);
                                                        final ItemMeta metaVidro = vidro.getItemMeta();
                                                        metaVidro.setDisplayName(" ");
                                                        vidro.setItemMeta(metaVidro);

                                                        inventory.setItem(0, vidro);
                                                        EventPlayerOpenCaixa.this.corAnterior = numeroRandom;
                                                        EventPlayerOpenCaixa.this.slotCores++;
                                                    } else {
                                                        int numeroRandom = random.nextInt(15);
                                                        while (numeroRandom == EventPlayerOpenCaixa.this.corAnterior) {
                                                            numeroRandom = random.nextInt(15);
                                                        }
                                                        final ItemStack vidro = new ItemStack(Material.LEGACY_STAINED_GLASS_PANE, 1, (short) numeroRandom);
                                                        final ItemMeta metaVidro = vidro.getItemMeta();
                                                        metaVidro.setDisplayName(" ");
                                                        vidro.setItemMeta(metaVidro);

                                                        if (EventPlayerOpenCaixa.this.slotCores <= 9) {
                                                            inventory.setItem(EventPlayerOpenCaixa.this.slotCores, vidro);
                                                        } else if (EventPlayerOpenCaixa.this.slotCores >= 9) {
                                                            inventory.setItem(EventPlayerOpenCaixa.this.slotCores + 7, vidro);
                                                        }
                                                        EventPlayerOpenCaixa.this.corAnterior = numeroRandom;
                                                        EventPlayerOpenCaixa.this.slotCores++;
                                                    }
                                                }
                                                // -------
                                                EventPlayerOpenCaixa.this.slotCores = 0;
                                                EventPlayerOpenCaixa.this.corAnterior = 0;
                                                //ITENS
                                                for (int i = 1; i <= 7; i++) {
                                                    if (i == 1) {
                                                        final int numeroRandom = random.nextInt(EventPlayerOpenCaixa.getListCaixa("spawners").size());
                                                        final ItemStack item = EventPlayerOpenCaixa.getListCaixa("spawners").get(numeroRandom);

                                                        inventory.setItem(10, item);
                                                        EventPlayerOpenCaixa.this.itemAnterior = numeroRandom;
                                                        EventPlayerOpenCaixa.this.slotItens++;
                                                    } else {
                                                        int numeroRandom = random.nextInt(EventPlayerOpenCaixa.getListCaixa("spawners").size());
                                                        while (numeroRandom == EventPlayerOpenCaixa.this.itemAnterior) {
                                                            numeroRandom = random.nextInt(EventPlayerOpenCaixa.getListCaixa("spawners").size());
                                                        }
                                                        final ItemStack item = EventPlayerOpenCaixa.getListCaixa("spawners").get(numeroRandom);

                                                        inventory.setItem(EventPlayerOpenCaixa.this.slotItens + 10, item);
                                                        EventPlayerOpenCaixa.this.itemAnterior = numeroRandom;
                                                        EventPlayerOpenCaixa.this.slotItens++;
                                                    }
                                                }
                                                //-------
                                                EventPlayerOpenCaixa.this.slotItens = 0;
                                                EventPlayerOpenCaixa.this.itemAnterior = 0;

                                                if (ProvisionalError.get(UUID).getStatus() == true) {
                                                    player.openInventory(inventory);
                                                    ProvisionalError.get(UUID).setStatus(false);
                                                }
                                                if (player.getOpenInventory() != null && player.getOpenInventory().getTopInventory().getName().equalsIgnoreCase("Caixa misteriosa")) {
                                                    player.openInventory(inventory);
                                                }
                                                player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1F, 1F);
                                            }
                                        }, 0L, 10L);

                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                Bukkit.getScheduler().cancelTask(rotacao);

                                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1F, 1F);

                                                //ITEM SORTIDO
                                                final int itemID = EventPlayerOpenCaixa.getItemRandom("spawners");
                                                final ItemStack itemGanho = EventPlayerOpenCaixa.getListCaixa("spawners").get(itemID);
                                                if (ItemGanhoUtil.get(UUID) == null) {
                                                    new ItemGanhoUtil(UUID, itemID, itemGanho).insert();
                                                } else {
                                                    ItemGanhoUtil.get(UUID).setItemID(itemID);
                                                    ItemGanhoUtil.get(UUID).setItem(itemGanho);
                                                }

                                                for (int i = 1; i <= 7; i++) {
                                                    inventoryfinish.setItem(9 + i, itemGanho);
                                                }
                                                // -------

                                                if (player.getOpenInventory() != null && player.getOpenInventory().getTopInventory().getName().equalsIgnoreCase("Caixa misteriosa")) {
                                                    player.openInventory(inventoryfinish);
                                                }
                                            }
                                        }, 135L);

                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                PlayerHasOpenCaixa.get(UUID).setStatus(false);
                                                if (player.getOpenInventory() != null && player.getOpenInventory().getTopInventory().getName().equalsIgnoreCase("Caixa misteriosa")) {
                                                    player.getOpenInventory().close();
                                                }
                                                final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(ItemGanhoUtil.get(UUID).getItem());
                                                for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                                    player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                                                }

                                                if (EventPlayerOpenCaixa.getChance("spawners").get(ItemGanhoUtil.get(UUID).getItemID()) <= 1) {
                                                    player.sendTitle(ChatColor.GOLD + "ULTRA RARO", ChatColor.DARK_GRAY + "Parabéns!", 20, 20, 20);
                                                    player.sendMessage(AlternateColor.alternate("\n \n&a * Você acabou de ganhar um item &6&nULTRA RARO!&a\n \n"));
                                                    player.getWorld().strikeLightningEffect(player.getLocation());
                                                    final Firework f = player.getWorld().spawn(player.getLocation(), Firework.class);
                                                    final FireworkMeta fm = f.getFireworkMeta();
                                                    fm.addEffect(FireworkEffect.builder()
                                                            .flicker(true)
                                                            .trail(true)
                                                            .with(Type.BALL_LARGE)
                                                            .withColor(Color.ORANGE)
                                                            .withFade(Color.YELLOW)
                                                            .build());
                                                    fm.setPower(2);
                                                    f.setFireworkMeta(fm);
                                                    final Firework f2 = player.getWorld().spawn(player.getLocation(), Firework.class);
                                                    final FireworkMeta fm2 = f2.getFireworkMeta();
                                                    fm2.addEffect(FireworkEffect.builder()
                                                            .flicker(true)
                                                            .trail(true)
                                                            .with(Type.BALL)
                                                            .withColor(Color.ORANGE)
                                                            .withFade(Color.YELLOW)
                                                            .build());
                                                    fm2.setPower(1);
                                                    f2.setFireworkMeta(fm2);

                                                    final MessageBuilder mb = new MessageBuilder("\n \n§a * " + player.getName() + " acabou de ganhar um item ").color(ChatColor.GOLD).append("ULTRA RARO!").item(ItemGanhoUtil.get(UUID).getItem());

                                                    for (final Player target : Bukkit.getOnlinePlayers()) {
                                                        if (!target.getName().equals(player.getName())) {
                                                            //mb.send(target);

                                                            target.sendMessage(AlternateColor.alternate("\n \n&a * " + player.getName() + " acabou de ganhar um item &6&nULTRA RARO!&a\n \n"));
                                                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                                        }
                                                    }
                                                }
                                                if (EventPlayerOpenCaixa.getChance("spawners").get(ItemGanhoUtil.get(UUID).getItemID()) > 1 && EventPlayerOpenCaixa.getChance("spawners").get(ItemGanhoUtil.get(UUID).getItemID()) <= 4) {
                                                    player.sendMessage(AlternateColor.alternate("\n \n&a * Você acabou de ganhar um item &6&nRARO!&a\n \n"));
                                                    player.getWorld().strikeLightningEffect(player.getLocation());
                                                }
                                                if (EventPlayerOpenCaixa.getChance("spawners").get(ItemGanhoUtil.get(UUID).getItemID()) > 4 && EventPlayerOpenCaixa.getChance("spawners").get(ItemGanhoUtil.get(UUID).getItemID()) < 7) {
                                                    player.sendMessage(AlternateColor.alternate("\n \n&a * Você acabou de ganhar um item &5&nLENDARIO!&a\n \n"));
                                                    player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                                }
                                                if (EventPlayerOpenCaixa.getChance("spawners").get(ItemGanhoUtil.get(UUID).getItemID()) >= 7) {
                                                    player.sendMessage(AlternateColor.alternate("\n \n&a * Você acabou de ganhar um item &8&nCOMUM!&a\n \n"));
                                                    player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                                }

                                                ItemGanhoUtil.get(UUID).setItem(null);
                                                ItemGanhoUtil.get(UUID).setItemID(0);
                                            }
                                        }, 180L);
                                    } else if (event.getItem().getItemMeta().getLore().get(1).substring(10).equals("Suprema")) {
                                        final ItemStack caixa = new ItemStack(Material.END_PORTAL_FRAME);
                                        final ItemMeta metaCaixa = caixa.getItemMeta();
                                        metaCaixa.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
                                        final List<String> lore = new ArrayList<String>();
                                        lore.add(" ");
                                        lore.add(ChatColor.YELLOW + "Tipo: " + ChatColor.GRAY + "Suprema");
                                        lore.add(" ");
                                        metaCaixa.setLore(lore);
                                        caixa.setItemMeta(metaCaixa);
                                        if (RemoveItemInv.removeItemDifferentLoreBoolean(player.getInventory(), caixa, 1) == false) {
                                            final ItemStack caixa2 = new ItemStack(Material.END_PORTAL_FRAME);
                                            final ItemMeta metaCaixa2 = caixa2.getItemMeta();
                                            metaCaixa2.setDisplayName(ChatColor.GREEN + "Caixa misteriosa");
                                            final List<String> lore2 = new ArrayList<String>();
                                            lore2.add(" ");
                                            lore2.add(ChatColor.YELLOW + "Tipo: " + ChatColor.DARK_GRAY + "Suprema");
                                            lore2.add(" ");
                                            metaCaixa2.setLore(lore2);
                                            caixa2.setItemMeta(metaCaixa2);
                                            RemoveItemInv.removeItemDifferentLore(player.getInventory(), caixa2, 1);
                                        }

                                        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Caixa misteriosa");
                                        final Inventory inventoryfinish = Bukkit.createInventory(null, 3 * 9, "Caixa misteriosa");

                                        final Random random = new Random();

                                        final int rotacao = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                //CORES
                                                for (int i = 1; i <= 20; i++) {
                                                    if (i == 1) {
                                                        final int numeroRandom = random.nextInt(15);
                                                        final ItemStack vidro = new ItemStack(Material.LEGACY_STAINED_GLASS_PANE, 1, (short) numeroRandom);
                                                        final ItemMeta metaVidro = vidro.getItemMeta();
                                                        metaVidro.setDisplayName(" ");
                                                        vidro.setItemMeta(metaVidro);

                                                        inventory.setItem(0, vidro);
                                                        EventPlayerOpenCaixa.this.corAnterior = numeroRandom;
                                                        EventPlayerOpenCaixa.this.slotCores++;
                                                    } else {
                                                        int numeroRandom = random.nextInt(15);
                                                        while (numeroRandom == EventPlayerOpenCaixa.this.corAnterior) {
                                                            numeroRandom = random.nextInt(15);
                                                        }
                                                        final ItemStack vidro = new ItemStack(Material.LEGACY_STAINED_GLASS_PANE, 1, (short) numeroRandom);
                                                        final ItemMeta metaVidro = vidro.getItemMeta();
                                                        metaVidro.setDisplayName(" ");
                                                        vidro.setItemMeta(metaVidro);

                                                        if (EventPlayerOpenCaixa.this.slotCores <= 9) {
                                                            inventory.setItem(EventPlayerOpenCaixa.this.slotCores, vidro);
                                                        } else if (EventPlayerOpenCaixa.this.slotCores >= 9) {
                                                            inventory.setItem(EventPlayerOpenCaixa.this.slotCores + 7, vidro);
                                                        }
                                                        EventPlayerOpenCaixa.this.corAnterior = numeroRandom;
                                                        EventPlayerOpenCaixa.this.slotCores++;
                                                    }
                                                }
                                                // -------
                                                EventPlayerOpenCaixa.this.slotCores = 0;
                                                EventPlayerOpenCaixa.this.corAnterior = 0;
                                                //ITENS
                                                for (int i = 1; i <= 7; i++) {
                                                    if (i == 1) {
                                                        final int numeroRandom = random.nextInt(EventPlayerOpenCaixa.getListCaixa("suprema").size());
                                                        final ItemStack item = EventPlayerOpenCaixa.getListCaixa("suprema").get(numeroRandom);

                                                        inventory.setItem(10, item);
                                                        EventPlayerOpenCaixa.this.itemAnterior = numeroRandom;
                                                        EventPlayerOpenCaixa.this.slotItens++;
                                                    } else {
                                                        int numeroRandom = random.nextInt(EventPlayerOpenCaixa.getListCaixa("suprema").size());
                                                        while (numeroRandom == EventPlayerOpenCaixa.this.itemAnterior) {
                                                            numeroRandom = random.nextInt(EventPlayerOpenCaixa.getListCaixa("suprema").size());
                                                        }
                                                        final ItemStack item = EventPlayerOpenCaixa.getListCaixa("suprema").get(numeroRandom);

                                                        inventory.setItem(EventPlayerOpenCaixa.this.slotItens + 10, item);
                                                        EventPlayerOpenCaixa.this.itemAnterior = numeroRandom;
                                                        EventPlayerOpenCaixa.this.slotItens++;
                                                    }
                                                }
                                                //-------
                                                EventPlayerOpenCaixa.this.slotItens = 0;
                                                EventPlayerOpenCaixa.this.itemAnterior = 0;

                                                if (ProvisionalError.get(UUID).getStatus() == true) {
                                                    player.openInventory(inventory);
                                                    ProvisionalError.get(UUID).setStatus(false);
                                                }
                                                if (player.getOpenInventory() != null && player.getOpenInventory().getTopInventory().getName().equalsIgnoreCase("Caixa misteriosa")) {
                                                    player.openInventory(inventory);
                                                }
                                                player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1F, 1F);
                                            }
                                        }, 0L, 10L);

                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                Bukkit.getScheduler().cancelTask(rotacao);

                                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1F, 1F);

                                                //ITEM SORTIDO
                                                final int itemID = EventPlayerOpenCaixa.getItemRandom("suprema");
                                                final ItemStack itemGanho = EventPlayerOpenCaixa.getListCaixa("suprema").get(itemID);
                                                if (ItemGanhoUtil.get(UUID) == null) {
                                                    new ItemGanhoUtil(UUID, itemID, itemGanho).insert();
                                                } else {
                                                    ItemGanhoUtil.get(UUID).setItemID(itemID);
                                                    ItemGanhoUtil.get(UUID).setItem(itemGanho);
                                                }

                                                for (int i = 1; i <= 7; i++) {
                                                    inventoryfinish.setItem(9 + i, itemGanho);
                                                }
                                                // -------

                                                if (player.getOpenInventory() != null && player.getOpenInventory().getTopInventory().getName().equalsIgnoreCase("Caixa misteriosa")) {
                                                    player.openInventory(inventoryfinish);
                                                }
                                            }
                                        }, 135L);

                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                PlayerHasOpenCaixa.get(UUID).setStatus(false);
                                                if (player.getOpenInventory() != null && player.getOpenInventory().getTopInventory().getName().equalsIgnoreCase("Caixa misteriosa")) {
                                                    player.getOpenInventory().close();
                                                }
                                                final HashMap<Integer, ItemStack> nope = player.getInventory().addItem(ItemGanhoUtil.get(UUID).getItem());
                                                for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                                                    player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                                                }

                                                if (EventPlayerOpenCaixa.getChance("suprema").get(ItemGanhoUtil.get(UUID).getItemID()) <= 1) {
                                                    player.sendTitle(ChatColor.GOLD + "ULTRA RARO", ChatColor.DARK_GRAY + "Parabéns!", 20, 20, 20);
                                                    player.sendMessage(AlternateColor.alternate("\n \n&a * Você acabou de ganhar um item &6&nULTRA RARO!&a\n \n"));
                                                    player.getWorld().strikeLightningEffect(player.getLocation());
                                                    final Firework f = player.getWorld().spawn(player.getLocation(), Firework.class);
                                                    final FireworkMeta fm = f.getFireworkMeta();
                                                    fm.addEffect(FireworkEffect.builder()
                                                            .flicker(true)
                                                            .trail(true)
                                                            .with(Type.BALL_LARGE)
                                                            .withColor(Color.ORANGE)
                                                            .withFade(Color.YELLOW)
                                                            .build());
                                                    fm.setPower(2);
                                                    f.setFireworkMeta(fm);
                                                    final Firework f2 = player.getWorld().spawn(player.getLocation(), Firework.class);
                                                    final FireworkMeta fm2 = f2.getFireworkMeta();
                                                    fm2.addEffect(FireworkEffect.builder()
                                                            .flicker(true)
                                                            .trail(true)
                                                            .with(Type.BALL)
                                                            .withColor(Color.ORANGE)
                                                            .withFade(Color.YELLOW)
                                                            .build());
                                                    fm2.setPower(1);
                                                    f2.setFireworkMeta(fm2);

                                                    final MessageBuilder mb = new MessageBuilder("\n \n§a * " + player.getName() + " acabou de ganhar um item ").color(ChatColor.GOLD).append("ULTRA RARO!").item(ItemGanhoUtil.get(UUID).getItem());

                                                    for (final Player target : Bukkit.getOnlinePlayers()) {
                                                        if (!target.getName().equals(player.getName())) {
                                                            //mb.send(target);

                                                            target.sendMessage(AlternateColor.alternate("\n \n&a * " + player.getName() + " acabou de ganhar um item &6&nULTRA RARO!&a\n \n"));
                                                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                                        }
                                                    }
                                                }
                                                if (EventPlayerOpenCaixa.getChance("suprema").get(ItemGanhoUtil.get(UUID).getItemID()) > 2 && EventPlayerOpenCaixa.getChance("suprema").get(ItemGanhoUtil.get(UUID).getItemID()) <= 4) {
                                                    player.sendMessage(AlternateColor.alternate("\n \n&a * Você acabou de ganhar um item &6&nRARO!&a\n \n"));
                                                    player.getWorld().strikeLightningEffect(player.getLocation());
                                                }
                                                if (EventPlayerOpenCaixa.getChance("suprema").get(ItemGanhoUtil.get(UUID).getItemID()) > 4 && EventPlayerOpenCaixa.getChance("suprema").get(ItemGanhoUtil.get(UUID).getItemID()) < 7) {
                                                    player.sendMessage(AlternateColor.alternate("\n \n&a * Você acabou de ganhar um item &5&nLENDARIO!&a\n \n"));
                                                    player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                                }
                                                if (EventPlayerOpenCaixa.getChance("suprema").get(ItemGanhoUtil.get(UUID).getItemID()) >= 7) {
                                                    player.sendMessage(AlternateColor.alternate("\n \n&a * Você acabou de ganhar um item &8&nCOMUM!&a\n \n"));
                                                    player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                                }

                                                ItemGanhoUtil.get(UUID).setItem(null);
                                                ItemGanhoUtil.get(UUID).setItemID(0);
                                            }
                                        }, 180L);
                                    }
                                } else {
                                    player.sendMessage(ChatColor.RED + " * Hm.. Vossa chave falhou ao tentar abrir a caixa D=");
                                    player.playSound(player.getLocation(), Sound.ENTITY_ITEM_BREAK, 1.0f, 1.0f);
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + "Ops, você não contém nenhum chave!");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, você pode abrir apenas uma caixa por vez!");
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onDamage(final EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            final Player player = (Player) event.getEntity();
            final String UUID = PlayerUUID.getUUID(player.getName());

            if (PlayerHasOpenCaixa.get(UUID) != null) {
                if (PlayerHasOpenCaixa.get(UUID).getStatus() == true) {
                    event.setCancelled(true);
                }
            }
        }
    }
}
