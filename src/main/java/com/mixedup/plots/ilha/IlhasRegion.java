package com.mixedup.plots.ilha;

import com.mixedup.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class IlhasRegion {

    private static List<IlhaRegion> regions = new ArrayList<>();

    public static IlhaRegion getRegion(final String regionID) {
        for (final IlhaRegion region : IlhasRegion.getRegions()) {
            if (region.getRegionID() == regionID) {
                return region;
            }
        }
        return null;
    }

    public static List<IlhaRegion> getRegions() {
        return IlhasRegion.regions;
    }

    public static void setRegions(final List<IlhaRegion> regions) {
        IlhasRegion.regions = regions;
    }

    public static void enableRegions() {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Ilhas_data");
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                final String pos1 = (1000 * rs.getInt("ID") - 650) + ":" + "0" + ":" + 650;
                final String pos2 = (1000 * rs.getInt("ID") - 350) + ":" + "256" + ":" + 350;
                final IlhaRegion ilha = new IlhaRegion(rs.getString("UUID"), pos1, pos2);
                getRegions().add(ilha);
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }
}
