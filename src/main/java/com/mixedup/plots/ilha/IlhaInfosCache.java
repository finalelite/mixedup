package com.mixedup.plots.ilha;

import com.mixedup.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class IlhaInfosCache {

    public static HashMap<String, IlhaInfosCache> CACHE = new HashMap<String, IlhaInfosCache>();
    //UUID, NivelDrops, NivelMobspawn, NivelGerador, PvP, Visitas, StatusPvP, MoedaUtilizada, Amigos
    private String UUID;
    private int nivelDrops;
    private int nivelMobspawn;
    private int nivelGerador;
    private boolean pvp;
    private boolean visitas;
    private boolean statusPvP;
    private String moedaUtilizada;
    private String amigos;

    public IlhaInfosCache(final String UUID, final int nivelDrops, final int nivelMobspawn, final int nivelGerador, final boolean pvp, final boolean visitas, final boolean statusPvP, final String moedaUtilizada, final String amigos) {
        this.UUID = UUID;
        this.nivelDrops = nivelDrops;
        this.nivelMobspawn = nivelMobspawn;
        this.nivelGerador = nivelGerador;
        this.pvp = pvp;
        this.visitas = visitas;
        this.statusPvP = statusPvP;
        this.moedaUtilizada = moedaUtilizada;
        this.amigos = amigos;
    }

    public static IlhaInfosCache get(final String UUID) {
        return IlhaInfosCache.CACHE.get(String.valueOf(UUID));
    }

    public static void setCache() {
        for (int i = 1; i <= IlhaInfosCache.getLastTerrain(); i++) {
            if (IlhaInfosCache.getUUID(i) != null) {
                new IlhaInfosCache(IlhaInfosCache.getUUID(i), IlhaInfosCache.getNivelDrops(i), IlhaInfosCache.getNivelMobspawn(i), IlhaInfosCache.getNivelGerador(i), IlhaInfosCache.getPvP(i), IlhaInfosCache.getVisitas(i), IlhaInfosCache.getStatusPvP(i), IlhaInfosCache.getMoedaUtilizada(i), IlhaInfosCache.getAmigos(i)).insert();
            }
        }
    }

    public static int getLastTerrain() {
        int last = 0;
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Ilha_infos ORDER BY ID");
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if (last < rs.getInt("ID") && rs.getInt("ID") != 0) {
                    last = rs.getInt("ID");
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return last;
    }

    public static String getMoedaUtilizada(final int ID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Ilha_infos WHERE ID = ?");
            st.setInt(1, ID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("MoedaUtilizada");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean getStatusPvP(final int ID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Ilha_infos WHERE ID = ?");
            st.setInt(1, ID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("StatusPvP");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean getPvP(final int ID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Ilha_infos WHERE ID = ?");
            st.setInt(1, ID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("PvP");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static int getNivelGerador(final int ID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Ilha_infos WHERE ID = ?");
            st.setInt(1, ID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("NivelGerador");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static int getNivelMobspawn(final int ID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Ilha_infos WHERE ID = ?");
            st.setInt(1, ID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("NivelMobspawn");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static int getNivelDrops(final int ID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Ilha_infos WHERE ID = ?");
            st.setInt(1, ID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("NivelDrops");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getUUID(final int ID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Ilha_infos WHERE ID = ?");
            st.setInt(1, ID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("UUID");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean getVisitas(final int ID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Ilha_infos WHERE ID = ?");
            st.setInt(1, ID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Visitas");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static String getAmigos(final int ID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Ilha_infos WHERE ID = ?");
            st.setInt(1, ID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Amigos");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public IlhaInfosCache insert() {
        IlhaInfosCache.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(final String UUID) {
        this.UUID = UUID;
    }

    public String getMoedaUtilizada() {
        return moedaUtilizada;
    }

    public void setMoedaUtilizada(final String moedaUtilizada) {
        this.moedaUtilizada = moedaUtilizada;
    }

    public boolean getStatusPvP() {
        return statusPvP;
    }

    public void setStatusPvP(final boolean statusPvP) {
        this.statusPvP = statusPvP;
    }

    public boolean getPvP() {
        return pvp;
    }

    public void setPvP(final boolean pvp) {
        this.pvp = pvp;
    }

    public int getNivelGerador() {
        return nivelGerador;
    }

    public void setNivelGerador(final int nivelGerador) {
        this.nivelGerador = nivelGerador;
    }

    public int getNivelMobspawn() {
        return nivelMobspawn;
    }

    public void setNivelMobspawn(final int nivelMobspawn) {
        this.nivelMobspawn = nivelMobspawn;
    }

    public int getNivelDrops() {
        return nivelDrops;
    }

    public void setNivelDrops(final int nivelDrops) {
        this.nivelDrops = nivelDrops;
    }

    public String getAmigos() {
        return amigos;
    }

    public void setAmigos(final String amigos) {
        this.amigos = amigos;
    }

    public void setVisita(final boolean visitas) {
        this.visitas = visitas;
    }

    public boolean getVisitas() {
        return visitas;
    }
}

