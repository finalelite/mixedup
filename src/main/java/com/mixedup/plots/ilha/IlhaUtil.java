package com.mixedup.plots.ilha;

import org.bukkit.Location;

import java.util.List;

public class IlhaUtil {

    public static IlhaRegion playerInArea(final Location loc) {

        final List<IlhaRegion> ilhaList = IlhasRegion.getRegions();
        for (final IlhaRegion rg : ilhaList) {

            final String[] pos1 = rg.getPos1().split(":");
            final String[] pos2 = rg.getPos2().split(":");

            if (!loc.getWorld().getName().equalsIgnoreCase("ilhas") && !loc.getWorld().getName().equalsIgnoreCase("ilhas-default"))
                return null;

            final int p1x = Integer.valueOf(pos1[0]);
            final int p1y = Integer.valueOf(pos1[1]);
            final int p1z = Integer.valueOf(pos1[2]);
            final int p2x = Integer.valueOf(pos2[0]);
            final int p2y = Integer.valueOf(pos2[1]);
            final int p2z = Integer.valueOf(pos2[2]);

            final int minX = p1x < p2x ? p1x : p2x;
            final int minY = p1y < p2y ? p1y : p2y;
            final int minZ = p1z < p2z ? p1z : p2z;

            final int maxX = p1x > p2x ? p1x : p2x;
            final int maxY = p1y > p2y ? p1y : p2y;
            final int maxZ = p1z > p2z ? p1z : p2z;

            if ((loc.getBlockX() >= minX) && (loc.getBlockX() <= maxX) && (loc.getBlockY() >= minY)
                    && (loc.getBlockY() <= maxY) && (loc.getBlockZ() >= minZ) && (loc.getBlockZ() <= maxZ)) {

                return rg;

            }
        }
        return null;
    }
}
