package com.mixedup.plots.ilha;

import com.mixedup.Main;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.RegionsSpawnAPI;
import com.mixedup.apis.TagAPI;
import com.mixedup.hashs.WrongLocation;
import com.mixedup.plots.terrain.TerrainInfosAPI;
import com.mixedup.plots.utils.LastPositionUtil;
import org.bukkit.*;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.Random;

public class PlayerInteractIlha implements Listener {

    @EventHandler
    public void onIgnite(final BlockIgniteEvent event) {
        if (event.getPlayer() != null) {
            Player player = event.getPlayer();
            String UUID = player.getUniqueId().toString();

            final String tag = TagAPI.getTag(UUID);
            if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Admin") || tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Moderador")) {
                return;
            }

            if (IlhaUtil.playerInArea(event.getBlock().getLocation()) != null) {
                if (IlhaUtil.playerInArea(event.getBlock().getLocation()).getRegionID().equals(UUID + "1")) {
                    event.setCancelled(false);
                    return;
                }

                if (TerrainInfosAPI.getAmigos(IlhaUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).equals("NULL")) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                    return;
                }

                if (TerrainInfosAPI.getAmigos(IlhaUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).contains(":") || !TerrainInfosAPI.getAmigos(IlhaUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).equals("NULL")) {
                    if (TerrainInfosAPI.getAmigos(IlhaUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).contains(":")) {
                        final String[] amigos = TerrainInfosAPI.getAmigos(IlhaUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).split(":");

                        for (int i = 1; i <= amigos.length; i++) {
                            if (amigos[i - 1].equals(UUID)) {
                                event.setCancelled(false);
                                break;
                            }
                            if (i == amigos.length) {
                                event.setCancelled(true);
                                player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                                final Random random = new Random();

                                if (random.nextBoolean() == true) {
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            }
                        }
                        return;
                    } else if (!TerrainInfosAPI.getAmigos(IlhaUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).equals("NULL")) {
                        if (TerrainInfosAPI.getAmigos(IlhaUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).equals(UUID)) {
                            event.setCancelled(false);
                        } else {
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                            final Random random = new Random();

                            if (random.nextBoolean() == true) {
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        }
                        return;
                    } else if (TerrainInfosAPI.getAmigos(IlhaUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).equals("NULL")) {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                        final Random random = new Random();

                        if (random.nextBoolean() == true) {
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onHited(final EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
            final Player player = (Player) event.getDamager();
            final Player target = (Player) event.getEntity();

            if (IlhaUtil.playerInArea(player.getLocation()) != null) {
                final IlhaRegion ilha = IlhaUtil.playerInArea(player.getLocation());

                if (IlhaAPI.getPvPStatus(ilha.getRegionID()) == false) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + " * Ops, o pvp está desabilitado dentro desta região!");
                }
            }
            if (IlhaUtil.playerInArea(target.getLocation()) != null) {
                final IlhaRegion ilha = IlhaUtil.playerInArea(target.getLocation());

                if (IlhaAPI.getPvPStatus(ilha.getRegionID()) == false) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + " * Ops, o pvp está desabilitado dentro desta região!");
                }
            }
        } else if (event.getDamager() instanceof Arrow && event.getEntity() instanceof Player) {
            if (IlhaUtil.playerInArea(event.getEntity().getLocation()) != null) {
                final IlhaRegion ilha = IlhaUtil.playerInArea(event.getEntity().getLocation());

                if (IlhaAPI.getPvPStatus(ilha.getRegionID()) == false) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void updateLoc(final PlayerMoveEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        String from = event.getFrom().getWorld().getName() + ":" + event.getFrom().getBlockX() + ":" + event.getFrom().getBlockY() + ":" + event.getFrom().getBlockZ();
        String to = event.getTo().getWorld().getName() + ":" + event.getTo().getBlockX() + ":" + event.getTo().getBlockY() + ":" + event.getTo().getBlockZ();
        if (from.equalsIgnoreCase(to)) return;

        if (IlhaUtil.playerInArea(player.getLocation()) == null) {
            if (player.getWorld().getName().equalsIgnoreCase("Ilhas") || player.getWorld().getName().equalsIgnoreCase("ilhas-default")) {
                if (LastPositionUtil.get(UUID) == null) {
                    final String position = player.getWorld().getName() + ":" + player.getLocation().getBlockX() + ":" + player.getLocation().getBlockY() + ":" + player.getLocation().getBlockZ();
                    new LastPositionUtil(UUID, position, position).insert();
                } else {
                    final String position = player.getWorld().getName() + ":" + player.getLocation().getBlockX() + ":" + player.getLocation().getBlockY() + ":" + player.getLocation().getBlockZ();
                    if (!LastPositionUtil.get(UUID).getLastPosition().equals(position)) {
                        LastPositionUtil.get(UUID).setLastPosition2(LastPositionUtil.get(UUID).getLastPosition());
                        LastPositionUtil.get(UUID).setLastPosition(position);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onMove(final PlayerMoveEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        String from = event.getFrom().getWorld().getName() + ":" + event.getFrom().getBlockX() + ":" + event.getFrom().getBlockY() + ":" + event.getFrom().getBlockZ();
        String to = event.getTo().getWorld().getName() + ":" + event.getTo().getBlockX() + ":" + event.getTo().getBlockY() + ":" + event.getTo().getBlockZ();
        if (from.equalsIgnoreCase(to)) return;

        if (player.getWorld().getName().equals("ilhas") || player.getWorld().getName().equalsIgnoreCase("ilhas-default")) {
            IlhaRegion rg = IlhaUtil.playerInArea(player.getLocation());
            if (rg != null) {
                final IlhaRegion ilha = rg;

                if (WrongLocation.get(UUID) == null) {
                    new WrongLocation(UUID, false).insert();
                } else if (WrongLocation.get(UUID).getStatus() == true) {
                    WrongLocation.get(UUID).setStatus(false);
                }

                if (ilha.getRegionID().equals(UUID + "1")) {
                    event.setCancelled(false);
                    return;
                } else {
                    if (IlhaInfosCache.get(ilha.getRegionID()).getAmigos().contains(UUID)) {
                        event.setCancelled(false);
                        return;
                    } else {
                        if (IlhaInfosCache.get(ilha.getRegionID()).getVisitas() == false) {
                            String tag = TagAPI.getTag(UUID);
                            if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Admin") || tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Moderador")) {
                                return;
                            }

                            final String[] loc = LastPositionUtil.get(UUID).getLastPosition2().split(":");
                            final Location location = new Location(Bukkit.getWorld(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), player.getLocation().getYaw(), player.getLocation().getPitch());
                            player.teleport(location);
                            player.sendMessage(ChatColor.RED + " * Ops, este local não pode ser acessado D=");
                        } else {
                            event.setCancelled(false);
                        }
                    }
                }
            } else {
                String tag = TagAPI.getTag(UUID);
                if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Admin") || tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Moderador")) {
                    return;
                }

                final String[] loc = LastPositionUtil.get(UUID).getLastPosition2().split(":");
                final Location location = new Location(Bukkit.getWorld(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), player.getLocation().getYaw(), player.getLocation().getPitch());
                player.teleport(location);
                player.sendMessage(ChatColor.RED + " * Ops, este local não pode ser acessado D=");

                if (WrongLocation.get(UUID) == null) {
                    new WrongLocation(UUID, true).insert();
                } else if (WrongLocation.get(UUID).getStatus() == false) {
                    WrongLocation.get(UUID).setStatus(true);
                }

                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        IlhaRegion rg = IlhaUtil.playerInArea(player.getLocation());
                        if (rg == null && WrongLocation.get(UUID).getStatus() == true) {
                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    if (rg == null && WrongLocation.get(UUID).getStatus() == true) {
                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                if (rg == null && WrongLocation.get(UUID).getStatus() == true) {
                                                    if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Admin") || tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Moderador")) {
                                                        return;
                                                    }

                                                    WrongLocation.get(UUID).setStatus(false);
                                                    final String[] loc = RegionsSpawnAPI.getLocation("spawn").split(":");
                                                    final Location location = new Location(Bukkit.getWorld(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), Float.parseFloat(loc[4]), Float.parseFloat(loc[5]));
                                                    player.teleport(location);
                                                }
                                            }
                                        }, 20L);
                                    }
                                }
                            }, 20L);
                        }
                    }
                }, 20L);
            }
        }
    }

    @EventHandler
    public void onClickArmorStand(final PlayerInteractAtEntityEvent event) {
        final Player player = event.getPlayer();
        final String UUID = player.getUniqueId().toString();

        final String tag = TagAPI.getTag(UUID);
        if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Admin") || tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Moderador")) {
            return;
        }

        if (event.getRightClicked().getType().equals(EntityType.ITEM_FRAME) || event.getRightClicked().getType().equals(EntityType.ARMOR_STAND)) {
            if (event.getRightClicked().getType().name().contains(Material.ARROW.name())) {
                if (IlhaUtil.playerInArea(event.getRightClicked().getLocation()) != null) {
                    if (IlhaAPI.getAmigos(UUID + "1") != null) {
                        if (IlhaUtil.playerInArea(event.getRightClicked().getLocation()).getRegionID().equals(UUID + "1")) {
                            event.setCancelled(false);
                            return;
                        }
                    }

                    if (IlhaAPI.getAmigos(IlhaUtil.playerInArea(event.getRightClicked().getLocation()).getRegionID()).equals("NULL")) {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                        return;
                    }

                    if (IlhaAPI.getAmigos(IlhaUtil.playerInArea(event.getRightClicked().getLocation()).getRegionID()).contains(":")) {
                        final String[] amigos = IlhaAPI.getAmigos(IlhaUtil.playerInArea(event.getRightClicked().getLocation()).getRegionID()).split(":");

                        for (int i = 1; i <= amigos.length; i++) {
                            if (amigos[i - 1].equals(UUID)) {
                                event.setCancelled(false);
                                break;
                            }
                            if (i == amigos.length) {
                                event.setCancelled(true);
                                player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                                final Random random = new Random();

                                if (random.nextBoolean() == true) {
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            }
                        }
                        return;
                    } else if (!IlhaAPI.getAmigos(IlhaUtil.playerInArea(event.getRightClicked().getLocation()).getRegionID()).equals("NULL")) {
                        if (IlhaAPI.getAmigos(IlhaUtil.playerInArea(event.getRightClicked().getLocation()).getRegionID()).equals(UUID)) {
                            event.setCancelled(false);
                        } else {
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                            final Random random = new Random();

                            if (random.nextBoolean() == true) {
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        }
                        return;
                    } else if (IlhaAPI.getAmigos(IlhaUtil.playerInArea(event.getRightClicked().getLocation()).getRegionID()).equals("NULL")) {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                        final Random random = new Random();

                        if (random.nextBoolean() == true) {
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onInteractEntity(final EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player) {
            final Player player = (Player) event.getDamager();
            final String UUID = player.getUniqueId().toString();

            final String tag = TagAPI.getTag(UUID);
            if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Admin") || tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Moderador")) {
                return;
            }

            if (event.getEntity().getType().equals(EntityType.ITEM_FRAME) || event.getEntity().getType().equals(EntityType.ARMOR_STAND)) {
                if (event.getEntity().getType().name().contains(Material.ARROW.name())) {
                    if (IlhaUtil.playerInArea(event.getEntity().getLocation()) != null) {
                        if (IlhaAPI.getAmigos(UUID + "1") != null) {
                            if (IlhaUtil.playerInArea(event.getEntity().getLocation()).getRegionID().equals(UUID + "1")) {
                                event.setCancelled(false);
                                return;
                            }
                        }

                        if (IlhaAPI.getAmigos(IlhaUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).equals("NULL")) {
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                            return;
                        }

                        if (IlhaAPI.getAmigos(IlhaUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).contains(":")) {
                            final String[] amigos = IlhaAPI.getAmigos(IlhaUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).split(":");

                            for (int i = 1; i <= amigos.length; i++) {
                                if (amigos[i - 1].equals(UUID)) {
                                    event.setCancelled(false);
                                    break;
                                }
                                if (i == amigos.length) {
                                    event.setCancelled(true);
                                    player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                                    final Random random = new Random();

                                    if (random.nextBoolean() == true) {
                                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                    }
                                }
                            }
                            return;
                        } else if (!IlhaAPI.getAmigos(IlhaUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).equals("NULL")) {
                            if (IlhaAPI.getAmigos(IlhaUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).equals(UUID)) {
                                event.setCancelled(false);
                            } else {
                                event.setCancelled(true);
                                player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                                final Random random = new Random();

                                if (random.nextBoolean() == true) {
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            }
                            return;
                        } else if (IlhaAPI.getAmigos(IlhaUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).equals("NULL")) {
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                            final Random random = new Random();

                            if (random.nextBoolean() == true) {
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        } else {
                            event.setCancelled(true);
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onProjectile(final ProjectileLaunchEvent event) {
        if (event.getEntity().getShooter() instanceof Player) {
            final Player player = (Player) event.getEntity().getShooter();
            final String UUID = player.getUniqueId().toString();

            final String tag = TagAPI.getTag(UUID);
            if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Admin") || tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Moderador")) {
                return;
            }

            if (event.getEntity().getType().name().contains(Material.ARROW.name())) {
                if (IlhaUtil.playerInArea(event.getEntity().getLocation()) != null) {
                    if (IlhaAPI.getAmigos(UUID + "1") != null) {
                        if (IlhaUtil.playerInArea(event.getEntity().getLocation()).getRegionID().equals(UUID + "1")) {
                            event.setCancelled(false);
                            return;
                        }
                    }

                    if (IlhaAPI.getAmigos(IlhaUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).equals("NULL")) {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                        return;
                    }

                    if (IlhaAPI.getAmigos(IlhaUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).contains(":")) {
                        final String[] amigos = IlhaAPI.getAmigos(IlhaUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).split(":");

                        for (int i = 1; i <= amigos.length; i++) {
                            if (amigos[i - 1].equals(UUID)) {
                                event.setCancelled(false);
                                break;
                            }
                            if (i == amigos.length) {
                                event.setCancelled(true);
                                player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                                final Random random = new Random();

                                if (random.nextBoolean() == true) {
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            }
                        }
                        return;
                    } else if (!IlhaAPI.getAmigos(IlhaUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).equals("NULL")) {
                        if (UUID != null && IlhaUtil.playerInArea(event.getEntity().getLocation()).getRegionID() != null && IlhaAPI.getAmigos(IlhaUtil.playerInArea(event.getEntity().getLocation()).getRegionID()) != null && IlhaAPI.getAmigos(IlhaUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).equals(UUID)) {
                            event.setCancelled(false);
                        } else {
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                            final Random random = new Random();

                            if (random.nextBoolean() == true) {
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        }
                        return;
                    } else if (IlhaAPI.getAmigos(IlhaUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).equals("NULL")) {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                        final Random random = new Random();

                        if (random.nextBoolean() == true) {
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onInteract(final PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        final String tag = TagAPI.getTag(UUID);
        if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Admin") || tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Moderador")) {
            return;
        }

        if (IlhaUtil.playerInArea(event.getPlayer().getLocation()) != null) {
            if (event.getAction().name().contains("CLICK_BLOCK") && event.getClickedBlock() != null && event.getClickedBlock().getType().name().contains("SIGN"))
                return;

            final IlhaRegion ilha = IlhaUtil.playerInArea(event.getPlayer().getLocation());
            if (IlhaAPI.getAmigos(UUID + "1") != null) {
                if (IlhaUtil.playerInArea(event.getPlayer().getLocation()).getRegionID().equals(UUID + "1")) {
                    event.setCancelled(false);
                    return;
                }
            }

            if (IlhaAPI.getAmigos(ilha.getRegionID()).equals("NULL")) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                return;
            }

            if (IlhaAPI.getAmigos(ilha.getRegionID()).contains(":")) {
                final String[] amigos = IlhaAPI.getAmigos(ilha.getRegionID()).split(":");

                for (int i = 1; i <= amigos.length; i++) {
                    if (amigos[i - 1].equals(UUID)) {
                        event.setCancelled(false);
                        break;
                    }
                    if (i == amigos.length) {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                        final Random random = new Random();

                        if (random.nextBoolean() == true) {
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    }
                }
                return;
            } else if (!IlhaAPI.getAmigos(ilha.getRegionID()).equals("NULL")) {
                if (IlhaAPI.getAmigos(ilha.getRegionID()).equals(UUID)) {
                    event.setCancelled(false);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                    final Random random = new Random();

                    if (random.nextBoolean() == true) {
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
                return;
            } else if (IlhaAPI.getAmigos(ilha.getRegionID()).equals("NULL")) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                final Random random = new Random();

                if (random.nextBoolean() == true) {
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            } else {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPlace(final BlockPlaceEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (IlhaUtil.playerInArea(event.getBlockPlaced().getLocation()) != null) {
            final IlhaRegion ilha = IlhaUtil.playerInArea(event.getBlockPlaced().getLocation());
            if (IlhaAPI.getAmigos(UUID + "1") != null) {
                if (IlhaUtil.playerInArea(event.getBlock().getLocation()).getRegionID().equals(UUID + "1")) {
                    event.setCancelled(false);
                    return;
                }
            }

            if (IlhaAPI.getAmigos(ilha.getRegionID()).equals("NULL")) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                return;
            }

            if (IlhaAPI.getAmigos(ilha.getRegionID()).contains(":")) {
                final String[] amigos = IlhaAPI.getAmigos(ilha.getRegionID()).split(":");

                for (int i = 1; i <= amigos.length; i++) {
                    if (amigos[i - 1].equals(UUID)) {
                        event.setCancelled(false);
                        break;
                    }
                    if (i == amigos.length) {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                        final Random random = new Random();

                        if (random.nextBoolean() == true) {
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    }
                }
                return;
            } else if (!IlhaAPI.getAmigos(ilha.getRegionID()).equals("NULL")) {
                if (IlhaAPI.getAmigos(ilha.getRegionID()).equals(UUID)) {
                    event.setCancelled(false);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                    final Random random = new Random();

                    if (random.nextBoolean() == true) {
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
                return;
            } else if (IlhaAPI.getAmigos(ilha.getRegionID()).equals("NULL")) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                final Random random = new Random();

                if (random.nextBoolean() == true) {
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }
        }
    }

    @EventHandler
    public void onBreak(final BlockBreakEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (IlhaUtil.playerInArea(event.getBlock().getLocation()) != null) {
            final IlhaRegion ilha = IlhaUtil.playerInArea(event.getBlock().getLocation());
            if (IlhaAPI.getAmigos(UUID + "1") != null) {
                if (IlhaUtil.playerInArea(event.getBlock().getLocation()).getRegionID().equals(UUID + "1")) {
                    event.setCancelled(false);
                    return;
                }
            }

            if (IlhaAPI.getAmigos(ilha.getRegionID()).equals("NULL")) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                return;
            }

            if (IlhaAPI.getAmigos(ilha.getRegionID()).contains(":")) {
                final String[] amigos = IlhaAPI.getAmigos(ilha.getRegionID()).split(":");

                for (int i = 1; i <= amigos.length; i++) {
                    if (amigos[i - 1].equals(UUID)) {
                        event.setCancelled(false);
                        break;
                    }
                    if (i == amigos.length) {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                        final Random random = new Random();

                        if (random.nextBoolean() == true) {
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    }
                }
                return;
            } else if (!IlhaAPI.getAmigos(ilha.getRegionID()).equals("NULL")) {
                if (IlhaAPI.getAmigos(ilha.getRegionID()).equals(UUID)) {
                    event.setCancelled(false);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                    final Random random = new Random();

                    if (random.nextBoolean() == true) {
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
                return;
            } else if (IlhaAPI.getAmigos(ilha.getRegionID()).equals("NULL")) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                final Random random = new Random();

                if (random.nextBoolean() == true) {
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }
        }
    }
}
