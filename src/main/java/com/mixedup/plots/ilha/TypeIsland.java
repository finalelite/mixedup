package com.mixedup.plots.ilha;

import java.util.HashMap;

public class TypeIsland {

    private static final HashMap<String, TypeIsland> CACHE = new HashMap<String, TypeIsland>();
    private final String UUID;
    private String type;

    public TypeIsland(final String UUID, final String type) {
        this.UUID = UUID;
        this.type = type;
    }

    public static TypeIsland get(final String UUID) {
        return TypeIsland.CACHE.get(String.valueOf(UUID));
    }

    public TypeIsland insert() {
        TypeIsland.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public String getUUID() {
        return UUID;
    }
}
