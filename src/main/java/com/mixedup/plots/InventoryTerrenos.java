package com.mixedup.plots;

import com.destroystokyo.paper.profile.PlayerProfile;
import com.destroystokyo.paper.profile.ProfileProperty;
import com.mixedup.apis.AccountAPI;
import com.mixedup.apis.GuildaAPI;
import com.mixedup.apis.TagAPI;
import com.mixedup.plots.ilha.IlhaAPI;
import com.mixedup.plots.terrain.TerrainInfosAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;

public class InventoryTerrenos {

    public static Inventory invAmigos(final String UUID) {
        final Inventory inv;

        if (TagAPI.getTag(UUID).equals("Membro")) {
            inv = Bukkit.createInventory(null, 5 * 9, "Amigos: ");
        } else {
            inv = Bukkit.createInventory(null, 6 * 9, "Amigos: ");
        }

        final ItemStack adicionar = new ItemStack(Material.PAPER);
        final ItemMeta metaAdd = adicionar.getItemMeta();
        metaAdd.setDisplayName(ChatColor.GREEN + "Adicionar amigo");
        adicionar.setItemMeta(metaAdd);

        final ItemStack remover = new ItemStack(Material.BARRIER);
        final ItemMeta metaRemover = remover.getItemMeta();
        metaRemover.setDisplayName(ChatColor.GREEN + "Remover amigo");
        remover.setItemMeta(metaRemover);

        final ItemStack vazio = new ItemStack(Material.PLAYER_HEAD);
        final SkullMeta meta = (SkullMeta) vazio.getItemMeta();
        meta.setDisplayName(ChatColor.YELLOW + "Vazio");
        vazio.setItemMeta(meta);

        inv.setItem(12, adicionar);
        inv.setItem(14, remover);

        if (GuildaAPI.getGuilda(UUID) != null) {
            if (GuildaAPI.getGuilda(UUID).equals("sanguinaria") || GuildaAPI.getGuilda(UUID).equals("nobre")) {
                final String[] amigos = TerrainInfosAPI.getAmigos(UUID + "1").split(":");
                if (TagAPI.getTag(UUID).equals("Membro")) {
                    if (TerrainInfosAPI.getAmigos(UUID + "1").equals("NULL")) {
                        for (int i = 1; i <= 5; i++) {
                            inv.setItem(i + 28, vazio);
                        }
                    } else {
                        for (int i = 1; i <= amigos.length; i++) {
                            final ItemStack cabeca = new ItemStack(Material.PLAYER_HEAD);
                            final SkullMeta cabecaMeta = (SkullMeta) cabeca.getItemMeta();
                            //cabecaMeta.setOwner(AccountAPI.getNick(amigos[i - 1]));
                            cabecaMeta.setDisplayName(ChatColor.YELLOW + AccountAPI.getNick(amigos[i - 1]));
                            cabeca.setItemMeta(cabecaMeta);

                            inv.setItem(i + 28, cabeca);
                        }
                        if (amigos.length < 5) {
                            for (int i = 1; i <= 5 - amigos.length; i++) {
                                inv.setItem(i + 28 + amigos.length, vazio);
                            }
                        }
                    }
                } else {
                    if (TerrainInfosAPI.getAmigos(UUID + "1").equals("NULL")) {
                        for (int i = 1; i <= 10; i++) {
                            if (i + 27 <= 34) {
                                inv.setItem(i + 27, vazio);
                            } else if (i + 27 > 34) {
                                inv.setItem(i + 29, vazio);
                            }
                        }
                    } else {
                        if (amigos.length < 10) {
                            for (int i = amigos.length; i <= 10; i++) {
                                if (i + 27 <= 34) {
                                    inv.setItem(i + 27, vazio);
                                } else if (i + 27 > 34) {
                                    inv.setItem(i + 29, vazio);
                                }
                            }
                        }
                        for (int i = 1; i <= amigos.length; i++) {
                            final ItemStack cabeca = new ItemStack(Material.PLAYER_HEAD);
                            final SkullMeta cabecaMeta = (SkullMeta) cabeca.getItemMeta();
                            if (!TerrainInfosAPI.getAmigos(UUID + "1").contains(":")) {
                                //cabecaMeta.setOwner(AccountAPI.getNick(TerrainInfosAPI.getAmigos(UUID + "1")));
                                cabecaMeta.setDisplayName(ChatColor.YELLOW + AccountAPI.getNick(TerrainInfosAPI.getAmigos(UUID + "1")));
                            } else {
                                //cabecaMeta.setOwner(AccountAPI.getNick(amigos[i - 1]));
                                cabecaMeta.setDisplayName(ChatColor.YELLOW + AccountAPI.getNick(amigos[i - 1]));
                            }
                            cabeca.setItemMeta(cabecaMeta);

                            if (i + 27 <= 34) {
                                inv.setItem(i + 27, cabeca);
                            } else if (i + 27 > 34) {
                                inv.setItem(i + 29, cabeca);
                            }
                        }
                    }
                }
            } else if (GuildaAPI.getGuilda(UUID) != null && GuildaAPI.getGuilda(UUID).equals("ancia")) {
                final String[] amigos = IlhaAPI.getAmigos(UUID + "1").split(":");
                if (TagAPI.getTag(UUID).equals("Membro")) {
                    if (IlhaAPI.getAmigos(UUID + "1").equals("NULL")) {
                        for (int i = 1; i <= 5; i++) {
                            inv.setItem(i + 28, vazio);
                        }
                    } else {
                        for (int i = 1; i <= amigos.length; i++) {
                            final ItemStack cabeca = new ItemStack(Material.PLAYER_HEAD);
                            final SkullMeta cabecaMeta = (SkullMeta) cabeca.getItemMeta();
                            //cabecaMeta.setOwner(AccountAPI.getNick(amigos[i - 1]));
                            cabecaMeta.setDisplayName(ChatColor.YELLOW + AccountAPI.getNick(amigos[i - 1]));
                            cabeca.setItemMeta(cabecaMeta);

                            inv.setItem(i + 28, cabeca);
                        }
                        if (amigos.length < 5) {
                            for (int i = 1; i <= 5 - amigos.length; i++) {
                                inv.setItem(i + 28 + amigos.length, vazio);
                            }
                        }
                    }
                } else {
                    if (IlhaAPI.getAmigos(UUID + "1").equals("NULL")) {
                        for (int i = 1; i <= 10; i++) {
                            if (i + 27 <= 34) {
                                inv.setItem(i + 27, vazio);
                            } else if (i + 27 > 34) {
                                inv.setItem(i + 29, vazio);
                            }
                        }
                    } else {
                        if (amigos.length < 10) {
                            for (int i = amigos.length; i <= 10; i++) {
                                if (i + 27 <= 34) {
                                    inv.setItem(i + 27, vazio);
                                } else if (i + 27 > 34) {
                                    inv.setItem(i + 29, vazio);
                                }
                            }
                        }
                        for (int i = 1; i <= amigos.length; i++) {
                            final ItemStack cabeca = new ItemStack(Material.PLAYER_HEAD);
                            final SkullMeta cabecaMeta = (SkullMeta) cabeca.getItemMeta();
                            if (!IlhaAPI.getAmigos(UUID + "1").contains(":")) {
                                //cabecaMeta.setOwner(AccountAPI.getNick(IlhaAPI.getAmigos(UUID + "1")));
                                cabecaMeta.setDisplayName(ChatColor.YELLOW + AccountAPI.getNick(IlhaAPI.getAmigos(UUID + "1")));
                            } else {
                                //cabecaMeta.setOwner(AccountAPI.getNick(amigos[i - 1]));
                                cabecaMeta.setDisplayName(ChatColor.YELLOW + AccountAPI.getNick(amigos[i - 1]));
                            }
                            cabeca.setItemMeta(cabecaMeta);

                            if (i + 27 <= 34) {
                                inv.setItem(i + 27, cabeca);
                            } else if (i + 27 > 34) {
                                inv.setItem(i + 29, cabeca);
                            }
                        }
                    }
                }
            }
        }

        return inv;
    }

    public static Inventory configEspaçonave(final String UUID) {

        final Inventory inv = Bukkit.createInventory(null, 3 * 9, "Configuração espaçonave: ");

        final ItemStack pvp = new ItemStack(Material.STONE_SWORD);
        final ItemMeta pvpMeta = pvp.getItemMeta();
        pvpMeta.setDisplayName(ChatColor.YELLOW + "Modo PvP");
        final ArrayList<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.DARK_GRAY + "Modo: " + ChatColor.RED + "Desabilitado");
        lore1.add(" ");
        lore1.add(ChatColor.DARK_GRAY + "Para habilitar está opção você deve");
        lore1.add(ChatColor.DARK_GRAY + "adquirir upgrade do modo pvp em");
        lore1.add(ChatColor.DARK_GRAY + "/espaçonave (Opção upgrades, pvp).");
        lore1.add(" ");
        pvpMeta.setLore(lore1);
        pvp.setItemMeta(pvpMeta);

        final ItemStack pvpon = new ItemStack(Material.STONE_SWORD);
        final ItemMeta pvponMeta = pvpon.getItemMeta();
        pvponMeta.setDisplayName(ChatColor.YELLOW + "Modo PvP");
        final ArrayList<String> lore4 = new ArrayList<String>();
        lore4.add(" ");
        lore4.add(ChatColor.DARK_GRAY + "Modo: " + ChatColor.GREEN + "Habilitado");
        lore4.add(" ");
        lore4.add(ChatColor.DARK_GRAY + "Ao desabilitar está opção vossa espaçonave");
        lore4.add(ChatColor.DARK_GRAY + "passa a não ter mais pvp.");
        lore4.add(" ");
        pvponMeta.setLore(lore4);
        pvpon.setItemMeta(pvponMeta);

        final ItemStack pvpoff = new ItemStack(Material.BARRIER);
        final ItemMeta pvpoffMeta = pvpoff.getItemMeta();
        pvpoffMeta.setDisplayName(ChatColor.YELLOW + "Modo PvP");
        final ArrayList<String> lore5 = new ArrayList<String>();
        lore5.add(" ");
        lore5.add(ChatColor.DARK_GRAY + "Modo: " + ChatColor.RED + "Desabilitado");
        lore5.add(" ");
        lore5.add(ChatColor.DARK_GRAY + "Ao habilitar está opção vossa espaçonave");
        lore5.add(ChatColor.DARK_GRAY + "passa a ter pvp ativado.");
        lore5.add(" ");
        pvpoffMeta.setLore(lore5);
        pvpoff.setItemMeta(pvpoffMeta);

        final ItemStack visitasoff = new ItemStack(Material.BARRIER);
        final ItemMeta visitasoffMeta = visitasoff.getItemMeta();
        visitasoffMeta.setDisplayName(ChatColor.YELLOW + "Visitas");
        final ArrayList<String> lore7 = new ArrayList<String>();
        lore7.add(" ");
        lore7.add(ChatColor.DARK_GRAY + "Modo: " + ChatColor.RED + "Desabilitado");
        lore7.add(" ");
        lore7.add(ChatColor.DARK_GRAY + "Ao habilitar está opção vossa espaçonave");
        lore7.add(ChatColor.DARK_GRAY + "se torna acessível através do comando");
        lore7.add(ChatColor.DARK_GRAY + "/casa (nick).");
        lore7.add(" ");
        visitasoffMeta.setLore(lore7);
        visitasoff.setItemMeta(visitasoffMeta);

        final ItemStack visitas = new ItemStack(Material.ENDER_PEARL);
        final ItemMeta visitasMeta = visitas.getItemMeta();
        visitasMeta.setDisplayName(ChatColor.YELLOW + "Visitas");
        final ArrayList<String> lore3 = new ArrayList<String>();
        lore3.add(" ");
        lore3.add(ChatColor.DARK_GRAY + "Modo: " + ChatColor.GREEN + "Habilitado");
        lore3.add(" ");
        lore3.add(ChatColor.DARK_GRAY + "Ao desabilitar está opção vossa espaçonave");
        lore3.add(ChatColor.DARK_GRAY + "se torna inacessível através do comando");
        lore3.add(ChatColor.DARK_GRAY + "/casa (nick).");
        lore3.add(" ");
        visitasMeta.setLore(lore3);
        visitas.setItemMeta(visitasMeta);

        final ItemStack item = new ItemStack(Material.PLAYER_HEAD);
        final SkullMeta meta = (SkullMeta) item.getItemMeta();
        //meta.setOwner(AccountAPI.getNick(UUID));
        meta.setDisplayName(ChatColor.YELLOW + "Amigos");
        item.setItemMeta(meta);

        if (IlhaAPI.getPvP(UUID) == false) {
            inv.setItem(11, pvp);
        } else if (IlhaAPI.getPvPStatus(UUID) == true) {
            inv.setItem(11, pvpon);
        } else {
            inv.setItem(11, pvpoff);
        }

        if (IlhaAPI.getVisitas(UUID) == true) {
            inv.setItem(13, visitas);
        } else {
            inv.setItem(13, visitasoff);
        }

        inv.setItem(15, item);

        return inv;
    }

    public static Inventory configIlha(final String UUID) {

        final Inventory inv = Bukkit.createInventory(null, 3 * 9, "Configuração ilha: ");

        final ItemStack pvp = new ItemStack(Material.STONE_SWORD);
        final ItemMeta pvpMeta = pvp.getItemMeta();
        pvpMeta.setDisplayName(ChatColor.YELLOW + "Modo PvP");
        final ArrayList<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.DARK_GRAY + "Modo: " + ChatColor.RED + "Desabilitado");
        lore1.add(" ");
        lore1.add(ChatColor.DARK_GRAY + "Para habilitar está opção você deve adquirir");
        lore1.add(ChatColor.DARK_GRAY + "upgrade do modo pvp em /ilha (Opção upgrades, pvp).");
        lore1.add(" ");
        pvpMeta.setLore(lore1);
        pvp.setItemMeta(pvpMeta);

        final ItemStack pvpon = new ItemStack(Material.STONE_SWORD);
        final ItemMeta pvponMeta = pvpon.getItemMeta();
        pvponMeta.setDisplayName(ChatColor.YELLOW + "Modo PvP");
        final ArrayList<String> lore4 = new ArrayList<String>();
        lore4.add(" ");
        lore4.add(ChatColor.DARK_GRAY + "Modo: " + ChatColor.GREEN + "Habilitado");
        lore4.add(" ");
        lore4.add(ChatColor.DARK_GRAY + "Ao desabilitar está opção vossa ilha");
        lore4.add(ChatColor.DARK_GRAY + "passa a não ter mais pvp.");
        lore4.add(" ");
        pvponMeta.setLore(lore4);
        pvpon.setItemMeta(pvponMeta);

        final ItemStack pvpoff = new ItemStack(Material.BARRIER);
        final ItemMeta pvpoffMeta = pvpoff.getItemMeta();
        pvpoffMeta.setDisplayName(ChatColor.YELLOW + "Modo PvP");
        final ArrayList<String> lore5 = new ArrayList<String>();
        lore5.add(" ");
        lore5.add(ChatColor.DARK_GRAY + "Modo: " + ChatColor.RED + "Desabilitado");
        lore5.add(" ");
        lore5.add(ChatColor.DARK_GRAY + "Ao habilitar está opção vossa ilha");
        lore5.add(ChatColor.DARK_GRAY + "passa a ter pvp ativado.");
        lore5.add(" ");
        pvpoffMeta.setLore(lore5);
        pvpoff.setItemMeta(pvpoffMeta);

        final ItemStack visitasoff = new ItemStack(Material.BARRIER);
        final ItemMeta visitasoffMeta = visitasoff.getItemMeta();
        visitasoffMeta.setDisplayName(ChatColor.YELLOW + "Visitas");
        final ArrayList<String> lore7 = new ArrayList<String>();
        lore7.add(" ");
        lore7.add(ChatColor.DARK_GRAY + "Modo: " + ChatColor.RED + "Desabilitado");
        lore7.add(" ");
        lore7.add(ChatColor.DARK_GRAY + "Ao habilitar está opção vossa ilha se");
        lore7.add(ChatColor.DARK_GRAY + "torna acessível através do comando");
        lore7.add(ChatColor.DARK_GRAY + "/casa (nick).");
        lore7.add(" ");
        visitasoffMeta.setLore(lore7);
        visitasoff.setItemMeta(visitasoffMeta);

        final ItemStack visitas = new ItemStack(Material.ENDER_PEARL);
        final ItemMeta visitasMeta = visitas.getItemMeta();
        visitasMeta.setDisplayName(ChatColor.YELLOW + "Visitas");
        final ArrayList<String> lore3 = new ArrayList<String>();
        lore3.add(" ");
        lore3.add(ChatColor.DARK_GRAY + "Modo: " + ChatColor.GREEN + "Habilitado");
        lore3.add(" ");
        lore3.add(ChatColor.DARK_GRAY + "Ao desabilitar está opção vossa ilha se");
        lore3.add(ChatColor.DARK_GRAY + "torna inacessível através do comando");
        lore3.add(ChatColor.DARK_GRAY + "/casa (nick).");
        lore3.add(" ");
        visitasMeta.setLore(lore3);
        visitas.setItemMeta(visitasMeta);

        final ItemStack item = new ItemStack(Material.PLAYER_HEAD);
        final SkullMeta meta = (SkullMeta) item.getItemMeta();
        //meta.setOwner(AccountAPI.getNick(UUID));
        meta.setDisplayName(ChatColor.YELLOW + "Amigos");
        item.setItemMeta(meta);

        if (IlhaAPI.getPvP(UUID) == false) {
            inv.setItem(11, pvp);
        } else if (IlhaAPI.getPvPStatus(UUID) == true) {
            inv.setItem(11, pvpon);
        } else {
            inv.setItem(11, pvpoff);
        }

        if (IlhaAPI.getVisitas(UUID) == true) {
            inv.setItem(13, visitas);
        } else {
            inv.setItem(13, visitasoff);
        }

        inv.setItem(15, item);

        return inv;
    }

    public static Inventory configTerreno(final String UUID) {

        final Inventory inv = Bukkit.createInventory(null, 3 * 9, "Configuração terreno: ");

        final ItemStack pvp = new ItemStack(Material.STONE_SWORD);
        final ItemMeta pvpMeta = pvp.getItemMeta();
        pvpMeta.setDisplayName(ChatColor.YELLOW + "Modo PvP");
        final ArrayList<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.DARK_GRAY + "Modo: " + ChatColor.RED + "Desabilitado");
        lore1.add(" ");
        lore1.add(ChatColor.DARK_GRAY + "Para habilitar está opção você deve");
        lore1.add(ChatColor.DARK_GRAY + "adquirir upgrade do modo pvp em");
        lore1.add(ChatColor.DARK_GRAY + "/terreno (Opção upgrades, pvp).");
        lore1.add(" ");
        pvpMeta.setLore(lore1);
        pvp.setItemMeta(pvpMeta);

        final ItemStack pvpon = new ItemStack(Material.STONE_SWORD);
        final ItemMeta pvponMeta = pvpon.getItemMeta();
        pvponMeta.setDisplayName(ChatColor.YELLOW + "Modo PvP");
        final ArrayList<String> lore4 = new ArrayList<String>();
        lore4.add(" ");
        lore4.add(ChatColor.DARK_GRAY + "Modo: " + ChatColor.GREEN + "Habilitado");
        lore4.add(" ");
        lore4.add(ChatColor.DARK_GRAY + "Ao desabilitar está opção vosso terreno");
        lore4.add(ChatColor.DARK_GRAY + "passa a não ter mais pvp.");
        lore4.add(" ");
        pvponMeta.setLore(lore4);
        pvpon.setItemMeta(pvponMeta);

        final ItemStack pvpoff = new ItemStack(Material.BARRIER);
        final ItemMeta pvpoffMeta = pvpoff.getItemMeta();
        pvpoffMeta.setDisplayName(ChatColor.YELLOW + "Modo PvP");
        final ArrayList<String> lore5 = new ArrayList<String>();
        lore5.add(" ");
        lore5.add(ChatColor.DARK_GRAY + "Modo: " + ChatColor.RED + "Desabilitado");
        lore5.add(" ");
        lore5.add(ChatColor.DARK_GRAY + "Ao habilitar está opção vosso terreno");
        lore5.add(ChatColor.DARK_GRAY + "passa a ter pvp ativado.");
        lore5.add(" ");
        pvpoffMeta.setLore(lore5);
        pvpoff.setItemMeta(pvpoffMeta);

        final ItemStack acesso = new ItemStack(Material.CHAINMAIL_BOOTS);
        final ItemMeta acessoMeta = acesso.getItemMeta();
        acessoMeta.setDisplayName(ChatColor.YELLOW + "Acesso");
        final ArrayList<String> lore2 = new ArrayList<String>();
        lore2.add(" ");
        lore2.add(ChatColor.DARK_GRAY + "Modo: " + ChatColor.GREEN + "Habilitado");
        lore2.add(" ");
        lore2.add(ChatColor.DARK_GRAY + "Ao desabilitar está opção vosso terreno");
        lore2.add(ChatColor.DARK_GRAY + "se torna inacessível para todo e qualquer");
        lore2.add(ChatColor.DARK_GRAY + "player.");
        lore2.add(" ");
        acessoMeta.setLore(lore2);
        acesso.setItemMeta(acessoMeta);

        final ItemStack acessoff = new ItemStack(Material.BARRIER);
        final ItemMeta acessoffMeta = acessoff.getItemMeta();
        acessoffMeta.setDisplayName(ChatColor.YELLOW + "Acesso");
        final ArrayList<String> lore6 = new ArrayList<String>();
        lore6.add(" ");
        lore6.add(ChatColor.DARK_GRAY + "Modo: " + ChatColor.RED + "Desabilitado");
        lore6.add(" ");
        lore6.add(ChatColor.DARK_GRAY + "Ao habilitar está opção vosso terreno");
        lore6.add(ChatColor.DARK_GRAY + "se torna acessível para todo e qualquer");
        lore6.add(ChatColor.DARK_GRAY + "player.");
        lore6.add(" ");
        acessoffMeta.setLore(lore6);
        acessoff.setItemMeta(acessoffMeta);

        final ItemStack visitasoff = new ItemStack(Material.BARRIER);
        final ItemMeta visitasoffMeta = visitasoff.getItemMeta();
        visitasoffMeta.setDisplayName(ChatColor.YELLOW + "Visitas");
        final ArrayList<String> lore7 = new ArrayList<String>();
        lore7.add(" ");
        lore7.add(ChatColor.DARK_GRAY + "Modo: " + ChatColor.RED + "Desabilitado");
        lore7.add(" ");
        lore7.add(ChatColor.DARK_GRAY + "Ao habilitar está opção vosso terreno");
        lore7.add(ChatColor.DARK_GRAY + "se torna acessível através do comando");
        lore7.add(ChatColor.DARK_GRAY + "/casa (nick).");
        lore7.add(" ");
        visitasoffMeta.setLore(lore7);
        visitasoff.setItemMeta(visitasoffMeta);

        final ItemStack visitas = new ItemStack(Material.ENDER_PEARL);
        final ItemMeta visitasMeta = visitas.getItemMeta();
        visitasMeta.setDisplayName(ChatColor.YELLOW + "Visitas");
        final ArrayList<String> lore3 = new ArrayList<String>();
        lore3.add(" ");
        lore3.add(ChatColor.DARK_GRAY + "Modo: " + ChatColor.GREEN + "Habilitado");
        lore3.add(" ");
        lore3.add(ChatColor.DARK_GRAY + "Ao desabilitar está opção vosso terreno");
        lore3.add(ChatColor.DARK_GRAY + "se torna inacessível através do comando");
        lore3.add(ChatColor.DARK_GRAY + "/casa (nick).");
        lore3.add(" ");
        visitasMeta.setLore(lore3);
        visitas.setItemMeta(visitasMeta);

        final ItemStack item = new ItemStack(Material.PLAYER_HEAD);
        final ItemMeta meta = (SkullMeta) item.getItemMeta();
        //meta.setOwner(AccountAPI.getNick(UUID));
        meta.setDisplayName(ChatColor.YELLOW + "Amigos");
        item.setItemMeta(meta);

        if (TerrainInfosAPI.getPvP(UUID) == false) {
            inv.setItem(10, pvp);
        } else if (TerrainInfosAPI.getPvPStatus(UUID) == true) {
            inv.setItem(10, pvpon);
        } else {
            inv.setItem(10, pvpoff);
        }

        if (TerrainInfosAPI.getAcesso(UUID) == true) {
            inv.setItem(12, acesso);
        } else {
            inv.setItem(12, acessoff);
        }

        if (TerrainInfosAPI.getTeletransporte(UUID) == true) {
            inv.setItem(14, visitas);
        } else {
            inv.setItem(14, visitasoff);
        }

        inv.setItem(16, item);

        return inv;
    }

    public static Inventory escolhaIlha() {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Tipo de ilha: ");

        final ItemStack padrao = new ItemStack(Material.GRASS_BLOCK);
        final ItemMeta metaP = padrao.getItemMeta();
        metaP.setDisplayName(ChatColor.GREEN + "Padrão");
        final ArrayList<String> lore = new ArrayList();
        lore.add(" ");
        lore.add(ChatColor.GRAY + " Ilha padrão, crie tudo do 0");
        lore.add(ChatColor.GRAY + " se auto desenvolva, e cuidado");
        lore.add(ChatColor.GRAY + " com o void!");
        lore.add(" ");
        metaP.setLore(lore);
        padrao.setItemMeta(metaP);

        final ItemStack custom = new ItemStack(Material.LIGHT_BLUE_CONCRETE);
        final ItemMeta metaC = custom.getItemMeta();
        metaC.setDisplayName(ChatColor.GREEN + "Personalizada");
        final ArrayList<String> lore1 = new ArrayList<>();
        lore1.add(" ");
        lore1.add(ChatColor.GRAY + " Ilha personalizada, viva em");
        lore1.add(ChatColor.GRAY + " auto mar em uma ilha caótica");
        lore1.add(ChatColor.GRAY + " e cuidado com os tubarões!");
        lore1.add(" ");
        metaC.setLore(lore1);
        custom.setItemMeta(metaC);

        inventory.setItem(12, padrao);
        inventory.setItem(14, custom);

        return inventory;
    }

    public static Inventory escolhaCompraMoeda(final String terrain) {

        final Inventory inv = Bukkit.createInventory(null, 3 * 9, "Escolha opção de pagamento: ");

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta moedaMeta = moeda.getItemMeta();
        moedaMeta.setDisplayName(ChatColor.YELLOW + "Moeda comum");
        final ArrayList<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.DARK_GRAY + "A o escolher está opção serão debitados");
        if (terrain.equals("terreno")) {
            lore1.add(ChatColor.DARK_GRAY + "de sua conta" + ChatColor.GREEN + " 20.000,00 " + ChatColor.DARK_GRAY + "moedas!");
        } else if (terrain.equals("ilha")) {
            lore1.add(ChatColor.DARK_GRAY + "de sua conta" + ChatColor.GREEN + " 25.000,00 " + ChatColor.DARK_GRAY + "moedas!");
        } else if (terrain.equals("espaçonave")) {
            lore1.add(ChatColor.DARK_GRAY + "de sua conta" + ChatColor.GREEN + " 30.000,00 " + ChatColor.DARK_GRAY + "moedas!");
        }
        lore1.add(" ");
        moedaMeta.setLore(lore1);
        moeda.setItemMeta(moedaMeta);

        final ItemStack moedanegra = new ItemStack(Material.IRON_NUGGET);
        final ItemMeta moedanegraMeta = moedanegra.getItemMeta();
        moedanegraMeta.setDisplayName(ChatColor.YELLOW + "Moeda negra");
        final ArrayList<String> lore2 = new ArrayList<String>();
        lore2.add(" ");
        lore2.add(ChatColor.DARK_GRAY + "A o escolher está opção serão debitados");
        if (terrain.equals("terreno")) {
            lore2.add(ChatColor.DARK_GRAY + "de sua conta" + ChatColor.RED + " 500,00 " + ChatColor.DARK_GRAY + "moedas negras!");
        } else if (terrain.equals("ilha")) {
            lore2.add(ChatColor.DARK_GRAY + "de sua conta" + ChatColor.RED + " 600,00 " + ChatColor.DARK_GRAY + "moedas negras!");
        } else if (terrain.equals("espaçonave")) {
            lore2.add(ChatColor.DARK_GRAY + "de sua conta" + ChatColor.RED + " 700,00 " + ChatColor.DARK_GRAY + "moedas negras!");
        }
        lore2.add(" ");
        moedanegraMeta.setLore(lore2);
        moedanegra.setItemMeta(moedanegraMeta);

        inv.setItem(12, moeda);
        inv.setItem(14, moedanegra);

        return inv;
    }

    public static Inventory getInvConfirmUpgradePvp() {

        final Inventory inv = Bukkit.createInventory(null, 3 * 9, "Confirmar upgrade pvp: ");

        final ItemStack confirmar = new ItemStack(Material.LIME_STAINED_GLASS_PANE);
        final ItemMeta metaConfirmar = confirmar.getItemMeta();
        metaConfirmar.setDisplayName(ChatColor.GREEN + "CONFIRMAR");
        confirmar.setItemMeta(metaConfirmar);

        final ItemStack negar = new ItemStack(Material.RED_STAINED_GLASS_PANE);
        final ItemMeta metaNegar = negar.getItemMeta();
        metaNegar.setDisplayName(ChatColor.RED + "NEGAR");
        negar.setItemMeta(metaNegar);

        inv.setItem(12, confirmar);
        inv.setItem(14, negar);

        return inv;
    }

    public static Inventory getInvConfirmUpgradeMobspawn() {

        final Inventory inv = Bukkit.createInventory(null, 3 * 9, "Confirmar upgrade mobspawn: ");

        final ItemStack confirmar = new ItemStack(Material.LIME_STAINED_GLASS_PANE);
        final ItemMeta metaConfirmar = confirmar.getItemMeta();
        metaConfirmar.setDisplayName(ChatColor.GREEN + "CONFIRMAR");
        confirmar.setItemMeta(metaConfirmar);

        final ItemStack negar = new ItemStack(Material.RED_STAINED_GLASS_PANE);
        final ItemMeta metaNegar = negar.getItemMeta();
        metaNegar.setDisplayName(ChatColor.RED + "NEGAR");
        negar.setItemMeta(metaNegar);

        inv.setItem(12, confirmar);
        inv.setItem(14, negar);

        return inv;
    }

    public static Inventory getInvConfirmUpgradeDrops() {

        final Inventory inv = Bukkit.createInventory(null, 3 * 9, "Confirmar upgrade drops: ");

        final ItemStack confirmar = new ItemStack(Material.LIME_STAINED_GLASS_PANE);
        final ItemMeta metaConfirmar = confirmar.getItemMeta();
        metaConfirmar.setDisplayName(ChatColor.GREEN + "CONFIRMAR");
        confirmar.setItemMeta(metaConfirmar);

        final ItemStack negar = new ItemStack(Material.RED_STAINED_GLASS_PANE);
        final ItemMeta metaNegar = negar.getItemMeta();
        metaNegar.setDisplayName(ChatColor.RED + "NEGAR");
        negar.setItemMeta(metaNegar);

        inv.setItem(12, confirmar);
        inv.setItem(14, negar);

        return inv;
    }

    public static Inventory getInvConfirmVenda(final String terrain) {

        final Inventory inv = Bukkit.createInventory(null, 3 * 9, "Confirmar venda " + terrain + ": ");

        final ItemStack confirmar = new ItemStack(Material.LIME_STAINED_GLASS_PANE);
        final ItemMeta metaConfirmar = confirmar.getItemMeta();
        metaConfirmar.setDisplayName(ChatColor.GREEN + "CONFIRMAR");
        confirmar.setItemMeta(metaConfirmar);

        final ItemStack negar = new ItemStack(Material.RED_STAINED_GLASS_PANE);
        final ItemMeta metaNegar = negar.getItemMeta();
        metaNegar.setDisplayName(ChatColor.RED + "NEGAR");
        negar.setItemMeta(metaNegar);

        inv.setItem(12, confirmar);
        inv.setItem(14, negar);

        return inv;
    }

    public static Inventory getInvConfirmCompra(final String terrain) {

        final Inventory inv = Bukkit.createInventory(null, 3 * 9, "Confirmar compra " + terrain + ": ");

        final ItemStack confirmar = new ItemStack(Material.LIME_STAINED_GLASS_PANE);
        final ItemMeta metaConfirmar = confirmar.getItemMeta();
        metaConfirmar.setDisplayName(ChatColor.GREEN + "CONFIRMAR");
        confirmar.setItemMeta(metaConfirmar);

        final ItemStack negar = new ItemStack(Material.RED_STAINED_GLASS_PANE);
        final ItemMeta metaNegar = negar.getItemMeta();
        metaNegar.setDisplayName(ChatColor.RED + "NEGAR");
        negar.setItemMeta(metaNegar);

        inv.setItem(12, confirmar);
        inv.setItem(14, negar);

        return inv;
    }

    public static Inventory getInvUpgrades(final String UUID, final String terrain) {

        final Inventory inv = Bukkit.createInventory(null, 3 * 9, "Upgrades " + terrain + ": ");

        final ItemStack drops3 = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta meta3 = (EnchantmentStorageMeta) drops3.getItemMeta();
        meta3.addStoredEnchant(Enchantment.LOOT_BONUS_MOBS, 3, true);
        meta3.setDisplayName(ChatColor.GREEN + "Drops");
        final ArrayList<String> lore6 = new ArrayList<String>();
        lore6.add(" ");
        lore6.add(ChatColor.GREEN + "Nível 3 (Nível maxímo)");
        lore6.add(" ");
        meta3.setLore(lore6);
        drops3.setItemMeta(meta3);

        final ItemStack drops2 = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta meta2 = (EnchantmentStorageMeta) drops2.getItemMeta();
        meta2.addStoredEnchant(Enchantment.LOOT_BONUS_MOBS, 2, true);
        meta2.setDisplayName(ChatColor.GOLD + "Drops");
        final ArrayList<String> lore5 = new ArrayList<String>();
        lore5.add(" ");
        lore5.add(ChatColor.DARK_GRAY + "Nível " + ChatColor.GREEN + "2");
        lore5.add(ChatColor.DARK_GRAY + "Upgrade: " + ChatColor.GREEN + "25.000,00");
        lore5.add(" ");
        meta2.setLore(lore5);
        drops2.setItemMeta(meta2);

        final ItemStack drops = new ItemStack(Material.ENCHANTED_BOOK);
        final EnchantmentStorageMeta meta = (EnchantmentStorageMeta) drops.getItemMeta();
        meta.addStoredEnchant(Enchantment.LOOT_BONUS_MOBS, 1, true);
        meta.setDisplayName(ChatColor.YELLOW + "Drops");
        final ArrayList<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.DARK_GRAY + "Nível " + ChatColor.GREEN + "1");
        lore1.add(ChatColor.DARK_GRAY + "Upgrade: " + ChatColor.GREEN + "15.000,00");
        lore1.add(" ");
        meta.setLore(lore1);
        drops.setItemMeta(meta);
        if (TerrainInfosAPI.getNivelDrops(UUID) == 1) {
            inv.setItem(10, drops);
        } else if (TerrainInfosAPI.getNivelDrops(UUID) == 2) {
            inv.setItem(10, drops2);
        } else if (TerrainInfosAPI.getNivelDrops(UUID) == 3) {
            inv.setItem(10, drops3);
        }

        final ItemStack mobspawner3 = new ItemStack(Material.SPAWNER);
        final ItemMeta metaSpawner3 = mobspawner3.getItemMeta();
        metaSpawner3.setDisplayName(ChatColor.GREEN + "Mobspawn");
        final ArrayList<String> lore8 = new ArrayList<String>();
        lore8.add(" ");
        lore8.add(ChatColor.GREEN + "Nível 3 (Nível maxímo)");
        lore8.add(" ");
        metaSpawner3.setLore(lore8);
        mobspawner3.setItemMeta(metaSpawner3);

        final ItemStack mobspawner2 = new ItemStack(Material.SPAWNER);
        final ItemMeta metaSpawner2 = mobspawner2.getItemMeta();
        metaSpawner2.setDisplayName(ChatColor.GOLD + "Mobspawn");
        final ArrayList<String> lore7 = new ArrayList<String>();
        lore7.add(" ");
        lore7.add(ChatColor.DARK_GRAY + "Nível " + ChatColor.GREEN + "2");
        lore7.add(ChatColor.DARK_GRAY + "Upgrade: " + ChatColor.GREEN + "25.000,00");
        lore7.add(" ");
        metaSpawner2.setLore(lore7);
        mobspawner2.setItemMeta(metaSpawner2);

        final ItemStack mobspawner = new ItemStack(Material.SPAWNER);
        final ItemMeta metaSpawner = mobspawner.getItemMeta();
        metaSpawner.setDisplayName(ChatColor.YELLOW + "Mobspawn");
        final ArrayList<String> lore2 = new ArrayList<String>();
        lore2.add(" ");
        lore2.add(ChatColor.DARK_GRAY + "Nível " + ChatColor.GREEN + "1");
        lore2.add(ChatColor.DARK_GRAY + "Upgrade: " + ChatColor.GREEN + "15.000,00");
        lore2.add(" ");
        metaSpawner.setLore(lore2);
        mobspawner.setItemMeta(metaSpawner);
        if (TerrainInfosAPI.getNivelMobspawn(UUID) == 1) {
            inv.setItem(12, mobspawner);
        } else if (TerrainInfosAPI.getNivelMobspawn(UUID) == 2) {
            inv.setItem(12, mobspawner2);
        } else if (TerrainInfosAPI.getNivelMobspawn(UUID) == 3) {
            inv.setItem(12, mobspawner3);
        }

        final ItemStack gerador3 = new ItemStack(Material.GOLD_BLOCK);
        final ItemMeta metaGerador3 = gerador3.getItemMeta();
        metaGerador3.setDisplayName(ChatColor.GREEN + "Gerador de minérios");
        final ArrayList<String> lore10 = new ArrayList<String>();
        lore10.add(" ");
        lore10.add(ChatColor.DARK_GRAY + "Nível 3 (Nível maxímo)");
        //ADD VALOR EVOLUÇÃO
        lore10.add(ChatColor.RED + "EM BREVE");
        lore10.add(" ");
        metaGerador3.setLore(lore10);
        gerador3.setItemMeta(metaGerador3);

        final ItemStack gerador2 = new ItemStack(Material.GOLD_BLOCK);
        final ItemMeta metaGerador2 = gerador2.getItemMeta();
        metaGerador2.setDisplayName(ChatColor.GOLD + "Gerador de minérios");
        final ArrayList<String> lore9 = new ArrayList<String>();
        lore9.add(" ");
        lore9.add(ChatColor.DARK_GRAY + "Nível " + ChatColor.GREEN + "2");
        //ADD VALOR EVOLUÇÃO
        lore9.add(ChatColor.RED + "EM BREVE");
        lore9.add(" ");
        metaGerador2.setLore(lore9);
        gerador2.setItemMeta(metaGerador2);

        final ItemStack gerador = new ItemStack(Material.GOLD_BLOCK);
        final ItemMeta metaGerador = gerador.getItemMeta();
        metaGerador.setDisplayName(ChatColor.YELLOW + "Gerador de minérios");
        final ArrayList<String> lore3 = new ArrayList<String>();
        lore3.add(" ");
        lore3.add(ChatColor.DARK_GRAY + "Nível " + ChatColor.GREEN + "1");
        //ADD VALOR EVOLUÇÃO
        lore3.add(ChatColor.RED + "EM BREVE");
        lore3.add(" ");
        metaGerador.setLore(lore3);
        gerador.setItemMeta(metaGerador);
        if (TerrainInfosAPI.getNivelGerador(UUID) == 1) {
            inv.setItem(14, gerador);
        } else if (TerrainInfosAPI.getNivelGerador(UUID) == 2) {
            inv.setItem(14, gerador2);
        } else if (TerrainInfosAPI.getNivelGerador(UUID) == 3) {
            inv.setItem(14, gerador3);
        }

        final ItemStack pvpoff = new ItemStack(Material.DIAMOND_SWORD);
        final ItemMeta metaPvpOff = pvpoff.getItemMeta();
        metaPvpOff.setDisplayName(ChatColor.GREEN + "PvP");
        final ArrayList<String> lore11 = new ArrayList<String>();
        lore11.add(" ");
        lore11.add(ChatColor.DARK_GRAY + "Modo: " + ChatColor.RED + "Desativado");
        lore11.add(" ");
        metaPvpOff.setLore(lore11);
        pvpoff.setItemMeta(metaPvpOff);

        final ItemStack pvp = new ItemStack(Material.DIAMOND_SWORD);
        final ItemMeta metaPvp = pvp.getItemMeta();
        metaPvp.setDisplayName(ChatColor.RED + "PvP");
        final ArrayList<String> lore4 = new ArrayList<String>();
        lore4.add(" ");
        lore4.add(ChatColor.DARK_GRAY + "Modo: " + ChatColor.GREEN + "Ativado");
        lore2.add(ChatColor.DARK_GRAY + "Upgrade: " + ChatColor.GREEN + "5.000,00");
        lore4.add(" ");
        metaPvp.setLore(lore4);
        pvp.setItemMeta(metaPvp);
        if (TerrainInfosAPI.getPvP(UUID) == true) {
            inv.setItem(16, pvp);
        } else {
            inv.setItem(16, pvpoff);
        }

        return inv;
    }

    public static Inventory getInvTerrain(final String guilda) {
        if (guilda.equals("nobre") || guilda.equals("sanguinaria")) {
            final Inventory invt = Bukkit.createInventory(null, 3 * 9, "Terreno: ");

            final ItemStack upgrades = new ItemStack(Material.MAP);
            final ItemMeta metaUpgrades = upgrades.getItemMeta();
            metaUpgrades.setDisplayName(ChatColor.YELLOW + "Upgrades terreno");
            final ArrayList<String> lore1 = new ArrayList<String>();
            lore1.add(" ");
            lore1.add(ChatColor.DARK_GRAY + "Sistema de McMMO para o seu terreno,");
            lore1.add(ChatColor.DARK_GRAY + "evolua nível de farm à nível de drops");
            lore1.add(ChatColor.DARK_GRAY + "com está opção.");
            lore1.add(" ");
            metaUpgrades.setLore(lore1);
            upgrades.setItemMeta(metaUpgrades);
            invt.setItem(11, upgrades);

            final ItemStack comprar = new ItemStack(Material.GRASS_BLOCK);
            final ItemMeta metaComprar = comprar.getItemMeta();
            metaComprar.setDisplayName(ChatColor.GREEN + "Comprar terreno");
            final ArrayList<String> lore2 = new ArrayList<String>();
            lore2.add(" ");
            lore2.add(ChatColor.DARK_GRAY + "Valor de compra (Moedas):" + ChatColor.GREEN + " 20.000,00");
            lore2.add(ChatColor.DARK_GRAY + "Valor de compra (Moedas negra):" + ChatColor.RED + " 500,00");
            lore2.add(" ");
            metaComprar.setLore(lore2);
            comprar.setItemMeta(metaComprar);
            invt.setItem(13, comprar);

            final ItemStack vender = new ItemStack(Material.BARRIER);
            final ItemMeta metaVender = vender.getItemMeta();
            metaVender.setDisplayName(ChatColor.RED + "Vender terreno");
            final ArrayList<String> lore3 = new ArrayList<String>();
            lore3.add(" ");
            lore3.add(ChatColor.DARK_GRAY + "Valor de venda (Moedas):" + ChatColor.GREEN + " 13.500,00");
            lore3.add(ChatColor.DARK_GRAY + "Valor de venda (Moedas negra):" + ChatColor.RED + " 215,00");
            lore3.add(" ");
            metaVender.setLore(lore3);
            vender.setItemMeta(metaVender);
            invt.setItem(15, vender);

            return invt;
        }

        if (guilda.equals("ancia")) {
            final Inventory invis = Bukkit.createInventory(null, 3 * 9, "Ilha: ");

            final ItemStack upgradesis = new ItemStack(Material.MAP);
            final ItemMeta metaUpgradesis = upgradesis.getItemMeta();
            metaUpgradesis.setDisplayName(ChatColor.YELLOW + "Upgrades ilha");
            final ArrayList<String> lore4 = new ArrayList<String>();
            lore4.add(" ");
            lore4.add(ChatColor.DARK_GRAY + "Sistema de McMMO para a sua ilha,");
            lore4.add(ChatColor.DARK_GRAY + "evolua nível de farm à nível de drops");
            lore4.add(ChatColor.DARK_GRAY + "com está opção.");
            lore4.add(" ");
            metaUpgradesis.setLore(lore4);
            upgradesis.setItemMeta(metaUpgradesis);
            invis.setItem(11, upgradesis);

            final ItemStack compraris = new ItemStack(Material.GRASS_BLOCK);
            final ItemMeta metaCompraris = compraris.getItemMeta();
            metaCompraris.setDisplayName(ChatColor.GREEN + "Comprar ilha");
            final ArrayList<String> lore5 = new ArrayList<String>();
            lore5.add(" ");
            lore5.add(ChatColor.DARK_GRAY + "Valor de compra (Moedas):" + ChatColor.GREEN + " 25.000,00");
            lore5.add(ChatColor.DARK_GRAY + "Valor de compra (Moedas negra):" + ChatColor.RED + " 600,00");
            lore5.add(" ");
            metaCompraris.setLore(lore5);
            compraris.setItemMeta(metaCompraris);
            invis.setItem(13, compraris);

            final ItemStack venderis = new ItemStack(Material.BARRIER);
            final ItemMeta metaVenderis = venderis.getItemMeta();
            metaVenderis.setDisplayName(ChatColor.RED + "Vender ilha");
            final ArrayList<String> lore6 = new ArrayList<String>();
            lore6.add(" ");
            lore6.add(ChatColor.DARK_GRAY + "Valor de venda (Moedas):" + ChatColor.GREEN + " 16.500,00");
            lore6.add(ChatColor.DARK_GRAY + "Valor de venda (Moedas negra):" + ChatColor.RED + " 315,00");
            lore6.add(" ");
            metaVenderis.setLore(lore6);
            venderis.setItemMeta(metaVenderis);
            invis.setItem(15, venderis);

            return invis;
        }
//
//        if (guilda.equals("sanguinaria")) {
//            Inventory inves = Bukkit.createInventory(null, 3 * 9, "Espaçonave: ");
//
//            ItemStack upgradeses = new ItemStack(Material.MAP);
//            ItemMeta metaUpgradeses = upgradeses.getItemMeta();
//            metaUpgradeses.setDisplayName(ChatColor.YELLOW + "Upgrades espaçonave");
//            ArrayList<String> lore7 = new ArrayList<String>();
//            lore7.add(" ");
//            lore7.add(ChatColor.DARK_GRAY + "Sistema de McMMO para a sua espaçonave,");
//            lore7.add(ChatColor.DARK_GRAY + "evolua nível de farm à nível de drops");
//            lore7.add(ChatColor.DARK_GRAY + "com está opção.");
//            lore7.add(" ");
//            metaUpgradeses.setLore(lore7);
//            upgradeses.setItemMeta(metaUpgradeses);
//            inves.setItem(11, upgradeses);
//
//            ItemStack comprares = new ItemStack(Material.GRASS_BLOCK);
//            ItemMeta metaComprares = comprares.getItemMeta();
//            metaComprares.setDisplayName(ChatColor.GREEN + "Comprar espaçonave");
//            ArrayList<String> lore8 = new ArrayList<String>();
//            lore8.add(" ");
//            lore8.add(ChatColor.DARK_GRAY + "Valor de compra (Moedas):" + ChatColor.GREEN + " 30.000,00");
//            lore8.add(ChatColor.DARK_GRAY + "Valor de compra (Moedas negra):" + ChatColor.RED + " 700,00");
//            lore8.add(" ");
//            metaComprares.setLore(lore8);
//            comprares.setItemMeta(metaComprares);
//            inves.setItem(13, comprares);
//
//            ItemStack venderes = new ItemStack(Material.BARRIER);
//            ItemMeta metaVenderes = venderes.getItemMeta();
//            metaVenderes.setDisplayName(ChatColor.RED + "Vender espaçonave");
//            ArrayList<String> lore9 = new ArrayList<String>();
//            lore9.add(" ");
//            lore9.add(ChatColor.DARK_GRAY + "Valor de venda (Moedas):" + ChatColor.GREEN + " 21.500,00");
//            lore9.add(ChatColor.DARK_GRAY + "Valor de venda (Moedas negra):" + ChatColor.RED + " 375,00");
//            lore9.add(" ");
//            metaVenderes.setLore(lore9);
//            venderes.setItemMeta(metaVenderes);
//            inves.setItem(15, venderes);
//
//            return inves;
//        }
        return null;
    }
}
