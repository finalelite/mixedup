package com.mixedup.plots;

import com.mixedup.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class CasaAPI {

    private static HashMap<String, String> cache = new HashMap<>();

    public static HashMap<String, String> getCache() { return cache; }

    private static String getLocationSQL(String uuid) {

        try {

            PreparedStatement stm = MySql.con.prepareStatement("SELECT * FROM Casa_data WHERE UUID = ?");

            stm.setString(1, uuid);

            ResultSet rs = stm.executeQuery();

            if(rs.next()) {

                String location = rs.getString("Location");
                cache.put(uuid, location);
                return location;

            } else return null;

        } catch (SQLException e) {

            return null;

        }

    }

    public static String getLocation(String uuid) {

        if(cache.containsKey(uuid)) return cache.get(uuid); else return getLocationSQL(uuid);

    }

    public static void setCasa(String uuid, String location) {

        try {

            PreparedStatement stm = MySql.con.prepareStatement("INSERT INTO Casa_data(UUID, Location) VALUES (?, ?)");

            stm.setString(1, uuid);
            stm.setString(2, location);
            stm.executeUpdate();

            cache.put(uuid, location);

        } catch (SQLException e) {

            e.printStackTrace();

        }

    }

    public static void updateCasa(String uuid, String location) {

        try {

            PreparedStatement stm = MySql.con.prepareStatement("UPDATE Casa_data SET Location = ? WHERE UUID = ?");

            stm.setString(2, uuid);
            stm.setString(1, location);
            stm.executeUpdate();

            if(cache.containsKey(uuid)) {

                cache.remove(uuid);
                cache.put(uuid, location);

            }

        } catch (SQLException e) {

            e.printStackTrace();

        }

    }

    public static void removeCasa(String uuid) {

        try {

            PreparedStatement stm = MySql.con.prepareStatement("DELETE FROM Casa_data WHERE UUID = ?");

            stm.setString(1, uuid);
            stm.executeUpdate();
            cache.remove(uuid);

        } catch (SQLException e) {

            e.printStackTrace();

        }

    }

}
