package com.mixedup.plots;

import com.mixedup.Main;
import com.mixedup.apis.GuildaAPI;
import com.mixedup.apis.LastPositionAPI;
import com.mixedup.apis.RegionsSpawnAPI;
import com.mixedup.apis.TagAPI;
import com.mixedup.economy.CoinsAPI;
import com.mixedup.plots.espaconave.*;
import com.mixedup.plots.ilha.IlhaAPI;
import com.mixedup.plots.ilha.IlhaRegion;
import com.mixedup.plots.ilha.IlhasRegion;
import com.mixedup.plots.ilha.TypeIsland;
import com.mixedup.plots.terrain.*;
import com.mixedup.plots.utils.AddUtil;
import com.mixedup.plots.utils.InfoMoedaUtil;
import com.mixedup.plots.utils.RemoveUtil;
import com.mixedup.utils.AlternateColor;
import com.mixedup.utils.ScoreboardManager;
import com.mixedup.utils.WorldEditUtil;
import com.sk89q.worldedit.WorldEditException;
import org.bukkit.*;
import org.bukkit.block.Biome;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.IOException;

public class PlayerInteractInventoryCompra implements Listener {

    private static void updateBiome(final String UUID, final World world) {
        final int id = IlhaAPI.getIdIlha(UUID + "1");
        final Location location = new Location(world, 1000 * id - 650, 60, 350);

        int x = 0;
        for (int z = 0; z <= 300; z++) {
            location.getBlock().setBiome(Biome.PLAINS);

            location.add(0, 0, 1);
            if (z == 300 && x <= 300) {
                z = 0;
                x++;
                location.add(1, 0, -300);
            } else if (x > 300) {
                break;
            }
        }
    }

    @EventHandler
    public void onMoveUpdateBiome(PlayerMoveEvent event) {
        if (event.getPlayer().getWorld().getName().equalsIgnoreCase("ilhas-default")) {
            if (event.getTo().getChunk().getBlock(0, 0, 0).getBiome().equals(Biome.THE_VOID)) {
                event.getTo().getChunk().load();

                int x = 0;
                int z = 0;

                for (int i = 0; i <= 15; i++) {
                    event.getTo().getChunk().getBlock(x, event.getTo().getBlockY(), z).setBiome(Biome.PLAINS);

                    z++;
                    if (i == 15) {
                        i = 0;
                        z = 0;
                        x++;
                    }
                    if (x > 15) {
                        break;
                    }
                }
            }
        }
    }

    @EventHandler
    public void onInteract(final InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();
        final String UUID = player.getUniqueId().toString();

        if (event.getInventory().getName().equalsIgnoreCase("Amigos: ")) {
            event.setCancelled(true);

            if (GuildaAPI.getGuilda(UUID) != null) {
                if (GuildaAPI.getGuilda(UUID).equals("sanguinaria") || GuildaAPI.getGuilda(UUID).equals("nobre")) {
                    if (event.getSlot() == 12) {
                        final String[] amigos = TerrainInfosAPI.getAmigos(UUID + "1").split(":");
                        if (TagAPI.getTag(UUID).equals("Membro")) {
                            if (amigos.length < 5) {
                                if (AddUtil.get(UUID) == null) {
                                    new AddUtil(UUID, true).insert();
                                } else {
                                    AddUtil.get(UUID).setStatus(true);
                                }
                                player.sendMessage(ChatColor.GREEN + "\n \n * Por favor, digite o nick do usuário desejado a ser adiconado a lista de amigos.\n \n");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.getOpenInventory().close();

                                //1
                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                    @Override
                                    public void run() { // 1
                                        if (AddUtil.get(UUID).getStatus() == true) {
                                            AddUtil.get(UUID).setStatus(false);
                                            player.sendMessage(ChatColor.RED + " * Ops, você demorou muito para informar um nick para adicionarmos a sua lista de amigos. Então cancelamos está ação!");
                                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        }
                                    }
                                }, 400L);
                            } else {
                                player.sendMessage(ChatColor.RED + "\n \n * Ops, você atingiu limite de amigos no seu terreno.\n \n" + ChatColor.DARK_GRAY + " - Adquira " + ChatColor.GREEN + "VIP" + ChatColor.DARK_GRAY + " e obtenha vantagens como estas em:\n" + ChatColor.YELLOW + " - www.finalelite.com.br\n \n");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        } else {
                            if (amigos.length < 10) {
                                if (AddUtil.get(UUID) == null) {
                                    new AddUtil(UUID, true).insert();
                                } else {
                                    AddUtil.get(UUID).setStatus(true);
                                }
                                player.sendMessage(ChatColor.GREEN + "\n \n * Por favor, digite o nick do usuário desejado a ser adiconado a lista de amigos.\n \n");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.getOpenInventory().close();

                                //2
                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                    @Override
                                    public void run() { // 2
                                        if (AddUtil.get(UUID).getStatus() == true) {
                                            AddUtil.get(UUID).setStatus(false);
                                            player.sendMessage(ChatColor.RED + " * Ops, você demorou muito para informar um nick para adicionarmos a sua lista de amigos. Então cancelamos está ação!");
                                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        }
                                    }
                                }, 400L);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, você atingiu limite de amigos no seu terreno.");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        }
                    }

                    if (event.getSlot() == 14) {
                        final String[] amigos = TerrainInfosAPI.getAmigos(UUID + "1").split(":");
                        if (amigos.length >= 1 && !amigos[0].equals("NULL")) {
                            if (RemoveUtil.get(UUID) == null) {
                                new RemoveUtil(UUID, true).insert();
                            } else {
                                RemoveUtil.get(UUID).setStatus(true);
                            }
                            player.sendMessage(ChatColor.GREEN + "\n \n * Por favor, digite o nick do usuário desejado a ser removido da lista de amigos.\n \n");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.getOpenInventory().close();

                            //3
                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() { // 3
                                    if (RemoveUtil.get(UUID).getStatus() == true) {
                                        RemoveUtil.get(UUID).setStatus(false);
                                        player.sendMessage(ChatColor.RED + " * Ops, você demorou muito para informar um nick para removermos de sua lista de amigos. Então cancelamos está ação!");
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    }
                                }
                            }, 400L);
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, você não contém nenhum amigo para efetuar isto.");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    }
                } else if (GuildaAPI.getGuilda(UUID) != null && GuildaAPI.getGuilda(UUID).equals("ancia")) {
                    if (event.getSlot() == 12) {
                        final String[] amigos = IlhaAPI.getAmigos(UUID + "1").split(":");
                        if (TagAPI.getTag(UUID).equals("Membro")) {
                            if (amigos.length < 5) {
                                if (AddUtil.get(UUID) == null) {
                                    new AddUtil(UUID, true).insert();
                                } else {
                                    AddUtil.get(UUID).setStatus(true);
                                }
                                player.sendMessage(ChatColor.GREEN + "\n \n * Por favor, digite o nick do usuário desejado a ser adiconado a lista de amigos.\n \n");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.getOpenInventory().close();

                                //4
                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                    @Override
                                    public void run() { // 4
                                        if (AddUtil.get(UUID).getStatus() == true) {
                                            AddUtil.get(UUID).setStatus(false);
                                            player.sendMessage(ChatColor.RED + " * Ops, você demorou muito para informar um nick para adicionarmos a sua lista de amigos. Então cancelamos está ação!");
                                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        }
                                    }
                                }, 400L);
                            } else {
                                player.sendMessage(ChatColor.RED + "\n \n * Ops, você atingiu limite de amigos em sua ilha.\n \n" + ChatColor.DARK_GRAY + " - Adquira " + ChatColor.GREEN + "VIP" + ChatColor.DARK_GRAY + " e obtenha vantagens como estas em:\n" + ChatColor.YELLOW + " - www.finalelite.com.br\n \n");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        } else {
                            if (amigos.length < 10) {
                                if (AddUtil.get(UUID) == null) {
                                    new AddUtil(UUID, true).insert();
                                } else {
                                    AddUtil.get(UUID).setStatus(true);
                                }
                                player.sendMessage(ChatColor.GREEN + "\n \n * Por favor, digite o nick do usuário desejado a ser adiconado a lista de amigos.\n \n");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.getOpenInventory().close();

                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                    @Override
                                    public void run() { // 5
                                        if (AddUtil.get(UUID).getStatus() == true) {
                                            AddUtil.get(UUID).setStatus(false);
                                            player.sendMessage(ChatColor.RED + " * Ops, você demorou muito para informar um nick para adicionarmos a sua lista de amigos. Então cancelamos está ação!");
                                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        }
                                    }
                                }, 400L);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, você atingiu limite de amigos em sua ilha.");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        }
                    }

                    if (event.getSlot() == 14) {
                        final String[] amigos = IlhaAPI.getAmigos(UUID + "1").split(":");
                        if (amigos.length >= 1 && !amigos[0].equals("NULL")) {
                            if (RemoveUtil.get(UUID) == null) {
                                new RemoveUtil(UUID, true).insert();
                            } else {
                                RemoveUtil.get(UUID).setStatus(true);
                            }
                            player.sendMessage(ChatColor.GREEN + "\n \n * Por favor, digite o nick do usuário desejado a ser removido da lista de amigos.\n \n");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.getOpenInventory().close();

                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() { // 6
                                    if (RemoveUtil.get(UUID).getStatus() == true) {
                                        RemoveUtil.get(UUID).setStatus(false);
                                        player.sendMessage(ChatColor.RED + " * Ops, você demorou muito para informar um nick para removermos de sua lista de amigos. Então cancelamos está ação!");
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    }
                                }
                            }, 400L);
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, você não contém nenhum amigo para efetuar isto.");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    }
                }
            }
        }

        //CONFIRMANDO COMPRA ESPAÇONAVE
        if (event.getInventory().getName().equalsIgnoreCase("Confirmar compra espaçonave: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 12) {
                if (GuildaAPI.getGuilda(UUID).equals("sanguinaria")) {
                    //player.sendMessage(ChatColor.RED + " * Temporariamente desabilitado!");
                    //                    player.getOpenInventory().close();
                    //                    return;
                    if (InfoMoedaUtil.get(UUID).getMoeda().equals("comum")) {
                        if (CoinsAPI.getCoins(UUID) >= 30000) {
                            if (EspaçonaveAPI.getIdEspaçonave(UUID + "1") == 0) {
                                EspaçonaveAPI.setEspaçonave(UUID + "1");
                                EspaçonaveAPI.setInfos(UUID + "1", "comum");
                                CoinsAPI.updateCoins(UUID, CoinsAPI.getCoins(UUID) - 30000);
//                                val sb = Main.scoreboardManager;
//                                if (sb.hasSquadScoreboard(player)) {
//
//                                } else {
//                                    sb.getDefaultScoreboard().updateEntry(player, "coins");
//                                }
                                final String loc1 = (1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 536) + ":" + "0" + ":" + "464";
                                final String loc2 = (1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 466) + ":" + "256" + ":" + "534";
                                final EspaconaveRegion espaçonave = new EspaconaveRegion(UUID + "1", loc1, loc2);
                                EspaçonavesRegion.getRegions().add(espaçonave);

                                final String loc1Schem = (1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 515) + ":" + "70" + ":" + "485";
                                final String loc2Schem = (1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 487) + ":" + "89" + ":" + "513";
                                final EspaçonaveSchem schem = new EspaçonaveSchem(UUID + "1", loc1Schem, loc2Schem);
                                EspaçonaveSchemList.getRegions().add(schem);

                                FarmAPI.setFarm(UUID + "1", ChatColor.GREEN + "Plot-1", true);
                                FarmAPI.setFarm(UUID + "2", ChatColor.GREEN + "Plot-2", false);
                                FarmAPI.setFarm(UUID + "3", ChatColor.GREEN + "Plot-3", false);
                                FarmAPI.setFarm(UUID + "4", ChatColor.GREEN + "Plot-4", false);

                                //DEPRECATED
                                final String location1 = (1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 473) + ":71:507";
                                MobspawnAPI.setInfo(UUID + "1", location1);
                                final String location2 = (1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 471) + ":71:499";
                                MobspawnAPI.setInfo(UUID + "2", location2);
                                final String location3 = (1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 473) + ":71:491";
                                MobspawnAPI.setInfo(UUID + "3", location3);
                                //

                                player.sendMessage(AlternateColor.alternate("\n \n&e * Espaçonave adquirida com sucesso.\n&8 - Foram debitados de sua conta 30.000,00 moedas\n&8 - Para mais informações sobre a sua espaçonave..\n&8 - Utilize /ajuda espaçonave ou /espaçonave config\n \n&a * Teletransportando em 10 segundos.\n \n"));
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                player.getOpenInventory().close();

                                final String pos1 = (1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 493) + ":" + "71" + ":" + "473";
                                final String pos2 = (1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 509) + ":" + "74" + ":" + "468";
                                final LojaEspaconave lojaRegion = new LojaEspaconave(UUID + "1", pos1, pos2);
                                LojasEspaconave.getRegions().add(lojaRegion);

                                final double x = 1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 501 + 0.5;
                                final double y = 70.5;
                                final double z = 499.5;
                                final Location location = new Location(Bukkit.getWorld("espaconave"), x, y, z);

                                EspaconaveRegion.loadEspaçonaves(location, UUID);

                                final String loc = "espaconave" + ":" + (1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 501 + 0.5) + ":" + "70" + ":" + "499";
                                LastPositionAPI.updateLocation(UUID, loc);

                                //Local spawn espaçonave
                                final double xSPAWN = 1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 529 + 0.5;
                                final double ySPAWN = 71.1;
                                final double zSPAWN = 499.5;
                                final Float yaw = -90f;
                                final Float pitch = 0f;
                                final Location locationSPAWN = new Location(Bukkit.getWorld("espaconave"), xSPAWN, ySPAWN, zSPAWN, yaw, pitch);
                                CasaAPI.setCasa(UUID, "espaconave:" + xSPAWN + ":" + ySPAWN + ":" + zSPAWN + ":" + yaw + ":" + pitch);
                                //7
                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                    @Override
                                    public void run() { // 10
                                        player.teleport(locationSPAWN);
                                    }
                                }, 200L);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, aparentemente você já contém alguma espaçonave.");
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, você não contém dinheiro para efetuar esta compra =(");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else if (InfoMoedaUtil.get(UUID).getMoeda().equals("negra")) {
                        if (CoinsAPI.getBlackCoins(UUID) >= 700) {
                            if (EspaçonaveAPI.getIdEspaçonave(UUID + "1") == 0) {
                                EspaçonaveAPI.setEspaçonave(UUID + "1");
                                EspaçonaveAPI.setInfos(UUID + "1", "comum");
                                CoinsAPI.updateBlackCoins(UUID, CoinsAPI.getBlackCoins(UUID) - 700);
//                                val sb = Main.scoreboardManager;
//                                if (sb.hasSquadScoreboard(player)) {
//
//                                } else {
//                                    sb.getDefaultScoreboard().updateEntry(player, "coins");
//                                }

                                final String loc1 = (1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 536) + ":" + "0" + ":" + "464";
                                final String loc2 = (1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 466) + ":" + "256" + ":" + "534";
                                final EspaconaveRegion espaçonave = new EspaconaveRegion(UUID + "1", loc1, loc2);
                                EspaçonavesRegion.getRegions().add(espaçonave);

                                final String loc1Schem = (1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 515) + ":" + "70" + ":" + "485";
                                final String loc2Schem = (1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 487) + ":" + "89" + ":" + "513";
                                final EspaçonaveSchem schem = new EspaçonaveSchem(UUID + "1", loc1Schem, loc2Schem);
                                EspaçonaveSchemList.getRegions().add(schem);

                                FarmAPI.setFarm(UUID + "1", ChatColor.GREEN + "Plot-1", true);
                                FarmAPI.setFarm(UUID + "2", ChatColor.GREEN + "Plot-2", false);
                                FarmAPI.setFarm(UUID + "3", ChatColor.GREEN + "Plot-3", false);
                                FarmAPI.setFarm(UUID + "4", ChatColor.GREEN + "Plot-4", false);

                                //DEPRECATED
                                final String location1 = (1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 473) + ":71:507";
                                MobspawnAPI.setInfo(UUID + "1", location1);
                                final String location2 = (1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 471) + ":71:499";
                                MobspawnAPI.setInfo(UUID + "2", location2);
                                final String location3 = (1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 473) + ":71:491";
                                MobspawnAPI.setInfo(UUID + "3", location3);
                                //

                                player.sendMessage(AlternateColor.alternate("\n \n&e * Espaçonave adquirida com sucesso.\n&8 - Foram debitados de sua conta 700,00 moedas negras\n&8 - Para mais informações sobre a sua espaçonave..\n&8 - Utilize /ajuda espaçonave ou /espaçonave config\n \n&a * Teletransportando em 10 segundos.\n \n"));
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                player.getOpenInventory().close();

                                final String pos1 = (1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 493) + ":" + "71" + ":" + "473";
                                final String pos2 = (1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 509) + ":" + "74" + ":" + "468";
                                final LojaEspaconave lojaRegion = new LojaEspaconave(UUID + "1", pos1, pos2);
                                LojasEspaconave.getRegions().add(lojaRegion);

                                final double x = 1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 501 + 0.5;
                                final double y = 70.5;
                                final double z = 499.5;
                                final Location location = new Location(Bukkit.getWorld("espaconave"), x, y, z);

                                EspaconaveRegion.loadEspaçonaves(location, UUID);

                                final String loc = "espaconave" + ":" + (1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 501 + 0.5) + ":" + "73" + ":" + "499";
                                LastPositionAPI.updateLocation(UUID, loc);

                                //Local spawn espaçonave
                                final double xSPAWN = 1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 529 + 0.5;
                                final double ySPAWN = 71.1;
                                final double zSPAWN = 499.5;
                                final Float yaw = -90f;
                                final Float pitch = 0f;
                                final Location locationSPAWN = new Location(Bukkit.getWorld("espaconave"), xSPAWN, ySPAWN, zSPAWN, yaw, pitch);
                                CasaAPI.setCasa(UUID, "espaconave:" + xSPAWN + ":" + ySPAWN + ":" + zSPAWN + ":" + yaw + ":" + pitch);
                                //8
                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                    @Override
                                    public void run() { // 11
                                        player.teleport(locationSPAWN);
                                    }
                                }, 200L);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, aparentemente você já contém alguma espaçonave.");
                            }
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você não contém dinheiro para efetuar esta compra =(");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
            }

            if (event.getSlot() == 14) {
                player.sendMessage(ChatColor.RED + " * Evento cancelado.");
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                player.getOpenInventory().close();
            }
        }
        //------------------

        //CONFIRMANDO COMPRA TERRENO
        if (event.getInventory().getName().equalsIgnoreCase("Confirmar compra terreno: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 12) {
                if (GuildaAPI.getGuilda(UUID).equals("nobre")) {
                    if (player.getWorld().getName().equalsIgnoreCase("Kepler-438b") || player.getWorld().getName().equalsIgnoreCase("Gliesse-667-Cc") || player.getWorld().getName().equalsIgnoreCase("KOI-3010.01") || player.getWorld().getName().equalsIgnoreCase("HD-85512-b")) {
                        if (InfoMoedaUtil.get(UUID).getMoeda().equals("comum")) {
                            if (CoinsAPI.getCoins(UUID) >= 20000) {
                                if (TerrainsUtil.verifySpawnNext(player.getLocation(), 160) == false) {
                                    if (TerrainsUtil.verify(player.getLocation(), 160) == null) {
                                        if (TerrainAPI.getTerrainPos1(UUID + "1") == null) {
                                            final String loc1 = player.getWorld().getName() + ":" + (player.getLocation().getBlockX() + 80) + ":" + 0 + ":" + (player.getLocation().getBlockZ() + 80);
                                            final String loc2 = player.getWorld().getName() + ":" + (player.getLocation().getBlockX() - 80) + ":" + 256 + ":" + (player.getLocation().getBlockZ() - 80);
                                            final RegionTr terrain = new RegionTr(UUID + "1", loc1, loc2);
                                            RegionsTr.getRegions().add(terrain);

                                            CasaAPI.setCasa(UUID, player.getWorld().getName() + ":" + player.getLocation().getX() + ":" + player.getLocation().getY() + ":" + player.getLocation().getZ() + ":" + player.getLocation().getYaw() + ":" + player.getLocation().getPitch());
                                            TerrainAPI.setTerrain(UUID + "1", loc1, loc2);
                                            TerrainInfosAPI.setInfos(UUID + "1", "comum");
                                            CoinsAPI.updateCoins(UUID, CoinsAPI.getCoins(UUID) - 20000);
//                                            val sb = Main.scoreboardManager;
//                                            if (sb.hasSquadScoreboard(player)) {
//
//                                            } else {
//                                                sb.getDefaultScoreboard().updateEntry(player, "coins");
//                                            }
                                            player.sendMessage(AlternateColor.alternate("\n \n&e * Terreno adquirido com sucesso.\n&8 - Foram debitados de sua conta 20.000,00 moedas\n&8 - Para mais informações sobre o seu terreno..\n&8 - Utilize /ajuda terreno ou /terreno config\n \n"));
                                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                            player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                            player.getOpenInventory().close();
                                        } else {
                                            player.sendMessage(ChatColor.RED + " * Ops, aparentemente você já contém algum terreno.");
                                        }
                                    } else {
                                        player.sendMessage(ChatColor.RED + "Ops, existe algum terreno muito próximo a esta região! Tente novamente em outra localidade.");
                                        player.getOpenInventory().close();
                                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                    }
                                } else {
                                    player.sendMessage(ChatColor.RED + "Ops, spawn próximo de mais! Tente novamente em outra localidade.");
                                    player.getOpenInventory().close();
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + "Ops, você não contém dinheiro para efetuar esta compra =(");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        } else if (InfoMoedaUtil.get(UUID).getMoeda().equals("negra")) {
                            if (CoinsAPI.getBlackCoins(UUID) >= 500) {
                                if (TerrainsUtil.verifySpawnNext(player.getLocation(), 160) == false) {
                                    if (TerrainsUtil.verify(player.getLocation(), 160) == null) {
                                        if (TerrainAPI.getTerrainPos1(UUID + "1") == null) {
                                            final String loc1 = player.getWorld().getName() + ":" + (player.getLocation().getBlockX() + 80) + ":" + 0 + ":" + (player.getLocation().getBlockZ() + 80);
                                            final String loc2 = player.getWorld().getName() + ":" + (player.getLocation().getBlockX() - 80) + ":" + 256 + ":" + (player.getLocation().getBlockZ() - 80);
                                            final RegionTr terrain = new RegionTr(UUID + "1", loc1, loc2);
                                            RegionsTr.getRegions().add(terrain);

                                            CasaAPI.setCasa(UUID, player.getWorld().getName() + ":" + player.getLocation().getX() + ":" + player.getLocation().getY() + ":" + player.getLocation().getZ() + ":" + player.getLocation().getYaw() + ":" + player.getLocation().getPitch());
                                            TerrainAPI.setTerrain(UUID + "1", loc1, loc2);
                                            TerrainInfosAPI.setInfos(UUID + "1", "negra");
                                            CoinsAPI.updateBlackCoins(UUID, CoinsAPI.getBlackCoins(UUID) - 500);
//                                            val sb = Main.scoreboardManager;
//                                            if (sb.hasSquadScoreboard(player)) {
//
//                                            } else {
//                                                sb.getDefaultScoreboard().updateEntry(player, "blackcoins");
//                                            }
                                            player.sendMessage(AlternateColor.alternate("\n \n&e * Terreno adquirido com sucesso.\n&8 - Foram debitados de sua conta 500,00 moedas negras\n&8 - Para mais informações sobre o seu terreno..\n&8 - Utilize /ajuda terreno ou /terreno config\n \n"));
                                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                            player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                            player.getOpenInventory().close();
                                        } else {
                                            player.sendMessage(ChatColor.RED + " * Ops, aparentemente você já contém algum terreno.");
                                        }
                                    } else {
                                        player.sendMessage(ChatColor.RED + "Ops, existe algum terreno muito próximo a esta região! Tente novamente em outra localidade.");
                                        player.getOpenInventory().close();
                                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                    }
                                } else {
                                    player.sendMessage(ChatColor.RED + "Ops, spawn próximo de mais! Tente novamente em outra localidade.");
                                    player.getOpenInventory().close();
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + "Ops, você não contém dinheiro para efetuar esta compra =(");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, você não pode adquirir um terreno neste local!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else if (GuildaAPI.getGuilda(UUID).equals("sanguinaria")) {
                    if (player.getWorld().getName().equalsIgnoreCase("skygrid")) {
                        if (InfoMoedaUtil.get(UUID).getMoeda().equals("comum")) {
                            if (CoinsAPI.getCoins(UUID) >= 20000) {
                                if (TerrainsUtil.verifySpawnNext(player.getLocation(), 160) == false) {
                                    if (TerrainsUtil.verify(player.getLocation(), 160) == null) {
                                        if (TerrainAPI.getTerrainPos1(UUID + "1") == null) {
                                            final String loc1 = player.getWorld().getName() + ":" + (player.getLocation().getBlockX() + 80) + ":" + 0 + ":" + (player.getLocation().getBlockZ() + 80);
                                            final String loc2 = player.getWorld().getName() + ":" + (player.getLocation().getBlockX() - 80) + ":" + 256 + ":" + (player.getLocation().getBlockZ() - 80);
                                            final RegionTr terrain = new RegionTr(UUID + "1", loc1, loc2);
                                            RegionsTr.getRegions().add(terrain);

                                            CasaAPI.setCasa(UUID, player.getWorld().getName() + ":" + player.getLocation().getX() + ":" + player.getLocation().getY() + ":" + player.getLocation().getZ() + ":" + player.getLocation().getYaw() + ":" + player.getLocation().getPitch());
                                            TerrainAPI.setTerrain(UUID + "1", loc1, loc2);
                                            TerrainInfosAPI.setInfos(UUID + "1", "comum");
                                            CoinsAPI.updateCoins(UUID, CoinsAPI.getCoins(UUID) - 20000);
//                                            val sb = Main.scoreboardManager;
//                                            if (sb.hasSquadScoreboard(player)) {
//
//                                            } else {
//                                                sb.getDefaultScoreboard().updateEntry(player, "coins");
//                                            }
                                            player.sendMessage(AlternateColor.alternate("\n \n&e * Terreno adquirido com sucesso.\n&8 - Foram debitados de sua conta 20.000,00 moedas\n&8 - Para mais informações sobre o seu terreno..\n&8 - Utilize /ajuda terreno ou /terreno config\n \n"));
                                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                            player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                            player.getOpenInventory().close();
                                        } else {
                                            player.sendMessage(ChatColor.RED + " * Ops, aparentemente você já contém algum terreno.");
                                        }
                                    } else {
                                        player.sendMessage(ChatColor.RED + "Ops, existe algum terreno muito próximo a esta região! Tente novamente em outra localidade.");
                                        player.getOpenInventory().close();
                                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                    }
                                } else {
                                    player.sendMessage(ChatColor.RED + "Ops, spawn próximo de mais! Tente novamente em outra localidade.");
                                    player.getOpenInventory().close();
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + "Ops, você não contém dinheiro para efetuar esta compra =(");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        } else if (InfoMoedaUtil.get(UUID).getMoeda().equals("negra")) {
                            if (CoinsAPI.getBlackCoins(UUID) >= 500) {
                                if (TerrainsUtil.verifySpawnNext(player.getLocation(), 160) == false) {
                                    if (TerrainsUtil.verify(player.getLocation(), 160) == null) {
                                        if (TerrainAPI.getTerrainPos1(UUID + "1") == null) {
                                            final String loc1 = player.getWorld().getName() + ":" + (player.getLocation().getBlockX() + 80) + ":" + 0 + ":" + (player.getLocation().getBlockZ() + 80);
                                            final String loc2 = player.getWorld().getName() + ":" + (player.getLocation().getBlockX() - 80) + ":" + 256 + ":" + (player.getLocation().getBlockZ() - 80);
                                            final RegionTr terrain = new RegionTr(UUID + "1", loc1, loc2);
                                            RegionsTr.getRegions().add(terrain);

                                            CasaAPI.setCasa(UUID, player.getWorld().getName() + ":" + player.getLocation().getX() + ":" + player.getLocation().getY() + ":" + player.getLocation().getZ() + ":" + player.getLocation().getYaw() + ":" + player.getLocation().getPitch());
                                            TerrainAPI.setTerrain(UUID + "1", loc1, loc2);
                                            TerrainInfosAPI.setInfos(UUID + "1", "negra");
                                            CoinsAPI.updateBlackCoins(UUID, CoinsAPI.getBlackCoins(UUID) - 500);
//                                            val sb = Main.scoreboardManager;
//                                            if (sb.hasSquadScoreboard(player)) {
//
//                                            } else {
//                                                sb.getDefaultScoreboard().updateEntry(player, "blackcoins");
//                                            }
                                            player.sendMessage(AlternateColor.alternate("\n \n&e * Terreno adquirido com sucesso.\n&8 - Foram debitados de sua conta 500,00 moedas negras\n&8 - Para mais informações sobre o seu terreno..\n&8 - Utilize /ajuda terreno ou /terreno config\n \n"));
                                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                            player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                            player.getOpenInventory().close();
                                        } else {
                                            player.sendMessage(ChatColor.RED + " * Ops, aparentemente você já contém algum terreno.");
                                        }
                                    } else {
                                        player.sendMessage(ChatColor.RED + "Ops, existe algum terreno muito próximo a esta região! Tente novamente em outra localidade.");
                                        player.getOpenInventory().close();
                                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                    }
                                } else {
                                    player.sendMessage(ChatColor.RED + "Ops, spawn próximo de mais! Tente novamente em outra localidade.");
                                    player.getOpenInventory().close();
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + "Ops, você não contém dinheiro para efetuar esta compra =(");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, você não pode adquirir um terreno neste local!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
            }

            if (event.getSlot() == 14) {
                player.sendMessage(ChatColor.RED + " * Evento cancelado.");
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                player.getOpenInventory().close();
            }
        }
        //------------------

        //CONFIRMANDO COMPRA ILHA
        if (event.getInventory().getName().equalsIgnoreCase("Confirmar compra ilha: ")) {
            event.setCancelled(true);

            //if (event.getSlot() == 12) {
            //                player.sendMessage(ChatColor.RED + " * Temporariamente desabilitado!");
            //                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            //                return;
            //            }

            if (event.getSlot() == 12) {
                if (GuildaAPI.getGuilda(UUID).equals("ancia")) {
                    if (InfoMoedaUtil.get(UUID).getMoeda().equals("comum")) {
                        if (CoinsAPI.getCoins(UUID) >= 25000) {
                            if (IlhaAPI.getIdIlha(UUID + "1") == 0) {
                                IlhaAPI.setIlha(UUID + "1");
                                IlhaAPI.setInfos(UUID + "1", "comum");

                                String pos1 = (1000 * IlhaAPI.getIdIlha(UUID + "1") - 650) + ":" + "0" + ":" + "650";
                                String pos2 = (1000 * IlhaAPI.getIdIlha(UUID + "1") - 350) + ":" + "256" + ":" + "350";
                                IlhaRegion ilha = new IlhaRegion(UUID + "1", pos1, pos2);
                                IlhasRegion.getRegions().add(ilha);

                                CoinsAPI.updateCoins(UUID, CoinsAPI.getCoins(UUID) - 25000);
                                ScoreboardManager sb = Main.scoreboardManager;
                                if (sb.hasSquadScoreboard(player)) {

                                } else {
                                    sb.getDefaultScoreboard().updateEntry(player, "coins");
                                }
                                player.sendMessage(AlternateColor.alternate("\n \n&e * Ilha adquirida com sucesso.\n&8 - Foram debitados de sua conta 25.000,00 moedas\n&8 - Para mais informações sobre a sua ilha..\n&8 - Utilize /ajuda ilha ou /ilha config\n \n&a * Teletransportando em 5 segundos.\n \n"));
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                player.getOpenInventory().close();

                                double x = 1000 * IlhaAPI.getIdIlha(UUID + "1") - 560 + 0.5;
                                double y = 69.1;
                                double z = 584.5;
                                Float yaw = -85f;
                                Float pitch = -0f;
                                Location location = new Location(Bukkit.getWorld("ilhas-default"), x, y, z, yaw, pitch);
                                CasaAPI.setCasa(UUID, "ilhas-default:" + String.valueOf(x) + ":" + String.valueOf(y) + ":" + String.valueOf(z) + ":" + String.valueOf(yaw) + ":" + String.valueOf(pitch));

                                new BukkitRunnable() {
                                    public void run() {
                                        try {
                                            location.getChunk().load();
                                            WorldEditUtil.paste(WorldEditUtil.load(Main.plugin.getDataFolder() + File.separator + "WorldEdit/schematics/ilha"), true, location);
                                        } catch (WorldEditException e) {
                                            e.printStackTrace();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }.runTaskLater(Main.getInstance(), 0);

                                String loc = "ilhas-default" + ":" + (1000 * IlhaAPI.getIdIlha(UUID + "1") - 560 + 0.5) + ":" + "69" + ":" + "584";
                                LastPositionAPI.updateLocation(UUID, loc);
                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                    @Override
                                    public void run() {
                                        player.teleport(location);
                                    }
                                }, 100L);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, aparentemente você já contém alguma ilha.");
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, você não contém dinheiro para efetuar esta compra =(");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else if (InfoMoedaUtil.get(UUID).getMoeda().equals("negra")) {
                        if (CoinsAPI.getBlackCoins(UUID) >= 600) {
                            if (IlhaAPI.getIdIlha(UUID + "1") == 0) {
                                IlhaAPI.setIlha(UUID + "1");
                                IlhaAPI.setInfos(UUID + "1", "negra");

                                String pos1 = (1000 * IlhaAPI.getIdIlha(UUID + "1") - 650) + ":" + "0" + ":" + 650;
                                String pos2 = (1000 * IlhaAPI.getIdIlha(UUID + "1") - 350) + ":" + "256" + ":" + 350;
                                IlhaRegion ilha = new IlhaRegion(UUID + "1", pos1, pos2);
                                IlhasRegion.getRegions().add(ilha);

                                CoinsAPI.updateBlackCoins(UUID, CoinsAPI.getBlackCoins(UUID) - 600);
                                ScoreboardManager sb = Main.scoreboardManager;
                                if (sb.hasSquadScoreboard(player)) {

                                } else {
                                    sb.getDefaultScoreboard().updateEntry(player, "blackcoins");
                                }

                                player.sendMessage(AlternateColor.alternate("\n \n&e * Ilha adquirida com sucesso.\n&8 - Foram debitados de sua conta 600,00 moedas negras\n&8 - Para mais informações sobre a sua ilha..\n&8 - Utilize /ajuda ilha ou /ilha config\n \n&a * Teletransportando em 5 segundos.\n \n"));
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                player.getOpenInventory().close();

                                double x = 1000 * IlhaAPI.getIdIlha(UUID + "1") - 560 + 0.5;
                                double y = 69.1;
                                double z = 584.5;
                                Float yaw = -85f;
                                Float pitch = -0f;
                                Location location = new Location(Bukkit.getWorld("ilhas-default"), x, y, z, yaw, pitch);

                                CasaAPI.setCasa(UUID, "ilhas-default:" + String.valueOf(x) + ":" + String.valueOf(y) + ":" + String.valueOf(z) + ":" + String.valueOf(yaw) + ":" + String.valueOf(pitch));


                                new BukkitRunnable() {
                                    public void run() {
                                        try {
                                            location.getChunk().load();
                                            WorldEditUtil.paste(WorldEditUtil.load(Main.plugin.getDataFolder() + File.separator + "WorldEdit/schematics/ilha"), true, location);
                                        } catch (WorldEditException e) {
                                            e.printStackTrace();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }.runTaskLater(Main.getInstance(), 0);

                                String loc = player.getWorld().getName() + ":" + String.valueOf(player.getLocation().getX()) + ":" + String.valueOf(player.getLocation().getY()) + ":" + String.valueOf(player.getLocation().getZ());
                                LastPositionAPI.updateLocation(UUID, loc);
                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                    @Override
                                    public void run() {
                                        player.teleport(location);
                                    }
                                }, 100L);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, aparentemente você já contém alguma ilha.");
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, você não contém dinheiro para efetuar esta compra =(");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    }
                }
            }

            if (event.getSlot() == 14) {
                player.sendMessage(ChatColor.RED + " * Evento cancelado.");
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                player.getOpenInventory().close();
            }
        }
        //------------------

        //MENU CONFIGURAÇÕES TERRENO
        if (event.getInventory().getName().equalsIgnoreCase("Configuração terreno: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 10) {
                if (TerrainInfosAPI.getPvP(UUID + "1") == true) {
                    if (TerrainInfosAPI.getPvPStatus(UUID + "1") == true) {
                        TerrainInfosAPI.updatePvPStatus(UUID + "1", false);
                        player.sendMessage(ChatColor.GREEN + " * Pvp desabilitado com sucesso.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        //9
                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                player.openInventory(InventoryTerrenos.configTerreno(UUID + "1"));
                            }
                        }, 5L);
                    } else {
                        TerrainInfosAPI.updatePvPStatus(UUID + "1", true);
                        player.sendMessage(ChatColor.GREEN + " * Pvp habilitado com sucesso.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        //10
                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                player.openInventory(InventoryTerrenos.configTerreno(UUID + "1"));
                            }
                        }, 5L);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + " * Para habilitar está opção você deve adquirir upgrade do modo pvp em /terreno (Opção upgrades, pvp).");
                }
            }

            if (event.getSlot() == 12) {
                if (TerrainInfosAPI.getAcesso(UUID + "1") == true) {
                    TerrainInfosAPI.updateAcesso(UUID + "1", false);
                    player.sendMessage(ChatColor.GREEN + " * Acesso desabilitado com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    //11
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryTerrenos.configTerreno(UUID + "1"));
                        }
                    }, 5L);
                } else {
                    TerrainInfosAPI.updateAcesso(UUID + "1", true);
                    player.sendMessage(ChatColor.GREEN + " * Acesso habilitado com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    //12
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryTerrenos.configTerreno(UUID + "1"));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 14) {
                if (TerrainInfosAPI.getTeletransporte(UUID + "1") == true) {
                    TerrainInfosAPI.updateVisitas(UUID + "1", false);
                    player.sendMessage(ChatColor.GREEN + " * Visitas desabilitadas com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    //13
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryTerrenos.configTerreno(UUID + "1"));
                        }
                    }, 5L);
                } else {
                    TerrainInfosAPI.updateVisitas(UUID + "1", true);
                    player.sendMessage(ChatColor.GREEN + " * Visita habilitada com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    //14
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryTerrenos.configTerreno(UUID + "1"));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 16) {
                //15
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventoryTerrenos.invAmigos(UUID));
                    }
                }, 5L);
            }
        }
        //------------------

        //MENU CONFIGURAÇÕES ILHA
        if (event.getInventory().getName().equalsIgnoreCase("Configuração ilha: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 11) {
                if (IlhaAPI.getPvP(UUID + "1") == true) {
                    if (IlhaAPI.getPvPStatus(UUID + "1") == true) {
                        IlhaAPI.updateStatusPvP(UUID + "1", false);
                        player.sendMessage(ChatColor.GREEN + " * Pvp desabilitado com sucesso.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        //16
                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                player.openInventory(InventoryTerrenos.configIlha(UUID + "1"));
                            }
                        }, 5L);
                    } else {
                        IlhaAPI.updateStatusPvP(UUID + "1", true);
                        player.sendMessage(ChatColor.GREEN + " * Pvp habilitado com sucesso.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        //17
                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                player.openInventory(InventoryTerrenos.configIlha(UUID + "1"));
                            }
                        }, 5L);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + " * Para habilitar está opção você deve adquirir upgrade do modo pvp em /ilha (Opção upgrades, pvp).");
                }
            }

            if (event.getSlot() == 13) {
                if (IlhaAPI.getVisitas(UUID + "1") == true) {
                    IlhaAPI.updateVisitas(UUID + "1", false);
                    player.sendMessage(ChatColor.GREEN + " * Visitas desabilitadas com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    //18
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryTerrenos.configIlha(UUID + "1"));
                        }
                    }, 5L);
                } else {
                    IlhaAPI.updateVisitas(UUID + "1", true);
                    player.sendMessage(ChatColor.GREEN + " * Visita habilitada com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    //19
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryTerrenos.configIlha(UUID + "1"));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 15) {
                //20
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventoryTerrenos.invAmigos(UUID));
                    }
                }, 5L);
            }
        }
        //------------------

        //MENU CONFIGURAÇÕES ESPAÇONAVE
        if (event.getInventory().getName().equalsIgnoreCase("Configuração espaçonave: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 11) {
                if (EspaçonaveAPI.getPvP(UUID + "1") == true) {
                    if (EspaçonaveAPI.getPvPStatus(UUID + "1") == true) {
                        EspaçonaveAPI.updateStatusPvP(UUID + "1", false);
                        player.sendMessage(ChatColor.GREEN + " * Pvp desabilitado com sucesso.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        //21
                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                player.openInventory(InventoryTerrenos.configEspaçonave(UUID + "1"));
                            }
                        }, 5L);
                    } else {
                        EspaçonaveAPI.updateStatusPvP(UUID + "1", true);
                        player.sendMessage(ChatColor.GREEN + " * Pvp habilitado com sucesso.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        //22
                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                player.openInventory(InventoryTerrenos.configEspaçonave(UUID + "1"));
                            }
                        }, 5L);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + " * Para habilitar está opção você deve adquirir upgrade do modo pvp em /espaçonave (Opção upgrades, pvp).");
                }
            }

            if (event.getSlot() == 13) {
                if (EspaçonaveAPI.getVisitas(UUID + "1") == true) {
                    EspaçonaveAPI.updateVisitas(UUID + "1", false);
                    player.sendMessage(ChatColor.GREEN + " * Visitas desabilitadas com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryTerrenos.configEspaçonave(UUID + "1"));
                        }
                    }, 5L);
                } else {
                    EspaçonaveAPI.updateVisitas(UUID + "1", true);
                    player.sendMessage(ChatColor.GREEN + " * Visita habilitada com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    //23
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryTerrenos.configEspaçonave(UUID + "1"));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 15) {
                //24
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventoryTerrenos.invAmigos(UUID));
                    }
                }, 5L);
            }
        }
        //------------------

        //MOEDA A SER UTILIZADA NA COMPRA
        if (event.getInventory().getName().equalsIgnoreCase("Escolha opção de pagamento: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 12) {
                if (InfoMoedaUtil.get(UUID) == null) {
                    new InfoMoedaUtil(UUID, "comum").insert();
                } else {
                    InfoMoedaUtil.get(UUID).setMoeda("comum");
                }
                //25
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (GuildaAPI.getGuilda(UUID).equals("nobre") || GuildaAPI.getGuilda(UUID).equals("sanguinaria")) {
                            player.openInventory(InventoryTerrenos.getInvConfirmCompra("terreno"));
                        }
                        if (GuildaAPI.getGuilda(UUID).equals("ancia")) {
                            player.openInventory(InventoryTerrenos.getInvConfirmCompra("ilha"));
                        }
                    }
                }, 5L);
            }

            if (event.getSlot() == 14) {
                if (InfoMoedaUtil.get(UUID) == null) {
                    new InfoMoedaUtil(UUID, "negra").insert();
                } else {
                    InfoMoedaUtil.get(UUID).setMoeda("negra");
                }
                //26
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (GuildaAPI.getGuilda(UUID).equals("nobre")) {
                            player.openInventory(InventoryTerrenos.getInvConfirmCompra("terreno"));
                        }
                        if (GuildaAPI.getGuilda(UUID).equals("ancia")) {
                            player.openInventory(InventoryTerrenos.getInvConfirmCompra("ilha"));
                        }
                        if (GuildaAPI.getGuilda(UUID).equals("sanguinaria")) {
                            player.openInventory(InventoryTerrenos.getInvConfirmCompra("espaçonave"));
                        }
                    }
                }, 5L);
            }
        }
        //------------------

        //CONFIRMAR COMPRA
        if (event.getInventory().getName().equalsIgnoreCase("Confirmar compra: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 12) {
                //26
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (GuildaAPI.getGuilda(UUID).equals("nobre")) {
                            player.openInventory(InventoryTerrenos.escolhaCompraMoeda("terreno"));
                        }
                        if (GuildaAPI.getGuilda(UUID).equals("ancia")) {
                            player.openInventory(InventoryTerrenos.escolhaCompraMoeda("ilha"));
                        }
                        if (GuildaAPI.getGuilda(UUID).equals("sanguinaria")) {
                            player.openInventory(InventoryTerrenos.escolhaCompraMoeda("espacionave"));
                        }
                    }
                }, 5L);
            }

            if (event.getSlot() == 14) {
                player.sendMessage(ChatColor.RED + " * Evento cancelado.");
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                player.getOpenInventory().close();
            }
        }
        //------------------

        //CONFIRMAR VENDA TERRENO
        if (event.getInventory().getName().equalsIgnoreCase("Confirmar venda espaçonave: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 12) {
                if (GuildaAPI.getGuilda(UUID).equals("sanguinaria")) {
                    if (EspaçonaveAPI.getIdEspaçonave(UUID + "1") != 0) {
                        EspaçonavesRegion.getRegions().clear();

                        if (EspaçonaveAPI.getMoedaUtilizada(UUID + "1").equals("comum")) {
                            CoinsAPI.updateCoins(UUID, CoinsAPI.getCoins(UUID) + 21500);
//                            val sb = Main.scoreboardManager;
//                            if (sb.hasSquadScoreboard(player)) {
//
//                            } else {
//                                sb.getDefaultScoreboard().updateEntry(player, "coins");
//                            }
                            player.sendMessage(AlternateColor.alternate("\n \n&e * Espaçonave vendida com sucesso.\n&8 - Foram adicionados em sua conta 21.500,00 moedas\n \n"));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else if (TerrainAPI.getMoedaUtilizada(UUID + "1").equals("negra")) {
                            CoinsAPI.updateBlackCoins(UUID, CoinsAPI.getBlackCoins(UUID) + 375);
//                            val sb = Main.scoreboardManager;
//                            if (sb.hasSquadScoreboard(player)) {
//
//                            } else {
//                                sb.getDefaultScoreboard().updateEntry(player, "blackcoins");
//                            }
                            player.sendMessage(AlternateColor.alternate("\n \n&e * Espaçonave vendida com sucesso.\n&8 - Foram adicionados em sua conta 375,00 moedas negras\n \n"));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }

                        EspaçonaveAPI.removeEspaçonave(UUID + "1");
                        EspaçonaveAPI.removeEspaçonaveInfos(UUID + "1");
                        CasaAPI.removeCasa(UUID);
                        for (int i = 1; i <= 30; i++) {
                            if (FarmAPI.getTexto(UUID + i) != null) {
                                FarmAPI.removeFarm(UUID + i);
                            } else {
                                break;
                            }
                        }
                        for (int i = 1; i <= 3; i++) {
                            MobspawnAPI.removeMobspawn(UUID + i);
                        }
                        EspaçonavesRegion.enableRegions();
                        player.getOpenInventory().close();

                        final String[] loc = RegionsSpawnAPI.getLocation("spawn").split(":");
                        final Location location = new Location(Bukkit.getWorld(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), Float.parseFloat(loc[4]), Float.parseFloat(loc[5]));
                        player.teleport(location);
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você não contém espaçonave para efetuar está venda.");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        player.getOpenInventory().close();
                    }
                }
            }

            if (event.getSlot() == 14) {
                player.sendMessage(ChatColor.RED + " * Evento cancelado.");
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                player.getOpenInventory().close();
            }
        }
        //------------------

        //CONFIRMAR VENDA TERRENO
        if (event.getInventory().getName().equalsIgnoreCase("Confirmar venda terreno: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 12) {
                if (GuildaAPI.getGuilda(UUID).equals("nobre")) {
                    if (TerrainAPI.getTerrainPos1(UUID + "1") != null) {
                        RegionsTr.getRegions().clear();

                        if (TerrainAPI.getMoedaUtilizada(UUID + "1").equals("comum")) {
                            CoinsAPI.updateCoins(UUID, CoinsAPI.getCoins(UUID) + 13500);
//                            val sb = Main.scoreboardManager;
//                            if (sb.hasSquadScoreboard(player)) {
//
//                            } else {
//                                sb.getDefaultScoreboard().updateEntry(player, "coins");
//                            }
                            player.sendMessage(AlternateColor.alternate("\n \n&e * Terreno vendido com sucesso.\n&8 - Foram adicionados em sua conta 13.500,00 moedas\n \n"));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else if (TerrainAPI.getMoedaUtilizada(UUID + "1").equals("negra")) {
                            CoinsAPI.updateBlackCoins(UUID, CoinsAPI.getBlackCoins(UUID) + 215);
//                            val sb = Main.scoreboardManager;
//                            if (sb.hasSquadScoreboard(player)) {
//
//                            } else {
//                                sb.getDefaultScoreboard().updateEntry(player, "blackcoins");
//                            }
                            player.sendMessage(AlternateColor.alternate("\n \n&e * Terreno vendido com sucesso.\n&8 - Foram adicionados em sua conta 215,00 moedas negras\n \n"));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }

                        TerrainAPI.removeTerrain(UUID + "1");
                        TerrainInfosAPI.removeTerrainInfo(UUID + "1");
                        CasaAPI.removeCasa(UUID);
                        RegionsTr.enableRegions();
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você não contém terreno para efetuar está venda.");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        player.getOpenInventory().close();
                    }
                }
            }

            if (event.getSlot() == 14) {
                player.sendMessage(ChatColor.RED + " * Evento cancelado.");
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                player.getOpenInventory().close();
            }
        }
        //------------------

        //CONFIRMAR VENDA ILHA
        if (event.getInventory().getName().equalsIgnoreCase("Confirmar venda ilha: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 12) {
                if (GuildaAPI.getGuilda(UUID).equals("ancia")) {
                    if (IlhaAPI.getIdIlha(UUID + "1") != 0) {
                        IlhasRegion.getRegions().clear();

                        if (IlhaAPI.getMoedaUtilizada(UUID + "1").equals("comum")) {
                            CoinsAPI.updateCoins(UUID, CoinsAPI.getCoins(UUID) + 16500);
//                            val sb = Main.scoreboardManager;
//                            if (sb.hasSquadScoreboard(player)) {
//
//                            } else {
//                                sb.getDefaultScoreboard().updateEntry(player, "coins");
//                            }
                            player.sendMessage(AlternateColor.alternate("\n \n&e * Ilha vendida com sucesso.\n&8 - Foram adicionados em sua conta 16.500,00 moedas\n \n"));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else if (IlhaAPI.getMoedaUtilizada(UUID + "1").equals("negra")) {
                            CoinsAPI.updateBlackCoins(UUID, CoinsAPI.getBlackCoins(UUID) + 315);
//                            val sb = Main.scoreboardManager;
//                            if (sb.hasSquadScoreboard(player)) {
//
//                            } else {
//                                sb.getDefaultScoreboard().updateEntry(player, "blackcoins");
//                            }
                            player.sendMessage(AlternateColor.alternate("\n \n&e * Ilha vendida com sucesso.\n&8 - Foram adicionados em sua conta 315,00 moedas negras\n \n"));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }

                        final String[] split = RegionsSpawnAPI.getLocation("spawn").split(":");
                        final Location location = new Location(Bukkit.getWorld(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]), Double.parseDouble(split[3]), Float.parseFloat(split[4]), Float.parseFloat(split[5]));
                        player.teleport(location);

                        IlhaAPI.removeIlha(UUID + "1");
                        IlhaAPI.removeIlhaInfos(UUID + "1");
                        CasaAPI.removeCasa(UUID);
                        IlhasRegion.enableRegions();
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você não contém uma ilha para efetuar está venda.");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        player.getOpenInventory().close();
                    }
                }
            }

            if (event.getSlot() == 14) {
                player.sendMessage(ChatColor.RED + " * Evento cancelado.");
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                player.getOpenInventory().close();
            }
        }
        //------------------

        //CLIQUES INVENTARIO UPGRADE TERRENO
        if (event.getInventory().getName().equalsIgnoreCase("Upgrades terreno: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 10) {
                if (TerrainInfosAPI.getNivelDrops(UUID) < 3 && TerrainInfosAPI.getNivelDrops(UUID) >= 1) {
                    //27
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryTerrenos.getInvConfirmUpgradeDrops());
                        }
                    }, 5L);
                } else if (TerrainInfosAPI.getNivelDrops(UUID) == 3) {
                    player.sendMessage(ChatColor.RED + "Ops, você já atingiu o nível maxímo desta função.");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }

            if (event.getSlot() == 12) {
                if (TerrainInfosAPI.getNivelMobspawn(UUID) < 3 && TerrainInfosAPI.getNivelMobspawn(UUID) >= 1) {
                    //28
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryTerrenos.getInvConfirmUpgradeMobspawn());
                        }
                    }, 5L);
                } else if (TerrainInfosAPI.getNivelMobspawn(UUID) == 3) {
                    player.sendMessage(ChatColor.RED + "Ops, você já atingiu o nível maxímo desta função.");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }

            if (event.getSlot() == 14) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + " * EM BREVE!");
                player.sendActionBar(ChatColor.RED + "EM BREVE!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }

            if (event.getSlot() == 16) {
                if (TerrainInfosAPI.getPvP(UUID + "1") == false) {
                    //29
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryTerrenos.getInvConfirmUpgradePvp());
                        }
                    }, 5L);
                } else {
                    if (TerrainInfosAPI.getPvP(UUID + "1") == false) {
                        TerrainInfosAPI.updatePvP(UUID + "1", true);
                        player.sendMessage(ChatColor.GREEN + " * PvP ativado em seu terreno.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        TerrainInfosAPI.updatePvP(UUID + "1", false);
                        player.sendMessage(ChatColor.GREEN + " * PvP desativado de seu terreno.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                }
            }
        }
        //------------------

        //CLIQUES INVENTARIO UPGRADE ILHAS
        if (event.getInventory().getName().equalsIgnoreCase("Upgrades ilha: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 10) {
                if (IlhaAPI.getNivelDrops(UUID) < 3 && IlhaAPI.getNivelDrops(UUID) >= 1) {
                    //30
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryTerrenos.getInvConfirmUpgradeDrops());
                        }
                    }, 5L);
                } else if (IlhaAPI.getNivelDrops(UUID) == 3) {
                    player.sendMessage(ChatColor.RED + "Ops, você já atingiu o nível maxímo desta função.");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }

            if (event.getSlot() == 12) {
                if (IlhaAPI.getNivelMobspawn(UUID) < 3 && IlhaAPI.getNivelMobspawn(UUID) >= 1) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryTerrenos.getInvConfirmUpgradeMobspawn());
                        }
                    }, 5L);
                } else if (IlhaAPI.getNivelMobspawn(UUID) == 3) {
                    player.sendMessage(ChatColor.RED + "Ops, você já atingiu o nível maxímo desta função.");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }

            if (event.getSlot() == 14) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + " * EM BREVE!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }

            if (event.getSlot() == 16) {
                if (IlhaAPI.getPvP(UUID + "1") == false) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryTerrenos.getInvConfirmUpgradePvp());
                        }
                    }, 5L);
                } else {
                    if (IlhaAPI.getPvP(UUID + "1") == true) {
                        IlhaAPI.updatePvP(UUID + "1", false);
                        player.sendMessage(ChatColor.GREEN + " * PvP ativado em sua ilha.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        IlhaAPI.updatePvP(UUID + "1", true);
                        player.sendMessage(ChatColor.GREEN + " * PvP desativado de sua ilha.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                }
            }
        }
        //------------------

        //CLIQUES INVENTARIO UPGRADE ESPAÇONAVE
        if (event.getInventory().getName().equalsIgnoreCase("Upgrades espaçonave: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 10) {
                if (EspaçonaveAPI.getNivelDrops(UUID) < 3 && EspaçonaveAPI.getNivelDrops(UUID) >= 1) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryTerrenos.getInvConfirmUpgradeDrops());
                        }
                    }, 5L);
                } else if (EspaçonaveAPI.getNivelDrops(UUID) == 3) {
                    player.sendMessage(ChatColor.RED + "Ops, você já atingiu o nível maxímo desta função.");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }

            if (event.getSlot() == 12) {
                if (EspaçonaveAPI.getNivelMobspawn(UUID) < 3 && EspaçonaveAPI.getNivelMobspawn(UUID) >= 1) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryTerrenos.getInvConfirmUpgradeMobspawn());
                        }
                    }, 5L);
                } else if (EspaçonaveAPI.getNivelMobspawn(UUID) == 3) {
                    player.sendMessage(ChatColor.RED + "Ops, você já atingiu o nível maxímo desta função.");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }

            if (event.getSlot() == 14) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + " * EM BREVE!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }

            if (event.getSlot() == 16) {
                if (EspaçonaveAPI.getPvP(UUID + "1") == false) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryTerrenos.getInvConfirmUpgradePvp());
                        }
                    }, 5L);
                } else {
                    if (EspaçonaveAPI.getPvP(UUID + "1") == true) {
                        EspaçonaveAPI.updatePvP(UUID + "1", false);
                        player.sendMessage(ChatColor.GREEN + " * PvP ativado em sua espaçonave.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        EspaçonaveAPI.updatePvP(UUID + "1", true);
                        player.sendMessage(ChatColor.GREEN + " * PvP desativado de sua espaçonave.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                }
            }
        }
        //------------------

        //CONFIRMAÇÃO UPGRADE PVP
        if (event.getInventory().getName().equalsIgnoreCase("Confirmar upgrade pvp: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 12) {
                if (TerrainInfosAPI.getPvP(UUID + "1") == false) {
                    if (CoinsAPI.getCoins(UUID) >= 5000) {
                        CoinsAPI.updateCoins(UUID, CoinsAPI.getCoins(UUID) - 5000);
                        TerrainInfosAPI.updatePvP(UUID + "1", true);
                        player.getOpenInventory().close();
                        player.sendMessage(ChatColor.GREEN + " * Modo pvp off adquirido com sucesso.");
                        player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você não contém dinheiro para efetuar este upgrade =(");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
            }

            if (event.getSlot() == 14) {
                player.sendMessage(ChatColor.RED + " * Evento cancelado.");
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                player.getOpenInventory().close();
            }
        }
        //------------------

        //CONFIRMAÇÃO UPGRADE MOBSPAWN
        if (event.getInventory().getName().equalsIgnoreCase("Confirmar upgrade mobspawn: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 12) {
                if (TerrainInfosAPI.getNivelMobspawn(UUID) == 1) {
                    if (CoinsAPI.getCoins(UUID) >= 15000) {
                        CoinsAPI.updateCoins(UUID, CoinsAPI.getCoins(UUID) - 15000);
                        TerrainInfosAPI.updateNivelMobspawn(UUID + "1");
                        player.getOpenInventory().close();
                        player.sendMessage(ChatColor.GREEN + " * Nível de mobspawn upado para nível " + AlternateColor.alternate("&n2") + ChatColor.GREEN + ".");
                        player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você não contém dinheiro para efetuar este upgrade =(");
                        player.sendActionBar(ChatColor.RED + "Você não contém dinheiro para efetuar este upgrade =(");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else if (TerrainInfosAPI.getNivelMobspawn(UUID) == 2) {
                    if (CoinsAPI.getCoins(UUID) >= 25000) {
                        CoinsAPI.updateCoins(UUID, CoinsAPI.getCoins(UUID) - 25000);
                        TerrainInfosAPI.updateNivelMobspawn(UUID + "1");
                        player.getOpenInventory().close();
                        player.sendMessage(ChatColor.GREEN + " * Nível de mobspawn upado para nível " + AlternateColor.alternate("&n3") + ChatColor.GREEN + ".");
                        player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você não contém dinheiro para efetuar este upgrade =(");
                        player.sendActionBar(ChatColor.RED + "Você não contém dinheiro para efetuar este upgrade =(");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
            }

            if (event.getSlot() == 14) {
                player.sendMessage(ChatColor.RED + " * Evento cancelado.");
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                player.getOpenInventory().close();
            }
        }
        //------------------

        //CONFIRMAÇÃO UPGRADE DROPS
        if (event.getInventory().getName().equalsIgnoreCase("Confirmar upgrade drops: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 12) {
                if (TerrainInfosAPI.getNivelDrops(UUID) == 1) {
                    if (CoinsAPI.getCoins(UUID) >= 15000) {
                        CoinsAPI.updateCoins(UUID, CoinsAPI.getCoins(UUID) - 15000);
                        TerrainInfosAPI.updateNivelDrops(UUID + "1");
                        player.getOpenInventory().close();
                        player.sendMessage(ChatColor.GREEN + " * Nível de drops upado para nível " + AlternateColor.alternate("&n2") + ChatColor.GREEN + ".");
                        player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você não contém dinheiro para efetuar este upgrade =(");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else if (TerrainInfosAPI.getNivelDrops(UUID) == 2) {
                    if (CoinsAPI.getCoins(UUID) >= 25000) {
                        CoinsAPI.updateCoins(UUID, CoinsAPI.getCoins(UUID) - 25000);
                        TerrainInfosAPI.updateNivelDrops(UUID + "1");
                        player.getOpenInventory().close();
                        player.sendMessage(ChatColor.GREEN + " * Nível de drops upado para nível " + AlternateColor.alternate("&n3") + ChatColor.GREEN + ".");
                        player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você não contém dinheiro para efetuar este upgrade =(");
                        player.sendActionBar(ChatColor.RED + "Você não contém dinheiro para efetuar este upgrade =(");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
            }

            if (event.getSlot() == 14) {
                player.sendMessage(ChatColor.RED + " * Evento cancelado.");
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                player.getOpenInventory().close();
            }
        }
        //------------------

        //ABRINDO INVENTARIO REFERENTE A TERRENO
        if (event.getInventory().getName().equalsIgnoreCase("Terreno: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 11) {

                player.sendMessage(ChatColor.RED + " * Temporariamente desabilitado!");
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                return;

                //if (TerrainAPI.getTerrainPos1(UUID + "1") != null) {
                //                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                //                        @Override
                //                        public void run() {
                //                            player.openInventory(InventoryTerrenos.getInvUpgrades(UUID + "1", "terreno"));
                //                        }
                //                    }, 5L);
                //                } else {
                //                    player.sendMessage(ChatColor.RED + "Ops, você não contém terreno para efetuar está venda.");
                //                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                //                }
            }

            if (event.getSlot() == 13) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (TerrainAPI.getTerrainPos1(UUID + "1") == null) {
                            player.openInventory(InventoryTerrenos.escolhaCompraMoeda("terreno"));
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, você já contém um terreno!");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    }
                }, 5L);
            }

            if (event.getSlot() == 15) {
                if (TerrainAPI.getTerrainPos1(UUID + "1") != null) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryTerrenos.getInvConfirmVenda("terreno"));
                        }
                    }, 5L);
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, você não contém terreno para efetuar está venda.");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }
        }
        //------------------


        if (event.getInventory().getName().equalsIgnoreCase("Tipo de ilha: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 12) {
                if (TypeIsland.get(UUID) == null) {
                    new TypeIsland(UUID, "padrao").insert();
                } else {
                    TypeIsland.get(UUID).setType("padrao");
                }
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventoryTerrenos.escolhaCompraMoeda("ilha"));
                    }
                }, 5L);
            }

            if (event.getSlot() == 14) {
                if (TypeIsland.get(UUID) == null) {
                    new TypeIsland(UUID, "custom").insert();
                } else {
                    TypeIsland.get(UUID).setType("custom");
                }
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventoryTerrenos.escolhaCompraMoeda("ilha"));
                    }
                }, 5L);
            }
        }


        //ABRINDO INVENTARIO REFERENTE A ILHA
        if (event.getInventory().getName().equalsIgnoreCase("Ilha: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 11) {
                player.sendMessage(ChatColor.RED + " * Temporariamente desabilitado!");
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                return;

                //if (IlhaAPI.getIdIlha(UUID + "1") != 0) {
                //                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                //                        @Override
                //                        public void run() {
                //                            player.openInventory(InventoryTerrenos.getInvUpgrades(UUID, "ilha"));
                //                        }
                //                    }, 5L);
                //                } else {
                //                    player.sendMessage(ChatColor.RED + "Ops, você não contém ilha para efetuar está venda.");
                //                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                //                }
            }

            if (event.getSlot() == 13) {
                if (IlhaAPI.getIdIlha(UUID + "1") == 0) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryTerrenos.escolhaCompraMoeda("ilha"));
                        }
                    }, 5L);
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, você já contém uma ilha!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }

            if (event.getSlot() == 15) {
                if (IlhaAPI.getIdIlha(UUID + "1") != 0) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryTerrenos.getInvConfirmVenda("ilha"));
                        }
                    }, 5L);
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, você não contém ilha para efetuar está venda.");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }
        }
        //------------------

        //ABRINDO INVENTARIO REFERENTE A ESPACIONAVE
        if (event.getInventory().getName().equalsIgnoreCase("Espaçonave: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 11) {

                player.sendMessage(ChatColor.RED + " * Temporariamente desabilitado!");
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                return;

                // if (EspaçonaveAPI.getIdEspaçonave(UUID + "1") != 0) {
                //                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                //                        @Override
                //                        public void run() {
                //                            player.openInventory(InventoryTerrenos.getInvUpgrades(UUID, "espaçonave"));
                //                        }
                //                    }, 5L);
                //                } else {
                //                    player.sendMessage(ChatColor.RED + "Ops, você não contém espaçonave para efetuar está venda.");
                //                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                //                }
            }

            if (event.getSlot() == 13) {
                if (EspaçonaveAPI.getIdEspaçonave(UUID + "1") == 0) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryTerrenos.escolhaCompraMoeda("espaçonave"));
                        }
                    }, 5L);
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, você já contém uma espaçonave.");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }

            if (event.getSlot() == 15) {
                if (EspaçonaveAPI.getIdEspaçonave(UUID + "1") != 0) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryTerrenos.getInvConfirmVenda("espaçonave"));
                        }
                    }, 5L);
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, você não contém espaçonave para efetuar está venda.");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }
        }
        //------------------
    }
}
