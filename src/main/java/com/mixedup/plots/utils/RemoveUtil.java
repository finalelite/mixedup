package com.mixedup.plots.utils;

import java.util.HashMap;

public class RemoveUtil {

    private static final HashMap<String, RemoveUtil> CACHE = new HashMap<String, RemoveUtil>();
    private final String UUID;
    private boolean status;

    public RemoveUtil(final String UUID, final boolean status) {
        this.UUID = UUID;
        this.status = status;
    }

    public static RemoveUtil get(final String UUID) {
        return RemoveUtil.CACHE.get(String.valueOf(UUID));
    }

    public RemoveUtil insert() {
        RemoveUtil.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(final boolean status) {
        this.status = status;
    }

    public String getUUID() {
        return UUID;
    }
}
