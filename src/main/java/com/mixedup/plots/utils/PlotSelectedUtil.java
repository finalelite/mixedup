package com.mixedup.plots.utils;

import java.util.HashMap;

public class PlotSelectedUtil {

    private static final HashMap<String, PlotSelectedUtil> CACHE = new HashMap<String, PlotSelectedUtil>();
    private final String UUID;
    private int ID;

    public PlotSelectedUtil(final String UUID, final int ID) {
        this.UUID = UUID;
        this.ID = ID;
    }

    public static PlotSelectedUtil get(final String UUID) {
        return PlotSelectedUtil.CACHE.get(String.valueOf(UUID));
    }

    public PlotSelectedUtil insert() {
        PlotSelectedUtil.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public int getID() {
        return ID;
    }

    public void setID(final int ID) {
        this.ID = ID;
    }

    public String getUUID() {
        return UUID;
    }
}
