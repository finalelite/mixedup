package com.mixedup.plots.utils;

import java.util.HashMap;

public class PlayerItIsUtil {

    private static final HashMap<String, PlayerItIsUtil> CACHE = new HashMap<String, PlayerItIsUtil>();
    private final String UUID;
    private boolean status;
    private String owner;

    public PlayerItIsUtil(final String UUID, final boolean status, final String owner) {
        this.UUID = UUID;
        this.status = status;
        this.owner = owner;
    }

    public static PlayerItIsUtil get(final String UUID) {
        return PlayerItIsUtil.CACHE.get(String.valueOf(UUID));
    }

    public PlayerItIsUtil insert() {
        PlayerItIsUtil.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(final String owner) {
        this.owner = owner;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(final boolean status) {
        this.status = status;
    }

    public String getUUID() {
        return UUID;
    }
}
