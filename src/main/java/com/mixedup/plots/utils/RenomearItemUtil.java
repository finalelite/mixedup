package com.mixedup.plots.utils;

import java.util.HashMap;

public class RenomearItemUtil {

    private static final HashMap<String, RenomearItemUtil> CACHE = new HashMap<String, RenomearItemUtil>();
    private final String UUID;
    private boolean status;

    public RenomearItemUtil(final String UUID, final boolean status) {
        this.UUID = UUID;
        this.status = status;
    }

    public static RenomearItemUtil get(final String UUID) {
        return RenomearItemUtil.CACHE.get(String.valueOf(UUID));
    }

    public RenomearItemUtil insert() {
        RenomearItemUtil.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(final boolean status) {
        this.status = status;
    }

    public String getUUID() {
        return UUID;
    }
}
