package com.mixedup.plots.utils;

import java.util.HashMap;

public class BackupList {

    private static final HashMap<String, BackupList> CACHE = new HashMap<String, BackupList>();
    private final String UUID;
    private String list;

    public BackupList(final String UUID, final String list) {
        this.UUID = UUID;
        this.list = list;
    }

    public static BackupList get(final String UUID) {
        return BackupList.CACHE.get(String.valueOf(UUID));
    }

    public BackupList insert() {
        BackupList.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public String getList() {
        return list;
    }

    public void setLists(final String list) {
        this.list = list;
    }

    public String getUUID() {
        return UUID;
    }
}
