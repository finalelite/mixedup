package com.mixedup.plots.utils;

import java.util.HashMap;

public class PlayerLoc {

    private static final HashMap<String, PlayerLoc> CACHE = new HashMap<String, PlayerLoc>();
    private final String UUID;
    private boolean status;

    public PlayerLoc(final String UUID, final boolean status) {
        this.UUID = UUID;
        this.status = status;
    }

    public static PlayerLoc get(final String UUID) {
        return PlayerLoc.CACHE.get(String.valueOf(UUID));
    }

    public PlayerLoc insert() {
        PlayerLoc.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(final boolean status) {
        this.status = status;
    }

    public String getUUID() {
        return UUID;
    }
}
