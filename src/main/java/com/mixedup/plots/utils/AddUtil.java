package com.mixedup.plots.utils;

import java.util.HashMap;

public class AddUtil {

    private static final HashMap<String, AddUtil> CACHE = new HashMap<String, AddUtil>();
    private final String UUID;
    private boolean status;

    public AddUtil(final String UUID, final boolean status) {
        this.UUID = UUID;
        this.status = status;
    }

    public static AddUtil get(final String UUID) {
        return AddUtil.CACHE.get(String.valueOf(UUID));
    }

    public AddUtil insert() {
        AddUtil.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(final boolean status) {
        this.status = status;
    }

    public String getUUID() {
        return UUID;
    }
}
