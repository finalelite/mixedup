package com.mixedup.plots.utils;


import java.util.HashMap;

public class InfoMoedaUtil {

    private static final HashMap<String, InfoMoedaUtil> CACHE = new HashMap<String, InfoMoedaUtil>();
    private final String UUID;
    private String moeda;

    public InfoMoedaUtil(final String UUID, final String moeda) {
        this.UUID = UUID;
        this.moeda = moeda;
    }

    public static InfoMoedaUtil get(final String UUID) {
        return InfoMoedaUtil.CACHE.get(String.valueOf(UUID));
    }

    public InfoMoedaUtil insert() {
        InfoMoedaUtil.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public String getMoeda() {
        return moeda;
    }

    public void setMoeda(final String moeda) {
        this.moeda = moeda;
    }

    public String getUUID() {
        return UUID;
    }
}
