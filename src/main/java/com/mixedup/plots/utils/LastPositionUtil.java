package com.mixedup.plots.utils;

import java.util.HashMap;

public class LastPositionUtil {

    private static final HashMap<String, LastPositionUtil> CACHE = new HashMap<String, LastPositionUtil>();
    private final String UUID;
    private String lastPosition;
    private String lastPosition2;

    public LastPositionUtil(final String UUID, final String lastPosition, final String lastPosition2) {
        this.UUID = UUID;
        this.lastPosition = lastPosition;
        this.lastPosition2 = lastPosition2;
    }

    public static LastPositionUtil get(final String UUID) {
        return LastPositionUtil.CACHE.get(String.valueOf(UUID));
    }

    public LastPositionUtil insert() {
        LastPositionUtil.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public String getLastPosition() {
        return lastPosition;
    }

    public void setLastPosition(final String lastPosition) {
        this.lastPosition = lastPosition;
    }

    public String getLastPosition2() {
        return lastPosition2;
    }

    public void setLastPosition2(final String lastPosition2) {
        this.lastPosition2 = lastPosition2;
    }

    public String getUUID() {
        return UUID;
    }
}
