package com.mixedup.plots.espaconave;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

public class MobspawnFixEvent implements Listener {

    @EventHandler
    public void onSpawn(final EntitySpawnEvent event) {
        if (event.getLocation().getWorld().getName().equalsIgnoreCase("espaconave")) {
            if (event.getEntityType().equals(EntityType.ARMOR_STAND)) return;
            if (EspaçonaveUtil.playerInAreaSchem(event.getLocation()) == null) {
                event.setCancelled(true);
            }
        }
    }
}
