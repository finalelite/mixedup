package com.mixedup.plots.espaconave;

import com.mixedup.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MobspawnAPI {

    public static void setInfo(final String UUID, final String location) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Mobspawn_data(UUID, Status, SpawnAtivo, Spawners, Location) VALUES (?, ?, ?, ?, ?)");
            st.setString(1, UUID);
            st.setBoolean(2, false);
            st.setString(3, "NULL");
            st.setString(4, "NULL");
            st.setString(5, location);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeMobspawn(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Mobspawn_data WHERE UUID = ?");
            st.setString(1, UUID);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeSpawner(final String UUID, final String spawner) {
        if (MobspawnAPI.getSpawners(UUID + "1").contains(":")) {
            final String[] split = MobspawnAPI.getSpawners(UUID + "1").split(":");
            String newSpawners = null;
            for (int i = 1; i <= split.length; i++) {
                if (!split[i - 1].contains(spawner)) {
                    if (newSpawners == null) {
                        newSpawners = split[i - 1];
                    } else {
                        newSpawners = newSpawners + ":" + split[i];
                    }
                }
            }
            MobspawnAPI.updateSpawners(UUID + "1", newSpawners);
        } else {
            if (MobspawnAPI.getSpawners(UUID + "1").contains(spawner)) {
                MobspawnAPI.updateSpawners(UUID + "1", "NULL");
            }
        }
    }

    public static void updateStatus(final String UUID, final boolean status) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Mobspawn_data SET Status = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setBoolean(1, status);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateSpawnAtivo(final String UUID, final String spawnAtivo) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Mobspawn_data SET SpawnAtivo = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setString(1, spawnAtivo);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateSpawners(final String UUID, final String spawners) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Mobspawn_data SET Spawners = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setString(1, spawners);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean getStatus(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Mobspawn_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Status");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String getSpawnAtivo(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Mobspawn_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("SpawnAtivo");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getSpawnersFromLoc(final String location) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Mobspawn_data WHERE Location = ?");
            st.setString(1, location);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Spawners");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean getSpawnAtivoFromLoc(final String location) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Mobspawn_data WHERE Location = ?");
            st.setString(1, location);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Status");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String getSpawners(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Mobspawn_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Spawners");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
