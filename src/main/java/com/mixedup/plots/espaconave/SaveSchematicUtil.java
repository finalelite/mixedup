package com.mixedup.plots.espaconave;

import com.mixedup.utils.WorldEditUtil;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.WorldEditException;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldedit.world.DataException;
import org.bukkit.Location;

import java.io.IOException;

public class SaveSchematicUtil {

    public static void saveTerrain(final String saveFile, final Location l1, final Location l2) throws WorldEditException, DataException, IOException {
        final Vector min = new Vector(l1.getBlockX(), l1.getBlockY(), l1.getBlockZ());
        final Vector max = new Vector(l2.getBlockX(), l2.getBlockY(), l2.getBlockZ());

        final CuboidRegion region = new CuboidRegion(new BukkitWorld(l1.getWorld()), min, max);
        WorldEditUtil.save(saveFile, WorldEditUtil.copy(region));
    }

    private static Vector getMin(final Location l1, final Location l2) {
        return new Vector(
                Math.min(l1.getBlockX(), l2.getBlockX()),
                Math.min(l1.getBlockY(), l2.getBlockY()),
                Math.min(l1.getBlockZ(), l2.getBlockZ())
        );
    }

    private static Vector getMax(final Location l1, final Location l2) {
        return new Vector(
                Math.max(l1.getBlockX(), l2.getBlockX()),
                Math.max(l1.getBlockY(), l2.getBlockY()),
                Math.max(l1.getBlockZ(), l2.getBlockZ())
        );
    }
}
