package com.mixedup.plots.espaconave;

import com.mixedup.MySql;
import org.bukkit.Material;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FarmAPI {

    public static void setFarm(final String UUID, final String texto, final Boolean status) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Farm_data(UUID, Texto, Bloco, Status) VALUES (?, ?, ?, ?)");
            st.setString(1, UUID);
            st.setString(2, texto);
            st.setString(3, Material.WHITE_STAINED_GLASS.name());
            st.setBoolean(4, status);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeFarm(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Farm_data WHERE UUID = ?");
            st.setString(1, UUID);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateStatus(final String UUID, final boolean status) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Farm_data SET Status = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setBoolean(1, status);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateTexto(final String UUID, final String texto) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Farm_data SET Texto = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setString(1, texto);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateBloco(final String UUID, final String bloco) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Farm_data SET Bloco = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setString(1, bloco);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean getStatus(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Farm_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Status");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String getTexto(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Farm_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Texto");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getBloco(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Farm_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Bloco");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
