package com.mixedup.plots.espaconave;

import java.util.HashMap;

public class MobspawnerOpenedUtil {

    private static final HashMap<String, MobspawnerOpenedUtil> CACHE = new HashMap<String, MobspawnerOpenedUtil>();
    private final String UUID;
    private int ID;

    public MobspawnerOpenedUtil(final String UUID, final int ID) {
        this.UUID = UUID;
        this.ID = ID;
    }

    public static MobspawnerOpenedUtil get(final String UUID) {
        return MobspawnerOpenedUtil.CACHE.get(String.valueOf(UUID));
    }

    public MobspawnerOpenedUtil insert() {
        MobspawnerOpenedUtil.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public int getID() {
        return ID;
    }

    public void setID(final int ID) {
        this.ID = ID;
    }

    public String getUUID() {
        return UUID;
    }
}
