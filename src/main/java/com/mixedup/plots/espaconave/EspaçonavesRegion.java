package com.mixedup.plots.espaconave;

import com.mixedup.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EspaçonavesRegion {

    private static List<EspaconaveRegion> regions = new ArrayList<>();

    public static EspaconaveRegion getRegion(final String regionID) {
        for (final EspaconaveRegion region : EspaçonavesRegion.getRegions()) {
            if (region.getRegionID() == regionID) {
                return region;
            }
        }
        return null;
    }

    public static List<EspaconaveRegion> getRegions() {
        return EspaçonavesRegion.regions;
    }

    public static void setRegions(final List<EspaconaveRegion> regions) {
        EspaçonavesRegion.regions = regions;
    }

    public static void enableRegions() {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Espaçonave_data");
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                final String pos1 = (1000 * rs.getInt("ID") - 536) + ":" + "0" + ":" + "464";
                final String pos2 = (1000 * rs.getInt("ID") - 466) + ":" + "256" + ":" + "534";
                final EspaconaveRegion espaçonaveRegion = new EspaconaveRegion(rs.getString("UUID"), pos1, pos2);
                getRegions().add(espaçonaveRegion);
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }
}
