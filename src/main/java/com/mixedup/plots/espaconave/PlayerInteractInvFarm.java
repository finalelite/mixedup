package com.mixedup.plots.espaconave;

import com.mixedup.Main;
import com.mixedup.apis.PainelSolarAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.economy.CoinsAPI;
import com.mixedup.listeners.MobsSpawnEvents;
import com.mixedup.plots.utils.BackupList;
import com.mixedup.plots.utils.InfoMoedaUtil;
import com.mixedup.plots.utils.PlotSelectedUtil;
import com.mixedup.plots.utils.RenomearItemUtil;
import com.mixedup.utils.WorldEditUtil;
import com.sk89q.worldedit.WorldEditException;
import com.sk89q.worldedit.world.DataException;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

public class PlayerInteractInvFarm implements Listener {

    int ID;
    int IDSave;
    String itensNew;
    String previous;
    boolean isItem;

    public static boolean containsSpawner(final String spawner, final String mob) {
        boolean status = false;

        if (MobspawnAPI.getSpawners(spawner).equals("NULL")) {
            status = false;
        } else {
            if (!MobspawnAPI.getSpawners(spawner).contains(":")) {
                final String[] split = MobspawnAPI.getSpawners(spawner).split("-");
                status = split[0].equalsIgnoreCase(ChatColor.RED + "Spawner de " + mob);
            } else {
                final String[] split1 = MobspawnAPI.getSpawners(spawner).split(":");
                for (int i = 1; i <= split1.length; i++) {
                    final String[] split = split1[i - 1].split("-");
                    if (split[0].equalsIgnoreCase(ChatColor.RED + "Spawner de " + mob)) {
                        status = true;
                        break;
                    }
                    if (i == split1.length) {
                        status = false;
                    }
                }
            }
        }
        return status;
    }

    public static void setSpawner(final Block block, final EntityType entity) {
        final BlockState blockState = block.getState();
        final CreatureSpawner spawner = (CreatureSpawner) blockState;
        spawner.setSpawnedType(entity);
        blockState.update();
    }

    @EventHandler
    public void onSay(final AsyncPlayerChatEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (RenomearItemUtil.get(UUID) != null && RenomearItemUtil.get(UUID).getStatus() == true) {
            event.setCancelled(true);

            final String[] message = event.getMessage().split(" ");
            final StringBuilder build = new StringBuilder();
            for (int i = 0; i < message.length; i++) {
                build.append(" ");
                build.append(message[i]);
            }
            final String msg = build.toString();

            FarmAPI.updateTexto(UUID + PlotSelectedUtil.get(UUID).getID(), ChatColor.YELLOW + msg.substring(1));
            RenomearItemUtil.get(UUID).setStatus(false);
            player.sendMessage(ChatColor.GREEN + "\n \n * Plot renomeado com sucesso!\n \n");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
    }

    @EventHandler
    public void onInteract(final InventoryClickEvent event) throws DataException, WorldEditException, IOException {
        Player player = (Player) event.getWhoClicked();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getInventory().getName() == null) return;

        if (event.getInventory().getName().equalsIgnoreCase("Remover painéis: ")) {
            event.setCancelled(true);
            int n = 0;

            if (event.getSlot() == 45) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventoryConfigsEspaçonave.getInvPaineis(UUID));
                    }
                }, 5L);
            }

            if (event.getSlot() > 10 && event.getSlot() < 17) {
                if (event.getInventory().getItem(event.getSlot()) == null) return;
                n = event.getSlot() - 9;

                PainelSolarAPI.removePainelFromUUID(UUID + n);
                final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(event.getInventory().getItem(event.getSlot()));
                for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                    player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                }
                player.getOpenInventory().close();
                player.sendMessage(ChatColor.GREEN + " * Painel solar removido com sucesso.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
            if (event.getSlot() > 18 && event.getSlot() < 26) {
                if (event.getInventory().getItem(event.getSlot()) == null) return;
                n = event.getSlot() - 11;

                PainelSolarAPI.removePainelFromUUID(UUID + n);
                final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(event.getInventory().getItem(event.getSlot()));
                for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                    player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                }
                player.getOpenInventory().close();
                player.sendMessage(ChatColor.GREEN + " * Painel solar removido com sucesso.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
            if (event.getSlot() > 27 && event.getSlot() < 35) {
                if (event.getInventory().getItem(event.getSlot()) == null) return;
                n = event.getSlot() - 13;

                PainelSolarAPI.removePainelFromUUID(UUID + n);
                final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(event.getInventory().getItem(event.getSlot()));
                for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                    player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                }
                player.getOpenInventory().close();
                player.sendMessage(ChatColor.GREEN + " * Painel solar removido com sucesso.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
            if (event.getSlot() > 36 && event.getSlot() < 41) {
                if (event.getInventory().getItem(event.getSlot()) == null) return;
                n = event.getSlot() - 15;

                PainelSolarAPI.removePainelFromUUID(UUID + n);
                final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(event.getInventory().getItem(event.getSlot()));
                for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                    player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                }
                player.getOpenInventory().close();
                player.sendMessage(ChatColor.GREEN + " * Painel solar removido com sucesso.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }

        if (event.getInventory().getName().equalsIgnoreCase("Configuração painéis solares: ")) {
            if (event.getClickedInventory().equals(player.getOpenInventory().getTopInventory())) {
                event.setCancelled(true);
            }
            if (event.getSlot() == 10) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventoryConfigsEspaçonave.getPaineisReturn(UUID));
                    }
                }, 5L);
            }
            if (event.getSlot() == 16) {
                if (event.getCursor().getType().equals(Material.DAYLIGHT_DETECTOR) && event.getCursor().getItemMeta().getDisplayName() != null) {
                    final ItemStack painel1 = new ItemStack(Material.DAYLIGHT_DETECTOR);
                    final ItemMeta meta1 = painel1.getItemMeta();
                    meta1.setDisplayName(ChatColor.YELLOW + "Painel solar");
                    final List<String> lore1 = new ArrayList<>();
                    lore1.add(" ");
                    lore1.add(ChatColor.DARK_GRAY + "Modelo: " + ChatColor.RED + "100Wp");
                    lore1.add(" ");
                    meta1.setLore(lore1);
                    painel1.setItemMeta(meta1);

                    final ItemStack painel2 = new ItemStack(Material.DAYLIGHT_DETECTOR);
                    final ItemMeta meta2 = painel2.getItemMeta();
                    meta2.setDisplayName(ChatColor.YELLOW + "Painel solar");
                    final List<String> lore2 = new ArrayList<>();
                    lore2.add(" ");
                    lore2.add(ChatColor.DARK_GRAY + "Modelo: " + ChatColor.RED + "200Wp");
                    lore2.add(" ");
                    meta2.setLore(lore2);
                    painel2.setItemMeta(meta2);

                    final ItemStack item = event.getCursor();
                    if (item.getItemMeta().getLore().get(1).substring(item.getItemMeta().getLore().get(1).length() - 5).equalsIgnoreCase("100Wp")) {
                        if (PainelSolarAPI.getLastPainel(UUID) < 30) {
                            if (item.getAmount() > 1) {
                                for (int i = 1; i <= item.getAmount(); i++) {
                                    if (PainelSolarAPI.getLastPainel(UUID) + 1 < 30) {
                                        PainelSolarAPI.setPainel(UUID + PainelSolarAPI.getLastPainel(UUID), "espaconave", "100", PainelSolarAPI.getLastPainel(UUID) + 1);
                                    } else {
                                        player.sendMessage(ChatColor.GREEN + " * " + i + " painéis solares de 100Wp, acabaram de ser adicionados e ativados!\n" + ChatColor.RED + "   !" + (event.getCursor().getAmount() - i) + " painéis não foram adicionados, pelo limite já excedido.");
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                                        event.getCursor().setAmount(event.getCursor().getAmount() - i);

                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                player.openInventory(InventoryConfigsEspaçonave.getInvPaineis(UUID));
                                            }
                                        }, 5L);
                                        break;
                                    }
                                    if (i == item.getAmount()) {
                                        player.sendMessage(ChatColor.GREEN + " * " + i + " painéis solares de 100Wp, acabaram de ser adicionados e ativados!");
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                                        event.getCursor().setType(Material.AIR);
                                        event.getCursor().setAmount(0);

                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                player.openInventory(InventoryConfigsEspaçonave.getInvPaineis(UUID));
                                            }
                                        }, 5L);
                                    }
                                }
                            } else {
                                player.sendMessage(ChatColor.GREEN + " * Painel solar de 100Wp, acaba de ser adicionado e ativado!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                PainelSolarAPI.setPainel(UUID + (PainelSolarAPI.getLastPainel(UUID) + 1), "Espaçonave", "100", PainelSolarAPI.getLastPainel(UUID) + 1);

                                event.getCursor().setType(Material.AIR);
                                event.getCursor().setAmount(0);

                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                    @Override
                                    public void run() {
                                        player.openInventory(InventoryConfigsEspaçonave.getInvPaineis(UUID));
                                    }
                                }, 5L);
                            }
                        } else {
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.RED + " * Ops, você atingiu o limite de paineis permitido por player!");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else if (item.getItemMeta().getLore().get(1).substring(item.getItemMeta().getLore().get(1).length() - 5).equalsIgnoreCase("200Wp")) {
                        if (PainelSolarAPI.getLastPainel(UUID) < 30) {
                            if (item.getAmount() > 1) {
                                for (int i = 1; i <= item.getAmount(); i++) {
                                    if (PainelSolarAPI.getLastPainel(UUID) + 1 < 30) {
                                        PainelSolarAPI.setPainel(UUID + (PainelSolarAPI.getLastPainel(UUID) + 1), "Espaçonave", "200", PainelSolarAPI.getLastPainel(UUID) + 1);
                                    } else {
                                        player.sendMessage(ChatColor.GREEN + " * " + i + " painéis solares de 200Wp, acabaram de ser adicionados e ativados!\n" + ChatColor.RED + "   !" + (event.getCursor().getAmount() - i) + " painéis não foram adicionados, pelo limite já excedido.");
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                                        event.getCursor().setAmount(event.getCursor().getAmount() - i);

                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                player.openInventory(InventoryConfigsEspaçonave.getInvPaineis(UUID));
                                            }
                                        }, 5L);
                                        break;
                                    }
                                    if (i == item.getAmount()) {
                                        player.sendMessage(ChatColor.GREEN + " * " + i + " painéis solares de 200Wp, acabaram de ser adicionados e ativados!");
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                                        event.getCursor().setType(Material.AIR);
                                        event.getCursor().setAmount(0);

                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                player.openInventory(InventoryConfigsEspaçonave.getInvPaineis(UUID));
                                            }
                                        }, 5L);
                                    }
                                }
                            } else {
                                player.sendMessage(ChatColor.GREEN + " * Painel solar de 200Wp, acaba de ser adicionado e ativado!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                PainelSolarAPI.setPainel(UUID + (PainelSolarAPI.getLastPainel(UUID) + 1), "Espaçonave", "200", PainelSolarAPI.getLastPainel(UUID) + 1);

                                event.getCursor().setType(Material.AIR);
                                event.getCursor().setAmount(0);

                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                    @Override
                                    public void run() {
                                        player.openInventory(InventoryConfigsEspaçonave.getInvPaineis(UUID));
                                    }
                                }, 5L);
                            }
                        } else {
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.RED + " * Ops, você atingiu o limite de paineis permitido por player!");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    }
                }
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventoryConfigsEspaçonave.getInvPaineis(UUID));
                    }
                }, 5L);
            }
        }

        if (event.getInventory().getName().equalsIgnoreCase("Remover spawners: ")) {

            if (event.getSlot() == 49) {
                player.getOpenInventory().close();
            }

            if (event.getInventory().getItem(event.getSlot()) != null) {
                if (!event.getInventory().getItem(event.getSlot()).getType().equals(Material.ARROW) && !event.getInventory().getItem(event.getSlot()).getType().equals(Material.BARRIER)) {
                    final String spawner = event.getInventory().getItem(event.getSlot()).getItemMeta().getDisplayName() + "-" + event.getInventory().getItem(event.getSlot()).getAmount();
                    MobspawnAPI.removeSpawner(UUID, spawner);

                    final HashMap<Integer, ItemStack> nope = player.getOpenInventory().getBottomInventory().addItem(event.getInventory().getItem(event.getSlot()));
                    for (final Entry<Integer, ItemStack> entry : nope.entrySet()) {
                        player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                    }
                    player.getOpenInventory().close();
                    player.sendMessage(ChatColor.GREEN + " * Spawner removido com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }

            if (event.getSlot() == 45 && event.getCurrentItem().getType().equals(Material.ARROW)) {
                event.setCancelled(true);

                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        final String[] itens = MobspawnAPI.getSpawners(UUID + "1").split(BackupList.get(UUID).getList());
                        final String[] listPrevious = itens[0].split(":");
                        if (listPrevious.length > 28) {
                            for (int i = 1; i <= 28; i++) {
                                if (PlayerInteractInvFarm.this.previous == null) {
                                    PlayerInteractInvFarm.this.previous = listPrevious[listPrevious.length - 1];
                                } else {
                                    PlayerInteractInvFarm.this.previous = PlayerInteractInvFarm.this.previous + ":" + listPrevious[listPrevious.length - 1];
                                }
                                if (i == 28) {
                                    player.openInventory(InventoryConfigsEspaçonave.getMobspawners(UUID, true, PlayerInteractInvFarm.this.previous));
                                }
                            }
                        } else {
                            player.openInventory(InventoryConfigsEspaçonave.getMobspawners(UUID, true, itens[0]));
                        }
                    }
                }, 5L);
            }

            if (event.getSlot() == 53 && event.getCurrentItem().getType().equals(Material.ARROW)) {
                event.setCancelled(true);

                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventoryConfigsEspaçonave.getMobspawners(UUID, true, BackupList.get(UUID).getList()));
                    }
                }, 5L);
            }
        }

        if (event.getInventory().getName().equalsIgnoreCase("Configuração mobspawner: ")) {

            if (event.getSlot() == 38) {
                if (MobspawnAPI.getStatus(UUID + MobspawnerOpenedUtil.get(UUID).getID()) == false) {
                    if (MobspawnAPI.getSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID()).equals("NULL")) {
                        player.sendMessage(ChatColor.RED + "Ops, selecione um mob a spawnar neste mobspawner!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        return;
                    }
                    MobspawnAPI.updateStatus(UUID + MobspawnerOpenedUtil.get(UUID).getID(), true);
                    player.sendMessage(ChatColor.GREEN + " * Mobspawn ativado!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.getOpenInventory().close();

                    if (MobspawnerOpenedUtil.get(UUID).getID() == 1) {
                        final Location loc1 = new Location(Bukkit.getWorld("espaconave"), 1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 473, 73, 507);
                        loc1.getBlock().setType(Material.AIR);
                        loc1.getBlock().setType(Material.SPAWNER);
                        final Block block = loc1.getBlock();
                        PlayerInteractInvFarm.setSpawner(block, MobsSpawnEvents.getEntityType(MobspawnAPI.getSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID())));
                    }
                    if (MobspawnerOpenedUtil.get(UUID).getID() == 2) {
                        final Location loc1 = new Location(Bukkit.getWorld("espaconave"), 1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 471, 73, 499);
                        loc1.getBlock().setType(Material.AIR);
                        loc1.getBlock().setType(Material.SPAWNER);
                        final Block block = loc1.getBlock();
                        PlayerInteractInvFarm.setSpawner(block, MobsSpawnEvents.getEntityType(MobspawnAPI.getSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID())));
                    }
                    if (MobspawnerOpenedUtil.get(UUID).getID() == 3) {
                        final Location loc1 = new Location(Bukkit.getWorld("espaconave"), 1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 473, 73, 491);
                        loc1.getBlock().setType(Material.AIR);
                        loc1.getBlock().setType(Material.SPAWNER);
                        final Block block = loc1.getBlock();
                        PlayerInteractInvFarm.setSpawner(block, MobsSpawnEvents.getEntityType(MobspawnAPI.getSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID())));
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "O mobspawn já se encontra ativado!");
                }
            }

            if (event.getSlot() == 42) {
                if (MobspawnAPI.getStatus(UUID + MobspawnerOpenedUtil.get(UUID).getID()) != false) {
                    MobspawnAPI.updateStatus(UUID + MobspawnerOpenedUtil.get(UUID).getID(), false);
                    player.sendMessage(ChatColor.GREEN + " * Mobspawn desativado!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfigsEspaçonave.getInvMobspawn(UUID + MobspawnerOpenedUtil.get(UUID).getID()));
                        }
                    }, 5L);

                    if (MobspawnerOpenedUtil.get(UUID).getID() == 1) {
                        final Location loc1 = new Location(Bukkit.getWorld("espaconave"), 1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 473, 73, 507);
                        loc1.getBlock().setType(Material.AIR);
                        loc1.getBlock().setType(Material.SPAWNER);
                    }
                    if (MobspawnerOpenedUtil.get(UUID).getID() == 2) {
                        final Location loc1 = new Location(Bukkit.getWorld("espaconave"), 1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 471, 73, 499);
                        loc1.getBlock().setType(Material.AIR);
                        loc1.getBlock().setType(Material.SPAWNER);
                    }
                    if (MobspawnerOpenedUtil.get(UUID).getID() == 3) {
                        final Location loc1 = new Location(Bukkit.getWorld("espaconave"), 1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 473, 73, 491);
                        loc1.getBlock().setType(Material.AIR);
                        loc1.getBlock().setType(Material.SPAWNER);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "O mobspawn já se encontra desativado!");
                }
            }

            //ADICIONAR +2 MOBS 1.13!
            if (event.getSlot() == 32) {
                if (PlayerInteractInvFarm.containsSpawner(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "ghast") == false) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, você não contém este mob!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "1").equalsIgnoreCase("ghast") && MobspawnAPI.getStatus(UUID + "1") == true && MobspawnerOpenedUtil.get(UUID).getID() != 1) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "2").equalsIgnoreCase("ghast") && MobspawnAPI.getStatus(UUID + "2") == true && MobspawnerOpenedUtil.get(UUID).getID() != 2) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "3").equalsIgnoreCase("ghast") && MobspawnAPI.getStatus(UUID + "3") == true && MobspawnerOpenedUtil.get(UUID).getID() != 3) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID()).equalsIgnoreCase("ghast")) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.GREEN + "Mob ativado!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    MobspawnAPI.updateSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "Ghast");

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfigsEspaçonave.getInvMobspawn(UUID + MobspawnerOpenedUtil.get(UUID).getID()));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 31) {
                if (PlayerInteractInvFarm.containsSpawner(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "aldeão zumbi") == false) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, você não contém este mob!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "1").equalsIgnoreCase("aldeão zumbi") && MobspawnAPI.getStatus(UUID + "1") == true && MobspawnerOpenedUtil.get(UUID).getID() != 1) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "2").equalsIgnoreCase("aldeão zumbi") && MobspawnAPI.getStatus(UUID + "2") == true && MobspawnerOpenedUtil.get(UUID).getID() != 2) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "3").equalsIgnoreCase("aldeão zumbi") && MobspawnAPI.getStatus(UUID + "3") == true && MobspawnerOpenedUtil.get(UUID).getID() != 3) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID()).equalsIgnoreCase("aldeão zumbi")) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.GREEN + "Mob ativado!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    MobspawnAPI.updateSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "Aldeão zumbi");

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfigsEspaçonave.getInvMobspawn(UUID + MobspawnerOpenedUtil.get(UUID).getID()));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 30) {
                if (PlayerInteractInvFarm.containsSpawner(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "cubo magma") == false) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, você não contém este mob!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "1").equalsIgnoreCase("cubo magma") && MobspawnAPI.getStatus(UUID + "1") == true && MobspawnerOpenedUtil.get(UUID).getID() != 1) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "2").equalsIgnoreCase("cubo magma") && MobspawnAPI.getStatus(UUID + "2") == true && MobspawnerOpenedUtil.get(UUID).getID() != 2) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "3").equalsIgnoreCase("cubo magma") && MobspawnAPI.getStatus(UUID + "3") == true && MobspawnerOpenedUtil.get(UUID).getID() != 3) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID()).equalsIgnoreCase("cubo magma")) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.GREEN + "Mob ativado!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    MobspawnAPI.updateSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "Cubo magma");

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfigsEspaçonave.getInvMobspawn(UUID + MobspawnerOpenedUtil.get(UUID).getID()));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 29) {
                if (PlayerInteractInvFarm.containsSpawner(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "bruxa") == false) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, você não contém este mob!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "1").equalsIgnoreCase("bruxa") && MobspawnAPI.getStatus(UUID + "1") == true && MobspawnerOpenedUtil.get(UUID).getID() != 1) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "2").equalsIgnoreCase("bruxa") && MobspawnAPI.getStatus(UUID + "2") == true && MobspawnerOpenedUtil.get(UUID).getID() != 2) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "3").equalsIgnoreCase("bruxa") && MobspawnAPI.getStatus(UUID + "3") == true && MobspawnerOpenedUtil.get(UUID).getID() != 3) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID()).equalsIgnoreCase("bruxa")) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.GREEN + "Mob ativado!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    MobspawnAPI.updateSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "Bruxa");

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfigsEspaçonave.getInvMobspawn(UUID + MobspawnerOpenedUtil.get(UUID).getID()));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 28) {
                if (PlayerInteractInvFarm.containsSpawner(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "esqueleto wither") == false) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, você não contém este mob!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "1").equalsIgnoreCase("esqueleto wither") && MobspawnAPI.getStatus(UUID + "1") == true && MobspawnerOpenedUtil.get(UUID).getID() != 1) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "2").equalsIgnoreCase("esqueleto wither") && MobspawnAPI.getStatus(UUID + "2") == true && MobspawnerOpenedUtil.get(UUID).getID() != 2) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "3").equalsIgnoreCase("esqueleto wither") && MobspawnAPI.getStatus(UUID + "3") == true && MobspawnerOpenedUtil.get(UUID).getID() != 3) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID()).equalsIgnoreCase("esqueleto wither")) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.GREEN + "Mob ativado!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    MobspawnAPI.updateSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "Esqueleto wither");

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfigsEspaçonave.getInvMobspawn(UUID + MobspawnerOpenedUtil.get(UUID).getID()));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 25) {
                if (PlayerInteractInvFarm.containsSpawner(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "homem porco zumbi") == false) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, você não contém este mob!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "1").equalsIgnoreCase("homem porco zumbi") && MobspawnAPI.getStatus(UUID + "1") == true && MobspawnerOpenedUtil.get(UUID).getID() != 1) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "2").equalsIgnoreCase("homem porco zumbi") && MobspawnAPI.getStatus(UUID + "2") == true && MobspawnerOpenedUtil.get(UUID).getID() != 2) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "3").equalsIgnoreCase("homem porco zumbi") && MobspawnAPI.getStatus(UUID + "3") == true && MobspawnerOpenedUtil.get(UUID).getID() != 3) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID()).equalsIgnoreCase("homem porco zumbi")) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.GREEN + "Mob ativado!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    MobspawnAPI.updateSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "Homem porco zumbi");

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfigsEspaçonave.getInvMobspawn(UUID + MobspawnerOpenedUtil.get(UUID).getID()));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 24) {
                if (PlayerInteractInvFarm.containsSpawner(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "blaze") == false) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, você não contém este mob!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "1").equalsIgnoreCase("blaze") && MobspawnAPI.getStatus(UUID + "1") == true && MobspawnerOpenedUtil.get(UUID).getID() != 1) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "2").equalsIgnoreCase("blaze") && MobspawnAPI.getStatus(UUID + "2") == true && MobspawnerOpenedUtil.get(UUID).getID() != 2) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "3").equalsIgnoreCase("blaze") && MobspawnAPI.getStatus(UUID + "3") == true && MobspawnerOpenedUtil.get(UUID).getID() != 3) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID()).equalsIgnoreCase("blaze")) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.GREEN + "Mob ativado!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    MobspawnAPI.updateSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "Blaze");

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfigsEspaçonave.getInvMobspawn(UUID + MobspawnerOpenedUtil.get(UUID).getID()));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 23) {
                if (PlayerInteractInvFarm.containsSpawner(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "slime") == false) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, você não contém este mob!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "1").equalsIgnoreCase("slime") && MobspawnAPI.getStatus(UUID + "1") == true && MobspawnerOpenedUtil.get(UUID).getID() != 1) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "2").equalsIgnoreCase("slime") && MobspawnAPI.getStatus(UUID + "2") == true && MobspawnerOpenedUtil.get(UUID).getID() != 2) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "3").equalsIgnoreCase("slime") && MobspawnAPI.getStatus(UUID + "3") == true && MobspawnerOpenedUtil.get(UUID).getID() != 3) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID()).equalsIgnoreCase("slime")) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.GREEN + "Mob ativado!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    MobspawnAPI.updateSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "Slime");

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfigsEspaçonave.getInvMobspawn(UUID + MobspawnerOpenedUtil.get(UUID).getID()));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 22) {
                if (PlayerInteractInvFarm.containsSpawner(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "aranha das cavernas") == false) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, você não contém este mob!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "1").equalsIgnoreCase("aranha das cavernas") && MobspawnAPI.getStatus(UUID + "1") == true && MobspawnerOpenedUtil.get(UUID).getID() != 1) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "2").equalsIgnoreCase("aranha das cavernas") && MobspawnAPI.getStatus(UUID + "2") == true && MobspawnerOpenedUtil.get(UUID).getID() != 2) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "3").equalsIgnoreCase("aranha das cavernas") && MobspawnAPI.getStatus(UUID + "3") == true && MobspawnerOpenedUtil.get(UUID).getID() != 3) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID()).equalsIgnoreCase("aranha das cavernas")) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.GREEN + "Mob ativado!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    MobspawnAPI.updateSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "Aranha das cavernas");

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfigsEspaçonave.getInvMobspawn(UUID + MobspawnerOpenedUtil.get(UUID).getID()));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 21) {
                if (PlayerInteractInvFarm.containsSpawner(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "enderman") == false) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, você não contém este mob!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "1").equalsIgnoreCase("enderman") && MobspawnAPI.getStatus(UUID + "1") == true && MobspawnerOpenedUtil.get(UUID).getID() != 1) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "2").equalsIgnoreCase("enderman") && MobspawnAPI.getStatus(UUID + "2") == true && MobspawnerOpenedUtil.get(UUID).getID() != 2) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "3").equalsIgnoreCase("enderman") && MobspawnAPI.getStatus(UUID + "3") == true && MobspawnerOpenedUtil.get(UUID).getID() != 3) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID()).equalsIgnoreCase("enderman")) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.GREEN + "Mob ativado!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    MobspawnAPI.updateSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "Enderman");

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfigsEspaçonave.getInvMobspawn(UUID + MobspawnerOpenedUtil.get(UUID).getID()));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 20) {
                if (PlayerInteractInvFarm.containsSpawner(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "creeper") == false) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, você não contém este mob!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "1").equalsIgnoreCase("creeper") && MobspawnAPI.getStatus(UUID + "1") == true && MobspawnerOpenedUtil.get(UUID).getID() != 1) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "2").equalsIgnoreCase("creeper") && MobspawnAPI.getStatus(UUID + "2") == true && MobspawnerOpenedUtil.get(UUID).getID() != 2) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "3").equalsIgnoreCase("creeper") && MobspawnAPI.getStatus(UUID + "3") == true && MobspawnerOpenedUtil.get(UUID).getID() != 3) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID()).equalsIgnoreCase("creeper")) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.GREEN + "Mob ativado!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    MobspawnAPI.updateSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "Creeper");

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfigsEspaçonave.getInvMobspawn(UUID + MobspawnerOpenedUtil.get(UUID).getID()));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 19) {
                if (PlayerInteractInvFarm.containsSpawner(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "aranha") == false) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, você não contém este mob!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "1").equalsIgnoreCase("aranha") && MobspawnAPI.getStatus(UUID + "1") == true && MobspawnerOpenedUtil.get(UUID).getID() != 1) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "2").equalsIgnoreCase("aranha") && MobspawnAPI.getStatus(UUID + "2") == true && MobspawnerOpenedUtil.get(UUID).getID() != 2) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "3").equalsIgnoreCase("aranha") && MobspawnAPI.getStatus(UUID + "3") == true && MobspawnerOpenedUtil.get(UUID).getID() != 3) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID()).equalsIgnoreCase("aranha")) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.GREEN + "Mob ativado!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    MobspawnAPI.updateSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "Aranha");

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfigsEspaçonave.getInvMobspawn(UUID + MobspawnerOpenedUtil.get(UUID).getID()));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 16) {
                if (PlayerInteractInvFarm.containsSpawner(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "esqueleto") == false) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, você não contém este mob!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "1").equalsIgnoreCase("esqueleto") && MobspawnAPI.getStatus(UUID + "1") == true && MobspawnerOpenedUtil.get(UUID).getID() != 1) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "2").equalsIgnoreCase("esqueleto") && MobspawnAPI.getStatus(UUID + "2") == true && MobspawnerOpenedUtil.get(UUID).getID() != 2) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "3").equalsIgnoreCase("esqueleto") && MobspawnAPI.getStatus(UUID + "3") == true && MobspawnerOpenedUtil.get(UUID).getID() != 3) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID()).equalsIgnoreCase("esqueleto")) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.GREEN + "Mob ativado!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    MobspawnAPI.updateSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "Esqueleto");

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfigsEspaçonave.getInvMobspawn(UUID + MobspawnerOpenedUtil.get(UUID).getID()));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 15) {
                if (PlayerInteractInvFarm.containsSpawner(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "zumbi") == false) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, você não contém este mob!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "1").equalsIgnoreCase("zumbi") && MobspawnAPI.getStatus(UUID + "1") == true && MobspawnerOpenedUtil.get(UUID).getID() != 1) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "2").equalsIgnoreCase("zumbi") && MobspawnAPI.getStatus(UUID + "2") == true && MobspawnerOpenedUtil.get(UUID).getID() != 2) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "3").equalsIgnoreCase("zumbi") && MobspawnAPI.getStatus(UUID + "3") == true && MobspawnerOpenedUtil.get(UUID).getID() != 3) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID()).equalsIgnoreCase("zumbi")) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.GREEN + "Mob ativado!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    MobspawnAPI.updateSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "Zumbi");

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfigsEspaçonave.getInvMobspawn(UUID + MobspawnerOpenedUtil.get(UUID).getID()));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 14) {
                if (PlayerInteractInvFarm.containsSpawner(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "coelho") == false) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, você não contém este mob!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "1").equalsIgnoreCase("coelho") && MobspawnAPI.getStatus(UUID + "1") == true && MobspawnerOpenedUtil.get(UUID).getID() != 1) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "2").equalsIgnoreCase("coelho") && MobspawnAPI.getStatus(UUID + "2") == true && MobspawnerOpenedUtil.get(UUID).getID() != 2) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "3").equalsIgnoreCase("coelho") && MobspawnAPI.getStatus(UUID + "3") == true && MobspawnerOpenedUtil.get(UUID).getID() != 3) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID()).equalsIgnoreCase("coelho")) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.GREEN + "Mob ativado!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    MobspawnAPI.updateSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "Coelho");

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfigsEspaçonave.getInvMobspawn(UUID + MobspawnerOpenedUtil.get(UUID).getID()));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 13) {
                if (PlayerInteractInvFarm.containsSpawner(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "ovelha") == false) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, você não contém este mob!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "1").equalsIgnoreCase("ovelha") && MobspawnAPI.getStatus(UUID + "1") == true && MobspawnerOpenedUtil.get(UUID).getID() != 1) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "2").equalsIgnoreCase("ovelha") && MobspawnAPI.getStatus(UUID + "2") == true && MobspawnerOpenedUtil.get(UUID).getID() != 2) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "3").equalsIgnoreCase("ovelha") && MobspawnAPI.getStatus(UUID + "3") == true && MobspawnerOpenedUtil.get(UUID).getID() != 3) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID()).equalsIgnoreCase("ovelha")) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.GREEN + "Mob ativado!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    MobspawnAPI.updateSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "Ovelha");

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfigsEspaçonave.getInvMobspawn(UUID + MobspawnerOpenedUtil.get(UUID).getID()));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 12) {
                if (PlayerInteractInvFarm.containsSpawner(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "porco") == false) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, você não contém este mob!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "1").equalsIgnoreCase("porco") && MobspawnAPI.getStatus(UUID + "1") == true && MobspawnerOpenedUtil.get(UUID).getID() != 1) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "2").equalsIgnoreCase("porco") && MobspawnAPI.getStatus(UUID + "2") == true && MobspawnerOpenedUtil.get(UUID).getID() != 2) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "3").equalsIgnoreCase("porco") && MobspawnAPI.getStatus(UUID + "3") == true && MobspawnerOpenedUtil.get(UUID).getID() != 3) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID()).equalsIgnoreCase("porco")) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.GREEN + "Mob ativado!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    MobspawnAPI.updateSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "Porco");

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfigsEspaçonave.getInvMobspawn(UUID + MobspawnerOpenedUtil.get(UUID).getID()));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 11) {
                if (PlayerInteractInvFarm.containsSpawner(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "galinha") == false) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, você não contém este mob!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "1").equalsIgnoreCase("galinha") && MobspawnAPI.getStatus(UUID + "1") == true && MobspawnerOpenedUtil.get(UUID).getID() != 1) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "2").equalsIgnoreCase("galinha") && MobspawnAPI.getStatus(UUID + "2") == true && MobspawnerOpenedUtil.get(UUID).getID() != 2) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "3").equalsIgnoreCase("galinha") && MobspawnAPI.getStatus(UUID + "3") == true && MobspawnerOpenedUtil.get(UUID).getID() != 3) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID()).equalsIgnoreCase("galinha")) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.GREEN + "Mob ativado!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    MobspawnAPI.updateSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "Galinha");

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfigsEspaçonave.getInvMobspawn(UUID + MobspawnerOpenedUtil.get(UUID).getID()));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 10) {
                if (PlayerInteractInvFarm.containsSpawner(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "vaca") == false) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, você não contém este mob!");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "1").equalsIgnoreCase("vaca") && MobspawnAPI.getStatus(UUID + "1") == true && MobspawnerOpenedUtil.get(UUID).getID() != 1) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "2").equalsIgnoreCase("vaca") && MobspawnAPI.getStatus(UUID + "2") == true && MobspawnerOpenedUtil.get(UUID).getID() != 2) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + "3").equalsIgnoreCase("vaca") && MobspawnAPI.getStatus(UUID + "3") == true && MobspawnerOpenedUtil.get(UUID).getID() != 3) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo em outro mobspawner!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return;
                }
                if (MobspawnAPI.getSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID()).equalsIgnoreCase("vaca")) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Ops, este mob já está ativo!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.GREEN + "Mob ativado!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    MobspawnAPI.updateSpawnAtivo(UUID + MobspawnerOpenedUtil.get(UUID).getID(), "Vaca");

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfigsEspaçonave.getInvMobspawn(UUID + MobspawnerOpenedUtil.get(UUID).getID()));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 18) {
                event.setCancelled(true);

                if (!MobspawnAPI.getSpawners(UUID + "1").equals("NULL") || MobspawnAPI.getSpawners(UUID + "1") == null) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfigsEspaçonave.getMobspawners(UUID, false, null));
                        }
                    }, 5L);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, você não contém nenhum spawner.");
                }
            }

            if (event.getSlot() == 40) {
                event.setCancelled(true);
            }

            if (event.getSlot() == 26) {
                if (event.getCursor().getType().equals(Material.SPAWNER) && event.getCursor().getItemMeta().getDisplayName() != null) {
                    final String itemName = event.getCursor().clone().getItemMeta().getDisplayName();
                    int quantia = event.getCursor().clone().getAmount();

                    final String[] mobs = {"Vaca", "Galinha", "Porco", "Ovelha", "Coelho", "Zumbi", "Esqueleto", "Aranha", "Creeper", "Enderman", "Aranha das cavernas", "Slime", "Blaze", "Homem porco zumbi", "Esqueleto wither", "Bruxa", "Cubo magma", "Aldeão zumbi", "Ghast"};
                    for (int i = 1; i <= mobs.length; i++) {
                        if (itemName.equals(ChatColor.RED + "Spawner de " + mobs[i - 1])) {
                            this.isItem = true;
                        }

                        if (i == mobs.length) {
                            if (this.isItem == false) {
                                event.setCancelled(true);
                                return;
                            } else {
                                this.isItem = false;
                            }
                        }
                    }

                    event.getCursor().setType(Material.AIR);
                    event.getCursor().setAmount(0);

                    event.setCancelled(true);

                    final Random random = new Random();
                    if (MobspawnAPI.getSpawners(UUID + "1").contains(":")) {
                        final String[] list = MobspawnAPI.getSpawners(UUID + "1").split(":");
                        for (int i = 1; i <= list.length; i++) {
                            final String[] item = list[list.length - i].split("-");
                            if (item[0].equals(itemName)) {
                                if (Integer.valueOf(item[1]) < 64) {
                                    quantia = quantia + Integer.valueOf(item[1]);

                                    for (int x = 1; x <= list.length; x++) {
                                        if (x - 1 != list.length - i) {
                                            if (this.itensNew == null) {
                                                this.itensNew = list[x - 1];
                                            } else {
                                                this.itensNew = this.itensNew + ":" + list[x - 1];
                                            }
                                        }

                                        if (x == list.length) {
                                            MobspawnAPI.updateSpawners(UUID + "1", this.itensNew);
                                            MobspawnAPI.updateSpawners(UUID + "2", this.itensNew);
                                            MobspawnAPI.updateSpawners(UUID + "3", this.itensNew);
                                            this.itensNew = null;
                                        }
                                    }
                                }
                            }
                        }
                        if (quantia > 64) {
                            MobspawnAPI.updateSpawners(UUID + "1", MobspawnAPI.getSpawners(UUID + "1") + ":" + itemName + "-" + 64 + "-" + random.nextInt(10000));
                            MobspawnAPI.updateSpawners(UUID + "2", MobspawnAPI.getSpawners(UUID + "1") + ":" + itemName + "-" + 64 + "-" + random.nextInt(10000));
                            MobspawnAPI.updateSpawners(UUID + "3", MobspawnAPI.getSpawners(UUID + "1") + ":" + itemName + "-" + 64 + "-" + random.nextInt(10000));
                            quantia = quantia - 64;
                            MobspawnAPI.updateSpawners(UUID + "1", MobspawnAPI.getSpawners(UUID + "1") + ":" + itemName + "-" + quantia + "-" + random.nextInt(10000));
                            MobspawnAPI.updateSpawners(UUID + "2", MobspawnAPI.getSpawners(UUID + "1") + ":" + itemName + "-" + quantia + "-" + random.nextInt(10000));
                            MobspawnAPI.updateSpawners(UUID + "3", MobspawnAPI.getSpawners(UUID + "1") + ":" + itemName + "-" + quantia + "-" + random.nextInt(10000));
                        } else {
                            MobspawnAPI.updateSpawners(UUID + "1", MobspawnAPI.getSpawners(UUID + "1") + ":" + itemName + "-" + quantia + "-" + random.nextInt(10000));
                            MobspawnAPI.updateSpawners(UUID + "2", MobspawnAPI.getSpawners(UUID + "1") + ":" + itemName + "-" + quantia + "-" + random.nextInt(10000));
                            MobspawnAPI.updateSpawners(UUID + "3", MobspawnAPI.getSpawners(UUID + "1") + ":" + itemName + "-" + quantia + "-" + random.nextInt(10000));
                        }
                    } else if (!MobspawnAPI.getSpawners(UUID + "1").equals("NULL")) {
                        final String[] item = MobspawnAPI.getSpawners(UUID + "1").split("-");
                        if (item[0].equals(itemName)) {
                            if (Integer.valueOf(item[1]) < 64) {
                                quantia = quantia + Integer.valueOf(item[1]);

                                MobspawnAPI.updateSpawners(UUID + "1", "NULL");
                                MobspawnAPI.updateSpawners(UUID + "2", "NULL");
                                MobspawnAPI.updateSpawners(UUID + "3", "NULL");
                            }
                        }
                        if (quantia > 64) {
                            MobspawnAPI.updateSpawners(UUID + "1", itemName + "-" + 64 + "-" + random.nextInt(10000));
                            MobspawnAPI.updateSpawners(UUID + "2", itemName + "-" + 64 + "-" + random.nextInt(10000));
                            MobspawnAPI.updateSpawners(UUID + "3", itemName + "-" + 64 + "-" + random.nextInt(10000));
                            quantia = quantia - 64;
                            MobspawnAPI.updateSpawners(UUID + "1", MobspawnAPI.getSpawners(UUID + "1") + ":" + itemName + "-" + quantia + "-" + random.nextInt(10000));
                            MobspawnAPI.updateSpawners(UUID + "2", MobspawnAPI.getSpawners(UUID + "1") + ":" + itemName + "-" + quantia + "-" + random.nextInt(10000));
                            MobspawnAPI.updateSpawners(UUID + "3", MobspawnAPI.getSpawners(UUID + "1") + ":" + itemName + "-" + quantia + "-" + random.nextInt(10000));
                        } else if (!MobspawnAPI.getSpawners(UUID + "1").equals("NULL")) {
                            MobspawnAPI.updateSpawners(UUID + "1", MobspawnAPI.getSpawners(UUID + "1") + ":" + itemName + "-" + quantia + "-" + random.nextInt(10000));
                            MobspawnAPI.updateSpawners(UUID + "2", MobspawnAPI.getSpawners(UUID + "1") + ":" + itemName + "-" + quantia + "-" + random.nextInt(10000));
                            MobspawnAPI.updateSpawners(UUID + "3", MobspawnAPI.getSpawners(UUID + "1") + ":" + itemName + "-" + quantia + "-" + random.nextInt(10000));
                        } else {
                            MobspawnAPI.updateSpawners(UUID + "1", itemName + "-" + quantia + "-" + random.nextInt(10000));
                            MobspawnAPI.updateSpawners(UUID + "2", itemName + "-" + quantia + "-" + random.nextInt(10000));
                            MobspawnAPI.updateSpawners(UUID + "3", itemName + "-" + quantia + "-" + random.nextInt(10000));
                        }
                    } else {
                        MobspawnAPI.updateSpawners(UUID + "1", itemName + "-" + quantia + "-" + random.nextInt(10000));
                        MobspawnAPI.updateSpawners(UUID + "2", itemName + "-" + quantia + "-" + random.nextInt(10000));
                        MobspawnAPI.updateSpawners(UUID + "3", itemName + "-" + quantia + "-" + random.nextInt(10000));
                    }

                    player.sendMessage(ChatColor.GREEN + "\n \n * Spawner adicionado com sucesso!\n \n");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    event.setCancelled(true);
                }
            }

            if (event.getClickedInventory().getName().equals("Configuração mobspawner: ")) {
                event.setCancelled(true);
            }
        }

        if (event.getInventory().getName().equalsIgnoreCase("Escolha opção de pagamento:  ")) {
            event.setCancelled(true);
            if (event.getSlot() == 12) {
                if (InfoMoedaUtil.get(UUID) == null) {
                    new InfoMoedaUtil(UUID, "comum").insert();
                } else {
                    InfoMoedaUtil.get(UUID).setMoeda("comum");
                }
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventoryConfigsEspaçonave.getConfirmarCompra());
                    }
                }, 5L);
            }

            if (event.getSlot() == 14) {
                if (InfoMoedaUtil.get(UUID) == null) {
                    new InfoMoedaUtil(UUID, "negra").insert();
                } else {
                    InfoMoedaUtil.get(UUID).setMoeda("negra");
                }
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventoryConfigsEspaçonave.getConfirmarCompra());
                    }
                }, 5L);
            }
        }

        if (event.getInventory().getName().equalsIgnoreCase("Configurar plot: ")) {
            if (event.getClick().equals(ClickType.SHIFT_LEFT)) event.setCancelled(true);
            if (event.getSlot() == 14) {
                event.setCancelled(true);

                if (RenomearItemUtil.get(UUID) == null) {
                    new RenomearItemUtil(UUID, true).insert();
                } else {
                    RenomearItemUtil.get(UUID).setStatus(true);
                }

                player.getOpenInventory().close();
                player.sendMessage(ChatColor.GREEN + "\n \n * Digite no chat o nome desejado para o plot.\n" + ChatColor.DARK_GRAY + "   Expira em 20 segundos.\n \n");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (RenomearItemUtil.get(UUID).getStatus() == true) {
                            RenomearItemUtil.get(UUID).setStatus(false);
                            player.sendMessage(ChatColor.RED + "\n \n * Renomeação do plot cancelado pela demora!\n \n");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    }
                }, 400L);
            } else if (event.getSlot() == 15) {
                event.setCancelled(true);
                if (!event.getCursor().getType().equals(Material.AIR)) {
                    final String bloco = event.getCursor().getType().name();
                    FarmAPI.updateBloco(UUID + PlotSelectedUtil.get(UUID).getID(), bloco);
                    player.sendMessage(ChatColor.GREEN + " * Bloco alterado com sucesso!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(InventoryConfigsEspaçonave.getConfigPlot(UUID + PlotSelectedUtil.get(UUID).getID()));
                        }
                    }, 5L);
                }
            } else if (event.getSlot() == 18) {
                event.setCancelled(true);

                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventoryConfigsEspaçonave.getInvPlots(UUID));
                    }
                }, 5L);
            } else {
                if (event.getClickedInventory().getName().equalsIgnoreCase(player.getOpenInventory().getTopInventory().getName())) {
                    event.setCancelled(true);
                }
            }
        }

        if (event.getInventory().getName().equalsIgnoreCase("Confirmar compra plot: ")) {
            event.setCancelled(true);

            if (InfoMoedaUtil.get(UUID) != null && InfoMoedaUtil.get(UUID).getMoeda().equals("comum")) {
                if (CoinsAPI.getCoins(UUID) >= 5000) {
                    for (int i = 1; i <= 22; i++) {
                        if (i == 22) {
                            player.sendMessage(ChatColor.RED + " * Ops, você já atingiu o limite de plots!");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            player.getOpenInventory().close();
                            InfoMoedaUtil.get(UUID).setMoeda("NULL");
                            break;
                        }
                        if (FarmAPI.getTexto(UUID + i) == null) {
                            CoinsAPI.updateCoins(UUID, CoinsAPI.getCoins(UUID) - 5000);
                            player.sendMessage(ChatColor.GREEN + "\n \n * Plot adquirido com sucesso!\n" + ChatColor.DARK_GRAY + "  Foram debitados de sua conta\n  5.000,00 moedas!\n \n");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                            player.getOpenInventory().close();

                            FarmAPI.setFarm(UUID + i, ChatColor.YELLOW + "Farm-" + i, false);
                            break;
                        }
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, você não contém dinheiro para efetuar esta compra =(");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            } else if (InfoMoedaUtil.get(UUID) != null && InfoMoedaUtil.get(UUID).getMoeda().equals("negra")) {
                if (CoinsAPI.getBlackCoins(UUID) >= 100) {
                    for (int i = 1; i <= 22; i++) {
                        if (i == 22) {
                            player.sendMessage(ChatColor.RED + " * Ops, você já atingiu o limite de plots!");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            player.getOpenInventory().close();
                            InfoMoedaUtil.get(UUID).setMoeda("NULL");
                            break;
                        }
                        if (FarmAPI.getTexto(UUID + i) == null) {
                            CoinsAPI.updateBlackCoins(UUID, CoinsAPI.getBlackCoins(UUID) - 100);
                            player.sendMessage(ChatColor.GREEN + "\n \n * Plot adquirido com sucesso!\n" + ChatColor.DARK_GRAY + "  Foram debitados de sua conta\n  100,00 moedas negras!\n \n");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                            player.getOpenInventory().close();

                            FarmAPI.setFarm(UUID + i, ChatColor.YELLOW + "Farm-" + i, false);
                            break;
                        }
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, você não contém dinheiro para efetuar esta compra =(");
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }
        }

        if (event.getInventory().getName().equalsIgnoreCase("Farms: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 36) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventoryConfigsEspaçonave.getInvFarm());
                        return;
                    }
                }, 5L);
            }

            if (event.getSlot() != 36) {
                if (event.getClickedInventory().getName().equalsIgnoreCase(player.getOpenInventory().getTopInventory().getName())) {
                    if (!event.getCurrentItem().getType().equals(Material.AIR) && event.isLeftClick()) {
                        final int slot = event.getSlot();
                        if (slot <= 16) {
                            this.ID = slot - 9;
                        } else if (slot <= 25) {
                            this.ID = slot - 11;
                        } else if (slot <= 34) {
                            this.ID = slot - 13;
                        }

                        if (FarmAPI.getStatus(UUID + this.ID) == true) {
                            player.sendMessage(ChatColor.RED + " * Ops, este plot já está ativo!");
                            return;
                        }

                        for (int i = 1; i <= 21; i++) {
                            if (FarmAPI.getStatus(UUID + i) == true) {
                                this.IDSave = i;
                                break;
                            }
                        }

                        //save
                        final int x1 = 1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 515;
                        final Location loc1 = new Location(Bukkit.getWorld("espaconave"), x1, 70, 485);

                        final int x2 = 1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 487;
                        final Location loc2 = new Location(Bukkit.getWorld("espaconave"), x2, 89, 513);
                        SaveSchematicUtil.saveTerrain(Main.plugin.getDataFolder() + File.separator + "Plots/" + UUID + this.IDSave, loc1, loc2);
                        FarmAPI.updateStatus(UUID + this.IDSave, false);

                        //paste
                        final File schematic = new File(Main.plugin.getDataFolder() + File.separator + "Plots/" + UUID + this.ID + ".schem");
                        if (schematic.exists()) {
                            WorldEditUtil.paste(WorldEditUtil.load(Main.plugin.getDataFolder() + File.separator + "Plots/" + UUID + this.ID), false, loc1);
                        } else {
                            WorldEditUtil.paste(WorldEditUtil.load(Main.plugin.getDataFolder() + File.separator + "Plots/vazio"), false, loc1);
                        }
                        FarmAPI.updateStatus(UUID + this.ID, true);
                        player.sendMessage(ChatColor.GREEN + "\n \n * Farm carregada com sucesso.\n \n");
                        player.getOpenInventory().close();
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else if (!event.getCurrentItem().getType().equals(Material.AIR) && event.isRightClick()) {
                        final int slot = event.getSlot();
                        if (slot <= 16) {
                            this.ID = slot - 9;
                        } else if (slot <= 25) {
                            this.ID = slot - 11;
                        } else if (slot <= 34) {
                            this.ID = slot - 13;
                        }

                        if (PlotSelectedUtil.get(UUID) == null) {
                            new PlotSelectedUtil(UUID, this.ID).insert();
                        } else {
                            PlotSelectedUtil.get(UUID).setID(this.ID);
                        }

                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                player.openInventory(InventoryConfigsEspaçonave.getConfigPlot(UUID + PlayerInteractInvFarm.this.ID));
                            }
                        }, 5L);
                    }
                }
            }
        }

        if (event.getInventory().getName().equalsIgnoreCase("Configuração farm: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 11) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventoryConfigsEspaçonave.getInvPlots(UUID));
                    }
                }, 5L);
            }

            if (event.getSlot() == 13) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(InventoryConfigsEspaçonave.getConfirmarMoeda());
                    }
                }, 5L);
            }

            if (event.getSlot() == 15) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + " * EM BREVE!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
    }
}
