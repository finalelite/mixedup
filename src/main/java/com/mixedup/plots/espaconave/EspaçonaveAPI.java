package com.mixedup.plots.espaconave;

import com.mixedup.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EspaçonaveAPI {

    public static void setInfos(final String UUID, final String moeda) {
        new EspaçonaveInfosCache(UUID, 1, 1, 1, false, true, true, moeda, "NULL").insert();
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Espaçonave_infos(UUID, NivelDrops, NivelMobspawn, NivelGerador, PvP, Visitas, StatusPvP, MoedaUtilizada, Amigos) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
            st.setString(1, UUID);
            st.setInt(2, 1);
            st.setInt(3, 1);
            st.setInt(4, 1);
            st.setBoolean(5, false);
            st.setBoolean(6, true);
            st.setBoolean(7, true);
            st.setString(8, moeda);
            st.setString(9, "NULL");
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeEspaçonaveInfos(final String UUID) {
        EspaçonaveInfosCache.CACHE.remove(UUID);
        try {
            final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Espaçonave_infos WHERE UUID = ?");
            st.setString(1, UUID);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeEspaçonave(final String UUID) {
        EspaçonaveInfosCache.CACHE.remove(UUID);
        try {
            final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Espaçonave_data WHERE UUID = ?");
            st.setString(1, UUID);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateAmigos(final String UUID, final String amigos) {
        EspaçonaveInfosCache.get(UUID).setAmigos(amigos);
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Espaçonave_infos SET Amigos = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setString(1, amigos);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateMoedaUtilizada(final String UUID, final String moeda) {
        EspaçonaveInfosCache.get(UUID).setMoedaUtilizada(moeda);
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Espaçonave_infos SET MoedaUtilizada = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setString(1, moeda);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateStatusPvP(final String UUID, final boolean status) {
        EspaçonaveInfosCache.get(UUID).setStatusPvP(status);
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Espaçonave_infos SET StatusPvP = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setBoolean(1, status);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateVisitas(final String UUID, final boolean status) {
        EspaçonaveInfosCache.get(UUID).setVisita(status);
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Espaçonave_infos SET Visitas = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setBoolean(1, status);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updatePvP(final String UUID, final boolean status) {
        EspaçonaveInfosCache.get(UUID).setPvP(status);
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Espaçonave_infos SET PvP = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setBoolean(1, status);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateNivelGerador(final String UUID, final int nivel) {
        EspaçonaveInfosCache.get(UUID).setNivelGerador(nivel);
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Espaçonave_infos SET NivelGerador = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setInt(1, nivel);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateNivelMobspawn(final String UUID, final int nivel) {
        EspaçonaveInfosCache.get(UUID).setNivelMobspawn(nivel);
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Espaçonave_infos SET NivelMobspawn = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setInt(1, nivel);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateNivelDrops(final String UUID, final int nivel) {
        EspaçonaveInfosCache.get(UUID).setNivelDrops(nivel);
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Espaçonave_infos SET NivelDrops = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setInt(1, nivel);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getAmigos(final String UUID) {
        if (EspaçonaveInfosCache.get(UUID) != null) {
            return EspaçonaveInfosCache.get(UUID).getAmigos();
        } else {
            return null;
        }
        //try {
        //            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Espaçonave_infos WHERE UUID = ?");
        //            st.setString(1, UUID);
        //            ResultSet rs = st.executeQuery();
        //            while (rs.next()) {
        //                return rs.getString("Amigos");
        //            }
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
        //        return null;
    }

    public static String getMoedaUtilizada(final String UUID) {
        if (EspaçonaveInfosCache.get(UUID) != null) {
            return EspaçonaveInfosCache.get(UUID).getMoedaUtilizada();
        } else {
            return null;
        }
        //try {
        //            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Espaçonave_infos WHERE UUID = ?");
        //            st.setString(1, UUID);
        //            ResultSet rs = st.executeQuery();
        //            while (rs.next()) {
        //                return rs.getString("MoedaUtilizada");
        //            }
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
        //        return null;
    }

    public static boolean getPvPStatus(final String UUID) {
        if (EspaçonaveInfosCache.get(UUID) != null) {
            return EspaçonaveInfosCache.get(UUID).getStatusPvP();
        } else {
            return true;
        }
        //try {
        //            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Espaçonave_infos WHERE UUID = ?");
        //            st.setString(1, UUID);
        //            ResultSet rs = st.executeQuery();
        //            while (rs.next()) {
        //                return rs.getBoolean("StatusPvP");
        //            }
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
        //        return true;
    }

    public static boolean getVisitas(final String UUID) {
        if (EspaçonaveInfosCache.get(UUID) != null) {
            return EspaçonaveInfosCache.get(UUID).getVisitas();
        } else {
            return true;
        }
        //try {
        //            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Espaçonave_infos WHERE UUID = ?");
        //            st.setString(1, UUID);
        //            ResultSet rs = st.executeQuery();
        //            while (rs.next()) {
        //                return rs.getBoolean("Visitas");
        //            }
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
        //        return true;
    }

    public static boolean getPvP(final String UUID) {
        if (EspaçonaveInfosCache.get(UUID) != null) {
            return EspaçonaveInfosCache.get(UUID).getPvP();
        } else {
            return false;
        }
        // try {
        //            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Espaçonave_infos WHERE UUID = ?");
        //            st.setString(1, UUID);
        //            ResultSet rs = st.executeQuery();
        //            while (rs.next()) {
        //                return rs.getBoolean("PvP");
        //            }
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
        //        return false;
    }

    public static int getNivelGerador(final String UUID) {
        if (EspaçonaveInfosCache.get(UUID) != null) {
            return EspaçonaveInfosCache.get(UUID).getNivelGerador();
        } else {
            return 1;
        }
        // try {
        //            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Espaçonave_infos WHERE UUID = ?");
        //            st.setString(1, UUID);
        //            ResultSet rs = st.executeQuery();
        //            while (rs.next()) {
        //                return rs.getInt("NivelGerador");
        //            }
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
        //        return 1;
    }

    public static int getNivelMobspawn(final String UUID) {
        if (EspaçonaveInfosCache.get(UUID) != null) {
            return EspaçonaveInfosCache.get(UUID).getNivelMobspawn();
        } else {
            return 1;
        }
        //try {
        //            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Espaçonave_infos WHERE UUID = ?");
        //            st.setString(1, UUID);
        //            ResultSet rs = st.executeQuery();
        //            while (rs.next()) {
        //                return rs.getInt("NivelMobspawn");
        //            }
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
        //        return 1;
    }

    public static int getNivelDrops(final String UUID) {
        if (EspaçonaveInfosCache.get(UUID) != null) {
            return EspaçonaveInfosCache.get(UUID).getNivelDrops();
        } else {
            return 1;
        }
        //try {
        //            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Espaçonave_infos WHERE UUID = ?");
        //            st.setString(1, UUID);
        //            ResultSet rs = st.executeQuery();
        //            while (rs.next()) {
        //                return rs.getInt("NivelDrops");
        //            }
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
        //        return 1;
    }

    public static void setEspaçonave(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Espaçonave_data(UUID) VALUES (?)");
            st.setString(1, UUID);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static int getIdEspaçonave(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Espaçonave_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("ID");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
