package com.mixedup.plots.espaconave;

import com.mixedup.Main;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.RegionsSpawnAPI;
import com.mixedup.apis.TagAPI;
import com.mixedup.hashs.WrongLocation;
import com.mixedup.plots.terrain.TerrainAPI;
import com.mixedup.plots.terrain.TerrainsUtil;
import com.mixedup.plots.utils.LastPositionUtil;
import com.mixedup.plots.utils.PlayerLoc;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockIgniteEvent.IgniteCause;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Random;

public class PlayerInteractEspaçonave implements Listener {

    public static void timeChange() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
            public void run() {
                final World world = Bukkit.getWorld("espaconave");
                world.setTime(19000);
                world.setStorm(false);
                world.setThundering(false);
            }
        }, 20L, 20L);
    }

    @EventHandler
    public void interactInvs(final PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (player.getLocation().getWorld().getName().equalsIgnoreCase("espaconave")) {
            final String[] invsFarmList = {(1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 484) + ":69:499", (1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 501) + ":69:482", (1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 518) + ":69:499", (1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 501) + ":69:516"};
            final String[] invsMobspawnList = {(1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 471) + ":71:507", (1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 469) + ":71:499", (1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 471) + ":71:491"};

            if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && event.getClickedBlock().getType().equals(Material.DAYLIGHT_DETECTOR)) {
                event.setCancelled(true);
                final Location blockLoc = event.getClickedBlock().getLocation();
                final String block = blockLoc.getBlockX() + ":" + blockLoc.getBlockY() + ":" + blockLoc.getBlockZ();

                if (block.equals((1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 501) + ":72:530")) {
                    player.openInventory(InventoryConfigsEspaçonave.getInvPaineis(UUID));
                }
            }

            if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && event.getClickedBlock().getType().equals(Material.SPAWNER)) {
                final Location blockLoc = event.getClickedBlock().getLocation();
                final String block = blockLoc.getBlockX() + ":" + blockLoc.getBlockY() + ":" + blockLoc.getBlockZ();

                for (int i = 1; i <= invsMobspawnList.length; i++) {
                    if (invsMobspawnList[i - 1].equals(block)) {
                        event.setCancelled(true);

                        if (block.equals((1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 471) + ":71:507")) {
                            player.openInventory(InventoryConfigsEspaçonave.getInvMobspawn(UUID + "1"));
                            if (MobspawnerOpenedUtil.get(UUID) == null) {
                                new MobspawnerOpenedUtil(UUID, 1).insert();
                            } else {
                                MobspawnerOpenedUtil.get(UUID).setID(1);
                            }
                        } else if (block.equals((1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 469) + ":71:499")) {
                            player.openInventory(InventoryConfigsEspaçonave.getInvMobspawn(UUID + "2"));
                            if (MobspawnerOpenedUtil.get(UUID) == null) {
                                new MobspawnerOpenedUtil(UUID, 2).insert();
                            } else {
                                MobspawnerOpenedUtil.get(UUID).setID(2);
                            }
                        } else if (block.equals((1000 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 471) + ":71:491")) {
                            player.openInventory(InventoryConfigsEspaçonave.getInvMobspawn(UUID + "3"));
                            if (MobspawnerOpenedUtil.get(UUID) == null) {
                                new MobspawnerOpenedUtil(UUID, 3).insert();
                            } else {
                                MobspawnerOpenedUtil.get(UUID).setID(3);
                            }
                        }
                    }
                }
            }

            if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && event.getClickedBlock().getType().equals(Material.CRAFTING_TABLE)) {
                final Location blockLoc = event.getClickedBlock().getLocation();
                final String block = blockLoc.getBlockX() + ":" + blockLoc.getBlockY() + ":" + blockLoc.getBlockZ();

                for (int i = 1; i <= invsFarmList.length; i++) {
                    if (invsFarmList[i - 1].equals(block)) {
                        event.setCancelled(true);
                        player.openInventory(InventoryConfigsEspaçonave.getInvFarm());
                    }
                }
            }
        }
    }

    @EventHandler
    public void onBlockIgnite(final BlockIgniteEvent event) {
        if (event.getCause() == IgniteCause.FIREBALL || event.getCause() == IgniteCause.SPREAD) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void entityExplode(final EntityExplodeEvent event) {
        if (EspaçonaveUtil.playerInArea(event.getLocation()) != null) {
            if (event.getEntityType().equals(EntityType.CREEPER)) {
                event.setCancelled(true);
            }
            if (event.getEntity() instanceof Fireball) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void fire(final ExplosionPrimeEvent event) {
        if (EspaçonaveUtil.playerInArea(event.getEntity().getLocation()) != null) {
            event.setFire(false);
        }
    }

    @EventHandler
    public void updateLoc(final PlayerMoveEvent event) {

        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (player.getWorld().getName().equalsIgnoreCase("espaconave")) {
            if (PlayerLoc.get(UUID) == null) {
                new PlayerLoc(UUID, true).insert();
            } else {
                PlayerLoc.get(UUID).setStatus(true);
            }
            if (!player.hasPotionEffect(PotionEffectType.NIGHT_VISION) && !player.hasPotionEffect(PotionEffectType.SLOW_FALLING)) {
                final PotionEffect effectN = new PotionEffect(PotionEffectType.NIGHT_VISION, 999999, 1, true, false);
                final PotionEffect effectS = new PotionEffect(PotionEffectType.SLOW_FALLING, 999999, 1, true, false);
                player.addPotionEffect(effectN);
                player.addPotionEffect(effectS);
            }
        } else if (player.hasPotionEffect(PotionEffectType.NIGHT_VISION) && player.hasPotionEffect(PotionEffectType.SLOW_FALLING)) {
            if (PlayerLoc.get(UUID) != null && PlayerLoc.get(UUID).getStatus() == true) {
                PlayerLoc.get(UUID).setStatus(false);
                player.removePotionEffect(PotionEffectType.NIGHT_VISION);
                player.removePotionEffect(PotionEffectType.SLOW_FALLING);
            }
        }

        if (EspaçonaveUtil.playerInArea(player.getLocation()) != null) {
            if (player.getWorld().getName().equalsIgnoreCase("espaconave")) {
                if (LastPositionUtil.get(UUID) == null) {
                    final String position = player.getWorld().getName() + ":" + player.getLocation().getBlockX() + ":" + player.getLocation().getBlockY() + ":" + player.getLocation().getBlockZ();
                    new LastPositionUtil(UUID, position, position).insert();
                } else {
                    final String position = player.getWorld().getName() + ":" + player.getLocation().getBlockX() + ":" + player.getLocation().getBlockY() + ":" + player.getLocation().getBlockZ();
                    if (!LastPositionUtil.get(UUID).getLastPosition().equals(position)) {
                        LastPositionUtil.get(UUID).setLastPosition2(LastPositionUtil.get(UUID).getLastPosition());
                        LastPositionUtil.get(UUID).setLastPosition(position);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onMove(final PlayerMoveEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        final String tag = TagAPI.getTag(UUID);
        if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Admin") || tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Moderador")) {
            return;
        }

        if (player.getWorld().getName().equals("espaconave")) {
            if (EspaçonaveUtil.playerInArea(player.getLocation()) != null) {
                final EspaconaveRegion espaconave = EspaçonaveUtil.playerInArea(player.getLocation());

                if (WrongLocation.get(UUID) == null) {
                    new WrongLocation(UUID, false).insert();
                } else if (WrongLocation.get(UUID).getStatus() == true) {
                    WrongLocation.get(UUID).setStatus(false);
                }

                //if (EspaçonaveAPI.getAmigos(espaconave.getRegionID()).contains(":")) {
                if (EspaçonaveInfosCache.get(espaconave.getRegionID()).getAmigos().contains(":")) {
                    //String[] amigos = EspaçonaveAPI.getAmigos(espaconave.getRegionID()).split(":");
                    final String[] amigos = EspaçonaveInfosCache.get(espaconave.getRegionID()).getAmigos().split(":");
                    if (espaconave.getRegionID().equals(UUID + "1")) {
                        event.setCancelled(false);
                        return;
                    } else {
                        if (!amigos[0].equals("NULL")) {
                            for (int i = 1; i <= amigos.length; i++) {
                                if (amigos[i - 1].equals(UUID)) {
                                    final PotionEffect effect = new PotionEffect(PotionEffectType.SLOW_FALLING, 999999, 1, true, false);
                                    player.addPotionEffect(effect);
                                    event.setCancelled(false);
                                    break;
                                }
                                if (i == amigos.length) {
                                    //if (EspaçonaveAPI.getVisitas(espaconave.getRegionID()) == false) {
                                    if (EspaçonaveInfosCache.get(espaconave.getRegionID()).getVisitas() == false) {
                                        final String[] loc = LastPositionUtil.get(UUID).getLastPosition2().split(":");
                                        final Location location = new Location(Bukkit.getWorld(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), player.getLocation().getYaw(), player.getLocation().getPitch());
                                        player.teleport(location);
                                        player.sendMessage(ChatColor.RED + " * Ops, este local não pode ser acessado D=");
                                    } else {
                                        event.setCancelled(false);
                                    }
                                }
                            }
                        } else {
                            final String[] loc = LastPositionUtil.get(UUID).getLastPosition2().split(":");
                            final Location location = new Location(Bukkit.getWorld(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), player.getLocation().getYaw(), player.getLocation().getPitch());
                            player.teleport(location);
                            player.sendMessage(ChatColor.RED + " * Ops, este local não pode ser acessado D=");
                        }
                    }
                    // } else if (!EspaçonaveAPI.getAmigos(espaconave.getRegionID()).equals("NULL")) {
                } else if (!EspaçonaveInfosCache.get(espaconave.getRegionID()).getAmigos().equals("NULL")) {
                    //if (EspaçonaveAPI.getAmigos(espaconave.getRegionID()).equals(UUID)) {
                    if (EspaçonaveInfosCache.get(espaconave.getRegionID()).getAmigos().equals(UUID)) {
                        final PotionEffect effect = new PotionEffect(PotionEffectType.SLOW_FALLING, 999999, 1, true, false);
                        player.addPotionEffect(effect);
                        event.setCancelled(false);
                        return;
                    } else {
                        //if (EspaçonaveAPI.getVisitas(espaconave.getRegionID()) == false) {
                        if (EspaçonaveInfosCache.get(espaconave.getRegionID()).getVisitas() == false) {
                            final String[] loc = LastPositionUtil.get(UUID).getLastPosition2().split(":");
                            final Location location = new Location(Bukkit.getWorld(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), player.getLocation().getYaw(), player.getLocation().getPitch());
                            player.teleport(location);
                            player.sendMessage(ChatColor.RED + " * Ops, este local não pode ser acessado D=");
                        } else {
                            event.setCancelled(false);
                        }
                    }
                } else {
                    //if (EspaçonaveAPI.getVisitas(espaconave.getRegionID()) == false) {
                    if (EspaçonaveInfosCache.get(espaconave.getRegionID()).getVisitas() == false) {
                        final String[] loc = LastPositionUtil.get(UUID).getLastPosition2().split(":");
                        final Location location = new Location(Bukkit.getWorld(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), player.getLocation().getYaw(), player.getLocation().getPitch());
                        player.teleport(location);
                        player.sendMessage(ChatColor.RED + " * Ops, este local não pode ser acessado D=");
                    } else {
                        event.setCancelled(false);
                    }
                }
            } else {
                final String[] loc = LastPositionUtil.get(UUID).getLastPosition2().split(":");
                final Location location = new Location(Bukkit.getWorld(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), player.getLocation().getYaw(), player.getLocation().getPitch());
                player.teleport(location);
                player.sendMessage(ChatColor.RED + " * Ops, este local não pode ser acessado D=");

                if (WrongLocation.get(UUID) == null) {
                    new WrongLocation(UUID, true).insert();
                } else if (WrongLocation.get(UUID).getStatus() == false) {
                    WrongLocation.get(UUID).setStatus(true);
                }

                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (EspaçonaveUtil.playerInArea(player.getLocation()) == null && WrongLocation.get(UUID).getStatus() == true) {
                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    if (EspaçonaveUtil.playerInArea(player.getLocation()) == null && WrongLocation.get(UUID).getStatus() == true) {
                                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                            @Override
                                            public void run() {
                                                if (EspaçonaveUtil.playerInArea(player.getLocation()) == null && WrongLocation.get(UUID).getStatus() == true) {
                                                    WrongLocation.get(UUID).setStatus(false);
                                                    final String[] loc = RegionsSpawnAPI.getLocation("spawn").split(":");
                                                    final Location location = new Location(Bukkit.getWorld(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]), Float.parseFloat(loc[4]), Float.parseFloat(loc[5]));
                                                    player.teleport(location);
                                                }
                                            }
                                        }, 20L);
                                    }
                                }
                            }, 20L);
                        }
                    }
                }, 20L);
            }
        }
    }

    @EventHandler
    public void onPlace(final BlockPlaceEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (LojasEspaconave.playerInArea(event.getBlockPlaced().getLocation()) != null) {
            if (event.getBlockPlaced().getType().equals(Material.SIGN) || event.getBlockPlaced().getType().equals(Material.WALL_SIGN)) {
                final Block block = event.getBlock();
                final org.bukkit.material.Sign sd = (org.bukkit.material.Sign) block.getState().getData();
                final Block attachedBlock = block.getRelative(sd.getAttachedFace());
                if (attachedBlock.getType() == Material.CHEST || attachedBlock.getType() == Material.TRAPPED_CHEST || attachedBlock.getType() == Material.ITEM_FRAME) {
                    event.setCancelled(false);
                    return;
                }
            }
        }

        if (EspaçonaveUtil.playerInArea(event.getBlock().getLocation()) != null) {
            final EspaconaveRegion espaconave = EspaçonaveUtil.playerInArea(event.getBlock().getLocation());
            if (EspaçonaveAPI.getAmigos(UUID + "1") != null) {
                if (EspaçonaveUtil.playerInArea(event.getBlock().getLocation()).getRegionID().equals(UUID + "1")) {
                    if (EspaçonaveUtil.playerInAreaSchem(event.getBlock().getLocation()) != null) {
                        event.setCancelled(false);
                    } else {
                        event.setCancelled(true);
                    }
                    return;
                }
            }

            if (EspaçonaveAPI.getAmigos(espaconave.getRegionID()).equals("NULL")) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + " * Ops, esta espaçonave contém proteção!");
                return;
            }

            if (EspaçonaveAPI.getAmigos(espaconave.getRegionID()).contains(":")) {
                final String[] amigos = EspaçonaveAPI.getAmigos(espaconave.getRegionID()).split(":");

                for (int i = 1; i <= amigos.length; i++) {
                    if (amigos[i - 1].equals(UUID)) {
                        if (EspaçonaveUtil.playerInAreaSchem(event.getBlock().getLocation()) != null) {
                            event.setCancelled(false);
                        } else {
                            event.setCancelled(true);
                        }
                        break;
                    }
                    if (i == amigos.length) {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, esta espaçonave contém proteção!");
                        final Random random = new Random();

                        if (random.nextBoolean() == true) {
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    }
                }
                return;
            } else if (!EspaçonaveAPI.getAmigos(espaconave.getRegionID()).equals("NULL")) {
                if (EspaçonaveAPI.getAmigos(espaconave.getRegionID()).equals(UUID)) {
                    if (EspaçonaveUtil.playerInAreaSchem(event.getBlock().getLocation()) != null) {
                        event.setCancelled(false);
                    } else {
                        event.setCancelled(true);
                    }
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + " * Ops, esta espaçonave contém proteção!");
                    final Random random = new Random();

                    if (random.nextBoolean() == true) {
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
                return;
            } else if (EspaçonaveAPI.getAmigos(espaconave.getRegionID()).equals("NULL")) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + " * Ops, esta espaçonave contém proteção!");
                final Random random = new Random();

                if (random.nextBoolean() == true) {
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }
        }
    }

    @EventHandler
    public void onClickArmorStand(final PlayerInteractAtEntityEvent event) {
        final Player player = event.getPlayer();
        final String UUID = player.getUniqueId().toString();

        final String tag = TagAPI.getTag(UUID);
        if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Admin") || tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Moderador")) {
            return;
        }

        if (event.getRightClicked().getType().equals(EntityType.ITEM_FRAME) || event.getRightClicked().getType().equals(EntityType.ARMOR_STAND)) {
            if (event.getRightClicked().getType().name().contains(Material.ARROW.name())) {
                if (EspaçonaveUtil.playerInArea(event.getRightClicked().getLocation()) != null) {
                    if (EspaçonaveAPI.getAmigos(UUID + "1") != null) {
                        if (EspaçonaveUtil.playerInArea(event.getRightClicked().getLocation()).getRegionID().equals(TerrainAPI.getExist(UUID + "1"))) {
                            event.setCancelled(false);
                            return;
                        }
                    }

                    if (EspaçonaveAPI.getAmigos(EspaçonaveUtil.playerInArea(event.getRightClicked().getLocation()).getRegionID()).equals("NULL")) {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, esta espaçonave contém proteção!");
                        return;
                    }

                    if (EspaçonaveAPI.getAmigos(EspaçonaveUtil.playerInArea(event.getRightClicked().getLocation()).getRegionID()).contains(":")) {
                        final String[] amigos = EspaçonaveAPI.getAmigos(EspaçonaveUtil.playerInArea(event.getRightClicked().getLocation()).getRegionID()).split(":");

                        for (int i = 1; i <= amigos.length; i++) {
                            if (amigos[i - 1].equals(UUID)) {
                                event.setCancelled(false);
                                break;
                            }
                            if (i == amigos.length) {
                                event.setCancelled(true);
                                player.sendMessage(ChatColor.RED + " * Ops, esta espaçonave contém proteção!");
                                final Random random = new Random();

                                if (random.nextBoolean() == true) {
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            }
                        }
                        return;
                    } else if (!EspaçonaveAPI.getAmigos(EspaçonaveUtil.playerInArea(event.getRightClicked().getLocation()).getRegionID()).equals("NULL")) {
                        if (EspaçonaveAPI.getAmigos(TerrainsUtil.playerInArea(event.getRightClicked().getLocation()).getRegionID()).equals(UUID)) {
                            event.setCancelled(false);
                        } else {
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.RED + " * Ops, esta espaçonave contém proteção!");
                            final Random random = new Random();

                            if (random.nextBoolean() == true) {
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        }
                        return;
                    } else if (EspaçonaveAPI.getAmigos(EspaçonaveUtil.playerInArea(event.getRightClicked().getLocation()).getRegionID()).equals("NULL")) {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, esta espaçonave contém proteção!");
                        final Random random = new Random();

                        if (random.nextBoolean() == true) {
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onInteractEntity(final EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player) {
            final Player player = (Player) event.getDamager();
            final String UUID = player.getUniqueId().toString();

            final String tag = TagAPI.getTag(UUID);
            if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Admin") || tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Moderador")) {
                return;
            }

            if (event.getEntity().getType().equals(EntityType.ITEM_FRAME) || event.getEntity().getType().equals(EntityType.ARMOR_STAND)) {
                if (event.getEntity().getType().name().contains(Material.ARROW.name())) {
                    if (EspaçonaveUtil.playerInArea(event.getEntity().getLocation()) != null) {
                        if (EspaçonaveAPI.getAmigos(UUID + "1") != null) {
                            if (EspaçonaveUtil.playerInArea(event.getEntity().getLocation()).getRegionID().equals(TerrainAPI.getExist(UUID + "1"))) {
                                event.setCancelled(false);
                                return;
                            }
                        }

                        if (EspaçonaveAPI.getAmigos(EspaçonaveUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).equals("NULL")) {
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.RED + " * Ops, esta espaçonave contém proteção!");
                            return;
                        }

                        if (EspaçonaveAPI.getAmigos(EspaçonaveUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).contains(":")) {
                            final String[] amigos = EspaçonaveAPI.getAmigos(EspaçonaveUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).split(":");

                            for (int i = 1; i <= amigos.length; i++) {
                                if (amigos[i - 1].equals(UUID)) {
                                    event.setCancelled(false);
                                    break;
                                }
                                if (i == amigos.length) {
                                    event.setCancelled(true);
                                    player.sendMessage(ChatColor.RED + " * Ops, esta espaçonave contém proteção!");
                                    final Random random = new Random();

                                    if (random.nextBoolean() == true) {
                                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                    }
                                }
                            }
                            return;
                        } else if (!EspaçonaveAPI.getAmigos(EspaçonaveUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).equals("NULL")) {
                            if (EspaçonaveAPI.getAmigos(TerrainsUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).equals(UUID)) {
                                event.setCancelled(false);
                            } else {
                                event.setCancelled(true);
                                player.sendMessage(ChatColor.RED + " * Ops, esta espaçonave contém proteção!");
                                final Random random = new Random();

                                if (random.nextBoolean() == true) {
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            }
                            return;
                        } else if (EspaçonaveAPI.getAmigos(EspaçonaveUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).equals("NULL")) {
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.RED + " * Ops, esta espaçonave contém proteção!");
                            final Random random = new Random();

                            if (random.nextBoolean() == true) {
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        } else {
                            event.setCancelled(true);
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onProjectile(final ProjectileLaunchEvent event) {
        if (event.getEntity().getShooter() instanceof Player) {
            final Player player = (Player) event.getEntity().getShooter();
            final String UUID = player.getUniqueId().toString();

            final String tag = TagAPI.getTag(UUID);
            if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Admin") || tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Moderador")) {
                return;
            }

            if (event.getEntity().getType().name().contains(Material.ARROW.name())) {
                if (EspaçonaveUtil.playerInArea(event.getEntity().getLocation()) != null) {
                    if (EspaçonaveAPI.getAmigos(UUID + "1") != null) {
                        if (EspaçonaveUtil.playerInArea(event.getEntity().getLocation()).getRegionID().equals(TerrainAPI.getExist(UUID + "1"))) {
                            event.setCancelled(false);
                            return;
                        }
                    }

                    if (EspaçonaveAPI.getAmigos(EspaçonaveUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).equals("NULL")) {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, esta espaçonave contém proteção!");
                        return;
                    }

                    if (EspaçonaveAPI.getAmigos(EspaçonaveUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).contains(":")) {
                        final String[] amigos = EspaçonaveAPI.getAmigos(EspaçonaveUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).split(":");

                        for (int i = 1; i <= amigos.length; i++) {
                            if (amigos[i - 1].equals(UUID)) {
                                event.setCancelled(false);
                                break;
                            }
                            if (i == amigos.length) {
                                event.setCancelled(true);
                                player.sendMessage(ChatColor.RED + " * Ops, esta espaçonave contém proteção!");
                                final Random random = new Random();

                                if (random.nextBoolean() == true) {
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            }
                        }
                        return;
                    } else if (!EspaçonaveAPI.getAmigos(EspaçonaveUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).equals("NULL")) {
                        if (EspaçonaveAPI.getAmigos(TerrainsUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).equals(UUID)) {
                            event.setCancelled(false);
                        } else {
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.RED + " * Ops, esta espaçonave contém proteção!");
                            final Random random = new Random();

                            if (random.nextBoolean() == true) {
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        }
                        return;
                    } else if (EspaçonaveAPI.getAmigos(EspaçonaveUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).equals("NULL")) {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, esta espaçonave contém proteção!");
                        final Random random = new Random();

                        if (random.nextBoolean() == true) {
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onInteract(final PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        final String tag = TagAPI.getTag(UUID);
        if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Admin") || tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Moderador")) {
            return;
        }

        if (EspaçonaveUtil.playerInArea(event.getPlayer().getLocation()) != null) {
            if (event.getAction().name().contains("CLICK_BLOCK") && event.getClickedBlock() != null && event.getClickedBlock().getType().name().contains("SIGN"))
                return;

            final EspaconaveRegion espaconave = EspaçonaveUtil.playerInArea(event.getPlayer().getLocation());
            if (EspaçonaveAPI.getAmigos(UUID + "1") != null) {
                if (EspaçonaveUtil.playerInArea(event.getPlayer().getLocation()).getRegionID().equals(UUID + "1")) {
                    if (EspaçonaveUtil.playerInAreaSchem(event.getPlayer().getLocation()) != null) {
                        event.setCancelled(false);
                    } else {
                        event.setCancelled(true);
                    }
                    return;
                }
            }

            if (EspaçonaveAPI.getAmigos(espaconave.getRegionID()).equals("NULL")) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + " * Ops, esta espaçonave contém proteção!");
                return;
            }

            if (EspaçonaveAPI.getAmigos(espaconave.getRegionID()).contains(":")) {
                final String[] amigos = EspaçonaveAPI.getAmigos(espaconave.getRegionID()).split(":");

                for (int i = 1; i <= amigos.length; i++) {
                    if (amigos[i - 1].equals(UUID)) {
                        if (EspaçonaveUtil.playerInAreaSchem(event.getPlayer().getLocation()) != null) {
                            event.setCancelled(false);
                        } else {
                            event.setCancelled(true);
                        }
                        break;
                    }
                    if (i == amigos.length) {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, esta espaçonave contém proteção!");
                        final Random random = new Random();

                        if (random.nextBoolean() == true) {
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    }
                }
                return;
            } else if (!EspaçonaveAPI.getAmigos(espaconave.getRegionID()).equals("NULL")) {
                if (EspaçonaveAPI.getAmigos(espaconave.getRegionID()).equals(UUID)) {
                    if (EspaçonaveUtil.playerInAreaSchem(event.getPlayer().getLocation()) != null) {
                        event.setCancelled(false);
                    } else {
                        event.setCancelled(true);
                    }
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + " * Ops, esta espaçonave contém proteção!");
                    final Random random = new Random();

                    if (random.nextBoolean() == true) {
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
                return;
            } else if (EspaçonaveAPI.getAmigos(espaconave.getRegionID()).equals("NULL")) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + " * Ops, esta espaçonave contém proteção!");
                final Random random = new Random();

                if (random.nextBoolean() == true) {
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            } else {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onBreak(final BlockBreakEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (LojasEspaconave.playerInArea(event.getBlock().getLocation()) != null) {
            if (event.getBlock().getType().equals(Material.SIGN) || event.getBlock().getType().equals(Material.WALL_SIGN)) {
                final Block block = event.getBlock();
                final org.bukkit.material.Sign sd = (org.bukkit.material.Sign) block.getState().getData();
                final Block attachedBlock = block.getRelative(sd.getAttachedFace());
                if (attachedBlock.getType() == Material.CHEST || attachedBlock.getType() == Material.TRAPPED_CHEST || attachedBlock.getType() == Material.ITEM_FRAME) {
                    event.setCancelled(false);
                    return;
                }
            }
        }

        if (EspaçonaveUtil.playerInArea(event.getBlock().getLocation()) != null) {
            final EspaconaveRegion espaconave = EspaçonaveUtil.playerInArea(event.getBlock().getLocation());
            if (EspaçonaveAPI.getAmigos(UUID + "1") != null) {
                if (EspaçonaveUtil.playerInArea(event.getBlock().getLocation()).getRegionID().equals(UUID + "1")) {
                    if (EspaçonaveUtil.playerInAreaSchem(event.getBlock().getLocation()) != null) {
                        event.setCancelled(false);
                    } else {
                        event.setCancelled(true);
                    }
                    return;
                }
            }

            if (EspaçonaveAPI.getAmigos(espaconave.getRegionID()).equals("NULL")) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + " * Ops, esta espaçonave contém proteção!");
                return;
            }

            if (EspaçonaveAPI.getAmigos(espaconave.getRegionID()).contains(":")) {
                final String[] amigos = EspaçonaveAPI.getAmigos(espaconave.getRegionID()).split(":");

                for (int i = 1; i <= amigos.length; i++) {
                    if (amigos[i - 1].equals(UUID)) {
                        if (EspaçonaveUtil.playerInAreaSchem(event.getBlock().getLocation()) != null) {
                            event.setCancelled(false);
                        } else {
                            event.setCancelled(true);
                        }
                        break;
                    }
                    if (i == amigos.length) {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, esta espaçonave contém proteção!");
                        final Random random = new Random();

                        if (random.nextBoolean() == true) {
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    }
                }
                return;
            } else if (!EspaçonaveAPI.getAmigos(espaconave.getRegionID()).equals("NULL")) {
                if (EspaçonaveAPI.getAmigos(espaconave.getRegionID()).equals(UUID)) {
                    if (EspaçonaveUtil.playerInAreaSchem(event.getBlock().getLocation()) != null) {
                        event.setCancelled(false);
                    } else {
                        event.setCancelled(true);
                    }
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + " * Ops, esta espaçonave contém proteção!");
                    final Random random = new Random();

                    if (random.nextBoolean() == true) {
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
                return;
            } else if (EspaçonaveAPI.getAmigos(espaconave.getRegionID()).equals("NULL")) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + " * Ops, esta espaçonave contém proteção!");
                final Random random = new Random();

                if (random.nextBoolean() == true) {
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }
        }
    }

    @EventHandler
    public void onHited(final EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
            final Player player = (Player) event.getDamager();
            final Player target = (Player) event.getEntity();

            if (EspaçonaveUtil.playerInArea(player.getLocation()) != null) {
                final EspaconaveRegion espaçonave = EspaçonaveUtil.playerInArea(player.getLocation());

                if (EspaçonaveAPI.getPvPStatus(espaçonave.getRegionID()) == false) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + " * Ops, o pvp está desabilitado dentro desta região!");
                }
            }
            if (EspaçonaveUtil.playerInArea(target.getLocation()) != null) {
                final EspaconaveRegion espaçonave = EspaçonaveUtil.playerInArea(player.getLocation());

                if (EspaçonaveAPI.getPvPStatus(espaçonave.getRegionID()) == false) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + " * Ops, o pvp está desabilitado dentro desta região!");
                }
            }
        }
    }

    @EventHandler
    public void onChangeWorld(final WeatherChangeEvent event) {
        if (event.getWorld().getName().equals("espaconave")) {
            if (event.getWorld().hasStorm() || event.getWorld().isThundering()) {
                event.getWorld().setStorm(false);
                event.getWorld().setThundering(false);
            }
            event.setCancelled(true);
        }
    }
}
