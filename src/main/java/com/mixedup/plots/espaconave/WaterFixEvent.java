package com.mixedup.plots.espaconave;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockSpreadEvent;

public class WaterFixEvent implements Listener {

    @EventHandler
    public void stopLiquids(final BlockFromToEvent event) {
        if (event.getBlock().getLocation().getWorld().getName().equalsIgnoreCase("espaconave")) {
            if (EspaçonaveUtil.playerInAreaSchem(event.getToBlock().getLocation()) == null) {
                if (event.getBlock().isLiquid()) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onBlockSpread(final BlockSpreadEvent event) {
        if (event.getBlock().getLocation().getWorld().getName().equalsIgnoreCase("espaconave")) {
            if (EspaçonaveUtil.playerInAreaSchem(event.getSource().getLocation()) == null) {
                if (event.getBlock().getType().name().contains("_CARPET")) {
                    event.setCancelled(true);
                }
            }
        }
    }
}
