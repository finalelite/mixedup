package com.mixedup.plots.espaconave;

import com.mixedup.apis.PainelSolarAPI;
import com.mixedup.plots.utils.BackupList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class InventoryConfigsEspaçonave {

    private static String listItems;
    private static int slot = 1;
    private static int quantia;

    public static Inventory getMobspawners(final String UUID, final boolean changedPage, final String listItens) {
        final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Remover spawners: ");

        final ItemStack sair = new ItemStack(Material.BARRIER);
        final ItemMeta metaSair = sair.getItemMeta();
        metaSair.setDisplayName(ChatColor.RED + "Sair");
        sair.setItemMeta(metaSair);

        inventory.setItem(49, sair);

        if (changedPage == true) {
            if (listItens.contains(":")) {
                final String[] list = listItens.split(":");
                for (int i = 1; i <= list.length; i++) {
                    final String[] item = list[i - 1].split("-");
                    final int quantia = Integer.valueOf(item[1]);

                    final ItemStack voltar = new ItemStack(Material.ARROW);
                    final ItemMeta metaVoltar = voltar.getItemMeta();
                    metaVoltar.setDisplayName(ChatColor.RED + "Voltar página");
                    voltar.setItemMeta(metaVoltar);

                    inventory.setItem(45, voltar);

                    if (InventoryConfigsEspaçonave.slot <= 7) {
                        final ItemStack spawner = new ItemStack(Material.SPAWNER, quantia);
                        final ItemMeta metaSpawn = spawner.getItemMeta();
                        metaSpawn.setDisplayName(ChatColor.RED + item[0]);
                        final List<String> lore = new ArrayList<String>();
                        lore.add(" ");
                        lore.add(ChatColor.GREEN + "Quantidade: " + ChatColor.RED + quantia);
                        lore.add(" ");
                        metaSpawn.setLore(lore);
                        spawner.setItemMeta(metaSpawn);

                        inventory.setItem(9 + InventoryConfigsEspaçonave.slot, spawner);
                        InventoryConfigsEspaçonave.slot++;
                    } else if (InventoryConfigsEspaçonave.slot <= 14) {
                        final ItemStack spawner = new ItemStack(Material.SPAWNER, quantia);
                        final ItemMeta metaSpawn = spawner.getItemMeta();
                        metaSpawn.setDisplayName(ChatColor.RED + item[0]);
                        final List<String> lore = new ArrayList<String>();
                        lore.add(" ");
                        lore.add(ChatColor.GREEN + "Quantidade: " + ChatColor.RED + quantia);
                        lore.add(" ");
                        metaSpawn.setLore(lore);
                        spawner.setItemMeta(metaSpawn);

                        inventory.setItem(11 + InventoryConfigsEspaçonave.slot, spawner);
                        InventoryConfigsEspaçonave.slot++;
                    } else if (InventoryConfigsEspaçonave.slot <= 21) {
                        final ItemStack spawner = new ItemStack(Material.SPAWNER, quantia);
                        final ItemMeta metaSpawn = spawner.getItemMeta();
                        metaSpawn.setDisplayName(ChatColor.RED + item[0]);
                        final List<String> lore = new ArrayList<String>();
                        lore.add(" ");
                        lore.add(ChatColor.GREEN + "Quantidade: " + ChatColor.RED + quantia);
                        lore.add(" ");
                        metaSpawn.setLore(lore);
                        spawner.setItemMeta(metaSpawn);

                        inventory.setItem(13 + InventoryConfigsEspaçonave.slot, spawner);
                        InventoryConfigsEspaçonave.slot++;
                    } else if (InventoryConfigsEspaçonave.slot <= 28) {
                        final ItemStack spawner = new ItemStack(Material.SPAWNER, quantia);
                        final ItemMeta metaSpawn = spawner.getItemMeta();
                        metaSpawn.setDisplayName(ChatColor.RED + item[0]);
                        final List<String> lore = new ArrayList<String>();
                        lore.add(" ");
                        lore.add(ChatColor.GREEN + "Quantidade: " + ChatColor.RED + quantia);
                        lore.add(" ");
                        metaSpawn.setLore(lore);
                        spawner.setItemMeta(metaSpawn);

                        inventory.setItem(15 + InventoryConfigsEspaçonave.slot, spawner);
                        InventoryConfigsEspaçonave.slot++;

                        if (15 + InventoryConfigsEspaçonave.slot == 43 && list.length > i) {
                            final ItemStack proximo = new ItemStack(Material.ARROW);
                            final ItemMeta metaProx = proximo.getItemMeta();
                            metaProx.setDisplayName(ChatColor.RED + "Próxima página");
                            proximo.setItemMeta(metaProx);

                            inventory.setItem(53, proximo);

                            for (int y = 29; y <= list.length; y++) {
                                if (InventoryConfigsEspaçonave.listItems == null) {
                                    InventoryConfigsEspaçonave.listItems = list[y - 1];
                                } else {
                                    InventoryConfigsEspaçonave.listItems = InventoryConfigsEspaçonave.listItems + ":" + list[y - 1];
                                }

                                if (y == list.length) {
                                    if (BackupList.get(UUID) == null) {
                                        new BackupList(UUID, InventoryConfigsEspaçonave.listItems).insert();
                                    } else {
                                        BackupList.get(UUID).setLists(InventoryConfigsEspaçonave.listItems);
                                    }
                                    InventoryConfigsEspaçonave.listItems = null;
                                }
                            }
                        }
                    }
                }
                InventoryConfigsEspaçonave.slot = 1;
            } else {
                final String[] item = listItens.split("-");
                final ItemStack spawner = new ItemStack(Material.SPAWNER, Integer.valueOf(item[1]));
                final ItemMeta metaSpawn = spawner.getItemMeta();
                metaSpawn.setDisplayName(ChatColor.RED + item[0]);
                spawner.setItemMeta(metaSpawn);

                inventory.setItem(10, spawner);
            }
        } else {

            if (!MobspawnAPI.getSpawners(UUID + "1").equals("NULL")) {
                if (MobspawnAPI.getSpawners(UUID + "1").contains(":")) {
                    final String[] list = MobspawnAPI.getSpawners(UUID + "1").split(":");
                    for (int i = 1; i <= list.length; i++) {
                        final String[] item = list[i - 1].split("-");
                        final int quantia = Integer.valueOf(item[1]);

                        if (InventoryConfigsEspaçonave.slot <= 7) {
                            final ItemStack spawner = new ItemStack(Material.SPAWNER, quantia);
                            final ItemMeta metaSpawn = spawner.getItemMeta();
                            metaSpawn.setDisplayName(ChatColor.RED + item[0]);
                            final List<String> lore = new ArrayList<String>();
                            lore.add(" ");
                            lore.add(ChatColor.GREEN + "Quantidade: " + ChatColor.RED + quantia);
                            lore.add(" ");
                            metaSpawn.setLore(lore);
                            spawner.setItemMeta(metaSpawn);

                            inventory.setItem(9 + InventoryConfigsEspaçonave.slot, spawner);
                            InventoryConfigsEspaçonave.slot++;
                        } else if (InventoryConfigsEspaçonave.slot <= 14) {
                            final ItemStack spawner = new ItemStack(Material.SPAWNER, quantia);
                            final ItemMeta metaSpawn = spawner.getItemMeta();
                            metaSpawn.setDisplayName(ChatColor.RED + item[0]);
                            final List<String> lore = new ArrayList<String>();
                            lore.add(" ");
                            lore.add(ChatColor.GREEN + "Quantidade: " + ChatColor.RED + quantia);
                            lore.add(" ");
                            metaSpawn.setLore(lore);
                            spawner.setItemMeta(metaSpawn);

                            inventory.setItem(11 + InventoryConfigsEspaçonave.slot, spawner);
                            InventoryConfigsEspaçonave.slot++;
                        } else if (InventoryConfigsEspaçonave.slot <= 21) {
                            final ItemStack spawner = new ItemStack(Material.SPAWNER, quantia);
                            final ItemMeta metaSpawn = spawner.getItemMeta();
                            metaSpawn.setDisplayName(ChatColor.RED + item[0]);
                            final List<String> lore = new ArrayList<String>();
                            lore.add(" ");
                            lore.add(ChatColor.GREEN + "Quantidade: " + ChatColor.RED + quantia);
                            lore.add(" ");
                            metaSpawn.setLore(lore);
                            spawner.setItemMeta(metaSpawn);

                            inventory.setItem(13 + InventoryConfigsEspaçonave.slot, spawner);
                            InventoryConfigsEspaçonave.slot++;
                        } else if (InventoryConfigsEspaçonave.slot <= 28) {
                            final ItemStack spawner = new ItemStack(Material.SPAWNER, quantia);
                            final ItemMeta metaSpawn = spawner.getItemMeta();
                            metaSpawn.setDisplayName(ChatColor.RED + item[0]);
                            final List<String> lore = new ArrayList<String>();
                            lore.add(" ");
                            lore.add(ChatColor.GREEN + "Quantidade: " + ChatColor.RED + quantia);
                            lore.add(" ");
                            metaSpawn.setLore(lore);
                            spawner.setItemMeta(metaSpawn);

                            inventory.setItem(15 + InventoryConfigsEspaçonave.slot, spawner);
                            InventoryConfigsEspaçonave.slot++;

                            if (15 + InventoryConfigsEspaçonave.slot == 43 && list.length > i) {
                                final ItemStack proximo = new ItemStack(Material.ARROW);
                                final ItemMeta metaProx = proximo.getItemMeta();
                                metaProx.setDisplayName(ChatColor.RED + "Próxima página");
                                proximo.setItemMeta(metaProx);

                                inventory.setItem(53, proximo);

                                for (int y = 29; y <= list.length; y++) {
                                    if (InventoryConfigsEspaçonave.listItems == null) {
                                        InventoryConfigsEspaçonave.listItems = list[y - 1];
                                    } else {
                                        InventoryConfigsEspaçonave.listItems = InventoryConfigsEspaçonave.listItems + ":" + list[y - 1];
                                    }

                                    if (y == list.length) {
                                        if (BackupList.get(UUID) == null) {
                                            new BackupList(UUID, InventoryConfigsEspaçonave.listItems).insert();
                                        } else {
                                            BackupList.get(UUID).setLists(InventoryConfigsEspaçonave.listItems);
                                        }
                                        InventoryConfigsEspaçonave.listItems = null;
                                    }
                                }
                            }
                        }
                    }
                    InventoryConfigsEspaçonave.slot = 1;
                } else {
                    final String[] item = MobspawnAPI.getSpawners(UUID + "1").split("-");
                    final ItemStack spawner = new ItemStack(Material.SPAWNER, Integer.valueOf(item[1]));
                    final ItemMeta metaSpawn = spawner.getItemMeta();
                    metaSpawn.setDisplayName(ChatColor.RED + item[0]);
                    spawner.setItemMeta(metaSpawn);

                    inventory.setItem(10, spawner);
                }
            }
        }
        return inventory;
    }

    public static Inventory getPaineisReturn(final String UUID) {
        final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Remover painéis: ");

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta meta = voltar.getItemMeta();
        meta.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(meta);
        inventory.setItem(45, voltar);

        if (PainelSolarAPI.getLastPainel(UUID) >= 1) {
            final ItemStack painel1 = new ItemStack(Material.DAYLIGHT_DETECTOR);
            final ItemMeta meta1 = painel1.getItemMeta();
            meta1.setDisplayName(ChatColor.YELLOW + "Painel solar");
            final List<String> lore1 = new ArrayList<>();
            lore1.add(" ");
            lore1.add(ChatColor.DARK_GRAY + "Modelo: " + ChatColor.RED + "100Wp");
            lore1.add(" ");
            meta1.setLore(lore1);
            painel1.setItemMeta(meta1);

            final ItemStack painel2 = new ItemStack(Material.DAYLIGHT_DETECTOR);
            final ItemMeta meta2 = painel2.getItemMeta();
            meta2.setDisplayName(ChatColor.YELLOW + "Painel solar");
            final List<String> lore2 = new ArrayList<>();
            lore2.add(" ");
            lore2.add(ChatColor.DARK_GRAY + "Modelo: " + ChatColor.RED + "200Wp");
            lore2.add(" ");
            meta2.setLore(lore2);
            painel2.setItemMeta(meta2);

            if (PainelSolarAPI.getLastPainel(UUID) > 0) {
                for (int i = 1; i <= 7; i++) {
                    if (PainelSolarAPI.getForcaFromUUID(UUID + i) != null) {
                        if (PainelSolarAPI.getForcaFromUUID(UUID + i).equals("100")) {
                            inventory.setItem(9 + i, painel1);
                        } else if (PainelSolarAPI.getForcaFromUUID(UUID + i).equals("200")) {
                            inventory.setItem(9 + i, painel2);
                        }
                    } else {
                        break;
                    }
                }
            }
            if (PainelSolarAPI.getLastPainel(UUID) > 7) {
                for (int i = 1; i <= 7; i++) {
                    if (PainelSolarAPI.getForcaFromUUID(UUID + (i + 7)) != null) {
                        if (PainelSolarAPI.getForcaFromUUID(UUID + (i + 7)).equals("100")) {
                            inventory.setItem(18 + i, painel1);
                        } else if (PainelSolarAPI.getForcaFromUUID(UUID + (i + 7)).equals("200")) {
                            inventory.setItem(18 + i, painel2);
                        }
                    } else {
                        break;
                    }
                }
            }
            if (PainelSolarAPI.getLastPainel(UUID) > 14) {
                for (int i = 1; i <= 7; i++) {
                    if (PainelSolarAPI.getForcaFromUUID(UUID + (i + 14)) != null) {
                        if (PainelSolarAPI.getForcaFromUUID(UUID + (i + 14)).equals("100")) {
                            inventory.setItem(27 + i, painel1);
                        } else if (PainelSolarAPI.getForcaFromUUID(UUID + (i + 14)).equals("200")) {
                            inventory.setItem(27 + i, painel2);
                        }
                    } else {
                        break;
                    }
                }
            }
            if (PainelSolarAPI.getLastPainel(UUID) > 20) {
                for (int i = 1; i <= 7; i++) {
                    if (PainelSolarAPI.getForcaFromUUID(UUID + (i + 20)) != null) {
                        if (PainelSolarAPI.getForcaFromUUID(UUID + (i + 20)).equals("100")) {
                            inventory.setItem(36 + i, painel1);
                        } else if (PainelSolarAPI.getForcaFromUUID(UUID + (i + 20)).equals("200")) {
                            inventory.setItem(36 + i, painel2);
                        }
                    } else {
                        break;
                    }
                }
            }
        }

        return inventory;
    }

    public static Inventory getInvPaineis(final String UUID) {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Configuração painéis solares: ");

        final ItemStack adcionar = new ItemStack(Material.CHEST);
        final ItemMeta metaAdd = adcionar.getItemMeta();
        metaAdd.setDisplayName(ChatColor.YELLOW + "Adicionar painel");
        final ArrayList<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.DARK_GRAY + "Para adicionar um painel solar arraste-o");
        lore1.add(ChatColor.DARK_GRAY + "para cá e clique com botão direito.");
        lore1.add(" ");
        metaAdd.setLore(lore1);
        adcionar.setItemMeta(metaAdd);

        final ItemStack remover = new ItemStack(Material.BARRIER);
        final ItemMeta metaRemover = remover.getItemMeta();
        metaRemover.setDisplayName(ChatColor.YELLOW + "Remover painéis");
        remover.setItemMeta(metaRemover);

        final ItemStack painel = new ItemStack(Material.DAYLIGHT_DETECTOR);
        final ItemMeta metaPainel = painel.getItemMeta();
        metaPainel.setDisplayName(ChatColor.GREEN + "Painéis solares");
        final ArrayList<String> lore2 = new ArrayList<>();
        lore2.add(" ");
        final int quantia = PainelSolarAPI.getLastPainel(UUID);
        int painel1 = 0;
        int painel2 = 0;
        for (int i = 1; i <= quantia; i++) {
            if (PainelSolarAPI.getForcaFromUUID(UUID + i) == null) {
                break;
            }
            if (PainelSolarAPI.getForcaFromUUID(UUID + i).equals("100")) {
                painel1 += 1;
                continue;
            }
            if (PainelSolarAPI.getForcaFromUUID(UUID + i).equals("200")) {
                painel2 += 1;
                continue;
            }
        }
        lore2.add(ChatColor.YELLOW + "Painéis 100Wp: " + ChatColor.RED + painel1);
        lore2.add(ChatColor.YELLOW + "Painéis 200Wp: " + ChatColor.RED + painel2);
        lore2.add(" ");
        metaPainel.setLore(lore2);
        painel.setItemMeta(metaPainel);

        inventory.setItem(10, remover);
        inventory.setItem(13, painel);
        inventory.setItem(16, adcionar);

        return inventory;
    }

    public static Inventory getInvMobspawn(final String UUID) {
        final Inventory inventory = Bukkit.createInventory(null, 6 * 9, "Configuração mobspawner: ");

        final String[] mobs = {"Vaca", "Galinha", "Porco", "Ovelha", "Coelho", "Zumbi", "Esqueleto", "Aranha", "Creeper", "Enderman", "Aranha das cavernas", "Slime", "Blaze", "Homem porco zumbi", "Esqueleto wither", "Bruxa", "Cubo magma", "Aldeão zumbi", "Ghast"};

        for (int i = 1; i <= mobs.length; i++) {

            final int ID = Integer.valueOf(UUID.substring(UUID.length() - 1), UUID.length());
            final String playerUUID = UUID.substring(0, UUID.length() - 1);

            final ItemStack mobspawner = new ItemStack(Material.EGG);
            final ItemMeta metaMobs = mobspawner.getItemMeta();
            if (mobs[i - 1].equals("Vaca")) {
                mobspawner.setType(Material.COW_SPAWN_EGG);
            }
            if (mobs[i - 1].equals("Galinha")) {
                mobspawner.setType(Material.CHICKEN_SPAWN_EGG);
            }
            if (mobs[i - 1].equals("Porco")) {
                mobspawner.setType(Material.PIG_SPAWN_EGG);
            }
            if (mobs[i - 1].equals("Ovelha")) {
                mobspawner.setType(Material.SHEEP_SPAWN_EGG);
            }
            if (mobs[i - 1].equals("Coelho")) {
                mobspawner.setType(Material.RABBIT_SPAWN_EGG);
            }
            if (mobs[i - 1].equals("Zumbi")) {
                mobspawner.setType(Material.ZOMBIE_SPAWN_EGG);
            }
            if (mobs[i - 1].equals("Esqueleto")) {
                mobspawner.setType(Material.SKELETON_SPAWN_EGG);
            }
            if (mobs[i - 1].equals("Aranha")) {
                mobspawner.setType(Material.SPIDER_SPAWN_EGG);
            }
            if (mobs[i - 1].equals("Creeper")) {
                mobspawner.setType(Material.CREEPER_SPAWN_EGG);
            }
            if (mobs[i - 1].equals("Enderman")) {
                mobspawner.setType(Material.ENDERMAN_SPAWN_EGG);
            }
            if (mobs[i - 1].equals("Aranha das cavernas")) {
                mobspawner.setType(Material.CAVE_SPIDER_SPAWN_EGG);
            }
            if (mobs[i - 1].equals("Slime")) {
                mobspawner.setType(Material.SLIME_SPAWN_EGG);
            }
            if (mobs[i - 1].equals("Blaze")) {
                mobspawner.setType(Material.BLAZE_SPAWN_EGG);
            }
            if (mobs[i - 1].equals("Homem porco zumbi")) {
                mobspawner.setType(Material.PIG_SPAWN_EGG);
            }
            if (mobs[i - 1].equals("Esqueleto wither")) {
                mobspawner.setType(Material.WITHER_SKELETON_SPAWN_EGG);
            }
            if (mobs[i - 1].equals("Bruxa")) {
                mobspawner.setType(Material.WITCH_SPAWN_EGG);
            }
            if (mobs[i - 1].equals("Cubo magma")) {
                mobspawner.setType(Material.MAGMA_CUBE_SPAWN_EGG);
            }
            if (mobs[i - 1].equals("Aldeão zumbi")) {
                mobspawner.setType(Material.ZOMBIE_VILLAGER_SPAWN_EGG);
            }
            if (mobs[i - 1].equals("Ghast")) {
                mobspawner.setType(Material.GHAST_SPAWN_EGG);
            }

            if (ID == 1) {
                if (!MobspawnAPI.getSpawnAtivo(playerUUID + "2").equals("NULL")) {
                    final String[] ativo2 = MobspawnAPI.getSpawnAtivo(playerUUID + "2").split("-");
                    if (ativo2[0].equals(mobs[i - 1]) && MobspawnAPI.getStatus(playerUUID + "2") == true) {
                        metaMobs.setDisplayName(ChatColor.YELLOW + "Spawner de " + mobs[i - 1] + ChatColor.GOLD + " !");
                    } else {
                        metaMobs.setDisplayName(ChatColor.YELLOW + "Spawner de " + mobs[i - 1]);
                    }
                } else if (!MobspawnAPI.getSpawnAtivo(playerUUID + "3").equals("NULL")) {
                    final String[] ativo2 = MobspawnAPI.getSpawnAtivo(playerUUID + "3").split("-");
                    if (ativo2[0].equals(mobs[i - 1]) && MobspawnAPI.getStatus(playerUUID + "3") == true) {
                        metaMobs.setDisplayName(ChatColor.YELLOW + "Spawner de " + mobs[i - 1] + ChatColor.GOLD + " !");
                    } else {
                        metaMobs.setDisplayName(ChatColor.YELLOW + "Spawner de " + mobs[i - 1]);
                    }
                } else {
                    metaMobs.setDisplayName(ChatColor.YELLOW + "Spawner de " + mobs[i - 1]);
                }
            } else if (ID == 2) {
                if (!MobspawnAPI.getSpawnAtivo(playerUUID + "1").equals("NULL")) {
                    final String[] ativo2 = MobspawnAPI.getSpawnAtivo(playerUUID + "1").split("-");
                    if (ativo2[0].equals(mobs[i - 1]) && MobspawnAPI.getStatus(playerUUID + "1") == true) {
                        metaMobs.setDisplayName(ChatColor.YELLOW + "Spawner de " + mobs[i - 1] + ChatColor.GOLD + " !");
                    } else {
                        metaMobs.setDisplayName(ChatColor.YELLOW + "Spawner de " + mobs[i - 1]);
                    }
                } else if (!MobspawnAPI.getSpawnAtivo(playerUUID + "3").equals("NULL")) {
                    final String[] ativo2 = MobspawnAPI.getSpawnAtivo(playerUUID + "3").split("-");
                    if (ativo2[0].equals(mobs[i - 1]) && MobspawnAPI.getStatus(playerUUID + "3") == true) {
                        metaMobs.setDisplayName(ChatColor.YELLOW + "Spawner de " + mobs[i - 1] + ChatColor.GOLD + " !");
                    } else {
                        metaMobs.setDisplayName(ChatColor.YELLOW + "Spawner de " + mobs[i - 1]);
                    }
                } else {
                    metaMobs.setDisplayName(ChatColor.YELLOW + "Spawner de " + mobs[i - 1]);
                }
            } else if (ID == 3) {
                if (!MobspawnAPI.getSpawnAtivo(playerUUID + "2").equals("NULL")) {
                    final String[] ativo2 = MobspawnAPI.getSpawnAtivo(playerUUID + "2").split("-");
                    if (ativo2[0].equals(mobs[i - 1]) && MobspawnAPI.getStatus(playerUUID + "2") == true) {
                        metaMobs.setDisplayName(ChatColor.YELLOW + "Spawner de " + mobs[i - 1] + ChatColor.GOLD + " !");
                    } else {
                        metaMobs.setDisplayName(ChatColor.YELLOW + "Spawner de " + mobs[i - 1]);
                    }
                } else if (!MobspawnAPI.getSpawnAtivo(playerUUID + "1").equals("NULL")) {
                    final String[] ativo2 = MobspawnAPI.getSpawnAtivo(playerUUID + "1").split("-");
                    if (ativo2[0].equals(mobs[i - 1]) && MobspawnAPI.getStatus(playerUUID + "1") == true) {
                        metaMobs.setDisplayName(ChatColor.YELLOW + "Spawner de " + mobs[i - 1] + ChatColor.GOLD + " !");
                    } else {
                        metaMobs.setDisplayName(ChatColor.YELLOW + "Spawner de " + mobs[i - 1]);
                    }
                } else {
                    metaMobs.setDisplayName(ChatColor.YELLOW + "Spawner de " + mobs[i - 1]);
                }
            }

            final ArrayList<String> lore = new ArrayList<String>();
            lore.add(" ");
            if (!MobspawnAPI.getSpawners(playerUUID + "1").equals("NULL")) {
                if (MobspawnAPI.getSpawners(playerUUID + "1").contains(":")) {
                    final String[] list = MobspawnAPI.getSpawners(playerUUID + "1").split(":");
                    for (int x = 1; x <= list.length; x++) {
                        final String[] item = list[x - 1].split("-");

                        if (item[0].contains(mobs[i - 1])) {
                            if (InventoryConfigsEspaçonave.quantia != 0) {
                                InventoryConfigsEspaçonave.quantia = InventoryConfigsEspaçonave.quantia + Integer.valueOf(item[1]);
                            } else {
                                InventoryConfigsEspaçonave.quantia = Integer.valueOf(item[1]);
                            }
                        }

                        if (x == list.length) {
                            if (InventoryConfigsEspaçonave.quantia != 0) {
                                lore.add(ChatColor.GREEN + "Quantidade: " + ChatColor.GRAY + InventoryConfigsEspaçonave.quantia);
                                InventoryConfigsEspaçonave.quantia = 0;
                            } else {
                                lore.add(ChatColor.GREEN + "Quantidade: " + ChatColor.RED + "0");
                            }
                        }
                    }
                } else {
                    final String[] item = MobspawnAPI.getSpawners(playerUUID + "1").split("-");
                    if (item[0].contains(mobs[i - 1])) {
                        lore.add(ChatColor.GREEN + "Quantidade: " + ChatColor.RED + item[1]);
                    } else {
                        lore.add(ChatColor.GREEN + "Quantidade: " + ChatColor.RED + "0");
                    }
                }
            } else {
                lore.add(ChatColor.GREEN + "Quantidade: " + ChatColor.RED + "0");
            }
            lore.add(" ");
            if (!MobspawnAPI.getSpawnAtivo(UUID).equals("NULL")) {
                final String[] ativo = MobspawnAPI.getSpawnAtivo(UUID).split("-");
                if (ativo[0].equals(mobs[i - 1])) {
                    lore.add(ChatColor.RED + "ATIVADO!");
                    lore.add(" ");
                }
            }

            if (ID == 1) {
                if (!MobspawnAPI.getSpawnAtivo(playerUUID + "2").equals("NULL")) {
                    final String[] ativo2 = MobspawnAPI.getSpawnAtivo(playerUUID + "2").split("-");
                    if (ativo2[0].equals(mobs[i - 1]) && MobspawnAPI.getStatus(playerUUID + "2") == true) {
                        lore.add(ChatColor.RED + "ATIVADO NA FARM-2!");
                        lore.add(ChatColor.RED + "INUTILIZAVEL.");
                        lore.add(" ");
                    }
                }
                if (!MobspawnAPI.getSpawnAtivo(playerUUID + "3").equals("NULL")) {
                    final String[] ativo2 = MobspawnAPI.getSpawnAtivo(playerUUID + "3").split("-");
                    if (ativo2[0].equals(mobs[i - 1]) && MobspawnAPI.getStatus(playerUUID + "3") == true) {
                        lore.add(ChatColor.RED + "ATIVADO NA FARM-3!");
                        lore.add(ChatColor.RED + "INUTILIZAVEL.");
                        lore.add(" ");
                    }
                }
            } else if (ID == 2) {
                if (!MobspawnAPI.getSpawnAtivo(playerUUID + "1").equals("NULL")) {
                    final String[] ativo2 = MobspawnAPI.getSpawnAtivo(playerUUID + "1").split("-");
                    if (ativo2[0].equals(mobs[i - 1]) && MobspawnAPI.getStatus(playerUUID + "1") == true) {
                        lore.add(ChatColor.RED + "ATIVADO NA FARM-1!");
                        lore.add(ChatColor.RED + "INUTILIZAVEL.");
                        lore.add(" ");
                    }
                }
                if (!MobspawnAPI.getSpawnAtivo(playerUUID + "3").equals("NULL")) {
                    final String[] ativo2 = MobspawnAPI.getSpawnAtivo(playerUUID + "3").split("-");
                    if (ativo2[0].equals(mobs[i - 1]) && MobspawnAPI.getStatus(playerUUID + "3") == true) {
                        lore.add(ChatColor.RED + "ATIVADO NA FARM-3!");
                        lore.add(ChatColor.RED + "INUTILIZAVEL.");
                        lore.add(" ");
                    }
                }
            } else if (ID == 3) {
                if (!MobspawnAPI.getSpawnAtivo(playerUUID + "2").equals("NULL")) {
                    final String[] ativo2 = MobspawnAPI.getSpawnAtivo(playerUUID + "2").split("-");
                    if (ativo2[0].equals(mobs[i - 1]) && MobspawnAPI.getStatus(playerUUID + "2") == true) {
                        lore.add(ChatColor.RED + "ATIVADO NA FARM-2!");
                        lore.add(ChatColor.RED + "INUTILIZAVEL.");
                        lore.add(" ");
                    }
                }
                if (!MobspawnAPI.getSpawnAtivo(playerUUID + "1").equals("NULL")) {
                    final String[] ativo2 = MobspawnAPI.getSpawnAtivo(playerUUID + "1").split("-");
                    if (ativo2[0].equals(mobs[i - 1]) && MobspawnAPI.getStatus(playerUUID + "1") == true) {
                        lore.add(ChatColor.RED + "ATIVADO NA FARM-1!");
                        lore.add(ChatColor.RED + "INUTILIZAVEL.");
                        lore.add(" ");
                    }
                }
            }

            metaMobs.setLore(lore);
            mobspawner.setItemMeta(metaMobs);

            if (i <= 7) {
                inventory.setItem(9 + i, mobspawner);
            } else if (i <= 14) {
                inventory.setItem(11 + i, mobspawner);
            } else if (i <= 21) {
                inventory.setItem(13 + i, mobspawner);
            }
        }

        final ItemStack remover = new ItemStack(Material.BARRIER);
        final ItemMeta metaRemover = remover.getItemMeta();
        metaRemover.setDisplayName(ChatColor.YELLOW + "Remover spawners");
        remover.setItemMeta(metaRemover);

        inventory.setItem(18, remover);

        final ItemStack adcionar = new ItemStack(Material.CHEST);
        final ItemMeta metaAdd = adcionar.getItemMeta();
        metaAdd.setDisplayName(ChatColor.YELLOW + "Adicionar spawner");
        final ArrayList<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.DARK_GRAY + "Para adicionar um spawner arraste-o");
        lore1.add(ChatColor.DARK_GRAY + "para cá e clique com botão direito.");
        lore1.add(" ");
        metaAdd.setLore(lore1);
        adcionar.setItemMeta(metaAdd);

        inventory.setItem(26, adcionar);

        final ItemStack ligar = new ItemStack(Material.LIME_WOOL);
        final ItemMeta metaLigar = ligar.getItemMeta();
        metaLigar.setDisplayName(ChatColor.GREEN + "ATIVAR MOBSPAWNER");
        ligar.setItemMeta(metaLigar);

        inventory.setItem(38, ligar);

        final ItemStack desligar = new ItemStack(Material.RED_WOOL);
        final ItemMeta metaDesligar = desligar.getItemMeta();
        metaDesligar.setDisplayName(ChatColor.RED + "DESATIVAR MOBSPAWNER");
        desligar.setItemMeta(metaDesligar);

        inventory.setItem(42, desligar);

        final ItemStack ativoNow = new ItemStack(Material.SPAWNER);
        final ItemMeta metaAtivoNow = ativoNow.getItemMeta();
        if (MobspawnAPI.getSpawnAtivo(UUID).equals("NULL")) {
            metaAtivoNow.setDisplayName(ChatColor.RED + "NENHUM MOB ATIVO");
        } else {
            metaAtivoNow.setDisplayName(ChatColor.YELLOW + MobspawnAPI.getSpawnAtivo(UUID));
            final ArrayList<String> lore = new ArrayList<String>();
            lore.add(" ");
            if (MobspawnAPI.getStatus(UUID) == true) {
                lore.add(ChatColor.GREEN + "Mobspawner ativado!");
            } else {
                lore.add(ChatColor.RED + "Mobspawner desativado!");
            }
            lore.add(" ");
            metaAtivoNow.setLore(lore);
        }
        ativoNow.setItemMeta(metaAtivoNow);

        inventory.setItem(40, ativoNow);

        return inventory;
    }

    public static Inventory getInvFarm() {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Configuração farm: ");

        final ItemStack farms = new ItemStack(Material.CHEST);
        final ItemMeta metaFarms = farms.getItemMeta();
        metaFarms.setDisplayName(ChatColor.YELLOW + "Farms");
        farms.setItemMeta(metaFarms);

        final ItemStack compras = new ItemStack(Material.GOLD_INGOT);
        final ItemMeta metaCompras = compras.getItemMeta();
        metaCompras.setDisplayName(ChatColor.YELLOW + "Comprar plot");
        compras.setItemMeta(metaCompras);

        final ItemStack configurar = new ItemStack(Material.COMPARATOR);
        final ItemMeta metaConfig = configurar.getItemMeta();
        metaConfig.setDisplayName(ChatColor.YELLOW + "Configurar (McMMO)");
        final ArrayList<String> lore = new ArrayList<String>();
        lore.add(" ");
        lore.add(ChatColor.RED + "EM BREVE");
        lore.add(" ");
        metaConfig.setLore(lore);
        configurar.setItemMeta(metaConfig);

        inventory.setItem(11, farms);
        inventory.setItem(13, compras);
        inventory.setItem(15, configurar);

        return inventory;
    }

    public static Inventory getInvPlots(final String UUID) {
        final Inventory inventory = Bukkit.createInventory(null, 5 * 9, "Farms: ");

        for (int i = 1; i <= 21; i++) {
            if (FarmAPI.getBloco(UUID + i) != null) {
                final ItemStack slot = new ItemStack(Material.getMaterial(FarmAPI.getBloco(UUID + i)), 1);
                final ItemMeta metaSlot = slot.getItemMeta();
                metaSlot.setDisplayName(FarmAPI.getTexto(UUID + i));
                final ArrayList<String> lore = new ArrayList<String>();
                if (FarmAPI.getStatus(UUID + i) == true) {
                    lore.add(" ");
                    lore.add(ChatColor.RED + "ATIVO!");
                    lore.add(" ");
                    lore.add(ChatColor.GOLD + "CLIQUE COM BOTAO DIREITO PARA CONFIGURAR-LO.");
                    lore.add(" ");
                } else {
                    lore.add(" ");
                    lore.add(ChatColor.GOLD + "CLIQUE COM BOTAO DIREITO PARA CONFIGURAR-LO.");
                    lore.add(" ");
                }
                metaSlot.setLore(lore);
                slot.setItemMeta(metaSlot);

                if (i <= 7) {
                    inventory.setItem(9 + i, slot);
                } else if (i <= 14) {
                    inventory.setItem(11 + i, slot);
                } else if (i <= 21) {
                    inventory.setItem(13 + i, slot);
                }
            } else {
                break;
            }
        }

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta metaVoltar = voltar.getItemMeta();
        metaVoltar.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(metaVoltar);

        inventory.setItem(36, voltar);

        return inventory;
    }

    public static Inventory getConfirmarMoeda() {
        final Inventory inv = Bukkit.createInventory(null, 3 * 9, "Escolha opção de pagamento:  ");

        final ItemStack moeda = new ItemStack(Material.GOLD_NUGGET);
        final ItemMeta moedaMeta = moeda.getItemMeta();
        moedaMeta.setDisplayName(ChatColor.YELLOW + "Moeda comum");
        final ArrayList<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.DARK_GRAY + "A o escolher está opção serão debitados");
        lore1.add(ChatColor.DARK_GRAY + "de sua conta" + ChatColor.GREEN + " 5.000,00 " + ChatColor.DARK_GRAY + "moedas!");
        lore1.add(" ");
        moedaMeta.setLore(lore1);
        moeda.setItemMeta(moedaMeta);

        final ItemStack moedanegra = new ItemStack(Material.IRON_NUGGET);
        final ItemMeta moedanegraMeta = moedanegra.getItemMeta();
        moedanegraMeta.setDisplayName(ChatColor.YELLOW + "Moeda negra");
        final ArrayList<String> lore2 = new ArrayList<String>();
        lore2.add(" ");
        lore2.add(ChatColor.DARK_GRAY + "A o escolher está opção serão debitados");
        lore2.add(ChatColor.DARK_GRAY + "de sua conta" + ChatColor.RED + " 100,00 " + ChatColor.DARK_GRAY + "moedas negras!");
        lore2.add(" ");
        moedanegraMeta.setLore(lore2);
        moedanegra.setItemMeta(moedanegraMeta);

        inv.setItem(12, moeda);
        inv.setItem(14, moedanegra);

        return inv;
    }

    public static Inventory getConfirmarCompra() {
        final Inventory inv = Bukkit.createInventory(null, 3 * 9, "Confirmar compra plot: ");

        final ItemStack confirmar = new ItemStack(Material.LIME_STAINED_GLASS_PANE);
        final ItemMeta metaConfirmar = confirmar.getItemMeta();
        metaConfirmar.setDisplayName(ChatColor.GREEN + "CONFIRMAR");
        confirmar.setItemMeta(metaConfirmar);

        final ItemStack negar = new ItemStack(Material.RED_STAINED_GLASS_PANE);
        final ItemMeta metaNegar = negar.getItemMeta();
        metaNegar.setDisplayName(ChatColor.RED + "NEGAR");
        negar.setItemMeta(metaNegar);

        inv.setItem(12, confirmar);
        inv.setItem(14, negar);

        return inv;
    }

    public static Inventory getConfigPlot(final String UUID) {
        final Inventory inventory = Bukkit.createInventory(null, 3 * 9, "Configurar plot: ");

        final String[] bloco = FarmAPI.getBloco(UUID).split(":");
        final ItemStack plot = new ItemStack(Material.getMaterial(bloco[0]), 1);
        final ItemMeta metaPlot = plot.getItemMeta();
        metaPlot.setDisplayName(FarmAPI.getTexto(UUID));
        plot.setItemMeta(metaPlot);

        final ItemStack renomear = new ItemStack(Material.NAME_TAG);
        final ItemMeta metaReno = renomear.getItemMeta();
        metaReno.setDisplayName(ChatColor.YELLOW + "Renomear plot");
        renomear.setItemMeta(metaReno);

        final ItemStack alterar = new ItemStack(Material.GRASS_BLOCK);
        final ItemMeta metaAlte = alterar.getItemMeta();
        metaAlte.setDisplayName(ChatColor.YELLOW + "Alterar bloco plot");
        final ArrayList<String> lore = new ArrayList<String>();
        lore.add(" ");
        lore.add(ChatColor.DARK_GRAY + "Arraste o bloco desejado");
        lore.add(ChatColor.DARK_GRAY + "até aqui!");
        lore.add(" ");
        metaAlte.setLore(lore);
        alterar.setItemMeta(metaAlte);

        final ItemStack voltar = new ItemStack(Material.ARROW);
        final ItemMeta metaVoltar = voltar.getItemMeta();
        metaVoltar.setDisplayName(ChatColor.RED + "Voltar");
        voltar.setItemMeta(metaVoltar);

        inventory.setItem(11, plot);
        inventory.setItem(14, renomear);
        inventory.setItem(15, alterar);
        inventory.setItem(18, voltar);

        return inventory;
    }
}
