package com.mixedup.plots.espaconave;

import java.util.HashMap;

public class LojaEspaconave {

    private static final HashMap<String, LojaEspaconave> CACHE = new HashMap<String, LojaEspaconave>();
    private static final int i = 1;
    private String regionID;
    private String pos1;
    private String pos2;

    public LojaEspaconave(final String regionID, final String pos1, final String pos2) {
        this.regionID = regionID;
        this.pos1 = pos1;
        this.pos2 = pos2;
    }

    public static LojaEspaconave get(final String UUID) {
        return LojaEspaconave.CACHE.get(UUID);
    }

    public LojaEspaconave insert() {
        LojaEspaconave.CACHE.put(regionID, this);

        return this;
    }

    public String getRegionID() {
        return this.regionID;
    }

    public void setRegionID(final String regionID) {
        this.regionID = regionID;
    }

    public String getPos1() {
        return this.pos1;
    }

    public void setPos1(final String pos1) {
        this.pos1 = pos1;
    }

    public String getPos2() {
        return this.pos2;
    }

    public void setPos2(final String pos2) {
        this.pos2 = pos2;
    }
}
