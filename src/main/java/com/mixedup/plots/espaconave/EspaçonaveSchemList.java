package com.mixedup.plots.espaconave;

import com.mixedup.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EspaçonaveSchemList {

    private static List<EspaçonaveSchem> regions = new ArrayList<>();

    public static EspaçonaveSchem getRegion(final String regionID) {
        for (final EspaçonaveSchem region : EspaçonaveSchemList.getRegions()) {
            if (region.getRegionID() == regionID) {
                return region;
            }
        }
        return null;
    }

    public static List<EspaçonaveSchem> getRegions() {
        return EspaçonaveSchemList.regions;
    }

    public static void setRegions(final List<EspaçonaveSchem> regions) {
        EspaçonaveSchemList.regions = regions;
    }

    public static void enableRegions() {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Espaçonave_data");
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                final String loc1 = (1000 * rs.getInt("ID") - 515) + ":" + "70" + ":" + "485";
                final String loc2 = (1000 * rs.getInt("ID") - 487) + ":" + "89" + ":" + "513";
                final EspaçonaveSchem schem = new EspaçonaveSchem(rs.getString("UUID"), loc1, loc2);
                getRegions().add(schem);
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }
}
