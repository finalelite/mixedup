package com.mixedup.plots.espaconave;

import com.mixedup.Main;
import com.mixedup.utils.AlternateColor;
import com.mixedup.utils.Hologram;
import com.mixedup.utils.WorldEditUtil;
import com.sk89q.worldedit.WorldEditException;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class EspaconaveRegion {

    private static final HashMap<String, EspaconaveRegion> CACHE = new HashMap<>();
    private String regionID;
    private String pos1;
    private String pos2;

    public EspaconaveRegion(final String regionID, final String pos1, final String pos2) {
        this.regionID = regionID;
        this.pos1 = pos1;
        this.pos2 = pos2;
    }

    public static EspaconaveRegion get(final String UUID) {
        return EspaconaveRegion.CACHE.get(UUID);
    }

    public static void loadEspaçonaves(final Location location, final String UUID) {
        // $1
        new BukkitRunnable() {
            @Override
            public void run() {
                final double xHologram1 = 1000.0 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 501.0 + 17.5;
                EspaconaveRegion.createHologram(UUID, xHologram1, 482.5);

                final Double xHologram3 = 1000.0 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 517.5;
                EspaconaveRegion.createHologram(UUID, xHologram3, 516.5);

                final double xHologram8 = 1000.0 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 501.0 + 0.5;
                final double yHologram8 = 71.0;
                final double zHologram8 = 530.5;
                final Location hologram8 = new Location(Bukkit.getWorld("espaconave"), xHologram8, yHologram8, zHologram8);
                final String[] text8 = {AlternateColor.alternate("&cConfigurar"), AlternateColor.alternate("&cPainéis solares")};
                Hologram.createHologram(hologram8, text8);

                final double xHologram9 = 1000.0 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 501.0 + 0.5;
                final double yHologram9 = 72.0;
                final double zHologram9 = 470.5;
                final Location hologram9 = new Location(Bukkit.getWorld("espaconave"), xHologram9, yHologram9, zHologram9);
                final String[] text9 = {AlternateColor.alternate("&aLoja")};
                Hologram.createHologram(hologram9, text9);

                try {
                    WorldEditUtil.paste(WorldEditUtil.load(Main.plugin.getDataFolder() + File.separator + "WorldEdit/schematics/espaconave-rpl"), true, location.add(0, 2, 0));
                } catch (final IOException | WorldEditException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskLater(Main.getInstance(), 0);

    }

    private static void createHologram(final String UUID, final Double xHologram1, final double v) {
        final Double yHologram1 = 69.7;
        final Double zHologram1 = 499.5;
        EspaconaveRegion.createPlotHologram(xHologram1, yHologram1, zHologram1);

        final Double xHologram2 = 1000.0 * EspaçonaveAPI.getIdEspaçonave(UUID + "1") - 501.0 + 0.5;
        final Double yHologram2 = 69.7;
        final Double zHologram2 = v;
        EspaconaveRegion.createPlotHologram(xHologram2, yHologram2, zHologram2);
    }

    private static void createPlotHologram(final Double xHologram2, final Double yHologram2, final Double zHologram2) {
        final Location hologram2 = new Location(Bukkit.getWorld("espaconave"), xHologram2, yHologram2, zHologram2);
        final String[] text2 = {AlternateColor.alternate("&eConfigurar"), AlternateColor.alternate("&eFarm")};
        Hologram.createHologram(hologram2, text2);
    }

    public EspaconaveRegion insert() {
        EspaconaveRegion.CACHE.put(regionID, this);

        return this;
    }

    public String getRegionID() {
        return this.regionID;
    }

    public void setRegionID(final String regionID) {
        this.regionID = regionID;
    }

    public String getPos1() {
        return this.pos1;
    }


    //---

    public void setPos1(final String pos1) {
        this.pos1 = pos1;
    }

    public String getPos2() {
        return this.pos2;
    }

    public void setPos2(final String pos2) {
        this.pos2 = pos2;
    }
}
