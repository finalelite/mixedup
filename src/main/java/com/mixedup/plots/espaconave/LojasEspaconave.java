package com.mixedup.plots.espaconave;

import com.mixedup.MySql;
import org.bukkit.Location;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LojasEspaconave {

    private static List<LojaEspaconave> regions = new ArrayList<>();

    public static LojaEspaconave getRegion(final String regionID) {
        for (final LojaEspaconave region : LojasEspaconave.getRegions()) {
            if (region.getRegionID() == regionID) {
                return region;
            }
        }
        return null;
    }

    public static List<LojaEspaconave> getRegions() {
        return LojasEspaconave.regions;
    }

    public static void setRegions(final List<LojaEspaconave> regions) {
        LojasEspaconave.regions = regions;
    }

    public static void enableRegions() {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Espaçonave_data");
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                final String pos1 = (1000 * rs.getInt("ID") - 492) + ":" + "73" + ":" + "474";
                final String pos2 = (1000 * rs.getInt("ID") - 510) + ":" + "71" + ":" + "467";
                final LojaEspaconave lojaRegion = new LojaEspaconave(rs.getString("UUID"), pos1, pos2);
                getRegions().add(lojaRegion);
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static LojaEspaconave playerInArea(final Location loc) {

        final List<LojaEspaconave> lojas = getRegions();
        for (final LojaEspaconave rg : lojas) {

            final String[] pos1 = rg.getPos1().split(":");
            final String[] pos2 = rg.getPos2().split(":");

            if (!pos1[0].equalsIgnoreCase(loc.getWorld().getName())) return null;

            final int p1x = Integer.valueOf(pos1[0]);
            final int p1y = Integer.valueOf(pos1[1]);
            final int p1z = Integer.valueOf(pos1[2]);
            final int p2x = Integer.valueOf(pos2[0]);
            final int p2y = Integer.valueOf(pos2[1]);
            final int p2z = Integer.valueOf(pos2[2]);

            final int minX = p1x < p2x ? p1x : p2x;
            final int minY = p1y < p2y ? p1y : p2y;
            final int minZ = p1z < p2z ? p1z : p2z;

            final int maxX = p1x > p2x ? p1x : p2x;
            final int maxY = p1y > p2y ? p1y : p2y;
            final int maxZ = p1z > p2z ? p1z : p2z;

            if ((loc.getBlockX() >= minX) && (loc.getBlockX() <= maxX) && (loc.getBlockY() >= minY)
                    && (loc.getBlockY() <= maxY) && (loc.getBlockZ() >= minZ) && (loc.getBlockZ() <= maxZ)) {

                return rg;

            }
        }
        return null;
    }
}
