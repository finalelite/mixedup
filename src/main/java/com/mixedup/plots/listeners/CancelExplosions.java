package com.mixedup.plots.listeners;

import com.mixedup.plots.espaconave.EspaçonaveUtil;
import com.mixedup.plots.ilha.IlhaUtil;
import com.mixedup.plots.terrain.TerrainsUtil;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;

public class CancelExplosions implements Listener {

    @EventHandler
    public void onExplode(final EntityExplodeEvent event) {
        if (IlhaUtil.playerInArea(event.getLocation()) != null || EspaçonaveUtil.playerInArea(event.getLocation()) != null || TerrainsUtil.playerInArea(event.getLocation()) != null) {
            event.setCancelled(true);
        }
    }
}
