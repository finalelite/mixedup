package com.mixedup.plots.listeners;

import com.mixedup.apis.GuildaAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.plots.espaconave.EspaçonaveAPI;
import com.mixedup.plots.ilha.IlhaAPI;
import com.mixedup.plots.terrain.TerrainInfosAPI;
import com.mixedup.plots.utils.AddUtil;
import com.mixedup.plots.utils.RemoveUtil;
import com.mixedup.utils.AlternateColor;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class PlayerSpeakEvents implements Listener {

    @EventHandler
    public void onSay(final AsyncPlayerChatEvent event) {

        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (AddUtil.get(UUID) != null && AddUtil.get(UUID).getStatus() == true) {
            event.setCancelled(true);
            if (GuildaAPI.getGuilda(UUID).equals("nobre") || GuildaAPI.getGuilda(UUID).equals("sanguinaria")) {
                if (!event.getMessage().contains(" ")) {
                    if (event.getMessage().equalsIgnoreCase(player.getName())) {
                        player.sendMessage(ChatColor.RED + " * Ops, você não pode se adicionar a lista de amigos!");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        return;
                    }
                    final String amigo = PlayerUUID.getUUIDcache(event.getMessage());

                    if (!TerrainInfosAPI.getAmigos(UUID + "1").equals("NULL") && !TerrainInfosAPI.getAmigos(UUID + "1").contains(":")) {
                        if (TerrainInfosAPI.getAmigos(UUID + "1").equals(amigo)) {
                            player.sendMessage(ChatColor.RED + "Ops, este amigo já está em sua lista!");
                            event.setCancelled(true);
                            return;
                        }
                    } else if (TerrainInfosAPI.getAmigos(UUID + "1").contains(":")) {
                        final String[] amigos = TerrainInfosAPI.getAmigos(UUID + "1").split(":");

                        for (int i = 1; i <= amigos.length; i++) {
                            if (amigos[i - 1].equals(amigo)) {
                                player.sendMessage(ChatColor.RED + "Ops, este amigo já está em sua lista!");
                                event.setCancelled(true);
                                return;
                            }
                        }
                    }

                    if (player.getName().equals(amigo)) {
                        player.sendMessage(ChatColor.RED + " * Ops, você não pode se adicionar na lista de amigos D=");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        event.setCancelled(true);
                        return;
                    }

                    if (PlayerUUID.getUUID(event.getMessage()) != null) {
                        if (TerrainInfosAPI.getAmigos(UUID + "1").equals("NULL")) {
                            TerrainInfosAPI.updateAmigos(UUID + "1", amigo);
                        } else {
                            TerrainInfosAPI.updateAmigos(UUID + "1", TerrainInfosAPI.getAmigos(UUID + "1") + ":" + amigo);
                        }
                        player.sendMessage(ChatColor.GREEN + " * Você acaba de adicionar " + AlternateColor.alternate("&n" + event.getMessage()) + ChatColor.GREEN + " a sua lista de amigos.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        AddUtil.get(UUID).setStatus(false);
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, não encontramos este usuário em nosso banco de dados.");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        AddUtil.get(UUID).setStatus(false);
                    }
                    event.setCancelled(true);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, o nick informado não existe ou contém algum erro!.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
                event.setCancelled(true);
            } else if (GuildaAPI.getGuilda(UUID).equals("ancia")) {
                if (!event.getMessage().contains(" ")) {
                    if (event.getMessage().equalsIgnoreCase(player.getName())) {
                        player.sendMessage(ChatColor.RED + " * Ops, você não pode se adicionar a lista de amigos!");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        return;
                    }
                    final String amigo = PlayerUUID.getUUIDcache(event.getMessage());

                    if (!IlhaAPI.getAmigos(UUID + "1").equals("NULL") && !IlhaAPI.getAmigos(UUID + "1").contains(":")) {
                        if (IlhaAPI.getAmigos(UUID + "1").equals(amigo)) {
                            player.sendMessage(ChatColor.RED + "Ops, este amigo já está em sua lista!");
                            event.setCancelled(true);
                            return;
                        }
                    } else if (IlhaAPI.getAmigos(UUID + "1").contains(":")) {
                        final String[] amigos = IlhaAPI.getAmigos(UUID + "1").split(":");

                        for (int i = 1; i <= amigos.length; i++) {
                            if (amigos[i - 1].equals(amigo)) {
                                player.sendMessage(ChatColor.RED + "Ops, este amigo já está em sua lista!");
                                event.setCancelled(true);
                                return;
                            }
                        }
                    }

                    if (player.getName().equals(amigo)) {
                        player.sendMessage(ChatColor.RED + " * Ops, você não pode se adicionar na lista de amigos D=");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        event.setCancelled(true);
                        return;
                    }

                    if (PlayerUUID.getUUID(event.getMessage()) != null) {
                        if (IlhaAPI.getAmigos(UUID + "1").equals("NULL")) {
                            IlhaAPI.updateAmigos(UUID + "1", amigo);
                        } else {
                            IlhaAPI.updateAmigos(UUID + "1", IlhaAPI.getAmigos(UUID + "1") + ":" + amigo);
                        }
                        player.sendMessage(ChatColor.GREEN + " * Você acaba de adicionar " + AlternateColor.alternate("&n" + event.getMessage()) + ChatColor.GREEN + " a sua lista de amigos.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        AddUtil.get(UUID).setStatus(false);
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, não encontramos este usuário em nosso banco de dados.");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        AddUtil.get(UUID).setStatus(false);
                    }
                    event.setCancelled(true);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, o nick informado não existe ou contém algum erro!.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    event.setCancelled(true);
                }
            }
        }

        if (RemoveUtil.get(UUID) != null && RemoveUtil.get(UUID).getStatus() == true) {
            if (GuildaAPI.getGuilda(UUID).equals("nobre") || GuildaAPI.getGuilda(UUID).equals("sanguinaria")) {
                if (!event.getMessage().contains(" ")) {
                    final String amigo = PlayerUUID.getUUIDcache(event.getMessage());
                    final String[] amigos = TerrainInfosAPI.getAmigos(UUID + "1").split(":");

                    if (player.getName().equals(amigo)) {
                        player.sendMessage(ChatColor.RED + " * Ops, você não pode se remover da lista de amigos D=");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        event.setCancelled(true);
                        return;
                    }

                    if (amigos.length > 1) {
                        String newamigos = null;
                        for (int i = 1; i <= amigos.length; i++) {
                            if (!amigos[i - 1].equals(amigo)) {
                                if (newamigos == null) {
                                    newamigos = amigos[i - 1];
                                } else {
                                    newamigos = newamigos + ":" + amigos[i - 1];
                                }
                            }

                            if (i == amigos.length) {
                                TerrainInfosAPI.updateAmigos(UUID + "1", newamigos);
                                player.sendMessage(ChatColor.GREEN + " * Você acaba de remover " + AlternateColor.alternate("&n" + event.getMessage()) + ChatColor.GREEN + " de sua lista de amigos.");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                RemoveUtil.get(UUID).setStatus(false);
                            }
                        }
                    } else if (amigos.length == 1) {
                        if (TerrainInfosAPI.getAmigos(UUID + "1").equals(amigo)) {
                            TerrainInfosAPI.updateAmigos(UUID + "1", "NULL");
                            player.sendMessage(ChatColor.GREEN + " * Você acaba de remover " + AlternateColor.alternate("&n" + event.getMessage()) + ChatColor.GREEN + " de sua lista de amigos.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            RemoveUtil.get(UUID).setStatus(false);
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, você não contém este amigo em sua lista!");
                        }
                    }
                    event.setCancelled(true);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, o nick informado não existe ou contém algum erro!.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    event.setCancelled(true);
                }
            } else if (GuildaAPI.getGuilda(UUID).equals("ancia")) {
                if (!event.getMessage().contains(" ")) {
                    final String amigo = PlayerUUID.getUUIDcache(event.getMessage());
                    final String[] amigos = IlhaAPI.getAmigos(UUID + "1").split(":");

                    if (player.getName().equals(amigo)) {
                        player.sendMessage(ChatColor.RED + " * Ops, você não pode se remover da lista de amigos D=");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        event.setCancelled(true);
                        return;
                    }

                    if (amigos.length > 1) {
                        String newamigos = null;
                        for (int i = 1; i <= amigos.length; i++) {
                            if (!amigos[i - 1].equals(amigo)) {
                                if (newamigos == null) {
                                    newamigos = amigos[i - 1];
                                } else {
                                    newamigos = newamigos + ":" + amigos[i - 1];
                                }
                            }

                            if (i == amigos.length) {
                                IlhaAPI.updateAmigos(UUID + "1", newamigos);
                                player.sendMessage(ChatColor.GREEN + " * Você acaba de remover " + AlternateColor.alternate("&n" + event.getMessage()) + ChatColor.GREEN + " de sua lista de amigos.");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                RemoveUtil.get(UUID).setStatus(false);
                            }
                        }
                    } else if (amigos.length == 1) {
                        if (IlhaAPI.getAmigos(UUID + "1").equals(amigo)) {
                            IlhaAPI.updateAmigos(UUID + "1", "NULL");
                            player.sendMessage(ChatColor.GREEN + " * Você acaba de remover " + AlternateColor.alternate("&n" + event.getMessage()) + ChatColor.GREEN + " de sua lista de amigos.");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            RemoveUtil.get(UUID).setStatus(false);
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, você não contém este amigo em sua lista!");
                        }
                    }
                    event.setCancelled(true);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, o nick informado não existe ou contém algum erro!.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    event.setCancelled(true);
                }
            }
        }
    }
}
