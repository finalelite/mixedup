package com.mixedup.plots.listeners;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import com.mixedup.plots.espaconave.EspaconaveRegion;
import com.mixedup.plots.espaconave.EspaçonaveAPI;
import com.mixedup.plots.espaconave.EspaçonaveUtil;
import com.mixedup.plots.ilha.IlhaAPI;
import com.mixedup.plots.ilha.IlhaRegion;
import com.mixedup.plots.ilha.IlhaUtil;
import com.mixedup.plots.terrain.RegionTr;
import com.mixedup.plots.terrain.TerrainInfosAPI;
import com.mixedup.plots.terrain.TerrainsUtil;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Random;

public class PlayerOpenChest implements Listener {

    @EventHandler
    public void onOpenChest(final PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        final String UUID = player.getUniqueId().toString();

        String tag = TagAPI.getTag(UUID);
        if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Moderador") || tag.equalsIgnoreCase("Admin")) return;

        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (event.getClickedBlock().getType().equals(Material.CHEST) || event.getClickedBlock().getType().equals(Material.CHEST_MINECART) || event.getClickedBlock().getType().equals(Material.TRAPPED_CHEST) || event.getClickedBlock().getType().equals(Material.FURNACE) || event.getClickedBlock().getType().equals(Material.FURNACE_MINECART)) {
                if (event.getClickedBlock().getWorld().getName().equalsIgnoreCase("ilhas-default")) {
                    if (IlhaUtil.playerInArea(event.getClickedBlock().getLocation()) != null) {
                        final IlhaRegion region = IlhaUtil.playerInArea(event.getClickedBlock().getLocation());
                        final String owner = region.getRegionID().substring(0, region.getRegionID().length() - 1);

                        if (UUID.equals(owner)) {
                            event.setCancelled(false);
                            return;
                        }

                        if (IlhaAPI.getAmigos(region.getRegionID()).contains(":")) {
                            final String[] amigos = IlhaAPI.getAmigos(region.getRegionID()).split(":");

                            for (int i = 1; i <= amigos.length; i++) {
                                if (amigos[i - 1].equalsIgnoreCase(UUID)) {
                                    event.setCancelled(false);
                                    return;
                                }
                                if (i == amigos.length) {
                                    event.setCancelled(true);
                                    player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                                    final Random random = new Random();

                                    if (random.nextBoolean() == true) {
                                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                    }
                                }
                            }
                            return;
                        } else if (!IlhaAPI.getAmigos(region.getRegionID()).equals("NULL")) {
                            if (IlhaAPI.getAmigos(region.getRegionID()).equals(UUID)) {
                                event.setCancelled(false);
                                return;
                            }
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                            final Random random = new Random();

                            if (random.nextBoolean() == true) {
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                            return;
                        } else if (IlhaAPI.getAmigos(region.getRegionID()).equals("NULL")) {
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.RED + " * Ops, esta ilha contém proteção!");
                            final Random random = new Random();

                            if (random.nextBoolean() == true) {
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        }
                    }
                } else {
                    //-----------------------------------------------------------------------------
                    if (TerrainsUtil.playerInArea(event.getClickedBlock().getLocation()) != null) {
                        final RegionTr region = TerrainsUtil.playerInArea(event.getClickedBlock().getLocation());
                        final String owner = region.getRegionID().substring(0, region.getRegionID().length() - 1);

                        if (UUID.equals(owner)) {
                            event.setCancelled(false);
                            return;
                        }

                        if (TerrainInfosAPI.getAmigos(region.getRegionID()).contains(":")) {
                            final String[] amigos = TerrainInfosAPI.getAmigos(region.getRegionID()).split(":");

                            for (int i = 1; i <= amigos.length; i++) {
                                if (amigos[i - 1].equalsIgnoreCase(UUID)) {
                                    event.setCancelled(false);
                                    return;
                                }
                                if (i == amigos.length) {
                                    event.setCancelled(true);
                                    player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                                    final Random random = new Random();

                                    if (random.nextBoolean() == true) {
                                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                    }
                                }
                            }
                            return;
                        } else if (!TerrainInfosAPI.getAmigos(region.getRegionID()).equals("NULL")) {
                            if (TerrainInfosAPI.getAmigos(region.getRegionID()).equals(UUID)) {
                                event.setCancelled(false);
                                return;
                            }
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                            final Random random = new Random();

                            if (random.nextBoolean() == true) {
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                            return;
                        } else if (TerrainInfosAPI.getAmigos(region.getRegionID()).equals("NULL")) {
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                            final Random random = new Random();

                            if (random.nextBoolean() == true) {
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        }
                    }
                }
            }
        }
    }
}
