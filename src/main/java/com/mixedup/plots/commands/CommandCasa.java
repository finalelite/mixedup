package com.mixedup.plots.commands;

import com.mixedup.Main;
import com.mixedup.apis.ConfigAPI;
import com.mixedup.apis.GuildaAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.plots.CasaAPI;
import com.mixedup.plots.espaconave.EspaçonaveAPI;
import com.mixedup.plots.ilha.IlhaAPI;
import com.mixedup.plots.ilha.IlhaUtil;
import com.mixedup.plots.terrain.TerrainInfosAPI;
import com.mixedup.plots.terrain.TerrainsUtil;
import com.mixedup.utils.AlternateColor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandCasa implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("casa") || cmd.getName().equalsIgnoreCase("home")) {
            if (args.length == 0) {
                if (CasaAPI.getLocation(UUID) != null) {
                    player.sendMessage(ChatColor.GREEN + " * Teletransportando com sucesso.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    final String[] casa = CasaAPI.getLocation(UUID).split(":");
                    final Location location = new Location(Bukkit.getWorld(casa[0]), Double.parseDouble(casa[1]), Double.parseDouble(casa[2]), Double.parseDouble(casa[3]), Float.parseFloat(casa[4]), Float.parseFloat(casa[5]));
                    player.teleport(location);
                    if(player.getAllowFlight()) player.setAllowFlight(false);
                    if(ConfigAPI.getFlyStatus(UUID)) ConfigAPI.updateFly(UUID, false);

                } else {
                    final String guilda = GuildaAPI.getGuilda(UUID);
                    if ("nobre".equals(guilda) || "sanguinaria".equals(guilda)) {
                        player.sendMessage(ChatColor.RED + " * Ops, você não contém nenhum terreno para efetuar este comando.");
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, você não contém nenhuma ilha para efetuar este comando.");
                    }
                }
            } else if (args.length == 1) {
                if (args[0].equalsIgnoreCase("ajuda")) {
                    player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes á &ncasa&c:\n \n&7 - /casa ou /home\n&7 - /casa definir\n&7 - /casa (habilitar/desabilitar) visitas\n \n"));
                    return false;
                }

                if (args[0].equals("definir") || args[0].equals("set")) {
                    if (CasaAPI.getLocation(UUID) != null) {
                        if (GuildaAPI.getGuilda(UUID) != null) {
                            if (GuildaAPI.getGuilda(UUID).equals("nobre") || GuildaAPI.getGuilda(UUID).equals("sanguinaria")) {
                                if (TerrainsUtil.playerInArea(player.getLocation()) != null) {
                                    if (TerrainsUtil.playerInArea(player.getLocation()).getRegionID().equals(UUID + "1")) {
                                        player.sendMessage(ChatColor.GREEN + "\n \n * Você acaba de definir um novo ponto de spawn para seu terreno.\n \n");
                                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);

                                        final String location = player.getLocation().getWorld().getName() + ":" + player.getLocation().getX() + ":" + player.getLocation().getY() + ":" + player.getLocation().getZ() + ":" + player.getLocation().getYaw() + ":" + player.getLocation().getPitch();
                                        CasaAPI.updateCasa(UUID, location);
                                    } else {
                                        player.sendMessage(ChatColor.RED + " * Ops, para efetuar este comando você deve estar em seu terreno!");
                                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                    }
                                } else {
                                    player.sendMessage(ChatColor.RED + " * Ops, para efetuar este comando você deve estar em seu terreno!");
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            } else if (GuildaAPI.getGuilda(UUID) != null && GuildaAPI.getGuilda(UUID).equals("ancia")) {
                                if (IlhaUtil.playerInArea(player.getLocation()) != null) {
                                    if (IlhaUtil.playerInArea(player.getLocation()).getRegionID().equals(UUID + "1")) {
                                        player.sendMessage(ChatColor.GREEN + "\n \n * Você acaba de definir um novo ponto de spawn para sua ilha.\n \n");
                                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);

                                        final String location = player.getWorld().getName() + ":" + player.getLocation().getX() + ":" + player.getLocation().getY() + ":" + player.getLocation().getZ() + ":" + player.getLocation().getYaw() + ":" + player.getLocation().getPitch();
                                        CasaAPI.updateCasa(UUID, location);
                                    } else {
                                        player.sendMessage(ChatColor.RED + " * Ops, para efetuar este comando você deve estar em sua ilha!");
                                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                    }
                                } else {
                                    player.sendMessage(ChatColor.RED + " * Ops, para efetuar este comando você deve estar em sua ilha!");
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            }
                        }
                    } else {
                        if (GuildaAPI.getGuilda(UUID) != null && GuildaAPI.getGuilda(UUID).equals("nobre")) {
                            player.sendMessage(ChatColor.RED + " * Ops, você não contém nenhum terreno para efetuar este comando.");
                        } else if (GuildaAPI.getGuilda(UUID) != null && GuildaAPI.getGuilda(UUID).equals("ancia")) {
                            player.sendMessage(ChatColor.RED + " * Ops, você não contém nenhuma ilha para efetuar este comando.");
                        } else if (GuildaAPI.getGuilda(UUID) != null && GuildaAPI.getGuilda(UUID).equals("sanguinaria")) {
                            player.sendMessage(ChatColor.RED + " * Ops, você não contém nenhuma espaçonave para efetuar este comando.");
                        }
                    }
                } else {
                    final String UUIDtarget = PlayerUUID.getUUID(args[0]);
                    if (CasaAPI.getLocation(UUIDtarget) != null) {
                        if (TerrainInfosAPI.getAmigos(UUIDtarget) != null) {
                            if (TerrainInfosAPI.getTeletransporte(UUIDtarget) == true) {
                                final String[] split = CasaAPI.getLocation(UUIDtarget).split(":");
                                final Location location = new Location(Bukkit.getWorld(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]), Double.parseDouble(split[3]), Float.parseFloat(split[4]), Float.parseFloat(split[4]));
                                player.teleport(location);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, este player não deseja receber visitas no momento!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else if (IlhaAPI.getAmigos(UUIDtarget) != null) {
                            if (IlhaAPI.getVisitas(UUIDtarget) == true) {
                                final String[] split = CasaAPI.getLocation(UUIDtarget).split(":");
                                final Location location = new Location(Bukkit.getWorld(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]), Double.parseDouble(split[3]), Float.parseFloat(split[4]), Float.parseFloat(split[4]));
                                player.teleport(location);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, este player não deseja receber visitas no momento!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else if (EspaçonaveAPI.getAmigos(UUIDtarget) != null) {
                            if (EspaçonaveAPI.getVisitas(UUIDtarget) == true) {
                                final String[] split = CasaAPI.getLocation(UUID).split(":");
                                final Location location = new Location(Bukkit.getWorld(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]), Double.parseDouble(split[3]), Float.parseFloat(split[4]), Float.parseFloat(split[4]));
                                player.teleport(location);
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, este player não deseja receber visitas no momento!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, este player não contém casa!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                }
            } else if (args.length == 2) {
                if (args[0].equals("habilitar") || args[0].equals("desabilitar")) {
                    if (args[1].equals("visitas")) {
                        if (args[0].equals("habilitar")) {
                            if (IlhaAPI.getVisitas(UUID) == false) {
                                if (GuildaAPI.getGuilda(UUID) != null && GuildaAPI.getGuilda(UUID).equals("ancia")) {
                                    IlhaAPI.updateVisitas(UUID, true);
                                    player.sendMessage(ChatColor.GREEN + "\n \n * Agora todos podem teletransportar a sua casa.\n \n");
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                } else if (GuildaAPI.getGuilda(UUID) != null && GuildaAPI.getGuilda(UUID).equals("nobre")) {
                                    TerrainInfosAPI.updateVisitas(UUID, true);
                                    player.sendMessage(ChatColor.GREEN + "\n \n * Agora todos podem teletransportar a sua casa.\n \n");
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                } else if (GuildaAPI.getGuilda(UUID) != null && GuildaAPI.getGuilda(UUID).equals("sanguinaria")) {
                                    //ARRUMAR AQUI
                                    IlhaAPI.updateVisitas(UUID, true);
                                    player.sendMessage(ChatColor.GREEN + "\n \n * Agora todos podem teletransportar a sua casa.\n \n");
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    //------------
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, isto já esta habilitado.");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        } else {
                            if (IlhaAPI.getVisitas(UUID) == true) {
                                if (GuildaAPI.getGuilda(UUID) != null && GuildaAPI.getGuilda(UUID).equals("ancia")) {
                                    IlhaAPI.updateVisitas(UUID, false);
                                    player.sendMessage(ChatColor.GREEN + "\n \n * Agora ninguem pode se teletransportar até sua casa.\n \n");
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                } else if (GuildaAPI.getGuilda(UUID) != null && GuildaAPI.getGuilda(UUID).equals("nobre")) {
                                    TerrainInfosAPI.updateVisitas(UUID, false);
                                    player.sendMessage(ChatColor.GREEN + "\n \n * Agora ninguem pode se teletransportar até sua casa.\n \n");
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                } else if (GuildaAPI.getGuilda(UUID) != null && GuildaAPI.getGuilda(UUID).equals("sanguinaria")) {
                                    //ARRUMAR AQUI
                                    IlhaAPI.updateVisitas(UUID, false);
                                    player.sendMessage(ChatColor.GREEN + "\n \n * Agora ninguem pode se teletransportar até sua casa.\n \n");
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    //------------
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, isto já esta desabilitado.");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, algum argumento aparentemente está errado.");
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, algum argumento aparentemente está errado.");
                }
            } else if (args.length >= 3) {
                player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes á &ncasa&c:\n \n&7 - /casa ou /home\n&7 - /casa definir\n&7 - /casa (habilitar/desabilitar) visitas\n \n"));
            }
        }
        return false;
    }
}
