package com.mixedup.plots.commands;

import com.mixedup.Main;
import com.mixedup.apis.GuildaAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.hashs.TerrainIsDemarcate;
import com.mixedup.plots.InventoryTerrenos;
import com.mixedup.plots.ilha.IlhaAPI;
import com.mixedup.plots.terrain.TerrainAPI;
import com.mixedup.utils.AlternateColor;
import org.bukkit.*;
import org.bukkit.Particle.DustOptions;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Random;

public class CommandsPlots implements CommandExecutor {

    boolean status;

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("terreno")) {
            if (GuildaAPI.getGuilda(UUID) != null) {
                if (GuildaAPI.getGuilda(UUID).equals("nobre") || GuildaAPI.getGuilda(UUID).equals("sanguinaria")) {
                    if (args.length == 0) {
                        player.openInventory(InventoryTerrenos.getInvTerrain("nobre"));
                    } else if (args.length == 1) {
                        if (args[0].equals("config")) {
                            if (TerrainAPI.getTerrainPos1(UUID + "1") != null) {
                                player.openInventory(InventoryTerrenos.configTerreno(UUID + "1"));
                            } else {
                                player.sendMessage(ChatColor.RED + "Ops, você não contém nenhum terreno para efetuar este comando.");
                            }
                        } else if (args[0].equals("demarcar") && GuildaAPI.getGuilda(UUID).equals("nobre")) {
                            player.sendMessage(ChatColor.RED + " * Temporariamente desabilitado!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            /*
                            if (TerrainAPI.getExist(UUID + "1") != null) {
                                if (TerrainIsDemarcate.get(UUID) == null) {
                                    new TerrainIsDemarcate(UUID, true).insert();
                                    player.sendMessage(ChatColor.GREEN + " * Terreno demarcado por 10 segundos!");
                                    player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                    this.status = false;
                                    //1
                                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                        @Override
                                        public void run() {
                                            CommandsPlots.this.status = true;
                                            TerrainIsDemarcate.get(UUID).setStatus(false);
                                        }
                                    }, 200L);

                                    //2
                                    final int rotacao1 = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                                        @Override
                                        public void run() {
                                            final String[] pos1 = TerrainAPI.getTerrainPos1(UUID + "1").split(":");
                                            final String[] pos2 = TerrainAPI.getTerrainPos2(UUID + "1").split(":");
                                            final World world = Bukkit.getWorld(pos1[0]);

                                            final int X1ORIGINAL = Integer.valueOf(pos1[1]);
                                            final int Z1ORIGINAL = Integer.valueOf(pos1[3]);
                                            final int X2ORIGINAL = Integer.valueOf(pos2[1]);
                                            final int Z2ORIGINAL = Integer.valueOf(pos2[3]);

                                            int X1A = Integer.valueOf(pos1[1]);
                                            final int Z1A = Integer.valueOf(pos1[3]);
                                            final int X2A = Integer.valueOf(pos2[1]);
                                            final int Z2A = Integer.valueOf(pos2[3]);

                                            final int X1C = Integer.valueOf(pos1[1]);
                                            int Z1C = Integer.valueOf(pos1[3]);
                                            final int X2C = Integer.valueOf(pos2[1]);
                                            final int Z2C = Integer.valueOf(pos2[3]);

                                            final int X1B = Integer.valueOf(pos1[1]);
                                            final int Z1B = Integer.valueOf(pos1[3]);
                                            int X2B = Integer.valueOf(pos2[1]);
                                            final int Z2B = Integer.valueOf(pos2[3]);

                                            final int X1D = Integer.valueOf(pos1[1]);
                                            final int Z1D = Integer.valueOf(pos1[3]);
                                            final int X2D = Integer.valueOf(pos2[1]);
                                            int Z2D = Integer.valueOf(pos2[3]);

                                            for (int i = 0; i <= 160; i++) {
                                                final double x12 = X2D + 0.5;
                                                final double y12 = world.getHighestBlockAt(X2D, Z2D).getY() + 0.5;
                                                final double z12 = Z2D + 0.5;

                                                if (Z1D < Z2D) {
                                                    final Location loc = new Location(world, X2D, world.getHighestBlockAt(X2D, Z2D).getY() - 1, Z2D);
                                                    if (loc.getBlock().getType().equals(Material.JUNGLE_LEAVES) || loc.getBlock().getType().equals(Material.OAK_LEAVES) || loc.getBlock().getType().equals(Material.SPRUCE_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LEAVES) || loc.getBlock().getType().equals(Material.BIRCH_LEAVES) || loc.getBlock().getType().equals(Material.DARK_OAK_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LOG) || loc.getBlock().getType().equals(Material.BIRCH_LOG) || loc.getBlock().getType().equals(Material.DARK_OAK_LOG) || loc.getBlock().getType().equals(Material.JUNGLE_LOG) || loc.getBlock().getType().equals(Material.OAK_LOG) || loc.getBlock().getType().equals(Material.SPRUCE_LOG) || loc.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                        boolean t = false;
                                                        for (int a = 1; a <= loc.getBlockY(); a++) {
                                                            final Location test = new Location(world, X2D, world.getHighestBlockAt(X2D, Z2D).getY() - a, Z2D);
                                                            final double x2 = test.getBlockX() + 0.5;
                                                            final double y2 = test.getBlockY() + 0.5;
                                                            final double z2 = test.getBlockZ() + 0.5;
                                                            final Location test2 = new Location(world, X2D, world.getHighestBlockAt(X2D, Z2D).getY() - a - 1, Z2D);
                                                            if (test.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.DARK_OAK_LEAVES) && !test2.getBlock().getType().equals(Material.BIRCH_LEAVES) && !test2.getBlock().getType().equals(Material.ACACIA_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LEAVES) && !test2.getBlock().getType().equals(Material.OAK_LEAVES) && !test2.getBlock().getType().equals(Material.JUNGLE_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LOG) && !test2.getBlock().getType().equals(Material.OAK_LOG) && !test2.getBlock().getType().equals(Material.JUNGLE_LOG) && !test2.getBlock().getType().equals(Material.DARK_OAK_LOG) && !test2.getBlock().getType().equals(Material.BIRCH_LOG) && !test2.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                                if (z2 >= Z1ORIGINAL) {
                                                                    final Location location = new Location(player.getWorld(), x2, y2, z2);
                                                                    final DustOptions dust = new DustOptions(Color.RED, 2);
                                                                    loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                                    Z2D--;
                                                                }
                                                            }
                                                            if (test.getBlock().getType().equals(Material.AIR)) {
                                                                t = true;
                                                            }
                                                        }
                                                        if (t == false) {
                                                            Z2D--;
                                                        }
                                                    } else {
                                                        if (z12 >= Z2ORIGINAL) {
                                                            final Location location = new Location(player.getWorld(), x12, y12, z12);
                                                            final Random random = new Random();
                                                            final DustOptions dust = new DustOptions(Color.fromBGR(random.nextInt(255), random.nextInt(255), random.nextInt(255)), 2);
                                                            loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                            Z2D--;
                                                        }
                                                    }
                                                } else if (Z1D > Z2D) {
                                                    final Location loc = new Location(world, X2D, world.getHighestBlockAt(X2D, Z2D).getY() - 1, Z2D);
                                                    if (loc.getBlock().getType().equals(Material.JUNGLE_LEAVES) || loc.getBlock().getType().equals(Material.OAK_LEAVES) || loc.getBlock().getType().equals(Material.SPRUCE_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LEAVES) || loc.getBlock().getType().equals(Material.BIRCH_LEAVES) || loc.getBlock().getType().equals(Material.DARK_OAK_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LOG) || loc.getBlock().getType().equals(Material.BIRCH_LOG) || loc.getBlock().getType().equals(Material.DARK_OAK_LOG) || loc.getBlock().getType().equals(Material.JUNGLE_LOG) || loc.getBlock().getType().equals(Material.OAK_LOG) || loc.getBlock().getType().equals(Material.SPRUCE_LOG) || loc.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                        boolean t = false;
                                                        for (int a = 1; a <= loc.getBlockY(); a++) {
                                                            final Location test = new Location(world, X2D, world.getHighestBlockAt(X2D, Z2D).getY() - a, Z2D);
                                                            final double x2 = test.getBlockX() + 0.5;
                                                            final double y2 = test.getBlockY() + 0.5;
                                                            final double z2 = test.getBlockZ() + 0.5;
                                                            final Location test2 = new Location(world, X2D, world.getHighestBlockAt(X2D, Z2D).getY() - a - 1, Z2D);
                                                            if (test.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.DARK_OAK_LEAVES) && !test2.getBlock().getType().equals(Material.BIRCH_LEAVES) && !test2.getBlock().getType().equals(Material.ACACIA_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LEAVES) && !test2.getBlock().getType().equals(Material.OAK_LEAVES) && !test2.getBlock().getType().equals(Material.JUNGLE_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LOG) && !test2.getBlock().getType().equals(Material.OAK_LOG) && !test2.getBlock().getType().equals(Material.JUNGLE_LOG) && !test2.getBlock().getType().equals(Material.DARK_OAK_LOG) && !test2.getBlock().getType().equals(Material.BIRCH_LOG) && !test2.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                                if (z2 <= Z1ORIGINAL) {
                                                                    final Location location = new Location(player.getWorld(), x2, y2, z2);
                                                                    final Random random = new Random();
                                                                    final DustOptions dust = new DustOptions(Color.fromBGR(random.nextInt(255), random.nextInt(255), random.nextInt(255)), 2);
                                                                    loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                                    Z2D++;
                                                                }
                                                            }
                                                            if (test.getBlock().getType().equals(Material.AIR)) {
                                                                t = true;
                                                            }
                                                        }
                                                        if (t == false) {
                                                            Z2D++;
                                                        }
                                                    } else {
                                                        if (z12 <= Z1ORIGINAL) {
                                                            final Location location = new Location(player.getWorld(), x12, y12, z12);
                                                            final Random random = new Random();
                                                            final DustOptions dust = new DustOptions(Color.fromBGR(random.nextInt(255), random.nextInt(255), random.nextInt(255)), 2);
                                                            loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                            Z2D++;
                                                        }
                                                    }
                                                }
                                            }

                                            for (int i = 0; i <= 160; i++) {
                                                final double x11 = X1C + 0.5;
                                                final double y11 = world.getHighestBlockAt(X1C, Z1C).getY() + 0.5;
                                                final double z11 = Z1C + 0.5;

                                                if (Z1C > Z2C) {
                                                    final Location loc = new Location(world, X1C, world.getHighestBlockAt(X1C, Z1C).getY() - 1, Z1C);
                                                    if (loc.getBlock().getType().equals(Material.JUNGLE_LEAVES) || loc.getBlock().getType().equals(Material.OAK_LEAVES) || loc.getBlock().getType().equals(Material.SPRUCE_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LEAVES) || loc.getBlock().getType().equals(Material.BIRCH_LEAVES) || loc.getBlock().getType().equals(Material.DARK_OAK_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LOG) || loc.getBlock().getType().equals(Material.BIRCH_LOG) || loc.getBlock().getType().equals(Material.DARK_OAK_LOG) || loc.getBlock().getType().equals(Material.JUNGLE_LOG) || loc.getBlock().getType().equals(Material.OAK_LOG) || loc.getBlock().getType().equals(Material.SPRUCE_LOG) || loc.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                        boolean t = false;
                                                        for (int a = 1; a <= loc.getBlockY(); a++) {
                                                            final Location test = new Location(world, X1C, world.getHighestBlockAt(X1C, Z1C).getY() - a, Z1C);
                                                            final double x2 = test.getBlockX() + 0.5;
                                                            final double y2 = test.getBlockY() + 0.5;
                                                            final double z2 = test.getBlockZ() + 0.5;
                                                            final Location test2 = new Location(world, X1C, world.getHighestBlockAt(X1C, Z1C).getY() - a - 1, Z1C);
                                                            if (test.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.DARK_OAK_LEAVES) && !test2.getBlock().getType().equals(Material.BIRCH_LEAVES) && !test2.getBlock().getType().equals(Material.ACACIA_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LEAVES) && !test2.getBlock().getType().equals(Material.OAK_LEAVES) && !test2.getBlock().getType().equals(Material.JUNGLE_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LOG) && !test2.getBlock().getType().equals(Material.OAK_LOG) && !test2.getBlock().getType().equals(Material.JUNGLE_LOG) && !test2.getBlock().getType().equals(Material.DARK_OAK_LOG) && !test2.getBlock().getType().equals(Material.BIRCH_LOG) && !test2.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                                if (z2 >= Z2ORIGINAL) {
                                                                    final Location location = new Location(player.getWorld(), x2, y2, z2);
                                                                    final DustOptions dust = new DustOptions(Color.RED, 2);
                                                                    loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                                    Z1C--;
                                                                }
                                                            }
                                                            if (test.getBlock().getType().equals(Material.AIR)) {
                                                                t = true;
                                                            }
                                                        }
                                                        if (t == false) {
                                                            Z1C--;
                                                        }
                                                    } else {
                                                        if (z11 >= Z2ORIGINAL) {
                                                            final Location location = new Location(player.getWorld(), x11, y11, z11);
                                                            final Random random = new Random();
                                                            final DustOptions dust = new DustOptions(Color.fromBGR(random.nextInt(255), random.nextInt(255), random.nextInt(255)), 2);
                                                            loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                            Z1C--;
                                                        }
                                                    }
                                                } else if (Z1C < Z2C) {
                                                    final Location loc = new Location(world, X1C, world.getHighestBlockAt(X1C, Z1C).getY() - 1, Z1C);
                                                    if (loc.getBlock().getType().equals(Material.JUNGLE_LEAVES) || loc.getBlock().getType().equals(Material.OAK_LEAVES) || loc.getBlock().getType().equals(Material.SPRUCE_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LEAVES) || loc.getBlock().getType().equals(Material.BIRCH_LEAVES) || loc.getBlock().getType().equals(Material.DARK_OAK_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LOG) || loc.getBlock().getType().equals(Material.BIRCH_LOG) || loc.getBlock().getType().equals(Material.DARK_OAK_LOG) || loc.getBlock().getType().equals(Material.JUNGLE_LOG) || loc.getBlock().getType().equals(Material.OAK_LOG) || loc.getBlock().getType().equals(Material.SPRUCE_LOG) || loc.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                        boolean t = false;
                                                        for (int a = 1; a <= loc.getBlockY(); a++) {
                                                            final Location test = new Location(world, X1C, world.getHighestBlockAt(X1C, Z1C).getY() - a, Z1C);
                                                            final double x2 = test.getBlockX() + 0.5;
                                                            final double y2 = test.getBlockY() + 0.5;
                                                            final double z2 = test.getBlockZ() + 0.5;
                                                            final Location test2 = new Location(world, X1C, world.getHighestBlockAt(X1C, Z1C).getY() - a - 1, Z1C);
                                                            if (test.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.DARK_OAK_LEAVES) && !test2.getBlock().getType().equals(Material.BIRCH_LEAVES) && !test2.getBlock().getType().equals(Material.ACACIA_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LEAVES) && !test2.getBlock().getType().equals(Material.OAK_LEAVES) && !test2.getBlock().getType().equals(Material.JUNGLE_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LOG) && !test2.getBlock().getType().equals(Material.OAK_LOG) && !test2.getBlock().getType().equals(Material.JUNGLE_LOG) && !test2.getBlock().getType().equals(Material.DARK_OAK_LOG) && !test2.getBlock().getType().equals(Material.BIRCH_LOG) && !test2.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                                if (z2 <= Z2ORIGINAL) {
                                                                    final Location location = new Location(player.getWorld(), x2, y2, z2);
                                                                    final DustOptions dust = new DustOptions(Color.RED, 2);
                                                                    loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                                    Z1C++;
                                                                }
                                                            }
                                                            if (test.getBlock().getType().equals(Material.AIR)) {
                                                                t = true;
                                                            }
                                                        }
                                                        if (t == false) {
                                                            Z1C++;
                                                        }
                                                    } else {
                                                        if (z11 <= Z2ORIGINAL) {
                                                            final Location location = new Location(player.getWorld(), x11, y11, z11);
                                                            final Random random = new Random();
                                                            final DustOptions dust = new DustOptions(Color.fromBGR(random.nextInt(255), random.nextInt(255), random.nextInt(255)), 2);
                                                            loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                            Z1C++;
                                                        }
                                                    }
                                                }
                                            }

                                            for (int i = 0; i <= 160; i++) {
                                                final double x10 = X2B + 0.5;
                                                final double y10 = world.getHighestBlockAt(X2B, Z2B).getY() + 0.5;
                                                final double z10 = Z2B + 0.5;

                                                if (X1B < X2B) {
                                                    final Location loc = new Location(world, X2B, world.getHighestBlockAt(X2B, Z2B).getY() - 1, Z2B);
                                                    if (loc.getBlock().getType().equals(Material.JUNGLE_LEAVES) || loc.getBlock().getType().equals(Material.OAK_LEAVES) || loc.getBlock().getType().equals(Material.SPRUCE_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LEAVES) || loc.getBlock().getType().equals(Material.BIRCH_LEAVES) || loc.getBlock().getType().equals(Material.DARK_OAK_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LOG) || loc.getBlock().getType().equals(Material.BIRCH_LOG) || loc.getBlock().getType().equals(Material.DARK_OAK_LOG) || loc.getBlock().getType().equals(Material.JUNGLE_LOG) || loc.getBlock().getType().equals(Material.OAK_LOG) || loc.getBlock().getType().equals(Material.SPRUCE_LOG) || loc.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                        boolean t = false;
                                                        for (int a = 1; a <= loc.getBlockY(); a++) {
                                                            final Location test = new Location(world, X2B, world.getHighestBlockAt(X2B, Z2B).getY() - a, Z2B);
                                                            final double x2 = test.getBlockX() + 0.5;
                                                            final double y2 = test.getBlockY() + 0.5;
                                                            final double z2 = test.getBlockZ() + 0.5;
                                                            final Location test2 = new Location(world, X2B, world.getHighestBlockAt(X2B, Z2B).getY() - a - 1, Z2B);
                                                            if (test.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.DARK_OAK_LEAVES) && !test2.getBlock().getType().equals(Material.BIRCH_LEAVES) && !test2.getBlock().getType().equals(Material.ACACIA_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LEAVES) && !test2.getBlock().getType().equals(Material.OAK_LEAVES) && !test2.getBlock().getType().equals(Material.JUNGLE_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LOG) && !test2.getBlock().getType().equals(Material.OAK_LOG) && !test2.getBlock().getType().equals(Material.JUNGLE_LOG) && !test2.getBlock().getType().equals(Material.DARK_OAK_LOG) && !test2.getBlock().getType().equals(Material.BIRCH_LOG) && !test2.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                                if (x2 >= X1ORIGINAL) {
                                                                    final Location location = new Location(player.getWorld(), x2, y2, z2);
                                                                    final DustOptions dust = new DustOptions(Color.RED, 2);
                                                                    loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                                    X2B--;
                                                                }
                                                            }
                                                            if (test.getBlock().getType().equals(Material.AIR)) {
                                                                t = true;
                                                            }
                                                        }
                                                        if (t == false) {
                                                            X2B--;
                                                        }
                                                    } else {
                                                        if (x10 >= X1ORIGINAL) {
                                                            final Location location = new Location(player.getWorld(), x10, y10, z10);
                                                            final Random random = new Random();
                                                            final DustOptions dust = new DustOptions(Color.fromBGR(random.nextInt(255), random.nextInt(255), random.nextInt(255)), 2);
                                                            loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                            X2B--;
                                                        }
                                                    }
                                                } else if (X1B > X2B) {
                                                    final Location loc = new Location(world, X2B, world.getHighestBlockAt(X2B, Z2B).getY() - 1, Z2B);
                                                    if (loc.getBlock().getType().equals(Material.JUNGLE_LEAVES) || loc.getBlock().getType().equals(Material.OAK_LEAVES) || loc.getBlock().getType().equals(Material.SPRUCE_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LEAVES) || loc.getBlock().getType().equals(Material.BIRCH_LEAVES) || loc.getBlock().getType().equals(Material.DARK_OAK_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LOG) || loc.getBlock().getType().equals(Material.BIRCH_LOG) || loc.getBlock().getType().equals(Material.DARK_OAK_LOG) || loc.getBlock().getType().equals(Material.JUNGLE_LOG) || loc.getBlock().getType().equals(Material.OAK_LOG) || loc.getBlock().getType().equals(Material.SPRUCE_LOG) || loc.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                        boolean t = false;
                                                        for (int a = 1; a <= loc.getBlockY(); a++) {
                                                            final Location test = new Location(world, X2B, world.getHighestBlockAt(X2B, Z2B).getY() - a, Z2B);
                                                            final double x2 = test.getBlockX() + 0.5;
                                                            final double y2 = test.getBlockY() + 0.5;
                                                            final double z2 = test.getBlockZ() + 0.5;
                                                            final Location test2 = new Location(world, X2B, world.getHighestBlockAt(X2B, Z2B).getY() - a - 1, Z2B);
                                                            if (test.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.DARK_OAK_LEAVES) && !test2.getBlock().getType().equals(Material.BIRCH_LEAVES) && !test2.getBlock().getType().equals(Material.ACACIA_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LEAVES) && !test2.getBlock().getType().equals(Material.OAK_LEAVES) && !test2.getBlock().getType().equals(Material.JUNGLE_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LOG) && !test2.getBlock().getType().equals(Material.OAK_LOG) && !test2.getBlock().getType().equals(Material.JUNGLE_LOG) && !test2.getBlock().getType().equals(Material.DARK_OAK_LOG) && !test2.getBlock().getType().equals(Material.BIRCH_LOG) && !test2.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                                if (x2 <= X1ORIGINAL) {
                                                                    final Location location = new Location(player.getWorld(), x2, y2, z2);
                                                                    final DustOptions dust = new DustOptions(Color.RED, 2);
                                                                    loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                                    X2B++;
                                                                }
                                                            }
                                                            if (test.getBlock().getType().equals(Material.AIR)) {
                                                                t = true;
                                                            }
                                                        }
                                                        if (t == false) {
                                                            X2B++;
                                                        }
                                                    } else {
                                                        if (x10 <= X1ORIGINAL) {
                                                            final Location location = new Location(player.getWorld(), x10, y10, z10);
                                                            final Random random = new Random();
                                                            final DustOptions dust = new DustOptions(Color.fromBGR(random.nextInt(255), random.nextInt(255), random.nextInt(255)), 2);
                                                            loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                            X2B++;
                                                        }
                                                    }
                                                }
                                            }

                                            for (int i = 0; i <= 160; i++) {
                                                final double x = X1A + 0.5;
                                                final double y = world.getHighestBlockAt(X1A, Z1A).getY() + 0.5;
                                                final double z = Z1A + 0.5;

                                                if (X1A > X2A) {
                                                    final Location loc = new Location(world, X1A, world.getHighestBlockAt(X1A, Z1A).getY() - 1, Z1A);
                                                    if (loc.getBlock().getType().equals(Material.JUNGLE_LEAVES) || loc.getBlock().getType().equals(Material.OAK_LEAVES) || loc.getBlock().getType().equals(Material.SPRUCE_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LEAVES) || loc.getBlock().getType().equals(Material.BIRCH_LEAVES) || loc.getBlock().getType().equals(Material.DARK_OAK_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LOG) || loc.getBlock().getType().equals(Material.BIRCH_LOG) || loc.getBlock().getType().equals(Material.DARK_OAK_LOG) || loc.getBlock().getType().equals(Material.JUNGLE_LOG) || loc.getBlock().getType().equals(Material.OAK_LOG) || loc.getBlock().getType().equals(Material.SPRUCE_LOG) || loc.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                        boolean t = false;
                                                        for (int a = 1; a <= loc.getBlockY(); a++) {
                                                            final Location test = new Location(world, X1A, world.getHighestBlockAt(X1A, Z1A).getY() - a, Z1A);
                                                            final double x2 = test.getBlockX() + 0.5;
                                                            final double y2 = test.getBlockY() + 0.5;
                                                            final double z2 = test.getBlockZ() + 0.5;
                                                            final Location test2 = new Location(world, X1A, world.getHighestBlockAt(X1A, Z1A).getY() - a - 1, Z1A);
                                                            if (test.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.DARK_OAK_LEAVES) && !test2.getBlock().getType().equals(Material.BIRCH_LEAVES) && !test2.getBlock().getType().equals(Material.ACACIA_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LEAVES) && !test2.getBlock().getType().equals(Material.OAK_LEAVES) && !test2.getBlock().getType().equals(Material.JUNGLE_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LOG) && !test2.getBlock().getType().equals(Material.OAK_LOG) && !test2.getBlock().getType().equals(Material.JUNGLE_LOG) && !test2.getBlock().getType().equals(Material.DARK_OAK_LOG) && !test2.getBlock().getType().equals(Material.BIRCH_LOG) && !test2.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                                if (x2 >= X2ORIGINAL) {
                                                                    final Location location = new Location(player.getWorld(), x2, y2, z2);
                                                                    final DustOptions dust = new DustOptions(Color.RED, 2);
                                                                    loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                                    X1A--;
                                                                }
                                                            }
                                                            if (test.getBlock().getType().equals(Material.AIR)) {
                                                                t = true;
                                                            }
                                                        }
                                                        if (t == false) {
                                                            X1A--;
                                                        }
                                                    } else {
                                                        if (x >= X2ORIGINAL) {
                                                            final Location location = new Location(player.getWorld(), x, y, z);
                                                            final Random random = new Random();
                                                            final DustOptions dust = new DustOptions(Color.fromBGR(random.nextInt(255), random.nextInt(255), random.nextInt(255)), 2);
                                                            loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                            X1A--;
                                                        }
                                                    }
                                                } else if (X1A < X2A) {
                                                    final Location loc = new Location(world, X1A, world.getHighestBlockAt(X1A, Z1A).getY() - 1, Z1A);
                                                    if (loc.getBlock().getType().equals(Material.JUNGLE_LEAVES) || loc.getBlock().getType().equals(Material.OAK_LEAVES) || loc.getBlock().getType().equals(Material.SPRUCE_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LEAVES) || loc.getBlock().getType().equals(Material.BIRCH_LEAVES) || loc.getBlock().getType().equals(Material.DARK_OAK_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LOG) || loc.getBlock().getType().equals(Material.BIRCH_LOG) || loc.getBlock().getType().equals(Material.DARK_OAK_LOG) || loc.getBlock().getType().equals(Material.JUNGLE_LOG) || loc.getBlock().getType().equals(Material.OAK_LOG) || loc.getBlock().getType().equals(Material.SPRUCE_LOG) || loc.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                        boolean t = false;
                                                        for (int a = 1; a <= loc.getBlockY(); a++) {
                                                            final Location test = new Location(world, X1A, world.getHighestBlockAt(X1A, Z1A).getY() - a, Z1A);
                                                            final double x2 = test.getBlockX() + 0.5;
                                                            final double y2 = test.getBlockY() + 0.5;
                                                            final double z2 = test.getBlockZ() + 0.5;
                                                            final Location test2 = new Location(world, X1A, world.getHighestBlockAt(X1A, Z1A).getY() - a - 1, Z1A);
                                                            if (test.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.DARK_OAK_LEAVES) && !test2.getBlock().getType().equals(Material.BIRCH_LEAVES) && !test2.getBlock().getType().equals(Material.ACACIA_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LEAVES) && !test2.getBlock().getType().equals(Material.OAK_LEAVES) && !test2.getBlock().getType().equals(Material.JUNGLE_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LOG) && !test2.getBlock().getType().equals(Material.OAK_LOG) && !test2.getBlock().getType().equals(Material.JUNGLE_LOG) && !test2.getBlock().getType().equals(Material.DARK_OAK_LOG) && !test2.getBlock().getType().equals(Material.BIRCH_LOG) && !test2.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                                if (x2 <= X2ORIGINAL) {
                                                                    final Location location = new Location(player.getWorld(), x2, y2, z2);
                                                                    final DustOptions dust = new DustOptions(Color.RED, 2);
                                                                    loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                                    X1A++;
                                                                }
                                                            }
                                                            if (test.getBlock().getType().equals(Material.AIR)) {
                                                                t = true;
                                                            }
                                                        }
                                                        if (t == false) {
                                                            X1A++;
                                                        }
                                                    } else {
                                                        if (x <= X2ORIGINAL) {
                                                            final Location location = new Location(player.getWorld(), x, y, z);
                                                            final Random random = new Random();
                                                            final DustOptions dust = new DustOptions(Color.fromBGR(random.nextInt(255), random.nextInt(255), random.nextInt(255)), 2);
                                                            loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                            X1A++;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }, 0L, 10L);

                                    //3
                                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                        @Override
                                        public void run() {
                                            if (CommandsPlots.this.status == true) {
                                                CommandsPlots.this.status = false;
                                                Bukkit.getScheduler().cancelTask(rotacao1);
                                            }
                                        }
                                    }, 205L);
                                } else if (TerrainIsDemarcate.get(UUID).getStatus() == false) {
                                    TerrainIsDemarcate.get(UUID).setStatus(true);
                                    player.sendMessage(ChatColor.GREEN + " * Terreno demarcado por 10 segundos!");
                                    player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                                    this.status = false;
                                    //4
                                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                        @Override
                                        public void run() {
                                            CommandsPlots.this.status = true;
                                            TerrainIsDemarcate.get(UUID).setStatus(false);
                                        }
                                    }, 200L);

                                    //5
                                    final int rotacao2 = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                                        @Override
                                        public void run() {
                                            final String[] pos1 = TerrainAPI.getTerrainPos1(UUID + "1").split(":");
                                            final String[] pos2 = TerrainAPI.getTerrainPos2(UUID + "1").split(":");
                                            final World world = Bukkit.getWorld(pos1[0]);

                                            final int X1ORIGINAL = Integer.valueOf(pos1[1]);
                                            final int Z1ORIGINAL = Integer.valueOf(pos1[3]);
                                            final int X2ORIGINAL = Integer.valueOf(pos2[1]);
                                            final int Z2ORIGINAL = Integer.valueOf(pos2[3]);

                                            int X1A = Integer.valueOf(pos1[1]);
                                            final int Z1A = Integer.valueOf(pos1[3]);
                                            final int X2A = Integer.valueOf(pos2[1]);
                                            final int Z2A = Integer.valueOf(pos2[3]);

                                            final int X1C = Integer.valueOf(pos1[1]);
                                            int Z1C = Integer.valueOf(pos1[3]);
                                            final int X2C = Integer.valueOf(pos2[1]);
                                            final int Z2C = Integer.valueOf(pos2[3]);

                                            final int X1B = Integer.valueOf(pos1[1]);
                                            final int Z1B = Integer.valueOf(pos1[3]);
                                            int X2B = Integer.valueOf(pos2[1]);
                                            final int Z2B = Integer.valueOf(pos2[3]);

                                            final int X1D = Integer.valueOf(pos1[1]);
                                            final int Z1D = Integer.valueOf(pos1[3]);
                                            final int X2D = Integer.valueOf(pos2[1]);
                                            int Z2D = Integer.valueOf(pos2[3]);

                                            for (int i = 0; i <= 160; i++) {
                                                final double x12 = X2D + 0.5;
                                                final double y12 = world.getHighestBlockAt(X2D, Z2D).getY() + 0.5;
                                                final double z12 = Z2D + 0.5;

                                                if (Z1D < Z2D) {
                                                    final Location loc = new Location(world, X2D, world.getHighestBlockAt(X2D, Z2D).getY() - 1, Z2D);
                                                    if (loc.getBlock().getType().equals(Material.JUNGLE_LEAVES) || loc.getBlock().getType().equals(Material.OAK_LEAVES) || loc.getBlock().getType().equals(Material.SPRUCE_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LEAVES) || loc.getBlock().getType().equals(Material.BIRCH_LEAVES) || loc.getBlock().getType().equals(Material.DARK_OAK_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LOG) || loc.getBlock().getType().equals(Material.BIRCH_LOG) || loc.getBlock().getType().equals(Material.DARK_OAK_LOG) || loc.getBlock().getType().equals(Material.JUNGLE_LOG) || loc.getBlock().getType().equals(Material.OAK_LOG) || loc.getBlock().getType().equals(Material.SPRUCE_LOG) || loc.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                        boolean t = false;
                                                        for (int a = 1; a <= loc.getBlockY(); a++) {
                                                            final Location test = new Location(world, X2D, world.getHighestBlockAt(X2D, Z2D).getY() - a, Z2D);
                                                            final double x2 = test.getBlockX() + 0.5;
                                                            final double y2 = test.getBlockY() + 0.5;
                                                            final double z2 = test.getBlockZ() + 0.5;
                                                            final Location test2 = new Location(world, X2D, world.getHighestBlockAt(X2D, Z2D).getY() - a - 1, Z2D);
                                                            if (test.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.DARK_OAK_LEAVES) && !test2.getBlock().getType().equals(Material.BIRCH_LEAVES) && !test2.getBlock().getType().equals(Material.ACACIA_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LEAVES) && !test2.getBlock().getType().equals(Material.OAK_LEAVES) && !test2.getBlock().getType().equals(Material.JUNGLE_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LOG) && !test2.getBlock().getType().equals(Material.OAK_LOG) && !test2.getBlock().getType().equals(Material.JUNGLE_LOG) && !test2.getBlock().getType().equals(Material.DARK_OAK_LOG) && !test2.getBlock().getType().equals(Material.BIRCH_LOG) && !test2.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                                if (z2 >= Z1ORIGINAL) {
                                                                    final Location location = new Location(player.getWorld(), x2, y2, z2);
                                                                    final DustOptions dust = new DustOptions(Color.RED, 2);
                                                                    loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                                    Z2D--;
                                                                }
                                                            }
                                                            if (test.getBlock().getType().equals(Material.AIR)) {
                                                                t = true;
                                                            }
                                                        }
                                                        if (t == false) {
                                                            Z2D--;
                                                        }
                                                    } else {
                                                        if (z12 >= Z2ORIGINAL) {
                                                            final Location location = new Location(player.getWorld(), x12, y12, z12);
                                                            final Random random = new Random();
                                                            final DustOptions dust = new DustOptions(Color.fromBGR(random.nextInt(255), random.nextInt(255), random.nextInt(255)), 2);
                                                            loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                            Z2D--;
                                                        }
                                                    }
                                                } else if (Z1D > Z2D) {
                                                    final Location loc = new Location(world, X2D, world.getHighestBlockAt(X2D, Z2D).getY() - 1, Z2D);
                                                    if (loc.getBlock().getType().equals(Material.JUNGLE_LEAVES) || loc.getBlock().getType().equals(Material.OAK_LEAVES) || loc.getBlock().getType().equals(Material.SPRUCE_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LEAVES) || loc.getBlock().getType().equals(Material.BIRCH_LEAVES) || loc.getBlock().getType().equals(Material.DARK_OAK_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LOG) || loc.getBlock().getType().equals(Material.BIRCH_LOG) || loc.getBlock().getType().equals(Material.DARK_OAK_LOG) || loc.getBlock().getType().equals(Material.JUNGLE_LOG) || loc.getBlock().getType().equals(Material.OAK_LOG) || loc.getBlock().getType().equals(Material.SPRUCE_LOG) || loc.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                        boolean t = false;
                                                        for (int a = 1; a <= loc.getBlockY(); a++) {
                                                            final Location test = new Location(world, X2D, world.getHighestBlockAt(X2D, Z2D).getY() - a, Z2D);
                                                            final double x2 = test.getBlockX() + 0.5;
                                                            final double y2 = test.getBlockY() + 0.5;
                                                            final double z2 = test.getBlockZ() + 0.5;
                                                            final Location test2 = new Location(world, X2D, world.getHighestBlockAt(X2D, Z2D).getY() - a - 1, Z2D);
                                                            if (test.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.DARK_OAK_LEAVES) && !test2.getBlock().getType().equals(Material.BIRCH_LEAVES) && !test2.getBlock().getType().equals(Material.ACACIA_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LEAVES) && !test2.getBlock().getType().equals(Material.OAK_LEAVES) && !test2.getBlock().getType().equals(Material.JUNGLE_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LOG) && !test2.getBlock().getType().equals(Material.OAK_LOG) && !test2.getBlock().getType().equals(Material.JUNGLE_LOG) && !test2.getBlock().getType().equals(Material.DARK_OAK_LOG) && !test2.getBlock().getType().equals(Material.BIRCH_LOG) && !test2.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                                if (z2 <= Z1ORIGINAL) {
                                                                    final Location location = new Location(player.getWorld(), x2, y2, z2);
                                                                    final DustOptions dust = new DustOptions(Color.RED, 2);
                                                                    loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                                    Z2D++;
                                                                }
                                                            }
                                                            if (test.getBlock().getType().equals(Material.AIR)) {
                                                                t = true;
                                                            }
                                                        }
                                                        if (t == false) {
                                                            Z2D++;
                                                        }
                                                    } else {
                                                        if (z12 <= Z1ORIGINAL) {
                                                            final Location location = new Location(player.getWorld(), x12, y12, z12);
                                                            final Random random = new Random();
                                                            final DustOptions dust = new DustOptions(Color.fromBGR(random.nextInt(255), random.nextInt(255), random.nextInt(255)), 2);
                                                            loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                            Z2D++;
                                                        }
                                                    }
                                                }
                                            }

                                            for (int i = 0; i <= 160; i++) {
                                                final double x11 = X1C + 0.5;
                                                final double y11 = world.getHighestBlockAt(X1C, Z1C).getY() + 0.5;
                                                final double z11 = Z1C + 0.5;

                                                if (Z1C > Z2C) {
                                                    final Location loc = new Location(world, X1C, world.getHighestBlockAt(X1C, Z1C).getY() - 1, Z1C);
                                                    if (loc.getBlock().getType().equals(Material.JUNGLE_LEAVES) || loc.getBlock().getType().equals(Material.OAK_LEAVES) || loc.getBlock().getType().equals(Material.SPRUCE_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LEAVES) || loc.getBlock().getType().equals(Material.BIRCH_LEAVES) || loc.getBlock().getType().equals(Material.DARK_OAK_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LOG) || loc.getBlock().getType().equals(Material.BIRCH_LOG) || loc.getBlock().getType().equals(Material.DARK_OAK_LOG) || loc.getBlock().getType().equals(Material.JUNGLE_LOG) || loc.getBlock().getType().equals(Material.OAK_LOG) || loc.getBlock().getType().equals(Material.SPRUCE_LOG) || loc.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                        boolean t = false;
                                                        for (int a = 1; a <= loc.getBlockY(); a++) {
                                                            final Location test = new Location(world, X1C, world.getHighestBlockAt(X1C, Z1C).getY() - a, Z1C);
                                                            final double x2 = test.getBlockX() + 0.5;
                                                            final double y2 = test.getBlockY() + 0.5;
                                                            final double z2 = test.getBlockZ() + 0.5;
                                                            final Location test2 = new Location(world, X1C, world.getHighestBlockAt(X1C, Z1C).getY() - a - 1, Z1C);
                                                            if (test.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.DARK_OAK_LEAVES) && !test2.getBlock().getType().equals(Material.BIRCH_LEAVES) && !test2.getBlock().getType().equals(Material.ACACIA_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LEAVES) && !test2.getBlock().getType().equals(Material.OAK_LEAVES) && !test2.getBlock().getType().equals(Material.JUNGLE_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LOG) && !test2.getBlock().getType().equals(Material.OAK_LOG) && !test2.getBlock().getType().equals(Material.JUNGLE_LOG) && !test2.getBlock().getType().equals(Material.DARK_OAK_LOG) && !test2.getBlock().getType().equals(Material.BIRCH_LOG) && !test2.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                                if (z2 >= Z2ORIGINAL) {
                                                                    final Location location = new Location(player.getWorld(), x2, y2, z2);
                                                                    final DustOptions dust = new DustOptions(Color.RED, 2);
                                                                    loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                                    Z1C--;
                                                                }
                                                            }
                                                            if (test.getBlock().getType().equals(Material.AIR)) {
                                                                t = true;
                                                            }
                                                        }
                                                        if (t == false) {
                                                            Z1C--;
                                                        }
                                                    } else {
                                                        if (z11 >= Z2ORIGINAL) {
                                                            final Location location = new Location(player.getWorld(), x11, y11, z11);
                                                            final Random random = new Random();
                                                            final DustOptions dust = new DustOptions(Color.fromBGR(random.nextInt(255), random.nextInt(255), random.nextInt(255)), 2);
                                                            loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                            Z1C--;
                                                        }
                                                    }
                                                } else if (Z1C < Z2C) {
                                                    final Location loc = new Location(world, X1C, world.getHighestBlockAt(X1C, Z1C).getY() - 1, Z1C);
                                                    if (loc.getBlock().getType().equals(Material.JUNGLE_LEAVES) || loc.getBlock().getType().equals(Material.OAK_LEAVES) || loc.getBlock().getType().equals(Material.SPRUCE_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LEAVES) || loc.getBlock().getType().equals(Material.BIRCH_LEAVES) || loc.getBlock().getType().equals(Material.DARK_OAK_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LOG) || loc.getBlock().getType().equals(Material.BIRCH_LOG) || loc.getBlock().getType().equals(Material.DARK_OAK_LOG) || loc.getBlock().getType().equals(Material.JUNGLE_LOG) || loc.getBlock().getType().equals(Material.OAK_LOG) || loc.getBlock().getType().equals(Material.SPRUCE_LOG) || loc.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                        boolean t = false;
                                                        for (int a = 1; a <= loc.getBlockY(); a++) {
                                                            final Location test = new Location(world, X1C, world.getHighestBlockAt(X1C, Z1C).getY() - a, Z1C);
                                                            final double x2 = test.getBlockX() + 0.5;
                                                            final double y2 = test.getBlockY() + 0.5;
                                                            final double z2 = test.getBlockZ() + 0.5;
                                                            final Location test2 = new Location(world, X1C, world.getHighestBlockAt(X1C, Z1C).getY() - a - 1, Z1C);
                                                            if (test.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.DARK_OAK_LEAVES) && !test2.getBlock().getType().equals(Material.BIRCH_LEAVES) && !test2.getBlock().getType().equals(Material.ACACIA_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LEAVES) && !test2.getBlock().getType().equals(Material.OAK_LEAVES) && !test2.getBlock().getType().equals(Material.JUNGLE_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LOG) && !test2.getBlock().getType().equals(Material.OAK_LOG) && !test2.getBlock().getType().equals(Material.JUNGLE_LOG) && !test2.getBlock().getType().equals(Material.DARK_OAK_LOG) && !test2.getBlock().getType().equals(Material.BIRCH_LOG) && !test2.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                                if (z2 <= Z2ORIGINAL) {
                                                                    final Location location = new Location(player.getWorld(), x2, y2, z2);
                                                                    final DustOptions dust = new DustOptions(Color.RED, 2);
                                                                    loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                                    Z1C++;
                                                                }
                                                            }
                                                            if (test.getBlock().getType().equals(Material.AIR)) {
                                                                t = true;
                                                            }
                                                        }
                                                        if (t == false) {
                                                            Z1C++;
                                                        }
                                                    } else {
                                                        if (z11 <= Z2ORIGINAL) {
                                                            final Location location = new Location(player.getWorld(), x11, y11, z11);
                                                            final Random random = new Random();
                                                            final DustOptions dust = new DustOptions(Color.fromBGR(random.nextInt(255), random.nextInt(255), random.nextInt(255)), 2);
                                                            loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                            Z1C++;
                                                        }
                                                    }
                                                }
                                            }

                                            for (int i = 0; i <= 160; i++) {
                                                final double x10 = X2B + 0.5;
                                                final double y10 = world.getHighestBlockAt(X2B, Z2B).getY() + 0.5;
                                                final double z10 = Z2B + 0.5;

                                                if (X1B < X2B) {
                                                    final Location loc = new Location(world, X2B, world.getHighestBlockAt(X2B, Z2B).getY() - 1, Z2B);
                                                    if (loc.getBlock().getType().equals(Material.JUNGLE_LEAVES) || loc.getBlock().getType().equals(Material.OAK_LEAVES) || loc.getBlock().getType().equals(Material.SPRUCE_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LEAVES) || loc.getBlock().getType().equals(Material.BIRCH_LEAVES) || loc.getBlock().getType().equals(Material.DARK_OAK_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LOG) || loc.getBlock().getType().equals(Material.BIRCH_LOG) || loc.getBlock().getType().equals(Material.DARK_OAK_LOG) || loc.getBlock().getType().equals(Material.JUNGLE_LOG) || loc.getBlock().getType().equals(Material.OAK_LOG) || loc.getBlock().getType().equals(Material.SPRUCE_LOG) || loc.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                        boolean t = false;
                                                        for (int a = 1; a <= loc.getBlockY(); a++) {
                                                            final Location test = new Location(world, X2B, world.getHighestBlockAt(X2B, Z2B).getY() - a, Z2B);
                                                            final double x2 = test.getBlockX() + 0.5;
                                                            final double y2 = test.getBlockY() + 0.5;
                                                            final double z2 = test.getBlockZ() + 0.5;
                                                            final Location test2 = new Location(world, X2B, world.getHighestBlockAt(X2B, Z2B).getY() - a - 1, Z2B);
                                                            if (test.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.DARK_OAK_LEAVES) && !test2.getBlock().getType().equals(Material.BIRCH_LEAVES) && !test2.getBlock().getType().equals(Material.ACACIA_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LEAVES) && !test2.getBlock().getType().equals(Material.OAK_LEAVES) && !test2.getBlock().getType().equals(Material.JUNGLE_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LOG) && !test2.getBlock().getType().equals(Material.OAK_LOG) && !test2.getBlock().getType().equals(Material.JUNGLE_LOG) && !test2.getBlock().getType().equals(Material.DARK_OAK_LOG) && !test2.getBlock().getType().equals(Material.BIRCH_LOG) && !test2.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                                if (x2 >= X1ORIGINAL) {
                                                                    final Location location = new Location(player.getWorld(), x2, y2, z2);
                                                                    final DustOptions dust = new DustOptions(Color.RED, 2);
                                                                    loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                                    X2B--;
                                                                }
                                                            }
                                                            if (test.getBlock().getType().equals(Material.AIR)) {
                                                                t = true;
                                                            }
                                                        }
                                                        if (t == false) {
                                                            X2B--;
                                                        }
                                                    } else {
                                                        if (x10 >= X1ORIGINAL) {
                                                            final Location location = new Location(player.getWorld(), x10, y10, z10);
                                                            final Random random = new Random();
                                                            final DustOptions dust = new DustOptions(Color.fromBGR(random.nextInt(255), random.nextInt(255), random.nextInt(255)), 2);
                                                            loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                            X2B--;
                                                        }
                                                    }
                                                } else if (X1B > X2B) {
                                                    final Location loc = new Location(world, X2B, world.getHighestBlockAt(X2B, Z2B).getY() - 1, Z2B);
                                                    if (loc.getBlock().getType().equals(Material.JUNGLE_LEAVES) || loc.getBlock().getType().equals(Material.OAK_LEAVES) || loc.getBlock().getType().equals(Material.SPRUCE_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LEAVES) || loc.getBlock().getType().equals(Material.BIRCH_LEAVES) || loc.getBlock().getType().equals(Material.DARK_OAK_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LOG) || loc.getBlock().getType().equals(Material.BIRCH_LOG) || loc.getBlock().getType().equals(Material.DARK_OAK_LOG) || loc.getBlock().getType().equals(Material.JUNGLE_LOG) || loc.getBlock().getType().equals(Material.OAK_LOG) || loc.getBlock().getType().equals(Material.SPRUCE_LOG) || loc.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                        boolean t = false;
                                                        for (int a = 1; a <= loc.getBlockY(); a++) {
                                                            final Location test = new Location(world, X2B, world.getHighestBlockAt(X2B, Z2B).getY() - a, Z2B);
                                                            final double x2 = test.getBlockX() + 0.5;
                                                            final double y2 = test.getBlockY() + 0.5;
                                                            final double z2 = test.getBlockZ() + 0.5;
                                                            final Location test2 = new Location(world, X2B, world.getHighestBlockAt(X2B, Z2B).getY() - a - 1, Z2B);
                                                            if (test.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.DARK_OAK_LEAVES) && !test2.getBlock().getType().equals(Material.BIRCH_LEAVES) && !test2.getBlock().getType().equals(Material.ACACIA_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LEAVES) && !test2.getBlock().getType().equals(Material.OAK_LEAVES) && !test2.getBlock().getType().equals(Material.JUNGLE_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LOG) && !test2.getBlock().getType().equals(Material.OAK_LOG) && !test2.getBlock().getType().equals(Material.JUNGLE_LOG) && !test2.getBlock().getType().equals(Material.DARK_OAK_LOG) && !test2.getBlock().getType().equals(Material.BIRCH_LOG) && !test2.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                                if (x2 <= X1ORIGINAL) {
                                                                    final Location location = new Location(player.getWorld(), x2, y2, z2);
                                                                    final DustOptions dust = new DustOptions(Color.RED, 2);
                                                                    loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                                    X2B++;
                                                                }
                                                            }
                                                            if (test.getBlock().getType().equals(Material.AIR)) {
                                                                t = true;
                                                            }
                                                        }
                                                        if (t == false) {
                                                            X2B++;
                                                        }
                                                    } else {
                                                        if (x10 <= X1ORIGINAL) {
                                                            final Location location = new Location(player.getWorld(), x10, y10, z10);
                                                            final Random random = new Random();
                                                            final DustOptions dust = new DustOptions(Color.fromBGR(random.nextInt(255), random.nextInt(255), random.nextInt(255)), 2);
                                                            loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                            X2B++;
                                                        }
                                                    }
                                                }
                                            }

                                            for (int i = 0; i <= 160; i++) {
                                                final double x = X1A + 0.5;
                                                final double y = world.getHighestBlockAt(X1A, Z1A).getY() + 0.5;
                                                final double z = Z1A + 0.5;

                                                if (X1A > X2A) {
                                                    final Location loc = new Location(world, X1A, world.getHighestBlockAt(X1A, Z1A).getY() - 1, Z1A);
                                                    if (loc.getBlock().getType().equals(Material.JUNGLE_LEAVES) || loc.getBlock().getType().equals(Material.OAK_LEAVES) || loc.getBlock().getType().equals(Material.SPRUCE_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LEAVES) || loc.getBlock().getType().equals(Material.BIRCH_LEAVES) || loc.getBlock().getType().equals(Material.DARK_OAK_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LOG) || loc.getBlock().getType().equals(Material.BIRCH_LOG) || loc.getBlock().getType().equals(Material.DARK_OAK_LOG) || loc.getBlock().getType().equals(Material.JUNGLE_LOG) || loc.getBlock().getType().equals(Material.OAK_LOG) || loc.getBlock().getType().equals(Material.SPRUCE_LOG) || loc.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                        boolean t = false;
                                                        for (int a = 1; a <= loc.getBlockY(); a++) {
                                                            final Location test = new Location(world, X1A, world.getHighestBlockAt(X1A, Z1A).getY() - a, Z1A);
                                                            final double x2 = test.getBlockX() + 0.5;
                                                            final double y2 = test.getBlockY() + 0.5;
                                                            final double z2 = test.getBlockZ() + 0.5;
                                                            final Location test2 = new Location(world, X1A, world.getHighestBlockAt(X1A, Z1A).getY() - a - 1, Z1A);
                                                            if (test.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.DARK_OAK_LEAVES) && !test2.getBlock().getType().equals(Material.BIRCH_LEAVES) && !test2.getBlock().getType().equals(Material.ACACIA_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LEAVES) && !test2.getBlock().getType().equals(Material.OAK_LEAVES) && !test2.getBlock().getType().equals(Material.JUNGLE_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LOG) && !test2.getBlock().getType().equals(Material.OAK_LOG) && !test2.getBlock().getType().equals(Material.JUNGLE_LOG) && !test2.getBlock().getType().equals(Material.DARK_OAK_LOG) && !test2.getBlock().getType().equals(Material.BIRCH_LOG) && !test2.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                                if (x2 >= X2ORIGINAL) {
                                                                    final Location location = new Location(player.getWorld(), x2, y2, z2);
                                                                    final DustOptions dust = new DustOptions(Color.RED, 2);
                                                                    loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                                    X1A--;
                                                                }
                                                            }
                                                            if (test.getBlock().getType().equals(Material.AIR)) {
                                                                t = true;
                                                            }
                                                        }
                                                        if (t == false) {
                                                            X1A--;
                                                        }
                                                    } else {
                                                        if (x >= X2ORIGINAL) {
                                                            final Location location = new Location(player.getWorld(), x, y, z);
                                                            final Random random = new Random();
                                                            final DustOptions dust = new DustOptions(Color.fromBGR(random.nextInt(255), random.nextInt(255), random.nextInt(255)), 2);
                                                            loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                            X1A--;
                                                        }
                                                    }
                                                } else if (X1A < X2A) {
                                                    final Location loc = new Location(world, X1A, world.getHighestBlockAt(X1A, Z1A).getY() - 1, Z1A);
                                                    if (loc.getBlock().getType().equals(Material.JUNGLE_LEAVES) || loc.getBlock().getType().equals(Material.OAK_LEAVES) || loc.getBlock().getType().equals(Material.SPRUCE_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LEAVES) || loc.getBlock().getType().equals(Material.BIRCH_LEAVES) || loc.getBlock().getType().equals(Material.DARK_OAK_LEAVES) || loc.getBlock().getType().equals(Material.ACACIA_LOG) || loc.getBlock().getType().equals(Material.BIRCH_LOG) || loc.getBlock().getType().equals(Material.DARK_OAK_LOG) || loc.getBlock().getType().equals(Material.JUNGLE_LOG) || loc.getBlock().getType().equals(Material.OAK_LOG) || loc.getBlock().getType().equals(Material.SPRUCE_LOG) || loc.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                        boolean t = false;
                                                        for (int a = 1; a <= loc.getBlockY(); a++) {
                                                            final Location test = new Location(world, X1A, world.getHighestBlockAt(X1A, Z1A).getY() - a, Z1A);
                                                            final double x2 = test.getBlockX() + 0.5;
                                                            final double y2 = test.getBlockY() + 0.5;
                                                            final double z2 = test.getBlockZ() + 0.5;
                                                            final Location test2 = new Location(world, X1A, world.getHighestBlockAt(X1A, Z1A).getY() - a - 1, Z1A);
                                                            if (test.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.AIR) && !test2.getBlock().getType().equals(Material.DARK_OAK_LEAVES) && !test2.getBlock().getType().equals(Material.BIRCH_LEAVES) && !test2.getBlock().getType().equals(Material.ACACIA_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LEAVES) && !test2.getBlock().getType().equals(Material.OAK_LEAVES) && !test2.getBlock().getType().equals(Material.JUNGLE_LEAVES) && !test2.getBlock().getType().equals(Material.SPRUCE_LOG) && !test2.getBlock().getType().equals(Material.OAK_LOG) && !test2.getBlock().getType().equals(Material.JUNGLE_LOG) && !test2.getBlock().getType().equals(Material.DARK_OAK_LOG) && !test2.getBlock().getType().equals(Material.BIRCH_LOG) && !test2.getBlock().getType().equals(Material.ACACIA_LOG)) {
                                                                if (x2 <= X2ORIGINAL) {
                                                                    final Location location = new Location(player.getWorld(), x2, y2, z2);
                                                                    final DustOptions dust = new DustOptions(Color.RED, 2);
                                                                    loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                                    X1A++;
                                                                }
                                                            }
                                                            if (test.getBlock().getType().equals(Material.AIR)) {
                                                                t = true;
                                                            }
                                                        }
                                                        if (t == false) {
                                                            X1A++;
                                                        }
                                                    } else {
                                                        if (x <= X2ORIGINAL) {
                                                            final Location location = new Location(player.getWorld(), x, y, z);
                                                            final Random random = new Random();
                                                            final DustOptions dust = new DustOptions(Color.fromBGR(random.nextInt(255), random.nextInt(255), random.nextInt(255)), 2);
                                                            loc.getWorld().spawnParticle(Particle.REDSTONE, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, dust);
                                                            X1A++;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }, 0L, 10L);

                                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                        @Override
                                        public void run() {
                                            if (CommandsPlots.this.status == true) {
                                                CommandsPlots.this.status = false;
                                                Bukkit.getScheduler().cancelTask(rotacao2);
                                            }
                                        }
                                    }, 205L);
                                } else {
                                    player.sendMessage(ChatColor.RED + "Ops, você já esta com área sendo demarcada!");
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + " * Ops, para efetuar este comando você deve conter um terreno!");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                            */
                        } else {
                            player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes á &nterreno&c:\n \n&8/terreno\n&8/terreno config\n&8/terreno demarcar\n \n"));
                        }
                    } else {
                        player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes á &nterreno&c:\n \n&8/terreno\n&8/terreno config\n&8/terreno demarcar\n \n"));
                    }
                }
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, para você utilizar este comando primeiramente selecione sua guilda.");
            }
        }

        if (cmd.getName().equalsIgnoreCase("ilha")) {
            if (GuildaAPI.getGuilda(UUID) != null) {
                if (GuildaAPI.getGuilda(UUID).equals("ancia")) {
                    if (args.length == 0) {
                        player.openInventory(InventoryTerrenos.getInvTerrain("ancia"));
                    } else if (args.length == 1) {
                        if (args[0].equals("config")) {
                            if (IlhaAPI.getIdIlha(UUID + "1") != 0) {
                                player.openInventory(InventoryTerrenos.configIlha(UUID + "1"));
                            } else {
                                player.sendMessage(ChatColor.RED + "Ops, você não contém nenhum terreno para efetuar este comando.");
                            }
                        } else {
                            player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes á &nilha&c:\n \n&8/ilha\n&8/ilha config\n \n"));
                        }
                    } else {
                        player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes á &nilha&c:\n \n&8/ilha\n&8/ilha config\n \n"));
                    }
                }
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, para você utilizar este comando primeiramente selecione sua guilda.");
            }
        }

        //if (cmd.getName().equalsIgnoreCase("espaçonave")) {
        //            if (GuildaAPI.getGuilda(UUID) != null) {
        //                if (GuildaAPI.getGuilda(UUID).equals("sanguinaria")) {
        //                    if (args.length == 0) {
        //                        player.openInventory(InventoryTerrenos.getInvTerrain("sanguinaria"));
        //                    } else if (args.length == 1) {
        //                        if (args[0].equals("config")) {
        //                            if (EspaçonaveAPI.getIdEspaçonave(UUID + String.valueOf("1")) != 0) {
        //                                player.openInventory(InventoryTerrenos.configEspaçonave(UUID + String.valueOf("1")));
        //                            } else {
        //                                player.sendMessage(ChatColor.RED + "Ops, você não contém nenhuma espaçonave para efetuar este comando.");
        //                            }
        //                        } else {
        //                            player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes á &nespaçonave&c:\n \n&8/espaçonave\n&8/espaçonave config\n \n"));
        //                        }
        //                    } else {
        //                        player.sendMessage(AlternateColor.alternate("\n \n&c * Comandos referentes á &nespaçonave&c:\n \n&8/espaçonave\n&8/espaçonave config\n \n"));
        //                    }
        //                }
        //            } else {
        //                player.sendMessage(ChatColor.RED + " * Ops, para você utilizar este comando primeiramente selecione sua guilda.");
        //            }
        //        }
        return false;
    }
}
