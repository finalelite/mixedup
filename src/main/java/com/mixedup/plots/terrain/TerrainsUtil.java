package com.mixedup.plots.terrain;

import com.mixedup.apis.RegionsSpawnAPI;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TerrainsUtil {

    public static RegionTr verify(final Location location, final int tamanho) {
        RegionTr tr = null;

        location.add(-(tamanho / 2), 0, -(tamanho / 2));

        int x = 0;
        for (int z = 0; z <= tamanho; z++) {

            if (TerrainsUtil.playerInArea(location) != null) {
                final String[] loc = TerrainsUtil.playerInArea(location).getPos1().split(":");
                if (location.getWorld().getName().equals(loc[0])) {
                    tr = TerrainsUtil.playerInArea(location);
                }
            }

            location.add(0, 0, 1);
            if (z == tamanho && x <= tamanho) {
                z = 0;
                x++;
                location.add(1, 0, -tamanho);
            } else if (x > tamanho) {
                break;
            }
        }
        return tr;
    }

    public static boolean verifySpawnNext(final Location playerLoc, final int tamanho) {
        if (playerLoc.getWorld().getName().equalsIgnoreCase("Kepler-438b")) {
            final String[] loc = RegionsSpawnAPI.getLocation("Kepler-438b").split(":");
            final Location center = new Location(Bukkit.getWorld("Kepler-438b"), Double.valueOf(loc[1]), Double.valueOf(loc[2]), Double.valueOf(loc[3]));
            return !(center.distance(playerLoc) > 750);
        } else if (playerLoc.getWorld().getName().equalsIgnoreCase("HD-85512-b")) {
            final String[] loc = RegionsSpawnAPI.getLocation("HD-85512-b").split(":");
            final Location center = new Location(Bukkit.getWorld("HD-85512-b"), Double.valueOf(loc[1]), Double.valueOf(loc[2]), Double.valueOf(loc[3]));
            return !(center.distance(playerLoc) > 750);
        } else if (playerLoc.getWorld().getName().equalsIgnoreCase("gliesse-667-cc")) {
            final String[] loc = RegionsSpawnAPI.getLocation("gliese-667-cc").split(":");
            final Location center = new Location(Bukkit.getWorld("Gliesse-667-cc"), Double.valueOf(loc[1]), Double.valueOf(loc[2]), Double.valueOf(loc[3]));
            return !(center.distance(playerLoc) > 750);
        } else if (playerLoc.getWorld().getName().equalsIgnoreCase("Koi-3010.01")) {
            final String[] loc = RegionsSpawnAPI.getLocation("Koi-3010.01").split(":");
            final Location center = new Location(Bukkit.getWorld("Koi-3010.01"), Double.valueOf(loc[1]), Double.valueOf(loc[2]), Double.valueOf(loc[3]));
            return !(center.distance(playerLoc) > 750);
        } else if (playerLoc.getWorld().getName().equalsIgnoreCase("skygrid")) {
            final String[] loc = RegionsSpawnAPI.getLocation("skygrid").split(":");
            final Location center = new Location(Bukkit.getWorld("skygrid"), Double.valueOf(loc[1]), Double.valueOf(loc[2]), Double.valueOf(loc[3]));
            return !(center.distance(playerLoc) > 750);
        }
        return true;
    }

    public static RegionTr playerInArea(final Location loc) {
        List<RegionTr> regionList = RegionsTr.getRegions();
        for (RegionTr rg : regionList) {

            final String[] pos1 = rg.getPos1().split(":");
            final String[] pos2 = rg.getPos2().split(":");

            if (pos1[0].equalsIgnoreCase(loc.getWorld().getName())) {
                final int p1x = Integer.valueOf(pos1[1]);
                final int p1y = Integer.valueOf(pos1[2]);
                final int p1z = Integer.valueOf(pos1[3]);
                final int p2x = Integer.valueOf(pos2[1]);
                final int p2y = Integer.valueOf(pos2[2]);
                final int p2z = Integer.valueOf(pos2[3]);

                final int minX = p1x < p2x ? p1x : p2x;
                final int minY = p1y < p2y ? p1y : p2y;
                final int minZ = p1z < p2z ? p1z : p2z;

                final int maxX = p1x > p2x ? p1x : p2x;
                final int maxY = p1y > p2y ? p1y : p2y;
                final int maxZ = p1z > p2z ? p1z : p2z;

                if ((loc.getBlockX() >= minX) && (loc.getBlockX() <= maxX) && (loc.getBlockY() >= minY)
                        && (loc.getBlockY() <= maxY) && (loc.getBlockZ() >= minZ) && (loc.getBlockZ() <= maxZ)) {

                    return rg;

                }
            }
        }
        return null;
    }
}
