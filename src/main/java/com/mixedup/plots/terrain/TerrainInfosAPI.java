package com.mixedup.plots.terrain;

import com.mixedup.MySql;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TerrainInfosAPI {

    public static void setInfos(final String UUID, final String moeda) {
        new TerrainInfosCache(UUID, 1, 1, 1, false, true, true, false, moeda, "NULL").insert();
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Terrain_infos(UUID, NivelDrops, NivelMobspawn, NivelGerador, PvP, Visitas, Teletransporte, StatusPvP, MoedaUtilizada, Amigos) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            st.setString(1, UUID);
            st.setInt(2, 1);
            st.setInt(3, 1);
            st.setInt(4, 1);
            st.setBoolean(5, false);
            st.setBoolean(6, true);
            st.setBoolean(7, true);
            st.setBoolean(8, false);
            st.setString(9, moeda);
            st.setString(10, "NULL");
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeTerrainInfo(final String UUID) {
        TerrainInfosCache.CACHE.remove(UUID);
        try {
            final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Terrain_infos WHERE UUID = ?");
            st.setString(1, UUID);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateAmigos(final String UUID, final String amigos) {
        TerrainInfosCache.get(UUID).setAmigos(amigos);
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Terrain_infos SET Amigos = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setString(1, amigos);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateVisitas(final String UUID, final boolean status) {
        TerrainInfosCache.get(UUID).setTeletransporte(status);
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Terrain_infos SET Teletransporte = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setBoolean(1, status);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateAcesso(final String UUID, final boolean status) {
        TerrainInfosCache.get(UUID).setVisita(status);
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Terrain_infos SET Visitas = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setBoolean(1, status);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updatePvPStatus(final String UUID, final boolean status) {
        TerrainInfosCache.get(UUID).setStatusPvP(status);
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Terrain_infos SET StatusPvP = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setBoolean(1, status);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updatePvP(final String UUID, final boolean status) {
        TerrainInfosCache.get(UUID).setPvP(status);
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Terrain_infos SET PvP = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setBoolean(1, status);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateNivelMobspawn(final String UUID) {
        TerrainInfosCache.get(UUID).setNivelMobspawn(TerrainInfosAPI.getNivelDrops(UUID) + 1);
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Terrain_infos SET NivelMobspawn = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setInt(1, TerrainInfosAPI.getNivelDrops(UUID) + 1);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateNivelDrops(final String UUID) {
        TerrainInfosCache.get(UUID).setNivelDrops(TerrainInfosAPI.getNivelDrops(UUID) + 1);
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Terrain_infos SET NivelDrops = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setInt(1, TerrainInfosAPI.getNivelDrops(UUID) + 1);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static int getNivelDrops(final String UUID) {
        if (TerrainInfosCache.get(UUID) != null) {
            return TerrainInfosCache.get(UUID).getNivelDrops();
        } else {
            return 1;
        }
        //try {
        //            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Terrain_infos WHERE UUID = ?");
        //            st.setString(1, UUID);
        //            ResultSet rs = st.executeQuery();
        //            while (rs.next()) {
        //                return rs.getInt("NivelDrops");
        //            }
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
        //        return 1;
    }

    public static int getNivelMobspawn(final String UUID) {
        if (TerrainInfosCache.get(UUID) != null) {
            return TerrainInfosCache.get(UUID).getNivelMobspawn();
        } else {
            return 1;
        }
        //try {
        //            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Terrain_infos WHERE UUID = ?");
        //            st.setString(1, UUID);
        //            ResultSet rs = st.executeQuery();
        //            while (rs.next()) {
        //                return rs.getInt("NivelMobspawn");
        //            }
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
        //        return 1;
    }

    public static int getNivelGerador(final String UUID) {
        if (TerrainInfosCache.get(UUID) != null) {
            return TerrainInfosCache.get(UUID).getNivelGerador();
        } else {
            return 1;
        }
        //try {
        //            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Terrain_infos WHERE UUID = ?");
        //            st.setString(1, UUID);
        //            ResultSet rs = st.executeQuery();
        //            while (rs.next()) {
        //                return rs.getInt("NivelGerador");
        //            }
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
        //        return 1;
    }

    public static boolean getPvP(final String UUID) {
        if (TerrainInfosCache.get(UUID) != null) {
            return TerrainInfosCache.get(UUID).getPvP();
        } else {
            return true;
        }
        //try {
        //            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Terrain_infos WHERE UUID = ?");
        //            st.setString(1, UUID);
        //            ResultSet rs = st.executeQuery();
        //            while (rs.next()) {
        //                return rs.getBoolean("PvP");
        //            }
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
        //        return true;
    }

    public static boolean getPvPStatus(final String UUID) {
        if (TerrainInfosCache.get(UUID) != null) {
            return TerrainInfosCache.get(UUID).getStatusPvP();
        } else {
            return true;
        }
        //try {
        //            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Terrain_infos WHERE UUID = ?");
        //            st.setString(1, UUID);
        //            ResultSet rs = st.executeQuery();
        //            while (rs.next()) {
        //                return rs.getBoolean("StatusPvP");
        //            }
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
        //        return true;
    }

    public static boolean getAcesso(final String UUID) {
        if (TerrainInfosCache.get(UUID) != null) {
            return TerrainInfosCache.get(UUID).getVisitas();
        } else {
            return true;
        }
        //try {
        //            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Terrain_infos WHERE UUID = ?");
        //            st.setString(1, UUID);
        //            ResultSet rs = st.executeQuery();
        //            while (rs.next()) {
        //                return rs.getBoolean("Visitas");
        //            }
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
        //        return true;
    }

    public static String getMoedaUtilizada(final String UUID) {
        if (TerrainInfosCache.get(UUID) != null) {
            return TerrainInfosCache.get(UUID).getMoedaUtilizada();
        } else {
            return null;
        }
        //  try {
        //            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Terrain_infos WHERE UUID = ?");
        //            st.setString(1, UUID);
        //            ResultSet rs = st.executeQuery();
        //            while (rs.next()) {
        //                return rs.getString("MoedaUtilizada");
        //            }
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
        //        return null;
    }

    public static boolean getTeletransporte(final String UUID) {
        if (TerrainInfosCache.get(UUID) != null) {
            return TerrainInfosCache.get(UUID).getTeletransporte();
        } else {
            return true;
        }
        //try {
        //            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Terrain_infos WHERE UUID = ?");
        //            st.setString(1, UUID);
        //            ResultSet rs = st.executeQuery();
        //            while (rs.next()) {
        //                return rs.getBoolean("Teletransporte");
        //            }
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
        //        return true;
    }

    public static String getAmigos(final String UUID) {
        if (TerrainInfosCache.get(UUID) != null) {
            return TerrainInfosCache.get(UUID).getAmigos();
        } else {
            return null;
        }
        //try {
        //            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Terrain_infos WHERE UUID = ?");
        //            st.setString(1, UUID);
        //            ResultSet rs = st.executeQuery();
        //            while (rs.next()) {
        //                return rs.getString("Amigos");
        //            }
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
        //        return null;
    }


    //---------------------------------------------------------------------------------------------------------


}
