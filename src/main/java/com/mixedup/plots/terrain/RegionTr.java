package com.mixedup.plots.terrain;

import java.util.HashMap;

public class RegionTr {

    public static HashMap<String, RegionTr> CACHE = new HashMap<String, RegionTr>();
    private String regionID;
    private String pos1;
    private String pos2;

    public RegionTr(final String regionID, final String pos1, final String pos2) {
        this.regionID = regionID;
        this.pos1 = pos1;
        this.pos2 = pos2;
    }

    public static RegionTr get(final String UUID) {
        return RegionTr.CACHE.get(UUID);
    }

    public RegionTr insert() {
        RegionTr.CACHE.put(regionID, this);

        return this;
    }

    public String getRegionID() {
        return this.regionID;
    }

    public void setRegionID(final String regionID) {
        this.regionID = regionID;
    }

    public String getPos1() {
        return this.pos1;
    }

    public void setPos1(final String pos1) {
        this.pos1 = pos1;
    }

    public String getPos2() {
        return this.pos2;
    }

    public void setPos2(final String pos2) {
        this.pos2 = pos2;
    }
}
