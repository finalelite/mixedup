package com.mixedup.plots.terrain;

import com.mixedup.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class TerrainCache {

    public static HashMap<String, TerrainCache> CACHE = new HashMap<String, TerrainCache>();
    private String UUID;
    private String Pos1;
    private String Pos2;

    public TerrainCache(final String UUID, final String Pos1, final String Pos2) {
        this.UUID = UUID;
        this.Pos1 = Pos1;
        this.Pos2 = Pos2;
    }

    public static TerrainCache get(final String UUID) {
        return TerrainCache.CACHE.get(String.valueOf(UUID));
    }

    public static void setCache() {
        for (int i = 1; i <= TerrainCache.getLastTerrain(); i++) {
            if (TerrainCache.getUUID(i) != null) {
                new TerrainCache(TerrainCache.getUUID(i), TerrainCache.getPos1(i), TerrainCache.getPos2(i)).insert();
            }
        }
    }

    public static int getLastTerrain() {
        int last = 0;
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Terrains_data ORDER BY ID");
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if (last < rs.getInt("ID") && rs.getInt("ID") != 0) {
                    last = rs.getInt("ID");
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return last;
    }

    public static String getUUID(final int ID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Terrains_data WHERE ID = ?");
            st.setInt(1, ID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("UUID");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getPos1(final int ID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Terrains_data WHERE ID = ?");
            st.setInt(1, ID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Pos1");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getPos2(final int ID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Terrains_data WHERE ID = ?");
            st.setInt(1, ID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Pos2");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public TerrainCache insert() {
        TerrainCache.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public String getPos1() {
        return Pos1;
    }

    public void setPos1(final String Pos1) {
        this.Pos1 = Pos1;
    }

    public String getPos2() {
        return Pos2;
    }

    public void setPos2(final String Pos2) {
        this.Pos2 = Pos2;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(final String UUID) {
        this.UUID = UUID;
    }
}
