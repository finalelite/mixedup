package com.mixedup.plots.terrain;

import com.mixedup.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RegionsTr {

    public static List<RegionTr> regions = new ArrayList<>();

    public static RegionTr getRegion(final String regionID) {
        for (final RegionTr region : RegionsTr.getRegions()) {
            if (region.getRegionID() == regionID) {
                return region;
            }
        }
        return null;
    }

    public static List<RegionTr> getRegions() {
        return regions;
    }

    public static void setRegions(final List<RegionTr> regions) {
        RegionsTr.regions = regions;
    }

    public static void enableRegions() {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Terrains_data");
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                final RegionTr region = new RegionTr(rs.getString("UUID"), rs.getString("Pos1"), rs.getString("Pos2"));
                getRegions().add(region);
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }
}
