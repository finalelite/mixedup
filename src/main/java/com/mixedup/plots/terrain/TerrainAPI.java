package com.mixedup.plots.terrain;

import com.mixedup.MySql;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TerrainAPI {

    public static void removeTerrain(final String UUID) {
        TerrainCache.CACHE.remove(UUID);
        try {
            final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Terrains_data WHERE UUID = ?");
            st.setString(1, UUID);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void setTerrain(final String UUID, final String pos1, final String pos2) {
        //if (TerrainCache.get(UUID) != null) {
        //            if (TerrainCache.get(UUID).getPos1().equalsIgnoreCase("NULL")) {
        //                TerrainCache.get(UUID).setPos1(pos1);
        //                TerrainCache.get(UUID).setPos2(pos2);
        //                try {
        //                    PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Terrains_data(UUID, Pos1, Pos2) VALUES (?, ?, ?)");
        //                    st.setString(1, UUID);
        //                    st.setString(2, pos1);
        //                    st.setString(3, pos2);
        //                    st.executeUpdate();
        //                } catch (SQLException e) {
        //                    e.printStackTrace();
        //                }
        //                return;
        //            }
        //        }
        new TerrainCache(UUID, pos1, pos2).insert();
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Terrains_data(UUID, Pos1, Pos2) VALUES (?, ?, ?)");
            st.setString(1, UUID);
            st.setString(2, pos1);
            st.setString(3, pos2);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getTerrainPos1(final String UUID) {
        if (TerrainCache.get(UUID) != null) {
            return TerrainCache.get(UUID).getPos1();
        } else {
            return null;
        }
        // if (TerrainCache.get(UUID) != null) {
        //            if (TerrainCache.get(UUID).getPos1().equalsIgnoreCase("NULL")) {
        //                return null;
        //            } else {
        //                return TerrainCache.get(UUID).getPos1();
        //            }
        //        } else {
        //            try {
        //                PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Terrains_data WHERE UUID = ?");
        //                st.setString(1, UUID);
        //                ResultSet rs = st.executeQuery();
        //                while (rs.next()) {
        //                    String pos1 = rs.getString("Pos1");
        //                    new TerrainCache(UUID, pos1, rs.getString("Pos2")).insert();
        //                    return pos1;
        //                }
        //            } catch (SQLException e) {
        //                e.printStackTrace();
        //            }
        //            return null;
        //        }
    }

    public static String getTerrainPos2(final String UUID) {
        if (TerrainCache.get(UUID) != null) {
            return TerrainCache.get(UUID).getPos2();
        } else {
            return null;
        }
        //if (TerrainCache.get(UUID) != null) {
        //            if (TerrainCache.get(UUID).getPos2().equalsIgnoreCase("NULL")) {
        //                return null;
        //            } else {
        //                return TerrainCache.get(UUID).getPos2();
        //            }
        //        } else {
        //            try {
        //                PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Terrains_data WHERE UUID = ?");
        //                st.setString(1, UUID);
        //                ResultSet rs = st.executeQuery();
        //                while (rs.next()) {
        //                    String pos2 = rs.getString("Pos2");
        //                    new TerrainCache(UUID, rs.getString("Pos1"), pos2).insert();
        //                    return pos2;
        //                }
        //            } catch (SQLException e) {
        //                e.printStackTrace();
        //            }
        //            return null;
        //        }
    }

    public static String getExist(final String UUID) {
        if (TerrainCache.get(UUID) != null) {
            return TerrainCache.get(UUID).getUUID();
        } else {
            //try {
            //                PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Terrains_data WHERE UUID = ?");
            //                st.setString(1, UUID);
            //                ResultSet rs = st.executeQuery();
            //                while (rs.next()) {
            //                    new TerrainCache(UUID, rs.getString("Pos1"), rs.getString("Pos2")).insert();
            //                    return rs.getString("UUID");
            //                }
            //            } catch (SQLException e) {
            //                e.printStackTrace();
            //            }
            //            new TerrainCache(UUID, "NULL", "NULL").insert();
            return null;
        }
    }

    public static String getMoedaUtilizada(final String UUID) {
        return TerrainInfosAPI.getMoedaUtilizada(UUID);
    }
}
