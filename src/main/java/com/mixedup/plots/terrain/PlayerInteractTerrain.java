package com.mixedup.plots.terrain;

import com.mixedup.Main;
import com.mixedup.apis.AccountAPI;
import com.mixedup.apis.TagAPI;
import com.mixedup.plots.utils.LastPositionUtil;
import com.mixedup.plots.utils.PlayerItIsUtil;
import org.bukkit.*;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.Random;

public class PlayerInteractTerrain implements Listener {

    @EventHandler
    public void onHited(final EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
            final Player player = (Player) event.getDamager();
            final Player target = (Player) event.getEntity();

            if (TerrainsUtil.playerInArea(player.getLocation()) != null) {
                final RegionTr terrain = TerrainsUtil.playerInArea(player.getLocation());

                if (TerrainInfosAPI.getPvPStatus(terrain.getRegionID()) == false) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + " * Ops, o pvp está desabilitado dentro desta região!");
                }
            }
            if (TerrainsUtil.playerInArea(target.getLocation()) != null) {
                final RegionTr terrain = TerrainsUtil.playerInArea(target.getLocation());

                if (TerrainInfosAPI.getPvPStatus(terrain.getRegionID()) == false) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + " * Ops, o pvp está desabilitado dentro desta região!");
                }
            }
        } else if (event.getDamager() instanceof Arrow && event.getEntity() instanceof Player) {
            if (TerrainsUtil.playerInArea(event.getEntity().getLocation()) != null) {
                final RegionTr terrain = TerrainsUtil.playerInArea(event.getEntity().getLocation());

                if (TerrainInfosAPI.getPvPStatus(terrain.getRegionID()) == false) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void updateLoc(final PlayerMoveEvent event) {
        final Player player = event.getPlayer();
        final String UUID = player.getUniqueId().toString();

        String from = event.getFrom().getWorld().getName() + ":" + event.getFrom().getBlockX() + ":" + event.getFrom().getBlockY() + ":" + event.getFrom().getBlockZ();
        String to = event.getTo().getWorld().getName() + ":" + event.getTo().getBlockX() + ":" + event.getTo().getBlockY() + ":" + event.getTo().getBlockZ();
        if (from.equalsIgnoreCase(to)) return;

        if (TerrainsUtil.playerInArea(player.getLocation()) == null) {
            if (LastPositionUtil.get(UUID) == null) {
                final String position = player.getWorld().getName() + ":" + player.getLocation().getBlockX() + ":" + player.getLocation().getBlockY() + ":" + player.getLocation().getBlockZ();

                Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        new LastPositionUtil(UUID, position, position).insert();
                    }
                });
            } else {
                final String position = player.getWorld().getName() + ":" + player.getLocation().getBlockX() + ":" + player.getLocation().getBlockY() + ":" + player.getLocation().getBlockZ();
                if (!LastPositionUtil.get(UUID).getLastPosition().equals(position)) {
                    Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            LastPositionUtil.get(UUID).setLastPosition2(LastPositionUtil.get(UUID).getLastPosition());
                            LastPositionUtil.get(UUID).setLastPosition(position);
                        }
                    });
                }
            }
        }
    }

    @EventHandler
    public void onMove(final PlayerMoveEvent event) {
        final Player player = event.getPlayer();
        final String UUID = player.getUniqueId().toString();

        String from = event.getFrom().getWorld().getName() + ":" + event.getFrom().getBlockX() + ":" + event.getFrom().getBlockY() + ":" + event.getFrom().getBlockZ();
        String to = event.getTo().getWorld().getName() + ":" + event.getTo().getBlockX() + ":" + event.getTo().getBlockY() + ":" + event.getTo().getBlockZ();
        if (from.equalsIgnoreCase(to)) return;

        RegionTr rg = TerrainsUtil.playerInArea(player.getLocation());
        if (rg != null) {
            if (TerrainCache.get(UUID + "1") != null) {
                if (rg.getRegionID().equals(UUID + "1")) {
                    event.setCancelled(false);
                    return;
                }
            }

            if (TerrainInfosAPI.getAmigos(rg.getRegionID()).contains(UUID)) {
                event.setCancelled(false);
                return;
            } else {
                if (TerrainInfosCache.get(rg.getRegionID()).getVisitas() == false) {
                    final String tag = TagAPI.getTag(UUID);
                    if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Admin") || tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Moderador")) {
                        return;
                    }

                    player.sendMessage(ChatColor.RED + " * Ops, este terreno está bloqueado a acessos de visitantes.");
                    final String[] loc = LastPositionUtil.get(UUID).getLastPosition2().split(":");
                    final Location location = new Location(Bukkit.getWorld(loc[0]), Integer.valueOf(loc[1]), Integer.valueOf(loc[2]), Integer.valueOf(loc[3]), player.getLocation().getYaw(), player.getLocation().getPitch());
                    player.teleport(location);
                } else {
                    if (PlayerItIsUtil.get(UUID).getStatus() == false) {
                        player.sendTitle(ChatColor.GREEN + "Você entrou no terreno de:", ChatColor.DARK_GRAY + AccountAPI.getNick(rg.getRegionID().substring(0, rg.getRegionID().length() - 1)), 20, 20, 20);
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                    if (PlayerItIsUtil.get(UUID) == null) {
                        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                new PlayerItIsUtil(UUID, true, AccountAPI.getNick(rg.getRegionID().substring(0, rg.getRegionID().length() - 1))).insert();
                            }
                        });
                    } else if (PlayerItIsUtil.get(UUID).getStatus() == false) {
                        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                PlayerItIsUtil.get(UUID).setStatus(true);
                                PlayerItIsUtil.get(UUID).setOwner(AccountAPI.getNick(rg.getRegionID().substring(0, rg.getRegionID().length() - 1)));
                            }
                        });
                    }
                }
                return;
            }
        } else {
            if (PlayerItIsUtil.get(UUID) == null) {
                Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        new PlayerItIsUtil(UUID, false, "NULL").insert();
                    }
                });
            } else if (PlayerItIsUtil.get(UUID).getStatus() == true) {
                player.sendTitle(ChatColor.RED + "Você saiu do terreno de:", ChatColor.DARK_GRAY + PlayerItIsUtil.get(UUID).getOwner(), 20, 20, 20);
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        PlayerItIsUtil.get(UUID).setStatus(false);
                    }
                });
            }
        }
    }

    @EventHandler
    public void onIgnite(final BlockIgniteEvent event) {
        if (event.getPlayer() != null) {
            Player player = event.getPlayer();
            String UUID = player.getUniqueId().toString();

            final String tag = TagAPI.getTag(UUID);
            if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Admin") || tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Moderador")) {
                return;
            }

            if (TerrainsUtil.playerInArea(event.getIgnitingEntity().getLocation()) != null) {
                if (TerrainAPI.getExist(UUID + "1") != null) {
                    if (TerrainsUtil.playerInArea(event.getIgnitingEntity().getLocation()).getRegionID().equals(TerrainAPI.getExist(UUID + "1"))) {
                        event.setCancelled(false);
                        return;
                    }
                }

                if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getIgnitingEntity().getLocation()).getRegionID()).equals("NULL")) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                    return;
                }

                if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getIgnitingEntity().getLocation()).getRegionID()).contains(":") || !TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getIgnitingEntity().getLocation()).getRegionID()).equals("NULL")) {
                    if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getIgnitingEntity().getLocation()).getRegionID()).contains(":")) {
                        final String[] amigos = TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getIgnitingEntity().getLocation()).getRegionID()).split(":");

                        for (int i = 1; i <= amigos.length; i++) {
                            if (amigos[i - 1].equals(UUID)) {
                                event.setCancelled(false);
                                break;
                            }
                            if (i == amigos.length) {
                                event.setCancelled(true);
                                player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                                final Random random = new Random();

                                if (random.nextBoolean() == true) {
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            }
                        }
                        return;
                    } else if (!TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getIgnitingEntity().getLocation()).getRegionID()).equals("NULL")) {
                        if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getIgnitingEntity().getLocation()).getRegionID()).equals(UUID)) {
                            event.setCancelled(false);
                        } else {
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                            final Random random = new Random();

                            if (random.nextBoolean() == true) {
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        }
                        return;
                    } else if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getIgnitingEntity().getLocation()).getRegionID()).equals("NULL")) {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                        final Random random = new Random();

                        if (random.nextBoolean() == true) {
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    }
                }
            }
        } else if (event.getIgnitingEntity() != null && TerrainsUtil.playerInArea(event.getIgnitingEntity().getLocation()) != null) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onClickArmorStand(final PlayerInteractAtEntityEvent event) {
        final Player player = event.getPlayer();
        final String UUID = player.getUniqueId().toString();

        final String tag = TagAPI.getTag(UUID);
        if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Admin") || tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Moderador")) {
            return;
        }

        if (TerrainsUtil.playerInArea(event.getRightClicked().getLocation()) != null) {
            if (TerrainAPI.getExist(UUID + "1") != null) {
                if (TerrainsUtil.playerInArea(event.getRightClicked().getLocation()).getRegionID().equals(TerrainAPI.getExist(UUID + "1"))) {
                    event.setCancelled(false);
                    return;
                }
            }

            if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getRightClicked().getLocation()).getRegionID()).equals("NULL")) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                return;
            }

            if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getRightClicked().getLocation()).getRegionID()).contains(":")) {
                final String[] amigos = TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getRightClicked().getLocation()).getRegionID()).split(":");

                for (int i = 1; i <= amigos.length; i++) {
                    if (amigos[i - 1].equals(UUID)) {
                        event.setCancelled(false);
                        break;
                    }
                    if (i == amigos.length) {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                        final Random random = new Random();

                        if (random.nextBoolean() == true) {
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    }
                }
                return;
            } else if (!TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getRightClicked().getLocation()).getRegionID()).equals("NULL")) {
                if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getRightClicked().getLocation()).getRegionID()).equals(UUID)) {
                    event.setCancelled(false);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                    final Random random = new Random();

                    if (random.nextBoolean() == true) {
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
                return;
            } else if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getRightClicked().getLocation()).getRegionID()).equals("NULL")) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                final Random random = new Random();

                if (random.nextBoolean() == true) {
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            } else {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onInteractEntity(final EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player) {
            final Player player = (Player) event.getDamager();
            final String UUID = player.getUniqueId().toString();

            final String tag = TagAPI.getTag(UUID);
            if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Admin") || tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Moderador")) {
                return;
            }

            if (event.getEntity().getType().equals(EntityType.ITEM_FRAME) || event.getEntity().getType().equals(EntityType.ARMOR_STAND)) {
                if (TerrainsUtil.playerInArea(event.getEntity().getLocation()) != null) {
                    if (TerrainAPI.getExist(UUID + "1") != null) {
                        if (TerrainsUtil.playerInArea(event.getEntity().getLocation()).getRegionID().equals(TerrainAPI.getExist(UUID + "1"))) {
                            event.setCancelled(false);
                            return;
                        }
                    }

                    if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).equals("NULL")) {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                        return;
                    }

                    if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).contains(":")) {
                        final String[] amigos = TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).split(":");

                        for (int i = 1; i <= amigos.length; i++) {
                            if (amigos[i - 1].equals(UUID)) {
                                event.setCancelled(false);
                                break;
                            }
                            if (i == amigos.length) {
                                event.setCancelled(true);
                                player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                                final Random random = new Random();

                                if (random.nextBoolean() == true) {
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            }
                        }
                        return;
                    } else if (!TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).equals("NULL")) {
                        if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).equals(UUID)) {
                            event.setCancelled(false);
                        } else {
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                            final Random random = new Random();

                            if (random.nextBoolean() == true) {
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        }
                        return;
                    } else if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).equals("NULL")) {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                        final Random random = new Random();

                        if (random.nextBoolean() == true) {
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onProjectile(final ProjectileLaunchEvent event) {
        if (event.getEntity().getShooter() instanceof Player) {
            final Player player = (Player) event.getEntity().getShooter();
            final String UUID = player.getUniqueId().toString();

            final String tag = TagAPI.getTag(UUID);
            if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Admin") || tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Moderador")) {
                return;
            }

            if (event.getEntity().getType().name().contains(Material.ARROW.name())) {
                if (TerrainsUtil.playerInArea(event.getEntity().getLocation()) != null) {
                    if (TerrainAPI.getExist(UUID + "1") != null) {
                        if (TerrainsUtil.playerInArea(event.getEntity().getLocation()).getRegionID().equals(TerrainAPI.getExist(UUID + "1"))) {
                            event.setCancelled(false);
                            return;
                        }
                    }

                    if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).equals("NULL")) {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                        return;
                    }

                    if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).contains(":")) {
                        final String[] amigos = TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).split(":");

                        for (int i = 1; i <= amigos.length; i++) {
                            if (amigos[i - 1].equals(UUID)) {
                                event.setCancelled(false);
                                break;
                            }
                            if (i == amigos.length) {
                                event.setCancelled(true);
                                player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                                final Random random = new Random();

                                if (random.nextBoolean() == true) {
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            }
                        }
                        return;
                    } else if (!TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).equals("NULL")) {
                        if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).equals(UUID)) {
                            event.setCancelled(false);
                        } else {
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                            final Random random = new Random();

                            if (random.nextBoolean() == true) {
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        }
                        return;
                    } else if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getEntity().getLocation()).getRegionID()).equals("NULL")) {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                        final Random random = new Random();

                        if (random.nextBoolean() == true) {
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onInteract(final PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        final String UUID = player.getUniqueId().toString();

        final String tag = TagAPI.getTag(UUID);
        if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Admin") || tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Moderador")) {
            return;
        }

        if (TerrainsUtil.playerInArea(event.getPlayer().getLocation()) != null) {
            if (event.getAction().name().contains("CLICK_BLOCK") && event.getClickedBlock() != null && event.getClickedBlock().getType().name().contains("SIGN"))
                return;

            if (TerrainAPI.getExist(UUID + "1") != null) {
                if (TerrainsUtil.playerInArea(event.getPlayer().getLocation()).getRegionID().equals(TerrainAPI.getExist(UUID + "1"))) {
                    event.setCancelled(false);
                    return;
                }
            }

            if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getPlayer().getLocation()).getRegionID()).equals("NULL")) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                return;
            }

            if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getPlayer().getLocation()).getRegionID()).contains(":")) {
                final String[] amigos = TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getPlayer().getLocation()).getRegionID()).split(":");

                for (int i = 1; i <= amigos.length; i++) {
                    if (amigos[i - 1].equals(UUID)) {
                        event.setCancelled(false);
                        break;
                    }
                    if (i == amigos.length) {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                        final Random random = new Random();

                        if (random.nextBoolean() == true) {
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    }
                }
                return;
            } else if (!TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getPlayer().getLocation()).getRegionID()).equals("NULL")) {
                if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getPlayer().getLocation()).getRegionID()).equals(UUID)) {
                    event.setCancelled(false);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                    final Random random = new Random();

                    if (random.nextBoolean() == true) {
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
                return;
            } else if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getPlayer().getLocation()).getRegionID()).equals("NULL")) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                final Random random = new Random();

                if (random.nextBoolean() == true) {
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            } else {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onBreak(final BlockBreakEvent event) {
        final Player player = event.getPlayer();
        final String UUID = player.getUniqueId().toString();

        final String tag = TagAPI.getTag(UUID);
        if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Admin") || tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Moderador")) {
            return;
        }

        if (TerrainsUtil.playerInArea(event.getBlock().getLocation()) != null) {
            if (TerrainAPI.getExist(UUID + "1") != null) {
                if (TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID().equals(TerrainAPI.getExist(UUID + "1"))) {
                    event.setCancelled(false);
                    return;
                }
            }

            if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).equals("NULL")) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                return;
            }

            if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).contains(":") || !TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).equals("NULL")) {
                if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).contains(":")) {
                    final String[] amigos = TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).split(":");

                    for (int i = 1; i <= amigos.length; i++) {
                        if (amigos[i - 1].equals(UUID)) {
                            event.setCancelled(false);
                            break;
                        }
                        if (i == amigos.length) {
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                            final Random random = new Random();

                            if (random.nextBoolean() == true) {
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        }
                    }
                    return;
                } else if (!TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).equals("NULL")) {
                    if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).equals(UUID)) {
                        event.setCancelled(false);
                    } else {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                        final Random random = new Random();

                        if (random.nextBoolean() == true) {
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    }
                    return;
                } else if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).equals("NULL")) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                    final Random random = new Random();

                    if (random.nextBoolean() == true) {
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPlace(final BlockPlaceEvent event) {
        final Player player = event.getPlayer();
        final String UUID = player.getUniqueId().toString();

        final String tag = TagAPI.getTag(UUID);
        if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Admin") || tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Moderador")) {
            return;
        }

        if (TerrainsUtil.playerInArea(event.getBlock().getLocation()) != null) {
            if (TerrainAPI.getExist(UUID + "1") != null) {
                if (TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID().equals(TerrainAPI.getExist(UUID + "1"))) {
                    event.setCancelled(false);
                    return;
                }
            }

            if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).equals("NULL")) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                return;
            }

            if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).contains(":") || !TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).equals("NULL")) {
                if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).contains(":")) {
                    final String[] amigos = TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).split(":");

                    for (int i = 1; i <= amigos.length; i++) {
                        if (amigos[i - 1].equals(UUID)) {
                            event.setCancelled(false);
                            break;
                        }
                        if (i == amigos.length) {
                            event.setCancelled(true);
                            player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                            final Random random = new Random();

                            if (random.nextBoolean() == true) {
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        }
                    }
                    return;
                } else if (!TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).equals("NULL")) {
                    if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).equals(UUID)) {
                        event.setCancelled(false);
                    } else {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                        final Random random = new Random();

                        if (random.nextBoolean() == true) {
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    }
                    return;
                } else if (TerrainInfosAPI.getAmigos(TerrainsUtil.playerInArea(event.getBlock().getLocation()).getRegionID()).equals("NULL")) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + " * Ops, este terreno contém proteção!");
                    final Random random = new Random();

                    if (random.nextBoolean() == true) {
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
            }
        }
    }
}
