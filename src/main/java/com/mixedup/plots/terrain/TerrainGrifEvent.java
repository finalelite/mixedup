package com.mixedup.plots.terrain;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockExplodeEvent;

public class TerrainGrifEvent implements Listener {

    @EventHandler
    public void onExplode(final BlockExplodeEvent event) {
        for (final org.bukkit.block.Block block : event.blockList()) {
            if (TerrainsUtil.playerInArea(block.getLocation()) != null) {
                event.setCancelled(true);
            }
        }
    }
}
