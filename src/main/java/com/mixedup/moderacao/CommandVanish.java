package com.mixedup.moderacao;

import br.com.finalelite.pauloo27.api.PauloAPI;
import br.com.finalelite.pauloo27.api.database.PlayerRole;
import com.mixedup.Main;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import com.mixedup.hashs.PlayerVanishHash;
import com.mixedup.utils.CentralizeMsg;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandVanish implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("vanish") || cmd.getName().equalsIgnoreCase("v")) {
            if (TagAPI.getTag(UUID).equalsIgnoreCase("Master") || TagAPI.getTag(UUID).equalsIgnoreCase("Supervisor") || TagAPI.getTag(UUID).equalsIgnoreCase("Admin") || TagAPI.getTag(UUID).equalsIgnoreCase("Moderador")) {
                if (args.length == 0) {
                    if (!PlayerVanishHash.isVanished(player)) {
                        PlayerVanishHash.toggleVanish(player);
                        for (final Player target : Bukkit.getOnlinePlayers()) {
                            if (!target.getName().equalsIgnoreCase(player.getName())) {
                                target.hidePlayer(Main.getInstance(), player);
                            }
                        }
                        player.sendMessage(ChatColor.GREEN + " * Vanish habilitado!");
                        player.setPlayerListHeader(ChatColor.translateAlternateColorCodes('&', " \n&6&lFINALELITE\n&aVocê está no modo vanish\n "));
                        player.setPlayerListFooter(CentralizeMsg.sendCenteredMessage("\n                         " +
                                "&6Twitter: &fwww.twitter.com/FinalElite                          " +
                                "\n&6Discord: &fbit.ly/FinalEliteDiscord" +
                                "\n &6Loja: &fwww.finalelite.com.br\n "));
                        player.setGameMode(GameMode.SPECTATOR);
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        PlayerVanishHash.toggleVanish(player);
                        for (final Player target : Bukkit.getOnlinePlayers()) {
                            if (!target.getName().equalsIgnoreCase(player.getName())) {
                                target.showPlayer(Main.getInstance(), player);
                            }
                        }
                        player.sendMessage(ChatColor.GREEN + " * Vanish desabilitado!");
                        player.setPlayerListHeader(ChatColor.translateAlternateColorCodes('&', " \n&6&lFINALELITE\n "));
                        player.setPlayerListFooter(CentralizeMsg.sendCenteredMessage("\n                         " +
                                "&6Twitter: &fwww.twitter.com/FinalElite                          " +
                                "\n&6Discord: &fbit.ly/FinalEliteDiscord" +
                                "\n &6Loja: &fwww.finalelite.com.br\n "));

                        if (PauloAPI.getInstance().getAccountInfo(player).getRole() == PlayerRole.MODERADOR)
                            player.setGameMode(GameMode.SURVIVAL);
                        else
                            player.setGameMode(GameMode.CREATIVE);

                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/vanish ou /v");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }

        return false;
    }
}
