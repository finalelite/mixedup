package com.mixedup.moderacao;

import com.mixedup.MySqlHub;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.xml.bind.DatatypeConverter;
import java.security.NoSuchAlgorithmException;
import java.sql.*;

public class PunirAPI {

    public static String generate(final int keyLen) throws NoSuchAlgorithmException {
        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
        keyGen.init(keyLen);
        SecretKey secretKey = keyGen.generateKey();
        byte[] encoded = secretKey.getEncoded();
        return DatatypeConverter.printHexBinary(encoded).toLowerCase();
    }

    public static void punir(final String UUID, final String autor, final String motivo, final String prova, final Timestamp tempo, final boolean eterno, final boolean mutado) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("INSERT INTO Punicoes_data(UUID, Por, Motivo, Prova, Tempo, Eterno, Mutado, CaseId) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
            st.setString(1, UUID);
            st.setString(2, autor);
            st.setString(3, motivo);
            st.setString(4, prova);
            st.setTimestamp(5, tempo);
            st.setBoolean(6, eterno);
            st.setBoolean(7, mutado);
            st.setString(8, "#" + generate(128).substring(22));
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static String getCaseId(final String UUID) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Punicoes_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("CaseId");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void punicaoInfo(final String UUID) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("INSERT INTO Punicoes_infos(UUID, Quantia) VALUES (?, ?)");
            st.setString(1, UUID);
            st.setInt(2, 1);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateVezesNeg(final String UUID) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("UPDATE Punicoes_infos SET Quantia = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setInt(1, PunirAPI.getVezes(UUID) - 1);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateVezes(final String UUID) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("UPDATE Punicoes_infos SET Quantia = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setInt(1, PunirAPI.getVezes(UUID) + 1);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static int getVezes(final String UUID) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Punicoes_infos WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("Quantia");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getExistInfo(final String UUID) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Punicoes_infos WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("UUID");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean getIsMutado(final String UUID) {
        if (MutedCache.get(UUID) == null) {
            try {
                final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Punicoes_data WHERE UUID = ?");
                st.setString(1, UUID);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final boolean muted = rs.getBoolean("Mutado");
                    new MutedCache(UUID, muted).insert();

                    return muted;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return false;
        } else {
            return MutedCache.get(UUID).getMuted();
        }
    }


    public static boolean getIsEterno(final String UUID) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Punicoes_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Eterno");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static Timestamp getTempo(final String UUID) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Punicoes_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getTimestamp("Tempo");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getProva(final String UUID) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Punicoes_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Prova");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getMotivo(final String UUID) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Punicoes_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Motivo");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getAutor(final String UUID) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Punicoes_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Por");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getExistPunicao(final String UUID) {
        if (MutedCache.get(UUID) == null) {
            try {
                final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Punicoes_data WHERE UUID = ?");
                st.setString(1, UUID);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    new MutedCache(UUID, rs.getBoolean("Mutado")).insert();
                    return rs.getString("UUID");
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return null;
        } else {
            return "a";
        }
    }

    public static void removerPunicao(final String UUID) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("DELETE FROM Punicoes_data WHERE UUID = ?");
            st.setString(1, UUID);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }

        if (MutedCache.get(UUID) != null) {

            MutedCache.get(UUID).setMuted(false);

        }

    }
}
