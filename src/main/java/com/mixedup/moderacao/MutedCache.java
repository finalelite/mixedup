package com.mixedup.moderacao;

import java.util.HashMap;

public class MutedCache {

    public static HashMap<String, MutedCache> CACHE = new HashMap<>();
    private final String uuid;
    private boolean muted;

    public MutedCache(final String uuid, final boolean muted) {
        this.uuid = uuid;
        this.muted = muted;
    }

    public static MutedCache get(final String uuid) {
        return MutedCache.CACHE.get(uuid);
    }

    public MutedCache insert() {
        MutedCache.CACHE.put(uuid, this);
        return this;
    }

    public boolean getMuted() {
        return muted;
    }

    public void setMuted(final boolean muted) {
        this.muted = muted;
    }
}
