package com.mixedup.moderacao;

import com.mixedup.Main;
import com.mixedup.apis.AccountAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.ServerAPI;
import com.mixedup.apis.TagAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.Date;

public class CommandDespunir implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("despunir")) {
            if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor") || TagAPI.getTag(UUID).equals("Admin") || TagAPI.getTag(UUID).equals("Moderador")) {
                if (args.length == 1) {
                    if (AccountAPI.getExistNick(args[0]) != null) {
                        final String UUIDtarget = PlayerUUID.getUUIDcache(args[0]);
                        if (PunirAPI.getIsEterno(UUIDtarget) == true || (new Date().getTime() / 1000) - (PunirAPI.getTempo(UUID).getTime() / 1000) > 0) {
                            PunirAPI.removerPunicao(UUIDtarget);
                            PunirAPI.updateVezesNeg(UUIDtarget);
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                            for (final Player target : Bukkit.getOnlinePlayers()) {
                                final String UID = PlayerUUID.getUUID(target.getName());

                                if (ServerAPI.getStaffOnlineExist(UID) != null) {
                                    try {
                                        final ByteArrayOutputStream b = new ByteArrayOutputStream();
                                        final DataOutputStream out = new DataOutputStream(b);

                                        out.writeUTF("Message");
                                        out.writeUTF(target.getName());
                                        final String msg = ChatColor.GREEN + "\n \n(!)" + " Punição removida de " + args[0] + "\n(!)Por: " + player.getName() + "\n \n";
                                        out.writeUTF(msg);

                                        player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                                    } catch (final Exception e) {
                                        //-
                                    }
                                }
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, este player não se encontra banido.");
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, este player não contém registros em nosso banco de dados.");
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/despunir (nick)");
                }
            }
        }

        return false;
    }
}
