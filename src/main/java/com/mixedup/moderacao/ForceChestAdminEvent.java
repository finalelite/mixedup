package com.mixedup.moderacao;

import com.mixedup.apis.TagAPI;
import com.mixedup.hashs.PlayerVanishHash;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.Container;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import java.util.Arrays;

public class ForceChestAdminEvent implements Listener {

    @EventHandler
    public void onClick(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        if (event.getClickedBlock() != null) {
            if (event.getClickedBlock().getType().equals(Material.CHEST) || event.getClickedBlock().getType().equals(Material.TRAPPED_CHEST)) {
                String[] staff = {"Master", "Supervisor", "Admin", "Moderador"};
                if (PlayerVanishHash.isVanished(player) && Arrays.asList(staff).contains(TagAPI.getTag(uuid))) {
                    event.setCancelled(true);

                    final Block block = event.getClickedBlock();
                    final Inventory inv = ((Container) block.getState()).getInventory();

                    Inventory invBypass = Bukkit.createInventory(null, 6 * 9, "Inventario: ");
                    for (int i = 1; i <= inv.getSize(); i++) {
                        if (inv.getItem(i - 1) != null && !inv.getItem(i - 1).getType().equals(Material.AIR)) {
                            invBypass.setItem(i - 1, inv.getItem(i - 1));
                        }
                    }

                    if (!player.isSneaking()) {
                        player.openInventory(invBypass);
                    } else {
                        player.openInventory(inv);
                    }
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
    }
}
