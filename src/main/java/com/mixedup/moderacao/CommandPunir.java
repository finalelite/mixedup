package com.mixedup.moderacao;

import com.mixedup.Main;
import com.mixedup.apis.*;
import com.mixedup.utils.CentralizeMsg;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class CommandPunir implements TabCompleter, CommandExecutor {


    private static final String[] COMMANDS = {"Abuso-de-bugs", "Ameaça", "Anti-jogo", "Descriminação", "Chantagem", "Estorno-pagamento", "Conta-fake", "Desinformação", "Divulgação", "Falsificação-de-provas", "Flood", "Hack", "Mensagem-fake", "Ofensa-á-staff", "Ofensa-ao-jogador", "Incentivo-ao-uso-de-hack"};

    private static String msgKick(final String UUID) {
        String msg = null;
        if (PunirAPI.getIsMutado(UUID) == false) {
            if (PunirAPI.getIsEterno(UUID) == true) {
                msg = CentralizeMsg.sendCenteredMessage(ChatColor.RED + "§lFinal Elite\n \nMotivo: " + ChatColor.GRAY + PunirAPI.getMotivo(UUID) + ChatColor.RED + "\nAutor: " + ChatColor.GRAY + PunirAPI.getAutor(UUID) + ChatColor.RED +
                        "\nProva: " + ChatColor.GRAY + PunirAPI.getProva(UUID) + ChatColor.RED + "\nID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUID) + ChatColor.RED + "\n \nFoi punido incorretamente? Mande-nos uma DM no Twitter " + ChatColor.BLUE + "@FinalElite");
            } else {
                msg = CentralizeMsg.sendCenteredMessage(ChatColor.RED + "§lFinal Elite\n \nMotivo: " + ChatColor.GRAY + PunirAPI.getMotivo(UUID) + ChatColor.RED + "\nAutor: " + ChatColor.GRAY + PunirAPI.getAutor(UUID) +
                        ChatColor.RED + "\nProva: " + ChatColor.GRAY + PunirAPI.getProva(UUID) + ChatColor.RED + "\nID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUID) + ChatColor.RED + "\nDuração: " + ChatColor.GRAY + HorarioAPI.getTime((PunirAPI.getTempo(UUID).getTime() / 1000) - (new Date().getTime() / 1000))
                        + ChatColor.RED + "\n \nFoi punido incorretamente? Crie um ticket em nosso discord " + ChatColor.BLUE + "discord.me/finalelite");
            }
        }
        return msg;
    }

    private static void punir(final String[] args, String motivo, final String UUID, final String UUIDtarget, final Player player, final String[] motivos, final int x) {
        if (args[1].equalsIgnoreCase("abuso-de-bugs")) {
            if (PunirAPI.getExistInfo(UUID + "abuso-de-bugs") == null) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 360);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, false);
                PunirAPI.punicaoInfo(UUIDtarget + "abuso-de-bugs");

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: " + HorarioAPI.getTime(1296000) + "\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            } else if (PunirAPI.getVezes(UUID + "abuso-de-bugs") == 1) {
                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, null, true, false);

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: Permanente\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            }
        }
        if (args[1].equalsIgnoreCase("ameaça")) {
            if (PunirAPI.getExistInfo(UUID + "ameaça") == null) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 120);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, false);
                PunirAPI.punicaoInfo(UUID + "ameaça");

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: " + HorarioAPI.getTime(432000) + "\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            } else if (PunirAPI.getVezes(UUID + "ameaça") == 1) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 168);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, false);
                PunirAPI.updateVezes(UUID + "ameaça");

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: " + HorarioAPI.getTime(604800) + "\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            } else if (PunirAPI.getVezes(UUID + "ameaça") == 2) {
                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, null, true, false);

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: Permanente\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            }
        }
        if (args[1].equalsIgnoreCase("Anti-jogo")) {
            if (PunirAPI.getExistInfo(UUID + "Anti-jogo") == null) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 10);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, false);
                PunirAPI.punicaoInfo(UUID + "Anti-jogo");

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: " + HorarioAPI.getTime(72000) + "\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            } else if (PunirAPI.getVezes(UUID + "Anti-jogo") == 1) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 24);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, false);
                PunirAPI.updateVezes(UUID + "Anti-jogo");

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: " + HorarioAPI.getTime(86400) + "\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            } else if (PunirAPI.getVezes(UUID + "Anti-jogo") == 2) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 120);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, false);
                PunirAPI.updateVezes(UUID + "Anti-jogo");

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: " + HorarioAPI.getTime(432000) + "\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            } else if (PunirAPI.getVezes(UUID + "Anti-jogo") == 3) {
                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, null, true, false);

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: Permanente\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            }
        }
        if (args[1].equalsIgnoreCase("descriminação")) {
            if (PunirAPI.getExistInfo(UUID + "descriminação") == null) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 120);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, false);
                PunirAPI.punicaoInfo(UUID + "descriminação");

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: " + HorarioAPI.getTime(432000) + "\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            } else if (PunirAPI.getVezes(UUID + "descriminação") == 1) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 240);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, false);
                PunirAPI.updateVezes(UUID + "descriminação");

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: " + HorarioAPI.getTime(864000) + "\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            } else if (PunirAPI.getVezes(UUID + "descriminação") == 2) {
                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, null, true, false);

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: Permanente\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            }
        }
        if (args[1].equalsIgnoreCase("chantagem")) {
            if (PunirAPI.getExistInfo(UUID + "chantagem") == null) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 168);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, false);
                PunirAPI.punicaoInfo(UUID + "chantagem");

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: " + HorarioAPI.getTime(604800) + "\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            } else if (PunirAPI.getVezes(UUID + "chantagem") == 1) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 360);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, false);
                PunirAPI.updateVezes(UUID + "chantagem");

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: " + HorarioAPI.getTime(1296000) + "\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            } else if (PunirAPI.getVezes(UUID + "chantagem") == 2) {
                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, null, true, false);

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: Permanente\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            }
        }
        if (args[1].equalsIgnoreCase("estorno-pagamento")) {
            PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, null, true, false);

            for (final Player target : Bukkit.getOnlinePlayers()) {
                String UID = PlayerUUID.getUUID(target.getName());
                if (UID.equals(UUIDtarget)) {
                    final String msg = msgKick(UUIDtarget);
                    target.kickPlayer(msg);
                }

                if (ServerAPI.getStaffOnlineExist(UID) != null) {
                    try {
                        final ByteArrayOutputStream b = new ByteArrayOutputStream();
                        final DataOutputStream out = new DataOutputStream(b);

                        out.writeUTF("Message");
                        out.writeUTF(target.getName());
                        final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: Permanente\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                        out.writeUTF(msg);

                        player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                    } catch (final Exception e) {
                        //-
                    }
                }
            }
        }
        if (args[1].equalsIgnoreCase("conta-fake")) {
            PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, null, true, false);

            for (final Player target : Bukkit.getOnlinePlayers()) {
                String UID = PlayerUUID.getUUID(target.getName());
                if (UID.equals(UUIDtarget)) {
                    final String msg = msgKick(UUIDtarget);
                    target.kickPlayer(msg);
                }

                if (ServerAPI.getStaffOnlineExist(UID) != null) {
                    try {
                        final ByteArrayOutputStream b = new ByteArrayOutputStream();
                        final DataOutputStream out = new DataOutputStream(b);

                        out.writeUTF("Message");
                        out.writeUTF(target.getName());
                        final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: Permanente\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                        out.writeUTF(msg);

                        player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                    } catch (final Exception e) {
                        //-
                    }
                }
            }
        }
        if (args[1].equalsIgnoreCase("desinformação")) {
            if (PunirAPI.getExistInfo(UUID + "desinformação") == null) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 12);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, true);
                PunirAPI.punicaoInfo(UUID + "desinformação");
            } else if (PunirAPI.getVezes(UUID + "desinformação") == 1) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 48);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, false);
                PunirAPI.updateVezes(UUID + "desinformação");

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: " + HorarioAPI.getTime(172800) + "\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            } else if (PunirAPI.getVezes(UUID + "desinformação") == 2) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 120);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, false);
                PunirAPI.updateVezes(UUID + "desinformação");

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: " + HorarioAPI.getTime(432000) + "\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            } else if (PunirAPI.getVezes(UUID + "desinformação") == 3) {
                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, null, true, false);

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: Permanente\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            }
        }
        if (args[1].equalsIgnoreCase("divulgação")) {
            if (PunirAPI.getExistInfo(UUID + "divulgação") == null) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 10);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, false);
                PunirAPI.punicaoInfo(UUID + "divulgação");

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: " + HorarioAPI.getTime(72000) + "\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            } else if (PunirAPI.getVezes(UUID + "divulgação") == 1) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 24);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, false);
                PunirAPI.updateVezes(UUID + "divulgação");

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: " + HorarioAPI.getTime(86400) + "\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            } else if (PunirAPI.getVezes(UUID + "divulgação") == 2) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 120);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, false);
                PunirAPI.updateVezes(UUID + "divulgação");

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: " + HorarioAPI.getTime(432000) + "\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            } else if (PunirAPI.getVezes(UUID + "divulgação") == 3) {
                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, null, true, false);

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: Permanente\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            }
        }
        if (args[1].equalsIgnoreCase("divulgação-de-servidores")) {
            PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, null, true, false);

            for (final Player target : Bukkit.getOnlinePlayers()) {
                String UID = PlayerUUID.getUUID(target.getName());
                if (UID.equals(UUIDtarget)) {
                    final String msg = msgKick(UUIDtarget);
                    target.kickPlayer(msg);
                }

                if (ServerAPI.getStaffOnlineExist(UID) != null) {
                    try {
                        final ByteArrayOutputStream b = new ByteArrayOutputStream();
                        final DataOutputStream out = new DataOutputStream(b);

                        out.writeUTF("Message");
                        out.writeUTF(target.getName());
                        final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: Permanente\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                        out.writeUTF(msg);

                        player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                    } catch (final Exception e) {
                        //-
                    }
                }
            }
        }
        if (args[1].equalsIgnoreCase("divulgação-de-site-ou-loja-fake")) {
            PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, null, true, false);

            for (final Player target : Bukkit.getOnlinePlayers()) {
                String UID = PlayerUUID.getUUID(target.getName());
                if (UID.equals(UUIDtarget)) {
                    final String msg = msgKick(UUIDtarget);
                    target.kickPlayer(msg);
                }

                if (ServerAPI.getStaffOnlineExist(UID) != null) {
                    try {
                        final ByteArrayOutputStream b = new ByteArrayOutputStream();
                        final DataOutputStream out = new DataOutputStream(b);

                        out.writeUTF("Message");
                        out.writeUTF(target.getName());
                        final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: Permanente\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                        out.writeUTF(msg);

                        player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                    } catch (final Exception e) {
                        //-
                    }
                }
            }
        }
        if (args[1].equalsIgnoreCase("falsificação-de-provas")) {
            PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, null, true, false);

            for (final Player target : Bukkit.getOnlinePlayers()) {
                String UID = PlayerUUID.getUUID(target.getName());
                if (UID.equals(UUIDtarget)) {
                    final String msg = msgKick(UUIDtarget);
                    target.kickPlayer(msg);
                }

                if (ServerAPI.getStaffOnlineExist(UID) != null) {
                    try {
                        final ByteArrayOutputStream b = new ByteArrayOutputStream();
                        final DataOutputStream out = new DataOutputStream(b);

                        out.writeUTF("Message");
                        out.writeUTF(target.getName());
                        final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: Permanente\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                        out.writeUTF(msg);

                        player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                    } catch (final Exception e) {
                        //-
                    }
                }
            }
        }
        if (args[1].equalsIgnoreCase("flood")) {
            if (PunirAPI.getExistInfo(UUID + "flood") == null) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 5);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, true);
                PunirAPI.punicaoInfo(UUID + "flood");
            } else if (PunirAPI.getVezes(UUID + "flood") == 1) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 24);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, true);
                PunirAPI.updateVezes(UUID + "flood");

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: " + HorarioAPI.getTime(86400) + "\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            } else if (PunirAPI.getVezes(UUID + "flood") == 2) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 24);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, false);
                PunirAPI.updateVezes(UUID + "flood");
            } else if (PunirAPI.getVezes(UUID + "flood") == 3) {
                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, null, true, false);

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: Permanente\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            }
        }
        if (args[1].equalsIgnoreCase("hack")) {
            PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, null, true, false);

            for (final Player target : Bukkit.getOnlinePlayers()) {
                String UID = PlayerUUID.getUUID(target.getName());
                if (UID.equals(UUIDtarget)) {
                    final String msg = msgKick(UUIDtarget);
                    target.kickPlayer(msg);
                }

                if (ServerAPI.getStaffOnlineExist(UID) != null) {
                    try {
                        final ByteArrayOutputStream b = new ByteArrayOutputStream();
                        final DataOutputStream out = new DataOutputStream(b);

                        out.writeUTF("Message");
                        out.writeUTF(target.getName());
                        final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: Permanente\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                        out.writeUTF(msg);

                        player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                    } catch (final Exception e) {
                        //-
                    }
                }
            }
        }
        if (args[1].equalsIgnoreCase("mensagem-fake")) {
            if (PunirAPI.getExistInfo(UUID + "mensagem-fake") == null) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 5);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, true);
                PunirAPI.punicaoInfo(UUID + "mensagem-fake");
            } else if (PunirAPI.getVezes(UUID + "mensagem-fake") == 1) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 24);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, false);
                PunirAPI.updateVezes(UUID + "mensagem-fake");

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: " + HorarioAPI.getTime(86400) + "\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            } else if (PunirAPI.getVezes(UUID + "mensagem-fake") == 2) {
                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, null, true, false);

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: Permanente\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            }
        }
        if (args[1].equalsIgnoreCase("nick-inapropriado")) {
            PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, null, true, false);

            for (final Player target : Bukkit.getOnlinePlayers()) {
                String UID = PlayerUUID.getUUID(target.getName());
                if (UID.equals(UUIDtarget)) {
                    final String msg = msgKick(UUIDtarget);
                    target.kickPlayer(msg);
                }

                if (ServerAPI.getStaffOnlineExist(UID) != null) {
                    try {
                        final ByteArrayOutputStream b = new ByteArrayOutputStream();
                        final DataOutputStream out = new DataOutputStream(b);

                        out.writeUTF("Message");
                        out.writeUTF(target.getName());
                        final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: Permanente\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                        out.writeUTF(msg);

                        player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                    } catch (final Exception e) {
                        //-
                    }
                }
            }
        }
        if (args[1].equalsIgnoreCase("ofensa-á-staff")) {
            if (PunirAPI.getExistInfo(UUID + "ofensa-á-staff") == null) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 72);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, false);
                PunirAPI.punicaoInfo(UUID + "ofensa-á-staff");

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: " + HorarioAPI.getTime(259200) + "\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            } else if (PunirAPI.getVezes(UUID + "ofensa-á-staff") == 1) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 168);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, false);
                PunirAPI.updateVezes(UUID + "ofensa-á-staff");

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: " + HorarioAPI.getTime(604800) + "\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            } else if (PunirAPI.getVezes(UUID + "ofensa-á-staff") == 2) {
                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, null, true, false);

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: Permanente\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            }
        }
        if (args[1].equalsIgnoreCase("ofensa-ao-jogador")) {
            if (PunirAPI.getExistInfo(UUID + "ofensa-ao-jogador") == null) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 24);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, true);
                PunirAPI.punicaoInfo(UUID + "ofensa-ao-jogador");
            } else if (PunirAPI.getVezes(UUID + "ofensa-ao-jogador") == 1) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 72);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, false);
                PunirAPI.updateVezes(UUID + "ofensa-ao-jogador");

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: " + HorarioAPI.getTime(259200) + "\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            } else if (PunirAPI.getVezes(UUID + "ofensa-ao-jogador") == 2) {
                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, null, true, false);

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: Permanente\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            }
        }
        if (args[1].equalsIgnoreCase("incentivo-ao-uso-de-hack")) {
            if (PunirAPI.getExistInfo(UUID + "incentivo-ao-uso-de-hack") == null) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 48);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, true);
                PunirAPI.punicaoInfo(UUID + "incentivo-ao-uso-de-hack");
            } else if (PunirAPI.getVezes(UUID + "incentivo-ao-uso-de-hack") == 1) {
                java.util.Date date = new Date();
                date.setHours(date.getHours() + 72);
                java.sql.Timestamp time = new java.sql.Timestamp(date.getTime());

                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, time, false, false);
                PunirAPI.updateVezes(UUID + "incentivo-ao-uso-de-hack");

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: " + HorarioAPI.getTime(259200) + "\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            } else if (PunirAPI.getVezes(UUID + "incentivo-ao-uso-de-hack") == 2) {
                PunirAPI.punir(UUIDtarget, player.getName(), motivos[x - 1].replaceAll("-", " "), motivo, null, true, false);

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    String UID = PlayerUUID.getUUID(target.getName());
                    if (UID.equals(UUIDtarget)) {
                        final String msg = msgKick(UUIDtarget);
                        target.kickPlayer(msg);
                    }

                    if (ServerAPI.getStaffOnlineExist(UID) != null) {
                        try {
                            final ByteArrayOutputStream b = new ByteArrayOutputStream();
                            final DataOutputStream out = new DataOutputStream(b);

                            out.writeUTF("Message");
                            out.writeUTF(target.getName());
                            final String msg = ChatColor.RED + "\n \n(!)" + args[0] + " foi punido por " + player.getName() + "\n(!)Motivo: " + motivos[x - 1] + "\n(!)Duração: Permanente\n(!)Prova: " + motivo + "\n(!)ID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(UUIDtarget) + "\n \n";
                            out.writeUTF(msg);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        } catch (final Exception e) {
                            //-
                        }
                    }
                }
            }
        }
    }

    @Override
    public List<String> onTabComplete(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        List<String> completions = new ArrayList<>(Arrays.asList(COMMANDS));

        if (cmd.getName().equalsIgnoreCase("punir")) {
            if (args.length == 2) {
                return completions;
            }
        }
        return null;
    }

    public boolean onCommand2(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("despunir")) {

        }
        return false;
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("punir")) {
            if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor") || TagAPI.getTag(UUID).equals("Admin") || TagAPI.getTag(UUID).equals("Moderador")) {
                if (args.length >= 2 && args.length <= 3) {
                    if (AccountAPI.getExistNick(args[0]) == null) {
                        player.sendMessage(ChatColor.RED + " * Ops, este player não contém registros em nosso banco de dados.");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    } else {
                        final String[] motivos = {"Abuso-de-bugs", "Ameaça", "Anti-jogo", "Descriminação", "Chantagem", "Estorno-pagamento", "Conta-fake", "Desinformação", "Divulgação", "Falsificação-de-provas", "Flood", "Hack", "Mensagem-fake", "Ofensa-á-staff", "Ofensa-ao-jogador", "Incentivo-ao-uso-de-hack"};
                        for (int x = 1; x <= motivos.length; x++) {
                            if (args[1].equalsIgnoreCase(motivos[x - 1])) {
                                if (args.length == 2) {
                                    String[] staff = {"Master", "Supervisor", "Admin"};
                                    if (Arrays.asList(staff).contains(TagAPI.getTag(UUID))) {
                                        final String UUIDtarget = PlayerUUID.getUUID(args[0]);
                                        if (PunirAPI.getExistPunicao(UUIDtarget) == null) {
                                            punir(args, "Não informado", UUID, UUIDtarget, player, motivos, x);
                                        } else {
                                            player.sendMessage(ChatColor.RED + " * Ops, este player já se encontra punido!");
                                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        }
                                    } else {
                                        player.sendMessage(ChatColor.RED + " * Ops, a prova informada contém algum erro ou não foi informada.");
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        return false;
                                    }
                                    return false;
                                } else if (args.length == 3) {
                                    if (args[2].contains("http://prntscr.com/")) {
                                        final String UUIDtarget = PlayerUUID.getUUID(args[0]);

                                        if (PunirAPI.getExistPunicao(UUIDtarget) == null) {
                                            punir(args, args[2], UUID, UUIDtarget, player, motivos, x);
                                        } else {
                                            player.sendMessage(ChatColor.RED + " * Ops, este player já se encontra punido!");
                                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        }
                                    } else if (args[2].contains("https://youtu.be/")) {
                                        final String UUIDtarget = PlayerUUID.getUUID(args[0]);

                                        if (PunirAPI.getExistPunicao(UUIDtarget) == null) {
                                            punir(args, args[2], UUID, UUIDtarget, player, motivos, x);
                                        } else {
                                            player.sendMessage(ChatColor.RED + " * Ops, este player já se encontra punido!");
                                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        }
                                    } else if (args[2].contains("https://www.youtube.com/")) {
                                        final String UUIDtarget = PlayerUUID.getUUID(args[0]);

                                        if (PunirAPI.getExistPunicao(UUIDtarget) == null) {
                                            punir(args, args[2], UUID, UUIDtarget, player, motivos, x);
                                        } else {
                                            player.sendMessage(ChatColor.RED + " * Ops, este player já se encontra punido!");
                                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        }
                                    } else if (args[2].contains("https://imgur.com/")) {
                                        final String UUIDtarget = PlayerUUID.getUUID(args[0]);

                                        if (PunirAPI.getExistPunicao(UUIDtarget) == null) {
                                            punir(args, args[2], UUID, UUIDtarget, player, motivos, x);
                                        } else {
                                            player.sendMessage(ChatColor.RED + " * Ops, este player já se encontra punido!");
                                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        }
                                    } else {
                                        player.sendMessage(ChatColor.RED + " * Ops, a prova informada contém algum erro.");
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/punir (nick) (motivo) (prova)");
                }
            }
        }
        return false;
    }

}
