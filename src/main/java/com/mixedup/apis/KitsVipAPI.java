package com.mixedup.apis;

import com.mixedup.Main;
import com.mixedup.MySql;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class KitsVipAPI {

    public static void createCountdown(final String UUID, final int countdown) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Countdown_kitvips(UUID, Countdown) VALUES (?, ?)");
            st.setString(1, UUID);
            st.setInt(2, countdown);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateCountdown(final String UUID, final int countdown) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Countdown_kitvips SET Countdown = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setInt(1, countdown);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static int getCountdown(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Countdown_kitvips WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("Countdown");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void createCountdown6horas(final String UUID, final int countdown) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Countdown_kit6horas(UUID, Countdown) VALUES (?, ?)");
            st.setString(1, UUID);
            st.setInt(2, countdown);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateCountdown6horas(final String UUID, final int countdown) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Countdown_kit6horas SET Countdown = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setInt(1, countdown);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static int getCountdown6horas(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Countdown_kit6horas WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("Countdown");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
