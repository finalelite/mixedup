package com.mixedup.apis;

import com.mixedup.MySqlHub;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TagAPI implements Listener {

    public static String getTag(final String UUID) {
        try {
            return TagCache.get(UUID).getTag();
        } catch (final Exception e) {
            new TagCache(UUID, getTagSQL(UUID)).insert();
            return getTag(UUID);
        }
    }

    public static String getTagSQL(final String UUID) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Tag_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getString("Tag");
            } else {
                if (UUID != null) {
                    setTag(UUID, "Membro");
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void setTag(final String UUID, final String tag) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("INSERT INTO Tag_data (UUID, Tag) VALUES (?, ?)");
            st.setString(1, UUID);
            st.setString(2, tag);
            st.executeUpdate();

            new TagCache(UUID, tag).insert();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateTag(final String UUID, final String tag) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("UPDATE Tag_data SET Tag = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setString(1, tag);
            st.executeUpdate();

            TagCache.get(UUID).setTag(tag);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    //----------------------------------------------------------------------------------------------------------------


    @EventHandler
    public void onJoin(final PlayerJoinEvent event) {
        final String UUID = event.getPlayer().getUniqueId().toString();
        new TagCache(UUID, getTagSQL(UUID)).insert();
    }

    @EventHandler
    public void onLeft(final PlayerQuitEvent event) {
        final String UUID = event.getPlayer().getUniqueId().toString();
        TagCache.CACHE.remove(UUID);
    }
}
