package com.mixedup.apis;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.mixedup.MySql;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.val;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

public class GuildaAPI {

    // static abuse, :(
    private static LoadingCache<String, GuildaAPI.GuildData> cache;

    static {
        GuildaAPI.clearCache();
    }

    public static void clearCache() {
        GuildaAPI.cache = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterAccess(30, TimeUnit.MINUTES)
                .build(new CacheLoader<String, GuildaAPI.GuildData>() {
                    public GuildaAPI.GuildData load(final String uuid) {
                        return GuildaAPI.getFromSQL(uuid);
                    }
                });
    }

    private static GuildaAPI.GuildData getFromSQL(final String uuid) {
        try (final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Guilda_data WHERE UUID = ?")) {
            st.setString(1, uuid);

            final val rs = st.executeQuery();
            if (rs.next())
                return new GuildaAPI.GuildData(rs.getString("Guilda"), rs.getInt("Rank"));
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static GuildaAPI.GuildData getGuildData(final String uuid) {
        try {
            return GuildaAPI.cache.get(uuid);
        } catch (final Exception e) {
            return null;
        }
    }

    public static String getGuilda(final String uuid) {
        try {
            return GuildaAPI.getGuildData(uuid).getGuild();
        } catch (final Exception e) {
            return null;
        }
    }

    public static int getGuildaRank(final String uuid) {
        return GuildaAPI.getGuildData(uuid).getRank();
    }

    public static boolean hasGuilda(final String uuid) {
        return GuildaAPI.getGuildData(uuid) != null;
    }

    public static void setGuilda(final String uuid, final String guild) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Guilda_data (UUID, Guilda, Rank) VALUES (?, ?, ?)");
            st.setString(1, uuid);
            st.setString(2, guild);
            st.setInt(3, 1);
            st.executeUpdate();

            GuildaAPI.getGuildData(uuid).setGuild(guild);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateRank(final String uuid) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Guilda_data SET Rank = ? WHERE UUID = ?");
            st.setString(2, uuid);
            st.setInt(1, GuildaAPI.getGuildaRank(uuid) + 1);
            st.executeUpdate();

            GuildaAPI.getGuildData(uuid).setRank(GuildaAPI.getGuildaRank(uuid) + 1);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateGuilda(final String uuid, final String guilda) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Guilda_data SET Guilda = ? WHERE UUID = ?");
            st.setString(2, uuid);
            st.setString(1, guilda);
            st.executeUpdate();

            GuildaAPI.getGuildData(uuid).setGuild(guilda);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    @AllArgsConstructor
    @Getter
    @Setter
    public static class GuildData {
        private String guild;
        private int rank;
    }
}
