package com.mixedup.apis;

import com.mixedup.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class WarpAPI {

    private static HashMap<String, Map<String, String>> cache = new HashMap<>();

    public static void createWarp(final String UUID, final String warp, final String location) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Warps_data(UUID, Warp, Location) VALUES (?, ?, ?)");
            st.setString(1, UUID);
            st.setString(2, warp);
            st.setString(3, location);
            st.executeUpdate();
            cache.get(UUID).put(warp, location);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeCache(String uuid) {

        cache.remove(uuid);

    }

    public static void addCache(String uuid) {

        Map<String, String> warps = new HashMap<>();

        try {

            PreparedStatement stm = MySql.con.prepareStatement("SELECT * FROM Warps_data WHERE UUID = ?");
            stm.setString(1, uuid);

            ResultSet rs = stm.executeQuery();

            while (rs.next()) {

                warps.put(rs.getString("Warp"), rs.getString("Location"));

            }

            cache.put(uuid, warps);

        } catch (SQLException e) {

            e.printStackTrace();

        }

    }

    public static List<String> getAllWarps(final String uuid) {

        return new ArrayList<>(cache.get(uuid).keySet());

    }

    public static String getLocation(final String uuid, final String warp) {

        if(cache.get(uuid).get(warp) != null) {

            return cache.get(uuid).get(warp);

        } else {

            return null;

        }

    }

    public static void removeWarp(final String warp, final String uuid) {
        try {

            final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Warps_data WHERE Warp = ? And UUID = ?");
            st.setString(1, warp);
            st.setString(2, uuid);
            st.executeUpdate();

            cache.get(uuid).remove(warp);

        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }
}
