package com.mixedup.apis;

import com.mixedup.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class InventoryAPI {

    public static void createInventory(final String UUID, final String inventory) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Inventory_data(UUID, Inventory) VALUES (?, ?)");
            st.setString(1, UUID);
            st.setString(2, inventory);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getInventory(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Inventory_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Inventory");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void removeInventory(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Inventory_data WHERE UUID = ?");
            st.setString(1, UUID);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }
}
