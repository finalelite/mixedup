package com.mixedup.apis;

import com.mixedup.MySql;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EnergiaAPI implements Listener {

    //Só utilizado uma vez
    public static void setInfo(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Energia_data(UUID, Energia) VALUES (?, ?)");
            st.setString(1, UUID);
            st.setDouble(2, 0);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }
    //------

    public static void updateEnergia(final String UUID, final double energia) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Energia_data SET Energia = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setDouble(1, energia);
            st.executeUpdate();

            EnergiaCache.get(UUID).setEnergia(energia);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static double getEnergia(final String UUID) {
        try {
            return EnergiaCache.get(UUID).getEnergia();
        } catch (final Exception e) {
            return 0.0;
        }
    }

    public static double getEnergiaSQL(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Energia_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getDouble("Energia");
            } else {
                EnergiaAPI.setInfo(UUID);
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0.0;
    }


    //----------------------------------------------------------------------------------------------------------------


    @EventHandler
    public void onJoin(final PlayerJoinEvent event) {
        final String UUID = event.getPlayer().getUniqueId().toString();
        new EnergiaCache(UUID, EnergiaAPI.getEnergiaSQL(UUID)).insert();
    }

    @EventHandler
    public void onLeft(final PlayerQuitEvent event) {
        final String UUID = event.getPlayer().getUniqueId().toString();
        EnergiaCache.CACHE.remove(UUID);
    }
}
