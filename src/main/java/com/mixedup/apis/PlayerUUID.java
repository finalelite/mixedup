package com.mixedup.apis;

import com.mixedup.MySqlHub;
import org.bukkit.Bukkit;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class PlayerUUID {

    public static String getUUIDsql(final String nick) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Account_data WHERE Nick = ?");
            st.setString(1, nick);
            final ResultSet rs = st.executeQuery();
            //while (rs.next()) {
            //    return rs.getString("UUID");
            //}
            if (rs.next()) {
                final String uuid = rs.getString("UUID");
                return uuid;
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getUUIDcache(final String nick) {
        return getUUID(nick);
    }

    public static String getUUID(String nick) {
        if (Bukkit.getPlayer(nick) != null) {
            return Bukkit.getPlayer(nick).getUniqueId().toString();
        } else if (get(nick) != null) {
            return get(nick).getUUID();
        } else {
            final String UUID = getUUIDsql(nick);
            new PlayerUUID(nick, UUID).insert();

            return UUID;
        }
    }

    public static String getWithMojang(final String name) {
        final String output = readURL("https://api.mojang.com/users/profiles/minecraft/" + name);

        if (output.isEmpty()) {
            return null;
        } else {
            return output.substring(7, 39);
        }
    }

    private static String readURL(final String url) {
        try {
            final HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();

            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "SkinsRestorer");
            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);
            con.setDoOutput(true);

            final StringBuilder output = new StringBuilder();
            final BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                output.append(line);
            }
            in.close();

            return output.toString();
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static HashMap<String, PlayerUUID> CACHE = new HashMap<>();

    private String nick;
    private String UUID;

    public PlayerUUID(String nick, String UUID) {
        this.nick = nick;
        this.UUID = UUID;
    }

    public static PlayerUUID get(String nick) {
        return CACHE.get(nick);
    }

    public PlayerUUID insert() {
        CACHE.put(this.nick, this);
        return this;
    }

    public String getUUID() {
        return this.UUID;
    }

    public void setUUID(final String UUID) {
        this.UUID = UUID;
    }
}
