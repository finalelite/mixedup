package com.mixedup.apis;

import com.mixedup.Main;
import com.mixedup.MySql;
import com.mixedup.hashs.WorldTimeHash;
import org.bukkit.Bukkit;
import org.bukkit.World;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class WorldAPI {

    public static void createWorldInfo(final String world, final boolean time, final boolean weather) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO World_data(World, TimeLocked, WeatherLocked) VALUES (?, ?, ?)");
            st.setString(1, world);
            st.setBoolean(2, time);
            st.setBoolean(3, weather);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getExistWorld(final String world) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM World_data WHERE World = ?");
            st.setString(1, world);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("World");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean getWeatherLocked(final String world) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM World_data WHERE World = ?");
            st.setString(1, world);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("WeatherLocked");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean getTimeLocked(final String world) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM World_data WHERE World = ?");
            st.setString(1, world);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("TimeLocked");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void updateWeatherLock(final String world, final boolean weather) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE World_data SET WeatherLocked = ? WHERE World = ?");
            st.setString(2, world);
            st.setBoolean(1, weather);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateTimeLock(final String world, final boolean time) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE World_data SET TimeLocked = ? WHERE World = ?");
            st.setString(2, world);
            st.setBoolean(1, time);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateTime() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
            @Override
            public void run() {
                for (final World world : Bukkit.getWorlds()) {
                    if (WorldTimeHash.get(world.getName()) == null) {
                        if (WorldAPI.getExistWorld(world.getName()) != null) {
                            new WorldTimeHash(world.getName(), WorldAPI.getTimeLocked(world.getName()), WorldAPI.getWeatherLocked(world.getName())).insert();
                        } else {
                            new WorldTimeHash(world.getName(), false, false).insert();
                        }
                    }
                    if (WorldTimeHash.get(world.getName()).getTime() == true) {
                        world.setTime(world.getTime() - 60);
                    }
                }
            }
        }, 60L, 60L);
    }
}
