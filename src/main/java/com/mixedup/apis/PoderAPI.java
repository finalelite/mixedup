package com.mixedup.apis;

import com.mixedup.Main;
import com.mixedup.MySql;
import com.mixedup.factions.FacAPI;
import lombok.val;
import org.bukkit.Bukkit;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class PoderAPI {

    public static Map<String, Integer> poderCache = new HashMap<>();

    public static void createInfo(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Poder_data(UUID, Poder) VALUES (?, ?)");
            st.setString(1, UUID);
            st.setDouble(2, 5);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static int getTotalPoder(final String uuid) {
        final val facName = FacAPI.getFacNome(uuid);
        return Bukkit.getOnlinePlayers().stream()
                .filter(pla ->
                        Objects.equals(FacAPI.getFacNome(pla.getUniqueId().toString()), facName))
                .mapToInt(pla -> PoderAPI.getPoder(pla.getUniqueId().toString())).sum();
    }

    public static int getPoder(final String UUID) {
        if (PoderAPI.poderCache.containsKey(UUID))
            return PoderAPI.poderCache.get(UUID);

        else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Poder_data WHERE UUID = ?");
                st.setString(1, UUID);
                final ResultSet rs = st.executeQuery();
                if (rs.next()) {
                    final val poder = rs.getInt("Poder");
                    PoderAPI.poderCache.put(UUID, poder);
                    return poder;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public static String getExist(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Poder_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("UUID");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void updatePoder(final String UUID, final int poder) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Poder_data SET Poder = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setDouble(1, poder);
            st.executeUpdate();
            PoderAPI.poderCache.put(UUID, poder);

            final val facName = FacAPI.getFacNome(UUID);
            Bukkit.getOnlinePlayers().stream()
                    .filter(p -> FacAPI.getFacNome(p.getUniqueId().toString()) != null && FacAPI.getFacNome(p.getUniqueId().toString()).equals(facName))
                    .forEach(p -> {
                        if (!Main.scoreboardManager.hasSquadScoreboard(p))
                            return;

                        Main.scoreboardManager.getSquadScoreboard().updateEntry(p, "user_power");
                        Main.scoreboardManager.getSquadScoreboard().updateEntry(p, "squad_power");
                    });

        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updatePoder() {
        new Thread(() -> {
            while (true) {

                try {
                    Thread.sleep(1000 * 60 * 10);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try (val st = MySql.con.prepareStatement("UPDATE Poder_data SET Poder = Poder + 1 WHERE Poder < 15 AND INSTR(?, UUID) != 0")) {
                    st.setString(1, Bukkit.getOnlinePlayers().stream()
                            .map(p -> p.getUniqueId().toString())
                            .collect(Collectors.joining(" ")));
                    st.executeUpdate();
                    PoderAPI.poderCache.replaceAll((s, integer) -> integer >= 15 ? 15 : integer + 1);
                    Bukkit.getOnlinePlayers()
                            .forEach(p -> {
                                if (!Main.scoreboardManager.hasSquadScoreboard(p))
                                    return;

                                Main.scoreboardManager.getSquadScoreboard().updateEntry(p, "user_power");
                                Main.scoreboardManager.getSquadScoreboard().updateEntry(p, "squad_power");
                            });
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

}
