package com.mixedup.apis;

import org.bukkit.ChatColor;

public class HorarioAPI {

    public static String getTime(final long secs) {

        final long days = secs / 86400;
        final long hours = (secs % 86400) / 3600;
        final long minutes = (secs % 3600) / 60;
        final long seconds = secs % 60;

        final StringBuilder sb = new StringBuilder();
        if (days >= 1) {
            sb.append(days);
            sb.append(days == 1 ? " dia " : " dias ");
        }
        if (hours >= 1) {
            sb.append(hours);
            sb.append(hours == 1 ? " hora " : " horas ");
        }
        if (minutes >= 1) {
            sb.append(minutes);
            sb.append(minutes == 1 ? " minuto " : " minutos ");
        }
        if (seconds >= 1) {
            sb.append("e ");
            sb.append(seconds);
            sb.append(seconds == 1 ? " segundo " : " segundos ");
        }
        return sb.toString().trim();
    }

    public static String getTimeJetpack(final long secs) {

        final long days = secs / 86400;
        final long hours = (secs % 86400) / 3600;
        final long minutes = (secs % 3600) / 60;
        final long seconds = secs % 60;

        final StringBuilder sb = new StringBuilder();
        if (days >= 1) {
            sb.append(days);
            sb.append(days == 1 ? " dia " : " dias ");
        }
        if (hours >= 1) {
            sb.append(hours);
            sb.append(hours == 1 ? " hora " : " horas ");
        }
        if (minutes >= 1) {
            sb.append(minutes);
            sb.append(minutes == 1 ? " minuto " : " minutos ");
        }
        if (seconds > 1) {
            sb.append(seconds);
            sb.append(seconds == 1 ? " segundo " : " segundos ");
        }
        if (seconds == 1) {
            return ChatColor.RED + " Sem energia!";
        }
        return sb.toString().trim();
    }
}
