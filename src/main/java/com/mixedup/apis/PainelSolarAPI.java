package com.mixedup.apis;

import com.mixedup.Main;
import com.mixedup.MySql;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PainelSolarAPI {

    public static void setPainel(final String UUID, final String location, final String forca, final int ID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Paineis_data(UUID, Location, Forca, ID) VALUES (?, ?, ?, ?)");
            st.setString(1, UUID);
            st.setString(2, location);
            st.setString(3, forca);
            st.setInt(4, ID);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateUUID(final String UUID, final String newUUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Paineis_data SET UUID = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setString(1, newUUID);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateID(final String UUID, final int ID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Paineis_data SET ID = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setInt(1, ID);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removePainelFromUUID(final String UUID) {
        int ID = 0;
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Paineis_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                ID = rs.getInt("ID");
            }

            final PreparedStatement st3 = MySql.con.prepareStatement("DELETE FROM Paineis_data WHERE UUID = ?");
            st3.setString(1, UUID);
            st3.executeUpdate();

            for (int i = ID + 1; i <= 25; i++) {
                if (PainelSolarAPI.getForcaFromUUID(UUID.substring(0, UUID.length() - 1) + i) != null) {
                    PainelSolarAPI.updateID(UUID.substring(0, UUID.length() - 1) + i, i - 1);
                    PainelSolarAPI.updateUUID(UUID.substring(0, UUID.length() - 1) + i, UUID.substring(0, UUID.length() - 1) + (i - 1));
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removePainel(final String location, final String UUID) {
        int ID = 0;
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Paineis_data WHERE Location = ?");
            st.setString(1, location);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                ID = rs.getInt("ID");
            }

            final PreparedStatement st3 = MySql.con.prepareStatement("DELETE FROM Paineis_data WHERE Location = ?");
            st3.setString(1, location);
            st3.executeUpdate();

            for (int i = ID + 1; i <= 30; i++) {
                try {
                    final PreparedStatement st2 = MySql.con.prepareStatement("SELECT * FROM Paineis_data WHERE UUID = ?");
                    st2.setString(1, UUID + ID);
                    final ResultSet rs2 = st2.executeQuery();
                    while (rs2.next()) {
                        PainelSolarAPI.updateID(UUID, ID - 1);
                        PainelSolarAPI.updateUUID(UUID + ID, UUID + (ID - 1));
                    }
                } catch (final SQLException e) {
                    continue;
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getForca(final String location) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Paineis_data WHERE Location = ?");
            st.setString(1, location);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Forca");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getForcaFromUUID(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Paineis_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Forca");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getPainelExist(final String location) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Paineis_data WHERE Location = ?");
            st.setString(1, location);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Location");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int getLastPainel(final String UUID) {
        int valor = 0;
        for (int i = 30; i >= 1; i--) {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Paineis_data WHERE UUID = ?");
                st.setString(1, UUID + i);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    valor = i;
                }
                if (valor != 0) {
                    i = 0;
                    break;
                }
            } catch (final SQLException e) {
                continue;
            }
        }
        return valor;
    }

    public static void updateEnergia() {
        final Thread thread = new Thread(() -> {
            while (true) {
                try {
                    Thread.currentThread().sleep(1000 * 300);
                } catch (final Exception e) {
                    e.printStackTrace();
                }

                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE Energia_data SET Energia  = Energia + ((SELECT COUNT(*) FROM Paineis_data WHERE Paineis_data.UUID = Energia_data.UUID AND Paineis_data.Forca = 100) * 0.00225) +((SELECT COUNT(*) FROM Paineis_data WHERE Paineis_data.UUID = Energia_data.UUID AND Paineis_data.Forca = 200) * 0.0018)");
                    st.executeUpdate();

                    for (final Player target : Bukkit.getOnlinePlayers()) {
                        EnergiaAPI.updateEnergia(target.getUniqueId().toString(), EnergiaAPI.getEnergiaSQL(target.getUniqueId().toString()));
                        if (!Main.scoreboardManager.hasSquadScoreboard(target)) {
                            Main.scoreboardManager.getDefaultScoreboard().updateEntry(target, "power");
                        }
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();

        //Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
        //            @Override
        //            public void run() {
        //                try {
        //                    PreparedStatement st = MySql.con.prepareStatement("UPDATE Energia_data SET Energia  = Energia + ((SELECT COUNT(*) FROM Paineis_data WHERE Paineis_data.UUID = Energia_data.UUID AND Paineis_data.Forca = 100) * 0.00225) +((SELECT COUNT(*) FROM Paineis_data WHERE Paineis_data.UUID = Energia_data.UUID AND Paineis_data.Forca = 200) * 0.0018)");
        //                    st.executeUpdate();
        //
        //                    for (Player target : Bukkit.getOnlinePlayers()) {
        //                        if (BackEnd.scoreboard.get(target.getUniqueId()) != null) {
        //                            BackEnd.scoreboard.get(target.getUniqueId()).updateScore(target);
        //                        }
        //                    }
        //                } catch (SQLException e) {
        //                    e.printStackTrace();
        //                }
        //            }
        //        }, 0L, 6000L);
        //-----------------------------------------------------------------------------
        //Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
        //            @Override
        //            public void run() {
        //                String UUID = null;
        //                try {
        //                    PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Energia_data");
        //                    ResultSet rs = st.executeQuery();
        //                    while (rs.next()) {
        //                        UUID = rs.getString("UUID");
        //
        //                        //--
        //                        for (int i = getLastPainel(UUID); i > 0; i--) {
        //                            try {
        //                                PreparedStatement st2 = MySql.con.prepareStatement("SELECT * FROM Paineis_data WHERE UUID = ?");
        //                                st2.setString(1, UUID + String.valueOf(i));
        //                                ResultSet rs2 = st2.executeQuery();
        //                                while (rs2.next()) {
        //                                    if (rs2.getString("Forca").equals("100")) {
        //                                        EnergiaAPI.updateEnergia(UUID, EnergiaAPI.getEnergia(UUID) + 0.00045);
        //                                    } else if (rs2.getString("Forca").equals("200")) {
        //                                        EnergiaAPI.updateEnergia(UUID, EnergiaAPI.getEnergia(UUID) + 0.0009);
        //                                    }
        //                                }
        //                            } catch (SQLException e) {
        //                                continue;
        //                            }
        //                        }
        //                        //--
        //                    }
        //                    for (Player target : Bukkit.getOnlinePlayers()) {
        //                        String UUIDtarget = PlayerUUID.getUUID(target.getName());
        //
        //                        if (UUIDtarget.equals(UUID)) {
        //                            BackEnd.scoreboard.get(target.getUniqueId()).updateScore(target);
        //                        }
        //                    }
        //                } catch (SQLException e) {
        //                    e.printStackTrace();
        //                }
        //            }
        //        }, 0L, 1200L);
    }
}
