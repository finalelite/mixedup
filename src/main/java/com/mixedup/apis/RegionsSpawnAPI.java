package com.mixedup.apis;

import com.mixedup.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class RegionsSpawnAPI {

    public static HashMap<String, RegionsSpawnAPI> CACHE = new HashMap<>();


    //-------------------------------------------------------------------------------------------------------------
    private final String region;
    private String location;

    public RegionsSpawnAPI(final String region, final String location) {
        this.region = region;
        this.location = location;
    }

    public static String getLocation(final String regionName) {
        if (RegionsSpawnAPI.get(regionName) != null) {
            return RegionsSpawnAPI.get(regionName).getLocation();
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Regions_spawn WHERE Region = ?");
                st.setString(1, regionName);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final String location = rs.getString("Location");
                    new RegionsSpawnAPI(regionName, location).insert();

                    return location;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public static RegionsSpawnAPI get(final String region) {
        return RegionsSpawnAPI.CACHE.get(region);
    }

    public RegionsSpawnAPI insert() {
        RegionsSpawnAPI.CACHE.put(region, this);
        return this;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(final String location) {
        this.location = location;
    }
}
