package com.mixedup.apis;

import com.mixedup.MySql;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConfigAPI implements Listener {

    public static void setInfos(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Config_infos(UUID, ChatLocal, ChatGlobal, Tell, Tpa, Depositos, Fly) VALUES (?, ?, ?, ?, ?, ?, ?)");
            st.setString(1, UUID);
            st.setBoolean(2, true);
            st.setBoolean(3, true);
            st.setBoolean(4, true);
            st.setBoolean(5, true);
            st.setBoolean(6, true);
            st.setBoolean(7, false);
            st.executeUpdate();

            new ConfigCache(UUID, true, true, true, true, true, false).insert();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeInfo(final String uuid) {
        ConfigCache.CACHE.remove(uuid);
    }

    public static String getExist(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Config_infos WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("UUID");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void setCache(final String uuid) {
        new ConfigCache(uuid, ConfigAPI.getChatLocalStatusSQL(uuid), ConfigAPI.getChatGlobalStatusSQL(uuid), ConfigAPI.getTellStatusSQL(uuid), ConfigAPI.getTpaStatusSQL(uuid), ConfigAPI.getDepositosStatusSQL(uuid), ConfigAPI.getFlyStatusSQL(uuid)).insert();
    }

    public static boolean getFlyStatus(final String UUID) {
        return ConfigCache.get(UUID).getFly();
    }

    public static boolean getFlyStatusSQL(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Config_infos WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Fly");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean getDepositosStatus(final String uuid) {
        return ConfigCache.get(uuid).getPayments();
    }

    public static boolean getDepositosStatusSQL(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Config_infos WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Depositos");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean getTpaStatus(final String uuid) {
        return ConfigCache.get(uuid).getTpa();
    }

    public static boolean getTpaStatusSQL(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Config_infos WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Tpa");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean getTellStatus(final String uuid) {
        return ConfigCache.get(uuid).getTell();
    }

    public static boolean getTellStatusSQL(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Config_infos WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Tell");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean getChatGlobalStatus(final String uuid) {
        if (ConfigCache.get(uuid) != null) {
            return ConfigCache.get(uuid).getChatGlobal();
        } else {
            return false;
        }
    }

    public static boolean getChatGlobalStatusSQL(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Config_infos WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("ChatGlobal");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean getChatLocalStatus(final String uuid) {
        if (ConfigCache.get(uuid) != null) {
            return ConfigCache.get(uuid).getChatLocal();
        } else {
            return false;
        }
    }

    public static boolean getChatLocalStatusSQL(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Config_infos WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("ChatLocal");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static void updateFly(final String UUID, final boolean status) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Config_infos SET Fly = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setBoolean(1, status);
            st.executeUpdate();

            ConfigCache.get(UUID).setFly(status);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateDepositos(final String UUID, final boolean status) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Config_infos SET Depositos = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setBoolean(1, status);
            st.executeUpdate();

            ConfigCache.get(UUID).setPayments(status);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateTpa(final String UUID, final boolean status) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Config_infos SET Tpa = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setBoolean(1, status);
            st.executeUpdate();

            ConfigCache.get(UUID).setTpa(status);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateTell(final String UUID, final boolean status) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Config_infos SET Tell = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setBoolean(1, status);
            st.executeUpdate();

            ConfigCache.get(UUID).setTell(status);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateChatLocal(final String UUID, final boolean status) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Config_infos SET ChatLocal = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setBoolean(1, status);
            st.executeUpdate();

            ConfigCache.get(UUID).setChatLocal(status);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateChatGlobal(final String UUID, final boolean status) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Config_infos SET ChatGlobal = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setBoolean(1, status);
            st.executeUpdate();

            ConfigCache.get(UUID).setChatGlobal(status);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }


    //---------------------------------------------------------------------------------------------------------------------


    @EventHandler
    public void onJoin(final PlayerJoinEvent event) {
        final String UUID = event.getPlayer().getUniqueId().toString();

        if (ConfigAPI.getExist(UUID) == null) {
            ConfigAPI.setInfos(UUID);
        } else {
            ConfigAPI.setCache(UUID);
        }
    }

    @EventHandler
    public void onLeft(final PlayerQuitEvent event) {
        ConfigAPI.removeInfo(event.getPlayer().getUniqueId().toString());
    }
}
