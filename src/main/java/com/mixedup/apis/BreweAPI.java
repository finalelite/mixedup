package com.mixedup.apis;

import com.mixedup.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BreweAPI {

    public static void createInfo(final String location, final String block) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Brewe_data(Location, Bloco) VALUES (?, ?)");
            st.setString(1, location);
            st.setString(2, block);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateInfo(final String location, final String bloco) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Brewe_data SET Bloco = ? WHERE Location = ?");
            st.setString(2, location);
            st.setString(1, bloco);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getExist(final String location) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Brewe_data WHERE Location = ?");
            st.setString(1, location);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Location");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getBloco(final String location) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Brewe_data WHERE Location = ?");
            st.setString(1, location);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Bloco");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void removeInfo(final String location) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Brewe_data WHERE Location = ?");
            st.setString(1, location);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }
}
