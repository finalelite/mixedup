package com.mixedup.apis;

import com.mixedup.MySqlHub;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

public class AccountAPI {

    public static HashMap<String, AccountAPI> CACHE = new HashMap<>();
    public static HashMap<String, String> NAME = new HashMap<>();
    private final String uuid;
    private String nick;

    public AccountAPI(final String uuid, final String nick) {
        this.uuid = uuid;
        this.nick = nick;
    }

    public static void setAccountInfo(final String UUID, final String nick) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("INSERT INTO Account_data(UUID, Nick) VALUES (?, ?)");
            st.setString(1, UUID);
            st.setString(2, nick);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateNick(final String UUID, final String nick) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("UPDATE Account_data SET Nick = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setString(1, nick);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }


    //------------------------------------------------------------------------------------------------------------------

    public static String getNick(final String uuid) {
        Player player = Bukkit.getPlayer(UUID.fromString(uuid));
        if (player != null && player.isOnline()) {
            return player.getName();
        } else if (NAME.containsKey(uuid)) {
            return NAME.get(uuid);
        }

        String nick = getNickfromUUID(uuid);
        if (nick != null) {
            NAME.put(uuid, getNickfromUUID(uuid));
        } else return null;

        return getNick(uuid);
    }

    public static String getNickfromUUID(final String uuid) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Account_data WHERE UUID = ?");
            st.setString(1, uuid);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Nick");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getExistNick(final String nick) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Account_data WHERE Nick = ?");
            st.setString(1, nick);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Nick");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int getIDplayer(final String nick) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Account_data WHERE Nick = ?");
            st.setString(1, nick);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("ID");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static AccountAPI get(final String uuid) {
        return AccountAPI.CACHE.get(uuid);
    }

    public AccountAPI insert() {
        return AccountAPI.CACHE.put(uuid, this);
    }

    public String getNick() {
        return nick;
    }

    public void setNick(final String nick) {
        this.nick = nick;
    }
}
