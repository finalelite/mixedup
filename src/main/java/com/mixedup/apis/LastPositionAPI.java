package com.mixedup.apis;

import java.util.HashMap;

public class LastPositionAPI {

    public static HashMap<String, LastPositionAPI> CACHE = new HashMap<>();
    private final String uuid;
    private String location;


    //------------------------------------------------------------------------------------------------------------------


    public LastPositionAPI(final String uuid, final String location) {
        this.uuid = uuid;
        this.location = location;
    }

    public static void createFirstLocation(final String UUID, final String location) {
        new LastPositionAPI(UUID, location).insert();
    }

    public static void updateLocation(final String UUID, final String location) {
        if (LastPositionAPI.get(UUID) != null) {
            LastPositionAPI.get(UUID).setLocation(location);
        } else {
            new LastPositionAPI(UUID, location).insert();
        }
    }

    public static String getLastLocation(final String UUID) {
        if (LastPositionAPI.get(UUID) == null) return null;
        return LastPositionAPI.get(UUID).getLocation();
    }

    public static LastPositionAPI get(final String uuid) {
        return LastPositionAPI.CACHE.get(uuid);
    }

    public LastPositionAPI insert() {
        LastPositionAPI.CACHE.put(uuid, this);
        return this;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(final String location) {
        this.location = location;
    }
}
