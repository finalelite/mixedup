package com.mixedup.apis;

import java.util.HashMap;

public class EnergiaCache {

    public static HashMap<String, EnergiaCache> CACHE = new HashMap<>();
    private final String UUID;
    private double energia;

    public EnergiaCache(final String UUID, final double energia) {
        this.UUID = UUID;
        this.energia = energia;
    }

    public static EnergiaCache get(final String UUID) {
        return EnergiaCache.CACHE.get(UUID);
    }

    public EnergiaCache insert() {
        EnergiaCache.CACHE.put(UUID, this);

        return this;
    }

    public double getEnergia() {
        return this.energia;
    }

    public void setEnergia(final double energia) {
        this.energia = energia;
    }
}
