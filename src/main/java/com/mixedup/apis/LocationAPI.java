package com.mixedup.apis;

import com.mixedup.MySqlHub;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LocationAPI {

    public static void setInfo(final String UUID, final String server) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("INSERT INTO Player_location(UUID, Location) VALUES (?, ?)");
            st.setString(1, UUID);
            st.setString(2, server);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateServer(final String UUID, final String server) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("UPDATE Player_location SET Location = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setString(1, server);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getServer(final String UUID) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Player_location WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Location");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
