package com.mixedup.apis;

import com.mixedup.MySql;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MobspawnAPI {

    public static void createLog(String location, String type, String player) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Mobspawn_log(Location, Type, Player, Date) VALUES (?, ?, ?, ?)");
            st.setString(1, location);
            st.setString(2, type);
            st.setString(3, player);
            st.setDate(4, new Date(new java.util.Date().getTime()));
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getExist(String location) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Mobspawn_data WHERE Location = ?");
            st.setString(1, location);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Location");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void setInfo(String location) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Mobspawn_data(Location) VALUES (?)");
            st.setString(1, location);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeInfo(String location) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Mobspawn_data WHERE Location = ?");
            st.setString(1, location);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
