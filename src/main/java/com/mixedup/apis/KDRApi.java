package com.mixedup.apis;

import com.mixedup.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class KDRApi {

    public static void createKDR(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO KDR_data(UUID, Kills, Deaths) VALUES (?, ?, ?)");
            st.setString(1, UUID);
            st.setInt(2, 0);
            st.setInt(3, 0);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getExistInfo(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM KDR_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("UUID");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static int getDeaths(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM KDR_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("Deaths");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }


    public static int getKills(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM KDR_data WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("Kills");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void updateKills(final String UUID, final int kills) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE KDR_data SET Kills = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setInt(1, kills);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateDeaths(final String UUID, final int deaths) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE KDR_data SET Deaths = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setInt(1, deaths);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }
}
