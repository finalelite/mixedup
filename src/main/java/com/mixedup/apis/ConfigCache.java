package com.mixedup.apis;

import java.util.HashMap;

public class ConfigCache {

    public static HashMap<String, ConfigCache> CACHE = new HashMap<>();
    private final String uuid;
    private boolean chatLocal;
    private boolean chatGlobal;
    private boolean tell;
    private boolean tpa;
    private boolean payments;
    private boolean fly;

    public ConfigCache(final String uuid, final boolean chatLocal, final boolean chatGlobal, final boolean tell, final boolean tpa, final boolean payments, final boolean fly) {
        this.uuid = uuid;
        this.chatLocal = chatLocal;
        this.chatGlobal = chatGlobal;
        this.tell = tell;
        this.tpa = tpa;
        this.payments = payments;
        this.fly = fly;
    }

    public static ConfigCache get(final String uuid) {
        return ConfigCache.CACHE.get(uuid);
    }

    public ConfigCache insert() {
        ConfigCache.CACHE.put(uuid, this);
        return this;
    }

    public boolean getChatLocal() {
        return chatLocal;
    }

    public void setChatLocal(final boolean chatLocal) {
        this.chatLocal = chatLocal;
    }

    public boolean getChatGlobal() {
        return chatGlobal;
    }

    public void setChatGlobal(final boolean chatGlobal) {
        this.chatGlobal = chatGlobal;
    }

    public boolean getTell() {
        return tell;
    }

    public void setTell(final boolean tell) {
        this.tell = tell;
    }

    public boolean getTpa() {
        return tpa;
    }

    public void setTpa(final boolean tpa) {
        this.tpa = tpa;
    }

    public boolean getPayments() {
        return payments;
    }

    public void setPayments(final boolean payments) {
        this.payments = payments;
    }

    public boolean getFly() {
        return fly;
    }

    public void setFly(final boolean fly) {
        this.fly = fly;
    }
}
