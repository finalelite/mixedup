package com.mixedup.apis;

import com.mixedup.MySql;
import com.mixedup.hashs.FurnaceCache;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FurnaceAPI {

    public static void createFurnace(final String UUID) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Furnace_data(UUID, Has) VALUES (?, ?)");
            st.setString(1, UUID);
            st.setBoolean(2, true);
            st.executeUpdate();

            if (FurnaceCache.get(UUID) == null) {
                new FurnaceCache(UUID, true).insert();
            } else {
                FurnaceCache.get(UUID).setHas(true);
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean has(final String UUID) {
        if (FurnaceCache.get(UUID) == null) {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Furnace_data WHERE UUID = ?");
                st.setString(1, UUID);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final boolean has = rs.getBoolean("Has");
                    new FurnaceCache(UUID, has).insert();
                    return has;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            new FurnaceCache(UUID, false).insert();
        } else {
            return FurnaceCache.get(UUID).has();
        }
        return false;
    }
}
