package com.mixedup.apis;

import com.mixedup.Main;
import com.mixedup.MySql;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class LojaPosAPI {

    public static void createLojaPos(final String UUID, final String location) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO LojaLoc_data(UUID, Location) VALUES (?, ?)");
                    st.setString(1, UUID);
                    st.setString(2, location);
                    st.executeUpdate();

                    new LojaPosAPI(UUID, location).insert();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void updateLocation(final String UUID, final String location) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("UPDATE LojaLoc_data SET Location = ? WHERE UUID = ?");
                    st.setString(2, UUID);
                    st.setString(1, location);
                    st.executeUpdate();

                    get(UUID).setLocation(location);
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static String getLocation(String UUID) {
        if (get(UUID) != null) {
            return get(UUID).getLocation();
        }
        return null;
    }

    public static void setCache() {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM LojaLoc_data");
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                new LojaPosAPI(rs.getString("UUID"), rs.getString("Location")).insert();
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeLojaPos(final String UUID) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM LojaLoc_data WHERE UUID = ?");
                    st.setString(1, UUID);
                    st.executeUpdate();

                    cache.remove(UUID);
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }


    //--------------------


    private String uuid;
    private String location;

    public static HashMap<String, LojaPosAPI> cache = new HashMap<>();

    public LojaPosAPI(String uuid, String location) {
        this.uuid = uuid;
        this.location = location;
    }

    public static LojaPosAPI get(String uuid) {
        return cache.get(uuid);
    }

    public LojaPosAPI insert() {
        cache.put(this.uuid, this);
        return this;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
