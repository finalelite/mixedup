package com.mixedup.apis;

import com.mixedup.MySql;
import com.mixedup.hashs.BeaconCache;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BeaconAPI {

    public static void createBeacon(final String UUID, final String location) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Beacons_data(UUID, Location, Multiplos, Forca, Pressa, Regeneracao, Superpulo, Velocidade, Efeito1, Efeito2, Efeito3, Efeito4, Efeito5, Ativo1, Ativo2, Ativo3, Ativo4, Ativo5, Facstatus, Ativo) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            st.setString(1, UUID);
            st.setString(2, location);
            st.setInt(3, 0);
            st.setInt(4, 0);
            st.setInt(5, 0);
            st.setInt(6, 0);
            st.setInt(7, 0);
            st.setInt(8, 0);
            st.setInt(9, 0);
            st.setInt(10, 0);
            st.setInt(11, 0);
            st.setInt(12, 0);
            st.setInt(13, 0);
            st.setBoolean(14, false);
            st.setBoolean(15, false);
            st.setBoolean(16, false);
            st.setBoolean(17, false);
            st.setBoolean(18, false);
            st.setBoolean(19, false);
            st.setBoolean(20, false);
            st.executeUpdate();

            new BeaconCache(UUID, location, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, false, false, false, false, false, false, false).insert();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteBeacon(String location) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Beacons_data WHERE Location = ?");
            st.setString(1, location);
            st.executeUpdate();

            BeaconCache.CACHE.remove(location);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateNivelAtivoSpeed(final String location, final int nivel) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Beacons_data SET Efeito5 = ? WHERE Location = ?");
            st.setString(2, location);
            st.setInt(1, nivel);
            st.executeUpdate();

            BeaconCache.get(location).setEfeito5(nivel);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateNivelAtivoJump(final String location, final int nivel) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Beacons_data SET Efeito4 = ? WHERE Location = ?");
            st.setString(2, location);
            st.setInt(1, nivel);
            st.executeUpdate();

            BeaconCache.get(location).setEfeito4(nivel);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateNivelAtivoRegen(final String location, final int nivel) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Beacons_data SET Efeito3 = ? WHERE Location = ?");
            st.setString(2, location);
            st.setInt(1, nivel);
            st.executeUpdate();

            BeaconCache.get(location).setEfeito3(nivel);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateNivelAtivoPressa(final String location, final int nivel) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Beacons_data SET Efeito2 = ? WHERE Location = ?");
            st.setString(2, location);
            st.setInt(1, nivel);
            st.executeUpdate();

            BeaconCache.get(location).setEfeito2(nivel);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateNivelAtivoForca(final String location, final int nivel) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Beacons_data SET Efeito1 = ? WHERE Location = ?");
            st.setString(2, location);
            st.setInt(1, nivel);
            st.executeUpdate();

            BeaconCache.get(location).setEfeito1(nivel);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateOscilar(final String location, final boolean status) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Beacons_data SET Facstatus = ? WHERE Location = ?");
            st.setString(2, location);
            st.setBoolean(1, status);
            st.executeUpdate();

            BeaconCache.get(location).setFacstatus(status);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateActive(final String location, final boolean status) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Beacons_data SET Ativo = ? WHERE Location = ?");
            st.setString(2, location);
            st.setBoolean(1, status);
            st.executeUpdate();

            BeaconCache.get(location).setAtivo(status);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateActiveSpeed(final String location, final boolean status) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Beacons_data SET Ativo5 = ? WHERE Location = ?");
            st.setString(2, location);
            st.setBoolean(1, status);
            st.executeUpdate();

            BeaconCache.get(location).setAtivo5(status);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateActiveJump(final String location, final boolean status) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Beacons_data SET Ativo4 = ? WHERE Location = ?");
            st.setString(2, location);
            st.setBoolean(1, status);
            st.executeUpdate();

            BeaconCache.get(location).setAtivo4(status);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateActiveRegen(final String location, final boolean status) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Beacons_data SET Ativo3 = ? WHERE Location = ?");
            st.setString(2, location);
            st.setBoolean(1, status);
            st.executeUpdate();

            BeaconCache.get(location).setAtivo3(status);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateActivePressa(final String location, final boolean status) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Beacons_data SET Ativo2 = ? WHERE Location = ?");
            st.setString(2, location);
            st.setBoolean(1, status);
            st.executeUpdate();

            BeaconCache.get(location).setAtivo2(status);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateActiveForca(final String location, final boolean status) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Beacons_data SET Ativo1 = ? WHERE Location = ?");
            st.setString(2, location);
            st.setBoolean(1, status);
            st.executeUpdate();

            BeaconCache.get(location).setAtivo1(status);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateSpeed(final String location, final int speed) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Beacons_data SET Velocidade = ? WHERE Location = ?");
            st.setString(2, location);
            st.setInt(1, speed);
            st.executeUpdate();

            BeaconCache.get(location).setVelocidade(speed);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateJump(final String location, final int jump) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Beacons_data SET Superpulo = ? WHERE Location = ?");
            st.setString(2, location);
            st.setInt(1, jump);
            st.executeUpdate();

            BeaconCache.get(location).setSuperpulo(jump);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateRegeneracao(final String location, final int regen) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Beacons_data SET Regeneracao = ? WHERE Location = ?");
            st.setString(2, location);
            st.setInt(1, regen);
            st.executeUpdate();

            BeaconCache.get(location).setRegeneracao(regen);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updatePressa(final String location, final int pressa) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Beacons_data SET Pressa = ? WHERE Location = ?");
            st.setString(2, location);
            st.setInt(1, pressa);
            st.executeUpdate();

            BeaconCache.get(location).setPressa(pressa);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateForca(final String location, final int forca) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Beacons_data SET Forca = ? WHERE Location = ?");
            st.setString(2, location);
            st.setInt(1, forca);
            st.executeUpdate();

            BeaconCache.get(location).setForca(forca);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateMultiplos(final String location, final int multiplos) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("UPDATE Beacons_data SET Multiplos = ? WHERE Location = ?");
            st.setString(2, location);
            st.setInt(1, multiplos);
            st.executeUpdate();

            BeaconCache.get(location).setMultiplos(multiplos);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean getOscilar(final String location) {
        if (BeaconCache.get(location) != null) {
            return BeaconCache.get(location).getFacstatus();
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Beacons_data WHERE Location = ?");
                st.setString(1, location);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final boolean facstatus = rs.getBoolean("Facstatus");
                    new BeaconCache(rs.getString("UUID"), location, rs.getInt("Multiplos"), rs.getInt("Forca"), rs.getInt("Pressa"), rs.getInt("Regeneracao"), rs.getInt("Superpulo"), rs.getInt("Velocidade"), rs.getInt("Efeito1"), rs.getInt("Efeito2"),
                            rs.getInt("Efeito3"), rs.getInt("Efeito4"), rs.getInt("Efeito5"), rs.getBoolean("Ativo1"), rs.getBoolean("Ativo2"), rs.getBoolean("Ativo3"), rs.getBoolean("Ativo4"), rs.getBoolean("Ativo5"), facstatus, rs.getBoolean("Ativo")).insert();
                    return facstatus;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return false;
        }
    }

    public static int getMultiplos(final String location) {
        if (BeaconCache.get(location) != null) {
            return BeaconCache.get(location).getMultiplos();
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Beacons_data WHERE Location = ?");
                st.setString(1, location);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final int multiplos = rs.getInt("Multiplos");
                    new BeaconCache(rs.getString("UUID"), location, multiplos, rs.getInt("Forca"), rs.getInt("Pressa"), rs.getInt("Regeneracao"), rs.getInt("Superpulo"), rs.getInt("Velocidade"), rs.getInt("Efeito1"), rs.getInt("Efeito2"),
                            rs.getInt("Efeito3"), rs.getInt("Efeito4"), rs.getInt("Efeito5"), rs.getBoolean("Ativo1"), rs.getBoolean("Ativo2"), rs.getBoolean("Ativo3"), rs.getBoolean("Ativo4"), rs.getBoolean("Ativo5"), rs.getBoolean("Facstatus"), rs.getBoolean("Ativo")).insert();
                    return multiplos;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return 0;
        }
    }

    public static Boolean getSpeedActivated(final String location) {
        if (BeaconCache.get(location) != null) {
            return BeaconCache.get(location).getAtivo5();
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Beacons_data WHERE Location = ?");
                st.setString(1, location);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final boolean ativo5 = rs.getBoolean("Ativo5");
                    new BeaconCache(rs.getString("UUID"), location, rs.getInt("Multiplos"), rs.getInt("Forca"), rs.getInt("Pressa"), rs.getInt("Regeneracao"), rs.getInt("Superpulo"), rs.getInt("Velocidade"), rs.getInt("Efeito1"), rs.getInt("Efeito2"),
                            rs.getInt("Efeito3"), rs.getInt("Efeito4"), rs.getInt("Efeito5"), rs.getBoolean("Ativo1"), rs.getBoolean("Ativo2"), rs.getBoolean("Ativo3"), rs.getBoolean("Ativo4"), ativo5, rs.getBoolean("Facstatus"), rs.getBoolean("Ativo")).insert();
                    return ativo5;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return false;
        }
    }

    public static int getSpeed(final String location) {
        if (BeaconCache.get(location) != null) {
            return BeaconCache.get(location).getVelocidade();
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Beacons_data WHERE Location = ?");
                st.setString(1, location);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final int velocidade = rs.getInt("Velocidade");
                    new BeaconCache(rs.getString("UUID"), location, rs.getInt("Multiplos"), rs.getInt("Forca"), rs.getInt("Pressa"), rs.getInt("Regeneracao"), rs.getInt("Superpulo"), velocidade, rs.getInt("Efeito1"), rs.getInt("Efeito2"),
                            rs.getInt("Efeito3"), rs.getInt("Efeito4"), rs.getInt("Efeito5"), rs.getBoolean("Ativo1"), rs.getBoolean("Ativo2"), rs.getBoolean("Ativo3"), rs.getBoolean("Ativo4"), rs.getBoolean("Ativo5"), rs.getBoolean("Facstatus"), rs.getBoolean("Ativo")).insert();
                    return velocidade;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return 0;
        }
    }

    public static Boolean getJumpActivated(final String location) {
        if (BeaconCache.get(location) != null) {
            return BeaconCache.get(location).getAtivo4();
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Beacons_data WHERE Location = ?");
                st.setString(1, location);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final boolean ativo4 = rs.getBoolean("Ativo4");
                    new BeaconCache(rs.getString("UUID"), location, rs.getInt("Multiplos"), rs.getInt("Forca"), rs.getInt("Pressa"), rs.getInt("Regeneracao"), rs.getInt("Superpulo"), rs.getInt("Velocidade"), rs.getInt("Efeito1"), rs.getInt("Efeito2"),
                            rs.getInt("Efeito3"), rs.getInt("Efeito4"), rs.getInt("Efeito5"), rs.getBoolean("Ativo1"), rs.getBoolean("Ativo2"), rs.getBoolean("Ativo3"), ativo4, rs.getBoolean("Ativo5"), rs.getBoolean("Facstatus"), rs.getBoolean("Ativo")).insert();
                    return ativo4;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return false;
        }
    }

    public static int getJump(final String location) {
        if (BeaconCache.get(location) != null) {
            return BeaconCache.get(location).getSuperpulo();
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Beacons_data WHERE Location = ?");
                st.setString(1, location);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final int superpulo = rs.getInt("Superpulo");
                    new BeaconCache(rs.getString("UUID"), location, rs.getInt("Multiplos"), rs.getInt("Forca"), rs.getInt("Pressa"), rs.getInt("Regeneracao"), superpulo, rs.getInt("Velocidade"), rs.getInt("Efeito1"), rs.getInt("Efeito2"),
                            rs.getInt("Efeito3"), rs.getInt("Efeito4"), rs.getInt("Efeito5"), rs.getBoolean("Ativo1"), rs.getBoolean("Ativo2"), rs.getBoolean("Ativo3"), rs.getBoolean("Ativo4"), rs.getBoolean("Ativo5"), rs.getBoolean("Facstatus"), rs.getBoolean("Ativo")).insert();
                    return superpulo;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return 0;
        }
    }

    public static Boolean getRegenActivated(final String location) {
        if (BeaconCache.get(location) != null) {
            return BeaconCache.get(location).getAtivo3();
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Beacons_data WHERE Location = ?");
                st.setString(1, location);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final boolean ativo3 = rs.getBoolean("Ativo3");
                    new BeaconCache(rs.getString("UUID"), location, rs.getInt("Multiplos"), rs.getInt("Forca"), rs.getInt("Pressa"), rs.getInt("Regeneracao"), rs.getInt("Superpulo"), rs.getInt("Velocidade"), rs.getInt("Efeito1"), rs.getInt("Efeito2"),
                            rs.getInt("Efeito3"), rs.getInt("Efeito4"), rs.getInt("Efeito5"), rs.getBoolean("Ativo1"), rs.getBoolean("Ativo2"), ativo3, rs.getBoolean("Ativo4"), rs.getBoolean("Ativo5"), rs.getBoolean("Facstatus"), rs.getBoolean("Ativo")).insert();
                    return ativo3;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return false;
        }
    }

    public static int getRegen(final String location) {
        if (BeaconCache.get(location) != null) {
            return BeaconCache.get(location).getRegeneracao();
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Beacons_data WHERE Location = ?");
                st.setString(1, location);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final int regen = rs.getInt("Regeneracao");
                    new BeaconCache(rs.getString("UUID"), location, rs.getInt("Multiplos"), rs.getInt("Forca"), rs.getInt("Pressa"), regen, rs.getInt("Superpulo"), rs.getInt("Velocidade"), rs.getInt("Efeito1"), rs.getInt("Efeito2"),
                            rs.getInt("Efeito3"), rs.getInt("Efeito4"), rs.getInt("Efeito5"), rs.getBoolean("Ativo1"), rs.getBoolean("Ativo2"), rs.getBoolean("Ativo3"), rs.getBoolean("Ativo4"), rs.getBoolean("Ativo5"), rs.getBoolean("Facstatus"), rs.getBoolean("Ativo")).insert();
                    return regen;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return 0;
        }
    }

    public static Boolean getPressaActivated(final String location) {
        if (BeaconCache.get(location) != null) {
            return BeaconCache.get(location).getAtivo2();
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Beacons_data WHERE Location = ?");
                st.setString(1, location);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final boolean ativo2 = rs.getBoolean("Ativo2");
                    new BeaconCache(rs.getString("UUID"), location, rs.getInt("Multiplos"), rs.getInt("Forca"), rs.getInt("Pressa"), rs.getInt("Regeneracao"), rs.getInt("Superpulo"), rs.getInt("Velocidade"), rs.getInt("Efeito1"), rs.getInt("Efeito2"),
                            rs.getInt("Efeito3"), rs.getInt("Efeito4"), rs.getInt("Efeito5"), rs.getBoolean("Ativo1"), ativo2, rs.getBoolean("Ativo3"), rs.getBoolean("Ativo4"), rs.getBoolean("Ativo5"), rs.getBoolean("Facstatus"), rs.getBoolean("Ativo")).insert();
                    return ativo2;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return false;
        }
    }

    public static int getPressa(final String location) {
        if (BeaconCache.get(location) != null) {
            return BeaconCache.get(location).getPressa();
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Beacons_data WHERE Location = ?");
                st.setString(1, location);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final int pressa = rs.getInt("Pressa");
                    new BeaconCache(rs.getString("UUID"), location, rs.getInt("Multiplos"), rs.getInt("Forca"), pressa, rs.getInt("Regeneracao"), rs.getInt("Superpulo"), rs.getInt("Velocidade"), rs.getInt("Efeito1"), rs.getInt("Efeito2"),
                            rs.getInt("Efeito3"), rs.getInt("Efeito4"), rs.getInt("Efeito5"), rs.getBoolean("Ativo1"), rs.getBoolean("Ativo2"), rs.getBoolean("Ativo3"), rs.getBoolean("Ativo4"), rs.getBoolean("Ativo5"), rs.getBoolean("Facstatus"), rs.getBoolean("Ativo")).insert();
                    return pressa;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return 0;
        }
    }

    public static int getSpeedActivatedLevel(final String location) {
        if (BeaconCache.get(location) != null) {
            return BeaconCache.get(location).getEfeito5();
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Beacons_data WHERE Location = ?");
                st.setString(1, location);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final int efeito5 = rs.getInt("Efeito5");
                    new BeaconCache(rs.getString("UUID"), location, rs.getInt("Multiplos"), rs.getInt("Forca"), rs.getInt("Pressa"), rs.getInt("Regeneracao"), rs.getInt("Superpulo"), rs.getInt("Velocidade"), rs.getInt("Efeito1"), rs.getInt("Efeito2"),
                            rs.getInt("Efeito3"), rs.getInt("Efeito4"), efeito5, rs.getBoolean("Ativo1"), rs.getBoolean("Ativo2"), rs.getBoolean("Ativo3"), rs.getBoolean("Ativo4"), rs.getBoolean("Ativo5"), rs.getBoolean("Facstatus"), rs.getBoolean("Ativo")).insert();
                    return efeito5;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return 0;
        }
    }

    public static int getJumpActivatedLevel(final String location) {
        if (BeaconCache.get(location) != null) {
            return BeaconCache.get(location).getEfeito4();
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Beacons_data WHERE Location = ?");
                st.setString(1, location);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final int efeito4 = rs.getInt("Efeito4");
                    new BeaconCache(rs.getString("UUID"), location, rs.getInt("Multiplos"), rs.getInt("Forca"), rs.getInt("Pressa"), rs.getInt("Regeneracao"), rs.getInt("Superpulo"), rs.getInt("Velocidade"), rs.getInt("Efeito1"), rs.getInt("Efeito2"),
                            rs.getInt("Efeito3"), efeito4, rs.getInt("Efeito5"), rs.getBoolean("Ativo1"), rs.getBoolean("Ativo2"), rs.getBoolean("Ativo3"), rs.getBoolean("Ativo4"), rs.getBoolean("Ativo5"), rs.getBoolean("Facstatus"), rs.getBoolean("Ativo")).insert();
                    return efeito4;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return 0;
        }
    }

    public static int getRegenActivatedLevel(final String location) {
        if (BeaconCache.get(location) != null) {
            return BeaconCache.get(location).getEfeito3();
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Beacons_data WHERE Location = ?");
                st.setString(1, location);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final int efeito3 = rs.getInt("Efeito3");
                    new BeaconCache(rs.getString("UUID"), location, rs.getInt("Multiplos"), rs.getInt("Forca"), rs.getInt("Pressa"), rs.getInt("Regeneracao"), rs.getInt("Superpulo"), rs.getInt("Velocidade"), rs.getInt("Efeito1"), rs.getInt("Efeito2"),
                            efeito3, rs.getInt("Efeito4"), rs.getInt("Efeito5"), rs.getBoolean("Ativo1"), rs.getBoolean("Ativo2"), rs.getBoolean("Ativo3"), rs.getBoolean("Ativo4"), rs.getBoolean("Ativo5"), rs.getBoolean("Facstatus"), rs.getBoolean("Ativo")).insert();
                    return efeito3;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return 0;
        }
    }

    public static int getPressaActivatedLevel(final String location) {
        if (BeaconCache.get(location) != null) {
            return BeaconCache.get(location).getEfeito2();
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Beacons_data WHERE Location = ?");
                st.setString(1, location);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final int efeito2 = rs.getInt("Efeito2");
                    new BeaconCache(rs.getString("UUID"), location, rs.getInt("Multiplos"), rs.getInt("Forca"), rs.getInt("Pressa"), rs.getInt("Regeneracao"), rs.getInt("Superpulo"), rs.getInt("Velocidade"), rs.getInt("Efeito1"), efeito2,
                            rs.getInt("Efeito3"), rs.getInt("Efeito4"), rs.getInt("Efeito5"), rs.getBoolean("Ativo1"), rs.getBoolean("Ativo2"), rs.getBoolean("Ativo3"), rs.getBoolean("Ativo4"), rs.getBoolean("Ativo5"), rs.getBoolean("Facstatus"), rs.getBoolean("Ativo")).insert();
                    return efeito2;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return 0;
        }
    }

    public static int getForcaActivatedLevel(final String location) {
        if (BeaconCache.get(location) != null) {
            return BeaconCache.get(location).getEfeito1();
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Beacons_data WHERE Location = ?");
                st.setString(1, location);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final int efeito1 = rs.getInt("Efeito1");
                    new BeaconCache(rs.getString("UUID"), location, rs.getInt("Multiplos"), rs.getInt("Forca"), rs.getInt("Pressa"), rs.getInt("Regeneracao"), rs.getInt("Superpulo"), rs.getInt("Velocidade"), efeito1, rs.getInt("Efeito2"),
                            rs.getInt("Efeito3"), rs.getInt("Efeito4"), rs.getInt("Efeito5"), rs.getBoolean("Ativo1"), rs.getBoolean("Ativo2"), rs.getBoolean("Ativo3"), rs.getBoolean("Ativo4"), rs.getBoolean("Ativo5"), rs.getBoolean("Facstatus"), rs.getBoolean("Ativo")).insert();
                    return efeito1;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return 0;
        }
    }

    public static boolean getForcaActivated(final String location) {
        if (BeaconCache.get(location) != null) {
            return BeaconCache.get(location).getAtivo1();
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Beacons_data WHERE Location = ?");
                st.setString(1, location);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final boolean ativo1 = rs.getBoolean("Ativo1");
                    new BeaconCache(rs.getString("UUID"), location, rs.getInt("Multiplos"), rs.getInt("Forca"), rs.getInt("Pressa"), rs.getInt("Regeneracao"), rs.getInt("Superpulo"), rs.getInt("Velocidade"), rs.getInt("Efeito1"), rs.getInt("Efeito2"),
                            rs.getInt("Efeito3"), rs.getInt("Efeito4"), rs.getInt("Efeito5"), ativo1, rs.getBoolean("Ativo2"), rs.getBoolean("Ativo3"), rs.getBoolean("Ativo4"), rs.getBoolean("Ativo5"), rs.getBoolean("Facstatus"), rs.getBoolean("Ativo")).insert();
                    return ativo1;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public static int getForca(final String location) {
        if (BeaconCache.get(location) != null) {
            return BeaconCache.get(location).getForca();
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Beacons_data WHERE Location = ?");
                st.setString(1, location);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final int forca = rs.getInt("Forca");
                    new BeaconCache(rs.getString("UUID"), location, rs.getInt("Multiplos"), forca, rs.getInt("Pressa"), rs.getInt("Regeneracao"), rs.getInt("Superpulo"), rs.getInt("Velocidade"), rs.getInt("Efeito1"), rs.getInt("Efeito2"),
                            rs.getInt("Efeito3"), rs.getInt("Efeito4"), rs.getInt("Efeito5"), rs.getBoolean("Ativo1"), rs.getBoolean("Ativo2"), rs.getBoolean("Ativo3"), rs.getBoolean("Ativo4"), rs.getBoolean("Ativo5"), rs.getBoolean("Facstatus"), rs.getBoolean("Ativo")).insert();
                    return forca;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return 0;
        }
    }

    public static boolean getActive(final String location) {
        if (BeaconCache.get(location) != null) {
            return BeaconCache.get(location).getAtivo();
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Beacons_data WHERE Location = ?");
                st.setString(1, location);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final boolean ativo = rs.getBoolean("Ativo");
                    new BeaconCache(rs.getString("UUID"), location, rs.getInt("Multiplos"), rs.getInt("Forca"), rs.getInt("Pressa"), rs.getInt("Regeneracao"), rs.getInt("Superpulo"), rs.getInt("Velocidade"), rs.getInt("Efeito1"), rs.getInt("Efeito2"),
                            rs.getInt("Efeito3"), rs.getInt("Efeito4"), rs.getInt("Efeito5"), rs.getBoolean("Ativo1"), rs.getBoolean("Ativo2"), rs.getBoolean("Ativo3"), rs.getBoolean("Ativo4"), rs.getBoolean("Ativo5"), rs.getBoolean("Facstatus"), ativo).insert();
                    return ativo;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return false;
        }
    }

    public static String getExistBeacon(final String location) {
        if (BeaconCache.get(location) != null) {
            return BeaconCache.get(location).getLocation();
        } else {
            try {
                final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Beacons_data WHERE Location = ?");
                st.setString(1, location);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    final String loc = rs.getString("Location");
                    new BeaconCache(rs.getString("UUID"), location, rs.getInt("Multiplos"), rs.getInt("Forca"), rs.getInt("Pressa"), rs.getInt("Regeneracao"), rs.getInt("Superpulo"), rs.getInt("Velocidade"), rs.getInt("Efeito1"), rs.getInt("Efeito2"),
                            rs.getInt("Efeito3"), rs.getInt("Efeito4"), rs.getInt("Efeito5"), rs.getBoolean("Ativo1"), rs.getBoolean("Ativo2"), rs.getBoolean("Ativo3"), rs.getBoolean("Ativo4"), rs.getBoolean("Ativo5"), rs.getBoolean("Facstatus"), rs.getBoolean("Ativo")).insert();
                    return loc;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
