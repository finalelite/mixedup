package com.mixedup.apis;

import java.util.HashMap;

public class TagCache {

    public static HashMap<String, TagCache> CACHE = new HashMap<>();
    private final String UUID;
    private String tag;

    public TagCache(final String UUID, final String tag) {
        this.UUID = UUID;
        this.tag = tag;
    }

    public static TagCache get(final String UUID) {
        return TagCache.CACHE.get(UUID);
    }

    public TagCache insert() {
        TagCache.CACHE.put(UUID, this);

        return this;
    }

    public String getTag() {
        return this.tag;
    }

    public void setTag(final String tag) {
        this.tag = tag;
    }
}
