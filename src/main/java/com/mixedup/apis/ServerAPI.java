package com.mixedup.apis;

import com.mixedup.MySqlHub;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ServerAPI {

    public static int getOnlineServer(final String Server) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Online_server WHERE Server = ?");
            st.setString(1, Server);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("Online");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getServerName(final int port) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Server_info WHERE Porta = ?");
            st.setInt(1, port);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("ServerName");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean getOnlineStatus(final String Server) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Online_server WHERE Server = ?");
            st.setString(1, Server);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Status");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void setInfo(final String Server, final int online, final boolean status) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("INSERT INTO Online_server(Server, Online, Status) VALUES (?, ?, ?)");
            st.setString(1, Server);
            st.setInt(2, online);
            st.setBoolean(3, status);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateOnline(final String Server, final int online) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("UPDATE Online_server SET Online = ? WHERE Server = ?");
            st.setString(2, Server);
            st.setInt(1, online);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateOnlineStatus(final String Server, final boolean status) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("UPDATE Online_server SET Status = ? WHERE Server = ?");
            st.setString(2, Server);
            st.setBoolean(1, status);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static int getPort(final String Server) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Server_info WHERE ServerName = ?");
            st.setString(1, Server);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("Porta");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getIp(final String Server) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Server_info WHERE ServerName = ?");
            st.setString(1, Server);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Ip");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void setInfoManutencao(final String Server, final boolean manutencao) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("INSERT INTO Lobbys_info(Server, Manutencao) VALUES (?, ?)");
            st.setString(1, Server);
            st.setBoolean(2, manutencao);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateManutencao(final String Server, final boolean manutencao) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("UPDATE Lobbys_info SET Manutencao = ? WHERE Server = ?");
            st.setString(2, Server);
            st.setBoolean(1, manutencao);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean getManutencao(final String Server) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Lobbys_info WHERE Server = ?");
            st.setString(1, Server);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Manutencao");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String getManutencaoExist(final String Server) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Lobbys_info WHERE Server = ?");
            st.setString(1, Server);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Server");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getStaffOnline() {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Staff_online");
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Nick");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getStaffOnlineID(final int ID) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Staff_online WHERE ID = ?");
            st.setInt(1, ID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Nick");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getStaffOnlineExist(final String UUID) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT * FROM Staff_online WHERE UUID = ?");
            st.setString(1, UUID);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("UUID");
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void setStaffOnline(final String UUID, final String nick, final String tag) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("INSERT INTO Staff_online(UUID, Nick, Tag) VALUES (?, ?, ?)");
            st.setString(1, UUID);
            st.setString(2, nick);
            st.setString(3, tag);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateStaffOnlineTagID(final int ID, final String players) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("UPDATE Staff_online SET Players = ? WHERE ID = ?");
            st.setInt(2, ID);
            st.setString(1, players);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateStaffOnlineTag(final String UUID, final String players) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("UPDATE Staff_online SET Players = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setString(1, players);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteStaffOnline(final String UUID) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("DELETE FROM Staff_online WHERE = UUID");
            st.setString(1, UUID);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void setRestarting(final boolean b) {
        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("UPDATE Restart_data SET Restarting = ? WHERE Server = 'Factions';");
            st.setBoolean(1, b);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }
}
