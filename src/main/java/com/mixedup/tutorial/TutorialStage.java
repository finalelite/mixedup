package com.mixedup.tutorial;

import org.bukkit.Location;

import java.util.List;

/**
 * @author zMathi
 */
public class TutorialStage {

    private final String name;
    private long time; //ticks
    private final List<Location> locations;
    private final List<String> message;

    public TutorialStage(final String name, final long time, final List<Location> locations, final List<String> message) {
        this.name = name;
        this.time = time;
        this.locations = locations;
        this.message = message;
    }

    public String getName() {
        return this.name;
    }

    public long getTime() {
        return this.time;
    }

    public void setTime(final long time) {
        this.time = time;
    }

    public List<Location> getLocations() {
        return this.locations;
    }

    public List<String> getMessage() {
        return this.message;
    }

    public boolean isFinished() {
        return !this.locations.isEmpty() || !this.message.isEmpty();
    }
}
