package com.mixedup.tutorial;

import com.mixedup.Main;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * @author zMathi
 */
public class TutorialListener implements Listener {

    @EventHandler(priority = EventPriority.LOW)
    public void onMove(final PlayerMoveEvent event) {
        if (TutorialManager.get().inTutorial(event.getPlayer())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onQuit(final PlayerQuitEvent event) {
        if (TutorialManager.get().inTutorial(event.getPlayer())) {
            TutorialManager.get().removeFromTutorial(event.getPlayer());
        }
    }

    @EventHandler
    public void onCommand(final PlayerCommandPreprocessEvent event) {
        final String command = event.getMessage().toLowerCase();
        if (command.startsWith("/") && TutorialManager.get().inTutorial(event.getPlayer()) && !command.startsWith("/tutorial sair")) {
            event.getPlayer().sendMessage(" §c* Comandos bloqueados durante o tutorial. Para sair use /tutorial sair");
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onBookClose(final PlayerEditBookEvent event) {
        final Player player = event.getPlayer();
        final BookMeta oldMeta = event.getPreviousBookMeta();
        final BookMeta meta = event.getNewBookMeta();

        final boolean isAnEditingBook = oldMeta.getDisplayName().startsWith("§aEditar mensagem ");

        if (!event.isSigning()) {
            if (player.hasMetadata(TutorialManager.EDITING_MESSAGE_METADATA)) {
                player.sendMessage(ChatColor.YELLOW + "Assine o livro para enviar o reporte.");
            }
            return;
        }

        if (!player.hasMetadata(TutorialManager.EDITING_MESSAGE_METADATA)) {
            if (isAnEditingBook) {
                player.sendMessage("§c * Você não está editando mensagem.");
                this.removeLater(player, oldMeta);

                event.setCancelled(true);
            }
            return;
        }

        if (!isAnEditingBook)
            return;

        final TutorialStage stage = TutorialManager.get().getStage(oldMeta.getDisplayName().split(" §7\\(")[1].split("\\)")[0]);
        stage.getMessage().clear();
        for (String page : meta.getPages()) {
            page = ChatColor.translateAlternateColorCodes('&', page).replace(ChatColor.BLACK.toString(), "");

            if (page.contains("\n")) {
                for (final String newPages : page.split("\n")) {
                    stage.getMessage().add(newPages);
                }
            } else {
                stage.getMessage().add(page);
            }
        }
        TutorialManager.get().saveStage(stage);

        player.sendMessage("§a * Mensagem do estágio '" + stage.getName() + "' editada com sucesso..");
        this.removeLater(player, oldMeta);
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onItemDrop(final PlayerDropItemEvent event) {
        if (event.isCancelled())
            return;

        final Player player = event.getPlayer();
        final ItemStack item = event.getItemDrop().getItemStack();
        final ItemMeta meta = item.getItemMeta();

        if (item.getType() != Material.WRITABLE_BOOK)
            return;

        final boolean isAnEditingBook = meta.getDisplayName().startsWith("§aEditar mensagem");
        if (!isAnEditingBook)
            return;

        event.getItemDrop().remove();

        if (player.hasMetadata(TutorialManager.EDITING_MESSAGE_METADATA)) {
            player.sendMessage("§c* Edição de mensagem cancelada.");
            player.removeMetadata(TutorialManager.EDITING_MESSAGE_METADATA, Main.getInstance());
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onInventoryClick(final InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();

        if (player.hasMetadata(TutorialManager.EDITING_MESSAGE_METADATA)) {
            player.sendMessage("§c* Por favor, edite a mensagem ou drope o livro para cancelar.");
            event.setCancelled(true);
        }
    }

    private void removeLater(final Player player, final BookMeta meta) {
        new BukkitRunnable() {
            @Override
            public void run() {
                player.removeMetadata(TutorialManager.EDITING_MESSAGE_METADATA, Main.getInstance());

                player.getInventory().all(Material.WRITABLE_BOOK).forEach((slot, o) -> {
                    if (o.getItemMeta().equals(meta))
                        player.getInventory().setItem(slot, new ItemStack(Material.AIR));
                });
            }
        }.runTaskLater(Main.getInstance(), 5);
    }
}
