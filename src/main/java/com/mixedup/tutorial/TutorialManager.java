package com.mixedup.tutorial;

import com.destroystokyo.paper.Title;
import com.mixedup.Main;
import com.mixedup.apis.RegionsSpawnAPI;
import com.mixedup.utils.CentralizeMsg;
import org.bukkit.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;

/**
 * @author zMathi
 */
public class TutorialManager {

    public static final String EDITING_MESSAGE_METADATA = "EMM";

    private static TutorialManager instance;
    private final HashMap<Integer, TutorialStage> stages;
    private YamlConfiguration file;
    private final Set<String> inTutorial;

    public TutorialManager() {
        this.inTutorial = new HashSet<>();
        this.stages = new HashMap<>();
        TutorialManager.instance = this;

        Bukkit.getPluginManager().registerEvents(new TutorialListener(), Main.getInstance());
        this.loadStages();
    }

    public static TutorialManager get() {
        return TutorialManager.instance;
    }

    public void loadStages() {
        final File file = new File(Main.getInstance().getDataFolder(), "tutorial.yml");

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (final IOException e) {
                e.printStackTrace();
            }
        }
        this.file = YamlConfiguration.loadConfiguration(file);

        if (!this.file.isSet("stages")) return;

        for (final String stageName : this.file.getConfigurationSection("stages").getKeys(false)) {
            final List<Location> locations = new ArrayList<>();
            for (final String l : this.file.getStringList("stages." + stageName + ".locations")) {
                final String[] split = l.split(":");
                locations.add(new Location(Bukkit.getWorld(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]), Double.parseDouble(split[3]), Float.parseFloat(split[4]), Float.parseFloat(split[5])));
            }

            final List<String> message = new ArrayList<>();
            for (final String line : this.file.getStringList("stages." + stageName + ".message")) {
                message.add(ChatColor.translateAlternateColorCodes('&', line));
            }

            final long time = this.file.getLong("stages." + stageName + ".time");

            this.addStage(new TutorialStage(stageName, time, locations, message));
        }
    }

    public void runAll(final Player player, final Consumer<Player> afterEnd) {
        final TutorialFactory factory = new TutorialFactory(this);

        for (int i = 0; i < this.stages.size(); i++) {
            final TutorialStage stage = this.stages.get(i);

            if (!stage.isFinished()) continue;
            factory.insertStage(i);
        }

        factory.insertConsumer(afterEnd, -1);
        factory.run(player);
    }

    public void runStage(final Player player, final TutorialStage stage) {
        this.runStage(player, stage, this::removeFromTutorial);
    }

    public void runStage(final Player player, final TutorialStage stage, final Consumer<Player> onEnd) {
        if (!stage.isFinished()) {
            player.sendMessage(" §c* Não foi possível iniciar esse tutorial pois ele ainda não está totalmente finalizado.");
            return;
        }

        this.inTutorial.forEach(name -> {
            if (!player.getName().equals(name)) {
                final Player other = Bukkit.getPlayer(name);

                other.hidePlayer(Main.getInstance(), player);
                player.hidePlayer(Main.getInstance(), other);
            }
        });
        this.inTutorial.add(player.getName());

        if (Main.scoreboardManager.hasSquadScoreboard(player)) {
            Main.scoreboardManager.getSquadScoreboard().disableScoreboardToPlayer(player);
        } else {
            Main.scoreboardManager.getDefaultScoreboard().disableScoreboardToPlayer(player);
        }

        player.setGameMode(GameMode.SPECTATOR);
        player.teleport(stage.getLocations().get(0));
        player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 10f, 5f);
        player.sendTitle(new Title(ChatColor.translateAlternateColorCodes('&', "&a&l" + stage.getName().toUpperCase()), ChatColor.translateAlternateColorCodes('&', "&fTutorial")));

        for (int i = 0; i < 100; i++)
            player.sendMessage(" ");

        player.sendMessage(CentralizeMsg.sendCenteredMessage("&a&lTUTORIAL - " + stage.getName().toUpperCase()));
        player.sendMessage("");
        player.sendMessage(stage.getMessage().toArray(new String[stage.getMessage().size()]));
        player.sendMessage("");

        if (stage.getLocations().size() > 1) {
            new BukkitRunnable() {
                private long ticks;
                private int location;

                @Override
                public void run() {
                    if (!TutorialManager.this.inTutorial(player)) {
                        this.cancel();
                        return;
                    }
                    this.ticks++;

                    if (this.ticks >= (stage.getTime() / stage.getLocations().size())) {
                        this.location++;
                        if (this.location >= stage.getLocations().size()) {
                            this.cancel();

                            if (onEnd != null) onEnd.accept(player);
                            return;
                        }
                        player.teleport(stage.getLocations().get(this.location));
                        this.ticks = 0;
                    }
                }
            }.runTaskTimer(Main.getInstance(), 0L, 1L);
        }
    }

    public void removeFromTutorial(final Player player) {
        this.inTutorial.remove(player.getName());

        this.inTutorial.forEach(name -> {
            final Player other = Bukkit.getPlayer(name);

            other.showPlayer(Main.getInstance(), player);
            player.showPlayer(Main.getInstance(), other);
        });

        final String[] split = RegionsSpawnAPI.getLocation("spawn").split(":");
        final Location location = new Location(Bukkit.getWorld(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]), Double.parseDouble(split[3]), Float.parseFloat(split[4]), Float.parseFloat(split[5]));
        player.teleport(location);
        player.setGameMode(GameMode.SURVIVAL);

        if (Main.scoreboardManager.hasSquadScoreboard(player)) {
            Main.scoreboardManager.getSquadScoreboard().enableScoreboardToPlayer(player);
        } else {
            Main.scoreboardManager.getDefaultScoreboard().enableScoreboardToPlayer(player);
        }
    }

    public boolean inTutorial(final Player player) {
        return this.inTutorial.contains(player.getName());
    }

    public Set<String> getInTutorial() {
        return this.inTutorial;
    }

    public void saveStage(final TutorialStage stage) {
        ConfigurationSection section = null;

        if (!file.isSet("stages")) {
            file.createSection("stages");
        }

        if (file.isSet("stages." + stage.getName())) {
            section = file.getConfigurationSection("stages").getConfigurationSection(stage.getName());
        } else {
            section = file.getConfigurationSection("stages").createSection(stage.getName());
        }

        final List<String> locations = new ArrayList<>();
        for (final Location location : stage.getLocations()) {
            locations.add(String.format("%s:%s:%s:%s:%s:%s", location.getWorld().getName(), location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch()));
        }

        section.set("locations", locations);
        section.set("message", stage.getMessage());
        section.set("time", stage.getTime());

        try {
            file.save(new File(Main.getInstance().getDataFolder(), "tutorial.yml"));
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    public void addStage(final TutorialStage stage) {
        this.stages.put(this.stages.size() + 1, stage);
    }

    public TutorialStage getStage(final String name) {
        return this.stages.values().stream().filter(tutorialStage -> tutorialStage.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
    }

    public HashMap<Integer, TutorialStage> getStages() {
        return this.stages;
    }
}
