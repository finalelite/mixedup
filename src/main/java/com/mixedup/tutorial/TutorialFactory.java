package com.mixedup.tutorial;

import com.mixedup.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.function.Consumer;

/**
 * @author zMathi
 */
public class TutorialFactory {

    private final TutorialManager manager;
    private Consumer<Player> product;

    public TutorialFactory(final TutorialManager manager) {
        this.manager = manager;
    }

    public void insertStage(final int stageId) {
        final long nextStageTime = this.manager.getStages().containsKey(stageId + 1) ? this.manager.getStages().get(stageId + 1).getTime() : -1;

        this.insertConsumer(player -> this.manager.runStage(player, this.manager.getStages().get(stageId), null), nextStageTime);
    }

    public void insertConsumer(final Consumer<Player> consumer, final long nextStageTime) {
        if (product == null) {
            product = consumer;
        } else {
            if (nextStageTime == -1) {
                product = product.andThen(consumer::accept);
            } else {
                product = product.andThen(player -> Bukkit.getScheduler().runTaskLater(Main.plugin, () -> consumer.accept(player), nextStageTime + 1));
            }
        }
    }

    public void run(final Player player) {
        product.accept(player);
    }
}
