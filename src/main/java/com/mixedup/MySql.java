package com.mixedup;

import org.bukkit.ChatColor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySql {

    public static Connection con;

    public static void connect() {
        if (!MySql.isConnected()) {
            try {
                MySql.con = DriverManager.getConnection("jdbc:mysql://localhost/mixedup?autoReconnect=true&useSSL=true", "root",
                        "9cjhUMZQBYYspLvrRbTYrgTrXudkbcSf");
                System.out.println(ChatColor.RED + "MySql Conectado");
            } catch (final SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void disconnect() {
        if (MySql.isConnected()) {
            try {
                MySql.con.close();
                System.out.println(ChatColor.RED + "MySql Desconectado");
            } catch (final SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean isConnected() {
        return (MySql.con != null);
    }
}
