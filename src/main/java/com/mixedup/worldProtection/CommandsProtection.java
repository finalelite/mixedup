package com.mixedup.worldProtection;

import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import com.mixedup.utils.RemoveItemInv;
import com.mixedup.worldProtection.hashs.PositonsHash;
import com.mixedup.worldProtection.hashs.ProtectionHash;
import com.mixedup.worldProtection.hashs.ProtectionUtil;
import com.mixedup.worldProtection.hashs.SelectedHash;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class CommandsProtection implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("desproteger")) {
            if (TagAPI.getTag(UUID).equalsIgnoreCase("Master") || TagAPI.getTag(UUID).equalsIgnoreCase("Supervisor")) {
                if (args.length == 0) {
                    if (ProtectionUtil.playerInArea(player.getLocation()) == null) {
                        player.sendMessage(ChatColor.RED + " * Ops, para remover uma proteção, esteja posicionado dentro de uma.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        return false;
                    } else {
                        ProtectionAPI.removeProtection(ProtectionUtil.playerInArea(player.getLocation()).getarea());
                        ProtectionHash.CACHE.remove(ProtectionUtil.playerInArea(player.getLocation()));
                        player.sendMessage(ChatColor.GREEN + " * Proteção removida com sucesso!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                }
            }
        }

        if (cmd.getName().equalsIgnoreCase("proteger")) {
            if (TagAPI.getTag(UUID).equalsIgnoreCase("Master") || TagAPI.getTag(UUID).equalsIgnoreCase("Supervisor")) {
                if (args.length == 0) {
                    if (SelectedHash.get(UUID) == null) {
                        new SelectedHash(UUID, false).insert();

                        final ItemStack stick = new ItemStack(Material.STICK);
                        final ItemMeta meta = stick.getItemMeta();
                        meta.setDisplayName(ChatColor.YELLOW + "Demarcador");
                        stick.setItemMeta(meta);
                        player.getInventory().addItem(stick);

                        player.sendMessage(ChatColor.YELLOW + " * Selecione a area a ser protegida!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else if (SelectedHash.get(UUID).gethasSelected() == false) {
                        final ItemStack stick = new ItemStack(Material.STICK);
                        final ItemMeta meta = stick.getItemMeta();
                        meta.setDisplayName(ChatColor.YELLOW + "Demarcador");
                        stick.setItemMeta(meta);
                        player.getInventory().addItem(stick);

                        player.sendMessage(ChatColor.YELLOW + " * Selecione a area a ser protegida!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        final String pos1 = PositonsHash.get(UUID).getPos1();
                        final String pos2 = PositonsHash.get(UUID).getPos2();
                        ProtectionAPI.createProtection(pos1 + "::" + pos2);

                        final ProtectionHash protection = new ProtectionHash(pos1 + "::" + pos2, pos1, pos2).insert();
                        ProtectionUtil.getRegions().add(protection);

                        final ItemStack stick = new ItemStack(Material.STICK);
                        final ItemMeta meta = stick.getItemMeta();
                        meta.setDisplayName(ChatColor.YELLOW + "Demarcador");
                        stick.setItemMeta(meta);
                        player.getInventory().addItem(stick);
                        RemoveItemInv.removeItems(player.getInventory(), stick, 1);
                        player.sendMessage(ChatColor.GREEN + " * Area protegida com sucesso!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        SelectedHash.get(UUID).sethasSelected(false);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/proteger");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
