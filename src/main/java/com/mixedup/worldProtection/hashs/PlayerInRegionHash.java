package com.mixedup.worldProtection.hashs;

import java.util.HashMap;

public class PlayerInRegionHash {

    private static final HashMap<String, PlayerInRegionHash> CACHE = new HashMap<String, PlayerInRegionHash>();
    private final String UUID;
    private boolean Status;

    public PlayerInRegionHash(final String UUID, final boolean Status) {
        this.UUID = UUID;
        this.Status = Status;
    }

    public static PlayerInRegionHash get(final String UUID) {
        return PlayerInRegionHash.CACHE.get(String.valueOf(UUID));
    }

    public PlayerInRegionHash insert() {
        PlayerInRegionHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(final boolean Status) {
        this.Status = Status;
    }

    public String getUUID() {
        return UUID;
    }
}
