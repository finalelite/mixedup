package com.mixedup.worldProtection.hashs;

import com.mixedup.MySql;
import org.bukkit.Location;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProtectionUtil {

    private static List<ProtectionHash> regions = new ArrayList<>();

    public static ProtectionHash getRegion(final String squad) {
        for (final ProtectionHash region : ProtectionUtil.getRegions()) {
            if (region.getarea() == squad) {
                return region;
            }
        }
        return null;
    }

    public static List<ProtectionHash> getRegions() {
        return ProtectionUtil.regions;
    }

    public static void setRegions(final List<ProtectionHash> regions) {
        ProtectionUtil.regions = regions;
    }

    public static ProtectionHash playerInArea(final Location loc) {
        final List<ProtectionHash> regionList = getRegions();
        for (final ProtectionHash rg : regionList) {

            final String[] pos1 = rg.getPos1().split(":");
            final String[] pos2 = rg.getPos2().split(":");

            if (!pos1[0].equalsIgnoreCase(loc.getWorld().getName())) continue;

            if (!pos1[0].equals("NULL")) {
                final int p1x = Integer.valueOf(pos1[1]);
                final int p1y = Integer.valueOf(pos1[2]);
                final int p1z = Integer.valueOf(pos1[3]);
                final int p2x = Integer.valueOf(pos2[1]);
                final int p2y = Integer.valueOf(pos2[2]);
                final int p2z = Integer.valueOf(pos2[3]);

                final int minX = p1x < p2x ? p1x : p2x;
                final int minY = p1y < p2y ? p1y : p2y;
                final int minZ = p1z < p2z ? p1z : p2z;

                final int maxX = p1x > p2x ? p1x : p2x;
                final int maxY = p1y > p2y ? p1y : p2y;
                final int maxZ = p1z > p2z ? p1z : p2z;

                if ((loc.getBlockX() >= minX) && (loc.getBlockX() <= maxX) && (loc.getBlockY() >= minY)
                        && (loc.getBlockY() <= maxY) && (loc.getBlockZ() >= minZ) && (loc.getBlockZ() <= maxZ)) {

                    return rg;

                }
            }
        }
        return null;
    }

    public static void enableRegions() {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Protections_data");
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                final String[] split = rs.getString("Location").split("::");
                final ProtectionHash protection = new ProtectionHash(rs.getString("Location"), split[0], split[1]).insert();
                getRegions().add(protection);
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }
}
