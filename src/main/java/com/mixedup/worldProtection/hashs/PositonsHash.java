package com.mixedup.worldProtection.hashs;

import java.util.HashMap;

public class PositonsHash {

    private static final HashMap<String, PositonsHash> CACHE = new HashMap<String, PositonsHash>();
    private final String UUID;
    private String pos1;
    private String pos2;

    public PositonsHash(final String UUID, final String pos1, final String pos2) {
        this.UUID = UUID;
        this.pos1 = pos1;
        this.pos2 = pos2;
    }

    public static PositonsHash get(final String UUID) {
        return PositonsHash.CACHE.get(String.valueOf(UUID));
    }

    public PositonsHash insert() {
        PositonsHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public String getPos2() {
        return pos2;
    }

    public void setPos2(final String pos2) {
        this.pos2 = pos2;
    }

    public String getPos1() {
        return pos1;
    }

    public void setPos1(final String pos1) {
        this.pos1 = pos1;
    }

    public String getUUID() {
        return UUID;
    }
}
