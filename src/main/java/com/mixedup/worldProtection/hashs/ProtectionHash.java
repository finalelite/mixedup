package com.mixedup.worldProtection.hashs;

import java.util.HashMap;

public class ProtectionHash {

    public static HashMap<String, ProtectionHash> CACHE = new HashMap<String, ProtectionHash>();
    private String area;
    private String pos1;
    private String pos2;

    public ProtectionHash(final String area, final String pos1, final String pos2) {
        this.area = area;
        this.pos1 = pos1;
        this.pos2 = pos2;
    }

    public static ProtectionHash get(final String area) {
        return ProtectionHash.CACHE.get(area);
    }

    public ProtectionHash insert() {
        ProtectionHash.CACHE.put(area, this);

        return this;
    }

    public String getarea() {
        return this.area;
    }

    public void setarea(final String area) {
        this.area = area;
    }

    public String getPos1() {
        return this.pos1;
    }

    public void setPos1(final String pos1) {
        this.pos1 = pos1;
    }

    public String getPos2() {
        return this.pos2;
    }

    public void setPos2(final String pos2) {
        this.pos2 = pos2;
    }
}
