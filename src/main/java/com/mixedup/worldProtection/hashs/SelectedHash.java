package com.mixedup.worldProtection.hashs;

import java.util.HashMap;

public class SelectedHash {

    private static final HashMap<String, SelectedHash> CACHE = new HashMap<String, SelectedHash>();
    private final String UUID;
    private boolean hasSelected;

    public SelectedHash(final String UUID, final boolean hasSelected) {
        this.UUID = UUID;
        this.hasSelected = hasSelected;
    }

    public static SelectedHash get(final String UUID) {
        return SelectedHash.CACHE.get(String.valueOf(UUID));
    }

    public SelectedHash insert() {
        SelectedHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean gethasSelected() {
        return hasSelected;
    }

    public void sethasSelected(final boolean hasSelected) {
        this.hasSelected = hasSelected;
    }

    public String getUUID() {
        return UUID;
    }
}
