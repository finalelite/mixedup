package com.mixedup.worldProtection.listeners;

import com.mixedup.Main;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import com.mixedup.worldProtection.hashs.PositonsHash;
import com.mixedup.worldProtection.hashs.SelectedHash;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerSelectAreaEvent implements Listener {

    @EventHandler
    public void onInteract(final PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && event.getPlayer().getItemInHand() != null && !event.getPlayer().getItemInHand().getType().equals(Material.AIR) && event.getPlayer().getItemInHand().getType().equals(Material.STICK) && event.getPlayer().getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "Demarcador")) {
            if (TagAPI.getTag(UUID).equalsIgnoreCase("Master") || TagAPI.getTag(UUID).equalsIgnoreCase("Supervisor")) {
                event.setCancelled(true);
                final String pos = event.getClickedBlock().getWorld().getName() + ":" + event.getClickedBlock().getLocation().getBlockX() + ":256:" + event.getClickedBlock().getLocation().getBlockZ();
                if (PositonsHash.get(UUID) == null) {
                    new PositonsHash(UUID, "NULL", pos).insert();

                    player.sendMessage(" \n" + ChatColor.YELLOW + " * Posição 2 definida!\n  " + ChatColor.GRAY + "Mundo: " + event.getClickedBlock().getWorld().getName() + " " + event.getClickedBlock().getLocation().getBlockX() + "x " + event.getClickedBlock().getLocation().getBlockY() + "y " + event.getClickedBlock().getLocation().getBlockZ() + "z\n ");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else if (!PositonsHash.get(UUID).getPos1().equalsIgnoreCase("NULL")) {
                    PositonsHash.get(UUID).setPos2(pos);
                    SelectedHash.get(UUID).sethasSelected(true);

                    player.sendMessage(" \n" + ChatColor.YELLOW + " * Posição 2 definida!\n  " + ChatColor.GRAY + "Mundo: " + event.getClickedBlock().getWorld().getName() + " " + event.getClickedBlock().getLocation().getBlockX() + "x " + event.getClickedBlock().getLocation().getBlockY() + "y " + event.getClickedBlock().getLocation().getBlockZ() + "z\n ");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.sendMessage(ChatColor.RED + " \n * Utilize /proteger novamente, para proteger a região.\n ");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    }, 20L);
                } else {
                    PositonsHash.get(UUID).setPos2(pos);

                    player.sendMessage(" \n" + ChatColor.YELLOW + " * Posição 2 definida!\n  " + ChatColor.GRAY + "Mundo: " + event.getClickedBlock().getWorld().getName() + " " + event.getClickedBlock().getLocation().getBlockX() + "x " + event.getClickedBlock().getLocation().getBlockY() + "y " + event.getClickedBlock().getLocation().getBlockZ() + "z\n ");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        } else if (event.getAction().equals(Action.LEFT_CLICK_BLOCK) && event.getPlayer().getItemInHand() != null && !event.getPlayer().getItemInHand().getType().equals(Material.AIR) && event.getPlayer().getItemInHand().getType().equals(Material.STICK) && event.getPlayer().getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "Demarcador")) {
            if (TagAPI.getTag(UUID).equalsIgnoreCase("Master") || TagAPI.getTag(UUID).equalsIgnoreCase("Supervisor")) {
                event.setCancelled(true);
                final String pos = event.getClickedBlock().getWorld().getName() + ":" + event.getClickedBlock().getLocation().getBlockX() + ":0:" + event.getClickedBlock().getLocation().getBlockZ();
                if (PositonsHash.get(UUID) == null) {
                    new PositonsHash(UUID, pos, "NULL").insert();

                    player.sendMessage(" \n" + ChatColor.YELLOW + " * Posição 1 definida!\n  " + ChatColor.GRAY + "Mundo: " + event.getClickedBlock().getWorld().getName() + " " + event.getClickedBlock().getLocation().getBlockX() + "x " + event.getClickedBlock().getLocation().getBlockY() + "y " + event.getClickedBlock().getLocation().getBlockZ() + "z\n ");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else if (!PositonsHash.get(UUID).getPos2().equalsIgnoreCase("NULL")) {
                    PositonsHash.get(UUID).setPos1(pos);
                    SelectedHash.get(UUID).sethasSelected(true);

                    player.sendMessage(" \n" + ChatColor.YELLOW + " * Posição 1 definida!\n  " + ChatColor.GRAY + "Mundo: " + event.getClickedBlock().getWorld().getName() + " " + event.getClickedBlock().getLocation().getBlockX() + "x " + event.getClickedBlock().getLocation().getBlockY() + "y " + event.getClickedBlock().getLocation().getBlockZ() + "z\n ");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.sendMessage(ChatColor.RED + " \n * Utilize /proteger novamente, para proteger a região.\n ");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    }, 20L);
                } else {
                    PositonsHash.get(UUID).setPos1(pos);

                    player.sendMessage(" \n" + ChatColor.YELLOW + " * Posição 1 definida!\n  " + ChatColor.GRAY + "Mundo: " + event.getClickedBlock().getWorld().getName() + " " + event.getClickedBlock().getLocation().getBlockX() + "x " + event.getClickedBlock().getLocation().getBlockY() + "y " + event.getClickedBlock().getLocation().getBlockZ() + "z\n ");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
    }
}
