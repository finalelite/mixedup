package com.mixedup.worldProtection.listeners;

import com.mixedup.apis.ConfigAPI;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.TagAPI;
import com.mixedup.hashs.JetpackHash;
import com.mixedup.worldProtection.hashs.PlayerInRegionHash;
import com.mixedup.worldProtection.hashs.ProtectionUtil;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.Arrays;

public class RegionEvents implements Listener {

    @EventHandler
    public void onInteract(final PlayerInteractEvent event) {
        if (event.getClickedBlock() != null && !event.getClickedBlock().getType().equals(Material.AIR) && ProtectionUtil.playerInArea(event.getClickedBlock().getLocation()) != null) {
            if (TagAPI.getTag(PlayerUUID.getUUID(event.getPlayer().getName())).equalsIgnoreCase("Master") || TagAPI.getTag(PlayerUUID.getUUID(event.getPlayer().getName())).equalsIgnoreCase("Supervisor"))
                return;
            if (event.getAction().name().contains("RIGHT_CLICK")) {
                if (event.getPlayer().getItemInHand().getType().equals(Material.LAVA_BUCKET) || event.getPlayer().getItemInHand().getType().equals(Material.WATER_BUCKET)) {
                    event.setCancelled(true);
                }
            }
            if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && event.getClickedBlock().getType().equals(Material.ENDER_CHEST)) {
                event.setCancelled(false);
                return;
            } else {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onChangeFoodLevel(final FoodLevelChangeEvent event) {
        if (ProtectionUtil.playerInArea(event.getEntity().getLocation()) != null) {
            if (event.getEntity() instanceof Player) {
                final Player player = (Player) event.getEntity();
                if (player.getFoodLevel() > event.getFoodLevel()) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onDamage(final EntityDamageEvent event) {
        if (ProtectionUtil.playerInArea(event.getEntity().getLocation()) != null) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onExplode(final EntityExplodeEvent event) {
        if (ProtectionUtil.playerInArea(event.getLocation()) != null) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onIgnite(final BlockIgniteEvent event) {
        if (event.getIgnitingEntity() != null && Arrays.asList(BlockIgniteEvent.IgniteCause.values()).contains(event.getCause())) {
            if (event.getIgnitingEntity().getWorld().getName().equalsIgnoreCase("spawn") || event.getIgnitingEntity().getWorld().getName().equalsIgnoreCase("guildaselect") || event.getIgnitingEntity().getWorld().getName().equalsIgnoreCase("aether")) {
                if (TagAPI.getTag(PlayerUUID.getUUID(event.getPlayer().getName())).equalsIgnoreCase("Master") || TagAPI.getTag(PlayerUUID.getUUID(event.getPlayer().getName())).equalsIgnoreCase("Supervisor"))
                    return;
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void entitySpawn(final CreatureSpawnEvent event) {
        if (ProtectionUtil.playerInArea(event.getLocation()) != null) {
            if (event.getEntityType().equals(EntityType.PLAYER)) return;
            if (event.getEntityType().equals(EntityType.ARMOR_STAND) && event.getSpawnReason().equals(SpawnReason.CUSTOM))
                return;
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onBreak(final BlockBreakEvent event) {
        if (ProtectionUtil.playerInArea(event.getBlock().getLocation()) != null) {
            if (TagAPI.getTag(PlayerUUID.getUUID(event.getPlayer().getName())).equalsIgnoreCase("Master")) {
                event.setCancelled(false);
            } else {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPlace(final BlockPlaceEvent event) {
        if (ProtectionUtil.playerInArea(event.getBlock().getLocation()) != null) {
            if (TagAPI.getTag(PlayerUUID.getUUID(event.getPlayer().getName())).equalsIgnoreCase("Master")) {
                event.setCancelled(false);
            } else {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onDrop(final PlayerDropItemEvent event) {
        if (ProtectionUtil.playerInArea(event.getPlayer().getLocation()) != null) {
            event.getPlayer().sendMessage(ChatColor.RED + " * Proibido dropar itens aqui!");
            event.setCancelled(true);
        }
    }

    @EventHandler (priority = EventPriority.MONITOR)
    public void onDrop(final EntityExplodeEvent event) {
        if (ProtectionUtil.playerInArea(event.getEntity().getLocation()) != null) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onMove(final PlayerMoveEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (ProtectionUtil.playerInArea(player.getLocation()) != null) {
            if (PlayerInRegionHash.get(UUID) == null) {
                new PlayerInRegionHash(UUID, true).insert();

                if (player.getWorld().getName().equalsIgnoreCase("aether")) return;
                player.sendTitle(ChatColor.GREEN + "Area protegida!", "", 20, 40, 20);
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                final String[] staff = {"Master", "Supervisor"};
                final String tag = TagAPI.getTag(UUID);
                if (!Arrays.asList(staff).contains(tag)) {
                    player.setGameMode(GameMode.ADVENTURE);
                }
                player.setCollidable(false);
            } else if (PlayerInRegionHash.get(UUID).getStatus() == false) {
                PlayerInRegionHash.get(UUID).setStatus(true);

                if (player.getWorld().getName().equalsIgnoreCase("aether")) return;
                player.sendTitle(ChatColor.GREEN + "Area protegida!", "", 20, 40, 20);
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                final String[] staff = {"Master", "Supervisor"};
                final String tag = TagAPI.getTag(UUID);
                if (!Arrays.asList(staff).contains(tag)) {
                    player.setGameMode(GameMode.ADVENTURE);
                }
                player.setCollidable(false);
            }
        } else if (PlayerInRegionHash.get(UUID) != null && PlayerInRegionHash.get(UUID).getStatus() == true) {
            if (player.getAllowFlight() == true && !player.getWorld().getName().equalsIgnoreCase("spawn") && !player.getWorld().getName().equalsIgnoreCase("guildaselect") && !player.isOp()) {

                final String tag = TagAPI.getTag(player.getUniqueId().toString());
                if (tag != null && !(tag.equalsIgnoreCase("Supervisor") || tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Moderador") || tag.equalsIgnoreCase("Admin"))) {
                    player.setAllowFlight(false);
                    ConfigAPI.updateFly(UUID, false);

                    if (JetpackHash.get(UUID) == null) {
                        new JetpackHash(UUID, false, false).insert();
                    } else {
                        JetpackHash.get(UUID).setStatus(false);
                    }
                }
            }
            PlayerInRegionHash.get(UUID).setStatus(false);

            if (player.getWorld().getName().equalsIgnoreCase("aether")) return;
            if (player.getWorld().getName().equalsIgnoreCase("spawn") || player.getWorld().getName().equalsIgnoreCase("guildaselect"))
                return;
            player.sendTitle(ChatColor.RED + "Area desprotegida!", "", 20, 40, 20);
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

            final String[] staff = {"Master", "Supervisor"};
            final String tag = TagAPI.getTag(UUID);
            if (!Arrays.asList(staff).contains(tag)) {
                player.setGameMode(GameMode.SURVIVAL);
            }
            player.setCollidable(true);
        }
    }
}
