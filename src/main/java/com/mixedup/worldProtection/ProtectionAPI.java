package com.mixedup.worldProtection;

import com.mixedup.MySql;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ProtectionAPI {

    public static void createProtection(final String location) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Protections_data(Location) VALUES (?)");
            st.setString(1, location);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeProtection(final String location) {
        try {
            final PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Protections_data WHERE Location = ?");
            st.setString(1, location);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }
}
