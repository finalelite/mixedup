package com.mixedup.hashs;

import java.util.HashMap;

public class IDprov {

    private static final HashMap<String, IDprov> CACHE = new HashMap<String, IDprov>();
    private static final HashMap<Integer, IDprov> CACHE2 = new HashMap<Integer, IDprov>();
    private String UUID;
    private final int IDNpcPlayer;

    public IDprov(final String UUIDwithNPCid, final int IDNpcPlayer) {
        UUID = UUIDwithNPCid;
        this.IDNpcPlayer = IDNpcPlayer;
    }

    public static IDprov get(final String UUID) {
        return IDprov.CACHE.get(String.valueOf(UUID));
    }

    public static IDprov getIDNpc(final int IDNpcPlayer) {
        return IDprov.CACHE2.get(Integer.valueOf(IDNpcPlayer));
    }

    public IDprov insert() {
        IDprov.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public int getIDNpcPlayer() {
        return IDNpcPlayer;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(final String UUID) {
        this.UUID = UUID;
    }
}
