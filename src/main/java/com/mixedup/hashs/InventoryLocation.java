package com.mixedup.hashs;

import java.util.HashMap;

public class InventoryLocation {

    private static final HashMap<String, InventoryLocation> CACHE = new HashMap<String, InventoryLocation>();
    private final String UUID;
    private String location;

    public InventoryLocation(final String UUID, final String location) {
        this.UUID = UUID;
        this.location = location;
    }

    public static InventoryLocation get(final String UUID) {
        return InventoryLocation.CACHE.get(String.valueOf(UUID));
    }

    public InventoryLocation insert() {
        InventoryLocation.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(final String location) {
        this.location = location;
    }

    public String getUUID() {
        return UUID;
    }
}
