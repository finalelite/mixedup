package com.mixedup.hashs;

import java.util.HashMap;

public class PlayerInSpawnHash {

    private static final HashMap<String, PlayerInSpawnHash> CACHE = new HashMap<String, PlayerInSpawnHash>();
    private final String UUID;
    private boolean Status;

    public PlayerInSpawnHash(final String UUID, final boolean Status) {
        this.UUID = UUID;
        this.Status = Status;
    }

    public static PlayerInSpawnHash get(final String UUID) {
        return PlayerInSpawnHash.CACHE.get(String.valueOf(UUID));
    }

    public PlayerInSpawnHash insert() {
        PlayerInSpawnHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(final boolean Status) {
        this.Status = Status;
    }

    public String getUUID() {
        return UUID;
    }
}
