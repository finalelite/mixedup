package com.mixedup.hashs;

import java.util.HashMap;

public class FurnaceCache {

    public static HashMap<String, FurnaceCache> CACHE = new HashMap<>();
    private final String UUID;
    private boolean has;

    public FurnaceCache(final String UUID, final boolean has) {
        this.UUID = UUID;
        this.has = has;
    }

    public static FurnaceCache get(final String UUID) {
        return FurnaceCache.CACHE.get(UUID);
    }

    public FurnaceCache insert() {
        FurnaceCache.CACHE.put(UUID, this);
        return this;
    }

    public boolean has() {
        return has;
    }

    public void setHas(final boolean has) {
        this.has = has;
    }
}
