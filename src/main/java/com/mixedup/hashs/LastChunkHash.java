package com.mixedup.hashs;

import java.util.HashMap;

public class LastChunkHash {

    private static final HashMap<String, LastChunkHash> CACHE = new HashMap<String, LastChunkHash>();
    private final String UUID;
    private String chunk;

    public LastChunkHash(final String UUID, final String chunk) {
        this.UUID = UUID;
        this.chunk = chunk;
    }

    public static LastChunkHash get(final String UUID) {
        return LastChunkHash.CACHE.get(String.valueOf(UUID));
    }

    public LastChunkHash insert() {
        LastChunkHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public String getChunk() {
        return chunk;
    }

    public void setChunk(final String chunk) {
        this.chunk = chunk;
    }

    public String getUUID() {
        return UUID;
    }
}
