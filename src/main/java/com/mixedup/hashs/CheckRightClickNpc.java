package com.mixedup.hashs;

import java.util.HashMap;

public class CheckRightClickNpc {

    private static final HashMap<String, CheckRightClickNpc> CACHE = new HashMap<String, CheckRightClickNpc>();
    private final String UUID;
    private boolean status;

    public CheckRightClickNpc(final String UUID, final boolean status) {
        this.UUID = UUID;
        this.status = status;
    }

    public static CheckRightClickNpc get(final String UUID) {
        return CheckRightClickNpc.CACHE.get(String.valueOf(UUID));
    }

    public CheckRightClickNpc insert() {
        CheckRightClickNpc.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(final boolean status) {
        this.status = status;
    }

    public String getUUID() {
        return UUID;
    }
}
