package com.mixedup.hashs;

import java.util.HashMap;

public class IDFurnace {

    private static final HashMap<String, IDFurnace> CACHE = new HashMap<String, IDFurnace>();
    private final String UUID;
    private int ID;

    public IDFurnace(final String UUID, final int ID) {
        this.UUID = UUID;
        this.ID = ID;
    }

    public static IDFurnace get(final String UUID) {
        return IDFurnace.CACHE.get(String.valueOf(UUID));
    }

    public IDFurnace insert() {
        IDFurnace.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public int getId() {
        return ID;
    }

    public void setId(final int ID) {
        this.ID = ID;
    }

    public String getUUID() {
        return UUID;
    }
}
