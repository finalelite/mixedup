package com.mixedup.hashs;

import java.util.HashMap;

public class HasLoggedStatus {

    private static final HashMap<String, HasLoggedStatus> CACHE = new HashMap<String, HasLoggedStatus>();
    private final String UUID;
    private boolean Logged;

    public HasLoggedStatus(final String UUID, final boolean Logged) {
        this.UUID = UUID;
        this.Logged = Logged;
    }

    public static HasLoggedStatus get(final String UUID) {
        return HasLoggedStatus.CACHE.get(String.valueOf(UUID));
    }

    public HasLoggedStatus insert() {
        HasLoggedStatus.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getLogged() {
        return Logged;
    }

    public void setLogged(final boolean Logged) {
        this.Logged = Logged;
    }

    public String getUUID() {
        return UUID;
    }
}
