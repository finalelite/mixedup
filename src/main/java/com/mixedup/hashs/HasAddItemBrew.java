package com.mixedup.hashs;

import java.util.HashMap;

public class HasAddItemBrew {

    private static final HashMap<String, HasAddItemBrew> CACHE = new HashMap<String, HasAddItemBrew>();
    private final String UUID;
    private boolean HasAdd;

    public HasAddItemBrew(final String UUID, final boolean HasAdd) {
        this.UUID = UUID;
        this.HasAdd = HasAdd;
    }

    public static HasAddItemBrew get(final String UUID) {
        return HasAddItemBrew.CACHE.get(String.valueOf(UUID));
    }

    public HasAddItemBrew insert() {
        HasAddItemBrew.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getHasAdd() {
        return HasAdd;
    }

    public void setHasAdd(final boolean HasAdd) {
        this.HasAdd = HasAdd;
    }

    public String getUUID() {
        return UUID;
    }
}
