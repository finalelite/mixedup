package com.mixedup.hashs;

import java.util.HashMap;

public class TerrainIsDemarcate {

    private static final HashMap<String, TerrainIsDemarcate> CACHE = new HashMap<String, TerrainIsDemarcate>();
    private final String UUID;
    private boolean status;

    public TerrainIsDemarcate(final String UUID, final boolean status) {
        this.UUID = UUID;
        this.status = status;
    }

    public static TerrainIsDemarcate get(final String UUID) {
        return TerrainIsDemarcate.CACHE.get(String.valueOf(UUID));
    }

    public TerrainIsDemarcate insert() {
        TerrainIsDemarcate.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(final boolean status) {
        this.status = status;
    }

    public String getUUID() {
        return UUID;
    }
}
