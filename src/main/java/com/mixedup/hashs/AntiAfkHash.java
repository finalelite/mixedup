package com.mixedup.hashs;

import java.util.HashMap;

public class AntiAfkHash {

    private String uuid;
    private String view;
    private int times;

    public static HashMap<String, AntiAfkHash> cache = new HashMap<>();

    public AntiAfkHash (String uuid, String view, int times) {
        this.uuid = uuid;
        this.view = view;
        this.times = times;
    }

    public static AntiAfkHash get(String uuid) {
        return cache.get(uuid);
    }

    public AntiAfkHash insert() {
        cache.put(this.uuid, this);
        return this;
    }

    public String getView() {
        return this.view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public int getTimes() {
        return this.times;
    }

    public void setTimes(int times) {
        this.times = times;
    }
}
