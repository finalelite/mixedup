package com.mixedup.hashs;

import java.util.Date;
import java.util.HashMap;

public class LastHitHash {

    public static final HashMap<String, LastHitHash> CACHE = new HashMap<String, LastHitHash>();
    private final String UUID;
    private Date date;

    public LastHitHash(final String UUID, final Date date) {
        this.UUID = UUID;
        this.date = date;
    }

    public static LastHitHash get(final String UUID) {
        return LastHitHash.CACHE.get(String.valueOf(UUID));
    }

    public LastHitHash insert() {
        LastHitHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(final Date date) {
        this.date = date;
    }

    public String getUUID() {
        return UUID;
    }
}
