package com.mixedup.hashs;

import java.util.HashMap;

public class DailyAwardsHash {

    public static HashMap<String, DailyAwardsHash> cache = new HashMap<>();
    private final String user;

    public DailyAwardsHash(final String user) {
        this.user = user;
    }

    public static DailyAwardsHash get(final String user) {
        return DailyAwardsHash.cache.get(user);
    }

    public DailyAwardsHash insert() {
        DailyAwardsHash.cache.put(user, this);
        return this;
    }
}
