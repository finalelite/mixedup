package com.mixedup.hashs;

import java.util.HashMap;

public class Tpa {

    private static final HashMap<String, Tpa> CACHE = new HashMap<String, Tpa>();
    private String UUID;
    private String UUIDtarget;
    private boolean status;

    public Tpa(final String UUID, final String UUIDtarget, final boolean status) {
        this.UUID = UUID;
        this.UUIDtarget = UUIDtarget;
        this.status = status;
    }

    public static Tpa get(final String UUID) {
        return Tpa.CACHE.get(String.valueOf(UUID));
    }

    public Tpa insert() {
        Tpa.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public String getUUIDtarget() {
        return UUIDtarget;
    }

    public void setUUIDtarget(final String UUIDtarget) {
        this.UUIDtarget = UUIDtarget;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(final String UUID) {
        this.UUID = UUID;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(final boolean status) {
        this.status = status;
    }
}
