package com.mixedup.hashs;

import java.util.HashMap;

public class IDNpc {

    private static final HashMap<String, IDNpc> CACHE = new HashMap<String, IDNpc>();
    private final String UUID;
    private int ID;

    public IDNpc(final String UUID, final int ID) {
        this.UUID = UUID;
        this.ID = ID;
    }

    public static IDNpc get(final String UUID) {
        return IDNpc.CACHE.get(String.valueOf(UUID));
    }

    public IDNpc insert() {
        IDNpc.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public int getId() {
        return ID;
    }

    public void setId(final int ID) {
        this.ID = ID;
    }

    public String getUUID() {
        return UUID;
    }


}
