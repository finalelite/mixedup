package com.mixedup.hashs;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class JetpackSaveHash {

    private String location;
    private ItemStack jetpack;
    private Player player;

    public static HashMap<String, JetpackSaveHash> cache = new HashMap<>();

    public JetpackSaveHash (String location, ItemStack jetpack, Player player) {
        this.location = location;
        this.jetpack = jetpack;
        this.player = player;
    }

    public static JetpackSaveHash get(String location) {
        return cache.get(location);
    }

    public JetpackSaveHash insert() {
        cache.put(this.location, this);
        return this;
    }

    public ItemStack getJetpack() {
        return this.jetpack;
    }

    public void setJetpack(ItemStack jetpack) {
        this.jetpack = jetpack;
    }

    public Player getPlayer() {
        return this.player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
