package com.mixedup.hashs;

import java.util.HashMap;

public class GuildaSelectProv {

    private static final HashMap<String, GuildaSelectProv> CACHE = new HashMap<String, GuildaSelectProv>();
    private final String UUID;
    private boolean status;

    public GuildaSelectProv(final String UUID, final boolean status) {
        this.UUID = UUID;
        this.status = status;
    }

    public static GuildaSelectProv get(final String UUID) {
        return GuildaSelectProv.CACHE.get(String.valueOf(UUID));
    }

    public GuildaSelectProv insert() {
        GuildaSelectProv.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(final boolean status) {
        this.status = status;
    }

    public String getUUID() {
        return UUID;
    }
}
