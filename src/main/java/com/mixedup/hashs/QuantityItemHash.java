package com.mixedup.hashs;

import java.util.HashMap;

public class QuantityItemHash {

    private static final HashMap<String, QuantityItemHash> CACHE = new HashMap<String, QuantityItemHash>();
    private final String UUID;
    private int ID;

    public QuantityItemHash(final String UUID, final int ID) {
        this.UUID = UUID;
        this.ID = ID;
    }

    public static QuantityItemHash get(final String UUID) {
        return QuantityItemHash.CACHE.get(String.valueOf(UUID));
    }

    public QuantityItemHash insert() {
        QuantityItemHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public int getId() {
        return ID;
    }

    public void setId(final int ID) {
        this.ID = ID;
    }

    public String getUUID() {
        return UUID;
    }
}
