package com.mixedup.hashs;

import java.util.HashMap;

public class TNTHash {

    private String location;
    private int level;

    public static HashMap<String, TNTHash> cache = new HashMap<>();

    public TNTHash (String location, int level) {
        this.location = location;
        this.level = level;
    }

    public static TNTHash get(String location) {
        return cache.get(location);
    }

    public TNTHash insert() {
        cache.put(this.location, this);
        return this;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
