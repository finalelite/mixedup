package com.mixedup.hashs;

import java.util.HashMap;

public class PlayerInArenaHash {

    private static final HashMap<String, PlayerInArenaHash> CACHE = new HashMap<String, PlayerInArenaHash>();
    private final String UUID;
    private boolean Status;

    public PlayerInArenaHash(final String UUID, final boolean Status) {
        this.UUID = UUID;
        this.Status = Status;
    }

    public static PlayerInArenaHash get(final String UUID) {
        return PlayerInArenaHash.CACHE.get(String.valueOf(UUID));
    }

    public PlayerInArenaHash insert() {
        PlayerInArenaHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(final boolean Status) {
        this.Status = Status;
    }

    public String getUUID() {
        return UUID;
    }
}
