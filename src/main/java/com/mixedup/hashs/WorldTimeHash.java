package com.mixedup.hashs;

import java.util.HashMap;

public class WorldTimeHash {

    private static final HashMap<String, WorldTimeHash> CACHE = new HashMap<String, WorldTimeHash>();
    private final String UUID;
    private boolean Time;
    private boolean Weather;

    public WorldTimeHash(final String UUID, final boolean Time, final boolean Weather) {
        this.UUID = UUID;
        this.Time = Time;
        this.Weather = Weather;
    }

    public static WorldTimeHash get(final String UUID) {
        return WorldTimeHash.CACHE.get(String.valueOf(UUID));
    }

    public WorldTimeHash insert() {
        WorldTimeHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getTime() {
        return Time;
    }

    public void setTime(final boolean Time) {
        this.Time = Time;
    }

    public boolean getWeather() {
        return Weather;
    }

    public void setWeather(final boolean Weather) {
        this.Weather = Weather;
    }

    public String getUUID() {
        return UUID;
    }
}
