package com.mixedup.hashs;

import java.util.HashMap;

public class WrongLocation {

    private static final HashMap<String, WrongLocation> CACHE = new HashMap<String, WrongLocation>();
    private final String UUID;
    private boolean status;

    public WrongLocation(final String UUID, final boolean status) {
        this.UUID = UUID;
        this.status = status;
    }

    public static WrongLocation get(final String UUID) {
        return WrongLocation.CACHE.get(String.valueOf(UUID));
    }

    public WrongLocation insert() {
        WrongLocation.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(final boolean status) {
        this.status = status;
    }

    public String getUUID() {
        return UUID;
    }
}
