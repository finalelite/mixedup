package com.mixedup.hashs;

import java.util.HashMap;

public class JetpackHash {

    private String uuid;
    private boolean status;
    private boolean fix;

    public static HashMap<String, JetpackHash> cache = new HashMap<>();

    public JetpackHash (String uuid, boolean status, boolean fix) {
        this.uuid = uuid;
        this.status = status;
        this.fix = fix;
    }

    public static JetpackHash get(String uuid) {
        return cache.get(uuid);
    }

    public JetpackHash insert() {
        cache.put(this.uuid, this);
        return this;
    }

    public boolean getFix() {
        return this.fix;
    }

    public void setFix(boolean fix) {
        this.fix = fix;
    }

    public boolean getStatus() {
        return this.status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
