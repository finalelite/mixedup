package com.mixedup.hashs;

import com.mixedup.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class PlayerVanishHash {
    private static final List<Player> players = new ArrayList<>();

    public static boolean isVanished(final Player player) {
        return PlayerVanishHash.players.contains(player);
    }

    public static boolean toggleVanish(final Player player) {
        if (PlayerVanishHash.isVanished(player))
            PlayerVanishHash.players.remove(player);
        else
            PlayerVanishHash.players.add(player);

        Bukkit.getOnlinePlayers().forEach(p -> {
            if (!Main.scoreboardManager.hasSquadScoreboard(player))
                Main.scoreboardManager.getDefaultScoreboard().updateEntry(p, "online");
        });
        return PlayerVanishHash.isVanished(player);
    }

    public static List<Player> getPlayers() {
        return new ArrayList<>(PlayerVanishHash.players);
    }

}
