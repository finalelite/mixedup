package com.mixedup.hashs;

import java.util.HashMap;

public class PainelOpenHash {

    private String uuid;
    private String location;

    public static HashMap<String, PainelOpenHash> cache = new HashMap<>();

    public PainelOpenHash (String uuid, String location) {
        this.uuid = uuid;
        this.location = location;
    }

    public static PainelOpenHash get(String uuid) {
        return cache.get(uuid);
    }

    public PainelOpenHash insert() {
        cache.put(this.uuid, this);
        return this;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
