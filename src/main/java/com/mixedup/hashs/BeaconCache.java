package com.mixedup.hashs;

import java.util.HashMap;

public class BeaconCache {

    public static HashMap<String, BeaconCache> CACHE = new HashMap<>();
    //UUID, Location, Multiplos, Forca, Pressa, Regeneracao, Superpulo, Velocidade, Efeito1, Efeito2, Efeito3, Efeito4, Efeito5,, Ativo1, Ativo2, Ativo3, Ativo4, Ativo5, Facstatus, Ativo
    private final String UUID;
    private String location;
    private int multiplos;
    private int forca;
    private int pressa;
    private int regeneracao;
    private int superpulo;
    private int velocidade;
    private int efeito1;
    private int efeito2;
    private int efeito3;
    private int efeito4;
    private int efeito5;
    private boolean ativo1;
    private boolean ativo2;
    private boolean ativo3;
    private boolean ativo4;
    private boolean ativo5;
    private boolean facstatus;
    private boolean ativo;

    public BeaconCache(final String UUID, final String location, final int multiplos, final int forca, final int pressa, final int regeneracao, final int superpulo, final int velocidade, final int efeito1, final int efeito2, final int efeito3, final int efeito4, final int efeito5, final boolean ativo1, final boolean ativo2, final boolean ativo3, final boolean ativo4, final boolean ativo5, final boolean facstatus, final boolean ativo) {
        this.UUID = UUID;
        this.location = location;
        this.multiplos = multiplos;
        this.forca = forca;
        this.pressa = pressa;
        this.regeneracao = regeneracao;
        this.superpulo = superpulo;
        this.velocidade = velocidade;
        this.efeito1 = efeito1;
        this.efeito2 = efeito2;
        this.efeito3 = efeito3;
        this.efeito4 = efeito4;
        this.efeito5 = efeito5;
        this.ativo1 = ativo1;
        this.ativo2 = ativo2;
        this.ativo3 = ativo3;
        this.ativo4 = ativo4;
        this.ativo5 = ativo5;
        this.facstatus = facstatus;
        this.ativo = ativo;
    }

    public static BeaconCache get(final String location) {
        return BeaconCache.CACHE.get(location);
    }

    public BeaconCache insert() {
        BeaconCache.CACHE.put(location, this);
        return this;
    }

    public boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(final boolean ativo) {
        this.ativo = ativo;
    }

    public boolean getFacstatus() {
        return facstatus;
    }

    public void setFacstatus(final boolean facstatus) {
        this.facstatus = facstatus;
    }

    public boolean getAtivo5() {
        return ativo5;
    }

    public void setAtivo5(final boolean ativo5) {
        this.ativo5 = ativo5;
    }

    public boolean getAtivo4() {
        return ativo4;
    }

    public void setAtivo4(final boolean ativo4) {
        this.ativo4 = ativo4;
    }

    public boolean getAtivo3() {
        return ativo3;
    }

    public void setAtivo3(final boolean ativo3) {
        this.ativo3 = ativo3;
    }

    public boolean getAtivo2() {
        return ativo2;
    }

    public void setAtivo2(final boolean ativo2) {
        this.ativo2 = ativo2;
    }

    public boolean getAtivo1() {
        return ativo1;
    }

    public void setAtivo1(final boolean ativo1) {
        this.ativo1 = ativo1;
    }

    public int getEfeito5() {
        return efeito5;
    }

    public void setEfeito5(final int efeito5) {
        this.efeito5 = efeito5;
    }

    public int getEfeito4() {
        return efeito4;
    }

    public void setEfeito4(final int efeito4) {
        this.efeito4 = efeito4;
    }

    public int getEfeito3() {
        return efeito3;
    }

    public void setEfeito3(final int efeito3) {
        this.efeito3 = efeito3;
    }

    public int getEfeito2() {
        return efeito2;
    }

    public void setEfeito2(final int efeito2) {
        this.efeito2 = efeito2;
    }

    public int getEfeito1() {
        return efeito1;
    }

    public void setEfeito1(final int efeito1) {
        this.efeito1 = efeito1;
    }

    public int getVelocidade() {
        return velocidade;
    }

    public void setVelocidade(final int velocidade) {
        this.velocidade = velocidade;
    }

    public int getSuperpulo() {
        return superpulo;
    }

    public void setSuperpulo(final int superpulo) {
        this.superpulo = superpulo;
    }

    public int getRegeneracao() {
        return regeneracao;
    }

    public void setRegeneracao(final int regeneracao) {
        this.regeneracao = regeneracao;
    }

    public int getPressa() {
        return pressa;
    }

    public void setPressa(final int pressa) {
        this.pressa = pressa;
    }

    public int getForca() {
        return forca;
    }

    public void setForca(final int forca) {
        this.forca = forca;
    }

    public int getMultiplos() {
        return multiplos;
    }

    public void setMultiplos(final int multiplos) {
        this.multiplos = multiplos;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(final String location) {
        this.location = location;
    }
}
