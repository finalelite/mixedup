package com.mixedup.hashs;

import java.util.HashMap;

public class AskedServeHash {

    private static final HashMap<String, AskedServeHash> CACHE = new HashMap<String, AskedServeHash>();
    private final String UUID;
    private boolean Asked;

    public AskedServeHash(final String UUID, final boolean Asked) {
        this.UUID = UUID;
        this.Asked = Asked;
    }

    public static AskedServeHash get(final String UUID) {
        return AskedServeHash.CACHE.get(String.valueOf(UUID));
    }

    public AskedServeHash insert() {
        AskedServeHash.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getAsked() {
        return Asked;
    }

    public void setAsked(final boolean Asked) {
        this.Asked = Asked;
    }

    public String getUUID() {
        return UUID;
    }
}
