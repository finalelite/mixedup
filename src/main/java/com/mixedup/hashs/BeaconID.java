package com.mixedup.hashs;

import java.util.HashMap;

public class BeaconID {

    private static final HashMap<String, BeaconID> CACHE = new HashMap<String, BeaconID>();
    private final String UUID;
    private int ID;

    public BeaconID(final String UUID, final int ID) {
        this.UUID = UUID;
        this.ID = ID;
    }

    public static BeaconID get(final String UUID) {
        return BeaconID.CACHE.get(String.valueOf(UUID));
    }

    public BeaconID insert() {
        BeaconID.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public int getId() {
        return ID;
    }

    public void setId(final int ID) {
        this.ID = ID;
    }

    public String getUUID() {
        return UUID;
    }
}
