package com.mixedup.hashs;

import java.util.HashMap;

public class HasNoticed {

    private static final HashMap<String, HasNoticed> CACHE = new HashMap<String, HasNoticed>();
    private final String UUID;
    private boolean isNoticed;

    public HasNoticed(final String UUID, final boolean isNoticed) {
        this.UUID = UUID;
        this.isNoticed = isNoticed;
    }

    public static HasNoticed get(final String UUID) {
        return HasNoticed.CACHE.get(String.valueOf(UUID));
    }

    public HasNoticed insert() {
        HasNoticed.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getIsNoticed() {
        return isNoticed;
    }

    public void setIsNoticed(final boolean isNoticed) {
        this.isNoticed = isNoticed;
    }

    public String getUUID() {
        return UUID;
    }
}
