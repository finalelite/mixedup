package com.mixedup.hashs;

import com.mixedup.Main;
import com.mixedup.MySql;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class DailyHash {

    private String uuid;
    private long time;

    public static HashMap<String, DailyHash> cache = new HashMap<>();

    public DailyHash (String uuid, long time) {
        this.uuid = uuid;
        this.time = time;
    }

    public static DailyHash get(String uuid) {
        return cache.get(uuid);
    }

    public DailyHash insert() {
        cache.put(this.uuid, this);
        return this;
    }

    public long getTime() {
        return this.time;
    }

    public void setTime(long time) {
        this.time = time;
    }


    public static void updateDaily(String uuid, long time) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement st = MySql.con.prepareStatement("UPDATE DailyAwards_data SET time = ? WHERE uuid = ?");
                    st.setString(2, uuid);
                    st.setLong(1, time);
                    st.executeUpdate();

                    get(uuid).setTime(time);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void setDaily(String uuid, long time) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement st = MySql.con.prepareStatement("INSERT INTO DailyAwards_data(uuid, time) VALUES (?, ?)");
                    st.setString(1, uuid);
                    st.setLong(2, time);
                    st.executeUpdate();

                    new DailyHash(uuid, time).insert();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.plugin);
    }

    public static void setCache() {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM DailyAwards_data");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                new DailyHash(rs.getString("uuid"), rs.getLong("time")).insert();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
