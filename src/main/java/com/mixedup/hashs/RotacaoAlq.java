package com.mixedup.hashs;

import java.util.HashMap;

public class RotacaoAlq {

    private static final HashMap<String, RotacaoAlq> CACHE = new HashMap<String, RotacaoAlq>();
    private final String Location;
    private int ID;

    public RotacaoAlq(final String Location, final int ID) {
        this.Location = Location;
        this.ID = ID;
    }

    public static RotacaoAlq get(final String Location) {
        return RotacaoAlq.CACHE.get(String.valueOf(Location));
    }

    public RotacaoAlq insert() {
        RotacaoAlq.CACHE.put(String.valueOf(Location), this);

        return this;
    }

    public int getId() {
        return ID;
    }

    public void setId(final int ID) {
        this.ID = ID;
    }

    public String getLocation() {
        return Location;
    }
}
