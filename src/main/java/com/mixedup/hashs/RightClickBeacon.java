package com.mixedup.hashs;

import java.util.HashMap;

public class RightClickBeacon {

    private static final HashMap<String, RightClickBeacon> CACHE = new HashMap<String, RightClickBeacon>();
    private final String UUID;
    private boolean Cliked;

    public RightClickBeacon(final String UUID, final boolean Cliked) {
        this.UUID = UUID;
        this.Cliked = Cliked;
    }

    public static RightClickBeacon get(final String UUID) {
        return RightClickBeacon.CACHE.get(String.valueOf(UUID));
    }

    public RightClickBeacon insert() {
        RightClickBeacon.CACHE.put(String.valueOf(UUID), this);

        return this;
    }

    public boolean getHasAdd() {
        return Cliked;
    }

    public void setHasAdd(final boolean Cliked) {
        this.Cliked = Cliked;
    }

    public String getUUID() {
        return UUID;
    }
}
