package com.mixedup.chat;

import com.destroystokyo.paper.event.player.PlayerAdvancementCriterionGrantEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class MessageJoin implements Listener {

    @EventHandler
    public void onEntry(final PlayerJoinEvent event) {
        event.setJoinMessage(null);
    }

    @EventHandler
    public void onAchiement(final PlayerAdvancementCriterionGrantEvent event) {
        event.setCancelled(true);
    }
}
