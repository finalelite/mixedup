package com.mixedup.chat;

import com.mixedup.apis.GuildaAPI;
import com.mixedup.apis.PlayerUUID;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class CommandPreprocessEvent implements Listener {

    @EventHandler
    public void onCommand(final PlayerCommandPreprocessEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (GuildaAPI.getGuilda(UUID) == null) {
            if (!event.getMessage().equalsIgnoreCase("/Guildasanguinariaconfirmar") && !event.getMessage().equalsIgnoreCase("/Guildaanciaconfirmar") && !event.getMessage().equalsIgnoreCase("/Guildanobreconfirmar")) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + " * Ops, primeiramente escolha uma Guilda.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
    }

    @EventHandler
    public void onSay(final AsyncPlayerChatEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (GuildaAPI.getGuilda(UUID) == null) {
            event.setCancelled(true);
            player.sendMessage(ChatColor.RED + " * Ops, primeiramente escolha uma Guilda.");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
    }
}
