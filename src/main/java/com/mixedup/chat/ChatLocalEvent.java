package com.mixedup.chat;

import com.mixedup.apis.*;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.hashs.ChatStatus;
import com.mixedup.hashs.PlayerVanishHash;
import com.mixedup.moderacao.PunirAPI;
import com.mixedup.tutorial.TutorialManager;
import com.mixedup.utils.AlternateColor;
import com.mixedup.utils.Chat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Arrays;
import java.util.Date;

public class ChatLocalEvent implements Listener {

    @EventHandler
    public void messageLeft(final PlayerQuitEvent event) {
        event.setQuitMessage(null);
    }

    @EventHandler (ignoreCancelled = true)
    public void onSay(final AsyncPlayerChatEvent event) {
        final Player player = event.getPlayer();
        final String UUID = PlayerUUID.getUUID(player.getName());
        final String msg = AlternateColor.alternate(event.getMessage());

        if (PunirAPI.getExistPunicao(UUID) != null) {
            if (PunirAPI.getIsMutado(UUID) == true) {
                if ((new Date().getTime() / 1000) - (PunirAPI.getTempo(UUID).getTime() / 1000) <= 0) {
                    PunirAPI.removerPunicao(UUID);
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "\n \n(!)Você está mutado!\n(!)Mutado por: " + PunirAPI.getAutor(UUID) + "\n(!)Motivo: " + PunirAPI.getMotivo(UUID) + "\n(!)Tempo da punição: " + HorarioAPI.getTime((new Date().getTime() / 1000) - (PunirAPI.getTempo(UUID).getTime() / 1000)) + "\n \n");
                    return;
                }
            }
        }

        if (Chat.getStatus() == true) {
            if (!TagAPI.getTag(UUID).equals("Master") || !TagAPI.getTag(UUID).equals("Supervisor") || !TagAPI.getTag(UUID).equals("Admin") || !TagAPI.getTag(UUID).equals("Moderador") || !TagAPI.getTag(UUID).equals("Suporte") || !TagAPI.getTag(UUID).equals("Youtuber")) {
                String[] staff = {"Master", "Supervisor", "Admin", "Suporte"};
                if (!Arrays.asList(staff).contains(TagAPI.getTag(UUID))) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "O chat está temporariamente desabilitado!");
                    return;
                }
            }
        }
        event.setCancelled(true);

        if (ChatStatus.get(UUID) != null && ChatStatus.get(UUID).getStatus() == true) {
            final String nome = FacAPI.getFacNome(UUID);
            String lista = null;
            if (!FacAPI.getRecrutas(nome).equals("NULL")) {
                lista = FacAPI.getRecrutas(nome);
            }
            if (!FacAPI.getMembros(nome).equals("NULL")) {
                if (lista == null) {
                    lista = FacAPI.getMembros(nome);
                } else {
                    lista = lista + ":" + FacAPI.getMembros(nome);
                }
            }
            if (!FacAPI.getCapitoes(nome).equals("NULL")) {
                if (lista == null) {
                    lista = FacAPI.getCapitoes(nome);
                } else {
                    lista = lista + ":" + FacAPI.getCapitoes(nome);
                }
            }
            if (!FacAPI.getLideres(nome).equals("NULL")) {
                if (lista == null) {
                    lista = FacAPI.getLideres(nome);
                } else {
                    lista = lista + ":" + FacAPI.getLideres(nome);
                }
            }

            if (lista.contains(":")) {
                final String[] list = lista.split(":");

                for (int i = 1; i <= list.length; i++) {
                    final Player target = Bukkit.getPlayerExact(AccountAPI.getNick(list[i - 1]));

                    if (target != null && target.isOnline() && !TutorialManager.get().inTutorial(target)) {
                        target.sendMessage(ChatColor.YELLOW + "[" + FacAPI.getHierarquia(UUID) + "]" + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + msg);
                    }
                }
            }
            return;
        }

        final int blockDistance = 100;
        final Location playerLocation = event.getPlayer().getLocation();
        boolean has = false;

        for (final Player target : Bukkit.getOnlinePlayers()) {
            if (TutorialManager.get().inTutorial(target)) {
                continue;
            }

            if (!player.getName().equals(target.getName()) && playerLocation.getWorld().equals(target.getLocation().getWorld()) && target.getLocation().distance(playerLocation) <= blockDistance && Bukkit.getOnlinePlayers().size() > 1) {
                final String UUIDtarget = PlayerUUID.getUUID(target.getName());
                if (!PlayerVanishHash.isVanished(target))
                    has = true;

                final String facName = FacAPI.getFacNome(UUID);
                String facTag = null;
                if (facName != null) {
                    facTag = "[" + FacAPI.getTagWithNome(facName) + "]";
                }

                if (TagAPI.getTag(UUID).equals("Membro")) {
                    if (!player.getName().equals(target.getName())) {
                        if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                            if (ConfigAPI.getChatLocalStatus(UUIDtarget) == true) {
                                if (ChatStatus.get(UUIDtarget) == null || ChatStatus.get(UUIDtarget).getStatus() == false) {
                                    target.sendMessage(getGuild(UUID) + ChatColor.GRAY + player.getName() + ": " + ChatColor.YELLOW + msg);
                                }
                            }
                        }
                    } else if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                        player.sendMessage(getGuild(UUID) + ChatColor.GRAY + player.getName() + ": " + ChatColor.YELLOW + msg);
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você está com chat local bloqueado. Para desbloquear utilize: /config");
                    }
                }

                if (TagAPI.getTag(UUID).equals("Master")) {
                    if (!player.getName().equals(target.getName())) {
                        if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                            if (ConfigAPI.getChatLocalStatus(UUIDtarget) == true) {
                                if (ChatStatus.get(UUIDtarget) == null || ChatStatus.get(UUIDtarget).getStatus() == false) {
                                    target.sendMessage(getGuild(UUID) + ChatColor.GOLD + "[GM] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                                }
                            }
                        }
                    } else if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                        player.sendMessage(getGuild(UUID) + ChatColor.GOLD
                                + "[GM] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você está com chat local bloqueado. Para desbloquear utilize: /config");
                    }
                }

                if (TagAPI.getTag(UUID).equals("Supervisor")) {
                    if (!player.getName().equals(target.getName())) {
                        if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                            if (ConfigAPI.getChatLocalStatus(UUIDtarget) == true) {
                                if (ChatStatus.get(UUIDtarget) == null || ChatStatus.get(UUIDtarget).getStatus() == false) {
                                    target.sendMessage(ChatColor.DARK_RED + "[Supervisor] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                                }
                            }
                        }
                    } else if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                        player.sendMessage(getGuild(UUID) + ChatColor.DARK_RED + "[Supervisor] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você está com chat local bloqueado. Para desbloquear utilize: /config");
                    }
                }

                if (TagAPI.getTag(UUID).equals("Admin")) {
                    if (!player.getName().equals(target.getName())) {
                        if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                            if (ConfigAPI.getChatLocalStatus(UUIDtarget) == true) {
                                if (ChatStatus.get(UUIDtarget) == null || ChatStatus.get(UUIDtarget).getStatus() == false) {
                                    target.sendMessage(getGuild(UUID) + ChatColor.RED + "[Admin] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                                }
                            }
                        }
                    } else if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                        player.sendMessage(getGuild(UUID) + ChatColor.RED + "[Admin] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você está com chat local bloqueado. Para desbloquear utilize: /config");
                    }
                }

                if (TagAPI.getTag(UUID).equals("Moderador")) {
                    if (!player.getName().equals(target.getName())) {
                        if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                            if (ConfigAPI.getChatLocalStatus(UUIDtarget) == true) {
                                if (ChatStatus.get(UUIDtarget) == null || ChatStatus.get(UUIDtarget).getStatus() == false) {
                                    target.sendMessage(getGuild(UUID) + ChatColor.DARK_GREEN + "[Moderador] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                                }
                            }
                        }
                    } else if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                        player.sendMessage(ChatColor.DARK_GREEN + "[Moderador] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você está com chat local bloqueado. Para desbloquear utilize: /config");
                    }
                }

                if (TagAPI.getTag(UUID).equals("Suporte")) {
                    if (!player.getName().equals(target.getName())) {
                        if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                            if (ConfigAPI.getChatLocalStatus(UUIDtarget) == true) {
                                if (ChatStatus.get(UUIDtarget) == null || ChatStatus.get(UUIDtarget).getStatus() == false) {
                                    target.sendMessage(getGuild(UUID) + ChatColor.GREEN + "[Suporte] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                                }
                            }
                        }
                    } else if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                        player.sendMessage(getGuild(UUID) + ChatColor.GREEN + "[Suporte] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você está com chat local bloqueado. Para desbloquear utilize: /config");
                    }
                }

                if (TagAPI.getTag(UUID).equals("Youtuber")) {
                    if (!player.getName().equals(target.getName())) {
                        if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                            if (ConfigAPI.getChatLocalStatus(UUIDtarget) == true) {
                                if (ChatStatus.get(UUIDtarget) == null || ChatStatus.get(UUIDtarget).getStatus() == false) {
                                    if (facName == null) {
                                        target.sendMessage(getGuild(UUID) + ChatColor.RED + "[Youtuber] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                                    } else {
                                        target.sendMessage(getGuild(UUID) + ChatColor.GRAY + facTag + ChatColor.RED + "[Youtuber] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                                    }
                                }
                            }
                        }
                    } else if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                        if (facName == null) {
                            player.sendMessage(getGuild(UUID) + ChatColor.RED + "[Youtuber] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                        } else {
                            player.sendMessage(getGuild(UUID) + ChatColor.GRAY + facTag + ChatColor.RED + "[Youtuber] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você está com chat local bloqueado. Para desbloquear utilize: /config");
                    }
                }

                if (TagAPI.getTag(UUID).equals("Vip")) {
                    if (!player.getName().equals(target.getName())) {
                        if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                            if (ConfigAPI.getChatLocalStatus(UUIDtarget) == true) {
                                if (ChatStatus.get(UUIDtarget) == null || ChatStatus.get(UUIDtarget).getStatus() == false) {
                                    if (facName == null) {
                                        target.sendMessage(getGuild(UUID) + ChatColor.YELLOW + "[Conde] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                                    } else {
                                        target.sendMessage(getGuild(UUID) + ChatColor.GRAY + facTag + ChatColor.YELLOW + "[Conde] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                                    }
                                }
                            }
                        }
                    } else if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                        if (facName == null) {
                            player.sendMessage(getGuild(UUID) + ChatColor.YELLOW + "[Conde] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                        } else {
                            player.sendMessage(getGuild(UUID) + ChatColor.GRAY + facTag + ChatColor.YELLOW + "[Conde] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você está com chat local bloqueado. Para desbloquear utilize: /config");
                    }
                }

                if (TagAPI.getTag(UUID).equals("Vip+")) {
                    if (!player.getName().equals(target.getName())) {
                        if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                            if (ConfigAPI.getChatLocalStatus(UUIDtarget) == true) {
                                if (ChatStatus.get(UUIDtarget) == null || ChatStatus.get(UUIDtarget).getStatus() == false) {
                                    if (facName == null) {
                                        target.sendMessage(getGuild(UUID) + ChatColor.AQUA + "[Lord] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                                    } else {
                                        target.sendMessage(getGuild(UUID) + ChatColor.GRAY + facTag + ChatColor.AQUA + "[Lord] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                                    }
                                }
                            }
                        }
                    } else if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                        if (facName == null) {
                            player.sendMessage(getGuild(UUID) + ChatColor.AQUA + "[Lord] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                        } else {
                            player.sendMessage(getGuild(UUID) + ChatColor.GRAY + facTag + ChatColor.AQUA + "[Lord] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você está com chat local bloqueado. Para desbloquear utilize: /config");
                    }
                }

                if (TagAPI.getTag(UUID).equals("Vip++")) {
                    if (!player.getName().equals(target.getName())) {
                        if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                            if (ConfigAPI.getChatLocalStatus(UUIDtarget) == true) {
                                if (ChatStatus.get(UUIDtarget) == null || ChatStatus.get(UUIDtarget).getStatus() == false) {
                                    if (facName == null) {
                                        target.sendMessage(getGuild(UUID) + ChatColor.DARK_RED + "[Duque] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                                    } else {
                                        target.sendMessage(getGuild(UUID) + ChatColor.GRAY + facTag + ChatColor.DARK_RED + "[Duque] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                                    }
                                }
                            }
                        }
                    } else if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                        if (facName == null) {
                            player.sendMessage(getGuild(UUID) + ChatColor.DARK_RED + "[Duque] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                        } else {
                            player.sendMessage(getGuild(UUID) + ChatColor.GRAY + facTag + ChatColor.DARK_RED + "[Duque] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você está com chat local bloqueado. Para desbloquear utilize: /config");
                    }
                }

                if (TagAPI.getTag(UUID).equals("Vip+++")) {
                    if (!player.getName().equals(target.getName())) {
                        if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                            if (ConfigAPI.getChatLocalStatus(UUIDtarget) == true) {
                                if (ChatStatus.get(UUIDtarget) == null || ChatStatus.get(UUIDtarget).getStatus() == false) {
                                    if (facName == null) {
                                        target.sendMessage(getGuild(UUID) + ChatColor.DARK_PURPLE + "[Titan] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                                    } else {
                                        target.sendMessage(getGuild(UUID) + ChatColor.GRAY + facTag + ChatColor.DARK_PURPLE + "[Titan] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                                    }
                                }
                            }
                        }
                    } else if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                        if (facName == null) {
                            player.sendMessage(getGuild(UUID) + ChatColor.DARK_PURPLE + "[Titan] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                        } else {
                            player.sendMessage(getGuild(UUID) + ChatColor.GRAY + facTag + ChatColor.DARK_PURPLE + "[Titan] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você está com chat local bloqueado. Para desbloquear utilize: /config");
                    }
                }
            }
        }

        if (has == true) {
            final String facName = FacAPI.getFacNome(UUID);
            String facTag = null;
            if (facName != null) {
                facTag = "[" + FacAPI.getTagWithNome(facName) + "]";
            }

            if (TagAPI.getTag(UUID).equals("Membro")) {
                if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                    player.sendMessage(getGuild(UUID) + ChatColor.DARK_GRAY + player.getName() + ": " + ChatColor.YELLOW + msg);
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, você está com chat local bloqueado. Para desbloquear utilize: /config");
                }
            }

            if (TagAPI.getTag(UUID).equals("Master")) {
                if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                    player.sendMessage(getGuild(UUID) + ChatColor.GOLD + "[GM] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, você está com chat local bloqueado. Para desbloquear utilize: /config");
                }
            }

            if (TagAPI.getTag(UUID).equals("Supervisor")) {
                if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                    player.sendMessage(getGuild(UUID) + ChatColor.DARK_RED + "[Supervisor] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, você está com chat local bloqueado. Para desbloquear utilize: /config");
                }
            }

            if (TagAPI.getTag(UUID).equals("Admin")) {
                if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                    player.sendMessage(getGuild(UUID) + ChatColor.RED + "[Admin] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, você está com chat local bloqueado. Para desbloquear utilize: /config");
                }
            }

            if (TagAPI.getTag(UUID).equals("Moderador")) {
                if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                    player.sendMessage(getGuild(UUID) + ChatColor.DARK_GREEN + "[Moderador] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, você está com chat local bloqueado. Para desbloquear utilize: /config");
                }
            }

            if (TagAPI.getTag(UUID).equals("Suporte")) {
                if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                    player.sendMessage(getGuild(UUID) + ChatColor.GREEN + "[Suporte] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, você está com chat local bloqueado. Para desbloquear utilize: /config");
                }
            }

            if (TagAPI.getTag(UUID).equals("Youtuber")) {
                if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                    if (facName == null) {
                        player.sendMessage(getGuild(UUID) + ChatColor.RED + "[Youtuber] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                    } else {
                        player.sendMessage(getGuild(UUID) + ChatColor.GRAY + facTag + ChatColor.RED + "[Youtuber] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, você está com chat local bloqueado. Para desbloquear utilize: /config");
                }
            }

            if (TagAPI.getTag(UUID).equals("Vip")) {
                if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                    if (facName == null) {
                        player.sendMessage(getGuild(UUID) + ChatColor.YELLOW + "[Conde] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                    } else {
                        player.sendMessage(getGuild(UUID) + ChatColor.GRAY + facTag + ChatColor.YELLOW + "[Conde] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, você está com chat local bloqueado. Para desbloquear utilize: /config");
                }
            }

            if (TagAPI.getTag(UUID).equals("Vip+")) {
                if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                    if (facName == null) {
                        player.sendMessage(getGuild(UUID) + ChatColor.AQUA + "[Lord] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                    } else {
                        player.sendMessage(getGuild(UUID) + ChatColor.GRAY + facTag + ChatColor.AQUA + "[Lord] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, você está com chat local bloqueado. Para desbloquear utilize: /config");
                }
            }

            if (TagAPI.getTag(UUID).equals("Vip++")) {
                if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                    if (facName == null) {
                        player.sendMessage(getGuild(UUID) + ChatColor.DARK_RED + "[Duque] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                    } else {
                        player.sendMessage(getGuild(UUID) + ChatColor.GRAY + facTag + ChatColor.DARK_RED + "[Duque] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, você está com chat local bloqueado. Para desbloquear utilize: /config");
                }
            }

            if (TagAPI.getTag(UUID).equals("Vip+++")) {
                if (ConfigAPI.getChatLocalStatus(UUID) == true) {
                    if (facName == null) {
                        player.sendMessage(getGuild(UUID) + ChatColor.DARK_PURPLE + "[Titan] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                    } else {
                        player.sendMessage(getGuild(UUID) + ChatColor.GRAY + facTag + ChatColor.DARK_PURPLE + "[Titan] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + msg);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, você está com chat local bloqueado. Para desbloquear utilize: /config");
                }
            }
        }

        if (has == false) {
            player.sendMessage(ChatColor.YELLOW + " * Ops, não há nenhum player próximo a você.");
        }

        has = false;
    }

    private static String getGuild(String uuid) {
        String guild = GuildaAPI.getGuilda(uuid);
        if (guild.equalsIgnoreCase("ancia")) {
            return ChatColor.BLUE + "<A> ";
        } else if (guild.equalsIgnoreCase("nobre")) {
            return ChatColor.DARK_GREEN + "<N> ";
        } else if (guild.equalsIgnoreCase("sanguinaria")) {
            return ChatColor.RED + "<S> ";
        } else {
            return null;
        }
    }
}
