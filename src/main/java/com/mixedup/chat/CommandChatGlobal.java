package com.mixedup.chat;

import com.mixedup.apis.*;
import com.mixedup.factions.FacAPI;
import com.mixedup.factions.hashs.ChatStatus;
import com.mixedup.moderacao.PunirAPI;
import com.mixedup.tutorial.TutorialManager;
import com.mixedup.utils.Chat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Date;

public class CommandChatGlobal implements CommandExecutor {

    public static String alternate(String text) {
        text = text.replace("&k", "").replace("&K", "").replace("&n", "").replace("&N", "").replace("&l", "").replace("&L", "").replace("&m", "").replace("&M", "");
        return text;
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("g")) {
            if (args.length >= 1) {
                if (ChatStatus.get(UUID) != null && ChatStatus.get(UUID).getStatus() == true) {
                    String[] staff = {"Master", "Supervisor", "Admin", "Suporte"};
                    if (!Arrays.asList(staff).contains(TagAPI.getTag(UUID))) {
                        return false;
                    }
                }

                if (PunirAPI.getExistPunicao(UUID) != null) {
                    if (PunirAPI.getIsMutado(UUID) == true) {
                        if ((new Date().getTime() / 1000) - (PunirAPI.getTempo(UUID).getTime() / 1000) <= 0) {
                            PunirAPI.removerPunicao(UUID);
                        } else {
                            player.sendMessage(ChatColor.RED + "\n \n(!)Você está mutado!\n(!)Mutado por: " + PunirAPI.getAutor(UUID) + "\n(!)Motivo: " + PunirAPI.getMotivo(UUID) + "\n(!)Tempo da punição: " + HorarioAPI.getTime((new Date().getTime() / 1000) - (PunirAPI.getTempo(UUID).getTime() / 1000)) + "\n \n");
                            return false;
                        }
                    }
                }

                if (Chat.getStatus() == true) {
                    if (!TagAPI.getTag(UUID).equals("Master") && !TagAPI.getTag(UUID).equals("Supervisor") && !TagAPI.getTag(UUID).equals("Admin") && !TagAPI.getTag(UUID).equals("Moderador") && !TagAPI.getTag(UUID).equals("Suporte") && !TagAPI.getTag(UUID).equals("Youtuber")) {
                        player.sendMessage( ChatColor.RED + "O chat está temporariamente desabilitado!");
                        return false;
                    }
                }

                String msg = "";
                for (final String teste : args) {
                    msg = msg + teste + " ";
                }
                msg.replaceAll("§", "&");

                if (TagAPI.getTag(UUID).equals("Master")) {
                    player.sendMessage(getGuild(UUID) +  ChatColor.GOLD + "[GM] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', msg));

                    for (final Player target : Bukkit.getServer().getOnlinePlayers()) {
                        if (TutorialManager.get().inTutorial(target)) {
                            continue;
                        }
                        final String UUIDtarget = PlayerUUID.getUUID(target.getName());

                        if (!target.getName().equals(player.getName())) {
                            if (ConfigAPI.getChatGlobalStatus(UUIDtarget) == true) {
                                if (ChatStatus.get(UUIDtarget) == null || ChatStatus.get(UUIDtarget).getStatus() == false) {
                                    target.sendMessage(getGuild(UUID) +  ChatColor.GOLD + "[GM] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', msg));
                                }
                            }
                        }
                    }
                }

                if (TagAPI.getTag(UUID).equals("Supervisor")) {
                    player.sendMessage(getGuild(UUID) +  ChatColor.DARK_RED + "[Supervisor] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', msg));

                    for (final Player target : Bukkit.getServer().getOnlinePlayers()) {
                        if (TutorialManager.get().inTutorial(target)) {
                            continue;
                        }
                        final String UUIDtarget = PlayerUUID.getUUID(target.getName());

                        if (!target.getName().equals(player.getName())) {
                            if (ConfigAPI.getChatGlobalStatus(UUIDtarget) == true) {
                                if (ChatStatus.get(UUIDtarget) == null || ChatStatus.get(UUIDtarget).getStatus() == false) {
                                    target.sendMessage(getGuild(UUID) +  ChatColor.DARK_RED + "[Supervisor] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', msg));
                                }
                            }
                        }
                    }
                }

                if (TagAPI.getTag(UUID).equals("Admin")) {
                    player.sendMessage(getGuild(UUID) +  ChatColor.RED + "[Admin] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', msg));

                    for (final Player target : Bukkit.getServer().getOnlinePlayers()) {
                        if (TutorialManager.get().inTutorial(target)) {
                            continue;
                        }
                        final String UUIDtarget = PlayerUUID.getUUID(target.getName());

                        if (!target.getName().equals(player.getName())) {
                            if (ConfigAPI.getChatGlobalStatus(UUIDtarget) == true) {
                                if (ChatStatus.get(UUIDtarget) == null || ChatStatus.get(UUIDtarget).getStatus() == false) {
                                    target.sendMessage(getGuild(UUID) +  ChatColor.RED + "[Admin] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', msg));
                                }
                            }
                        }
                    }
                }

                if (TagAPI.getTag(UUID).equals("Moderador")) {
                    player.sendMessage(getGuild(UUID) +  ChatColor.DARK_GREEN + "[Moderador] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', msg));

                    for (final Player target : Bukkit.getServer().getOnlinePlayers()) {
                        if (TutorialManager.get().inTutorial(target)) {
                            continue;
                        }
                        final String UUIDtarget = PlayerUUID.getUUID(target.getName());

                        if (!target.getName().equals(player.getName())) {
                            if (ConfigAPI.getChatGlobalStatus(UUIDtarget) == true) {
                                if (ChatStatus.get(UUIDtarget) == null || ChatStatus.get(UUIDtarget).getStatus() == false) {
                                    target.sendMessage(getGuild(UUID) +  ChatColor.DARK_GREEN + "[Moderador] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', msg));
                                }
                            }
                        }
                    }
                }

                if (TagAPI.getTag(UUID).equals("Suporte")) {
                    player.sendMessage(getGuild(UUID) +  ChatColor.GREEN + "[Suporte] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', msg));

                    for (final Player target : Bukkit.getServer().getOnlinePlayers()) {
                        if (TutorialManager.get().inTutorial(target)) {
                            continue;
                        }
                        final String UUIDtarget = PlayerUUID.getUUID(target.getName());

                        if (!target.getName().equals(player.getName())) {
                            if (ConfigAPI.getChatGlobalStatus(UUIDtarget) == true) {
                                if (ChatStatus.get(UUIDtarget) == null || ChatStatus.get(UUIDtarget).getStatus() == false) {
                                    target.sendMessage(getGuild(UUID) +  ChatColor.GREEN + "[Suporte] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', msg));
                                }
                            }
                        }
                    }
                }

                final String facName = FacAPI.getFacNome(UUID);
                String facTag = null;
                if (facName != null) {
                    facTag = "[" + FacAPI.getTagWithNome(facName) + "]";
                }

                if (TagAPI.getTag(UUID).equals("Youtuber")) {
                    if (facName == null) {
                        player.sendMessage(getGuild(UUID) +  ChatColor.RED + "[Youtuber] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', msg));
                    } else {
                        player.sendMessage(getGuild(UUID) +  ChatColor.GRAY + facTag + ChatColor.RED + "[Youtuber] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', msg));
                    }

                    for (final Player target : Bukkit.getServer().getOnlinePlayers()) {
                        if (TutorialManager.get().inTutorial(target)) {
                            continue;
                        }
                        final String UUIDtarget = PlayerUUID.getUUID(target.getName());

                        if (!target.getName().equals(player.getName())) {
                            if (ConfigAPI.getChatGlobalStatus(UUIDtarget) == true) {
                                if (ChatStatus.get(UUIDtarget) == null || ChatStatus.get(UUIDtarget).getStatus() == false) {
                                    if (facName == null) {
                                        target.sendMessage(getGuild(UUID) +  ChatColor.RED + "[Youtuber] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', msg));
                                    } else {
                                        target.sendMessage(getGuild(UUID) +  ChatColor.GRAY + facTag + ChatColor.RED + "[Youtuber] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', msg));
                                    }
                                }
                            }
                        }
                    }
                }

                if (TagAPI.getTag(UUID).equals("Vip")) {
                    if (facName == null) {
                        player.sendMessage(getGuild(UUID) +  ChatColor.YELLOW + "[Conde] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + ChatColor.GRAY + CommandChatGlobal.alternate(msg));
                    } else {
                        player.sendMessage(getGuild(UUID) +  ChatColor.GRAY + facTag + ChatColor.YELLOW + "[Conde] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + ChatColor.GRAY + CommandChatGlobal.alternate(msg));
                    }

                    for (final Player target : Bukkit.getServer().getOnlinePlayers()) {
                        if (TutorialManager.get().inTutorial(target)) {
                            continue;
                        }
                        final String UUIDtarget = PlayerUUID.getUUID(target.getName());

                        if (!target.getName().equals(player.getName())) {
                            if (ConfigAPI.getChatGlobalStatus(UUIDtarget) == true) {
                                if (ChatStatus.get(UUIDtarget) == null || ChatStatus.get(UUIDtarget).getStatus() == false) {
                                    if (facName == null) {
                                        target.sendMessage(getGuild(UUID) +  ChatColor.YELLOW + "[Conde] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + ChatColor.GRAY + CommandChatGlobal.alternate(msg));
                                    } else {
                                        target.sendMessage(getGuild(UUID) +  ChatColor.GRAY + facTag + ChatColor.YELLOW + "[Conde] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + ChatColor.GRAY + CommandChatGlobal.alternate(msg));
                                    }
                                }
                            }
                        }
                    }
                }

                if (TagAPI.getTag(UUID).equals("Vip+")) {
                    if (facName == null) {
                        player.sendMessage(getGuild(UUID) +  ChatColor.AQUA + "[Lord] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + CommandChatGlobal.alternate(msg));
                    } else {
                        player.sendMessage(getGuild(UUID) +  ChatColor.GRAY + facTag + ChatColor.AQUA + "[Lord] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + CommandChatGlobal.alternate(msg));
                    }
                    for (final Player target : Bukkit.getServer().getOnlinePlayers()) {
                        if (TutorialManager.get().inTutorial(target)) {
                            continue;
                        }
                        final String UUIDtarget = PlayerUUID.getUUID(target.getName());

                        if (!target.getName().equals(player.getName())) {
                            if (ConfigAPI.getChatGlobalStatus(UUIDtarget) == true) {
                                if (ChatStatus.get(UUIDtarget) == null || ChatStatus.get(UUIDtarget).getStatus() == false) {
                                    if (facName == null) {
                                        target.sendMessage(getGuild(UUID) +  ChatColor.AQUA + "[Lord] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + CommandChatGlobal.alternate(msg));
                                    } else {
                                        target.sendMessage(getGuild(UUID) +  ChatColor.GRAY + facTag + ChatColor.AQUA + "[Lord] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + CommandChatGlobal.alternate(msg));
                                    }
                                }
                            }
                        }
                    }
                }

                if (TagAPI.getTag(UUID).equals("Vip++")) {
                    if (facName == null) {
                        player.sendMessage(getGuild(UUID) +  ChatColor.DARK_RED + "[Duque] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + CommandChatGlobal.alternate(msg));
                    } else {
                        player.sendMessage(getGuild(UUID) +  ChatColor.GRAY + facTag + ChatColor.DARK_RED + "[Duque] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + CommandChatGlobal.alternate(msg));
                    }

                    for (final Player target : Bukkit.getServer().getOnlinePlayers()) {
                        if (TutorialManager.get().inTutorial(target)) {
                            continue;
                        }
                        final String UUIDtarget = PlayerUUID.getUUID(target.getName());

                        if (!target.getName().equals(player.getName())) {
                            if (ConfigAPI.getChatGlobalStatus(UUIDtarget) == true) {
                                if (ChatStatus.get(UUIDtarget) == null || ChatStatus.get(UUIDtarget).getStatus() == false) {
                                    if (facName == null) {
                                        target.sendMessage(getGuild(UUID) +  ChatColor.DARK_RED + "[Duque] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + CommandChatGlobal.alternate(msg));
                                    } else {
                                        target.sendMessage(getGuild(UUID) +  ChatColor.GRAY + facTag + ChatColor.DARK_RED + "[Duque] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + CommandChatGlobal.alternate(msg));
                                    }
                                }
                            }
                        }
                    }
                }

                if (TagAPI.getTag(UUID).equals("Vip+++")) {
                    if (facName == null) {
                        player.sendMessage(getGuild(UUID) +  ChatColor.DARK_PURPLE + "[Titan] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + CommandChatGlobal.alternate(msg));
                    } else {
                        player.sendMessage(getGuild(UUID) +  ChatColor.GRAY + facTag + ChatColor.DARK_PURPLE + "[Titan] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + CommandChatGlobal.alternate(msg));
                    }

                    for (final Player target : Bukkit.getServer().getOnlinePlayers()) {
                        if (TutorialManager.get().inTutorial(target)) {
                            continue;
                        }
                        final String UUIDtarget = PlayerUUID.getUUID(target.getName());

                        if (!target.getName().equals(player.getName())) {
                            if (ConfigAPI.getChatGlobalStatus(UUIDtarget) == true) {
                                if (ChatStatus.get(UUIDtarget) == null || ChatStatus.get(UUIDtarget).getStatus() == false) {
                                    if (facName == null) {
                                        target.sendMessage(getGuild(UUID) +  ChatColor.DARK_PURPLE + "[Titan] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + CommandChatGlobal.alternate(msg));
                                    } else {
                                        target.sendMessage(getGuild(UUID) +  ChatColor.GRAY + facTag + ChatColor.DARK_PURPLE + "[Titan] " + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + CommandChatGlobal.alternate(msg));
                                    }
                                }
                            }
                        }
                    }
                }

                if (TagAPI.getTag(UUID).equals("Membro")) {
                    if (facName == null) {
                        player.sendMessage(getGuild(UUID) +  ChatColor.GRAY + player.getName() + ": " + ChatColor.GRAY + msg);
                    } else {
                        player.sendMessage(getGuild(UUID) +  ChatColor.GRAY + facTag + ChatColor.GRAY + player.getName() + ": " + ChatColor.GRAY + msg);
                    }

                    for (final Player target : Bukkit.getServer().getOnlinePlayers()) {
                        if (TutorialManager.get().inTutorial(target)) {
                            continue;
                        }
                        final String UUIDtarget = PlayerUUID.getUUID(target.getName());

                        if (!target.getName().equals(player.getName())) {
                            if (ConfigAPI.getChatGlobalStatus(UUIDtarget) == true) {
                                if (facName == null) {
                                    target.sendMessage(getGuild(UUID) +  ChatColor.GRAY + player.getName() + ": " + ChatColor.GRAY + msg);
                                } else {
                                    target.sendMessage(getGuild(UUID) +  ChatColor.GRAY + facTag + ChatColor.GRAY + player.getName() + ": " + ChatColor.GRAY + msg);
                                }
                            }
                        }
                    }
                }
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, para conversar no global digite algo após o /g");
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            }
        }
        return false;
    }

    private static String getGuild(String uuid) {
        String guild = GuildaAPI.getGuilda(uuid);
        if (guild.equalsIgnoreCase("ancia")) {
            return ChatColor.BLUE + "<A> ";
        } else if (guild.equalsIgnoreCase("nobre")) {
            return ChatColor.DARK_GREEN + "<N> ";
        } else if (guild.equalsIgnoreCase("sanguinaria")) {
            return ChatColor.RED + "<S> ";
        } else {
            return null;
        }
    }
}
