package com.mixedup.chat;

import com.mixedup.tutorial.TutorialManager;
import com.mixedup.utils.AlternateColor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class MessagesAutomatics {

    private static int time;

    public static void enable() {
        messages();
        actionBar();
    }

    private static void messages() {
        String[] messages = {
                ChatColor.YELLOW + "\nJogadores " + ChatColor.AQUA + "VIPs" + ChatColor.YELLOW + " possuem diversas vantagens no servidor,\nAdquira você também acessando: " + ChatColor.AQUA + "www.finalelite.com.br\n ",
                ChatColor.GOLD + "§l\nMEGA PROMOÇÃO!" + ChatColor.AQUA + " 30% " + ChatColor.YELLOW + "de desconto em toda nossa loja!\nAcesse " + ChatColor.AQUA + "www.finalelite.com.br" + ChatColor.YELLOW + " para conferir.\n ",
                ChatColor.GREEN + "\nEncontrou algum erro ou infrator em nosso servidor?\nReporte-os no canal " + ChatColor.RED + "#suporte" + ChatColor.GREEN + " de nosso Discord.\n ",
                ChatColor.GREEN + "\nPossui alguma dúvida relacionada ao servidor?\nUtilize " + ChatColor.RED + "/ajuda" + ChatColor.GREEN + " ou contate algum de nossos suportes.\n "
        };
        final Thread thread = new Thread(() -> {
            while (true) {
                try {
                    Thread.currentThread().sleep(1000 * 90);
                } catch (final Exception e) {
                    e.printStackTrace();
                }

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    if (TutorialManager.get().inTutorial(target)) {
                        continue;
                    }

                    target.sendMessage(messages[time]);
                }
                time++;

                if (time > 3) {
                    time = 0;
                }
            }
        });
        thread.start();
    }

    private static void actionBar() {
        final Thread thread = new Thread(() -> {
            while (true) {
                try {
                    Thread.currentThread().sleep(1000 * 45);
                } catch (final Exception e) {
                    e.printStackTrace();
                }

                for (final Player target : Bukkit.getOnlinePlayers()) {
                    target.sendActionBar(AlternateColor.alternate(ChatColor.RED + "Servidor em versão BETA!"));
                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 100.0f);
                }
            }
        });
        thread.start();
    }
}
