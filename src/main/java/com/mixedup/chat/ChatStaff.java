package com.mixedup.chat;

import com.mixedup.Main;
import com.mixedup.MySqlHub;
import com.mixedup.apis.PlayerUUID;
import com.mixedup.apis.ServerAPI;
import com.mixedup.apis.TagAPI;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ChatStaff implements CommandExecutor {

    private static int getLastID() {
        int id = 0;

        try {
            final PreparedStatement st = MySqlHub.con.prepareStatement("SELECT max(ID) FROM Staff_online");
            final ResultSet rs = st.executeQuery();
            if (rs.next()) {
                id = rs.getInt(1);
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

        final Player player = (Player) sender;
        final String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("s") || cmd.getName().equalsIgnoreCase("staff")) {
            if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor") || TagAPI.getTag(UUID).equals("Admin") || TagAPI.getTag(UUID).equals("Moderador") || TagAPI.getTag(UUID).equals("Suporte")) {
                if (args.length == 0) {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/s (msg)" + ChatColor.DARK_GRAY + " ou " + ChatColor.GRAY + "/staff (msg)");
                    return false;
                }
                if (args.length >= 1) {
                    final StringBuilder build = new StringBuilder();
                    for (int i = 0; i < args.length; i++) {
                        build.append(" ");
                        build.append(args[i]);
                    }
                    final String msg = build.toString();

                    if (TagAPI.getTag(UUID).equals("Master")) {
                        int i = 1;

                        while (i <= ChatStaff.getLastID()) {
                            if (ServerAPI.getStaffOnlineID(i) != null) {
                                final String nick = ServerAPI.getStaffOnlineID(i);

                                if (!player.getName().equals(nick)) {
                                    try {
                                        final ByteArrayOutputStream b = new ByteArrayOutputStream();
                                        final DataOutputStream out = new DataOutputStream(b);

                                        out.writeUTF("Message");
                                        out.writeUTF(nick);
                                        out.writeUTF(ChatColor.RED + "[STAFF] " + ChatColor.GOLD + player.getName() + ChatColor.GRAY + " : " + ChatColor.WHITE + msg);

                                        player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                                    } catch (final Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    player.sendMessage(ChatColor.RED + "[STAFF] " + ChatColor.GOLD + player.getName() + ChatColor.GRAY + " : " + ChatColor.WHITE + msg);
                                }
                                i++;
                            } else {
                                i++;
                            }
                        }
                    }

                    if (TagAPI.getTag(UUID).equals("Supervisor")) {
                        int i = 1;

                        while (i <= ChatStaff.getLastID()) {
                            if (ServerAPI.getStaffOnlineID(i) != null) {
                                final String nick = ServerAPI.getStaffOnlineID(i);

                                if (!player.getName().equals(nick)) {
                                    try {
                                        final ByteArrayOutputStream b = new ByteArrayOutputStream();
                                        final DataOutputStream out = new DataOutputStream(b);

                                        out.writeUTF("Message");
                                        out.writeUTF(nick);
                                        out.writeUTF(ChatColor.RED + "[STAFF] " + ChatColor.DARK_RED + player.getName() + ChatColor.GRAY + " : " + ChatColor.WHITE + msg);

                                        player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                                    } catch (final Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    player.sendMessage(ChatColor.RED + "[STAFF] " + ChatColor.DARK_RED + player.getName() + ChatColor.GRAY + " : " + ChatColor.WHITE + msg);
                                }
                                i++;
                            } else {
                                i++;
                            }
                        }
                    }

                    if (TagAPI.getTag(UUID).equals("Admin")) {
                        int i = 1;

                        while (i <= ChatStaff.getLastID()) {
                            if (ServerAPI.getStaffOnlineID(i) != null) {
                                final String nick = ServerAPI.getStaffOnlineID(i);

                                if (!player.getName().equals(nick)) {
                                    try {
                                        final ByteArrayOutputStream b = new ByteArrayOutputStream();
                                        final DataOutputStream out = new DataOutputStream(b);

                                        out.writeUTF("Message");
                                        out.writeUTF(nick);
                                        out.writeUTF(ChatColor.RED + "[STAFF] " + ChatColor.RED + player.getName() + ChatColor.GRAY + " : " + ChatColor.WHITE + msg);

                                        player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                                    } catch (final Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    player.sendMessage(ChatColor.RED + "[STAFF] " + ChatColor.RED + player.getName() + ChatColor.GRAY + " : " + ChatColor.WHITE + msg);
                                }
                                i++;
                            } else {
                                i++;
                            }
                        }
                    }


                    if (TagAPI.getTag(UUID).equals("Moderador")) {
                        int i = 1;

                        while (i <= ChatStaff.getLastID()) {
                            if (ServerAPI.getStaffOnlineID(i) != null) {
                                final String nick = ServerAPI.getStaffOnlineID(i);

                                if (!player.getName().equals(nick)) {
                                    try {
                                        final ByteArrayOutputStream b = new ByteArrayOutputStream();
                                        final DataOutputStream out = new DataOutputStream(b);

                                        out.writeUTF("Message");
                                        out.writeUTF(nick);
                                        out.writeUTF(ChatColor.RED + "[STAFF] " + ChatColor.DARK_GREEN + player.getName() + ChatColor.GRAY + " : " + ChatColor.WHITE + msg);

                                        player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                                    } catch (final Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    player.sendMessage(ChatColor.RED + "[STAFF] " + ChatColor.DARK_GREEN + player.getName() + ChatColor.GRAY + " : " + ChatColor.WHITE + msg);
                                }
                                i++;
                            } else {
                                i++;
                            }
                        }
                    }

                    if (TagAPI.getTag(UUID).equals("Suporte")) {
                        int i = 1;

                        while (i <= ChatStaff.getLastID()) {
                            if (ServerAPI.getStaffOnlineID(i) != null) {
                                final String nick = ServerAPI.getStaffOnlineID(i);

                                if (!player.getName().equals(nick)) {
                                    try {
                                        final ByteArrayOutputStream b = new ByteArrayOutputStream();
                                        final DataOutputStream out = new DataOutputStream(b);

                                        out.writeUTF("Message");
                                        out.writeUTF(nick);
                                        out.writeUTF(ChatColor.RED + "[STAFF] " + ChatColor.GREEN + player.getName() + ChatColor.GRAY + " : " + ChatColor.WHITE + msg);

                                        player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                                    } catch (final Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    player.sendMessage(ChatColor.RED + "[STAFF] " + ChatColor.GREEN + player.getName() + ChatColor.GRAY + " : " + ChatColor.WHITE + msg);
                                }
                                i++;
                            } else {
                                i++;
                            }
                        }
                    }
                }
                return false;
            }
        }
        return false;
    }
}
