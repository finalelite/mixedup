package com.mixedup.chat;

import com.mixedup.apis.ConfigAPI;
import com.mixedup.hashs.PlayerVanishHash;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class TellCommand implements CommandExecutor {

    private static final HashMap<Player, Player> tells = new HashMap<>();

    public static void remove(final Player p) {

        if (TellCommand.tells.containsKey(p)) {

            TellCommand.tells.remove(TellCommand.tells.get(p));
            TellCommand.tells.remove(p);

        }

    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String lb, final String[] args) {

        if (sender instanceof Player) {

            final Player p = (Player) sender;

            if (cmd.getName().equalsIgnoreCase("tell") || cmd.getName().equalsIgnoreCase("msg")) {

                if (args.length < 2) {

                    p.sendMessage(ChatColor.RED + " * O uso correto deste comando é /" + lb + " <jogador> <mensagem>.");
                    return true;

                } else {

                    final Player p2 = Bukkit.getPlayer(args[0]);

                    if (p2 != null && !PlayerVanishHash.isVanished(p2)) {

                        if (p2.getName().equals(p.getName())) {

                            p.sendMessage(ChatColor.RED + " * Você está tentando falar consigo mesmo? procure um médico.");
                            return true;

                        } else {

                            final String uuid = p2.getUniqueId().toString();

                            if (ConfigAPI.getTellStatus(uuid)) {

                                final String msg = String.join(" ", args).substring(args[0].length());

                                p2.sendMessage(ChatColor.GREEN + " * " + ChatColor.YELLOW + p.getName() + ChatColor.GRAY + " disse a você:" + msg);
                                p.sendMessage(ChatColor.GREEN + " * " + ChatColor.GRAY + "Você disse á " + ChatColor.YELLOW + p2.getName() + ChatColor.DARK_GRAY + ":" + ChatColor.GRAY + msg);

                                if (TellCommand.tells.containsKey(p2)) {

                                    if (!TellCommand.tells.get(p2).equals(p2)) {

                                        TellCommand.tells.remove(p2);
                                        TellCommand.tells.put(p2, p);

                                    }

                                } else {

                                    TellCommand.tells.put(p2, p);

                                }

                                if (TellCommand.tells.containsKey(p)) {

                                    if (!TellCommand.tells.get(p).equals(p2)) {

                                        TellCommand.tells.remove(p);
                                        TellCommand.tells.put(p, p2);
                                        return true;

                                    } else {

                                        return true;

                                    }

                                } else {

                                    TellCommand.tells.put(p, p2);
                                    return true;

                                }

                            } else {

                                p.sendMessage(ChatColor.RED + " Ops, este jogador está com mensagens privadas desativadas.");
                                return true;

                            }

                        }

                    } else {

                        p.sendMessage(ChatColor.RED + " * Ops, esse jogador aparentemente está offline no momento.");

                    }

                }

            } else {

                if (cmd.getName().equalsIgnoreCase("r")) {

                    if (args.length >= 1) {

                        if (TellCommand.tells.containsKey(p) && !PlayerVanishHash.isVanished(TellCommand.tells.get(p))) {

                            final Player p2 = TellCommand.tells.get(p);

                            if (ConfigAPI.getTellStatus(p2.getUniqueId().toString())) {

                                final String msg = String.join(" ", args);

                                p2.sendMessage(ChatColor.GREEN + " * " + ChatColor.YELLOW + p.getName() + ChatColor.GRAY + " disse a você: " + msg);
                                p.sendMessage(ChatColor.GREEN + " * " + ChatColor.GRAY + "Você disse á " + ChatColor.YELLOW + p2.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + msg);
                                return true;

                            } else {

                                p.sendMessage(ChatColor.RED + " * Ops, este jogador está com mensagens privadas desativadas.");
                                return true;

                            }

                        } else {

                            p.sendMessage(ChatColor.RED + " * Ops, você não está falando com ninguém ou o mesmo aparentemente saiu.");
                            return true;

                        }

                    } else {

                        p.sendMessage(ChatColor.RED + " * O uso correto deste comando é /r <mensagem>.");
                        return true;

                    }

                }

            }

        } else {

            sender.sendMessage(ChatColor.RED + " * Este comando está disponível apenas para jogadores.");
            return true;

        }

        return true;

    }
}
